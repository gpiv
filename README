
GPIV is a graphic user interface for analyzing images obtained from a
fluid flow that has been seeded with tracer particles by the so-called
Particle Image Velocimetry technique. It is meant to have a quick
overview of the parameters of all piv processes, easily changing them,
running the processes and visualizing their results in an interactive
way. It uses LIBGPIV, which contains the main routines for data in-and
output, processing etc. The software is written in ANSI-C under the
LINUX operating system by using the GTK/GNOME libraries.

GPIV is Free Software licensed under the GNU
Public license terms. See the COPYING file for the license.

The main web page can be found at: http://gpiv.sourceforge.net/



Compilation and installation 
============================ 

The General instructions of compilation and installation can be found
in INSTALL.  There are some additional configuration options for this
program, as well. These are: 
--enable-cam
--enable-trig
--enable-rta=RTA_DIR
--enable-k=K_DIR
--enable-img-width=WIDTH
--enable-img-height=HEIGHT 
--enable-debug 

An explanation of these options is printed with ./configure --help.



Features
=========

General
========
- Data storage in ASCII format or HDF version 5 format.
- Loading of images with with file-open menu, "open" button or with "drag and 
  drop" from the gnome file-manager "nautilus" into the buffer list.
- Quick execution of all enabled process on all selected buffers.


Display
========
- Visualization of interrogation area contours and highlighting them when 
  pointing to it.
- Displays pointer position and belonging estimated values.


Image recording
===============
- Sending trigger pulses over the parallel port by using Real Time Linux and 
  RTAI
- Obtaining images from a IEEE1394 (Firewire) IIDC_Compliant CCD camera

Image info
===========
- shows image name, dimensions and type of correlation (cross or auto).
- defining of spatial and time scales, position of image within the experiment.
- Adding and updating header information (date of creation, project, comment 
  etc).


Image evaluation
=================
- Interrogation at the entire image or at a defined region within the image
  frames, resulting into a displacement field on a rectangular grid.
- Interrogation at a single arbitrary point, along a vertical or horizontal 
  line.
- Re-interrogation of a single area with, different parameters.
- Dragging a single interrogation area from the rectangular grid.
- Arbitrary interrogation area sizes.
- Global pre-shifting.
- Local pre-shifting or zero offsetting of the interrogation areas.
- Forward and central interrogation scheme.
- Adaptive interrogation area sizes to obtain high resolution and dynamic range.
- Different interpolation schemes for sub-pixel estimation.
- Cross and auto correlation.
- Defining the order of correlation peak to be used as estimator.
- Visualization of interrogation area's and correlation function.


Data validation
===========
- Manually enabling and disabling of PIV estimators interactively.
- Checking on peak-locking effects.
- Validation on outliers by snr value or median test.


Post-processing
===============
- apply time and spatial scaling
- calculate statistics and subtraction of mean values from estimators
- calculation of vorticity and strain.

