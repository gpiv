/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (Callback) functions for console
 * $Log: console.c,v $
 * Revision 1.30  2008-05-07 08:34:20  gerber
 * uses .h5 and .H5 extension for hdf files (previously: .gpi)
 *
 * Revision 1.29  2008-04-28 12:00:57  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.28  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.27  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.26  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.25  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.24  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.23  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.22  2006-09-18 07:27:03  gerber
 * *** empty log message ***
 *
 * Revision 1.21  2006/01/31 14:28:11  gerber
 * version 0.3.0
 *
 * Revision 1.19  2005/02/26 09:43:30  gerber
 * parameter flags (parameter_logic) defined as gboolean
 *
 * Revision 1.18  2005/02/26 09:17:13  gerber
 * structured of interrogate function by using gpiv_piv_isiadapt
 *
 * Revision 1.17  2005/02/12 13:09:21  gerber
 * Changing some structure and constant names for DAC
 *
 * Revision 1.16  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.15  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.14  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.13  2003/09/04 13:31:54  gerber
 * init of printing (unfinished)
 *
 * Revision 1.12  2003/09/01 11:17:14  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.11  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.10  2003/07/31 11:43:26  gerber
 * display images in gnome canvas (HOERAreset)
 *
 * Revision 1.9  2003/07/25 15:40:23  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.8  2003/07/13 14:38:18  gerber
 * changed error handling of libgpiv
 *
 * Revision 1.7  2003/07/12 21:21:15  gerber
 * changed error handling libgpiv
 *
 * Revision 1.5  2003/07/10 11:56:07  gerber
 * added man page
 *
 * Revision 1.4  2003/07/06 15:29:49  gerber
 * repair message text closing buffer
 *
 * Revision 1.3  2003/07/05 13:14:57  gerber
 * drag and drop of a _series_ of filenames from NAUTILUS
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include "support.h"

#include "gpiv_gui.h"
#include "console_interface.h"
#include "dialog_interface.h"
#include "console_menus.h"
#include "console.h"
#include "display.h"

#ifdef ENABLE_DAC
#include "dac.h"
#endif /* ENABLE_DAC */

#ifdef ENABLE_TRIG
#include "dac_trig.h"
#endif /* ENABLE_TRIG */

#ifdef ENABLE_CAM
#include "dac_cam.h"
#endif /* ENABLE_CAM */

#include "piveval_interrogate.h"
#include "pivvalid.h"
#include "pivpost.h"
#include "preferences.h"
#include "print.h"
#include "io.h"
#include "utils.h"

#define CHOOSER


static void
file_saveas_accept (GpivConsole *gpiv, 
                    const gchar *fname
                    );

typedef enum _Format Format;
enum _Format {
    RAW_ASCII,
    HDF,
    GPIV_DAVIS,
}; 


static void
exec_save (Display * disp);
/*
 * Local functions
 */

static void
exec_save (Display * disp)
/*-----------------------------------------------------------------------------
 * Store all data in display_act->file_uri_name.h5 in hdf5 format
 */
{
    char *err_msg = NULL;
    char *fname_out_nosuf, *fname_par;

    gchar *uri_string_out, *uri_string_tmp;
    GnomeVFSURI *uri_out = NULL, *uri_tmp = NULL;


/*
 * Create a valid uri_out to build the local or tmp fname_out_nosuf. 
 * Type of suffix doesn't actually matter as the correct suffix will
 * be extended later to fname_out
 */
    if (gpiv_par->verbose) g_message ("exec_save:: file_uri_name = %s", display_act->file_uri_name);
    uri_string_out = 
        gnome_vfs_make_uri_from_input (g_strdup_printf 
                                       ("%s%s" ,display_act->file_uri_name, 
                                        GPIV_EXT_GPIV));
    uri_out = gnome_vfs_uri_new (uri_string_out);
    g_free (uri_string_out);

    if (gnome_vfs_uri_is_local (uri_out)) {
        fname_out_nosuf = g_strdup_printf ("%s" , display_act->file_uri_name);
        if (gpiv_par->verbose) g_message ("exec_save:: LOCAL fname_out_nosuf = %s", fname_out_nosuf);
    } else {
        const gchar      *tmp_dir = g_get_tmp_dir ();
        const gchar      *user_name = g_get_user_name ();
        fname_out_nosuf = g_strdup_printf ("%s/%s/%s" , tmp_dir, 
                                     user_name, 
                                     strtok (gnome_vfs_uri_extract_short_name 
                                             (uri_out), "."));
        if (gpiv_par->verbose) g_message ("exec_save:: URI fname_out_suf for to tmp: %s", fname_out_nosuf);
    }
/*     return; */


    if (gpiv_par->verbose) g_message ("exec_save:: STARTING THE REAL JOB");
    if (gpiv_par->hdf) {
        write_hdf_img_data (fname_out_nosuf, uri_out);

    } else {
/*
 * Store all data in ASCII format at different files
 */
#define SAVE_IMG
#ifdef SAVE_IMG
        write_img (fname_out_nosuf, uri_out);
#endif /* SAVE_IMG */
        write_ascii_parameters (fname_out_nosuf, uri_out, GPIV_EXT_PAR);
        write_ascii_data (fname_out_nosuf, uri_out);
    }

    g_free (fname_out_nosuf);

}


/*
 * Main gpiv-gui callbacks
 */


void
delete_console (GtkWidget *widget,
                GdkEvent  *event,
                gpointer   data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    guint i;
    GtkDialog *gpiv_exit = NULL;

#ifdef ENABLE_TRIG
    exec_trigger_stop ();
#endif /* ENABLE_TRIG */
#ifdef ENABLE_CAM
    exec_cam_stop ();
#endif /* ENABLE_CAM */

    if (nbufs > 0) {
        for (i = 0; i < nbufs; i++) {
/*             if (display[i] != NULL) display_act = display[i]; */
        display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), i); 
            if (!display_act->img->saved_img 
               || !display_act->pida->saved_piv
               || !display_act->pida->saved_histo
               || !display_act->pida->saved_vor
               || !display_act->pida->saved_nstrain
               || !display_act->pida->saved_sstrain) {
                gpiv_exit = create_exit_dialog ();
                gtk_widget_show_all (GTK_WIDGET (gpiv_exit));
            } else {
                free_all_bufmems (display_act);
                gtk_main_quit ();
            }
            
        }
        
    } else {
        gtk_main_quit ();
    }
}



void
on_clist_buf_rowselect (GtkWidget *clist,
                        gint row,
                        gint column, 
                        GdkEventButton *event, 
                        gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    gchar *text;
    GpivConsole * gpiv = gtk_object_get_data (GTK_OBJECT (clist), "gpiv");
    GList *lis; 
    gint cnt = 0, buf_dum = 0;
    gboolean saved_img_local;

  
    if (!exec_process && GTK_CLIST (clist)->selection) { 
        for (lis = GTK_CLIST (clist)->selection; lis; lis = lis->next) { 
            row = GPOINTER_TO_INT (lis->data);
/*             g_message ("on_clist_buf_rowselect:: row = %d", row); */
            gtk_clist_get_text (GTK_CLIST (clist), row, /* column */ 0, &text);
            display_act = gtk_clist_get_row_data (GTK_CLIST (clist /*= gpiv->clist_buf */), 
                                                  row); 
            saved_img_local = display_act->img->saved_img;

            if (cnt == 0) {
                gpiv->first_selected_row = row;
            } else {
                gpiv->last_selected_row = row;
            }
/*
 * BUGFIX: update variables of display before leaving the focus. 
 * BUGFIX: Already done when creating display
 */
            if (display_act->intreg->exist_int) {
                display_act->intreg->row_start_old = 0;
                display_act->intreg->col_start_old = 0;
                display_act->intreg->par->row_start = gl_piv_par->row_start;
                display_act->intreg->par->row_end = gl_piv_par->row_end;
                display_act->intreg->par->col_start = gl_piv_par->col_start;
                display_act->intreg->par->col_end = gl_piv_par->col_end;
                display_act->intreg->par->int_size_f = gl_piv_par->int_size_f;
                display_act->intreg->par->int_size_i = gl_piv_par->int_size_i;
                display_act->intreg->par->int_shift = gl_piv_par->int_shift;
                display_act->intreg->par->pre_shift_row = gl_piv_par->pre_shift_row;
                display_act->intreg->par->pre_shift_col = gl_piv_par->pre_shift_col;
            }


/* if (GTK_WIDGET_REALIZED (display_act->pida.display)) */

/*             display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf),  */
/*                                                   row);  */

/*
 * Set variables of new active display equal to parameters
 */
            if (display_act->intreg->exist_int
                && (display_act->intreg->par->row_start != gl_piv_par->row_start
                    || display_act->intreg->par->row_end != gl_piv_par->row_end
                    || display_act->intreg->par->col_start != gl_piv_par->col_start
                    || display_act->intreg->par->col_end != gl_piv_par->col_end
                    || display_act->intreg->par->int_size_f != gl_piv_par->int_size_f
                    || display_act->intreg->par->int_size_i != gl_piv_par->int_size_i
                    || display_act->intreg->par->int_shift != gl_piv_par->int_shift
                    || display_act->intreg->par->pre_shift_row != gl_piv_par->pre_shift_row
                    || display_act->intreg->par->pre_shift_col != gl_piv_par->pre_shift_col)
                ) {
                destroy_all_intregs (display_act);
                create_all_intregs (display_act);
            }

            if (display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) 
                ) { 
/*                 gchar *author = "Guppie GRAAF"; */
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
                
/*
 * update labels and entries in image header tab/window
 */
                update_imgh_entries (gpiv, display_act->img->image->header);
                display_act->img->saved_img = saved_img_local;
                update_eval_entries (gpiv, display_act->img->image->header);
            }


            cnt++; 
        }

        if (cnt == 1) {
            gpiv->last_selected_row = gpiv->first_selected_row;
        }
        
/*
 * exchange first and last
 */
        if (gpiv->last_selected_row < gpiv->first_selected_row) {
            buf_dum = gpiv->last_selected_row;
            gpiv->last_selected_row = gpiv->first_selected_row;
            gpiv->first_selected_row = buf_dum;
        }
        
    }
/*     g_message("on_clist_buf_rowselect:: nbufs = %d, first=%d last = %d", */
/*               nbufs, gpiv->first_selected_row, gpiv->last_selected_row); */

}



void  
on_clist_buf_drag_data_received  (GtkWidget          *widget,
                                  GdkDragContext     *context,
                                  gint                x,
                                  gint                y,
                                  GtkSelectionData   *selection_data,
                                  guint               info,
                                  guint               time)
/*-----------------------------------------------------------------------------
 * load the data from selected uris with drag and drop
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *fname_in; 
    gchar **uri_v;
    gint i;

    if (info != TARGET_URI_LIST) {
        g_message ("on_clist_buf_drag_data_received: info != TARGET_URI_LIST");
        return;
    }
    if (selection_data == NULL) {
        g_message ("on_clist_buf_drag_data_received: selection_data == NULL");
        return;
    }
    if(selection_data->length < 0) {
        g_message ("on_clist_buf_drag_data_received: selection_data->length < 0");
        return;
    }

    uri_v = g_uri_list_extract_uris ((gchar *) selection_data->data);

    for (i = 0; uri_v[i] != NULL; i++) {
        if (gpiv_par->verbose) g_message ("on_clist_buf_drag_data_received:: uri_v[%d] = %s", i, uri_v[i]);
        fname_in = g_strdup (uri_v[i]);
        g_strchomp (fname_in);
        select_action_from_name (gpiv, fname_in);
        g_free (fname_in);
    }

	g_strfreev (uri_v);
}


void
on_open_activate_response (GtkDialog *dialog,
                          gint response,
                          gpointer data
                          )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(dialog), "gpiv");
/*     Display *disp = gtk_object_get_data (GTK_OBJECT(dialog), "display"); */
        char *filename;


    g_assert ( response == GTK_RESPONSE_ACCEPT
              || response == GTK_RESPONSE_CANCEL);

    switch (response) {
    case GTK_RESPONSE_ACCEPT:
        filename = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
        if (gpiv_par->verbose) g_message ("on_open_activate:: filename = %s", filename);
        select_action_from_name (gpiv, filename);
        g_free (filename);
        break;

    case GTK_RESPONSE_CANCEL:
        break;

    default:
        g_warning("on_open_activate_response: should not arrive here");
        break;
    }
}



void 
on_open_activate (GtkMenuItem *menuitem, 
                  gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (menuitem), "gpiv");
    GtkWidget *dialog;

    if (gpiv_par->verbose) g_message ("on_open_activate:: fname_last = %s", 
                                      gpiv_var->fname_last);
    dialog = gtk_file_chooser_dialog_new ("Open Uri/File",
                                          GTK_WINDOW (gpiv->console),
                                          GTK_FILE_CHOOSER_ACTION_OPEN,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                          NULL);
    
    gtk_file_chooser_set_local_only (GTK_FILE_CHOOSER (dialog), FALSE); 
    gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), gpiv_var->fname_last);

#undef USE_CALLBACK
#ifdef USE_CALLBACK
/* Will not open dialog when pressing ^O */
    g_signal_connect (dialog,
                      "response",
                      G_CALLBACK (on_open_activate_response),
                      NULL);
    
    g_signal_connect_swapped (GTK_DIALOG (dialog),
                              "response", 
                              G_CALLBACK (gtk_widget_destroy),
                              dialog);

    gtk_object_set_data (GTK_OBJECT (dialog),
                         "gpiv",
                         gpiv);

#else
    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
        char *filename;

        filename = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
        if (gpiv_par->verbose) g_message ("on_open_activate:: filename = %s", filename);
        select_action_from_name (gpiv, filename);
        g_free (filename);
    }
    
    gtk_widget_destroy (dialog);
#endif /* USE_CALLBACK */
#ifdef USE_CALLBACK
#undef USE_CALLBACK
#endif
}


void 
on_save_activate (GtkMenuItem * menuitem, 
                  gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data (GTK_OBJECT (menuitem), "gpiv");
    save_all_data (gpiv);
}


void 
on_save_as_activate (GtkMenuItem *menuitem, 
                     gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (menuitem), "gpiv");
    GtkWidget *dialog;

    dialog = gtk_file_chooser_dialog_new ("Save as",
                                          GTK_WINDOW (gpiv->console),
                                          GTK_FILE_CHOOSER_ACTION_SAVE,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                          NULL);
    
    gtk_file_chooser_set_local_only (GTK_FILE_CHOOSER (dialog), FALSE); 
    gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), gpiv_var->fname_last);

    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
        char *fname = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
        if (gpiv_par->verbose) g_message ("on_save_as_activate:: fname = %s", fname);
        file_saveas_accept (gpiv, fname /* dialog */);
        g_free (fname);
    }
     
    gtk_widget_destroy (dialog);
}


static void
file_saveas_accept (GpivConsole *gpiv, 
                    const gchar *fname
                    )
/* ----------------------------------------------------------------------------
 * Stores PIV image, data and related under different name
 */
{
    gchar *text_uri, *suffix;
    GnomeVFSURI* uri = NULL;
    gchar *fname_base;  /* file name without suffix and directory name */ 
    gchar *clist_buf_txt[MAX_BUFS][2], cl_int[3];
    gboolean local_hdf_par;

/*
 * Set gpiv_par->hdf if filename has suffix GPIV_EXT_GPIV.
 * Reset after calling exec_save.
 * Set saved_img to FALSE to guarantee saving image.
 */
    suffix = g_strdup (strrchr (fname, '.'));
    if (strcmp (suffix, GPIV_EXT_GPIV) == 0) {
        local_hdf_par = gpiv_par->hdf;
        gpiv_par->img_fmt = IMG_FMT_HDF;
    }
    display_act->img->saved_img = FALSE;

/*
 * Check if uri is local filesystem
 * create proper local filename or uri and short name
 */
    text_uri = gnome_vfs_make_uri_from_shell_arg (fname);
    uri = gnome_vfs_uri_new (text_uri);
    g_free (text_uri);

    if (gnome_vfs_uri_is_local (uri)) {
        const gchar    *path =  gnome_vfs_uri_get_path (uri);
        gchar          *dirname,     /* directory name */
                       *fname_nosuf,   /* filename including dirname/uri, without suffix */
                       *fname_home;  /* filename with $HOME substituted by ~, without suffix */ 

        fname_home = replace_home_dir_with_tilde (path);
        dirname = g_strdup (g_path_get_dirname (fname_home));
        g_free (fname_home);

/*         dirname = g_strdup (g_path_get_dirname  */
/*                             (replace_home_dir_with_tilde (path))); */

        fname_base = g_strdup (strtok (/*g_path_get_basename (path) */ dirname, "."));
        strtok (fname_base, ".");
        fname_nosuf = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S, fname_base, NULL));
        fname_home = replace_home_dir_with_tilde (fname_nosuf);
        if (gpiv_par->verbose) g_message ("file_saveas_accept:: LOCAL dirname = %s fname_base = %s fname_home = %s", 
                   dirname, fname_base, fname_home);

        display_act->file_uri_name = g_strdup_printf ("%s", fname_home);

        g_free (dirname);
        g_free (fname_home);
        g_free (fname_nosuf);

    } else {
        fname_base = strtok (gnome_vfs_uri_extract_short_name (uri), ".");
        g_snprintf (display_act->file_uri_name, GPIV_MAX_CHARS, "%s", 
                    strtok (gnome_vfs_uri_to_string (uri, TRUE), "."));
    }

/*
 * Substituting file_uri_name in display_act
 * Changing fname in clist and in Image tab of the console
 * storing data of display_act
 * Resetting gpiv_par->hdf (and gpiv_par->img_fmt)
 */
    gtk_clist_remove (GTK_CLIST (gpiv->clist_buf), display_act->id);
    g_snprintf (cl_int, 3, "%d", display_act->id);
    clist_buf_txt[display_act->id][0] = (gchar *) cl_int;
    clist_buf_txt[display_act->id][1] = fname_base;
    gtk_clist_insert (GTK_CLIST (gpiv->clist_buf), display_act->id, 
                     clist_buf_txt[display_act->id]);
            g_snprintf (display_act->msg_display_default, GPIV_MAX_CHARS, "%s",
                display_act->file_uri_name);
    gnome_appbar_set_default (GNOME_APPBAR (display_act->appbar),
                              display_act->msg_display_default);
    update_imgh_entries (gpiv, display_act->img->image->header);
    exec_save (display_act);

    if (strcmp (suffix, GPIV_EXT_GPIV) == 0) {
/*         gpiv_par->hdf = local_hdf_par; */
        gpiv_par->img_fmt = default_par->img_fmt;
    }

    g_free (suffix);
    g_free (fname_base);
}



void
save_all_data (GpivConsole * gpiv)
/*-----------------------------------------------------------------------------
 */
{
    guint row, ibuf;

    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                  row); 
            ibuf = display_act->id;
            if (display[ibuf] != NULL
                && display_act->mwin != NULL
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }

            exec_save (display_act);

        }
    }
}



void 
on_print_activate (GtkMenuItem * menuitem, 
                   gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (menuitem), "gpiv");
    GtkDialog *gpiv_print_dialog = create_print_dialog (gpiv);
    gtk_widget_show (GTK_WIDGET (gpiv_print_dialog));
}



void 
on_execute_activate (GtkMenuItem * menuitem, 
                     gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data (GTK_OBJECT (menuitem), "gpiv");
    guint row, ibuf;

    cancel_process = FALSE;

#ifdef ENABLE_CAM
#ifdef ENABLE_TRIG
/*
 * RTA may trigger camera, but camera and RTAI trigger system may also be used
 * separately
 */
    if (/* !cancel_process && */ gpiv_par->process__trig 
        && gpiv_par->process__cam) {
        exec_cam_start (gpiv);
        exec_trigger_start ();
        if (trig_par->ttime->mode == GPIV_TIMER_MODE__DURATION) {
            gpiv->first_selected_row = 0;
            gpiv->last_selected_row = trig_par->ttime->cycles - 1;
        }
    } else if (/* !cancel_process && */ gpiv_par->process__cam) {
        exec_cam_start (gpiv);
        if (gl_cam_par->mode == GPIV_CAM_MODE__DURATION) {
            gpiv->first_selected_row = 0;
            gpiv->last_selected_row = gl_cam_par->cycles - 1;
        }
    } else if (/* !cancel_process && */ gpiv_par->process__trig) {
        exec_trigger_start ();
    }
#else /* ENABLE_TRIG */
/*
 * RTA triggering disabled, only camera may be used
 */
    if (/* !cancel_process && */ gpiv_par->process__cam) {
        exec_cam_start (gpiv);
        if (gl_cam_par->mode == GPIV_CAM_MODE__DURATION) {
            gpiv->first_selected_row = 0;
            gpiv->last_selected_row = gl_cam_par->cycles - 1;
        }
    }
#endif /* ENABLE_TRIG */
#else /* ENABLE_CAM */
#ifdef ENABLE_TRIG
/*
 * Only RTA triggering may be used, camera disabled
 */
if (/* !cancel_process && */ gpiv_par->process__trig) {
        exec_trigger_start ();
    }
#endif /* ENABLE_TRIG */
#endif /* ENABLE_CAM */


    if (nbufs > 0) {
        exec_process = TRUE;
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                  row); 
            if (display[display_act->id] != NULL
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }
            
#ifdef ENABLE_IMGPROC
            if (!cancel_process && gpiv_par->process__imgproc) {
                exec_imgproc (gpiv);
            }
#endif

            if (!cancel_process && gpiv_par->process__piv) {
                exec_piv (gpiv);
            }

            if (!cancel_process && gpiv_par->process__gradient) {
                exec_gradient ();
            }

            if (!cancel_process && gpiv_par->process__resstats) {
                gpiv_var->residu_stats = TRUE;
                exec_errvec (gpiv->pivvalid);
            }

            if (!cancel_process && gpiv_par->process__errvec) {
                gpiv_var->residu_stats = FALSE;
                exec_errvec (gpiv->pivvalid);
            }

            if (!cancel_process && gpiv_par->process__peaklock) {
                exec_peaklock (gpiv->pivvalid);
            }

            if (!cancel_process && gpiv_par->process__scale) {
                exec_scale (gpiv->pivpost);
            }

            if (!cancel_process && gpiv_par->process__average) {
                exec_savg (gpiv->pivpost);
            }

            if (!cancel_process && gpiv_par->process__subtract) {
                exec_subavg (gpiv->pivpost);
            }

            if (!cancel_process && gpiv_par->process__vorstra) {
                exec_vorstra ();
            }

        }

        exec_process = FALSE;
    }
}


void 
on_stop_activate (GtkMenuItem *menuitem, 
                  gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
/*     warning_gpiv(_("process has been stopped")); */
}


void
on_button_stop_press (GtkWidget * widget, 
                      gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    cancel_process = TRUE;
    message_gpiv (_("cancel_process = TRUE"));
}


void
on_button_stop_release (GtkWidget * widget, 
                        gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    cancel_process = FALSE;
    message_gpiv (_("cancel_process = FALSE"));
/* process has been stopped */
}


/* static gboolean  */
/* on_display_delete(GtkWidget *widget, */
/*                   GdkEvent  *event, */
/*                   gpointer   data) */
/* { */
/*     return FALSE; */
/* } */


void
on_close_activate (GtkMenuItem * menuitem, 
                   gpointer user_data)
/*-----------------------------------------------------------------------------
 * Remove multiple selected displays
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (menuitem), "gpiv");
    Display *disp = NULL;

    gchar *err_msg;
    guint row;
    guint row_close = gpiv->first_selected_row; /* row to be closed */
    guint first = gpiv->first_selected_row; /* first_selected_row will be modified! */
    guint last = gpiv->last_selected_row;   /* last_selected_row will be modified! */
    guint loc_nbufs = nbufs;
    gboolean buf_closed = TRUE;

#ifdef ENABLE_CAM
    if ((err_msg = gpiv_cam_free_camvar (&cam_var)) != NULL) {
        error_gpiv ("from: on_close_activate\n from: gpiv_cam_free_camvar\n %s", 
                  err_msg);
    }
#endif /* ENABLE_CAM */

    if (nbufs > 0) {
/*
 * Depending on the response if a display/buffer with unsaved data will be closed,
 * the same or next row in the clist_buf will taken for selecting next display.
 */
        for (row = first; row <= last; row++) {
            if (!buf_closed) {
                if (row_close < nbufs - 1) {
                    row_close++;
                }
            }
            disp = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf),
                                                row_close);
            close_buffer__check_saved (gpiv, disp);

/*
 * Check if a buffer has been closed
 */
            if (loc_nbufs == nbufs) {
                buf_closed = FALSE;
            } else {
                buf_closed = TRUE;
                loc_nbufs = nbufs;
            }
        }       


    } else {
        update_imgh_entries (gpiv, gl_image_par);
        update_eval_entries (gpiv, gl_image_par);
    }

#ifdef ENABLE_IMGPROC
    set_imgproc_filtervar (gpiv, GPIV_IMGFI_SUBACK, nbufs - 1);
#endif
}



void 
on_exit_activate (GtkMenuItem * menuitem,
                  gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (menuitem), "gpiv");
    GtkDialog *gpiv_exit;
    guint i;

#ifdef ENABLE_TRIG
    exec_trigger_stop ();
#endif /* ENABLE_TRIG */

#ifdef ENABLE_CAM
    exec_cam_stop ();
#endif /* ENABLE_CAM */

    if (nbufs > 0) {
        for (i = 0; i < nbufs; i++) {
/*             if (display[i] != NULL) display_act = display[i]; */
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), i); 

            if (!display_act->img->saved_img
                || !display_act->pida->saved_piv
                || !display_act->pida->saved_histo
                || !display_act->pida->saved_vor
                || !display_act->pida->saved_nstrain
                || !display_act->pida->saved_sstrain) {
                gpiv_exit = create_exit_dialog ();
                gtk_widget_show (GTK_WIDGET (gpiv_exit));
            } else {
                free_all_bufmems (display_act);
                gtk_main_quit ();
            }

        }
        
    } else {
        gtk_main_quit ();
    }
}


void
on_preferences_activate (GtkWidget * widget,
                         gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    GtkDialog *preferences = NULL ; 

    preferences = create_preferences (gpiv);
}



void on_about_activate (GtkMenuItem * menuitem, 
                        gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GtkWidget *about;
    about = create_about ();

}




void on_manual_activate (GtkMenuItem * menuitem, 
                         gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GError* error = NULL;

    if (!gnome_help_display ("index.html", NULL, &error)) {
        g_warning ("%s", error);
	g_error_free (error);
    }


}

/*
 * Gnome toolbar buttons
 */


void 
on_button_open_clicked (GtkButton *button, 
                        gpointer data)
/*-----------------------------------------------------------------------------
 * Uses gtk2
 */
{
    gchar *msg = "Opens PIV image";
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (button), "gpiv");
    GtkWidget *dialog = NULL;


    if (gpiv_par->verbose) g_message ("on_button_open_activate:: fname_last = %s", gpiv_var->fname_last);
    dialog = gtk_file_chooser_dialog_new ("Open Uri/File",
                                          GTK_WINDOW (gpiv->console),
                                          GTK_FILE_CHOOSER_ACTION_OPEN,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                          NULL);
    
    gtk_file_chooser_set_local_only (GTK_FILE_CHOOSER (dialog), FALSE); 
    gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), gpiv_var->fname_last);

    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
        char *filename = NULL;

        filename = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
        if (gpiv_par->verbose) g_message ("on_button_open_activate:: filename = %s", filename);
        select_action_from_name (gpiv, filename);
        g_free (filename);
    }
    
    gtk_widget_destroy (dialog);

    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_menubar_activate (GtkWidget * widget, 
                     gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    if (GTK_CHECK_MENU_ITEM (widget)->active) {
        gtk_widget_show (gpiv->menubar);
    } else {
        gtk_widget_hide (gpiv->menubar);
    }
}



void 
on_toolbuttons_activate (GtkWidget * widget, 
                         gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    if (GTK_CHECK_MENU_ITEM (widget)->active) {
        gtk_widget_show (gpiv->toolbar1);
/*    gtk_widget_ref(settings_menu_gpiv_popup[1].widget); */
/* 	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM */
/* 				       (settings_menu_gpiv_popup[1].widget),  */
/*                                        TRUE); */
    } else {
	 gtk_widget_hide (gpiv->toolbar1);
/* 	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM */
/* 				       (settings_menu_gpiv_popup[1].widget),  */
/*                                        FALSE); */
    }

}



void 
on_gpivbuttons_activate (GtkWidget * widget, 
                         gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    if (GTK_CHECK_MENU_ITEM (widget)->active) {
        gtk_widget_show (gpiv->handlebox1);

    } else {
        gtk_widget_hide (gpiv->handlebox1);
    }
}



void 
on_tabulator_activate (GtkWidget * widget, 
                       gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    if (GTK_CHECK_MENU_ITEM (widget)->active) {
        gtk_widget_show (gpiv->notebook);
    } else {
        gtk_widget_hide (gpiv->notebook);
    }

}



void 
on_tooltip_activate (GtkWidget * widget, 
                     gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    if (GTK_CHECK_MENU_ITEM (widget)->active) {
        gtk_tooltips_enable (gpiv->tooltips );
    } else {
        gtk_tooltips_disable (gpiv->tooltips );
    }


}



void
on_buffer_set_focus (GtkWindow * window,
                     GtkWidget * widget, 
                     gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    /* SEE: on_clist_buf_rowselect */
}



void 
gtk_window_destroy (GtkButton * button, 
                    gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{

}



void
on_appbar_display_user_response (GnomeAppBar * gnomeappbar,
                                 gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{

}



void
on_button_open_enter (GtkContainer * container,
                      GtkDirectionType direction, 
                      gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (container), "gpiv");
    gchar *msg = _("Opens image/PIV data (and display)");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_save_enter (GtkContainer * container,
                      GtkDirectionType direction, 
                      gpointer user_data)
{/*-----------------------------------------------------------------------------
  */

    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (container), "gpiv");
    gchar *msg = _("Saves data");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_print_enter (GtkContainer * container,
                       GtkDirectionType direction, 
                       gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (container), "gpiv");
    gchar *msg = _("Prints selected buffer(s)");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_execute_enter (GtkContainer * container,
                         GtkDirectionType direction, 
                         gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (container), "gpiv");
    gchar *msg = _("Executes all tickmarked processes");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_stop_enter (GtkContainer * container,
                      GtkDirectionType direction, 
                      gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (container), "gpiv");
    gchar *msg = _("Cancels any running processes");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_close_enter (GtkContainer * container,
                       GtkDirectionType direction, 
                       gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (container), "gpiv");
    gchar *msg = _("Close active buffer(s)");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_exit_enter (GtkContainer * container,
                      GtkDirectionType direction, 
                      gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (container), "gpiv");
    gchar *msg = _("Exits GPIV");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



#ifdef ENABLE_CAM
void 
on_toolbar_checkbutton_cam (GtkWidget * widget, 
                            gpointer data)
/*-----------------------------------------------------------------------------
 * toolbar containing checkbutton for recording process
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__cam = TRUE;
    } else {
	gpiv_par->process__cam = FALSE;
    }
}
#endif /* ENABLE_CAM */


#ifdef ENABLE_TRIG
void 
on_toolbar_checkbutton_trig (GtkWidget * widget, 
                             gpointer data)
/*-----------------------------------------------------------------------------
 * toolbar containing checkbutton for triggering process
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__trig = TRUE;
    } else {
	gpiv_par->process__trig = FALSE;
    }
}
#endif /* ENABLE_TRIG */


#ifdef ENABLE_IMGPROC
void
on_toolbar_checkbutton_imgproc (GtkWidget * widget, 
                                gpointer data)
/*-----------------------------------------------------------------------------
 * toolbar containing checkbutton for image process
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__imgproc = TRUE;
    } else {
	gpiv_par->process__imgproc = FALSE;
    }
}
#endif /* ENABLE_IMGPROC */



void 
on_toolbar_checkbutton_piv (GtkWidget * widget, 
                            gpointer data)
/*-----------------------------------------------------------------------------
 * toolbar containing checkbutton for processes
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__piv = TRUE;
    } else {
	gpiv_par->process__piv = FALSE;
    }
}



void 
on_toolbar_checkbutton_gradient (GtkWidget * widget, 
                                 gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__gradient = TRUE;
    } else {
	gpiv_par->process__gradient = FALSE;
    }
}



void 
on_toolbar_checkbutton_resstats (GtkWidget * widget, 
                                 gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__resstats = TRUE;
        gpiv_var->residu_stats = TRUE;
    } else {
	gpiv_par->process__resstats = FALSE;
        gpiv_var->residu_stats = FALSE;
    }
}



void 
on_toolbar_checkbutton_errvec (GtkWidget * widget, 
                               gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__errvec = TRUE;
    } else {
	gpiv_par->process__errvec = FALSE;
    }
}



void 
on_toolbar_checkbutton_peaklck (GtkWidget * widget, 
                                gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__peaklock = TRUE;
    } else {
	gpiv_par->process__peaklock = FALSE;
    }
}



void 
on_toolbar_checkbutton_scale (GtkWidget * widget, 
                              gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__scale = TRUE;
    } else {
	gpiv_par->process__scale = FALSE;
    }
}



void 
on_toolbar_checkbutton_average (GtkWidget * widget, 
                                gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__average = TRUE;
    } else {
	gpiv_par->process__average = FALSE;
    }
}



void 
on_toolbar_checkbutton_subavg (GtkWidget * widget, 
                               gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__subtract = TRUE;
    } else {
	gpiv_par->process__subtract = FALSE;
    }
}



void 
on_toolbar_checkbutton_vorstra (GtkWidget * widget, 
                                gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
	gpiv_par->process__vorstra = TRUE;
    } else {
	gpiv_par->process__vorstra = FALSE;
    }
}




/* PivData *pida_active = &display_act->pida; */

void 
on_button_quit_no_clicked (GtkButton * button, 
                           gpointer user_data)
/*-----------------------------------------------------------------------------
 * exit, message dialog callbacks
 */
{
/*     gnome_dialog_close (GTK_DIALOG (gpiv_exit)); */
/*
 * Gnome2:
 */
/*     gtk_widget_destroy (GTK_DIALOG (GTK_DIALOG (gpiv_exit))); */
}



void 
on_button_quit_gpiv_yes_clicked (GtkButton * button, 
                                 gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    free_all_bufmems (display_act);
    gtk_main_quit ();
}



/* void  */
/* on_button_message_clicked (GtkButton * button,  */
/*                           gpointer user_data) */
/* BUGFIX: obsolete function: on_button_message_clicked; clean up */
void
on_button_message_clicked (GtkDialog *dialog,
                           gint response,
                           gpointer data
                           )
/*-----------------------------------------------------------------------------
 */
{
    g_assert (response == GTK_RESPONSE_ACCEPT);

    switch (response) {
    case GTK_RESPONSE_ACCEPT:
        /*         gnome_dialog_close (GNOME_DIALOG (gpiv_exit)); */
        /*         gtk_widget_destroy (GTK_DIALOG (gpiv_exit)); */

    default:
        g_warning ("on_message_clicked: should not arrive here");
        break;
    }
}



void
on_notebook_switch_page (GtkNotebook *notebook,
                         GtkNotebookPage *page,
                         gint page_num,
                         gpointer user_data
                         )
/*-----------------------------------------------------------------------------
 */
{
    gpiv_var->tab_pos = page_num;

    gnome_config_push_prefix ("/gpiv/RuntimeVariables/");
    gnome_config_set_int ("tab_pos", gpiv_var->tab_pos);
    gnome_config_pop_prefix ();
    gnome_config_sync ();


}
