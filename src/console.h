/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */


/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/
/*
 * General callbacks
 * $Log: console.h,v $
 * Revision 1.13  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.12  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.11  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.10  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.9  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.8  2006-09-18 07:27:04  gerber
 * *** empty log message ***
 *
 * Revision 1.7  2006/01/31 14:28:11  gerber
 * version 0.3.0
 *
 * Revision 1.6  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.5  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.4  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 * Revision 1.3  2003/09/01 11:17:14  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.2  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef CONSOLE_H
#define CONSOLE_H

GtkWidget *gpiv_exit;

/*
 * Callback functions for main
 */

void 
on_clist_buf_rowselect (GtkWidget *clist, 
                        gint row, 
                        gint column,
                        GdkEventButton *event, 
                        gpointer data);


void  
on_clist_buf_drag_data_received (GtkWidget *widget,
                                 GdkDragContext *context,
                                 gint x,
                                 gint y,
                                 GtkSelectionData *selection_data,
                                 guint info,
                                 guint time);

void 
delete_console (GtkWidget *widget,
                GdkEvent  *event,
                gpointer   data);

/*
 * Select sub-menu
 */
void 
select_all (gpointer data, 
            guint action, 
            GtkWidget *widget);

void 
select_none (gpointer data, 
             guint action, 
             GtkWidget *widget);

void
on_open_activate (GtkMenuItem *menuitem,
                  gpointer user_data);

void
on_save_activate (GtkMenuItem *menuitem,
                  gpointer user_data);

void
save_all_data (GpivConsole *gpiv);

void
on_print_activate (GtkMenuItem *menuitem,
                   gpointer user_data);
void
on_execute_activate (GtkMenuItem *menuitem,
                     gpointer user_data);
void
on_stop_activate (GtkMenuItem *menuitem,
                  gpointer user_data);

void
on_close_activate (GtkMenuItem *menuitem,
                   gpointer user_data);

void
on_save_as_activate (GtkMenuItem *menuitem,
                     gpointer user_data);

/* void */
/* file_saveas_ok_sel(GtkWidget *widget,  */
/*                    GtkFileSelection * fs */
/*                    ); */

void
file_saveas_ok_sel (GpivConsole *gpiv, 
                   GtkWidget *dialog
                   );

void
on_exit_activate (GtkMenuItem *menuitem,
                 gpointer user_data);

void
on_close_buffer_activate (GtkMenuItem *menuitem,
                 gpointer user_data);

void
on_preferences_activate (GtkWidget *widget,
/*                         GtkMenuItem *menuitem, */
                        gpointer user_data);

void
on_about_activate (GtkMenuItem *menuitem,
                  gpointer user_data);


void
on_manual_activate (GtkMenuItem *menuitem,
                    gpointer user_data);

void
on_button_open_clicked (GtkButton *button,
                       gpointer user_data);

void
on_menubar_activate (GtkWidget *widget, 
                     gpointer data);


void
on_toolbuttons_activate (GtkWidget *widget, 
                         gpointer data);


void
on_gpivbuttons_activate (GtkWidget *widget, 
                         gpointer data);


void
on_tabulator_activate (GtkWidget *widget, 
                       gpointer data);


void
on_tooltip_activate (GtkWidget *widget, 
                     gpointer data);


void
on_buffer_set_focus                    (GtkWindow       *window,
                                        GtkWidget       *widget,
					gpointer         user_data);

void
gtk_window_destroy                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_view_options_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_appbar_display_user_response        (GnomeAppBar     *gnomeappbar,
                                        gpointer         user_data);

void 
on_button_open_enter                   (GtkContainer    *container,
                                        GtkDirectionType direction,
                                        gpointer         user_data);
void
on_button_save_enter                   (GtkContainer    *container,
                                        GtkDirectionType direction,
                                        gpointer         user_data);

void
on_button_print_enter                   (GtkContainer    *container,
                                        GtkDirectionType direction,
                                        gpointer         user_data);

void
on_button_execute_enter                (GtkContainer    *container,
                                        GtkDirectionType direction,
                                        gpointer         user_data);

void
on_button_stop_enter                   (GtkContainer    *container,
                                        GtkDirectionType direction,
                                        gpointer         user_data);

void
on_button_stop_press                   (GtkWidget      *widget, 
                                         gpointer       data);

void
on_button_stop_release                 (GtkWidget      *widget, 
                                         gpointer       data);

void
on_button_close_enter                   (GtkContainer    *container,
                                        GtkDirectionType direction,
                                        gpointer         user_data);

void
on_button_exit_enter                   (GtkContainer    *container,
                                        GtkDirectionType direction,
                                        gpointer         user_data);


/*
 * process toolbar callbacks
 */
#ifdef ENABLE_CAM
void
on_toolbar_checkbutton_cam (GtkWidget *widget, 
			   gpointer data);
#endif /* ENABLE_CAM */

#ifdef ENABLE_TRIG
void
on_toolbar_checkbutton_trig (GtkWidget *widget, 
			   gpointer data);
#endif /* ENABLE_TRIG */

#ifdef ENABLE_IMGPROC
void
on_toolbar_checkbutton_imgproc (GtkWidget *widget, 
                                gpointer data);
#endif /* ENABLE_IMGPROC */
void
on_toolbar_checkbutton_piv (GtkWidget *widget, 
                            gpointer data);

void
on_toolbar_checkbutton_gradient (GtkWidget *widget, 
                                 gpointer data);

void
on_toolbar_checkbutton_resstats (GtkWidget *widget, 
                                 gpointer data);

void 
on_toolbar_checkbutton_errvec (GtkWidget *widget, 
                               gpointer data);

void
on_toolbar_checkbutton_peaklck (GtkWidget *widget, 
				gpointer data);

void
on_toolbar_checkbutton_scale (GtkWidget *widget, 
                              gpointer data);

void
on_toolbar_checkbutton_average (GtkWidget *widget, 
				gpointer data);

void
on_toolbar_checkbutton_subavg (GtkWidget *widget, 
                               gpointer data);

void 
on_toolbar_checkbutton_vorstra (GtkWidget *widget, 
                                gpointer data);


/* void */
/* on_button_quit_no_clicked             (GtkButton       *button, */
/*                                         gpointer         user_data); */

/* void */
/* on_button_quit_gpiv_yes_clicked             (GtkButton       *button, */
/*                                         gpointer         user_data); */

/* void */
/* on_button_message_clicked             (GtkButton       *button, */
/*                                         gpointer         user_data); */

/* void */
/* on_button_error_clicked             (GtkButton       *button, */
/*                                         gpointer         user_data); */

void
on_button_message_clicked (GtkDialog *dialog,
                           gint response,
                           gpointer data
                           );

void
on_notebook_switch_page (GtkNotebook *notebook,
                         GtkNotebookPage *page,
                         gint page_num,
                         gpointer user_data);


#endif  /* CONSOLE_H */
