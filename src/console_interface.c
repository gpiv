/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8 c-style: "K&R" -*- */


/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: console_interface.c,v $
 * Revision 1.20  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.19  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.18  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.17  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.16  2007-02-16 17:09:48  gerber
 * added Gauss weighting on I.A. and SPOF filtering (on correlation function)
 *
 * Revision 1.15  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.14  2006-09-18 07:27:05  gerber
 * *** empty log message ***
 *
 * Revision 1.13  2006/01/31 14:28:11  gerber
 * version 0.3.0
 *
 * Revision 1.11  2005/03/05 17:38:51  gerber
 * repaired documantation and About
 *
 * Revision 1.10  2005/02/12 14:12:12  gerber
 * Changed tabular names and titles
 *
 * Revision 1.9  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.8  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.7  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 * Revision 1.6  2003/09/01 11:17:14  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.5  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.4  2003/07/20 18:16:37  gerber
 * improved about text
 *
 * Revision 1.3  2003/07/05 13:14:57  gerber
 * drag and drop of a _series_ of filenames from NAUTILUS
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include "support.h"

#include "gpiv_gui.h"
#include "utils.h"
#include "console_interface.h"
#include "console_menus.h"
#ifdef ENABLE_CAM
#include "dac_cam.h"
#endif /* ENABLE_CAM */
#ifdef ENABLE_TRIG
#include "dac_trig.h"
#endif /* ENABLE_TRIG */
#ifdef ENABLE_IMGPROC
#include "imgproc.h"
#endif /* ENABLE_IMGPROC */

#include "piveval.h"
#include "pivvalid.h"
#include "pivpost.h"




GtkWidget *
create_menu_gpiv_popup(GpivConsole *gpiv
                       )
/*-----------------------------------------------------------------------------
 */
{
        GnomeApp *console = gpiv->console;
        GtkWidget *menu_gpiv_popup = NULL;

        menu_gpiv_popup = gtk_menu_new();
        gtk_object_set_data (GTK_OBJECT (console),
                             "menu_gpiv_popup",
                             menu_gpiv_popup);
        gnome_app_fill_menu (GTK_MENU_SHELL (menu_gpiv_popup),
                             menubar_gpiv_popup,
                             NULL, 
                             FALSE,
                             0);
        
        gtk_widget_ref (menubar_gpiv_popup[0].widget);
        gtk_object_set_data_full (GTK_OBJECT (console),
                                  "file",
                                  menubar_gpiv_popup[0].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        
        
        gtk_widget_ref (menubar_gpiv_popup[1].widget);
        gtk_object_set_data_full (GTK_OBJECT (console),
                                  "settings",
                                  menubar_gpiv_popup[1].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        
        
        gtk_widget_ref (menubar_gpiv_popup[2].widget);
        gtk_object_set_data_full (GTK_OBJECT (console),
                                  "help",
                                  menubar_gpiv_popup[2].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        
        gtk_widget_ref (help_menu_gpiv_popup[0].widget);
        gtk_object_set_data_full (GTK_OBJECT (console),
                                  "tooltip",
                                  help_menu_gpiv_popup[0].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (help_menu_gpiv_popup[0].widget),
                             "gpiv",
                        gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->console),
                             "help_menu_gpiv_popup0",
                             help_menu_gpiv_popup[0].widget);

        
        
        gtk_widget_ref (file_menu_gpiv_popup[0].widget);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv_popup[0].widget),
                             "gpiv",
                             gpiv);

        

        gtk_widget_ref (file_menu_gpiv_popup[1].widget);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv_popup[1].widget),
                             "gpiv",
                             gpiv);
        
        
        
        gtk_widget_ref (file_menu_gpiv_popup[2].widget);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv_popup[2].widget),
                             "gpiv",
                             gpiv);
        
        
        
        gtk_widget_ref (file_menu_gpiv_popup[3].widget);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv_popup[3].widget),
                             "gpiv",
                             gpiv);
        
        
        
        gtk_widget_ref (file_menu_gpiv_popup[4].widget);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv_popup[4].widget),
                             "gpiv",
                             gpiv);
        
        

        gtk_widget_ref (file_menu_gpiv_popup[5].widget);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv_popup[5].widget),
                             "gpiv",
                             gpiv);
        
        
        
        gtk_widget_ref (settings_menu_gpiv_popup[0].widget);
        gtk_object_set_data_full (GTK_OBJECT (console),
                                  "gpiv_buttons",
                                  settings_menu_gpiv_popup[0].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (settings_menu_gpiv_popup[0].widget),
                             "gpiv",
                             gpiv);
        if (gpiv_par->console__view_gpivbuttons) {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (settings_menu_gpiv_popup[0].
                                                 widget), 
                                                TRUE);
        } else {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (settings_menu_gpiv_popup[0].
                                                 widget),
                                                FALSE);
        }
        
        
        
        gtk_widget_ref (settings_menu_gpiv_popup[1].widget);
        gtk_object_set_data_full (GTK_OBJECT (console),
                                  "tabulator",
                                  settings_menu_gpiv_popup[1].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (settings_menu_gpiv_popup[1].widget),
                             "gpiv",
                             gpiv);
        if (gpiv_par->console__view_tabulator) {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (settings_menu_gpiv_popup[1].
                                                 widget), 
                                                TRUE);
        } else {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (settings_menu_gpiv_popup[1].
                                                 widget),
                                                FALSE);
        }
        
/*
 * settings_menu_gpiv_popup[2] represents NOMEUIINFO_SEPARATOR
 */

        gtk_widget_ref(settings_menu_gpiv_popup[3].widget);
        gtk_object_set_data_full(GTK_OBJECT(console),
                                 "preferences",
                                 settings_menu_gpiv_popup[3].widget,
                                 (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data(GTK_OBJECT(settings_menu_gpiv_popup[3].widget),
                            "gpiv",
                            gpiv);




        if (gpiv_par->console__show_tooltips) {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (help_menu_gpiv_popup[0].
                                                 widget), 
                                                TRUE);
        } else {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (help_menu_gpiv_popup[0].
                                                 widget),
                                                FALSE);
        }
        
        
        
        return menu_gpiv_popup;
}



GpivConsole *
create_gpiv (void
            )
/*-----------------------------------------------------------------------------
 */
{
        GpivConsole *gpiv = g_new0 (GpivConsole, 1);
	GdkPixbuf *logo = NULL;
        gint i;

        gpiv->tooltips = gtk_tooltips_new ();

/*
 * Gnome2: from book Official dev.Guide
 */
 	logo = gdk_pixbuf_new_from_file(PIXMAPSDIR "gpiv_logo.png", NULL);

        gpiv->console = g_object_new(GNOME_TYPE_APP,
                                     "title", "gpiv console",
                                     "app-id", "gpiv",
                                     "default-width",  CONSOLE_WIDTH,
                                     "default-height", CONSOLE_HEIGHT,
                                     "icon", logo,
                                     NULL);
        if (logo != NULL) g_object_unref (logo);
        
        
        
        gtk_object_set_data (GTK_OBJECT (gpiv->console),
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->console), 
                          "delete_event" ,
                          G_CALLBACK (delete_console),
                          NULL);
        
        

        gpiv->dock1 = GNOME_APP (gpiv->console)->dock;
        gtk_widget_ref (gpiv->dock1);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "dock1",
                                  gpiv->dock1,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->dock1);
        
        
        
/*
 * Menu bar with gnome_app_create_menus
 */

        gnome_app_create_menus (GNOME_APP (gpiv->console),
                                menubar_gpiv);
        
        
        gtk_widget_ref (menubar_gpiv[0].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "file",
                                  menubar_gpiv[0].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        
        
        gtk_widget_ref (file_menu_gpiv[0].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "open",
                                  file_menu_gpiv[0].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv[0].widget),
                             "gpiv",
                             gpiv);
        
        
        gtk_widget_ref (file_menu_gpiv[1].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "save",
                                  file_menu_gpiv[1].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv[1].widget),
                             "gpiv",
                             gpiv);
        
        gtk_widget_ref (file_menu_gpiv[2].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "save_as",
                                  file_menu_gpiv[2].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv[2].widget),
                             "gpiv",
                             gpiv);

        
        gtk_widget_ref (file_menu_gpiv[3].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "print",
                                  file_menu_gpiv[3].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv[3].widget),
                             "gpiv",
                             gpiv);
        
        
        gtk_widget_ref (file_menu_gpiv[4].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "close",
                                  file_menu_gpiv[4].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (file_menu_gpiv[4].widget),
                             "gpiv",
                             gpiv);
        
        
        gtk_widget_ref (file_menu_gpiv[5].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "separator",
                                  file_menu_gpiv[5].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        
        
        gtk_widget_ref (file_menu_gpiv[6].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "exit",
                                  file_menu_gpiv[6].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        
        
        gtk_widget_ref (menubar_gpiv[1].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "settings",
                                  menubar_gpiv[1].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        
        
        gtk_widget_ref (settings_menu_gpiv[0].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "gpiv_buttons",
                                  settings_menu_gpiv[0].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (settings_menu_gpiv[0].widget),
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->console),
                             "settings_menu_gpiv0",
                             settings_menu_gpiv[0].widget);
        
        
        gtk_widget_ref (settings_menu_gpiv[1].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "tabulator",
                                  settings_menu_gpiv[1].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (settings_menu_gpiv[1].widget),
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->console),
                             "settings_menu_gpiv1",
                             settings_menu_gpiv[1].widget);
        
        
        gtk_widget_ref (settings_menu_gpiv[3].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "preferences",
                                  settings_menu_gpiv[3].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (settings_menu_gpiv[3].widget),
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->console),
                             "settings_menu_gpiv3",
                             settings_menu_gpiv[3].widget);
        
        
        gtk_widget_ref (menubar_gpiv[2].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "help",
                                  menubar_gpiv[2].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        
        
        gtk_widget_ref (help_menu_gpiv[0].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "tooltip",
                                  help_menu_gpiv[0].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (help_menu_gpiv[0].widget),
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->console),
                             "help_menu_gpiv0",
                             help_menu_gpiv[0].widget);
        
        
        gtk_widget_ref (help_menu_gpiv[1].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "manual",
                                  help_menu_gpiv[1].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (help_menu_gpiv[1].widget),
                             "gpiv",
                             gpiv);
        
        
        gtk_widget_ref (help_menu_gpiv[2].widget);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "about",
                                  help_menu_gpiv[2].widget,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_object_set_data (GTK_OBJECT (help_menu_gpiv[2].widget),
                             "gpiv",
                             gpiv);
        
        
 
/*
 * toolbar
 */
/*    gnome_app_create_toolbar (GNOME_APP (gpiv->console),
                           toolbar_gpiv);
*/
        gpiv->toolbar1 = gtk_toolbar_new ();
        gtk_widget_show (gpiv->toolbar1);
        gnome_app_add_toolbar (GNOME_APP (gpiv->console), 
                               GTK_TOOLBAR (gpiv->toolbar1), 
                               "gpiv->toolbar1",
                               BONOBO_DOCK_ITEM_BEH_EXCLUSIVE,
                               BONOBO_DOCK_TOP, 
                               1, 
                               0, 
                               0);
        gtk_toolbar_set_style (GTK_TOOLBAR (gpiv->toolbar1), 
                               GTK_TOOLBAR_BOTH);
        gpiv->tmp_toolbar_icon_size = gtk_toolbar_get_icon_size (GTK_TOOLBAR 
                                                                 (gpiv->
                                                                  toolbar1));
        
        

        gpiv->button_open = 
                (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-open");
        gtk_widget_show (gpiv->button_open);
        gtk_container_add (GTK_CONTAINER (gpiv->toolbar1), 
                           gpiv->button_open);
        gtk_tool_item_set_tooltip (GTK_TOOL_ITEM (gpiv->button_open), 
                                   gpiv->tooltips, 
                                   _("Open a PIV image or data-file"), NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_open), 
                             "gpiv",
                             gpiv);
        g_signal_connect ((gpointer) gpiv->button_open, 
                          "clicked",
                          G_CALLBACK (on_button_open_clicked),
                          NULL);
        
        
        
        
        gpiv->button_save = 
                (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-save");
        gtk_widget_show (gpiv->button_save);
        gtk_container_add (GTK_CONTAINER (gpiv->toolbar1), 
                           gpiv->button_save);
        gtk_tool_item_set_tooltip (GTK_TOOL_ITEM (gpiv->button_save), 
                                   gpiv->tooltips, 
                                   _("Save data"), NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_save),
                             "gpiv",
                        gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_save),
                          "clicked",
                          G_CALLBACK (on_save_activate), 
                          NULL);
        
        
        
        
/*         gpiv->button_print =  */
/*                 (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-print"); */
/*         gtk_widget_show (gpiv->button_print); */
/*         gtk_container_add (GTK_CONTAINER (gpiv->toolbar1),  */
/*                            gpiv->button_print); */
/*         gtk_tool_item_set_tooltip (GTK_TOOL_ITEM (gpiv->button_print),  */
/*                                    gpiv->tooltips,  */
/*                                    _("Print the displayed data of active buffer(s)"), */
/*                                    NULL); */
        
/*         gtk_object_set_data (GTK_OBJECT (gpiv->button_print), */
/*                              "gpiv", */
/*                              gpiv); */
/*         g_signal_connect (GTK_OBJECT (gpiv->button_print), */
/*                           "clicked", */
/*                           G_CALLBACK (on_print_activate),  */
/*                           NULL); */
/*         gtk_widget_hide (GTK_WIDGET (gpiv->button_print)); */
        
        
        
        
        gpiv->button_execute = 
                (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-execute");
        gtk_widget_show (gpiv->button_execute);
        gtk_container_add (GTK_CONTAINER (gpiv->toolbar1), 
                           gpiv->button_execute);
        gtk_tool_item_set_tooltip (GTK_TOOL_ITEM (gpiv->button_execute), 
                                   gpiv->tooltips, 
                                   _("Execute the enabled chain process(es)"),
                                   NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_execute),
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_execute),
                          "clicked",
                          G_CALLBACK (on_execute_activate), 
                          NULL);
        
        
        
        
        gpiv->button_stop = 
                (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-stop");
        gtk_widget_show (gpiv->button_stop);
        gtk_container_add (GTK_CONTAINER (gpiv->toolbar1), 
                           gpiv->button_stop);
        gtk_tool_item_set_tooltip (GTK_TOOL_ITEM (gpiv->button_stop), 
                                   gpiv->tooltips, 
                                   _("Cancels all running processes"), NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_stop),
                             "gpiv",
                             gpiv);
        g_signal_connect ((gpointer) gpiv->button_stop, "clicked",
                          G_CALLBACK (on_button_stop_press),
                          NULL); 
        g_signal_connect (GTK_OBJECT (gpiv->button_stop), 
                          "button_press_event",
                          G_CALLBACK (on_button_stop_press), 
                          NULL);    
        g_signal_connect (GTK_OBJECT (gpiv->button_stop), 
                          "button_release_event",
                          G_CALLBACK (on_button_stop_release),
                          NULL);
        
        
        
        
        gpiv->button_close = (GtkWidget*) gtk_tool_button_new_from_stock 
                ("gtk-close");
        gtk_widget_show (gpiv->button_close);
        gtk_container_add (GTK_CONTAINER (gpiv->toolbar1), 
                           gpiv->button_close);
        gtk_tool_item_set_tooltip (GTK_TOOL_ITEM (gpiv->button_close), 
                                   gpiv->tooltips, 
                                   _("Close active buffer(s).\nA warning message will be issued for unsaved data"), 
                                   NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_close),
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_close),
                          "clicked",
		       G_CALLBACK (on_close_activate),
                          NULL);
        
        


/*     gpiv->vseparator = gtk_vseparator_new (); */
/*     gtk_widget_show (gpiv->vseparator); */
/*     gtk_container_add (GTK_CONTAINER (gpiv->toolbar1), gpiv->vseparator); */

/*     gnome_app_add_docked (GNOME_APP (gpiv->console),  */
/*                           GTK_CONTAINER (gpiv->toolbar1),  */
/*                           gpiv->vseparator,  */
/*                           "vseparator", */
/*                           BONOBO_DOCK_ITEM_BEH_NORMAL, */
/*                           BONOBO_DOCK_TOP, 2, 0, 0); */
    



        gpiv->button_exit = 
                (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-quit");
        gtk_widget_show (gpiv->button_exit);
        gtk_container_add (GTK_CONTAINER (gpiv->toolbar1), 
                           gpiv->button_exit);
        gtk_tool_item_set_tooltip (GTK_TOOL_ITEM (gpiv->button_exit), 
                                   gpiv->tooltips, 
                                   _("Exit GPIV.\nA warning message will be issued for unsaved data"), 
                                   NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_exit),
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_exit), 
                          "clicked",
                          G_CALLBACK (on_exit_activate), 
                          NULL);    
        
/*
 * vbox_main contains handleboxes for toolbar, gpiv toolbar, tabulator 
 * and bufferlist
 */
        gpiv->vbox_main = gtk_vbox_new (FALSE,
                                        0);
        gtk_widget_ref (gpiv->vbox_main);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "vbox_main",
                                  gpiv->vbox_main,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->vbox_main);
        gnome_app_set_contents (GNOME_APP (gpiv->console), 
                                gpiv->vbox_main);
        


/*
 * handlebox and toolbar for gpiv chain processes buttons
 */
        gpiv->handlebox1 = gtk_handle_box_new ();
        gtk_widget_ref (gpiv->handlebox1);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "handlebox1",
                                  gpiv->handlebox1,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_box_pack_start (GTK_BOX (gpiv->vbox_main),
                            gpiv->handlebox1,
                            FALSE, 
                            TRUE, 
                            0);


/*
 * Scrolled window
 */
        gpiv->scrolledwindow_handbox1 = gtk_scrolled_window_new (NULL,
                                                                 NULL);
        gtk_widget_ref (gpiv->scrolledwindow_handbox1);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "scrolledwindow_handbox1",
                                  gpiv->scrolledwindow_handbox1,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->scrolledwindow_handbox1);
        gtk_container_add (GTK_CONTAINER (gpiv->handlebox1),
                           gpiv->scrolledwindow_handbox1);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
                                  (gpiv->scrolledwindow_handbox1),
                                        GTK_POLICY_AUTOMATIC, 
                                        GTK_POLICY_NEVER);
        
        
        gpiv->viewport_handbox1 =
                gtk_viewport_new (NULL,
                                  NULL);
        gtk_widget_ref (gpiv->viewport_handbox1);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                             "viewport_handbox1",
                                  gpiv->viewport_handbox1,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->viewport_handbox1);
        gtk_container_add (GTK_CONTAINER (gpiv->scrolledwindow_handbox1), 
                           gpiv->viewport_handbox1);
        gtk_widget_set_size_request (GTK_WIDGET (gpiv->viewport_handbox1),
                                     410, 
                                     50);
        

/*
 * toolbar with buttons for enabling processes
 */

        gpiv->toolbar2 =
                gtk_toolbar_new ();
        gtk_widget_ref (gpiv->toolbar2);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "gpiv->toolbar2",
                                  gpiv->toolbar2,
                                  (GtkDestroyNotify) gtk_widget_unref);
/*
 * BUGFIX: Gnome2: disable?
 */
/*     gtk_toolbar_set_space_style (GTK_TOOLBAR (gpiv->toolbar2), */
/*                                  GTK_TOOLBAR_SPACE_LINE); */
        gtk_widget_show (gpiv->toolbar2);
        gtk_container_add (GTK_CONTAINER (gpiv->viewport_handbox1),
                           gpiv->toolbar2);
        
        
        
        gpiv->hbox_toolbar2 = gtk_hbox_new (FALSE,
                                            0);
        gtk_widget_ref (gpiv->vbox_main);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "hbox_toolbar2", 
                                  gpiv->hbox_toolbar2,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->hbox_toolbar2);
        gtk_container_add (GTK_CONTAINER (gpiv->toolbar2),
                           gpiv->hbox_toolbar2);    



#ifdef ENABLE_CAM
/*
 * camera process button
 */
    gpiv->button_toolbar_cam = gtk_check_button_new_with_label(_("camera"));
    gtk_widget_ref(gpiv->button_toolbar_cam);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
			     "button_toolbar_cam",
                             gpiv->button_toolbar_cam,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(gpiv->button_toolbar_cam);
    gtk_box_pack_start(GTK_BOX(gpiv->hbox_toolbar2),
                       gpiv->button_toolbar_cam,
                       FALSE,
		       FALSE,
                       0);
    gtk_tooltips_set_tip(gpiv->tooltips,
                         gpiv->button_toolbar_cam,
 			 _("Enables camera for image recording within the chain process. \n\
The process will be executed by clicking the Execute button"),
                         NULL);

    gtk_object_set_data(GTK_OBJECT(gpiv->button_toolbar_cam), 
			"gpiv",
                        gpiv);
    gtk_signal_connect(GTK_OBJECT(gpiv->button_toolbar_cam), 
                       "enter",
		       G_CALLBACK (on_button_dac_camstart_enter),
                       NULL);
    gtk_signal_connect(GTK_OBJECT(gpiv->button_toolbar_cam), 
                       "leave",
		       G_CALLBACK (on_widget_leave),
                       NULL);
    gtk_signal_connect(GTK_OBJECT(gpiv->button_toolbar_cam), 
                       "clicked",
		       G_CALLBACK (on_toolbar_checkbutton_cam),
                       NULL);
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_cam),
                                gpiv_par->process__cam);

#endif /* ENABLE_CAM */


#ifdef ENABLE_TRIG
/*
 * trigger process button
 */
    gpiv->button_toolbar_trig = gtk_check_button_new_with_label(_("trigger"));
    gtk_widget_ref(gpiv->button_toolbar_trig);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
			     "button_toolbar_trig",
                             gpiv->button_toolbar_trig,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(gpiv->button_toolbar_trig);
    gtk_box_pack_start(GTK_BOX(gpiv->hbox_toolbar2),
                       gpiv->button_toolbar_trig,
                       FALSE,
		       FALSE,
                       0);
    gtk_tooltips_set_tip(gpiv->tooltips,
                         gpiv->button_toolbar_trig,
 			 _("Enables (RTAI) camera and laser triggering within the chain process. \n\
The process will be executed by clicking the Execute button"),
                         NULL);

    gtk_object_set_data(GTK_OBJECT(gpiv->button_toolbar_trig), 
			"gpiv",
                        gpiv);
    gtk_signal_connect(GTK_OBJECT(gpiv->button_toolbar_trig), 
                       "enter",
		       G_CALLBACK (on_button_dac_triggerstart_enter),
                       NULL);
    gtk_signal_connect(GTK_OBJECT(gpiv->button_toolbar_trig), 
                       "leave",
		       G_CALLBACK (on_widget_leave),
                       NULL);
    gtk_signal_connect(GTK_OBJECT(gpiv->button_toolbar_trig), 
                       "clicked",
		       G_CALLBACK (on_toolbar_checkbutton_trig),
                       NULL);
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_trig),
                                gpiv_par->process__trig);

#endif /* ENABLE_TRIG */


#ifdef ENABLE_IMGPROC
/*
 * Image processing button
 */
    gpiv->button_toolbar_imgproc = gtk_check_button_new_with_label(_("image"));
    gtk_widget_ref(gpiv->button_toolbar_imgproc);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
			     "button_toolbar_imgproc",
                             gpiv->button_toolbar_imgproc,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(gpiv->button_toolbar_imgproc);
    gtk_box_pack_start(GTK_BOX(gpiv->hbox_toolbar2),
                       gpiv->button_toolbar_imgproc,
                       FALSE,
		       FALSE,
                       0);
    gtk_tooltips_set_tip(gpiv->tooltips,
                         gpiv->button_toolbar_imgproc,
 			 _("Enables image processing within the chain process. \n\
The process will be executed by clicking the Execute button"),
                         NULL);

    gtk_object_set_data(GTK_OBJECT(gpiv->button_toolbar_imgproc), 
			"gpiv",
                        gpiv);
    g_signal_connect(GTK_OBJECT(gpiv->button_toolbar_imgproc), 
                       "enter",
		       G_CALLBACK (on_button_imgproc_enter),
                       NULL);
    g_signal_connect(GTK_OBJECT(gpiv->button_toolbar_imgproc), 
                       "leave",
		       G_CALLBACK (on_widget_leave),
                       NULL);
    g_signal_connect(GTK_OBJECT(gpiv->button_toolbar_imgproc), 
                       "clicked",
		       G_CALLBACK (on_toolbar_checkbutton_imgproc),
                       NULL);
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_imgproc),
                                gpiv_par->process__imgproc);

#endif /* ENABLE_IMGPROC */
/*
 * piv process button
 */
        gpiv->button_toolbar_piv = gtk_check_button_new_with_label ( _("piv"));
        gtk_widget_ref (gpiv->button_toolbar_piv);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "button_toolbar_piv",
                                  gpiv->button_toolbar_piv,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_piv);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                            gpiv->button_toolbar_piv,
                            FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_piv,
                              _("Enables PIV image interrogation for chain processing: analysing of a PIV image \
 (pair), resulting into the mean displacements of the particle images within \
each interrogation area. \n\
The process will be executed by clicking the Execute button"),
                              NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_piv), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_piv), 
                          "enter",
                          G_CALLBACK (on_button_piv_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_piv), 
                          "leave",
                          G_CALLBACK (on_widget_leave),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_piv), 
                          "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_piv),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_piv),
                                     gpiv_par->process__piv);
        
        
        
        gpiv->button_toolbar_gradient =
                gtk_check_button_new_with_label ( _("gradient"));
        gtk_widget_ref (gpiv->button_toolbar_gradient);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "button_toolbar_gradient",
                                  gpiv->button_toolbar_gradient,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_gradient);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                            gpiv->button_toolbar_gradient,
                            FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_gradient, 
                              _("Enables gradient for chain processing: Disables velocities with gradients larger than \
0.05 over the interrogation area. \
\nThe process will be executed by clicking the \"Execute\" button"),
                              NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_gradient), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_gradient), 
                          "enter",
                          G_CALLBACK (on_button_valid_gradient_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_gradient), 
                          "leave",
                          G_CALLBACK (on_widget_leave),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_gradient), 
                          "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_gradient),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_gradient),
                                     gpiv_par->process__gradient);
        

        
        gpiv->button_toolbar_resstats = gtk_check_button_new_with_label ( _("residu"));
        gtk_widget_ref (gpiv->button_toolbar_resstats);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "button_toolbar_resstats",
                                  gpiv->button_toolbar_resstats,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_resstats);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                            gpiv->button_toolbar_resstats,
                            FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_resstats,
                              _("Enables residu statistics for chain processing: calculates \
the residus of displacements for detection of outliers and shows an histogram \
of them. The histogram will be displayed in the Piv Validation tab. \n\
The process will be executed by clicking the Execute button"),
                              NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_resstats), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_resstats), 
                          "enter",
                          G_CALLBACK (on_button_valid_errvec_resstats_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_resstats), 
                          "leave",
                          G_CALLBACK (on_widget_leave),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_resstats), 
                          "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_resstats),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_resstats),
                                     gpiv_par->process__resstats);
       
        
        
        gpiv->button_toolbar_errvec = gtk_check_button_new_with_label
                ( _("validate"));
        gtk_widget_ref (gpiv->button_toolbar_errvec);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "button_toolbar_errvec",
			     gpiv->button_toolbar_errvec,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_errvec);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                            gpiv->button_toolbar_errvec,
                            FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_errvec,
                              _("Enables validation for chain processing: detects \
outliers of PIV data by testing on median residu or by Signal to Noise \
Ratio magnitudes and, eventually, substitutes. \n\
The process will be executed by clicking the Execute button"),
                              NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_errvec), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_errvec), 
                          "enter",
                          G_CALLBACK (on_button_valid_errvec_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_errvec), 
                          "leave",
                          G_CALLBACK (on_widget_leave), NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_errvec), 
                       "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_errvec),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_errvec),
                                     gpiv_par->process__errvec);
       
        
        
        gpiv->button_toolbar_peaklock =
                gtk_check_button_new_with_label ( _("sub-pixel"));
        gtk_widget_ref (gpiv->button_toolbar_peaklock);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "button_toolbar_peaklock",
                                  gpiv->button_toolbar_peaklock,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_peaklock);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                            gpiv->button_toolbar_peaklock,
                            FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_peaklock, 
                              _("Enables peak-lock for chain processing: shows an histogram \
of sub-pixel displacements to check on peak-locking effects. \
 The histogram will be displayed in the Piv Validation tab. \n\
The process will be executed by clicking the \"Execute\" button"),
                              NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_peaklock), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_peaklock), 
                          "enter",
                          G_CALLBACK (on_button_valid_peaklck_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_peaklock), 
                          "leave",
                          G_CALLBACK (on_widget_leave),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_peaklock),
                          "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_peaklck),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_peaklock),
                                     gpiv_par->process__peaklock);
       
        
        gpiv->button_toolbar_scale =
                gtk_check_button_new_with_label ( _("scale"));
        gtk_widget_ref (gpiv->button_toolbar_scale);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "button_toolbar_scale",
                                  gpiv->button_toolbar_scale,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_scale);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                            gpiv->button_toolbar_scale, FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_scale, 
                              _("Enables scaling for chain processing: scales spatial displacements \
over all data. \
\nThe process will be executed by clicking the \"Execute\" button"),
                              NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_scale), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_scale), 
                          "enter",
                          G_CALLBACK (on_button_post_scale_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_scale), 
                          "leave",
                          G_CALLBACK (on_widget_leave),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_scale), 
                          "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_scale),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_scale),
                                     gpiv_par->process__scale);
       

        gpiv->button_toolbar_average =
                gtk_check_button_new_with_label ( _("average"));
        gtk_widget_ref (gpiv->button_toolbar_average);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "button_toolbar_average",
                                  gpiv->button_toolbar_average,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_average);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                            gpiv->button_toolbar_average, FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_average, 
                              _("Enables average for chain processing: Calculates spatial average displacements \
over all data. \
\nThe process will be executed by clicking the \"Execute\" button"),
                              NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_average), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_average), 
                          "enter",
                          G_CALLBACK (on_button_post_savg_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_average), 
                          "leave",
                          G_CALLBACK (on_widget_leave),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_average), 
                          "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_average),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_average),
                                     gpiv_par->process__average);
        

        gpiv->button_toolbar_subavg =
                gtk_check_button_new_with_label ( _("subtract"));
        gtk_widget_ref (gpiv->button_toolbar_subavg);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "button_toolbar_subavg",
                                  gpiv->button_toolbar_subavg,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_subavg);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                       gpiv->button_toolbar_subavg,
                            FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_subavg, 
                              _("Enables subtract for chain processing: subtracts spatial average displacements \
from all data. \
\nThe process will be executed by clicking the \"Execute\" button"),
                              NULL);

        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_subavg), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_subavg), 
                          "enter",
                          G_CALLBACK (on_button_post_subavg_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_subavg), 
                          "leave",
                          G_CALLBACK (on_widget_leave),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_subavg), 
                       "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_subavg),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_subavg),
                                     gpiv_par->process__subtract);
        
        
        gpiv->button_toolbar_vorstra =
                gtk_check_button_new_with_label ( _("vorticity"));
        gtk_widget_ref (gpiv->button_toolbar_vorstra);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "button_toolbar_vorstra",
                                  gpiv->button_toolbar_vorstra,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->button_toolbar_vorstra);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_toolbar2),
                            gpiv->button_toolbar_vorstra,
                            FALSE,
                            FALSE,
                            0);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->button_toolbar_vorstra, 
                              _("Enables vorticity for chain processing: calculates \
 vorticity or strain magnitudes from a velocity field.\n\
The process will be executed by clicking the Execute button"),
                              NULL);
        
        gtk_object_set_data (GTK_OBJECT (gpiv->button_toolbar_vorstra), 
                             "gpiv",
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_vorstra), 
                          "enter",
                          G_CALLBACK (on_button_post_vorstra_enter),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_vorstra),
                          "leave",
                          G_CALLBACK (on_widget_leave),
                          NULL);
        g_signal_connect (GTK_OBJECT (gpiv->button_toolbar_vorstra), 
                          "clicked",
                          G_CALLBACK (on_toolbar_checkbutton_vorstra),
                          NULL);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->button_toolbar_vorstra),
                                     gpiv_par->process__vorstra);
        
        
        
/* 
 * gpiv->hbox_main contains a handlebox with buffer list and tabulator
 */
        gpiv->hbox_main = gtk_hbox_new (FALSE,
                                        0);
        gtk_widget_ref (gpiv->hbox_main);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "hbox_main",
                                  gpiv->hbox_main,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->hbox_main);
        gtk_box_pack_start (GTK_BOX (gpiv->vbox_main),
                            gpiv->hbox_main,
                            TRUE,
                            TRUE,
                            0);
        

/*
 * Notebook (tabulator)
 */
        gpiv->notebook = gtk_notebook_new ();
        gtk_widget_ref (gpiv->notebook);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "notebook",
                                  gpiv->notebook,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->notebook);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_main),
                            gpiv->notebook,
                            TRUE,
                            TRUE,
                            0);
        
        
#ifdef ENABLE_DAC
/*
 * Tabulator for Data Acquisition
 */
        gpiv->dac = create_dac (GNOME_APP(gpiv->console),
                                  gpiv->notebook);
        
        gpiv->tablabel_dac = gtk_label_new ( _("Record"));
        gtk_widget_ref (gpiv->tablabel_dac);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "tablabel_dac",
                                  gpiv->tablabel_dac,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->tablabel_dac);
        gtk_notebook_set_tab_label 
                (GTK_NOTEBOOK (gpiv->notebook),
                 GTK_WIDGET (gtk_notebook_get_nth_page (GTK_NOTEBOOK
                                                        (gpiv->notebook), 
                                                        PAGE_DAC)),
                 GTK_WIDGET(gpiv->tablabel_dac));
#endif /* ENABLE_DAC */        

/*
 * Tabulator for Image header
 */
        gpiv->imgh = create_imgh (GNOME_APP(gpiv->console),
                                  gpiv->notebook);
        
        gpiv->tablabel_imgh = gtk_label_new ( _("Image"));
        gtk_widget_ref (gpiv->tablabel_imgh);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "tablabel_imgh",
                                  gpiv->tablabel_imgh,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->tablabel_imgh);
        gtk_notebook_set_tab_label 
                (GTK_NOTEBOOK (gpiv->notebook),
                 GTK_WIDGET (gtk_notebook_get_nth_page (GTK_NOTEBOOK
                                                        (gpiv->notebook), 
                                                        PAGE_IMGH)),
                 GTK_WIDGET (gpiv->tablabel_imgh));
        
        
        
        gtk_object_set_data (GTK_OBJECT (gpiv->imgh->radiobutton_mouse_1), 
                             "gpiv",
                             gpiv);                


#ifdef ENABLE_IMGPROC
/*
 * Tabulator for Image processing
 */
        gpiv->imgproc = create_imgproc (GNOME_APP(gpiv->console),
                                  gpiv->notebook);
        
        gpiv->tablabel_imgproc = gtk_label_new ( _("Process"));
        gtk_widget_ref (gpiv->tablabel_imgproc);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "tablabel_imgproc",
                                  gpiv->tablabel_imgproc,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->tablabel_imgproc);
        gtk_notebook_set_tab_label 
                (GTK_NOTEBOOK (gpiv->notebook),
                 GTK_WIDGET (gtk_notebook_get_nth_page (GTK_NOTEBOOK
                                                        (gpiv->notebook), 
                                                        PAGE_IMGPROC)),
                 GTK_WIDGET (gpiv->tablabel_imgproc));


        for (i = 0; i < IMG_FILTERS; i++) {
        gtk_object_set_data (GTK_OBJECT (gpiv->imgproc->imf_inf[i]->button_filter), 
                             "gpiv",
                             gpiv);
        }

        gtk_object_set_data (GTK_OBJECT (gpiv->imgproc->button), 
                             "gpiv",
                             gpiv);

#endif

/*
 * Tabulator for Interrogation
 */
        gpiv->piveval = create_piveval (GNOME_APP(gpiv->console),
                                        gpiv->notebook);
        gpiv->tablabel_piveval = gtk_label_new ( _("Interrogate"));
        gtk_widget_ref (gpiv->tablabel_piveval);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "tablabel_piveval",
                                  gpiv->tablabel_piveval,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->tablabel_piveval);
        gtk_notebook_set_tab_label 
                (GTK_NOTEBOOK (gpiv->notebook),
                 GTK_WIDGET (gtk_notebook_get_nth_page (GTK_NOTEBOOK
                                                        (gpiv->notebook), 
                                                        PAGE_PIVEVAL)),
                 GTK_WIDGET (gpiv->tablabel_piveval));
        
        
        
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_mouse_0), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_mouse_1), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_mouse_2), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_mouse_3), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_mouse_4), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_mouse_5), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_mouse_6), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->checkbutton_monitor), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_fit_none), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_fit_gauss), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_fit_power), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_fit_gravity), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_peak_1), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_peak_2), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_peak_3), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_weightkernel), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_zerooff), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_centraldiff), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->radiobutton_imgdeform), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->checkbutton_weight_ia),
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->checkbutton_spof),
                             "gpiv",
                             gpiv);
/*     gtk_object_set_data (GTK_OBJECT (gpiv->piveval->spinbutton_monitor_zoom),  */
/*                         "gpiv", */
/*                         gpiv); */
/*     gtk_object_set_data (GTK_OBJECT (gpiv->piveval->spinbutton_monitor_vectorscale),  */
/*                         "gpiv", */
/*                         gpiv); */
        gtk_object_set_data (GTK_OBJECT (gpiv->piveval->button), 
                             "gpiv",
                             gpiv);
        
        
/*
 * Tabulator for PIV-data validation
 */
        gpiv->pivvalid = create_pivvalid (GNOME_APP(gpiv->console),
                                          gpiv->notebook);
        
        gpiv->tablabel_pivvalid = gtk_label_new ( _("Validate"));
        gtk_widget_ref (gpiv->tablabel_pivvalid);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "tablabel_pivvalid",
                                  gpiv->tablabel_pivvalid,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->tablabel_pivvalid);
        gtk_notebook_set_tab_label 
                (GTK_NOTEBOOK (gpiv->notebook),
                 GTK_WIDGET (gtk_notebook_get_nth_page (GTK_NOTEBOOK
                                                        (gpiv->notebook), 
                                                        PAGE_PIVVALID)),
                 GTK_WIDGET (gpiv->tablabel_pivvalid));
        
        
        
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->button_gradient), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->radiobutton_disable_0), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->radiobutton_disable_1), 
                             "gpiv", 
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->radiobutton_disable_2), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->radiobutton_disable_3), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->radiobutton_disable_4), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->button_gradient), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->button_errvec_resstats), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->button_peaklck), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->button_uhisto), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivvalid->button_vhisto), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT
                             (gpiv->pivvalid->radiobutton_errvec_residu_snr), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT
                             (gpiv->pivvalid->radiobutton_errvec_residu_median), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT
                             (gpiv->pivvalid->radiobutton_errvec_residu_normmedian), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT
                             (gpiv->pivvalid->checkbutton_errvec_disres), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT
                             (gpiv->pivvalid->radiobutton_errvec_subst_0), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT
                             (gpiv->pivvalid->radiobutton_errvec_subst_1), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT
                             (gpiv->pivvalid->radiobutton_errvec_subst_2), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT
                        (gpiv->pivvalid->radiobutton_errvec_subst_3), 
                        "gpiv",
                        gpiv);
        gtk_object_set_data (GTK_OBJECT
                             (gpiv->pivvalid->button_errvec), 
                        "gpiv",
                             gpiv);

/*
 * Tabulator for PIV-data post-processing
 */

/* pivpost->checkbutton_scale */
        gpiv->pivpost = create_pivpost (GNOME_APP(gpiv->console),
                                        gpiv->notebook);
/*     gtk_object_set_data (GTK_OBJECT (gpiv->console),  */
/*                         "gpiv", */
/*                         gpiv); */

        gpiv->tablabel_pivpost = gtk_label_new ( _("Post process"));
        gtk_widget_ref (gpiv->tablabel_pivpost);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "tablabel_pivpost",
                                  gpiv->tablabel_pivpost,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->tablabel_pivpost);
        gtk_notebook_set_tab_label 
                (GTK_NOTEBOOK (gpiv->notebook),
                 GTK_WIDGET (gtk_notebook_get_nth_page (GTK_NOTEBOOK
                                                        (gpiv->notebook), 
                                                        PAGE_PIVPOST)),
                 GTK_WIDGET (gpiv->tablabel_pivpost));
        
        
        
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->button_scale), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->button_savg), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->button_subavg), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->
                                         radiobutton_vorstra_output_1), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->
                                         radiobutton_vorstra_output_2), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->
                                         radiobutton_vorstra_output_3), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->
                                         radiobutton_vorstra_diffscheme_1), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->
                                         radiobutton_vorstra_diffscheme_2), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->
                                         radiobutton_vorstra_diffscheme_3), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->
                                         radiobutton_vorstra_diffscheme_4), 
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (gpiv->pivpost->button_vorstra), 
                             "gpiv",
                             gpiv);
        
        
        g_signal_connect (GTK_OBJECT (gpiv->notebook), 
                          "switch-page",
                          G_CALLBACK (on_notebook_switch_page), 
                          NULL);
        gtk_notebook_set_page (GTK_NOTEBOOK (gpiv->notebook),
                               gpiv_var->tab_pos);

/*
 * handlebox of buffer list
 */
        gpiv->handlebox_buf = gtk_handle_box_new ();
        gtk_widget_ref (gpiv->handlebox_buf);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "handlebox_buf",
                                  gpiv->handlebox_buf,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->handlebox_buf);
        gtk_box_pack_start (GTK_BOX (gpiv->hbox_main),
                            gpiv->handlebox_buf,
                            TRUE,
                            TRUE,
                            0);
        gtk_handle_box_set_handle_position (GTK_HANDLE_BOX (gpiv->handlebox_buf),
                                            GTK_POS_TOP);
        gtk_tooltips_set_tip (gpiv->tooltips,
                              gpiv->handlebox_buf,
                              _("buffer names and numbers"), 
                              NULL);
        
        
        gpiv->alignment_buf = gtk_alignment_new (0.0,
                                                 0.0,
                                                 0.0,
                                                 1.0);
        gtk_widget_ref (gpiv->alignment_buf);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "alignment_buf",
                                  gpiv->alignment_buf,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->alignment_buf);
        gtk_container_add (GTK_CONTAINER (gpiv->handlebox_buf),
                           gpiv->alignment_buf);
        
        
        gpiv->eventbox_buf = gtk_event_box_new ();
        gtk_widget_ref (gpiv->eventbox_buf);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "gpiv->eventbox_buf",
                                  gpiv->eventbox_buf,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->eventbox_buf);
        gtk_container_add (GTK_CONTAINER (gpiv->alignment_buf),
                           gpiv->eventbox_buf);
        
        
        gpiv->scrolledwindow_buf = gtk_scrolled_window_new (NULL,
                                                            NULL);
        gtk_widget_ref (gpiv->scrolledwindow_buf);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console),
                                  "scrolledwindow_buf",
                                  gpiv->scrolledwindow_buf,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->scrolledwindow_buf);
        gtk_container_add (GTK_CONTAINER (gpiv->eventbox_buf),
                           gpiv->scrolledwindow_buf);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
                                        (gpiv->scrolledwindow_buf),
                                        /* GTK_POLICY_NEVER */
                                        GTK_POLICY_AUTOMATIC,
                                        GTK_POLICY_AUTOMATIC);
/*   gpiv->scrolledwindow_buf_adj = gtk_adjustment_new (0.0, 500.0, 100.0, 5.0, 50.0, 2500.0); */
/*   gtk_scrolled_window_set_hadjustment (GTK_SCROLLED_WINDOW (gpiv->scrolledwindow_buf), */
/* 				       GTK_ADJUSTMENT (gpiv->scrolledwindow_buf_adj)); */
/*       gnome_canvas_set_scroll_region ( GNOME_CANVAS (canvas_display ()->display_act->gpd.display),  */
/* 				      0.0, 0.0, IMAGE_WIDTH, IMAGE_HEIGHT); */


        gpiv->viewport_buf =
                gtk_viewport_new (NULL,
                                  NULL);
        gtk_widget_ref (gpiv->viewport_buf);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "viewport_buf",
                                  gpiv->viewport_buf,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->viewport_buf);
        gtk_container_add (GTK_CONTAINER (gpiv->scrolledwindow_buf), 
                           gpiv->viewport_buf);
        gtk_widget_set_size_request (gpiv->viewport_buf,
                                     120,
                                     400);
        
   
#ifdef GTK4
enum
{
        BUFFER_NUMBER,
        BUFFER_NAME,
        PIV_DATA,
        VALIDATED,
        STORED,
        N_COLUMNS
};

        gpiv->list_buf = 
                gtk_treelist_store_new (N_COLUMNS,       /* Total number of columns */
                                    G_TYPE_INT,      /* Buffer number           */
                                    G_TYPE_STRING,   /* Buffer name             */
                                    G_TYPE_BOOLEAN,  /* PIV data?    */
                                    G_TYPE_BOOLEAN,  /* Validated PIV data?    */
                                    G_TYPE_BOOLEAN,  /* Stored PIV data?    */
                                    );
#endif

        gpiv->clist_buf = gtk_clist_new (2);
        gtk_widget_ref (gpiv->clist_buf);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "clist_buf",
                                  gpiv->clist_buf,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->clist_buf);
        gtk_container_add (GTK_CONTAINER (gpiv->viewport_buf/* scrolledwindow_buf */ ), 
                           gpiv->clist_buf);
        gtk_tooltips_set_tip (gpiv->tooltips, gpiv->clist_buf, 
                              _("buffer list"), 
                              NULL);
        gtk_clist_set_column_width (GTK_CLIST (gpiv->clist_buf), 
                                    0, 
                                    15);
        gtk_clist_set_column_width (GTK_CLIST (gpiv->clist_buf), 
                                    1, 
                                    80);
        gtk_clist_set_selection_mode (GTK_CLIST (gpiv->clist_buf),
                                      GTK_SELECTION_EXTENDED /* MULTIPLE */);
        gtk_clist_column_titles_show (GTK_CLIST (gpiv->clist_buf));
        
        gtk_object_set_data (GTK_OBJECT (gpiv->clist_buf), 
                             "gpiv", 
                             gpiv);
        g_signal_connect (GTK_OBJECT (gpiv->clist_buf), 
                          "select_row",
                          G_CALLBACK (on_clist_buf_rowselect), 
                          NULL);
        
        
        gpiv->label_buf_1 = gtk_label_new ( _("#"));
        gtk_widget_ref (gpiv->label_buf_1);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "label_buf_1",
                                  gpiv->label_buf_1,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->label_buf_1);
        gtk_clist_set_column_widget (GTK_CLIST (gpiv->clist_buf), 
                                     0, 
                                     gpiv->label_buf_1);
        
        
        gpiv->label_buf_2 = gtk_label_new ( _("buffer name"));
        gtk_widget_ref (gpiv->label_buf_2);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "label_buf_2",
                                  gpiv->label_buf_2,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->label_buf_2);
        gtk_clist_set_column_widget (GTK_CLIST (gpiv->clist_buf),
                                     1,
                                     gpiv->label_buf_2);
        gtk_misc_set_alignment (GTK_MISC (gpiv->label_buf_2),
                                0.0,
                                0.5);
        gtk_clist_column_titles_passive (GTK_CLIST (gpiv->clist_buf));
/*   gtk_clist_set_policy (GTK_CLIST (gpiv->clist_buf), */
/*                                GTK_POLICY_ALWAYS, */
/*                                GTK_POLICY_ALWAYS); */


        gtk_clist_set_column_auto_resize (GTK_CLIST (gpiv->clist_buf),
                                          1,
                                          TRUE);
        
        gpiv->scrolledwindow_buf_adj =
                gtk_adjustment_new (0.0,
                                    500.0,
                                    100.0,
                                    5.0,
                                    50.0, 
                                    250.0 /* 2500.0 */);
        gtk_scrolled_window_set_hadjustment (GTK_SCROLLED_WINDOW
                                             (gpiv->scrolledwindow_buf),
                                             GTK_ADJUSTMENT
                                             (gpiv->scrolledwindow_buf_adj));
        
        
        gtk_drag_dest_set (GTK_WIDGET (gpiv->clist_buf),
                           GTK_DEST_DEFAULT_ALL,
                           target_table, 
                           G_N_ELEMENTS (target_table),
                           GDK_ACTION_COPY);
        g_signal_connect (GTK_OBJECT (gpiv->clist_buf), 
                          "drag_data_received",
                          G_CALLBACK (on_clist_buf_drag_data_received),
                          NULL);
        
/*
 * Application bar buttons
 */
        gpiv->appbar = gnome_appbar_new (TRUE,
                                         TRUE, 
                                         GNOME_PREFERENCES_ALWAYS);
        gtk_widget_ref (gpiv->appbar);
        gtk_object_set_data_full (GTK_OBJECT (gpiv->console), 
                                  "appbar",
                                  gpiv->appbar,
                                  (GtkDestroyNotify) gtk_widget_unref);
        gtk_widget_show (gpiv->appbar);
        gnome_app_set_statusbar (GNOME_APP (gpiv->console),
                                 gpiv->appbar);
/*   gtk_widget_set_sensitive (gpiv->appbar, FALSE); */





/*
 * set gpiv->menubars after definition of all console widgets
 */
        if (gpiv_par->console__view_gpivbuttons) {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (settings_menu_gpiv[0].widget), 
                                                TRUE);
        } else {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (settings_menu_gpiv[0].widget),
                                                FALSE);
        }
        
        
        if (gpiv_par->console__view_tabulator) {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (settings_menu_gpiv[1].widget), 
                                                TRUE);
        } else {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (settings_menu_gpiv[1].widget),
                                                FALSE);
        }
        
        
        if (gpiv_par->console__show_tooltips) {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (help_menu_gpiv[0].widget), 
                                                TRUE);
        } else {
                gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                                (help_menu_gpiv[0].widget),
                                                FALSE);
        }
        
        
        gnome_app_install_menu_hints (GNOME_APP (gpiv->console), 
                                      menubar_gpiv);
        return gpiv;
}
