/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

/*
 * $Log: console_interface.h,v $
 * Revision 1.9  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.8  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.7  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.6  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.5  2006-09-18 07:27:05  gerber
 * *** empty log message ***
 *
 * Revision 1.4  2006/01/31 14:28:11  gerber
 * version 0.3.0
 *
 * Revision 1.2  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
*/
/* -- widgets from   GtkWidget *gpiv_gui_gtk ------------*/
/* #ifdef HAVE_CONFIG_H */
/* #  include <config.h> */
/* #endif */

/* GtkWidget* create_exit (void); */

#ifndef CONSOLE_INTERFACE_H
#define CONSOLE_INTERFACE_H

#include "gpiv_gui.h"
#ifdef ENABLE_DAC 
#include "dac_interface.h"
#endif /* ENABLE_DAC  */
#include "imgh_interface.h"
#ifdef ENABLE_IMGPROC
#include "imgproc_interface.h"
#endif /* ENABLE_IMGPROC */
#include "piveval_interface.h"
#include "pivvalid_interface.h"
#include "pivpost_interface.h"

/*
 * widget for pop-up menu
 */

typedef struct _GpivConsole GpivConsole;
struct _GpivConsole {
/*
 * General widgets for gpiv-gui
 */
    GnomeApp *console;
    GtkWidget *appbar;
    GtkTooltips *tooltips;

    GtkWidget *dock1;
    GtkWidget *alignment3;
    GtkWidget *menubar;
    GtkWidget *eventbox1;
    GtkWidget *toolbar1;
    gint tmp_toolbar_icon_size;
    GtkWidget *button_open;
    GtkWidget *button_save;
/*     GtkWidget *button_print; */
    GtkWidget *button_execute;
    GtkWidget *button_stop;
    GtkWidget *button_close;
    GtkWidget *vseparator;
    GtkWidget *button_exit;
    GtkWidget *hbox_main;
    GtkWidget *handlebox_buf;
  
/*
 * Menu widgets
 */
    GtkWidget *toggle_view_buttons;
    const GtkWidget *toggle_view_tool;
    const GtkWidget *toggle_view_tab;

/*
 * Buffer handlebox & clist
 */
    gint first_selected_row;
    gint last_selected_row;
    GtkWidget *alignment_buf; 
    GtkWidget *eventbox_buf;
    GtkWidget *scrolledwindow_buf;
    GtkObject *scrolledwindow_buf_adj;
    GtkWidget *button_buf;
    GtkWidget *viewport_buf;
#ifdef GTK4
    GtkTreeStore *list_buf;
#else
#endif /* GTK4 */
    GtkWidget *clist_buf;
    GtkWidget *label_buf_1;
    GtkWidget *label_buf_2;
GtkWidget *vbox_main;

/*
 * gpiv toolbox
 */
    GtkWidget *handlebox1;
    GtkWidget *scrolledwindow_handbox1;
    GtkWidget *viewport_handbox1;
    GtkWidget *toolbar2;
    GtkWidget *hbox_toolbar2;
#ifdef ENABLE_CAM
    GtkWidget *button_toolbar_cam;
#endif
#ifdef ENABLE_TRIG
    GtkWidget *button_toolbar_trig;
#endif
#ifdef ENABLE_IMGPROC
    GtkWidget *button_toolbar_imgproc;
#endif
    GtkWidget *button_toolbar_piv;
    GtkWidget *button_toolbar_gradient;
    GtkWidget *button_toolbar_resstats;
    GtkWidget *button_toolbar_errvec;
    GtkWidget *button_toolbar_peaklock;
    GtkWidget *button_toolbar_scale;
    GtkWidget *button_toolbar_average;
    GtkWidget *button_toolbar_subavg;
    GtkWidget *button_toolbar_vorstra;

/*
 * Tabulator
 */
    GtkWidget *notebook; 
#ifdef ENABLE_CAM
    Dac *dac;
    GtkWidget *tablabel_dac;
#else /* ENABLE_CAM */
#ifdef ENABLE_TRIG
    Dac *dac;
    GtkWidget *tablabel_dac;
#endif /* ENABLE_TRIG */
#endif /* ENABLE_CAM */
    Imgheader *imgh;
    GtkWidget *tablabel_imgh;
#ifdef ENABLE_IMGPROC
    Imgprocess *imgproc;
    GtkWidget *tablabel_imgproc;
#endif
    PivEval *piveval;
    GtkWidget *tablabel_piveval;
    PivValid *pivvalid;
    GtkWidget *tablabel_pivvalid;
    PivPost *pivpost;
    GtkWidget *tablabel_pivpost;
    
    GSList *mouse_sel_group;

};


GpivConsole *
create_gpiv(void);

GtkWidget * 
create_menu_gpiv_popup (GpivConsole *gpiv);



#endif   /*  CONSOLE_INTERFACE_H */
