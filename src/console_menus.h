
/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/


/*
 * menu definitions
 * $Log: console_menus.h,v $
 * Revision 1.10  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.9  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.8  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.7  2006/01/31 14:28:11  gerber
 * version 0.3.0
 *
 * Revision 1.6  2005/06/15 09:40:40  gerber
 * debugged, optimized
 *
 * Revision 1.5  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.4  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 * Revision 1.3  2003/09/01 11:17:14  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef MENUS_H
#define MENUS_H

#include "console.h"

enum {
	TARGET_URI_LIST = MAX_BUFS
};

static GtkTargetEntry target_table[] = {
/*   { "text/plain", 0, 0 } */
  { "text/uri-list", 0, TARGET_URI_LIST }
};

/*
 * menu bar menus
 */
static GnomeUIInfo file_menu_gpiv[] =
{
  GNOMEUIINFO_MENU_OPEN_ITEM (on_open_activate, NULL),
  GNOMEUIINFO_MENU_SAVE_ITEM (on_save_activate, NULL),
  GNOMEUIINFO_MENU_SAVE_AS_ITEM (on_save_as_activate, NULL),
/*   GNOMEUIINFO_MENU_PRINT_ITEM (on_print_activate, NULL), */
  GNOMEUIINFO_MENU_CLOSE_ITEM (on_close_activate, NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_EXIT_ITEM (on_exit_activate, NULL),
  GNOMEUIINFO_END
};


static GnomeUIInfo settings_menu_gpiv[] =
{
  {
    GNOME_APP_UI_TOGGLEITEM, N_("GPIV buttons"),
    N_("show GPIV check-buttons"),
    (gpointer) on_gpivbuttons_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL 
  },

  {
    GNOME_APP_UI_TOGGLEITEM, N_("tabulator"),
    N_("show tabulator containing parameter settings"),
    (gpointer) on_tabulator_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL 
  },

  GNOMEUIINFO_SEPARATOR,

  {
    GNOME_APP_UI_ITEM, N_("Preferences"),
    N_("Define settings of the application"),
    (gpointer) on_preferences_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL 
  },

  GNOMEUIINFO_END
};



static GnomeUIInfo help_menu_gpiv[] =
{
  {
    GNOME_APP_UI_TOGGLEITEM, N_("show tooltips"),
    N_("show extended information in a small pop-up window"),
    (gpointer) on_tooltip_activate, /* NULL */ "gpiv", NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL 
  },

  {
    GNOME_APP_UI_ITEM, N_("manual"),
    NULL,
    (gpointer) on_manual_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL
  },

/*   GNOMEUIINFO_HELP ("/home/gerber/gpiv/docs/index.html"), */
  GNOMEUIINFO_MENU_ABOUT_ITEM (on_about_activate, NULL),
  GNOMEUIINFO_END
};



static GnomeUIInfo menubar_gpiv[] =
{
  GNOMEUIINFO_MENU_FILE_TREE (file_menu_gpiv),
  GNOMEUIINFO_MENU_SETTINGS_TREE (settings_menu_gpiv),
  GNOMEUIINFO_MENU_HELP_TREE (help_menu_gpiv),
  GNOMEUIINFO_END
};


/*
 * Popup menus
 */

static GnomeUIInfo file_menu_gpiv_popup[] =
{
  GNOMEUIINFO_MENU_OPEN_ITEM (on_open_activate, NULL),
  GNOMEUIINFO_MENU_SAVE_ITEM (on_save_activate, NULL),
  GNOMEUIINFO_MENU_SAVE_AS_ITEM (on_save_as_activate, NULL),
/*   GNOMEUIINFO_MENU_PRINT_ITEM (on_print_activate, NULL), */
  GNOMEUIINFO_MENU_CLOSE_ITEM (on_close_activate, NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_EXIT_ITEM (on_exit_activate, NULL),
  GNOMEUIINFO_END
};


static GnomeUIInfo settings_menu_gpiv_popup[] =
{
  {
    GNOME_APP_UI_TOGGLEITEM, N_("GPIV buttons"),
    N_("show GPIV check-buttons"),
    (gpointer) on_gpivbuttons_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL 
  },
  {
    GNOME_APP_UI_TOGGLEITEM, N_("tabulator"),
    N_("show tabulator containing parameter settings"),
    (gpointer) on_tabulator_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL 
  },
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM, N_("Preferences"),
    NULL,
    (gpointer) on_preferences_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL
  },
  GNOMEUIINFO_END
};



static GnomeUIInfo help_menu_gpiv_popup[] =
{
  {
    GNOME_APP_UI_TOGGLEITEM, N_("show tooltips"),
    N_("show extended information in a small pop-up window"),
    (gpointer) on_tooltip_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL 
  },
  {
    GNOME_APP_UI_ITEM, N_("manual"),
    NULL,
    (gpointer) on_manual_activate, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    0, (GdkModifierType) 0, NULL
  },
  GNOMEUIINFO_MENU_ABOUT_ITEM (on_about_activate, NULL),
  GNOMEUIINFO_END
};



static GnomeUIInfo menubar_gpiv_popup[] =
{
  GNOMEUIINFO_MENU_FILE_TREE (file_menu_gpiv_popup),
  GNOMEUIINFO_MENU_SETTINGS_TREE (settings_menu_gpiv_popup),
  GNOMEUIINFO_MENU_HELP_TREE (help_menu_gpiv_popup),
  GNOMEUIINFO_END
};



#endif   /*  MENUS_H */

