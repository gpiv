/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Dac callbacks
 * $Id: dac.c,v 1.7 2008-09-16 11:13:09 gerber Exp $
 */


#ifdef ENABLE_DAC

#include "gpiv_gui.h"
#include "utils.h"
#include "console.h"
#include "dac.h"


/*
 * BUGFIX: Put somwhere to update on regular basis:
        gtk_label_set_text(GTK_LABEL(gpiv->dac->label_temp), 
                           "Temp");
*/

static int init, start, stop, error;


/* pthread_mutex_t mutex_rec = PTHREAD_MUTEX_INITIALIZER; */


/*
 * Global functions
 */

/*
 * Callback functions
 */

void
on_entry_dac_fname (GtkSpinButton *widget, 
                    GtkWidget *entry
                    )
/*-----------------------------------------------------------------------------
 */
{
    gint i;
    Dac * dac = gtk_object_get_data (GTK_OBJECT (widget), "dac");

    push_list_lastfnames ((gchar *) gtk_entry_get_text( GTK_ENTRY( dac->entry_fname)));
    dac->combo_fname_items = NULL;
    for (i = 0; i < gpiv_var->number_fnames_last; i++) {
        dac->combo_fname_items = g_list_append (dac->combo_fname_items, 
                                                gpiv_var->fn_last[i]);
    }
    gtk_combo_set_popdown_strings (GTK_COMBO (dac->combo_fname), 
                                   dac->combo_fname_items);
    g_list_free (dac->combo_fname_items);

/*
 * not used:
 *     dac_par.fname__set = TRUE;
 */
}


void
on_checkbutton_fname_date_enter (GtkWidget *widget, 
                                 gpointer data
                                 )
/* ----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Extends the name with current date");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void
on_checkbutton_fname_date(GtkWidget *widget, 
                          gpointer data
                          )
/* ----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	gpiv_var->fname_date = TRUE;
    } else {
	gpiv_var->fname_date = FALSE;
    }
    gnome_config_push_prefix("/gpiv/RuntimeVariables/");
    gnome_config_set_bool("fname_date", gpiv_var->fname_date);
    gnome_config_pop_prefix();
    gnome_config_sync();
}



void
on_checkbutton_fname_time_enter(GtkWidget *widget, 
                                 gpointer data
                                 )
/* ----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Extends the name with current time");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void
on_checkbutton_fname_time(GtkWidget *widget, 
                          gpointer data
                          )
/* ----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	gpiv_var->fname_time = TRUE;
    } else {
	gpiv_var->fname_time = FALSE;
    }
    gnome_config_push_prefix("/gpiv/RuntimeVariables/");
    gnome_config_set_bool("fname_time", gpiv_var->fname_time);
    gnome_config_pop_prefix();
    gnome_config_sync();
}



void
on_radiobutton_dac_mouse_1_enter(GtkWidget * widget, 
                                  GtkWidget * entry
                                  )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("indefinite periodic recording");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void
on_radiobutton_dac_mouse_2_enter(GtkWidget * widget, 
                                  GtkWidget * entry
                                  )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("with pre-defined number of images");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void
on_radiobutton_dac_mouse_3_enter(GtkWidget * widget, 
                                  GtkWidget * entry
                                  )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("one shot with interrupd");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void
on_radiobutton_dac_mouse_4_enter(GtkWidget * widget, 
                                  GtkWidget * entry
                                  )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("periodic with interrupt");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void
on_radiobutton_dac_mouse_5_enter(GtkWidget * widget, 
                                  GtkWidget * entry
                                  )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("with incremental separation time dt");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void
on_radiobutton_dac_mouse_6_enter(GtkWidget * widget, 
                                  GtkWidget * entry
                                  )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("double exposure");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



#ifdef ENABLE_CAM
void
on_radiobutton_dac_mouse(GtkWidget * widget, 
                         GtkWidget * entry
                         )
/*-----------------------------------------------------------------------------
 */
{
    gint mouse_select = atoi(gtk_object_get_data(GTK_OBJECT(widget),
					"mouse_select"));
    gl_cam_par->mode = mouse_select;
/*     g_warning("on_radiobutton_dac_mouse:: mode = %d",  */
/*               gl_cam_par.mode); */

    if (mouse_select == GPIV_CAM_MODE__PERIODIC) {}
    if (mouse_select == GPIV_CAM_MODE__DURATION) {}
    if (mouse_select == GPIV_CAM_MODE__ONE_SHOT_IRQ) {}
    if (mouse_select == GPIV_CAM_MODE__TRIGGER_IRQ) {}
    if (mouse_select == GPIV_CAM_MODE__INCREMENT) {}
    if (mouse_select == GPIV_CAM_MODE__DOUBLE) {}

}
#else /* ENABLE_CAM */

#ifdef ENABLE_TRIG
void
on_radiobutton_dac_mouse(GtkWidget * widget, 
                         GtkWidget * entry
                         )
/*-----------------------------------------------------------------------------
 */
{
    gint mouse_select = atoi(gtk_object_get_data(GTK_OBJECT(widget),
					"mouse_select"));
    trig_par.ttime.mode = mouse_select;
/*     g_warning("on_radiobutton_dac_mouse:: ttime.mode = %d",  */
/*               dac_par.ttime.mode); */

    if (mouse_select == GPIV_TIMER_MODE__PERIODIC) {}
    if (mouse_select == GPIV_TIMER_MODE__DURATION) {}
    if (mouse_select == GPIV_TIMER_MODE__ONE_SHOT_IRQ) {}
    if (mouse_select == GPIV_TIMER_MODE__TRIGGER_IRQ) {}
    if (mouse_select == GPIV_TIMER_MODE__INCREMENT) {}
    if (mouse_select == GPIV_TIMER_MODE__DOUBLE) {}

}

#endif /* ENABLE_TRIG */
#endif /* ENABLE_CAM */

#ifdef ENABLE_TRIG
#endif /* ENABLE_TRIG */


#ifdef ENABLE_CAM
void
on_spinbutton_dac_trigger_nf(GtkSpinButton * widget, 
                            GtkWidget * entry
                            )
/*-----------------------------------------------------------------------------
 */
{
    gl_cam_par->cycles = gtk_spin_button_get_value_as_int(widget);
}

#else /* ENABLE_CAM */
#ifdef ENABLE_TRIG
void
on_spinbutton_dac_trigger_nf(GtkSpinButton * widget, 
                            GtkWidget * entry
                            )
/*-----------------------------------------------------------------------------
 */
{
    trig_par.ttime.cycles = gtk_spin_button_get_value_as_int(widget);
}

#endif /* ENABLE_TRIG */
#endif /* ENABLE_CAM */


#endif /* ENABLE_DAC  */
