/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (callback) functions for dac
 * $Log: dac.h,v $
 * Revision 1.3  2006-09-18 07:27:05  gerber
 * *** empty log message ***
 *
 * Revision 1.2  2005/02/26 09:17:13  gerber
 * structured of interrogate function by using gpiv_piv_isiadapt
 *
 * Revision 1.1  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 */

#ifndef DAC_H
#define DAC_H
#ifdef ENABLE_DAC

/* #define MAX_COMBO_LIST 5 */

/* #define DACPAR_TIMING_MIN 3 */
/* #define DACPAR_TIMING_MAX 3 */
/* #define DACPAR_CAP_MIN 100 */
/* #define DACPAR_CAP_MAX 1000 */
/* #define DACPAR_NIMG_MIN 1 */
/* #define DACPAR_NIMG_MAX MAX_BUFS */


/*
 * Callback functions
 */

/* gboolean */
/* on_darea_expose (GtkWidget *widget, */
/*                  GdkEventExpose *event, */
/*                  gpointer user_data); */

void
on_entry_dac_fname(GtkSpinButton *widget, 
                   GtkWidget *entry);

void
on_checkbutton_fname_date_enter(GtkWidget *widget, 
                                gpointer data);

void
on_checkbutton_fname_date(GtkWidget *widget, 
                          gpointer data);

void
on_checkbutton_fname_time_enter(GtkWidget *widget, 
                                gpointer data);

void
on_checkbutton_fname_time(GtkWidget *widget, 
                          gpointer data);

void
on_radiobutton_dac_mouse(GtkWidget * widget, 
                         GtkWidget * entry);

void
on_radiobutton_dac_mouse_1_enter(GtkWidget * widget, 
                                 GtkWidget * entry);

void
on_radiobutton_dac_mouse_2_enter(GtkWidget * widget, 
                                  GtkWidget * entry);

void
on_radiobutton_dac_mouse_3_enter(GtkWidget * widget, 
                                  GtkWidget * entry);

void
on_radiobutton_dac_mouse_4_enter(GtkWidget * widget, 
                                  GtkWidget * entry);

void
on_radiobutton_dac_mouse_5_enter(GtkWidget * widget, 
                                  GtkWidget * entry);

void
on_radiobutton_dac_mouse_6_enter(GtkWidget * widget, 
                                  GtkWidget * entry);

void
on_spinbutton_dac_trigger_nf(GtkSpinButton * widget, 
                            GtkWidget * entry);

#endif /* ENABLE_DAC  */
#endif  /* DAC_H */
