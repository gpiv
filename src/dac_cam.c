/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2005, 2006, 2007, 2008
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * functions for dac_cam
 * $Log: dac_cam.c,v $
 * Revision 1.7  2008-04-28 12:00:57  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.6  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.5  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.4  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.3  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.2  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.1  2006-09-18 07:29:49  gerber
 * Split up of triggering and image recording (camera)
 *
 */
#ifdef ENABLE_CAM

#include <pthread.h>
#include "gpiv_gui.h"
#include "utils.h"
#include "console.h"
#include "dac_cam.h"
#include "display.h"
#include <libdc1394/dc1394_control.h>

static struct tm *dac_time;
static GDate *date;

pthread_mutex_t mutex_rec = PTHREAD_MUTEX_INITIALIZER;


typedef struct _ImgFrame ImgFrame;
struct _ImgFrame {
    unsigned char **A;
    unsigned char **B;
/*     gint count; */
};

/*
 * Prototypes of local functions
 */
static void
rec_frame(ImgFrame *img_frame
          );

static void
thread_rec_frame(void *img_frame
                 );

static void
thread_rec_frame_serie(ImgFrame img_frame[GPIV_NIMG_MAX]
                       );

static void
add_buffer(GpivConsole *gpiv,
           gchar *fname_base,
           gint ibuf, 
           unsigned char **frameA, 
           unsigned char **frameB
           );

static void
renew_buffer(GpivConsole *gpiv,
             gchar *fname_base,
             gint ibuf, 
             unsigned char **frameA, 
             unsigned char **frameB
             );

static void 
recreate_img(Display * disp,
             unsigned char **frameA, 
             unsigned char **frameB
             );


static int
open_dac_img(GpivCamVar *cam_var,
             unsigned char **frameA,
             unsigned char **frameB,
             Image * img,
             gboolean alloc_mem
             );

static void
load_dac_buffer(GpivConsole *gpiv,
                GpivCamVar *cam_var,
                gboolean second_image,
                char *fname,
                int ibuf,
                unsigned char **frameA,
                unsigned char **frameB,
                enum ClistPut clist_put
                );

static void
rec_and_display(GpivConsole *gpiv
                );

/*
 * Global functions
 */
void 
exec_cam_start(GpivConsole * gpiv
               )
/*-----------------------------------------------------------------------------
 */
{
/*
 * dc1394 stuff
 */
/*     quadlet_t value; */

   cancel_process = FALSE;

/*
 * report camera's feature set
 * in: on_menu_camera_select
 */
/*         dc1394_print_feature_set(&cam_var->feature_set[0]); */

/*    g_message("exec_cam_start:: mode[%d] = %s", */
/*              cam_var->misc_info[0].mode, */
/*              format0_desc[cam_var->misc_info[0].mode - MODE_FORMAT0_MIN]); */
/*    g_message("exec_cam_start:: format[%d] = %s", */
/*              cam_var->misc_info[0].format,  */
/*              format_desc[cam_var->misc_info[0].format - FORMAT_MIN]); */

    if (verbose) g_warning("exec_cam_start:: fps = %d", cam_var->misc_info[0].framerate/*  - FRAMERATE_MIN */);
    if (dc1394_setup_capture(cam_var->camera[0].handle, 
                             cam_var->camera[0].id, 
                             cam_var->misc_info[0].iso_channel, 
                             cam_var->misc_info[0].format, 
                             cam_var->misc_info[0].mode, 
                             cam_var->maxspeed,

                             cam_var->misc_info[0].framerate, 

/*                              FRAMERATE_7_5,   */

/* FRAMERATE_3_75, */

                             &cam_var->capture[0])
        != DC1394_SUCCESS) {
        dc1394_release_camera(cam_var->handle, &cam_var->capture[0]);
        raw1394_destroy_handle(cam_var->handle);
        warning_gpiv(_("Unable to setup camera-\n\
check line %d of %s to make sure\n\
that the video mode,framerate and format are\n\
supported by your camera"),
                    __LINE__,__FILE__);
        return;
    }


/*
 * have the camera start sending us data
 */
    if (dc1394_start_iso_transmission(cam_var->handle, cam_var->capture[0].node) 
        != DC1394_SUCCESS) {
        dc1394_release_camera(cam_var->handle, &cam_var->capture[0]);
        raw1394_destroy_handle(cam_var->handle);
        warning_gpiv(_("Unable to start camera iso transmission"));
        return;
    }

/*
 * let sending data for a while before grabbing and storing to stabilize
 * data transfer
 */
/*     sleep(1); */
    usleep(1000);


/*
 * set trigger mode and use triggering or just free run
 */
    
    if (gpiv_par->console__trigger_cam) {
        if (verbose) g_warning("exec_cam_start:: trigger_cam");
/*             if (dc1394_set_trigger_mode(cam_var->handle,  */
/*                                         cam_var->capture[0].node,  */
/*                                         TRIGGER_MODE_0) != DC1394_SUCCESS) */
/*                 { */
/*                     warning_gpiv(_("unable to set camera trigger mode")); */
/*                 } */
/*             exec_trigger_start(); */

            rec_and_display(gpiv);

        /*             exec_trigger_stop(); */
    } else {
        if (verbose) g_warning("exec_cam_start:: !trigger_cam");
            rec_and_display(gpiv);
    }


/*     pthread_exit(NULL); */

/*
 * Stop data transmission
 */
        if (verbose) g_warning("exec_cam_start:: Stop data transmission and release camera");
    if (dc1394_stop_iso_transmission(cam_var->handle, cam_var->capture[0].node)
        != DC1394_SUCCESS
/*         && dc1394_release_camera(cam_var->handle, &cam_var->capture[0])   */
/*         != DC1394_SUCCESS  */
        ) {
        warning_gpiv(_("couldn't stop the camera?"));
    }
/* in: exec_stop_cam    dc1394_release_camera(cam_var->handle, &cam_var->capture[0]);     */
/*in: exec_stop_cam:    raw1394_destroy_handle(cam_var->handle); */

}


void 
exec_cam_stop (void
               )
/*-----------------------------------------------------------------------------
 */
{
    gint i;

    cancel_process = TRUE;
    for (i = 0; i < cam_var->numCameras; i++) { 
        if (dc1394_stop_iso_transmission(cam_var->handle, cam_var->capture[i].node)
            != DC1394_SUCCESS) 
            {
                warning_gpiv(_("couldn't stop the camera?"));
            }
        
        pthread_exit(NULL);
        dc1394_release_camera(cam_var->handle, &cam_var->capture[0]);
    }
    if (cam_var->numCameras > 0) {
        raw1394_destroy_handle(cam_var->handle);
    }
}



/*
 * Callback functions
 */
void
on_menu_camera_select (GtkWidget *widget, 
                       gpointer data)
/* ----------------------------------------------------------------------------
 */
{
}



void
on_menu_format (GtkWidget *widget, 
                gpointer data)
/* ----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gint mode = (int) data;
    gint format = 0;
    gint was_iso_on;
/*         change_mode_and_format(format, FORMAT_VGA_NONCOMPRESSED); */
/*   IsoFlowCheck(state); */

       if (verbose) g_message("on_menu_format:: ");
    if (!dc1394_get_iso_status(cam_var->camera[0].handle, cam_var->camera[0].id, 
                               &cam_var->misc_info[0].is_iso_on)) {
        g_warning("Cannot get ISO status");
    } else {
        if (cam_var->misc_info[0].is_iso_on > 0) {
            cancel_process = TRUE;
        }
    }
    was_iso_on = cam_var->misc_info[0].is_iso_on;
/*        g_message("on_menu_format:: is_iso_on = %d", was_iso_on); */


    if (mode >= MODE_FORMAT0_MIN && mode <= MODE_FORMAT0_MAX) {
        format = FORMAT_VGA_NONCOMPRESSED;

/*         g_message("on_menu_format:: mode[%d] = %s",  */
/*                   mode, format0_desc[mode - MODE_FORMAT0_MIN]); */
        
/*         g_message(":n_menu_format: format[%d] = %s", */
/*                   format, */
/*                   format_desc[format - FORMAT_MIN]); */
    } else if  (mode >= MODE_FORMAT1_MIN && mode <= MODE_FORMAT1_MAX) {
        format = FORMAT_SVGA_NONCOMPRESSED_1;
    } else if  (mode >= MODE_FORMAT2_MIN && mode <= MODE_FORMAT2_MAX) {
        format = FORMAT_SVGA_NONCOMPRESSED_2;
    } else if  (mode >= MODE_FORMAT6_MIN && mode <= MODE_FORMAT6_MAX) {
        format = FORMAT_STILL_IMAGE;
    } else if  (mode >= MODE_FORMAT7_MIN && mode <= MODE_FORMAT7_MAX) {
        format = FORMAT_SCALABLE_IMAGE_SIZE;
/*     } else if  (mode >= COLOR_FORMAT7_MIN && mode <= COLOR_FORMAT7_MAX) { */
/*         format = FORMAT_SCALABLE_IMAGE_SIZE; */
    } else {
        warning_gpiv(_("on_menu_format: non valid format"));
    }

    if (!dc1394_set_video_format(cam_var->camera[0].handle, 
                                 cam_var->camera[0].id,
                                 format)) {
        if (verbose) g_warning("on_menu_format:: Could not set video format");
    } else {
        cam_var->misc_info[0].format = format;
    }

    if (!dc1394_set_video_mode(cam_var->camera[0].handle,
                               cam_var->camera[0].id,
                               mode)) {
        if (verbose) g_warning("on_menu_format:: Could not set video format");
    } else {
        cam_var->misc_info[0].mode = mode;
    }



    if (was_iso_on > 0) {
        exec_cam_start(gpiv);
    }

/*   BuildFpsMenu(); */


/*   UpdateTriggerFrame(); */

/*   IsoFlowResume(state); */


/*     MODE_320x240_YUV422, */
/*     MODE_640x480_YUV411, */
/*     MODE_640x480_YUV422, */
/*     MODE_640x480_RGB, */
/*     MODE_640x480_MONO, */
/*     MODE_640x480_MONO16 */
}



void
on_menu_fps (GtkWidget *widget, 
             gpointer data)
/* ----------------------------------------------------------------------------
 */
{
    cam_var->misc_info[0].framerate = (int) data/*  - FRAMERATE_MIN */;
/* cam_var->misc_info[0].framerat */
    if (verbose) g_warning("on_menu_fps:: fps = %d", cam_var->misc_info[0].framerate);
}


/*
 * BUGFIX: include in trigger callbacks?
 */
void
on_trigger_polarity_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
/* ----------------------------------------------------------------------------
 */
  if (!dc1394_set_trigger_polarity(cam_var->camera[0].handle,
                                   cam_var->camera[0].id,
                                   togglebutton->active)) {
      message_gpiv(_("Cannot set trigger polarity"));
  } else {
      cam_var->feature_set[0].feature[FEATURE_TRIGGER-FEATURE_MIN].
          trigger_polarity = (int) togglebutton->active;
  }
}



void
on_trigger_external_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
/* ----------------------------------------------------------------------------
 */
  if (!dc1394_feature_on_off(cam_var->camera[0].handle, 
                             cam_var->camera[0].id, 
                             FEATURE_TRIGGER, 
                             togglebutton->active)) {
      message_gpiv(_("Cannot set external trigger source"));
  } else {
      cam_var->feature_set[0].feature[FEATURE_TRIGGER - FEATURE_MIN].is_on =
          togglebutton->active;
  }
/*   UpdateTriggerFrame(); */
}



void
on_trigger_mode_activate (GtkWidget *widget, 
                         gpointer user_data)
/* ----------------------------------------------------------------------------
 */
{
  if (!dc1394_set_trigger_mode(cam_var->camera[0].handle, cam_var->camera[0].id,
                               (int) user_data)) {
      message_gpiv(_("Cannot set trigger mode"));
  } else {
      cam_var->feature_set[0].feature[FEATURE_TRIGGER-FEATURE_MIN].
          trigger_mode = (int) user_data;
  }

/*   UpdateTriggerFrame(); */
}



/* void */
/* on_trigger_value_changed               (GtkAdjustment    *adj, */
/*                                         gpointer         user_data) */
/* { */
/*   if (!dc1394_set_feature_value(cam_var->camera[0].handle, cam_var->camera[0].id, */
/*                                 FEATURE_TRIGGER, adj->value)) */
/*     message_gpiv(_("Cannot set external trigger count")); */
/*   else */
/*     cam_var->feature_set[0].feature[FEATURE_TRIGGER-FEATURE_MIN].value = */
/*        adj->value; */
/* } */


void
on_checkbutton_camera_trigger_enter (GtkWidget *widget, 
                                     gpointer data)
/* ----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Enables triggering of the camera");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void
on_checkbutton_camera_trigger (GtkWidget *widget, 
                               gpointer data)
/* ----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	gpiv_par->console__trigger_cam = TRUE;
    } else {
	gpiv_par->console__trigger_cam = FALSE;
    }
}



void
on_man_auto_menu (GtkWidget *widget, 
                  gpointer data)
/* ----------------------------------------------------------------------------
 */
{
    GtkWidget *scale = gtk_object_get_data(GTK_OBJECT(widget), "scale");
    int feature = (int) data;
    enum VariableType {
        MAN,
        AUTO    
    } var_type;
    
    var_type = atoi(gtk_object_get_data(GTK_OBJECT(widget), "var_type"));
    if (!dc1394_auto_on_off(cam_var->camera[0].handle, 
                            cam_var->camera[0].id,
                            feature, var_type)) {
        message_gpiv(_("on_man_auto_menu: Cannnot set auto / manual"));
    }
    
    if (var_type == MAN) {
        gtk_widget_set_sensitive (GTK_WIDGET(scale), TRUE);
    } else if (var_type == AUTO) {
        gtk_widget_set_sensitive (GTK_WIDGET(scale), FALSE);
    }

}



void
on_scale_changed (GtkAdjustment *adj, 
                  gpointer user_data)
/* ----------------------------------------------------------------------------
 */
{
    switch((int)user_data) {
    case FEATURE_TEMPERATURE:
        if (!dc1394_set_temperature(cam_var->camera[0].handle,
                                    cam_var->camera[0].id,
                                    adj->value))
            message_gpiv(_("on_scale_changed: Cannot set temperature"));
        else
            cam_var->feature_set[0].feature[FEATURE_TEMPERATURE-FEATURE_MIN].
                value = adj->value;
        break;

    default:
/*         g_message("on_scale_changed:: feature = %d value = %f",  */
/*                   (gint) user_data, adj->value); */
        if (!dc1394_set_feature_value(cam_var->camera[0].handle,
                                      cam_var->camera[0].id,
                                      (gint) user_data,
                                      adj->value)) {
            message_gpiv(_("on_scale_changed: Cannot set feature"));
        } else {
            cam_var->feature_set[0].feature[(gint) user_data - FEATURE_MIN].
                value = adj->value;
        }
        break;
    }

}



void 
on_button_dac_camstart_enter (GtkWidget *widget, 
                              gpointer data
                              )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Records PIV images");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_button_dac_camstart (GtkWidget * widget, 
                        gpointer data
                        )
/* ----------------------------------------------------------------------------
 * The actual calculation of particle image displacements
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");

    cancel_process = FALSE;
/*in exec_cam_start:     exec_trigger_start(); */
    exec_cam_start(gpiv);

}



void 
on_button_dac_camstop_enter (GtkWidget *widget, 
                             gpointer data
                             )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Stops recording PIV images");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_button_dac_camstop (GtkWidget * widget, 
                       gpointer data
                       )
/* ----------------------------------------------------------------------------
 * The actual calculation of particle image displacements
 */
{
    cancel_process = TRUE;
#ifdef ENABLE_TRIG
    exec_trigger_stop();
#endif
    exec_cam_stop();
}


/*
 * Local functions
 */

static int
open_dac_img(GpivCamVar *cam_var,
             unsigned char **frameA,
             unsigned char **frameB,
             Image * img,
             gboolean alloc_mem
             )
/*-----------------------------------------------------------------------------
 * Opens an image obtained from the camera (Counterpart of open_img from file)
 */
{
    gint i, j;
    gchar *month;
/*
 * parameter initializing of image
 */
    gpiv_img_parameters_set(img->image->header, FALSE);

    img->image->header->ncolumns = cam_var->capture[0].frame_width;
    img->image->header->nrows = cam_var->capture[0].frame_height;
    img->image->header->x_corr = gpiv_par->x_corr;
    img->image->header->ncolumns__set = TRUE;
    img->image->header->nrows__set = TRUE;
    img->image->header->x_corr__set = TRUE;

    gl_image_par = gpiv_img_cp_parameters (img->image->header);
    if (verbose) g_message("open_dac_img:: 1 creation_date = %s", 
              img->image->header->creation_date);
/* 
 * Apply time and date to creation_date (buffer) parameter
 */
    month = month_name(g_date_month(date));
    g_snprintf(img->image->header->creation_date,
               GPIV_MAX_CHARS,
               "%d %s %d %d:%d:%d",
               g_date_day(date), 
               month, 
               g_date_year(date),
               dac_time->tm_hour,
               dac_time->tm_min, 
               dac_time->tm_sec);
    g_free(month);


/*     if(alloc_mem) { */
    img->img1 = gpiv_alloc_img(img->image_par);
/*     } */

    for (i = 0; i < img->image->header->nrows ; i++) {
        for (j = 0; j < img->image->header->ncolumns; j++) {
/*
 * format 640x480 = 1:1.3
 */
            img->img1[i][j/*  + i/3 */] = (guint16) frameA[i][j];
        }
    }



    if (gpiv_par->x_corr) {
 /*        if(alloc_mem) { */
        img->img2 = gpiv_alloc_img(img->image_par);
/*         } */
        for (i = 0; i < img->image->header->nrows ; i++) {
            for (j = 0; j < img->image->header->ncolumns; j++) {
/*
 * format 640x480 = 1:1.3
 */
                img->img2[i][j/*  + i/3 */] = (guint16) frameB[i][j];
            }
        }
    } else {
        img->img2 = img->img1;
    }

 
    img->exist_img = TRUE;
    img->saved_img = FALSE;
    return 0;
}


static void
load_dac_buffer (GpivConsole *gpiv,
                 GpivCamVar *cam_var,
                 gboolean second_image,
                 char *fname,
                 int ibuf,
                 unsigned char **frameA,
                 unsigned char **frameB,
                 enum ClistPut clist_put
                 )
/*-----------------------------------------------------------------------------
 * create display and its (popup) menu, load image, piv data and puts 
 * buffername into clist
 */
{
    gint return_val = 0;
    gchar *clist_buf_txt[MAX_BUFS][2], cl_int[4];


    display[ibuf] = create_display(fname, ibuf, gpiv);
    display_act = display[ibuf];
    gtk_widget_show(display_act->mwin);

/*     zoom_display(display_act, display_act->zoom_index); */



    g_snprintf (display_act->file_uri_name, GPIV_MAX_CHARS, "%s", fname);
    if((return_val = open_dac_img (cam_var, frameA, frameB, 
                                   display_act->img, FALSE))
       != 0) {
        gtk_object_destroy (GTK_OBJECT(display[ibuf]->mwin));
        if (ibuf > 0) {
            ibuf = nbufs - 1;;
            display_act = display[ibuf];
        } else {
            display_act = NULL;
        }
        nbufs--;
        return;
    }

/* 
 * Pop-up menu after loading data
 */
    display_act->display_popupmenu = create_display_popupmenu (display_act);
    gtk_widget_show (display_act->display_popupmenu);
    gtk_signal_connect_object (GTK_OBJECT(display_act->mwin), 
                               "button_press_event",
                               GTK_SIGNAL_FUNC (on_my_popup_handler), 
                               GTK_OBJECT(display_act->display_popupmenu));


    zoom_display (display_act, display_act->zoom_index);

/*
 * Displaying
 */
    create_background (display_act);
    create_img (display_act);
    if (display_act->img->exist_img ) {
        show_img1 (display_act);
    } else {
        hide_img1 (display_act);
    }
    
    if (gl_image_par->x_corr) {
        if (display_act->img->exist_img) {
            show_img2 (display_act);
        } else {
            hide_img2 (display_act);
        }
    }


/*     display_act->img.img_mean = image_mean(display_act->img.img1, */
/*                                            gpiv_par->img_width,  */
/*                                            gpiv_par->img_height); */


    if (gpiv_par->display__intregs == 1) {
        if (verbose) g_message("load_dac_buffer:: create_all_intregs");
        create_all_intregs(display_act);
    }

/*
 * Add buffer to list
 */
    g_snprintf(cl_int, 4, "%d", ibuf);
    clist_buf_txt[ibuf][0] = (gchar *) cl_int;
    clist_buf_txt[ibuf][1] = fname;
    if (clist_put == PREPEND) {
        gtk_clist_prepend(GTK_CLIST(gpiv->clist_buf), 
                          clist_buf_txt[ibuf]);
    } else if (clist_put == INSERT) {
        gtk_clist_insert(GTK_CLIST(gpiv->clist_buf), ibuf, 
                         clist_buf_txt[ibuf]);
    } else if (clist_put == APPEND) {
        gtk_clist_append(GTK_CLIST(gpiv->clist_buf), 
                         clist_buf_txt[ibuf]);
    } else {
        error_gpiv("non-existent CLIST enumerate");
    }
    
   gtk_clist_set_row_data(GTK_CLIST(gpiv->clist_buf), ibuf, 
                           display_act);
 

/*
 * Copy general image parameters to the buffer parameters 
 * and display in imgh tabulator
 */
/*    gpiv_img_cp_parameters(gl_image_par, display_act->img.image_par); */

   if (verbose) g_message ("load_dac_buffer:: ncolumns=%d nrows = %d",
              display_act->img->image->header->ncolumns, display_act->img->image->header->nrows);
    gtk_widget_set_size_request (display_act->canvas, 
                                 (gint) (display_act->zoom_factor * 
                                         display_act->img->image->header->ncolumns), 
                                 (gint) (display_act->zoom_factor * 
                                         display_act->img->image->header->nrows));
    
}



static void
rec_and_display (GpivConsole *gpiv
/*                 , ImgFrame *img_frame */
                 )
/*-----------------------------------------------------------------------------
 */
{
    gint ibuf = 0;
    ImgFrame img_frame[GPIV_NIMG_MAX];

    gint return_val = 0;
    pthread_t thread_rec;
    pthread_attr_t attr;

    gint mode, cycles, DURATION;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);


/*
 * Select which parameters and constants to be used; from RTAI trigger
 * system (if enabled, this may be used) or from camera. If trigger has not 
 * been enabled only from camera
 */
#ifdef ENABLE_TRIG
    if (gpiv_par->process_trig) {
        mode = gl_trig_par.ttime.mode;
        cycles = gl_trig_par.ttime.cycles;
        DURATION = GPIV_TIMER_MODE__DURATION;
    } else {
        mode = gl_cam_par->mode;
        cycles = gl_cam_par->cycles;
        DURATION = GPIV_CAM_MODE__DURATION;
    }
#else /* ENABLE_TRIG */
    mode = gl_cam_par->mode;
    cycles = gl_cam_par->cycles;
    DURATION = GPIV_CAM_MODE__DURATION;
#endif /* ENABLE_TRIG */

/*
 * Closing existing buffer windows and cleaning up Clist
 * BUGFIX: windows remain open
 */
    if (mode == DURATION) {
        for (ibuf = 0; ibuf < nbufs; ibuf++) {
            if (display[ibuf] != NULL) {
                close_buffer(gpiv, display[ibuf]);
            }
        }
        gtk_clist_clear(GTK_CLIST(gpiv->clist_buf));
        nbufs = 0;
            
/*
 * allocating image mem
 */
        for (ibuf = 0; ibuf < cycles; ibuf++) {
            img_frame[ibuf].A = 
                gpiv_ucmatrix(cam_var->capture[0].frame_height, 
                              cam_var->capture[0].frame_width);
            if (gpiv_par->x_corr) {
                img_frame[ibuf].B = gpiv_ucmatrix(cam_var->capture[0].frame_height, 
                                                  cam_var->capture[0].frame_width);
            }
        }

/*
 * recording
 */

#define THREADING
/* 
 * use threading
 */
/*         if (verbose)  */
/*             g_message("rec_and_display:: Creating thread_rec from %d", pthread_self ()); */
#ifdef THREADING
        if ((return_val = pthread_create (&thread_rec, 
                                          &attr,
                                          (void *) &thread_rec_frame_serie, 
                                          (void *) &img_frame)) == TRUE) {
            pthread_exit(NULL);
            g_error("return code from pthread_create() is %d\n", return_val);
        }
        pthread_join(thread_rec, NULL);
        if (verbose) g_message ("rec_and_display:: waited on thread_rec. Done");

/*
 * no threading used
 */
#else
        for (ibuf = 0; ibuf < cycles; ibuf++) {
            if (cancel_process) break;
            rec_frame(&img_frame[ibuf]);

            progress_value = (gfloat) (ibuf +  1.0) / 
                (gfloat) cycles;
            g_snprintf(progress_string, GPIV_MAX_CHARS, 
                       "recording image #%d", ibuf);
            while (g_main_iteration(FALSE));
            gtk_progress_set_value(gnome_appbar_get_progress
                                   (GNOME_APPBAR(gpiv->appbar)), 
                                   progress_value * 100.0);
            gnome_appbar_push(GNOME_APPBAR(gpiv->appbar), 
                              progress_string);
    }

#endif /* THREADING */
#undef THREADING
/*
 * displaying and freeing image mem
 */
        for (ibuf = 0; ibuf < cycles; ibuf++) {
            add_buffer(gpiv, gpiv_var->fn_last[0], ibuf, img_frame[ibuf].A, 
                       img_frame[ibuf].B);
            gpiv_free_ucmatrix(img_frame[ibuf].A);
            if (gpiv_par->x_corr) {
                gpiv_free_ucmatrix(img_frame[ibuf].B);
            }
        }

/*
 * timing is indefinite periodic
 * display only and continuously images in buffer 0
 */
    } else {
        ibuf = 0;
        gtk_clist_select_row(GTK_CLIST(gpiv->clist_buf), ibuf, 0); 
        if (verbose) g_message("rec_and_display:: INDEFINITE");

/* BUGFIX: for Gtk2 */
/*                 gtk_progress_bar_set_pulse_step (GNOME_APPBAR(gpiv->appbar), */
/*                                                  0.1); */

        img_frame[ibuf].A = 
            gpiv_ucmatrix(cam_var->capture[0].frame_height, 
                          cam_var->capture[0].frame_width);
        if (gpiv_par->x_corr) {
            img_frame[ibuf].B = gpiv_ucmatrix(cam_var->capture[0].frame_height, 
                                              cam_var->capture[0].frame_width);
        }

        while (!cancel_process) {
            rec_frame(&img_frame[ibuf]);
            while (g_main_iteration(FALSE));
            renew_buffer(gpiv, gpiv_var->fn_last[0], ibuf, img_frame[ibuf].A, 
                         img_frame[ibuf].B);
/* BUGFIX: for Gtk2 */
/*                     gtk_progress_bar_pulse ((GNOME_APPBAR(gpiv->appbar)) */


        }
            
        gpiv_free_ucmatrix(img_frame[ibuf].A);
        if (gpiv_par->x_corr) {
            gpiv_free_ucmatrix(img_frame[ibuf].B);
        }
        
    }

   pthread_attr_destroy(&attr);
}


static void
thread_rec_frame(void *img_frame)
/*-----------------------------------------------------------------------------
 */
{
    ImgFrame *my_frame = NULL;
    unsigned char **frameA = my_frame->A;
    unsigned char **frameB = my_frame->B;

    if (verbose) g_warning("thread_rec_frame:: 0");
    my_frame = (ImgFrame *) img_frame;


/*     unsigned char **frameA = gpiv_ucmatrix(cam_var->capture[0].frame_height,  */
/*                           cam_var->capture[0].frame_width); */


    if (verbose) g_message("thread_rec_frame:: 0.1");
/*
 * capture one frame
 */
    if (dc1394_single_capture(cam_var->handle, &cam_var->capture[0]) 
        != DC1394_SUCCESS) {
        dc1394_release_camera(cam_var->handle, &cam_var->capture[0]);
        raw1394_destroy_handle(cam_var->handle);
        warning_gpiv(_("unable to capture a frame"));
/*         return(); */
    } else {
        if (verbose) g_message("thread_rec_frame:: 1");


/*
 * Copy buffer content to tmp buffer array
 * block data for thread
 */

        pthread_mutex_lock(&mutex_rec);
        memcpy( frameA[0], (const char *) cam_var->capture[0].capture_buffer, 
                sizeof(char) * cam_var->capture[0].frame_width * 
                cam_var->capture[0].frame_height);
        pthread_mutex_unlock(&mutex_rec);
        if (verbose) g_message("thread_rec_frame:: 2");
    }    

/*
 * Repeat if cross correlation; x_corr = TRUE
 */
    if (gpiv_par->x_corr) {
        if (dc1394_single_capture(cam_var->handle, &cam_var->capture[0]) 
            != DC1394_SUCCESS) {
            dc1394_release_camera(cam_var->handle, &cam_var->capture[0]);
            raw1394_destroy_handle(cam_var->handle);
            warning_gpiv(_("unable to capture a frame"));
/*             return(); */
        } else {
            
            pthread_mutex_lock(&mutex_rec);
            memcpy( frameB[0], (const char *) cam_var->capture[0].capture_buffer, 
                    sizeof(char) * cam_var->capture[0].frame_width * 
                    cam_var->capture[0].frame_height);
            pthread_mutex_unlock(&mutex_rec);
        }
    }
    
    
/*     gpiv_free_ucmatrix(frameA); */

    if (verbose) g_warning("thread_rec_frame:: 3/3");
    pthread_exit(0);
}




static void
thread_rec_frame_serie(ImgFrame img_frame[GPIV_NIMG_MAX]
                       )
/*-----------------------------------------------------------------------------
 * capture a series of images
 */
{
    gint ibuf, cycles;

#ifdef ENABLE_TRIG
    cycles = gl_trig_par.ttime.cycles;
#else /* ENABLE_TRIG */
    cycles = gl_cam_par->cycles;
#endif /* ENABLE_TRIG */

/* g_message("thread_rec_frame_series:: 0 with id = %d", (int) pthread_self ()); */
    for (ibuf = 0; ibuf < cycles; ibuf++) {
        if (cancel_process) break;
/* BUGFIX: thread_rec_frame_serie calls  rec_frame, NOT thread_ rec_frame; correct?
   rec_frame(&img_frame[ibuf]);
   }
/* g_message("thread_rec_frame_series:: 1/1"); */
        pthread_exit(NULL);
    }
}


static void
rec_frame(ImgFrame *img_frame
          )
/*-----------------------------------------------------------------------------
/*
 * capture a single image frame or frame pair
 */
{

/* g_message("rec_frame:: 0"); */

    if (dc1394_single_capture(cam_var->handle, &cam_var->capture[0]) 
        != DC1394_SUCCESS) {
        dc1394_release_camera(cam_var->handle, &cam_var->capture[0]);
        raw1394_destroy_handle(cam_var->handle);
        g_warning("unable to capture a frame");
        return;
    }

/*
 * Copy buffer content to tmp buffer array
 */
    memcpy( img_frame->A[0], 
            (const char *) cam_var->capture[0].capture_buffer, 
            sizeof(char) * cam_var->capture[0].frame_width * 
            cam_var->capture[0].frame_height);
    
    if (gpiv_par->x_corr) {
/*
 * capture second frame
 */
        if (dc1394_single_capture(cam_var->handle, &cam_var->capture[0]) 
            != DC1394_SUCCESS) {
            dc1394_release_camera(cam_var->handle, &cam_var->capture[0]);
            raw1394_destroy_handle(cam_var->handle);
            g_warning("unable to capture a frame");
            return;
        }

        memcpy( img_frame->B[0], 
                (const char *) cam_var->capture[0].capture_buffer, 
                sizeof(char) * cam_var->capture[0].frame_width * 
                cam_var->capture[0].frame_height);   
    }
    
/* g_message("rec_frame:: 3/3"); */
}



static void
add_buffer(GpivConsole *gpiv,
           gchar *fname_base,
           gint ibuf, 
           unsigned char **frameA, 
           unsigned char **frameB
           )
/*-----------------------------------------------------------------------------
 */
{
    gchar cbuf[4], cdate[GPIV_MAX_CHARS], ctime[GPIV_MAX_CHARS];
/*     GTimeVal *my_gtime = NULL; */
    time_t ltime_t;
/*
 * Use numbers or timing to concanate after buffer names
 */


/* BUGFIX GTimeVal might be used for more accurate timing */
/*     my_gtime = g_malloc(sizeof(GTimeVal)); */
/*     g_get_current_time(my_gtime); */

    date = g_date_new ();
    g_date_set_time(date, time (NULL));
    ltime_t = time(&ltime_t);
    dac_time = localtime(&ltime_t);
    g_snprintf(ctime, sizeof(gint) * 3 + 2, 
               "%d.%d.%d_", 
               dac_time->tm_hour, 
               dac_time->tm_min, 
               dac_time->tm_sec);


    g_snprintf(cdate, sizeof(gint) * 3 + 2, 
               "%d.%d.%d_", 
               g_date_day(date), 
               g_date_month(date), 
               g_date_year(date));

    g_snprintf(cbuf, 4, "%d", ibuf);

    if (gpiv_var->fname_date
        && gpiv_var->fname_time) {
        fname_base = g_strdup(g_strconcat(gpiv_var->fn_last[0],
                                          (gchar *) cdate, 
                                          (gchar *) ctime, 
                                          (gchar *) cbuf, 
                                          NULL));
    } else if (gpiv_var->fname_date) {
        fname_base = g_strdup(g_strconcat(gpiv_var->fn_last[0],
                                          (gchar *) cdate, 
                                          (gchar *) cbuf, 
                                          NULL));
    } else if (gpiv_var->fname_time) {
        fname_base = g_strdup(g_strconcat(gpiv_var->fn_last[0],
                                          (gchar *) ctime, 
                                          (gchar *) cbuf, 
                                          NULL));
    } else {
        fname_base = g_strdup(g_strconcat(gpiv_var->fn_last[0],
                                          (gchar *) cbuf, 
                                          NULL));   
    }

    load_dac_buffer(gpiv, cam_var, FALSE, fname_base, ibuf, frameA, frameB, 
                    INSERT);
    nbufs++;

/*     g_free(my_gtime);  */
    g_free(fname_base); 
    g_date_free(date); 
    return;
}



static void
renew_buffer(GpivConsole *gpiv,
             gchar *fname_base,
             gint ibuf, 
             unsigned char **frameA, 
             unsigned char **frameB
             )
/*-----------------------------------------------------------------------------
 * updates image of buffer
 */
{
/*
 * Open buffer 0 if not exist
 */

    if (display[ibuf] == NULL) {
        add_buffer(gpiv, fname_base, ibuf, frameA, frameB);
    } else {
        display_act = display[ibuf];

        recreate_img(display_act, frameA, frameB);
        if (display_act->img->exist_img) {
            show_img1(display_act);
        } else {
            hide_img1(display_act);
        }
        
        if (gl_image_par->x_corr) {
            if (display_act->img->exist_img) {
                show_img2(display_act);
            } else {
                hide_img2(display_act);
            }
        }
        
    }


}



static void 
recreate_img(Display * disp,
             unsigned char **frameA,
             unsigned char **frameB
             )
/* ----------------------------------------------------------------------------
 * Re-creates image in gnome canvas; without memallocating of rgbbuf_img1 and
 * rgbbuf_img2
 * row stride; each row is a 4-byte buffer array
 */
{
    guchar *pos1 = NULL;
/*     guchar *pos2 = NULL; */
    gint i, j;
    GdkPixbuf *pixbuf1 = NULL;
/*     GdkPixbuf *pixbuf2 = NULL; */
    guint16 fact = 1;
    gint depth = 8;

    assert (disp != NULL);
    //assert (disp->img->img1 != NULL);
    assert (disp->img->rgbbuf_img1 != NULL);
    if (disp->img->image->header->x_corr) {
      //  assert (disp->img->img2 != NULL);
        assert (disp->img->rgbbuf_img2 != NULL);
    }
    assert (disp->img->exist_img);
    

    fact = fact << (disp->img->image->header->depth - depth);
    disp->img->rgb_img_width = disp->img->image->header->ncolumns * 3;
    while ((disp->img->rgb_img_width) % 4 != 0) {
        disp->img->rgb_img_width++;
    } 
    
    pixbuf1 = gdk_pixbuf_new_from_data(disp->img->rgbbuf_img1,
                                       GDK_COLORSPACE_RGB,
                                       FALSE,          /* gboolean has_alpha */
                                       disp->img->image->header->depth,
                                       disp->img->image->header->ncolumns,
                                       disp->img->image->header->nrows,
                                       disp->img->rgb_img_width, /* rowstride */ 
                                       NULL, 
                                       NULL);
    


    pos1 = disp->img->rgbbuf_img1;
   for (i = 0; i < disp->img->image->header->nrows; i++) {
	for (j = 0; j < disp->img->image->header->ncolumns; j++) {
	    *pos1++ =  (guchar) (frameA[i][j] / fact);
	    *pos1++ =  (guchar) (frameA[i][j] / fact);
	    *pos1++ =  (guchar) (frameA[i][j] / fact);
	}
    }


    gnome_canvas_item_set(GNOME_CANVAS_ITEM (disp->img->gci_img1),
                          "pixbuf", pixbuf1,
                          NULL);
/*     gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM (disp->img->gci_img1)); */
/*     gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (disp->img->gci_img1)); */

/* gtk_widget_pop_visual(); */
/*   gtk_widget_pop_colormap(); */


    gdk_pixbuf_unref (pixbuf1);
    
    
    
/*     if(image->header->x_corr) { */
/*         pos2 = NULL; */
        
/*         disp->img->rgbbuf_img2 = g_malloc(disp->img->rgb_img_width * 3 * */
/*                                          gpiv_par->img_height); */
        
/*         pixbuf2 = gdk_pixbuf_new_from_data(disp->img->rgbbuf_img2, */
/*                                            GDK_COLORSPACE_RGB, */
/*                                            FALSE,  */
/*                                            depth,  */
/*                                            gpiv_par->img_width,  */
/*                                            gpiv_par->img_height, */
/*                                            disp->img->rgb_img_width,  */
/*                                            NULL,  */
/*                                            NULL); */
        
/*         if (disp->img->gci_img2 != NULL) { */
/*             destroy_img(disp); */
/*         } */

/*         disp->img->gci_img2 =  */
/*             gnome_canvas_item_new( gnome_canvas_root( GNOME_CANVAS */
/*                                                       (disp->canvas)), */
/*                                    gnome_canvas_pixbuf_get_type (), */
/*                                    "pixbuf", pixbuf2, */
/*                                    NULL); */
        
        
/*         pos2 = disp->img->rgbbuf_img2; */
/*         for (i = 0; i < gpiv_par->img_height; i++) { */
/*             for (j = 0; j < gpiv_par->img_width; j++) { */
/*                 *pos2++ = (guchar) (frameB[i][j] / fact); */
/*                 *pos2++ = (guchar) (frameB[i][j] / fact); */
/*                 *pos2++ = (guchar) (frameB[i][j] / fact); */
/*             } */
/*         } */
        
/*         gdk_pixbuf_unref (pixbuf2); */
        
/*     } else { */
        disp->img->gci_img2 = disp->img->gci_img2;
/*     } */
    
}



#endif /* ENABLE_CAM */
