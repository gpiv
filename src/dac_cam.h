/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (callback) functions for dac
 * $Log: dac_cam.h,v $
 * Revision 1.3  2006-09-18 07:27:05  gerber
 * *** empty log message ***
 *
 * Revision 1.2  2005/01/31 14:41:41  gerber
 * copyrighted and cleaned-up
 *
 * Revision 1.1  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 */

#ifndef DAC_CAM_H
#define DAC_CAM_H
#ifdef ENABLE_CAM



static const char *format0_desc[NUM_FORMAT0_MODES] =
    {
        "160x120 YUV (4:4:4)",
        "320x240 YUV (4:2:2)",
        "640x480 YUV (4:1:1)",
        "640x480 YUV (4:2:2)",
        "640x480 RGB (24bpp)",
        "640x480 Mono (8bpp)",
        "640x480 Mono (16bpp)"
    };

static const char *format1_desc[NUM_FORMAT1_MODES] =
    {
        "800x600 (YUV)",
        "800x600 (RGB)",
        "800x600 (MONO)",
        "1024x768 (YUV422)",
        "1024x768 (RGB)",
        "1024x768 (MONO)",
        "800x600 (MONO16)",
        "1024x768 (MONO16)"
    };

static const char *format2_desc[NUM_FORMAT2_MODES] =
    {
        "1280x960 (YUV)",
        "1280x960 (RGB)",
        "1280x960 (MONO)",
        "1600x1200 (YUV422)",
        "1600x1200 (RGB)",
        "1600x1200 (MONO)",
        "1280x960 (MONO16)",
        "1600x1200 (MONO16)"
    };

static const char *format6_desc[NUM_FORMAT6_MODES] =
    {
        "EXIF"
    };

static const char *format7_desc[NUM_MODE_FORMAT7] =
    {
        "FORMAT7 (0)",
        "FORMAT7 (1)",
        "FORMAT7 (2)",
        "FORMAT7 (3)",
        "FORMAT7 (4)",
        "FORMAT7 (5)",
        "FORMAT7 (6)",
        "FORMAT7 (7)"
    };

static const char *color_format7_desc[NUM_COLOR_FORMAT7] =
    {
        "FORMAT7 (MONO8)",
        "FORMAT7 (YUV411)",
        "FORMAT7 (YUV422)",
        "FORMAT7 (YUV444)",
        "FORMAT7 (RGB8)",
        "FORMAT7 (MONO16)",
        "FORMAT7 (RGB16"
    };





static const char *trigger_mode_desc[NUM_TRIGGER_MODE] =
    {
        "TRIGGER MODE 0",
        "TRIGGER MODE 1",
        "TRIGGER MODE 2",
        "TRIGGER MODE 3"
    };


static const char *format_desc[NUM_FORMATS] =
    {
    "FORMAT_VGA_NONCOMPRESSED",
    "FORMAT_SVGA_NONCOMPRESSED_1",
    "FORMAT_SVGA_NONCOMPRESSED_2",
    "RESERVED 1",
    "RESERVED 2",
    "RESERVED 3",
    "FORMAT_STILL_IMAGE= 390",
    "FORMAT_SCALABLE_IMAGE_SIZE"
    };

/*
 * Already declared in libdc1394.h
 */
/*
const char *dc1394_feature_desc[NUM_FEATURES] =
  {
    "Brightness",
    "Exposure",
    "Sharpness",
    "White Balance",
    "Hue",
    "Saturation",
    "Gamma",
    "Shutter",
    "Gain",
    "Iris",
    "Focus",
    "Temperature",
    "Trigger",
    "Zoom",
    "Pan",
    "Tilt",
    "Optical Filter",
    "Capture Size",
    "Capture Quality"
  };
*/


static const char * fps_label_list[NUM_FRAMERATES] = {
  "1.875 fps",
  "3.75 fps",
  "7.5 fps",
  "15 fps",
  "30 fps",
  "60 fps"
};

/*
 * Global functions
 */

void
exec_cam_start(GpivConsole *gpiv
               );

void
exec_cam_stop(void
              );

/*
 * Callback functions
 */

void
on_menu_camera_select(GtkWidget *widget, 
                          gpointer data);

void
on_menu_format(GtkWidget *widget, 
               gpointer data);

void
on_menu_fps(GtkWidget *widget, 
               gpointer data);

void
on_trigger_external_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_trigger_polarity_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_trigger_mode_activate(GtkWidget *widget, 
                         gpointer data);

#ifdef ENABLE_TRIG
void
on_checkbutton_camera_trigger_enter(GtkWidget *widget, 
                                    gpointer data);

void
on_checkbutton_camera_trigger(GtkWidget *widget, 
                              gpointer data);
#endif /* ENABLE_TRIG */

void
on_man_auto_menu(GtkWidget *widget, 
                     gpointer data);

void
on_scale_changed(GtkAdjustment *adj, 
         gpointer user_data);

void 
on_button_dac_camstart_enter(GtkWidget *widget, 
                             gpointer data);

void
on_button_dac_camstart(GtkWidget *widget, 
		  gpointer data);

void 
on_button_dac_camstop_enter(GtkWidget *widget, 
                    gpointer data);

void
on_button_dac_camstop(GtkWidget *widget, 
		  gpointer data);

#endif /* ENABLE_CAM */
#endif /* DAC_CAM_H */
