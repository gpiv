/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Data acquisition header interface
 * $Log: dac_cam_interface.c,v $
 * Revision 1.3  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.2  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.1  2006-09-18 07:29:50  gerber
 * Split up of triggering and image recording (camera)
 *
 */
#ifdef ENABLE_CAM
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gpiv_gui.h"
#include "dac_cam_interfac.h"
#include "dac_cam.h"


static void
create_camera_menu(GnomeApp *main_window,
                   Cam *cam
                   )
/*-----------------------------------------------------------------------------
 * From coriander: build_menus.c BuildCameraMenu
 */
{
    int i;
    char tmp[GPIV_MAX_CHARS];

/*
 * remove previous menu
 */
    if (cam->camera_select != NULL) {
        gtk_widget_destroy(GTK_WIDGET (cam->camera_select)); 
    }

    cam->camera_select = gtk_option_menu_new ();
    gtk_widget_ref (cam->camera_select);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
                              "camera_select", 
                              cam->camera_select,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (cam->camera_select);
    gtk_container_add(GTK_CONTAINER (cam->vbox_cam),
		      cam->camera_select);

    if (cam_var.numCameras >= 1) {
        gtk_widget_set_sensitive (cam->camera_select, TRUE);
    } else {
        gtk_widget_set_sensitive (cam->camera_select, FALSE );
    }



    cam->camera_select_menu = gtk_menu_new ();
    if (cam_var.numCameras > 0) {
        for (i = 0; i < cam_var.numCameras; i++) {
            g_snprintf(tmp,
                       GPIV_MAX_CHARS, 
                       "Node %d: %s %s",
                       cam_var.camera[i].id, 
                       cam_var.camera[i].vendor, 
                       cam_var.camera[i].model);
            cam->menu_item = gtk_menu_item_new_with_label (_(tmp));
            gtk_widget_show (cam->menu_item);
            gtk_menu_append (GTK_MENU (cam->camera_select_menu), 
                             cam->menu_item);
            gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->camera_select), 
                                      cam->camera_select_menu);
            g_signal_connect (GTK_OBJECT (cam->menu_item), 
                                "activate",
                                GTK_SIGNAL_FUNC (on_menu_camera_select),
                                (int *) i);
/*
 * BUGFIX apply last camera to img_par.source
 */
            g_snprintf(image_par.source, GPIV_MAX_CHARS, "%s %s", 
                       cam_var.camera[i].vendor,
                       cam_var.camera[i].model);
            image_par.source__set = TRUE;
        }
    } else {
        g_snprintf(tmp,
                   GPIV_MAX_CHARS, 
                   "No camera");
        cam->menu_item = gtk_menu_item_new_with_label (_(tmp));
        gtk_widget_show (cam->menu_item);
        gtk_menu_append (GTK_MENU (cam->camera_select_menu), 
                         cam->menu_item);
        gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->camera_select), 
                                  cam->camera_select_menu);

        g_snprintf(image_par.source, GPIV_MAX_CHARS, "No camera");
        image_par.source__set = TRUE;
    }



/*     gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->camera_select),  */
/*                               cam->camera_select_menu); */

/*
 * sets the active menu item:
 */
    gtk_option_menu_set_history (GTK_OPTION_MENU (cam->camera_select) , 
                                 /* camera_act */ 0);

}



static void
create_format_menu(GnomeApp *main_window,
                   GpivConsole * gpiv,
                   Cam *cam
                   )
/*-----------------------------------------------------------------------------
 * From coriander: build_menus.c BuildCameraMenu
 */
{
    int i;
    char tmp[GPIV_MAX_CHARS];
    quadlet_t modes, formats;

    if (cam_var.numCameras > 0) {
        dc1394_query_supported_formats(cam_var.camera[0].handle, 
                                       cam_var.camera[0].id, 
                                       &formats);
        dc1394_query_supported_modes(cam_var.camera[0].handle, 
                                     cam_var.camera[0].id, 
                                     FORMAT_VGA_NONCOMPRESSED, 
                                     &modes);
    }
/*
 * remove previous menu
 */
    if (cam->format_menu != NULL) {
        gtk_widget_destroy(GTK_WIDGET (cam->format_menu)); 
    }
    
    cam->format_menu = gtk_option_menu_new ();
    gtk_widget_ref (cam->format_menu);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
                              "format_menu", 
                              cam->format_menu,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (cam->format_menu);
    gtk_container_add(GTK_CONTAINER (cam->vbox_cam),
		      cam->format_menu);
    
    
    cam->format_menu_menu = gtk_menu_new ();
    if (cam_var.numCameras > 0) {
/*
 * Format 0
 */
        if (formats & (0x1 << 31)) {
            for (i = 0; i < NUM_FORMAT0_MODES; i++) {
                if ( modes & (0x1 << (31-i))) {
                    g_snprintf(tmp,
                               GPIV_MAX_CHARS, 
                               "Format 0: %s", 
                               format0_desc[i]);
                    cam->menu_item = gtk_menu_item_new_with_label (_(tmp));
                    gtk_widget_show (cam->menu_item);
                    gtk_menu_append (GTK_MENU (cam->format_menu_menu), 
                                     cam->menu_item);
                    gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->format_menu), 
                                              cam->format_menu_menu);
                    
                    gtk_widget_set_sensitive(cam->menu_item,
                                             modes & (0x1 << (31-i)) );
                    
                    
                    gtk_object_set_data(GTK_OBJECT (cam->menu_item),
                                        "gpiv",
                                        gpiv);
                    g_signal_connect (GTK_OBJECT (cam->menu_item), 
                                        "activate",
                                        GTK_SIGNAL_FUNC (on_menu_format),
                                        (int*)(i + MODE_FORMAT0_MIN));
                }
            }
        }
        

/*
 * Format 1
 */
        if (formats & (0x1 << 30)) {
            for (i = 0; i < NUM_FORMAT1_MODES; i++) {
                if ( modes & (0x1 << (30-i))) {
                    g_snprintf(tmp,
                               GPIV_MAX_CHARS, 
                               "Format 1: %s", 
                               format1_desc[i]);
                    cam->menu_item = gtk_menu_item_new_with_label (_(tmp));
                    gtk_widget_show (cam->menu_item);
                    gtk_menu_append (GTK_MENU (cam->format_menu_menu), 
                                     cam->menu_item);
                    gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->format_menu), 
                                              cam->format_menu_menu);
                    
                    gtk_widget_set_sensitive(cam->menu_item,
                                             modes & (0x1 << (30-i)) );
                    g_signal_connect (GTK_OBJECT (cam->menu_item), 
                                        "activate",
                                        GTK_SIGNAL_FUNC (on_menu_format),
                                        (int*)(i + MODE_FORMAT1_MIN));
                }
            }
        }



/*
 * Format 2
 */
        if (formats & (0x1 << 29)) {
            for (i = 0; i < NUM_FORMAT2_MODES; i++) {
                if ( modes & (0x1 << (29-i))) {
                    g_snprintf(tmp,
                               GPIV_MAX_CHARS, 
                               "Format 2: %s", 
                               format2_desc[i]);
                    cam->menu_item = gtk_menu_item_new_with_label (_(tmp));
                    gtk_widget_show (cam->menu_item);
                    gtk_menu_append (GTK_MENU (cam->format_menu_menu), 
                                     cam->menu_item);
                    gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->format_menu), 
                                              cam->format_menu_menu);
                    
                    gtk_widget_set_sensitive(cam->menu_item,
                                             modes & (0x1 << (29-i)) );
                    
                    g_signal_connect (GTK_OBJECT (cam->menu_item), 
                                        "activate",
                                        GTK_SIGNAL_FUNC (on_menu_format),
                                        (int*)(i + MODE_FORMAT2_MIN));
                }
            }
        }


/*
 * Format 6
 */
        if (formats & (0x1 << 25)) {
            for (i = 0; i < NUM_FORMAT6_MODES; i++) {
                if ( modes & (0x1 << (25-i))) {
                    g_snprintf(tmp,
                               GPIV_MAX_CHARS, 
                               "Format 6: %s", 
                               format6_desc[i]);
                    cam->menu_item = gtk_menu_item_new_with_label (_(tmp));
                    gtk_widget_show (cam->menu_item);
                    gtk_menu_append (GTK_MENU (cam->format_menu_menu), 
                                     cam->menu_item);
                    gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->format_menu), 
                                              cam->format_menu_menu);
                    
                    gtk_widget_set_sensitive(cam->menu_item,
                                             modes & (0x1 << (25-i)) );
                    
                    g_signal_connect (GTK_OBJECT (cam->menu_item), 
                                        "activate",
                                        GTK_SIGNAL_FUNC (on_menu_format),
                                        (int*)(i + MODE_FORMAT6_MIN));
                } 
           }
        }


    } else {
        g_snprintf(tmp,
                   GPIV_MAX_CHARS, 
                   "No Formats");
        cam->menu_item = gtk_menu_item_new_with_label (_(tmp));
        gtk_widget_show (cam->menu_item);
        gtk_menu_append (GTK_MENU (cam->format_menu_menu), 
                         cam->menu_item);
        gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->format_menu), 
                                  cam->format_menu_menu);
    }
    
    
    
/*     gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->format_menu),  */
/*                               cam->format_menu_menu); */

/*
 * sets the active menu item:
 */
    gtk_option_menu_set_history (GTK_OPTION_MENU (cam->format_menu) , 
                                 /* camera_act */ 0);


}




static void
create_fps_menu(GnomeApp *main_window,
                   Cam *cam
                   )
/*-----------------------------------------------------------------------------
 * From coriander: build_menus.c BuildCameraMenu
 */
{
    dc1394bool_t cont=DC1394_TRUE;
    gint new_framerate = 0;
    gint index[NUM_FRAMERATES];

    gint i, f, k = 0, err;
    gchar tmp[GPIV_MAX_CHARS];
    quadlet_t value;

    if (cam_var.numCameras > 0) {
        if (cam_var.misc_info[0].format == FORMAT_SCALABLE_IMAGE_SIZE) {
            value = 0; /* format 7 has no fixed framerates */
        } else {
            err=dc1394_query_supported_framerates(cam_var.camera[0].handle, 
                                                  cam_var.camera[0].id, 
                                                  cam_var.misc_info[0].format, 
                                                  cam_var.misc_info[0].mode, 
                                                  &value);
            if (!err) g_warning("Could not query supported framerates");
        }
    }


/*
 * remove previous menu
 */
    if (cam->fps_menu != NULL) {
        gtk_widget_destroy(GTK_WIDGET (cam->fps_menu)); 
    }
    
    cam->fps_menu = gtk_option_menu_new ();
    gtk_widget_ref (cam->fps_menu);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
                              "fps_menu", 
                              cam->fps_menu,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (cam->fps_menu);
    gtk_container_add(GTK_CONTAINER (cam->vbox_cam),
		      cam->fps_menu);
    
    
    cam->fps_menu_menu = gtk_menu_new ();
    if (cam_var.numCameras > 0) {
        k = 0;
        for (f=FRAMERATE_MIN, i=0; f <= FRAMERATE_MAX; i++, f++) {
/*
 * 31 to 31-num_framerates
 */
            if (value & (0x1 << (31-i) ) ) { 
                index[i]=k;
                k++;
                cam->menu_item = gtk_menu_item_new_with_label (_(fps_label_list[i]));
                gtk_widget_show (cam->menu_item);
                gtk_menu_append (GTK_MENU (cam->fps_menu_menu), 
                                 cam->menu_item);
                
/*                 gtk_object_set_data(GTK_OBJECT (cam->menu_item), */
/*                                     "gpiv", */
/*                                     gpiv); */
                g_signal_connect (GTK_OBJECT (cam->menu_item), 
                                    "activate",
                                    GTK_SIGNAL_FUNC (on_menu_fps),
/*                                     (int*)(i + MODE_FPS0_MIN)); */
                                    (int*)f);
            }
            else
                index[i] = -1; // framerate not supported
        }
        gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->fps_menu), 
                                  cam->fps_menu_menu);
        
        if (index[cam_var.misc_info[0].framerate-FRAMERATE_MIN] < 0 ) {
// previously selected framerate unsupported!!
// automatically switch to nearest fps available
            for (i = 1; i <= ((NUM_FRAMERATES>>1) +1); i++) {
// search radius is num_framerates/2 +1 for safe rounding     
                if (((cam_var.misc_info[0].framerate-FRAMERATE_MIN-i)>=0) && cont) {
                    if (index[cam_var.misc_info[0].framerate - FRAMERATE_MIN-i]
                        >= 0) {
// try down
                        new_framerate=cam_var.misc_info[0].framerate - i;
                        cont=DC1394_FALSE;
                    }
                }
                if (((cam_var.misc_info[0].framerate - FRAMERATE_MIN + i)
                     < NUM_FRAMERATES) 
                    && cont) {
                    if (index[cam_var.misc_info[0].framerate - FRAMERATE_MIN + i]
                        >=0) {
// try up
                        new_framerate=cam_var.misc_info[0].framerate + i;
                        cont=DC1394_FALSE;
                    }
                }
            }
            snprintf(tmp, GPIV_MAX_CHARS, 
                     "create_fps_menu: Invalid framerate. Updating to nearest: %s\n",
                     fps_label_list[new_framerate-FRAMERATE_MIN]);
            warning_gpiv(tmp);
            err = dc1394_set_video_framerate(cam_var.camera[0].handle,
                                             cam_var.camera[0].id,
                                             new_framerate);
            if (!err)
                warning_gpiv("create_fps_menu: Cannot set video framerate");
            cam_var.misc_info[0].framerate = new_framerate;
        }
        gtk_option_menu_set_history (GTK_OPTION_MENU (cam->fps_menu), 
                                     index[cam_var.misc_info[0].framerate -
                                           FRAMERATE_MIN]);
        
    }    
}





static void
create_trigger_mode(GnomeApp *main_window,
                   Cam *cam
                   )
/*-----------------------------------------------------------------------------
 * From coriander: build_menus.c BuildCameraMenu
 */
{
    gint err, i, f, modes = 0;
    quadlet_t value;
    gint k=0;
    gint index[NUM_TRIGGER_MODE];


    if (cam_var.numCameras > 0) {
        err = dc1394_query_feature_characteristics(cam_var.camera[0].handle,
                                                   cam_var.camera[0].id,
                                                   FEATURE_TRIGGER,
                                                   &value);
        if (!err) g_warning("Could not query trigger feature characteristics");
        modes = ( (value & (0xF << 12))>>12 );
    }



/*
 * remove previous menu
 */
    if (cam->trigger_mode != NULL) {
        gtk_widget_destroy(GTK_WIDGET (cam->trigger_mode)); 
    }
    
    cam->trigger_mode = gtk_option_menu_new ();
    gtk_widget_ref (cam->trigger_mode);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
                              "trigger_mode", 
                              cam->trigger_mode,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (cam->trigger_mode);
    gtk_container_add(GTK_CONTAINER (cam->vbox_cam),
		      cam->trigger_mode);
    
    
    cam->trigger_mode_menu = gtk_menu_new ();
    if (cam_var.numCameras > 0) {
        if (modes) {
/*
 * at least one mode present
 * external trigger available:
 */
            for (f = TRIGGER_MODE_MIN, i = 0; f <= TRIGGER_MODE_MAX; i++, f++) {
                if (modes & (0x1<<(TRIGGER_MODE_MAX-f))) {
                    index[i]=k;
                    k++;
                    cam->menu_item = 
                        gtk_menu_item_new_with_label (_(trigger_mode_desc[i]));
                    gtk_widget_show (cam->menu_item);
                    gtk_menu_append (GTK_MENU (cam->trigger_mode_menu), 
                                     cam->menu_item);
	      g_signal_connect (GTK_OBJECT (cam->menu_item), 
                                  "activate",
				  GTK_SIGNAL_FUNC (on_trigger_mode_activate),
				  (int*) f);
                }
                else
                    index[i]=0;
            }
        }
        gtk_option_menu_set_menu (GTK_OPTION_MENU (cam->trigger_mode), 
                                  cam->trigger_mode_menu);
/*
 * sets the active menu item:
 */
        gtk_option_menu_set_history (GTK_OPTION_MENU (cam->trigger_mode), 
                                     index[cam_var.feature_set[0].feature
                                           [FEATURE_TRIGGER - FEATURE_MIN]
                                           .trigger_mode]);
    }

}



static void
create_camera_feature_menu_scale(GnomeApp *main_window,
                                 Cam *cam,
                                 gint FEATURE,
                                 gchar *description,
                                 gint row,
                                 
                                 GtkWidget *option,
                                 GtkWidget *option_menu,
                                 GtkWidget *menu_item_option_man,
                                 GtkWidget *menu_item_option_auto,
                                 GtkWidget *menu_item_option_na,
                                 GtkObject *adj_scale,
                                 GtkWidget *scale
                                 )
/*-----------------------------------------------------------------------------
 */
{
    GtkWidget *label;
    gint col = 0;

    label = gtk_label_new(_(description));
    gtk_widget_ref(label);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "label",
			     label,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(label);
    


     if (cam_var.numCameras > 0
         && cam_var.feature_set[0].feature[FEATURE - FEATURE_MIN].
         readout_capable) {
         adj_scale =
             gtk_adjustment_new(0,
                                cam_var.feature_set[0].
                                feature[FEATURE - FEATURE_MIN].min,
                                cam_var.feature_set[0].
                                feature[FEATURE - FEATURE_MIN].max,
                                1,
                                10,
                                0);
     } else {
         adj_scale = 
             gtk_adjustment_new(0,
                                0, 
                                255, 
                                1,
                                10,
                                0);
     }
     scale = gtk_hscale_new (GTK_ADJUSTMENT 
                                           (adj_scale));
     gtk_widget_ref (scale);
     gtk_object_set_data_full (GTK_OBJECT (main_window), 
                               "scale", 
                               scale,
                               (GtkDestroyNotify) gtk_widget_unref);
     gtk_widget_show (scale);
     gtk_widget_set_sensitive (scale, 
                               FALSE);
     gtk_scale_set_digits (GTK_SCALE (scale), 
                           0);
     
     if (cam_var.numCameras > 0) {
         if (cam_var.feature_set[0].feature[FEATURE - FEATURE_MIN].
         readout_capable) {
             gtk_adjustment_set_value(GTK_ADJUSTMENT (adj_scale),
                                      cam_var.feature_set[0].
                                      feature[FEATURE - FEATURE_MIN].value);
         }
         
         if (cam_var.feature_set[0].feature[FEATURE - FEATURE_MIN].
             manual_capable) {
/*
 * set glider active and connect
*/
             gtk_widget_set_sensitive (scale, TRUE);
             g_signal_connect (GTK_OBJECT (adj_scale), 
                                 "value_changed", 
                                 GTK_SIGNAL_FUNC (on_scale_changed),
                                (int*) FEATURE);
         }
     }



    option = gtk_option_menu_new ();
    gtk_widget_ref (option);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
                              "option", 
                              option,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (option);
    gtk_widget_set_sensitive (option, FALSE);
/*     gtk_object_set_data(GTK_OBJECT (option), */
/* 			"dac",  */
/* 			dac); */
  


    option_menu = gtk_menu_new ();
    if (cam_var.numCameras > 0) {
        if (cam_var.feature_set[0].feature[FEATURE - FEATURE_MIN].
            manual_capable) {
/* 
 * add menu man
 */
            gtk_widget_set_sensitive (option, 
                                      TRUE);
            menu_item_option_man = 
                gtk_menu_item_new_with_label (_("Man"));
            gtk_widget_show (menu_item_option_man);
            gtk_menu_append (GTK_MENU (option_menu), 
                             menu_item_option_man);
            gtk_object_set_data(GTK_OBJECT (menu_item_option_man),
                                "scale", 
                                scale);
            gtk_object_set_data(GTK_OBJECT (menu_item_option_man),
                                "var_type", 
                                "0");
            g_signal_connect (GTK_OBJECT (menu_item_option_man), 
                                "activate",
                                GTK_SIGNAL_FUNC (on_man_auto_menu),
                                (int*) FEATURE);
        }    
        
        if (cam_var.feature_set[0].feature[FEATURE - FEATURE_MIN].
            auto_capable) {
/*
 * add menu auto
 */
            gtk_widget_set_sensitive (option, TRUE);
            menu_item_option_auto = gtk_menu_item_new_with_label (_("Auto"));
            gtk_widget_show (menu_item_option_auto);
            gtk_menu_append (GTK_MENU (option_menu), 
                             menu_item_option_auto);
            gtk_object_set_data(GTK_OBJECT (menu_item_option_auto),
                                "scale", 
                                scale);
            gtk_object_set_data(GTK_OBJECT (menu_item_option_auto),
                                "var_type", 
                                "1");
            g_signal_connect (GTK_OBJECT (menu_item_option_auto), 
                                "activate",
                                GTK_SIGNAL_FUNC (on_man_auto_menu),
                                (int*) FEATURE);
        }
/*
 * add menu non avail.
 */
        if (!cam_var.feature_set[0].feature[FEATURE - FEATURE_MIN].auto_capable
            && !cam_var.feature_set[0].feature[FEATURE - FEATURE_MIN].
            manual_capable
            ) {
            gtk_widget_set_sensitive (option, 
                                      FALSE);
            menu_item_option_na = gtk_menu_item_new_with_label (_("N/A"));
            gtk_widget_show (menu_item_option_na);
            gtk_menu_append (GTK_MENU (option_menu), 
                             menu_item_option_na);
        }
     }
     gtk_option_menu_set_menu (GTK_OPTION_MENU (option), 
                               option_menu);




    gtk_table_attach(GTK_TABLE(cam->table_cam),
		     label, 
                     col, 
                     col + 1, 
                     row, 
                     row + 1,
		     (GtkAttachOptions) 0,
		     (GtkAttachOptions) 0,
		     0,
		     0);



    gtk_table_attach(GTK_TABLE(cam->table_cam),
		     option, 
                     col + 1, 
                     col + 2, 
                     row, 
                     row + 1,
		     (GtkAttachOptions) 0,
		     (GtkAttachOptions) (0),
		     0,
		     0);


     gtk_table_attach(GTK_TABLE(cam->table_cam),
                      scale, 
                      col + 2, 
                      col + 3, 
                      row, 
                      row + 1,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL),
                      0,
                      0);
}

Cam *
create_cam(GnomeApp *main_window, 
	   GtkWidget *container
	   )
/*-----------------------------------------------------------------------------
 * Image Info window with data from header
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT (main_window), "gpiv");
    Cam * cam = g_new0(Cam, 1);


    dac->cam->frame_cam = gtk_frame_new(_("Camera"));
    gtk_widget_ref(dac->cam->frame_cam);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "dac_frame_cam",
			     dac->cam->frame_cam,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->frame_cam);
    gtk_table_attach(GTK_TABLE(dac->table),
		     dac->cam->frame_cam, 
		     1, 
		     2, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    dac->cam->vbox_cam = gtk_vbox_new(FALSE,
                                 0);
    gtk_widget_ref(dac->cam->vbox_cam);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "vbox_cam",
			     dac->cam->vbox_cam,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->vbox_cam);
    gtk_container_add(GTK_CONTAINER (dac->cam->frame_cam),
		      dac->cam->vbox_cam);
/*     gtk_table_attach(GTK_TABLE(dac->cam->table_cam), */
/* 		     dac->cam->vbox_cam, */
/* 		     0, */
/* 		     3, */
/* 		     6, */
/* 		     7, */
/* 		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), */
/* 		     (GtkAttachOptions) 0, */
/* 		     0, */
/* 		     0); */

/*     gtk_container_add(GTK_CONTAINER (hbox), */
/* 		      label); */

/*
 * Camera select
 */
/*
 * Camera Shutter menu's and adjuster
 */

    create_camera_menu(main_window, 
                       dac->cam);

    create_format_menu(main_window, 
                       gpiv,
                       dac->cam);

    create_fps_menu(main_window, 
                    dac->cam);




/*     dac->cam->frame_trigger = gtk_frame_new(_("Trigger")); */
/*     gtk_widget_ref(dac->cam->frame_trigger); */
/*     gtk_object_set_data_full(GTK_OBJECT (main_window), */
/* 			     "dac_frame_trigger", */
/* 			     dac->cam->frame_trigger, */
/* 			     (GtkDestroyNotify) gtk_widget_unref); */
/*     gtk_widget_show(dac->cam->frame_trigger); */
/*     gtk_container_add(GTK_CONTAINER (dac->cam->vbox_cam), */
/* 		      dac->cam->frame_trigger); */




/*     dac->cam->vbox_trigger = gtk_vbox_new(FALSE, */
/*                                      0); */
/*     gtk_widget_ref(dac->cam->vbox_trigger); */
/*     gtk_object_set_data_full(GTK_OBJECT (main_window), */
/* 			     "vbox_trigger", */
/* 			     dac->cam->vbox_trigger, */
/* 			     (GtkDestroyNotify) gtk_widget_unref); */
/*     gtk_widget_show(dac->cam->vbox_trigger); */
/*     gtk_container_add(GTK_CONTAINER (dac->cam->frame_trigger), */
/* 		      dac->cam->vbox_trigger); */




    dac->cam->hbox_trigger = gtk_hbox_new(FALSE,
                                     0);
    gtk_widget_ref(dac->cam->hbox_trigger);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "hbox_trigger",
			     dac->cam->hbox_trigger,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->hbox_trigger);
    gtk_container_add(GTK_CONTAINER (dac->cam->vbox_cam),
		      dac->cam->hbox_trigger);




    dac->cam->trigger_external = gtk_toggle_button_new_with_label (_("External trigger"));
    gtk_widget_ref (dac->cam->trigger_external);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
                              "trigger_external", 
                              dac->cam->trigger_external,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (dac->cam->trigger_external);
    gtk_container_add(GTK_CONTAINER (dac->cam->hbox_trigger),
		      dac->cam->trigger_external);
    gtk_widget_set_sensitive (dac->cam->trigger_external, 
                            FALSE);
    g_signal_connect (GTK_OBJECT (dac->cam->trigger_external), "toggled",
                      GTK_SIGNAL_FUNC (on_trigger_external_toggled),
                      NULL);




  dac->cam->trigger_polarity = gtk_toggle_button_new_with_label (_("Polarity"));
  gtk_widget_ref (dac->cam->trigger_polarity);
  gtk_object_set_data_full (GTK_OBJECT (main_window), 
                            "trigger_polarity", 
                            dac->cam->trigger_polarity,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (dac->cam->trigger_polarity);
  gtk_container_add(GTK_CONTAINER (dac->cam->hbox_trigger),
		      dac->cam->trigger_polarity);
  gtk_widget_set_sensitive (dac->cam->trigger_polarity, 
                            FALSE);
  g_signal_connect (GTK_OBJECT (dac->cam->trigger_polarity), "toggled",
                      GTK_SIGNAL_FUNC (on_trigger_polarity_toggled),
                      NULL);



  create_trigger_mode(main_window, 
                      dac->cam);
/*
 * Enable triggering with the cam
 */
#ifdef ENABLE_TRIG
    dac->cam->checkbutton_camera_trigger = 
        gtk_check_button_new_with_label(_("RTAI triggering"));
    gtk_widget_ref(dac->cam->checkbutton_camera_trigger);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "button_camera_trigger",
                             dac->cam->checkbutton_camera_trigger,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->checkbutton_camera_trigger);
    gtk_container_add(GTK_CONTAINER (dac->cam->vbox_cam),
		      dac->cam->checkbutton_camera_trigger);
    gtk_tooltips_set_tip(gpiv->tooltips,
                         dac->cam->checkbutton_camera_trigger,
 			 _("Enables triggering of the camera and laser. "
                           "Else, the camera will 'free run', mostly at "
                           "30 frames per second."),
                         NULL);

    gtk_object_set_data(GTK_OBJECT (dac->cam->checkbutton_camera_trigger), 
			"gpiv",
                        gpiv);
    g_signal_connect(GTK_OBJECT (dac->cam->checkbutton_camera_trigger), 
                       "enter",
		       G_CALLBACK (on_checkbutton_camera_trigger_enter),
                       NULL);
    g_signal_connect(GTK_OBJECT (dac->cam->checkbutton_camera_trigger), 
                       "leave",
		       G_CALLBACK (on_widget_leave),
                       NULL);
    g_signal_connect(GTK_OBJECT (dac->cam->checkbutton_camera_trigger), 
                       "clicked",
		       G_CALLBACK (on_checkbutton_camera_trigger),
                       NULL);
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (dac->cam->checkbutton_camera_trigger),
                                gpiv_par.trigger_cam);
#endif /* ENABLE_TRIG */



    dac->cam->table_cam = gtk_table_new(9,
                                   2,
                                  FALSE);
    gtk_widget_ref(dac->cam->table_cam);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "dac_table_cam",
			     dac->cam->table_cam,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->table_cam);
    gtk_container_add(GTK_CONTAINER (dac->cam->vbox_cam),
		      dac->cam->table_cam);



/*
 * Report Camera temperature
 */



    dac->cam->label_label_temp = gtk_label_new(_("temp (C): "));
    gtk_widget_ref(dac->cam->label_label_temp);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "dac_label_label_temp",
			     dac->cam->label_label_temp,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->label_label_temp);
    gtk_table_attach(GTK_TABLE(dac->cam->table_cam),
		     dac->cam->label_label_temp,
		     0,
		     3,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) 0,
		     0,
		     0);




    dac->cam->label_temp = gtk_label_new("");
    gtk_widget_ref(dac->cam->label_temp);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "dac_label_temp",
			     dac->cam->label_temp,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->label_temp);
    gtk_table_attach(GTK_TABLE(dac->cam->table_cam),
		     dac->cam->label_temp,
		     2,
		     3,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) 0,
		     0,
		     0);

/*     g_warning("cr_dac:: feature T; %s", */
/*               dc1394_feature_desc[FEATURE_TEMPERATURE - FEATURE_MIN]); */



/*
 * Camera Exposure menu's and adjuster
 *
 * First define the scale as its data are set for the menu items
 */
    create_camera_feature_menu_scale(main_window,
                                     dac->cam,
                                     FEATURE_EXPOSURE,
                                     "Exp:",
                                     /* row */ 2,
                                     dac->cam->camera_exposure,
                                     dac->cam->camera_exposure_menu,
                                     dac->cam->menu_item_camera_exposure_man,
                                     dac->cam->menu_item_camera_exposure_auto,
                                     dac->cam->menu_item_camera_exposure_na,
                                     dac->cam->adj_exposure_scale,
                                     dac->cam->exposure_scale
                                     );
/*
 * Camera Iris menu's and adjuster
 */
    create_camera_feature_menu_scale(main_window,
                                     dac->cam,
                                     FEATURE_IRIS,
                                     "Iris:",
                                     3,
                                     dac->cam->camera_iris,
                                     dac->cam->camera_iris_menu,
                                     dac->cam->menu_item_camera_iris_man,
                                     dac->cam->menu_item_camera_iris_auto,
                                     dac->cam->menu_item_camera_iris_na,
                                     dac->cam->adj_iris_scale,
                                     dac->cam->iris_scale
                                     );

/*
 * Camera Shutter menu's and adjuster
 */
    create_camera_feature_menu_scale(main_window,
                                     dac->cam,
                                     FEATURE_SHUTTER,
                                     "Shut:",
                                     4,
                                     dac->cam->camera_shutter,
                                     dac->cam->camera_shutter_menu,
                                     dac->cam->menu_item_camera_shutter_man,
                                     dac->cam->menu_item_camera_shutter_auto,
                                     dac->cam->menu_item_camera_shutter_na,
                                     dac->cam->adj_shutter_scale,
                                     dac->cam->shutter_scale
                                     );

/*
 * Camera Gain menu's and adjuster
 */
    create_camera_feature_menu_scale(main_window,
                                     dac->cam,
                                     FEATURE_GAIN,
                                     "Gain:",
                                     5,
                                     dac->cam->camera_gain,
                                     dac->cam->camera_gain_menu,
                                     dac->cam->menu_item_camera_gain_man,
                                     dac->cam->menu_item_camera_gain_auto,
                                     dac->cam->menu_item_camera_gain_na,
                                     dac->cam->adj_gain_scale,
                                     dac->cam->gain_scale
                                     );
    
/*
 * Camera Temperature menu's and adjuster
 */
    create_camera_feature_menu_scale(main_window,
                                     dac->cam,
                                     FEATURE_TEMPERATURE,
                                     "Temp:",
                                     6,
                                     dac->cam->camera_temp,
                                     dac->cam->camera_temp_menu,
                                     dac->cam->menu_item_camera_temp_man,
                                     dac->cam->menu_item_camera_temp_auto,
                                     dac->cam->menu_item_camera_temp_na,
                                     dac->cam->adj_temp_scale,
                                     dac->cam->temp_scale
                                     );

/*
 * Camera Zoom menu's and adjuster
 */
    create_camera_feature_menu_scale(main_window,
                                     dac->cam,
                                     FEATURE_ZOOM,
                                     "Zoom:",
                                     7,
                                     dac->cam->camera_zoom,
                                     dac->cam->camera_zoom_menu,
                                     dac->cam->menu_item_camera_zoom_man,
                                     dac->cam->menu_item_camera_zoom_auto,
                                     dac->cam->menu_item_camera_zoom_na,
                                     dac->cam->adj_zoom_scale,
                                     dac->cam->zoom_scale
                                     );

/*
 * Camera Pan menu's and adjuster
 */
    create_camera_feature_menu_scale(main_window,
                                     dac->cam,
                                     FEATURE_PAN,
                                     "Pan:",
                                     8,
                                     dac->cam->camera_pan,
                                     dac->cam->camera_pan_menu,
                                     dac->cam->menu_item_camera_pan_man,
                                     dac->cam->menu_item_camera_pan_auto,
                                     dac->cam->menu_item_camera_pan_na,
                                     dac->cam->adj_pan_scale,
                                     dac->cam->pan_scale
                                     );
    
/*
 * Camera Tilt menu's and adjuster
 */
    create_camera_feature_menu_scale(main_window,
                                     dac->cam,
                                     FEATURE_TILT,
                                     "Tilt:",
                                     9,
                                     dac->cam->camera_tilt,
                                     dac->cam->camera_tilt_menu,
                                     dac->cam->menu_item_camera_tilt_man,
                                     dac->cam->menu_item_camera_tilt_auto,
                                     dac->cam->menu_item_camera_tilt_na,
                                     dac->cam->adj_tilt_scale,
                                     dac->cam->tilt_scale
                                     );

/*
 * Record activating button
 */
    dac->cam->button_cam_start = gtk_button_new_with_label(_("start camera"));
    gtk_widget_ref(dac->cam->button_cam_start);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "button_cam_start",
			     dac->cam->button_cam_start,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->button_cam_start);
    gtk_table_attach(GTK_TABLE(dac->table),
		     dac->cam->button_cam_start, 
		     0, 
		     1, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 dac->cam->button_cam_start,
			 _("Starts camera to record PIV images of an area that is illuminated "
                           "by a (double cavity Nd-YAG) laser and is observed "
                           "by a CCD camera. Camera and lasers are eventually "
                           "triggered by TTL pulses from the (parallel) output "
                           "port."),
			 NULL);

    gtk_object_set_data(GTK_OBJECT (dac->cam->button_cam_start),
			"gpiv",
			gpiv);
    g_signal_connect(GTK_OBJECT (dac->cam->button_cam_start),
		       "enter",
		       G_CALLBACK (on_button_dac_camstart_enter),
		       NULL);
    g_signal_connect(GTK_OBJECT (dac->cam->button_cam_start),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect(GTK_OBJECT (dac->cam->button_cam_start),
/* 		       "clicked", */
/* 		       "released", */
		       "pressed",
		       G_CALLBACK (on_button_dac_camstart),
		       NULL);


/*
 * Record de-activating button
 */
    dac->cam->button_cam_stop = gtk_button_new_with_label(_("stop camera"));
    gtk_widget_ref(dac->cam->button_cam_stop);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
			     "button_cam_stop",
			     dac->cam->button_cam_stop,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(dac->cam->button_cam_stop);
    gtk_table_attach(GTK_TABLE(dac->table),
		     dac->cam->button_cam_stop, 
		     1, 
		     2, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 dac->cam->button_cam_stop,
			 _("Stops camera to record of PIV images "
                           "and, eventually, cancels sending of trigger pulses"),
			 NULL);

    gtk_object_set_data(GTK_OBJECT (dac->cam->button_cam_stop),
			"gpiv",
			gpiv);
    g_signal_connect(GTK_OBJECT (dac->cam->button_cam_stop),
		       "enter",
		       G_CALLBACK (on_button_dac_camstop_enter),
		       NULL);
    g_signal_connect(GTK_OBJECT (dac->cam->button_cam_stop),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect(GTK_OBJECT (dac->cam->button_cam_stop),
		       "clicked",
		       G_CALLBACK (on_button_dac_camstop),
		       NULL);
    return cam;
}
