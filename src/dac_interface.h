
/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Dac tab
 * $Log: dac_interface.h,v $
 * Revision 1.2  2008-09-16 11:17:42  gerber
 * Updated because of cvs conflict
 *
 * Revision 1.1  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 */
#ifndef GPIV_DAC_INTERFACE_H
#define GPIV_DAC_INTERFACE_H



/* #undef ENABLE_CAM */
#ifdef ENABLE_CAM
typedef struct _DacDraw DacDraw;
struct _DacDraw {
  GtkWidget *window;
  GtkWidget *darea;
};


DacDraw *
create_dacdraw(GpivCamVar *cam_var,
	       guchar *rgbbuf);


typedef struct _Cam Cam;
struct _Cam {
  GtkWidget *frame_cam;
  GtkWidget *vbox_cam;
  GtkWidget *table_cam;
/*   GtkWidget *label_cam; */
  GtkWidget *camera_select;
  GtkWidget *camera_select_menu;
  GtkWidget *menu_item;

  GtkWidget *format_menu;
  GtkWidget *format_menu_menu;
/*   GtkWidget *format0_menu_menu; */

  GtkWidget *fps_menu;
  GtkWidget *fps_menu_menu;

 /*  GtkWidget *frame_trigger; */
/*   GtkWidget *vbox_trigger; */
  GtkWidget *hbox_trigger;
  GtkWidget *trigger_external;
  GtkWidget *trigger_polarity;
  GtkWidget* trigger_mode;
  GtkWidget* trigger_mode_menu;

  GtkWidget *checkbutton_camera_trigger;
  GtkWidget *camera_exposure;
  GtkWidget *camera_exposure_menu;
  GtkWidget *menu_item_camera_exposure_man;
  GtkWidget *menu_item_camera_exposure_auto;
  GtkWidget *menu_item_camera_exposure_na;
  GtkObject *adj_exposure_scale;
  GtkWidget *exposure_scale;

  GtkWidget *camera_iris;
  GtkWidget *camera_iris_menu;
  GtkWidget *menu_item_camera_iris_man;
  GtkWidget *menu_item_camera_iris_auto;
  GtkWidget *menu_item_camera_iris_na;
  GtkObject *adj_iris_scale;
  GtkWidget *iris_scale;

  GtkWidget *camera_shutter;
  GtkWidget *camera_shutter_menu;
  GtkWidget *menu_item_camera_shutter_man;
  GtkWidget *menu_item_camera_shutter_auto;
  GtkWidget *menu_item_camera_shutter_na;
  GtkObject *adj_shutter_scale;
  GtkWidget *shutter_scale;

  GtkWidget *camera_gain;
  GtkWidget *camera_gain_menu;
  GtkWidget *menu_item_camera_gain_man;
  GtkWidget *menu_item_camera_gain_auto;
  GtkWidget *menu_item_camera_gain_na;
  GtkObject *adj_gain_scale;
  GtkWidget *gain_scale;

  GtkWidget *camera_temp;
  GtkWidget *camera_temp_menu;
  GtkWidget *menu_item_camera_temp_man;
  GtkWidget *menu_item_camera_temp_auto;
  GtkWidget *menu_item_camera_temp_na;
  GtkObject *adj_temp_scale;
  GtkWidget *temp_scale;

  GtkWidget *label_temp;
  GtkWidget *label_label_temp;

  GtkWidget *camera_zoom;
  GtkWidget *camera_zoom_menu;
  GtkWidget *menu_item_camera_zoom_man;
  GtkWidget *menu_item_camera_zoom_auto;
  GtkWidget *menu_item_camera_zoom_na;
  GtkObject *adj_zoom_scale;
  GtkWidget *zoom_scale;

  GtkWidget *camera_pan;
  GtkWidget *camera_pan_menu;
  GtkWidget *menu_item_camera_pan_man;
  GtkWidget *menu_item_camera_pan_auto;
  GtkWidget *menu_item_camera_pan_na;
  GtkObject *adj_pan_scale;
  GtkWidget *pan_scale;

  GtkWidget *camera_tilt;
  GtkWidget *camera_tilt_menu;
  GtkWidget *menu_item_camera_tilt_man;
  GtkWidget *menu_item_camera_tilt_auto;
  GtkWidget *menu_item_camera_tilt_na;
  GtkObject *adj_tilt_scale;
  GtkWidget *tilt_scale;


  GtkWidget *button_cam_start;
  GtkWidget *button_cam_stop;
};
#endif /* ENABLE_CAM */


typedef struct _Trig Trig;
struct _Trig {
  GtkWidget *frame_trigger;
  GtkWidget *table_trigger;
/*   GtkWidget *label_trigger; */
  GtkWidget *vbox_trigger;
  GSList *mouse_trigger_group;
  GtkWidget *radiobutton_mouse_1;
  GtkWidget *radiobutton_mouse_2;
#ifdef ENABLE_TRIG
  GtkWidget *radiobutton_mouse_3;
  GtkWidget *radiobutton_mouse_4;
  GtkWidget *radiobutton_mouse_5;
  GtkWidget *radiobutton_mouse_6;
  GtkWidget *label_trigger_dt;
  GtkObject *spinbutton_adj_trigger_dt;
  GtkWidget *spinbutton_trigger_dt;
  GtkWidget *label_trigger_incrdt;
  GtkObject *spinbutton_adj_trigger_incrdt;
  GtkWidget *spinbutton_trigger_incrdt;
  GtkWidget *label_trigger_cap;
  GtkObject *spinbutton_adj_trigger_cap;
  GtkWidget *spinbutton_trigger_cap;
#endif /* ENABLE_TRIG */
  GtkWidget *label_trigger_nf;
  GtkObject *spinbutton_adj_trigger_nf;
  GtkWidget *spinbutton_trigger_nf;
#ifdef ENABLE_TRIG
  GtkWidget *button_trigger_start;
  GtkWidget *button_trigger_stop;
#endif /* ENABLE_TRIG */
};



typedef struct _Dac Dac;
struct _Dac {
  GtkWidget *vbox_label;
  GtkWidget *label_title;

  GtkWidget *vbox_scroll;
  GtkWidget *scrolledwindow;
  GtkWidget *viewport;
  GtkWidget *table;
  
  GtkWidget *frame_fname;
  GtkWidget *table_fname;
  GtkWidget *label_fname;
  GtkWidget *entry_fname;
  GtkWidget *combo_fname;
  GList *combo_fname_items;
  GtkWidget *checkbutton_fname_date;
  GtkWidget *checkbutton_fname_time;

  Trig *trig;
#ifdef ENABLE_CAM
  Cam *cam;
#endif /* ENABLE_CAM */
};


/* void */
/* create_camera_exposure_menu(GtkWidget *main_window, */
/* 			    Dac *dac); */
/* void */
/* create_camera_iris_menu(GtkWidget *main_window, */
/* 			Dac *dac); */

/* void */
/* create_camera_shutter_menu(GtkWidget *main_window, */
/* 			   Dac *dac); */

/* void */
/* create_camera_gain_menu(GtkWidget *main_window, */
/* 			Dac *dac); */

/* void */
/* create_camera_menu(GtkWidget *main_window, */
/* 		   Dac *dac); */

Dac *
create_dac(GnomeApp *main_window, 
	   GtkWidget *container);


#endif /* GPIV_DAC_INTERFACE_H */
