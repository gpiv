/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Dac callbacks
 * $Id: dac_trig.c,v 1.1 2006-09-18 07:29:51 gerber Exp $
 */
#ifdef ENABLE_TRIG

#include "gpiv_gui.h"
#include "console.h"
#include "dac_trig.h"

/* #include <sys/types.h> */
/* #include <sys/stat.h> */
/* #include <fcntl.h> */


/*
 * BUGFIX: Put somwhere to update on regular basis:
        gtk_label_set_text(GTK_LABEL(gpiv->dac->label_temp), 
                           "Temp");
*/

static int init, start, stop, error;

/*
 * Prototypes of local functions
 */

/*
 * Global functions
 */
void
exec_trigger_start (void
            )
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;
    int on = 1, param_ok;

    g_message("exec_trigger_start: 0");
    if (!gpiv_trig_openrtfs(&init, &start, &stop, &error)) {
        err_msg = _("Fail in fifo open");
        warning_gpiv(err_msg);
        return;
    }


    g_message("exec_trigger_start: 1");
    gpiv_trig_test_parameter(&trig_par);

/*
 * write the timing details to /dev/rtf/1
 */
    g_message("exec_trigger_start: 2");
    if((write(init, &trig_par.ttime, sizeof(GpivTrigTime))) < 0)  {
        err_msg = _("Fail in setting camera and Laser timing");
        warning_gpiv(err_msg);
        return;
    }

    g_message("exec_trigger_start: 3");
    if((read(error, &param_ok, sizeof(int))) < 0) {
        err_msg = _("Fail in receipt of confirmation");
        warning_gpiv(err_msg);
        return;
    }
        
    g_message("exec_trigger_start: 4");
    if (param_ok != 1) {
            warning_gpiv("Invalid parameters entered \n");
        
    } else { 
        if (verbose) 
            g_message (_("Parameters: \n\
cam_acq_period: %lld \n\
laser_trig_pw:  %lld \n\
time2laser:     %lld \n\
dt:             %lld \n\
mode:           %d \n\
cycles:         %d \n\
increment:      %d\n"),
                       trig_par.ttime.cam_acq_period, 
                       trig_par.ttime.laser_trig_pw,
                       trig_par.ttime.time2laser, 
                       trig_par.ttime.dt, 
                       trig_par.ttime.mode, 
                       trig_par.ttime.cycles, 
                       (int) trig_par.ttime.increment);
        
        g_message("exec_trigger_start: 5");
        if((write(start, &on, sizeof(int))) < 0) {
            err_msg = _("Fail in starting camera and Laser timing");
            warning_gpiv(err_msg);
            return;
        }
    }
    
    g_message("exec_trigger_start: 6");
/*     gpiv_par.process_trig = TRUE; */
}



void
exec_trigger_stop (void
           )
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;
    int off = 0;

    if((write(stop, &off, sizeof(int))) < 0)  {
        err_msg = _("Fail stopping camera and Laser timing");
        warning_gpiv(err_msg);
        return;
    }

/*     gpiv_par.process_trig = FALSE; */
}


/*
 * Callback functions
 */
void
on_spinbutton_dac_trigger_dt(GtkSpinButton * widget, 
                            GtkWidget * entry
                            )
/*-----------------------------------------------------------------------------
 */
{
    gfloat val = gtk_spin_button_get_value_as_float(widget);
    trig_par.ttime.dt =  (RTIME)  (GPIV_MILI2NANO * val);
}



void
on_spinbutton_dac_trigger_incrdt(GtkSpinButton * widget, 
                            GtkWidget * entry
                            )
/*-----------------------------------------------------------------------------
 */
{
    gfloat val = gtk_spin_button_get_value_as_float(widget);
    trig_par.ttime.increment = (RTIME) (GPIV_MILI2NANO * val);
}



void
on_spinbutton_dac_trigger_cap(GtkSpinButton * widget, 
                            GtkWidget * entry
                            )
/*-----------------------------------------------------------------------------
 */
{
    gfloat val = gtk_spin_button_get_value_as_float(widget);
    trig_par.ttime.cam_acq_period  =  (RTIME) (GPIV_MILI2NANO * val);
}



void 
on_button_dac_triggerstart_enter(GtkWidget *widget, 
                    gpointer data
                    )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Uploads and Starts timings to kernel module");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_button_dac_triggerstart(GtkWidget * widget, 
              gpointer data
              )
/* ----------------------------------------------------------------------------
 */
{
/*     gchar *msg =  */
/*         warning_gpiv(msg); */
    cancel_process = FALSE;
    exec_trigger_start();
}



void 
on_button_dac_triggerstop_enter(GtkWidget *widget, 
                    gpointer data
                    )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Stops timings");
    gnome_appbar_set_status(GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_button_dac_triggerstop(GtkWidget * widget, 
              gpointer data
              )
/* ----------------------------------------------------------------------------
 */
{
/*     gchar *msg =  */
/*         warning_gpiv(msg); */
    cancel_process = TRUE;
    exec_trigger_stop();
}

#endif /* ENABLE_TRIG */
