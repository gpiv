/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Dac callbacks
 * $Id: dac_trig.h,v 1.2 2007-03-22 16:00:32 gerber Exp $
 */

#ifndef DAC_TRIG_H
#define DAC_TRIG_H
#ifdef ENABLE_TRIG

/*
 * Global functions
 */
void
exec_trigger_start (void
                    );

void
exec_trigger_stop (void
                   );

/*
 * Callback functions
 */

void
on_spinbutton_dac_trigger_dt(GtkSpinButton * widget, 
                            GtkWidget * entry);

void
on_spinbutton_dac_trigger_incrdt(GtkSpinButton * widget, 
                                 GtkWidget * entry);
void
on_spinbutton_dac_trigger_cap(GtkSpinButton * widget, 
                            GtkWidget * entry);

void 
on_button_dac_triggerstart_enter(GtkWidget *widget, 
                                 gpointer data);

void
on_button_dac_triggerstart(GtkWidget *widget, 
		  gpointer data);

void 
on_button_dac_triggerstop_enter(GtkWidget *widget, 
                    gpointer data);

void
on_button_dac_triggerstop(GtkWidget *widget, 
		  gpointer data);

#endif /* ENABLE_TRIG */
#endif /* DAC_TRIG_H */
