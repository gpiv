/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.


   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (Callback) functions for dialogs
 * $Id: dialog.c,v 1.1 2008-09-16 11:20:18 gerber Exp $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include "support.h"
#include "gpiv_gui.h"



void
on_button_quit_gpiv_clicked (GtkDialog *dialog,
                             gint response,
                             gpointer data
                             )
/*-----------------------------------------------------------------------------
 * exit, message dialog callbacks
 */
{
    g_assert( response == GTK_RESPONSE_ACCEPT 
              || response == GTK_RESPONSE_REJECT);

    switch (response) {
    case GTK_RESPONSE_ACCEPT:
        free_all_bufmems(display_act);
        gtk_main_quit();
        break;

    case GTK_RESPONSE_REJECT:
        gtk_widget_destroy(GTK_WIDGET (dialog));
        break;

    default:
        g_warning("on_button_quit_gpiv_clicked: should not arrive here");
        break;
    }
}



void
on_close_buffer_response (GtkDialog *dialog,
                          gint response,
                          gpointer data
                          )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(dialog), "gpiv");
    Display *disp = gtk_object_get_data (GTK_OBJECT(dialog), "display");


    g_assert ( response == GTK_RESPONSE_ACCEPT
              || response == GTK_RESPONSE_REJECT);

/*     g_message ("on_close_buffer_response:: id = %d", */
/*                display_act->id); */

    switch (response) {
    case GTK_RESPONSE_ACCEPT:

/*
 * Deleting buffer and data
 */
        close_buffer (gpiv, disp/* , row */);
        break;
 
    case GTK_RESPONSE_REJECT:
/*
 * No action; just keep buffer with (unstored) data
 */
        break;

    default:
        g_warning("on_close_buffer_response: should not arrive here");
        break;
    }

/*     g_message ("on_close_buffer_response:: FINISHED"); */
}
