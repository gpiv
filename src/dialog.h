
/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/
/*
 * Callbacks from dialogs
 * $Id: dialog.h,v 1.1 2008-09-16 11:19:37 gerber Exp $
 */

#ifndef DIALOG_H
#define DIALOG_H


/*
 *  Exit, message, error dialog callbacks
 */
void
on_button_quit_gpiv_clicked(GtkDialog *dialog,
                            gint response,
                            gpointer data
                            );
/*
 * From: create_close_buffer
 */
void
on_close_buffer_response(GtkDialog *dialog,
			 gint response,
			 gpointer data
			 );

/* void */
/* on_error_response(GtkDialog *dialog, */
/*                   gint response, */
/*                   gpointer data */
/*                   ); */



#endif  /* CONSOLE_H */
