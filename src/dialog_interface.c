/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8 c-style: "K&R" -*- */


/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Id: dialog_interface.c,v 1.1 2008-09-16 11:19:37 gerber Exp $
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "support.h"

#include "gpiv_gui.h"
#include "dialog.h"
#include "dialog_interface.h"



GtkWidget 
*create_about (void
             )
/*-----------------------------------------------------------------------------
 */
{
        GtkWidget *about;
	GdkPixbuf *logo = NULL;
        const gchar *authors[] = {
                "Gerber van der Graaf <gerber_graaf@users.sourceforge.net>",
#ifdef ENABLE_TRIG
                "Julio Soria",
#endif /* ENABLE_TRIG */
                NULL
        };


	logo = gdk_pixbuf_new_from_file(PIXMAPSDIR "gpiv_logo.png", NULL);

	if (logo != NULL) {
		GdkPixbuf *temp_logo = NULL;
		
		temp_logo = gdk_pixbuf_scale_simple (logo,
			gdk_pixbuf_get_width (logo),
			gdk_pixbuf_get_height (logo),
			GDK_INTERP_HYPER);
		g_object_unref (logo);

		logo = temp_logo;
	}

        about = g_object_new (GTK_TYPE_ABOUT_DIALOG,
                              "name","Gpiv",
                              "version", VERSION,
                              "copyright", _("Copyright G. Van der Graaf"),
			      "comments", _("GPIV is a program for (Digital) \
Particle Image Velocimetry. It \
interrogates images (pairs) of a fluid flow that has been seeded \
with tracer particles, resulting into the velocity field of \
the fluid flow."),
                              "authors", authors ,
                              "logo", logo,
                              "icon", logo,
                              NULL);

        gtk_object_set_data (GTK_OBJECT (about), 
                             "about", 
                             about);

        g_signal_connect_swapped (GTK_DIALOG (about),
                                  "response", 
                                  G_CALLBACK (gtk_widget_destroy),
                                  about);

	gtk_widget_show(GTK_WIDGET(about));
        if (logo != NULL) g_object_unref (logo);
        return about;
}



GtkDialog
*create_exit_dialog (/* GtkWidget *console */
             )
/*-----------------------------------------------------------------------------
 */
{
        GtkDialog *exit_dialog;
        GtkWidget *dialog_vbox1;
        GtkWidget *hbox;
        GtkImage *icon;
        GtkLabel *text;
 	GdkPixbuf *logo = NULL; 

        GtkWidget *exit_message_label;
        gchar *message = _("There are unsaved data that will be lost.\nAre you sure you want to quit?");


/*
 * Gnome2:
 */
/*  	logo = gdk_pixbuf_new_from_file(PIXMAPSDIR "gpiv_logo.png", NULL); */
        exit_dialog = GTK_DIALOG( gtk_dialog_new_with_buttons 
                           ("gpiv exit",
                            NULL,
                            GTK_DIALOG_DESTROY_WITH_PARENT,
                            GTK_STOCK_NO, GTK_RESPONSE_REJECT,
                            GTK_STOCK_YES, GTK_RESPONSE_ACCEPT,
                            NULL));
        
        gtk_dialog_set_default_response(exit_dialog,
                                         GTK_RESPONSE_REJECT);
        g_signal_connect( exit_dialog,
                          "response",
                          G_CALLBACK (on_button_quit_gpiv_clicked),
                          NULL);

  gtk_window_set_icon(GTK_WINDOW( exit_dialog), logo);
         if (logo != NULL) g_object_unref (logo);
        


        hbox = g_object_new(GTK_TYPE_HBOX,
                            "border-width", 8,
                            NULL);
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(exit_dialog)->vbox),
                           hbox);
	gtk_widget_show(GTK_WIDGET (hbox));




        icon = g_object_new(GTK_TYPE_IMAGE,
                                   "stock", /* GTK_STOCK_DIALOG_QUESTION */ GTK_STOCK_DIALOG_WARNING,
                                   "icon-size", GTK_ICON_SIZE_DIALOG,
                                   "xalign", 0.5,
                                   "yalign", 1.0,
                                   NULL);
        gtk_box_pack_start (GTK_BOX(hbox),
                            GTK_WIDGET(icon),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (icon));




       text = g_object_new(GTK_TYPE_LABEL,
                           "wrap", TRUE,
                           "use-markup", TRUE,
                           "label", message,
                           NULL);
       gtk_box_pack_start (GTK_BOX(hbox),
                           GTK_WIDGET(text),
                           FALSE,
                           FALSE,
                           0);
       gtk_widget_show(GTK_WIDGET (text));

       return exit_dialog;
}



GtkDialog
*create_message_dialog (gchar * message
                        )
/*-----------------------------------------------------------------------------
 */
{
        GtkDialog *message_dialog;
        GtkWidget *hbox;
        GtkImage *icon;
        GtkLabel *text;
 	GdkPixbuf *logo = NULL; 

/*
 * Gnome2:
 */
        message_dialog = GTK_DIALOG( gtk_dialog_new_with_buttons 
                                 ("gpiv message",
                                  NULL,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_STOCK_OK, GTK_RESPONSE_NONE,
                                  NULL));

       g_signal_connect_swapped (GTK_DIALOG (message_dialog),
				  "response", 
				  G_CALLBACK (gtk_widget_destroy),
				  message_dialog);


   gtk_window_set_icon(GTK_WINDOW( message_dialog), logo); 
        if (logo != NULL) g_object_unref (logo);

        hbox = g_object_new(GTK_TYPE_HBOX,
                            "border-width", 8,
                            NULL);
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(message_dialog)->vbox),
                           hbox);
	gtk_widget_show(GTK_WIDGET (hbox));




        icon = g_object_new(GTK_TYPE_IMAGE,
                                   "stock", GTK_STOCK_DIALOG_INFO,
                                   "icon-size", GTK_ICON_SIZE_DIALOG,
                                   "xalign", 0.5,
                                   "yalign", 1.0,
                                   NULL);
        gtk_box_pack_start (GTK_BOX(hbox),
                            GTK_WIDGET(icon),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (icon));



        text =  g_object_new(GTK_TYPE_LABEL,
                             "wrap", TRUE,
                             "use-markup", TRUE,
                             "label", message,
                             NULL);
        gtk_box_pack_start (GTK_BOX(hbox),
                            GTK_WIDGET(text),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (text));
	gtk_widget_show(GTK_WIDGET (text));


        return message_dialog;
}



GtkDialog
*create_warning_dialog (gchar * message
                        )
/*-----------------------------------------------------------------------------
 */
{
        GtkDialog *warning_dialog;
        GtkWidget *hbox;
        GtkImage *icon;
        GtkLabel *text;
 	GdkPixbuf *logo = NULL; 

/*
 * Gnome2:
 */
        warning_dialog = GTK_DIALOG( gtk_dialog_new_with_buttons 
                                 ("gpiv warning",
                                  NULL,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_STOCK_OK, GTK_RESPONSE_NONE,
                                  NULL));

        g_signal_connect_swapped (GTK_DIALOG (warning_dialog),
				  "response", 
				  G_CALLBACK (gtk_widget_destroy),
				  warning_dialog);


   gtk_window_set_icon(GTK_WINDOW( warning_dialog), logo); 
        if (logo != NULL) g_object_unref (logo);

        hbox = g_object_new(GTK_TYPE_HBOX,
                            "border-width", 8,
                            NULL);
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(warning_dialog)->vbox),
                           hbox);
	gtk_widget_show(GTK_WIDGET (hbox));




        icon = g_object_new(GTK_TYPE_IMAGE,
                                   "stock", GTK_STOCK_DIALOG_WARNING,
                                   "icon-size", GTK_ICON_SIZE_DIALOG,
                                   "xalign", 0.5,
                                   "yalign", 1.0,
                                   NULL);
        gtk_box_pack_start (GTK_BOX(hbox),
                            GTK_WIDGET(icon),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (icon));



        text =  g_object_new(GTK_TYPE_LABEL,
                             "wrap", TRUE,
                             "use-markup", TRUE,
                             "label", message,
                             NULL);
        gtk_box_pack_start (GTK_BOX(hbox),
                            GTK_WIDGET(text),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (text));
/*         gtk_container_add (GTK_CONTAINER (GTK_DIALOG(warning_dialog)->vbox), */
/*                            GTK_WIDGET (text)); */
	gtk_widget_show(GTK_WIDGET (text));


        return warning_dialog;
}



GtkDialog
*create_error_dialog (gchar * message
                 )
/*-----------------------------------------------------------------------------
 */
{
        GtkDialog *error_dialog;
        GtkWidget *hbox;
        GtkImage *icon;
        GtkLabel *text;


        error_dialog = GTK_DIALOG( gtk_dialog_new_with_buttons 
                               ("gpiv error",
                                NULL,
                                GTK_DIALOG_MODAL 
                                | GTK_DIALOG_DESTROY_WITH_PARENT,
                                GTK_STOCK_OK, GTK_RESPONSE_NONE,
                                NULL));
        
	g_signal_connect_swapped (GTK_DIALOG (error_dialog),
				  "response", 
				  G_CALLBACK (gtk_widget_destroy),
				  error_dialog);
        
/*
 * BUGFIX: probably not necessary as OK button connect with
 */
/*         g_signal_connect( error_box, */
/*                           "response", */
/*                           G_CALLBACK(on_error_response), */
/*                           NULL); */


        hbox = g_object_new(GTK_TYPE_HBOX,
                            "border-width", 8,
                            NULL);
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(error_dialog)->vbox),
                           hbox);
	gtk_widget_show(GTK_WIDGET (hbox));




        icon = g_object_new(GTK_TYPE_IMAGE,
                                   "stock", GTK_STOCK_DIALOG_ERROR,
                                   "icon-size", GTK_ICON_SIZE_DIALOG,
                                   "xalign", 0.5,
                                   "yalign", 1.0,
                                   NULL);
        gtk_box_pack_start (GTK_BOX (hbox),
                            GTK_WIDGET (icon),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (icon));
        



        text = g_object_new(GTK_TYPE_LABEL,
                                   "wrap", TRUE,
                                   "use-markup", TRUE,
                                   "label", message,
                                   NULL);
        gtk_box_pack_start (GTK_BOX (hbox),
                            GTK_WIDGET (text),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (text));

        return error_dialog;
}



GtkDialog
*create_close_buffer_dialog (GpivConsole * gpiv, 
                             Display *disp,
                             char *message
                             )
/*-----------------------------------------------------------------------------
 */
{
        GtkDialog *close_buffer_dialog;
        GtkWidget *hbox;
        GtkImage *icon;
        GtkLabel *text;

        close_buffer_dialog = GTK_DIALOG(gtk_dialog_new_with_buttons 
                            ("gpiv close_buffer",
                             GTK_WINDOW (gpiv->console),
                             GTK_DIALOG_DESTROY_WITH_PARENT,
                             GTK_STOCK_NO, GTK_RESPONSE_REJECT,
                             GTK_STOCK_YES, GTK_RESPONSE_ACCEPT,
                             NULL));

        g_signal_connect (close_buffer_dialog,
                          "response",
                          G_CALLBACK (on_close_buffer_response),
                          NULL);
    
        g_signal_connect_swapped (GTK_DIALOG (close_buffer_dialog),
                                  "response", 
                                  G_CALLBACK (gtk_widget_destroy),
                                  close_buffer_dialog);


        hbox = g_object_new(GTK_TYPE_HBOX,
                            "border-width", 8,
                            NULL);
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(close_buffer_dialog)->vbox),
                           hbox);
	gtk_widget_show(GTK_WIDGET (hbox));




        icon = g_object_new(GTK_TYPE_IMAGE,
                                   "stock", /* GTK_STOCK_DIALOG_QUESTION */ GTK_STOCK_DIALOG_WARNING,
                                   "icon-size", GTK_ICON_SIZE_DIALOG,
                                   "xalign", 0.5,
                                   "yalign", 1.0,
                                   NULL);
        gtk_box_pack_start (GTK_BOX(hbox),
                            GTK_WIDGET(icon),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (icon));
        



        text = g_object_new(GTK_TYPE_LABEL,
                            "wrap", TRUE,
                            "use-markup", TRUE,
                            "label", message,
                            NULL);
        gtk_box_pack_start (GTK_BOX(hbox),
                            GTK_WIDGET(text),
                            FALSE,
                            FALSE,
                            0);
	gtk_widget_show(GTK_WIDGET (text));

        gtk_object_set_data (GTK_OBJECT (close_buffer_dialog),
                             "gpiv",
                             gpiv);
        gtk_object_set_data (GTK_OBJECT (close_buffer_dialog),
                             "display",
                             disp);


        return close_buffer_dialog;
}

