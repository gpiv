/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

/*
 * $Id: dialog_interface.h,v 1.1 2008-09-16 11:19:37 gerber Exp $
 *
 * widgets for small dialogs and message windows
 */
#ifndef DIALOG_INTERFACE_H
#define DIALOG_INTERFACE_H

#include "gpiv_gui.h"



GtkWidget *
/* GnomeAbout *  */
create_about (void);

GtkDialog *
create_exit_dialog (void);

GtkDialog *
create_message_dialog (gchar *message);

GtkDialog *
create_warning_dialog (gchar *message);

GtkDialog *
create_error_dialog (gchar *message);

GtkDialog *
create_close_buffer_dialog (GpivConsole *gpiv, 
                            Display *disp,
                            char *message);

#endif   /* DIALOG_INTERFACE_H */
