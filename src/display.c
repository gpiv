/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------

gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
  libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

This file is part of gpiv.

Gpiv is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
* (callback) functions for the display
* $Log: display.c,v $
* Revision 1.24  2007-12-19 08:42:35  gerber
* debugged
*
* Revision 1.23  2007-11-23 16:24:07  gerber
* release 0.5.0: Kafka
*
* Revision 1.22  2007-03-22 16:00:32  gerber
* Added image processing tabulator
*
* Revision 1.21  2007/02/05 15:17:09  gerber
* auto stretching, broadcast display settings to buffers from preferences
*
* Revision 1.20  2007-01-29 11:27:43  gerber
* added image formats png, gif, tif png, bmp, improved buffer display
*
* Revision 1.19  2006/01/31 14:28:12  gerber
* version 0.3.0
*
* Revision 1.18  2005/06/15 15:03:54  gerber
* Optional Anti Aliased canvas for viewer
*
* Revision 1.17  2005/06/15 09:40:40  gerber
* debugged, optimized
*
* Revision 1.16  2005/02/26 09:17:13  gerber
* structured of interrogate function by using gpiv_piv_isiadapt
*
* Revision 1.15  2005/01/19 15:53:41  gerber
* Initiation of Data Acquisition (DAC); trigerring of lasers and camera
* by using RTAI and Realtime Linux, recording images from IEEE1394
* (Firewire) IIDC compliant camera's
*
* Revision 1.14  2004/10/15 19:24:05  gerber
* GPIV_ and Gpiv prefix to defines and structure names of libgpiv
*
* Revision 1.13  2004/06/14 21:19:23  gerber
* Image depth up to 16 bits.
* Improvement "single int" and "drag int" in Eval tab.
* Viewer's pop-up menu.
* Adaption for gpiv_matrix_* and gpiv_vector_*.
* Resizing console.
* See Changelog for further info.
*
* Revision 1.12  2003/09/01 11:17:14  gerber
* improved monitoring of interrogation process
*
* Revision 1.11  2003/08/22 15:24:52  gerber
* interactive spatial scaling
*
* Revision 1.10  2003/07/31 11:43:26  gerber
* display images in gnome canvas (HOERAreset)
*
* Revision 1.9  2003/07/13 14:38:18  gerber
* changed error handling of libgpiv
*
* Revision 1.8  2003/07/12 21:21:15  gerber
* changed error handling libgpiv
*
* Revision 1.6  2003/07/10 11:56:07  gerber
* added man page
*
* Revision 1.5  2003/07/05 13:14:57  gerber
* drag and drop of a _series_ of filenames from NAUTILUS
*
* Revision 1.4  2003/07/04 10:47:00  gerber
* cleaning up
*
* Revision 1.3  2003/07/03 17:08:02  gerber
* display ruler adjusted for scaled data
*
* Revision 1.2  2003/06/27 13:47:26  gerber
* display ruler, line/point evaluation
*
* Revision 1.1.1.1  2003/06/17 17:10:52  gerber
* Imported gpiv
*
*/

#include "gpiv_gui.h"
#include "utils.h"
#include "display_interface.h"
#include "display.h"
#include "display_event.h"
#include "display_zoom.h"
#include "piveval.h"
#include "dialog_interface.h"
#include "console.h"


static void
on_adj_changed__adapt_hcanvas (Display *disp
                               );
static void
on_adj_changed__adapt_vcanvas (Display *disp
                               );
static void
on_adj_changed__adapt_hruler (Display *disp
                              );
static void
on_adj_changed__adapt_vruler (Display *disp
                              );

static void
synchronize_menu (GtkWidget *menu_src,
                  gboolean active,
                  GtkWidget *menu_dest
                  );

/*
 * Callback display functions
 */
gint
on_button_display_origin_press_event (GtkWidget *widget, 
                                      GdkEventButton *event, 
                                      gpointer data
                                      )
/*-----------------------------------------------------------------------------
 */
{
    gtk_menu_popup (GTK_MENU (display_act->display_popupmenu), NULL, NULL, 
                    NULL, NULL, event->button, event->time);
    return FALSE;
}


void 
on_display_set_focus (GtkWidget *widget, 
                      gpointer data
                      )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = gtk_object_get_data(GTK_OBJECT (widget), "disp");
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint i = 0, row_selected = 0;
    gchar *text, labelno[GPIV_MAX_CHARS];
    gtk_clist_set_selection_mode (GTK_CLIST (gpiv->clist_buf),
                                  GTK_SELECTION_SINGLE);

    g_snprintf (labelno, GPIV_MAX_CHARS,"%d", disp->id);


    for (i = 0; i < nbufs; i++) {
        gtk_clist_get_text (GTK_CLIST (gpiv->clist_buf), i, 0, &text);

        if (strcmp(labelno, text) == 0) {
            row_selected = i;
        }
    }


    gtk_clist_select_row (GTK_CLIST (gpiv->clist_buf), row_selected, 0);

/* g_warning("on_display_set_focus:: disp->id=%d row_selected=%d",  */
/* disp->id, row_selected); */

    gtk_clist_set_selection_mode (GTK_CLIST (gpiv->clist_buf),
                                  GTK_SELECTION_EXTENDED);
}



void
delete_display (GtkWidget *widget,
                GdkEvent  *event,
                gpointer   data
                )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    Display *disp = gtk_object_get_data (GTK_OBJECT (widget), "disp");


    close_buffer__check_saved (gpiv, disp);
}



void 
on_adj_changed (GtkAdjustment *adj, 
                gpointer data
                )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = gtk_object_get_data (GTK_OBJECT (adj), "disp");

    enum variable_type {
        X_ADJ = 0,
        Y_ADJ = 1
    } var_type;

    var_type = atoi (gtk_object_get_data (GTK_OBJECT (adj), "var_type"));
/*         g_message ("ON_ADJ_CHANGED:: " */
/*                    "value = %f lower = %f upper = %f page_size = %f", */
/*                    adj->value, adj->lower, adj->upper, adj->page_size); */

    if (var_type == X_ADJ) {
        disp->x_adj_value = (gdouble) adj->value;
        disp->x_adj_upper = (gdouble) adj->upper;
        disp->x_adj_lower = (gdouble) adj->lower;
        disp->x_page_size = (gdouble) adj->page_size;

        on_adj_changed__adapt_hcanvas (disp);
        on_adj_changed__adapt_hruler (disp);
    }

    if (var_type == Y_ADJ) {
        disp->y_adj_value = (gdouble) adj->value;
        disp->y_adj_upper = (gdouble) adj->upper;
        disp->y_adj_lower = (gdouble) adj->lower;
        disp->y_page_size = (gdouble) adj->page_size;

        on_adj_changed__adapt_vcanvas (disp);
        on_adj_changed__adapt_vruler (disp);
    }
/*     g_message ("N_SCROLLEDWINDOW_DISPLAY_ADJ_CHANGED:: x_adj_value = %f" */
/* " x_adj_lower = %f x_adj_upper = %f x_page_size = %f", */
/*                disp->x_adj_value, disp->x_adj_lower, disp->x_adj_upper, */
/*                disp->x_page_size); */
}


/*
 * Callbacks for menus
 */
void
on_menu_synchronize_popup (GtkMenuItem *menuitem,
                           gpointer user_data)
/*-----------------------------------------------------------------------------
 * Synchronizing with popup menu item, which will actually change the
 * displaying of the menubar. 
 */
{
    Display *disp = display_act;
    gint menu_id = (int) user_data;
    GtkWidget *popupmenu;


    if (menu_id == VIEW_MENUBAR) {
        popupmenu = gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                                        "view_menubar_popup_menu");
        if (GTK_CHECK_MENU_ITEM(menuitem)->active == TRUE) {
            synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
        } else {
            synchronize_menu (GTK_WIDGET (menuitem), FALSE, popupmenu);
        }


    } else if (menu_id == VIEW_RULERS) {
        popupmenu = gtk_object_get_data( GTK_OBJECT (disp->mwin), 
                                         "view_rulers_popup_menu");
        if (GTK_CHECK_MENU_ITEM (menuitem)->active == TRUE) {
            synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
        } else {
            synchronize_menu (GTK_WIDGET (menuitem), FALSE, popupmenu);
        }


    } else if (menu_id == STRETCH_AUTO) {
        popupmenu = gtk_object_get_data( GTK_OBJECT (disp->mwin), 
                                         "stretch_auto_popup_menu");
        if (GTK_CHECK_MENU_ITEM (menuitem)->active == TRUE) {
            synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
        } else {
            synchronize_menu (GTK_WIDGET (menuitem), FALSE, popupmenu);
        }


    } else if (menu_id == VIEW_BLUE) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_background_display0");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);

 
    } else if (menu_id == VIEW_BLACK) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_background_display1");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VIEW_IMAGE_A) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_background_display2");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VIEW_IMAGE_B) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_background_display3");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VIEW_INTERROGATION_AREAS) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_piv_display0");
        if (GTK_CHECK_MENU_ITEM(menuitem)->active == TRUE) {
            synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
        } else {
            synchronize_menu (GTK_WIDGET (menuitem), FALSE, popupmenu);
        }


    } else if (menu_id == VIEW_VELOCITY_VECTORS) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_piv_display1");
        if (GTK_CHECK_MENU_ITEM (menuitem)->active == TRUE) {
            synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
        } else {
            synchronize_menu (GTK_WIDGET (menuitem), FALSE, popupmenu);
        }


    } else if (menu_id == VIEW_NONE_SCALARS) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_scalardata_display0");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VIEW_VORTICITY) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_scalardata_display1");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VIEW_SHEAR_STRAIN) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_scalardata_display2");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VIEW_NORMAL_STRAIN) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "view_scalardata_display3");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
        
        
    } else if (menu_id == VECTOR_COLOR_PEAK) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "vectorcolor_menu_display0");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VECTOR_COLOR_SNR) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "vectorcolor_menu_display1");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VECTOR_COLOR_MAGNGRAY) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "vectorcolor_menu_display2");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);


    } else if (menu_id == VECTOR_COLOR_MAGNCOLOR) {
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), 
                                         "vectorcolor_menu_display3");
        synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
    }
}



void 
on_stretch_activate (GtkMenuItem *menuitem,
                     gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;
    GtkAdjustment *hadj = GTK_ADJUSTMENT (disp->hadj);
        (disp->vadj) ;
    GtkAdjustment *vadj = GTK_ADJUSTMENT (disp->vadj) ;

    check__zoom_factor (&disp->zoom_factor);
    stretch_window (disp);

    vadj->lower = 0.0;
    vadj->upper = (gdouble)  disp->img->image->header->nrows * disp->zoom_factor;
    vadj->page_size = (gdouble) (disp->img->image->header->nrows 
                                 * disp->zoom_factor);
    hadj->lower = 0.0;
    hadj->upper = (gdouble) disp->img->image->header->ncolumns * disp->zoom_factor;
    hadj->page_size = (gdouble) (disp->img->image->header->ncolumns
                                 * disp->zoom_factor);
}


void 
on_zoom_activate (GtkMenuItem *menuitem,
                  gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;
    gint zoom_index = (int) user_data;
    gchar *key = NULL;
    GtkWidget *popupmenu;


    key = g_strdup_printf ("zmv_%d", (int) zoom_index);
    popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), key);

    synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
    g_free (key);
}


void
on_vector_scale_activate (GtkMenuItem *menuitem,
                          gpointer user_data)
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;
    gchar *key = NULL;
    GtkWidget *popupmenu;
    guint16 fact = 1;

    fact = fact << (int) user_data;
    gpiv_par->display__vector_scale = fact;

    key = g_strdup_printf ("vs_mv_%d", (int) user_data);
    popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), key);
    synchronize_menu (GTK_WIDGET (menuitem), TRUE, popupmenu);
    g_free (key);
}


/*
 * Callbacks for popup menus
 */
void 
view_toggle_menubar (GtkWidget *widget, 
                     gpointer data
                     )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;
    GtkWidget *menu = 
        gtk_object_get_data (GTK_OBJECT(disp->mwin), 
                             "view_menubar");

/*
 * Synchronizing menu item from menu bar.
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        synchronize_menu (widget, TRUE, menu);
    } else {
        synchronize_menu (widget, FALSE, menu);
    }

/*
 * Changing the displaying of menubar
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        gtk_widget_show (GTK_WIDGET (disp->menubar));
    } else {
        gtk_widget_hide (GTK_WIDGET (disp->menubar));
    }
}



void 
view_toggle_rulers (GtkWidget *widget, 
                    gpointer data
                    )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;


/*
 * Synchronizing menu item from menu bar.
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        synchronize_menu (widget, TRUE, disp->view_rulers);
    } else {
        synchronize_menu (widget, FALSE, disp->view_rulers);
    }

/*
 * Changing the displaying of rulers
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        gtk_widget_show (GTK_WIDGET (disp->hruler));
        gtk_widget_show (GTK_WIDGET (disp->vruler));
    } else {
        gtk_widget_hide (GTK_WIDGET (disp->hruler));
        gtk_widget_hide (GTK_WIDGET (disp->vruler));
    }
}



void 
view_toggle_stretch_display_auto (GtkWidget *widget, 
                                  gpointer data
                                  )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;


/*
 * Synchronizing menu item from menu bar.
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        synchronize_menu (widget, TRUE, disp->stretch_auto);
    } else {
        synchronize_menu (widget, FALSE, disp->stretch_auto);
    }

/*
 * Changing the displaying of rulers
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        gpiv_par->display__stretch_auto = TRUE;
        check__zoom_factor (&disp->zoom_factor);
        stretch_window (disp);
    } else {
        gpiv_par->display__stretch_auto = FALSE;
    }
}



void 
view_toggle_stretch_display (GtkWidget *widget, 
                             gpointer data
                             )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;
    gint screen_width = gdk_screen_width ();
    gint screen_height = gdk_screen_height ();


    check__zoom_factor (&disp->zoom_factor);
    stretch_window (disp);
}


void 
select_zoomscale (gpointer data, 
                  guint action, 
                  GtkWidget *widget
                  )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;
    gint zoom_index = action;
    gchar *key1 = NULL, *key2 = NULL;
    GtkWidget *popupmenu, *menu;


    key1 = g_strdup_printf ("zmv_%d", (int) zoom_index);
    popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), key1);

    key2 = g_strdup_printf ("zoom_menu_%d", (int) zoom_index);
    menu = gtk_object_get_data (GTK_OBJECT (disp->mwin), key2);

    synchronize_menu (popupmenu, TRUE, menu);

    zoom_display (disp, zoom_index);
    g_free (key1);
    g_free (key2);
}



void
select_view_background (gpointer data, 
                        guint action, 
                        GtkWidget *widget)
/*-----------------------------------------------------------------------------
 */
{
    gchar *color = NULL;
    Display *disp = display_act;
    GtkWidget *menu = NULL, *popupmenu = NULL;
 
    gpiv_par->display__backgrnd = action;
    hide_img1 (disp);
    hide_img2 (disp);

/*
 * Select menu and popup menu widgets
 */
    if (action == SHOW_BG_DARKBLUE) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "blue_background_menu");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "view_background_display0");

    } else if (action == SHOW_BG_BLACK) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "black_background_menu");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "view_background_display1");

    } else if (action == SHOW_BG_IMG1) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "image_a_background_menu");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "view_background_display2");

    } else if (action == SHOW_BG_IMG2) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "image_b_background_menu");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "view_background_display3");
    }

/*
 * Synchronize
 */
    synchronize_menu (popupmenu, TRUE, menu);

/*
 * Perform action
 */
    if (action == SHOW_BG_DARKBLUE) {
        color = "darkblue";
        if (disp->gci_bg != NULL) {
            gnome_canvas_item_set (GNOME_CANVAS_ITEM (disp->gci_bg),
                                   "fill_color", color,
                                   NULL);
        }

    } else if (action == SHOW_BG_BLACK) {
        color = "black";
        if (disp->gci_bg != NULL) {
            gnome_canvas_item_set (GNOME_CANVAS_ITEM (disp->gci_bg),
                                   "fill_color", color,
                                   NULL);
        }

    } else if (action == SHOW_BG_IMG1) {
        show_img1 (display_act);

    } else if (action == SHOW_BG_IMG2) {
        show_img2 (display_act);

    } else {
        color = "black";
        if (disp->gci_bg != NULL) {
            gnome_canvas_item_set (GNOME_CANVAS_ITEM (disp->gci_bg),
                                   "fill_color", color,
                                   NULL);
        } else {
            g_warning (_("select_view_background: should not arrive here"));
        }
    }

}



void 
view_toggle_intregs (GtkWidget *widget, 
                     gpointer data
                     )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;

/*
 * Synchronizing menu item from menu bar.
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        synchronize_menu (widget, TRUE, disp->view_interrogation_areas);
    } else {
        synchronize_menu (widget, FALSE, disp->view_interrogation_areas);
    }

/*
 * Changing the displaying of interrogation area's
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        if (view_toggle_intregs_flag) {
            if (display_act->display_intregs == FALSE) {
                display_act->display_intregs = TRUE;
                create_all_intregs (display_act);
                show_all_intregs (display_act);
            }
        }
    } else {
        if (display_act->display_intregs == TRUE) {
            destroy_all_intregs(display_act);
            display_act->display_intregs = FALSE;
        }
    }
}



void 
view_toggle_piv (GtkWidget *widget, 
                gpointer data
                )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = display_act;

/*
 * Synchronizing menu item from menu bar.
 */
    if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
        synchronize_menu (widget, TRUE, disp->view_velocity_vectors);
    } else {
        synchronize_menu (widget, FALSE, disp->view_velocity_vectors);
    }

/*
 * Changing the displaying of piv data (velocity vectors)
 */
    if (display_act->pida->exist_piv) {
	if (GTK_CHECK_MENU_ITEM (widget)->active == TRUE) {
	    if (display_act->display_piv == FALSE) {
                display_act->display_piv = TRUE;
                create_all_vectors (display_act->pida);
            }
	} else {
	    if (display_act->display_piv == TRUE) {
                display_act->display_piv = FALSE;
                destroy_all_vectors (display_act->pida);
            }
	}
    }
}



void
select_view_scalardata (gpointer data, 
                        guint action, 
                        GtkWidget *widget
                        )
/*-----------------------------------------------------------------------------
 */
{
    gchar *color = NULL;
    Display *disp = display_act;
    GtkWidget *menu = NULL, *popupmenu = NULL;


    gpiv_par->display__scalar =  action;
    
    hide_all_scalars (disp, GPIV_VORTICITY);
    hide_all_scalars (disp, GPIV_S_STRAIN);
    hide_all_scalars (disp, GPIV_N_STRAIN);

/*
 * Select menu and popup menu widgets
 */
    if (action == SHOW_SC_NONE) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "view_none_scalars");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "view_scalardata_display0");

    } else if (action == SHOW_SC_VORTICITY) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "view_vorticity");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "view_scalardata_display1");

    } else if (action == SHOW_SC_SSTRAIN) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "view_shear_strain");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "view_scalardata_display2");

   } else if (action == SHOW_SC_NSTRAIN) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "view_normal_strain");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "view_scalardata_display3");
    }


    synchronize_menu (popupmenu, TRUE, menu);

/*
 * Perform action
 */
    if (action == SHOW_SC_NONE) {
        hide_all_scalars (disp, GPIV_VORTICITY);
        hide_all_scalars (disp, GPIV_S_STRAIN);
        hide_all_scalars (disp, GPIV_N_STRAIN);

    } else if (action == SHOW_SC_VORTICITY) {
        show_all_scalars (display_act, GPIV_VORTICITY);
        
    } else if (action == SHOW_SC_SSTRAIN) {
        show_all_scalars (display_act, GPIV_S_STRAIN);
        
    } else if (action == SHOW_SC_NSTRAIN) {
        show_all_scalars (display_act, GPIV_N_STRAIN);

    } else {
        color = "black";
        if (disp->gci_bg != NULL) {
            gnome_canvas_item_set (GNOME_CANVAS_ITEM (disp->gci_bg),
                                   "fill_color", color,
                                   NULL);
        } else {
            g_warning (_("select_view_scalardata: should not arrive here"));
        }
    }

}



void 
select_vectorscale (gpointer data, 
                    guint action, 
                    GtkWidget *widget
                    )
/*-----------------------------------------------------------------------------
  Setting vector_scale from display pop-up menu */
{
    Display *disp = display_act;
    gchar *key1 = NULL, *key2 = NULL;
    GtkWidget *menu,  *popupmenu;
    guint16 fact = 1;

    gpiv_par->display__vector_scale = fact << (int) action;;

    key1 = g_strdup_printf ("vector_scale_menu_%d", (int) action);
    menu = gtk_object_get_data (GTK_OBJECT (disp->mwin), key1);

    key2 = g_strdup_printf ("vs_mv_%d", (int) action);
    popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin), key2);

    synchronize_menu (popupmenu, TRUE, menu);

    update_all_vectors (display_act->pida);
    g_free (key1);
    g_free (key2);
}



void 
select_vectorcolor (gpointer data, 
                    guint action, 
                    GtkWidget *widget
                    )
/*-----------------------------------------------------------------------------
  Setting vector_color from display pop-up menu */
{
    Display *disp = display_act;
    GtkWidget *menu, *popupmenu;

/*
 * Select menu and popup menu widgets
 */
    if (action == SHOW_PEAKNR) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "vector_color_peak_nr");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "vectorcolor_menu_display0");

    } else if (action == SHOW_SNR) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "vector_color_snr");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "vectorcolor_menu_display1");

    } else if (action == SHOW_MAGNITUDE_GRAY) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "vector_color_magngray");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "vectorcolor_menu_display2");

    } else if (action == SHOW_MAGNITUDE) {
        menu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                    "vector_color_magncolor");
        popupmenu = gtk_object_get_data (GTK_OBJECT (disp->mwin),
                                         "vectorcolor_menu_display3");
    }

/*
 * Synchronize
 */
    synchronize_menu (popupmenu, TRUE, menu);

/*
 * Perform action
 */
    gpiv_par->display__vector_color = action;
    update_all_vectors (display_act->pida);
}



void
nav_popup_click_handler (GtkWidget *widget, 
                         GdkEventButton *event,
                         gpointer data
                         )
/*-----------------------------------------------------------------------------
 */
{
    /* Seee gimp: nav_window.c */
}



void 
on_view_options_clicked (GtkButton *button, 
                         gpointer user_data
                         )
/*-----------------------------------------------------------------------------
 */
{

}


/* 
 * Local functions
 */
static void
on_adj_changed__adapt_hcanvas (Display *disp
                               )
/*-----------------------------------------------------------------------------
 */
{
    int x_old, y_old;
    gnome_canvas_get_scroll_offsets (GNOME_CANVAS (disp->canvas),
                                             &x_old,
                                             &y_old);
    gnome_canvas_scroll_to (GNOME_CANVAS (disp->canvas),
                            (int) disp->x_adj_value,
                            y_old);
}


static void
on_adj_changed__adapt_vcanvas (Display *disp
                               )
/*-----------------------------------------------------------------------------
 */
{
    int x_old, y_old;
    gnome_canvas_get_scroll_offsets (GNOME_CANVAS (disp->canvas),
                                             &x_old,
                                             &y_old);
    gnome_canvas_scroll_to (GNOME_CANVAS (disp->canvas),
                            x_old,
                            (int) disp->y_adj_value);
}

static void
on_adj_changed__adapt_hruler (Display *disp
                              )
/*-----------------------------------------------------------------------------
 */
{
    set__hrulerscale (disp);
}


static void
on_adj_changed__adapt_vruler (Display *disp
                              )
/*-----------------------------------------------------------------------------
 */
{
    set__vrulerscale (disp);
}


static void
synchronize_menu (GtkWidget *menu_src,
                  gboolean state,
                  GtkWidget *menu_dest
                  )
/*-----------------------------------------------------------------------------
 * Synchronizes menu_dest with menu_src, which is in 'state' position.
 * Only change if the state between menu_src and menu_dest differ in order 
 * to avoid a loop.
 */
{
    if (GTK_CHECK_MENU_ITEM (menu_src) != NULL
        && GTK_CHECK_MENU_ITEM (menu_src)->active == state) {
        if (GTK_CHECK_MENU_ITEM (menu_dest) != NULL
            && gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (menu_dest))
            == !state) {
            gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menu_dest),
                                            state);
        } 
    }
}




