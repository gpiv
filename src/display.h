/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */
 
/*----------------------------------------------------------------------

  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/


/*
 * (callback) functions for the display
 * $Log: display.h,v $
 * Revision 1.14  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.13  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.12  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.11  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.10  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.9  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.8  2005/06/15 09:40:40  gerber
 * debugged, optimized
 *
 * Revision 1.7  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.6  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.5  2003/09/01 11:17:14  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.4  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.3  2003/07/31 11:43:26  gerber
 * display images in gnome canvas (HOERAreset)
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include "display_image.h"
#include "display_event.h"
#include "display_zoom.h"


enum DataType {
    DATA_TYPE__INTREG,
    DATA_TYPE__PIV
};


enum DisplayMenuId {
    VIEW_MENUBAR,
    VIEW_RULERS,
    STRETCH_AUTO,
    VIEW_BLUE,
    VIEW_BLACK,
    VIEW_IMAGE_A,
    VIEW_IMAGE_B,
    VIEW_INTERROGATION_AREAS,
    VIEW_VELOCITY_VECTORS,
    VIEW_NONE_SCALARS,
    VIEW_VORTICITY,
    VIEW_SHEAR_STRAIN,
    VIEW_NORMAL_STRAIN,
    VECTOR_COLOR_PEAK,
    VECTOR_COLOR_SNR,
    VECTOR_COLOR_MAGNGRAY,
    VECTOR_COLOR_MAGNCOLOR
};


gboolean shift_pressed, ctrl_pressed, alt_pressed;
gint enable_col_start, enable_col_end, enable_row_start, 
    enable_row_end;

void
/* on_display_set_focus(gpointer data, guint action, GtkWidget *widget); */
on_display_set_focus (GtkWidget *widget, 
                      gpointer data);

void 
delete_display (GtkWidget *widget,
                GdkEvent  *event,
                gpointer   data);

void 
on_adj_changed (GtkAdjustment *adj, 
                gpointer data);

/* void  */
/* select_zoomscale (GtkMenuItem * menuitem, gpointer data, guint action); */

void 
zoom_display (Display *disp, 
              gint zoom_index);

/*
 * Callback functions for the display menu
 */

void
on_menu_synchronize_popup (GtkMenuItem *menuitem,
                           gpointer user_data);

void 
on_stretch_activate (GtkMenuItem *menuitem,
                     gpointer user_data);

void 
on_zoom_activate (GtkMenuItem *menuitem,
                  gpointer user_data);

void
on_vector_scale_activate (GtkMenuItem *menuitem,
                          gpointer user_data);

/*
 * Callbacks for popup menus
 */
void
view_toggle_menubar (GtkWidget *widget, 
                     gpointer data);

void
view_toggle_rulers (GtkWidget *widget, 
                    gpointer data);

void
view_toggle_stretch_display_auto (GtkWidget *widget, 
                                  gpointer data);

void
view_toggle_stretch_display (GtkWidget *widget, 
                             gpointer data);

void 
select_zoomscale (gpointer data, 
                  guint action, 
                  GtkWidget *widget);

void
select_view_background (gpointer data, 
                        guint action, 
                        GtkWidget *widget);

void
view_toggle_intregs (GtkWidget *widget, 
                     gpointer data);

void
view_toggle_piv (GtkWidget *widget, 
                 gpointer data);

void
select_view_scalardata (gpointer data, 
                        guint action, 
                        GtkWidget *widget);

void
select_vectorscale (gpointer data, 
                    guint action, 
                    GtkWidget *widget);

void
select_vectorcolor (gpointer data, 
                    guint action, 
                    GtkWidget *widget);

void 
on_view_options_clicked (GtkButton *button, 
                         gpointer user_data);


#endif /* DISPLAY_H */
