/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------

gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
  libraries.

Copyright (C) 2006, 2007, 2008
Gerber van der Graaf

This file is part of gpiv.

Gpiv is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/


#include "gpiv_gui.h"
#include "display.h"
#include "display_zoom.h"
#include "piveval_interrogate.h"

GdkCursor *cursor;

static void
canvas_display_motion_notify__NO_MS (Display *disp, 
				     gint x, 
				     gint y, 
				     gint *index_x, 
				     gint *index_y
				     );

static void
canvas_display_button_release__AOI_MS (Display *disp, 
				       GpivConsole *gpiv,
				       GtkWidget *view_piv_display0,
				       gdouble x, 
				       gdouble y
				       );

static void
canvas_display_motion_notify__DRAGINT_MS (Display *disp, 
                                          gint x, 
                                          gint y,
                                          gint *index_x, 
                                          gint *index_y
                                          );

static void
canvas_display_motion_notify__SINGLE_POINT_MS (Display *disp, 
					       gint x, 
					       gint y
					       );


static char *
canvas_display_button_press__SINGLE_AREA_MS (GpivConsole *gpiv,
					     Display *disp,
					     gint x, 
					     gint y
					     );

static char *
canvas_display_button_release__SINGLE_AREA_MS (GpivConsole *gpiv,
					       Display *disp,
					       gdouble x, 
					       gdouble y
					       );

static void
canvas_display_button_press__SINGLE_POINT_MS (GpivConsole *gpiv,
					      Display *disp,
					      gint x, 
					      gint y
					      );

static void
canvas_display_button_release__HVLINE_MS (Display *disp, 
					   GpivConsole *gpiv,
					   GtkWidget *view_piv_display0, 
					   gdouble x, 
					   gdouble y
					   );
static void
canvas_display_button_press__DISABLE_POINT_MS (Display *disp, 
					       gint x, 
					       gint y
					       );
static void
set__NO_MS (GpivConsole *gpiv,
	    Display *disp
	    );

static void
search_nearest_index (Display *disp, 
                     gint x, 
                     gint y, 
                     gint *index_x, 
                     gint *index_y,
                     gint data_type
		     );

static void
highlight_intreg (gint index_y, 
                 gint index_x, 
                 Display *disp
                 );

static void
create_msg_display_with_pivdata (GpivPivData *piv_data, 
                                gint index_y, 
                                gint index_x,
                                gint scale
                                );

static void
create_msg_display_with_scdata (GpivScalarData *sc_data,
                               gchar *sc_type,
                               gint index_y, 
                               gint index_x,
                               gint scale
                               );
static void
create_msg_display_with_pivscdata (GpivPivData *piv_data,
                                  GpivScalarData *sc_data,
                                  gchar *sc_type,
                                  gint index_y, 
                                  gint index_x,
                                  gint scale
                                  );

static void
create_line (Display *disp, 
             gint x1, 
             gint y1, 
             gint x2, 
             gint y2
             );

static void
update_line (gint x1, 
             gint y1, 
             gint x2, 
             gint y2
	     );

static void
destroy_line ();

static void
update_rect (gint x, 
             gint y
	     );

static void
create_rect (Display *disp, 
             gint x, 
             gint y
             );

static void
destroy_rect ();

static void
pane_canvas (Display *disp,
             gfloat x, 
             gfloat y
             );

static void
pane_canvas_x (Display *disp,
               gfloat x
               );

static void
pane_canvas_y (Display *disp,
               gfloat y
               );

/*
 * Public display functions
 */
gboolean
canvas_display_enter_notify (GtkWidget *widget, 
                            GdkEventMotion *event,
                            gpointer data
                            )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = gtk_object_get_data(GTK_OBJECT(widget), "disp");
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");


    disp->index_x_old = 0;
    disp->index_y_old = 0;
    disp->index_old = FALSE;


    if (m_select == NO_MS) {
	cursor = gdk_cursor_new(GDK_DOTBOX);

    } else if (m_select == SINGLE_AREA_MS
               || m_select == ENABLE_POINT_MS 
               || m_select == DISABLE_POINT_MS) {
	cursor = gdk_cursor_new(GDK_CROSSHAIR);



    } else if ( m_select == DRAG_MS ) {
	cursor = gdk_cursor_new(GDK_CROSS);



    } else if (m_select == SINGLE_POINT_MS
               || m_select == V_LINE_MS
               || m_select == H_LINE_MS
               ) {
        if (m_select == V_LINE_MS) {
            cursor = gdk_cursor_new (GDK_SB_V_DOUBLE_ARROW);
        }
        if (m_select == H_LINE_MS) {
            cursor = gdk_cursor_new (GDK_SB_H_DOUBLE_ARROW);
        }
	
        if (m_select == SINGLE_POINT_MS) {
            cursor = gdk_cursor_new(GDK_CROSS);
        }


	if (display_act->pida->exist_piv && disp->display_piv) {
	    hide_all_vectors(display_act->pida);
	}

        if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
            hide_all_intregs(disp);
	}



    } else if (m_select == AOI_MS 
               || m_select == ENABLE_AREA_MS 
               || m_select == DISABLE_AREA_MS) {
	cursor = gdk_cursor_new(GDK_CROSSHAIR);

        if (m_select == AOI_MS) {
            if (display_act->pida->exist_piv && disp->display_piv) {
                hide_all_vectors(display_act->pida);
            }

            if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
                hide_all_intregs(disp);
            }
        }



    } else if (m_select == SPANLENGTH_MS
               || m_select == V_SPANLENGTH_MS
               || m_select == H_SPANLENGTH_MS
               ) {
	cursor = gdk_cursor_new(GDK_CROSS);
    }

    gdk_window_set_cursor(disp->mwin->window, cursor);


    return TRUE;
}



gboolean
canvas_display_motion_notify (GtkWidget *widget, 
                              GdkEventMotion *event,
                              gpointer data
                              )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = gtk_object_get_data(GTK_OBJECT(widget), "disp");
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    GtkAdjustment *hadj = GTK_ADJUSTMENT (disp->hadj);
    GtkAdjustment *vadj = GTK_ADJUSTMENT (disp->hadj);

    gdouble x, y;
    gint x_grab, y_grab;
    gint index_y = 0, index_x = 0;
    
    GdkModifierType state;
    GnomeCanvasPoints *points;


    points = gnome_canvas_points_new(2);    
    
    if (event->is_hint) {
	gdk_window_get_pointer (event->window, &x_grab, &y_grab, &state);

	x = x_grab;
	y = y_grab;

    } else {
	x = event->x;
	y = event->y;

	state = event->state;
    }

    
/*             g_message ("canvas_display_motion_notify:: x = %f y = %f x/zoom = %f y/zoom = %f",  */
/*                        x, y, x / disp->zoom_factor, y / disp->zoom_factor); */

    /*     if (x >= hadj->value && x <  hadj->value + hadj->page_size */
    /*         && y >= vadj->value && y < vadj->value + vadj->page_size) { */


    if (x >= 0 && x < disp->img->image->header->ncolumns * disp->zoom_factor
        && y >= 0 && y < disp->img->image->header->nrows * disp->zoom_factor) {

        /*
         * display particle displacements / velocities and its attributes,
         */
        if (m_select == NO_MS 
            || m_select == SINGLE_AREA_MS 
            || m_select == DRAG_MS
            || m_select == ENABLE_POINT_MS 
            || m_select == DISABLE_POINT_MS) {


            /*
             * display locations of interrogation area's and estimators,
             * only display locations of interrogation area's,
             * or nothing
             */

            canvas_display_motion_notify__NO_MS (disp, 
                                                 (gint) (x / disp->zoom_factor),
                                                 (gint) (y / disp->zoom_factor), 
                                                 &index_x, 
                                                 &index_y);

            /*
             * Pane (moving) the canvas. Identic effect as moving the scroll bars.
             */
            if (m_select == NO_MS) {
                if (state & GDK_BUTTON1_MASK) {
                    pane_canvas (disp, x, y);
                }
            }


            if ( m_select == DRAG_MS ) {
                canvas_display_motion_notify__DRAGINT_MS (disp, 
                                                          (gint) (x / disp->zoom_factor),
                                                          (gint) (y / disp->zoom_factor), 
                                                          &index_x, 
                                                          &index_y);
            }
        
            disp->index_x_old = index_x;
            disp->index_y_old = index_y;


            /*
             * Only displays pointer position
             */
        } else if (m_select == AOI_MS 
                   || m_select == ENABLE_AREA_MS 
                   || m_select == DISABLE_AREA_MS ) {
            g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d px y=%d px",
                       (gint) (x / disp->zoom_factor), 
                       (gint) (y / disp->zoom_factor));
        
            /*
             * Update the drawing of the rectangle
             */
            if (state & GDK_BUTTON1_MASK) {
                if (gci_aoi != NULL) {
                    update_rect((gint) (x / disp->zoom_factor), 
                                (gint) (y / disp->zoom_factor));
                }
            }



        } else if ( m_select == SINGLE_POINT_MS) {
            canvas_display_motion_notify__SINGLE_POINT_MS (disp, 
                                                           (gint) x / disp->zoom_factor, 
                                                           (gint) y / disp->zoom_factor);
        

 
            /*
             * Update a vertical line
             */
        } else if (m_select == V_LINE_MS) {
            if (state & GDK_BUTTON1_MASK) {
                g_snprintf (msg_display, GPIV_MAX_CHARS, "x=%d px y=%d px", 
                            (gint) (disp->xgrab_first / disp->zoom_factor), 
                            (gint) (y / disp->zoom_factor));
                if ( gci_line != NULL ) {
                    update_line ((gint) (disp->xgrab_first / disp->zoom_factor) , 
                                 (gint) (disp->ygrab_first / disp->zoom_factor),
                                 (gint) (disp->xgrab_first / disp->zoom_factor),
                                 (gint) (y / disp->zoom_factor));
                }
            } else {
                g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d px y=%d px",
                           (gint) (x / disp->zoom_factor), 
                           (gint) (y / disp->zoom_factor));
            }
        

 
            /*
             * Update a horizontal line
             */
        } else if (m_select == H_LINE_MS) {
            if (state & GDK_BUTTON1_MASK) {
                g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d y=%d", 
                           (gint) (x / disp->zoom_factor), 
                           (gint) (disp->ygrab_first / disp->zoom_factor));
                if (gci_line != NULL ) {
                    update_line ((gint) (disp->xgrab_first / disp->zoom_factor), 
                                 (gint) (disp->ygrab_first / disp->zoom_factor),
                                 (gint) (x / disp->zoom_factor),
                                 (gint) (disp->ygrab_first / disp->zoom_factor));
                }
            } else {
                g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d px y=%d px", 
                           (gint) (x / disp->zoom_factor), 
                           (gint) (y / disp->zoom_factor));
            }


            
        } else if (m_select == SPANLENGTH_MS) {
            g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d px y=%d px", 
                       (gint) (x / disp->zoom_factor), 
                       (gint) (y / disp->zoom_factor));
        
            if (state & GDK_BUTTON1_MASK && gci_line != NULL ) {
                g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d y=%d", 
                           (gint) (x / disp->zoom_factor), 
                           (gint) (y / disp->zoom_factor));
                update_line ((gint) (disp->xgrab_first / disp->zoom_factor), 
                             (gint) (disp->ygrab_first / disp->zoom_factor),
                             (gint) (x / disp->zoom_factor),
                             (gint) (y / disp->zoom_factor));
            }

        } else if (m_select == V_SPANLENGTH_MS) {
            g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d px y=%d px", 
                       (gint) (x / disp->zoom_factor), 
                       (gint) (y / disp->zoom_factor));
        
            if (state & GDK_BUTTON1_MASK && gci_line != NULL ) {
                g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d y=%d", 
                           (gint) (disp->xgrab_first / disp->zoom_factor), 
                           (gint) (y / disp->zoom_factor));
                update_line ((gint) (disp->xgrab_first / disp->zoom_factor), 
                             (gint) (disp->ygrab_first / disp->zoom_factor),
                             (gint) (disp->xgrab_first / disp->zoom_factor),
                             (gint) (y / disp->zoom_factor));
            }
        
        } else if (m_select == H_SPANLENGTH_MS) {
            g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d px y=%d px", 
                       (gint) (x / disp->zoom_factor), 
                       (gint) (y / disp->zoom_factor));
        
            if (state & GDK_BUTTON1_MASK && gci_line != NULL ) {
                g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d y=%d", 
                           (gint) (x / disp->zoom_factor), 
                           (gint) (disp->ygrab_first / disp->zoom_factor));
                update_line ((gint) (disp->xgrab_first / disp->zoom_factor), 
                             (gint) (disp->ygrab_first / disp->zoom_factor),
                             (gint) (x / disp->zoom_factor),
                             (gint) (disp->ygrab_first / disp->zoom_factor));
                
            }
        }

    
    
        gnome_appbar_push(GNOME_APPBAR(disp->appbar), msg_display);
        gnome_appbar_push(GNOME_APPBAR(gpiv->appbar), msg_display);
    
    }
    

    /*     } else { */
    /*         g_message("canvas_display_motion_notify: out of image borders"); */


    gnome_canvas_points_free(points);
    return TRUE;
}



gboolean
canvas_display_button_press (GtkWidget *widget, 
                            GdkEventButton *event,
                            gpointer data
                            )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = gtk_object_get_data(GTK_OBJECT(widget), "disp");
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gdouble x, y;
    gint x_grab, y_grab;
    gint index_x = 0, index_y = 0;
    GdkModifierType state;
    GnomeCanvasPoints *points;
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");


    points = gnome_canvas_points_new(2);
    GtkMenu *menu = GTK_MENU (disp->display_popupmenu);

    gdk_window_get_pointer(event->window, &x_grab, &y_grab, &state);
    x = x_grab ;
    y = y_grab ;



    if (event->button == 1) {
        disp->xgrab_first = x;
        disp->ygrab_first = y;
/*             g_message ("canvas_display_button_press:: xgrab_first = %f ygrab_first = %f", */
/*                        disp->xgrab_first, disp->ygrab_first); */
/*
 * No mouse select
 */
        if (m_select == NO_MS) {
            cursor = gdk_cursor_new (GDK_FLEUR );
            gdk_window_set_cursor (disp->mwin->window, cursor);
/*             g_message ("canvas_display_button_press:: x_grab = %d y_grab = %d", */
/*                        x_grab, y_grab); */
/*             g_message ("CANVAS_DISPLAY_BUTTON_PRESS:: x = %f y = %f", x, y); */


/*
 * select Area Of Interest
 */
        } else if (m_select == AOI_MS) {
/*
 * storige of original AOI
 */
            disp->intreg->col_start_old = disp->pida->piv_par->col_start;
            disp->intreg->row_start_old = disp->pida->piv_par->row_start;
            
/*             g_message ("CANVAS_DISPLAY_BUTTON_PRESS (AOI):: x_grab = %d y_grab = %d", */
/*                        x_grab, y_grab); */
            create_rect (disp, 
                         disp->xgrab_first / disp->zoom_factor, 
                         disp->ygrab_first / disp->zoom_factor);

/*
 * select at single interrogation area
 */
        } else if ((m_select == SINGLE_AREA_MS || m_select == DRAG_MS) &&
                   disp->img->exist_img) {
            canvas_display_button_press__SINGLE_AREA_MS (gpiv, disp, 
                                                         x / disp->zoom_factor, 
                                                         y / disp->zoom_factor);

/*
 * select a single point
 */
        } else if (m_select == SINGLE_POINT_MS && disp->img->exist_img) {
            canvas_display_button_press__SINGLE_POINT_MS (gpiv, disp, 
                                                          x / disp->zoom_factor, 
                                                          y / disp->zoom_factor); 


/*
 * select a vertical line
 */
        } else if (m_select == V_LINE_MS && display_act->img->exist_img) {
/*             g_message ("CANVAS_DISPLAY_BUTTON_PRESS (V_LINE_MS) 11:: x_grab = %d y_grab = %d", */
/*                        x_grab, y_grab); */
            if (gci_line != NULL) destroy_line();
            create_line (disp, 
                         disp->xgrab_first / disp->zoom_factor, 
                         disp->ygrab_first / disp->zoom_factor,
                         disp->xgrab_first / disp->zoom_factor, 
                         disp->ygrab_first / disp->zoom_factor);
             

/*
 * select a horizontal line
 */
        } else if (m_select == H_LINE_MS && disp->img->exist_img) {
/*             g_message ("CANVAS_DISPLAY_BUTTON_PRESS (H_LINE_MS) 1:: x_grab = %d y_grab = %d", */
/*                        x_grab, y_grab); */
            if (gci_line != NULL) destroy_line();
            create_line (disp, 
                         disp->xgrab_first / disp->zoom_factor, 
                         disp->ygrab_first / disp->zoom_factor, 
                         disp->xgrab_first / disp->zoom_factor, 
                         disp->ygrab_first / disp->zoom_factor);
             
        } else if ( m_select == ENABLE_POINT_MS ) {
/*
 * Only active with piv data
 */
            if (disp->pida->exist_piv && disp->display_piv 
/*                && GTK_CHECK_MENU_ITEM(view_piv_display0)->active */
                ) {
                search_nearest_index (disp, x, y, &index_x, &index_y,
/*                                      DATA_TYPE__INTREG); */
                                      DATA_TYPE__PIV);
                disp->pida->piv_data->peak_no[index_y][index_x] = 1;
                update_vector (disp->pida, index_y, index_x);
            }


/*
 * Enabling and disabling only active with piv data
 */
        } else if ((m_select == DISABLE_POINT_MS
                    ||  m_select == ENABLE_AREA_MS
                    ||  m_select == DISABLE_AREA_MS)
                   && disp->pida->exist_piv && disp->display_piv) {
            canvas_display_button_press__DISABLE_POINT_MS (disp, x, y); 


        } else if (m_select == SPANLENGTH_MS 
                   && disp->img->exist_img) {
            if (gci_line != NULL) destroy_line();
            create_line (disp,  
                         disp->xgrab_first / disp->zoom_factor,
                         disp->ygrab_first / disp->zoom_factor,
                         disp->xgrab_first / disp->zoom_factor, 
                         disp->ygrab_first / disp->zoom_factor);
            
            
            
        } else if (m_select == V_SPANLENGTH_MS 
                   && disp->img->exist_img) {
            if (gci_line != NULL) destroy_line();
            create_line (disp,  
                         disp->xgrab_first / disp->zoom_factor,
                         disp->ygrab_first / disp->zoom_factor,
                         disp->xgrab_first / disp->zoom_factor, 
                         disp->ygrab_first / disp->zoom_factor);
            
            
            
        } else if (m_select == H_SPANLENGTH_MS  
                   && disp->img->exist_img) {
            if (gci_line != NULL) destroy_line();
            create_line (disp,  
                         disp->xgrab_first / disp->zoom_factor,
                         disp->ygrab_first / disp->zoom_factor,
                         disp->xgrab_first / disp->zoom_factor,
                         disp->ygrab_first / disp->zoom_factor);
            
            
        } else {
            g_warning(_("canvas_display_button_press: should not arrive here; m_select=%d"), 
                      m_select);
        }
        

    } else if (event->button == 3) {
        gtk_menu_popup (menu, NULL, NULL, NULL, NULL, 
                        event->button, event->time);
    }


    gnome_canvas_points_free(points);
    return TRUE;
}


gboolean
canvas_display_button_release (GtkWidget *widget, 
                               GdkEventButton *event,
                               gpointer data
                               )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = gtk_object_get_data(GTK_OBJECT(widget), "disp");
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    GtkWidget *view_piv_display0 = 
                        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                                            "view_piv_display0");
    gdouble x, y;
    gint x_grab, y_grab;
    gint i, j;
    GdkModifierType state;


    shift_pressed = event->state & GDK_SHIFT_MASK;
    gdk_window_get_pointer (event->window, &x_grab, &y_grab, &state);

/* BUGFIX: repair for different mouse selections */
    x = x_grab;
    y = y_grab;


/*     g_message ("canvas_display_button_release:: x = %f y = %f", x, y); */
    if (event->button == 1) {

	if (m_select == AOI_MS && gci_aoi != NULL) {
            canvas_display_button_release__AOI_MS (disp, gpiv,
                                                   view_piv_display0,
                                                   (gdouble) (x), 
                                                   (gdouble) (y));



        } else if ((m_select == V_LINE_MS || m_select == H_LINE_MS) 
                   && disp->img->exist_img) {
            /*             g_message ("CANVAS_DISPLAY_BUTTON_RELEASE (HV_LINE):: x_grab = %d y_grab = %d", */
            /*                        x_grab, y_grab); */
            canvas_display_button_release__HVLINE_MS (disp, gpiv,
                                                      view_piv_display0,
                                                      (gdouble) (x), 
                                                      (gdouble) (y));
              


/*
 * analyse at single interrogation area
 */
        } else if ((m_select == SINGLE_AREA_MS || m_select == DRAG_MS) &&
                   disp->img->exist_img) {
            canvas_display_button_release__SINGLE_AREA_MS(gpiv, disp, 
                                                          (gdouble) (x), 
                                                          (gdouble) (y));



	} else if (m_select == ENABLE_AREA_MS && gci_aoi != NULL) {
            enable_col_end = x;
            enable_row_end = y;
            if (disp->pida->exist_piv) {
                for (i = 0; i < disp->pida->piv_data->ny; i++) {
                    for (j = 0; j < disp->pida->piv_data->nx; j++) {
                        if (disp->pida->piv_data->point_x[i][j] >= 
                            enable_col_start
                            && disp->pida->piv_data->point_x[i][j] < 
                            enable_col_end
                            && disp->pida->piv_data->point_y[i][j] >= 
                            enable_row_start 
                            && disp->pida->piv_data->point_y[i][j] < 
                            enable_row_end) {
                            disp->pida->piv_data->peak_no[i][j] = 1;
                            if (disp->display_piv)                         
                                update_vector(disp->pida, i, j);
                            
                        }
                    }
                }
            }
	    if (gci_aoi != NULL) destroy_rect();



	} else if (m_select == DISABLE_AREA_MS && gci_aoi != NULL) {
            enable_col_end = x;
            enable_row_end = y;
            if (disp->pida->exist_piv) {
                for (i = 0; i < disp->pida->piv_data->ny; i++) {
                    for (j = 0; j < disp->pida->piv_data->nx; j++) {
                        if (disp->pida->piv_data->point_x[i][j] >= 
                            enable_col_start
                            && disp->pida->piv_data->point_x[i][j] < 
                            enable_col_end 
                            && disp->pida->piv_data->point_y[i][j] >= 
                            enable_row_start 
                            && disp->pida->piv_data->point_y[i][j] < 
                            enable_row_end) {
                            disp->pida->piv_data->peak_no[i][j] = -1;
                            disp->pida->piv_data->snr[i][j] = GPIV_SNR_DISABLE;

                            if (disp->pida->post_par->set == TRUE) {
                                disp->pida->piv_data->dx[i][j] = 
                                    disp->pida->post_par->set_dx;
                                disp->pida->piv_data->dy[i][j] = 
                                    disp->pida->post_par->set_dx;
                            }

                            if (disp->display_piv)                         
                                update_vector(disp->pida, i, j);
                            
                        }
                    }
                }
            }
	    if (gci_aoi != NULL) destroy_rect();



        } else if (m_select == SPANLENGTH_MS 
                   && disp->img->exist_img
                   && gci_line != NULL
                   ) {
            gfloat hdist = abs ((x - disp->xgrab_first) / disp->zoom_factor);
            gfloat vdist = abs ((y - disp->ygrab_first) / disp->zoom_factor);

            gpiv_var->img_span_px = sqrt (hdist * hdist + vdist * vdist);
            if (gci_line != NULL) destroy_line ();
            gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                      (gpiv->imgh->spinbutton_sscale_px), 
                                      gpiv_var->img_span_px
                                      );



        } else if (m_select == V_SPANLENGTH_MS 
                   && disp->img->exist_img
                   && gci_line != NULL
                   ) {
            gpiv_var->img_span_px = abs ((y - disp->ygrab_first) / disp->zoom_factor);
            if (gci_line != NULL) destroy_line ();
           gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                      (gpiv->imgh->spinbutton_sscale_px), 
                                      gpiv_var->img_span_px);



        } else if (m_select == H_SPANLENGTH_MS  
                   && disp->img->exist_img
                   && gci_line != NULL
                   ) {
            gpiv_var->img_span_px = abs ((x - disp->xgrab_first) / disp->zoom_factor);
            if (gci_line != NULL) destroy_line ();
           gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                      (gpiv->imgh->spinbutton_sscale_px), 
                                      gpiv_var->img_span_px);


        
/*
 * No action
 */
        } else if (m_select == GPIV_NONE 
                   || m_select == SINGLE_POINT_MS
                   || m_select == ENABLE_POINT_MS
                   || m_select == DISABLE_POINT_MS
                   ) {
            cursor = gdk_cursor_new (GDK_DOTBOX);
            gdk_window_set_cursor (disp->mwin->window, cursor);

        } else {
            g_warning(_("canvas_display_button_release: should not arrive here; m_select=%d"), 
                      m_select);
        }   
    }

    return TRUE;
}



gboolean 
canvas_display_leave_notify (GtkWidget *widget, 
                             GdkEventMotion *event,
                             gpointer data
                             )
/*-----------------------------------------------------------------------------
 */
{
    Display *disp = gtk_object_get_data(GTK_OBJECT(widget), "disp");
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");


    gnome_appbar_push(GNOME_APPBAR(disp->appbar), 
                      disp->msg_display_default);
    gnome_appbar_push(GNOME_APPBAR(gpiv->appbar), msg_default);


    if (m_select == SINGLE_POINT_MS 
        && GTK_CHECK_MENU_ITEM(view_piv_display0)->active
        ) {
        if (!disp->intreg->exist_int) {
            update_intreg1(disp, m_select_index_y, m_select_index_x);
            update_intreg2(disp, m_select_index_y, m_select_index_x);
        }

        if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
            show_all_intregs(disp);
        }


       if (disp->pida->exist_piv && disp->display_piv) {
            show_all_vectors(disp->pida);
        }


    } else if (m_select == DRAG_MS) {
        if (!disp->intreg->exist_int) {
            update_intreg1(disp, disp->index_y_old, disp->index_x_old);
            update_intreg2(disp, disp->index_y_old, disp->index_x_old);
        }

    } else if (m_select == SINGLE_AREA_MS && disp->intreg->exist_int) {
        if (!disp->intreg->exist_int) {
            update_intreg1(disp, 0, 0);
            update_intreg2(disp, 0, 0);
        }


    } else if (m_select == AOI_MS
        || m_select == V_LINE_MS
        || m_select == H_LINE_MS) {
        if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
           show_all_intregs(disp);
        }

        if (disp->pida->exist_piv && disp->display_piv) {
            show_all_vectors(disp->pida);
        }

        if (gci_line != NULL) {
            gtk_object_destroy(GTK_OBJECT(gci_line));
            gci_line = NULL;
        }

    }



    if (GDK_BUTTON1_MASK) {
/* 
 * Cancel AOI_MS selection
 */
	if (m_select == AOI_MS) {
	    if (gci_aoi != NULL) {
		disp->pida->piv_par->col_start = disp->intreg->col_start_old;
		disp->pida->piv_par->row_start = disp->intreg->row_start_old;
		gtk_spin_button_set_value(GTK_SPIN_BUTTON
					  (gpiv->piveval->spinbutton_colstart),
					  disp->pida->piv_par->col_start);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON
					  (gpiv->piveval->spinbutton_rowstart),
					  disp->pida->piv_par->row_start);
                if (gci_aoi != NULL) destroy_rect();
	    }

/*             show_all_intregs(disp); */

	} else if ( m_select == ENABLE_AREA_MS 
                    || m_select == DISABLE_AREA_MS) { 
             enable_col_start = 0;
             enable_row_start = 0;
             enable_col_end = 0;
             enable_row_end = 0;
	    if (gci_aoi != NULL) destroy_rect();

/*
 * Cancel V_LINE_MS, LINE_MS selection
 * (V_, H_) SPANLENGTH_MS
 */
        } else if (m_select == V_LINE_MS 
                   || m_select == H_LINE_MS
                   || m_select == SPANLENGTH_MS
                   || m_select == V_SPANLENGTH_MS
                   || m_select == H_SPANLENGTH_MS
                   ) {
            if (gci_line != NULL) destroy_line();
        }
    }




/*
 * General
 */
    if (cursor != NULL) {
	gdk_cursor_destroy(cursor);
	cursor = NULL;
    }

    if (disp->intreg->gci_intreg2[disp->index_y_old][disp->index_x_old] 
        != NULL) {
        update_intreg2(disp, disp->index_y_old, disp->index_x_old);
    }

    if (disp->intreg->gci_intreg1[disp->index_y_old][disp->index_x_old] 
        != NULL) {
        update_intreg1(disp, disp->index_y_old, disp->index_x_old);
    }

    disp->index_x_old = 0;
    disp->index_y_old = 0;


    return TRUE;
}

/*
 * Private display functions
 */


static void
canvas_display_motion_notify__NO_MS (Display *disp, 
                                    gint x, 
                                    gint y, 
                                    gint *index_x, 
                                    gint *index_y
                                    )
/*-----------------------------------------------------------------------------
 * Moves pointer over the display canvas if no mouse selection (NO_MS) is set
 */
{
    GpivPivData *piv_data = disp->pida->piv_data;
    GpivPivData *piv_data_scaled = disp->pida->piv_data_scaled;
    GpivScalarData *vor_data = disp->pida->vor_data;
    GpivScalarData *vor_data_scaled = disp->pida->vor_data_scaled;
    GpivScalarData *sstrain_data = disp->pida->sstrain_data;
    GpivScalarData *sstrain_data_scaled = disp->pida->sstrain_data_scaled;
    GpivScalarData *nstrain_data = disp->pida->nstrain_data;
    GpivScalarData *nstrain_data_scaled = disp->pida->nstrain_data_scaled;
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");

    
    *index_y = 0;
    *index_x = 0;

    if ((disp->pida->exist_piv && disp->display_piv
         && GTK_CHECK_MENU_ITEM(view_piv_display0)->active
         && piv_data->ny == disp->intreg->data->ny 
         && piv_data->nx == disp->intreg->data->nx
         && piv_data->point_y[0][0] == disp->intreg->data->point_y[0][0]
         && piv_data->point_x[0][0] == disp->intreg->data->point_x[0][0]
         && piv_data->point_y[piv_data->ny - 1][piv_data->nx - 1] == 
         disp->intreg->data->point_y[disp->intreg->data->ny - 1]
         [disp->intreg->data->nx - 1]
         && piv_data->point_x[piv_data->ny - 1][piv_data->nx - 1] == 
         disp->intreg->data->point_x[disp->intreg->data->ny - 1]
         [disp->intreg->data->nx - 1] )
        || (disp->pida->exist_piv && disp->display_piv
            && !GTK_CHECK_MENU_ITEM(view_piv_display0)->active)
        ) {
        
        search_nearest_index(disp, x, y, index_x, index_y, DATA_TYPE__PIV);
        if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
            highlight_intreg(*index_y, *index_x, disp);
        }
        

        if (disp->pida->exist_vor 
            && disp->display_scalar == SHOW_SC_VORTICITY) {
            if (disp->pida->scaled_piv) {
                create_msg_display_with_pivscdata (piv_data_scaled, 
                                                   vor_data_scaled,
                                                   "vor",
                                                   *index_y, 
                                                   *index_x,
                                                   disp->pida->scaled_piv);
            } else {
                create_msg_display_with_pivscdata (piv_data, 
                                                   vor_data,
                                                   "vor",
                                                   *index_y, 
                                                   *index_x,
                                                   disp->pida->scaled_piv);
            }
            
        } else if (disp->pida->exist_sstrain 
                   && disp->display_scalar == SHOW_SC_SSTRAIN) {
            if (disp->pida->scaled_piv) {
                create_msg_display_with_pivscdata (piv_data_scaled, 
                                                   sstrain_data_scaled,
                                                   "sstrain",
                                                   *index_y, 
                                                   *index_x,
                                                   disp->pida->scaled_piv);
            } else {
                create_msg_display_with_pivscdata (piv_data, 
                                                   sstrain_data,
                                                   "sstrain",
                                                   *index_y, 
                                                   *index_x,
                                                   disp->pida->scaled_piv);
            }
            
        } else if (disp->pida->exist_nstrain
                   && disp->display_scalar == SHOW_SC_NSTRAIN) {

            if (disp->pida->scaled_piv) {
                create_msg_display_with_pivscdata (piv_data_scaled, 
                                                   nstrain_data_scaled,
                                                   "nstrain",
                                                   *index_y, 
                                                   *index_x,
                                                   disp->pida->scaled_piv);
            } else {
                create_msg_display_with_pivscdata (piv_data, 
                                                   nstrain_data,
                                                   "nstrain",
                                                   *index_y, 
                                                   *index_x,
                                                   disp->pida->scaled_piv);
            }
            
        } else {
            if (disp->pida->scaled_piv) {
                create_msg_display_with_pivdata (piv_data_scaled,
                                                 *index_y, 
                                                 *index_x,
                                                 disp->pida->scaled_piv);
                
            } else {
                create_msg_display_with_pivdata (piv_data,
                                                 *index_y, 
                                                 *index_x,
                                                 disp->pida->scaled_piv);
            }
        }
        

/*
 * piv data exist, but are not displayed
 * displays eventually piv-derived scalar data
 */
    } else if ((disp->pida->exist_piv 
                && !disp->display_piv
                && GTK_CHECK_MENU_ITEM(view_piv_display0)->active
                && piv_data->ny == disp->intreg->data->ny 
                && piv_data->nx == disp->intreg->data->nx
                && piv_data->point_y[0][0] == disp->intreg->data->point_y[0][0]
                && piv_data->point_x[0][0] == disp->intreg->data->point_x[0][0]
                && piv_data->point_y[piv_data->ny - 1][piv_data->nx - 1] == 
                disp->intreg->data->point_y[disp->intreg->data->ny - 1]
                [disp->intreg->data->nx - 1]
                && piv_data->point_x[piv_data->ny - 1][piv_data->nx - 1] == 
                disp->intreg->data->point_x[disp->intreg->data->ny - 1]
                [disp->intreg->data->nx - 1]
                && ((disp->pida->exist_vor 
                     && disp->display_scalar == SHOW_SC_VORTICITY)
                    || (disp->pida->exist_sstrain 
                        && disp->display_scalar == SHOW_SC_SSTRAIN)
                    || (disp->pida->exist_nstrain && 
                        disp->display_scalar == SHOW_SC_NSTRAIN))
                )
               || (disp->pida->exist_piv 
                   && !disp->display_piv
                   && !GTK_CHECK_MENU_ITEM(view_piv_display0)->active
                   && ((disp->pida->exist_vor 
                        && disp->display_scalar == SHOW_SC_VORTICITY)
                       || (disp->pida->exist_sstrain 
                           && disp->display_scalar == SHOW_SC_SSTRAIN)
                       || (disp->pida->exist_nstrain 
                           && disp->display_scalar == SHOW_SC_NSTRAIN))
                   )
               ) {
        
        search_nearest_index(disp, x, y, index_x, index_y, 
                             DATA_TYPE__PIV);
        if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
            highlight_intreg(*index_y, *index_x, disp);
        }

        if (disp->pida->exist_vor && 
            disp->display_scalar == SHOW_SC_VORTICITY) {
            if (disp->pida->scaled_piv) {
                create_msg_display_with_scdata(vor_data_scaled,
                                               "vor",
                                               *index_y, 
                                               *index_x,
                                               disp->pida->scaled_piv);
            } else {
                create_msg_display_with_scdata(vor_data,
                                               "vor",
                                               *index_y, 
                                               *index_x,
                                               disp->pida->scaled_piv);
            }
            
        } else if (disp->pida->exist_sstrain && 
                   disp->display_scalar == SHOW_SC_SSTRAIN) {
            if (disp->pida->scaled_piv) {
                create_msg_display_with_scdata(sstrain_data_scaled,
                                               "sstrain",
                                               *index_y, 
                                               *index_x,
                                               disp->pida->scaled_piv);
            } else {
                create_msg_display_with_scdata(sstrain_data,
                                               "sstrain",
                                               *index_y, 
                                               *index_x,
                                               disp->pida->scaled_piv);
            }
            
        } else if (disp->pida->exist_nstrain 
                   && disp->display_scalar == SHOW_SC_NSTRAIN) {
            if (disp->pida->scaled_piv) {
                create_msg_display_with_scdata(nstrain_data_scaled,
                                               "nstrain",
                                               *index_y, 
                                               *index_x,
                                               disp->pida->scaled_piv);
            } else {
                create_msg_display_with_scdata(nstrain_data,
                                               "nstrain",
                                               *index_y, 
                                               *index_x,
                                               disp->pida->scaled_piv);
            }
        }
        
/*
 * PIV data (and resulting derivatives) do not exist are and not displayed
 * Interrogation area's are displayed
 */
    } else if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
        search_nearest_index(disp, x, y, index_x, index_y, DATA_TYPE__INTREG);
        highlight_intreg(*index_y, *index_x, disp);
        
        g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%3.2f y=%3.2f i=%d j=%d ",
                   disp->intreg->data->point_x[*index_y][*index_x],
                   disp->intreg->data->point_y[*index_y][*index_x], 
                   *index_y, *index_x);
        
        
/*
 * Only image is displayed
 */
    } else if (disp->display_backgrnd == SHOW_BG_IMG1) {
        if (disp->pida->scaled_piv) {
            g_snprintf(msg_display, GPIV_MAX_CHARS, 
                       "xp=%d px  ==>  %5.4f m   yp=%d px  ==>  %5.4f m   img #1: pixval=%d",
                       x,
                       disp->img->image->header->s_scale * x * 1e-3
                       + disp->img->image->header->z_off_x, 
                       y,
                       disp->img->image->header->s_scale * y * 1e-3
                       + disp->img->image->header->z_off_y,
                       disp->img->image->frame1[y][x]
                       );
        } else {
            g_snprintf(msg_display, GPIV_MAX_CHARS, 
                       "xp=%d px yp=%d px   img #1: pixval=%d",
                       x, y,
                       disp->img->image->frame1[y][x]
                       );
        }
        
    } else if (disp->display_backgrnd == SHOW_BG_IMG2) {
        if (disp->pida->scaled_piv) {
            g_snprintf(msg_display, GPIV_MAX_CHARS, 
                       "xp=%d px  ==>  %5.4f m   yp=%d px  ==>  %5.4f m   img #1: pixval=%d",
                       x,
                       disp->img->image->header->s_scale * x  * 1e-3
                       + disp->img->image->header->z_off_x, 
                       y,
                       disp->img->image->header->s_scale * y * 1e-3
                       + disp->img->image->header->z_off_y,
                       disp->img->image->frame2[y][x]
                       );
        } else {
            g_snprintf(msg_display, GPIV_MAX_CHARS, 
                       "xp=%d px yp=%d px   img #2: pixval=%d",
                       x, y,
                       disp->img->image->frame2[y][x]
                       );
        }
        
    } else {
        g_snprintf(msg_display, GPIV_MAX_CHARS, "No data are displayed");
    }
    
}



static void
canvas_display_button_release__AOI_MS (Display *disp, 
                                      GpivConsole *gpiv,
                                      GtkWidget *view_piv_display0,
                                      gdouble x, 
                                      gdouble y
                                      )
/*-----------------------------------------------------------------------------
* Performs action on mouse button release when AOI_MS has been enabled
*/
{
    PivEval *eval = gpiv->piveval;
    gdouble l_col_start, l_col_end, l_row_start, l_row_end;


    if (gci_aoi != NULL) destroy_rect();

    if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
        destroy_all_intregs(disp);
    }


/*     g_message ("canvas_display_button_release__AOI_MS:: x = %f y = %f" */
/*                " xgrab_first = %f ygrab_first = %f", */
/*                x, y, disp->xgrab_first, disp->ygrab_first); */

    if (x >= disp->xgrab_first) {
        l_col_start = disp->xgrab_first / disp->zoom_factor;
        l_col_end = x / disp->zoom_factor;
        disp->pida->piv_par->col_start = (gint) l_col_start;
        disp->pida->piv_par->col_end = (gint) l_col_end;
    } else {
        l_col_end = disp->xgrab_first / disp->zoom_factor;
        l_col_start = x / disp->zoom_factor;
        disp->pida->piv_par->col_end = (gint) l_col_end;
        disp->pida->piv_par->col_start = (gint) l_col_start;
    }
    
    if (y >= disp->ygrab_first) {
        l_row_start = disp->ygrab_first / disp->zoom_factor;
        l_row_end = y / disp->zoom_factor;
        disp->pida->piv_par->row_start = (gint) l_row_start;
        disp->pida->piv_par->row_end = (gint) l_row_end;
    } else {
        l_row_end = disp->ygrab_first / disp->zoom_factor;
        l_row_start = y / disp->zoom_factor;
        disp->pida->piv_par->row_end = (gint) l_row_end;
        disp->pida->piv_par->row_start = (gint) l_row_start;
        
    }
    

    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (eval->spinbutton_colstart), 
                               (gdouble)l_col_start);

    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (eval->spinbutton_colend), 
                               (gdouble) l_col_end);

    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (eval->spinbutton_rowstart), 
                               (gdouble) l_row_start);

    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (eval->spinbutton_rowend), 
                               (gdouble) l_row_end);

    if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
        create_all_intregs (disp);
    }

}



static void
canvas_display_motion_notify__DRAGINT_MS (Display *disp, 
                                          gint x, 
                                          gint y,
                                          gint *index_x, 
                                          gint *index_y
                                          )
/*-----------------------------------------------------------------------------
 * displaces interrogation area nearest to pointer
 */
{
    if (disp->intreg->exist_int) {
        if (!disp->index_old) {
            disp->index_x_old = *index_x;
            disp->index_y_old = *index_y;
            disp->index_old = TRUE;
        }
        
        if (*index_x == disp->index_x_old 
            && *index_y == disp->index_y_old) {
            
            gnome_canvas_item_set 
                (GNOME_CANVAS_ITEM(disp->intreg->
                                   gci_intreg1[*index_y][*index_x]), 
                 "x1", (double) x - disp->pida->piv_par->int_size_f / 2,
                 "y1", (double) y - disp->pida->piv_par->int_size_f / 2,
                 "x2", (double) x + disp->pida->piv_par->int_size_f / 2,
                 "y2", (double) y + disp->pida->piv_par->int_size_f / 2,
                 NULL);
            
            gnome_canvas_item_set 
                (GNOME_CANVAS_ITEM(disp->intreg->
                                   gci_intreg2[*index_y][*index_x]), 
                 "x1", (double) x - disp->pida->piv_par->int_size_i / 
                 2 + disp->pida->piv_par->pre_shift_col,
                 "y1", (double) y - disp->pida->piv_par->int_size_i / 
                 2 + disp->pida->piv_par->pre_shift_row,
                 "x2", (double) x + disp->pida->piv_par->int_size_i / 
                 2 + disp->pida->piv_par->pre_shift_col,
                 "y2", (double) y + disp->pida->piv_par->int_size_i / 
                 2 + disp->pida->piv_par->pre_shift_row,
                 NULL);
/*
 * put the interrogation area back to its original location
 */
        } else {
            update_intreg1(disp, disp->index_y_old, disp->index_x_old);
            update_intreg2(disp, disp->index_y_old, disp->index_x_old);
        }
    }
}



static void
canvas_display_motion_notify__SINGLE_POINT_MS (Display *disp, 
                                              gint x, 
                                              gint y
                                              )
/*-----------------------------------------------------------------------------
* Interrogates at a single arbitrary point in the image.
* I am using the first interrogation area ([0][0]) temporarly for 
* displaying
*/
{
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");


    if (disp->intreg->gci_intreg1[0][0] != NULL && 
        disp->intreg->gci_intreg2[0][0] != NULL
        ) {
        if (!GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {

           if (disp->intreg->gci_intreg1[0][0] != NULL) {
             gnome_canvas_item_show 
                (GNOME_CANVAS_ITEM(disp->intreg->gci_intreg1[0][0]));
           }
           if (disp->intreg->gci_intreg2[0][0] != NULL) {
               gnome_canvas_item_show 
                   (GNOME_CANVAS_ITEM(disp->intreg->gci_intreg2[0][0]));
           }
           disp->display_intregs = TRUE;
        }
        
        if (disp->pida->exist_piv && disp->pida->scaled_piv) {
            g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%3.2f mm  y=%3.2f mm", 
                       (gfloat) x * disp->img->image->header->s_scale *10e2
                       + (gfloat) disp->img->image->header->z_off_x *10e2,
                       (gfloat) y * disp->img->image->header->s_scale *10e2
                       + (gfloat) disp->img->image->header->z_off_y *10e2);
        } else {
            g_snprintf(msg_display, GPIV_MAX_CHARS, "x=%d px y=%d px", x, y);
        }
        
        gnome_canvas_item_set 
            (GNOME_CANVAS_ITEM(disp->intreg->gci_intreg1[0][0]), 
             "x1", (double) x - disp->pida->piv_par->int_size_f / 2,
             "y1", (double) y - disp->pida->piv_par->int_size_f / 2,
             "x2", (double) x + disp->pida->piv_par->int_size_f / 2,
             "y2", (double) y + disp->pida->piv_par->int_size_f / 2,
             NULL);
        
        gnome_canvas_item_set 
            (GNOME_CANVAS_ITEM(disp->intreg->gci_intreg2[0][0]), 
             "x1", (double) x - disp->pida->piv_par->int_size_f / 2 
             + disp->pida->piv_par->pre_shift_col,
             "y1", (double) y - disp->pida->piv_par->int_size_f / 2 
             + disp->pida->piv_par->pre_shift_row,
             "x2", (double) x + disp->pida->piv_par->int_size_f / 2 
             + disp->pida->piv_par->pre_shift_col,
             "y2", (double) y + disp->pida->piv_par->int_size_f / 2 
             + disp->pida->piv_par->pre_shift_row,
             NULL);
        
    } else {
        create_intreg1 (disp, 0, 0);
        create_intreg2 (disp, 0, 0);
    }
    
}



static char *
canvas_display_button_press__SINGLE_AREA_MS (GpivConsole *gpiv,
                                            Display *disp,
                                            gint x, 
                                            gint y
                                            )
/*-----------------------------------------------------------------------------
* Performs action on mouse button press when SINGLE_AREA_MS has been enabled
*/
{
    char *err_msg = NULL;
    gint index_x = 0, index_y = 0;
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");


    if (disp->pida->exist_piv
/*         && GTK_CHECK_MENU_ITEM(view_piv_display0)->active */
/*         && disp->intreg->data->nx == disp->pida->piv_data->nx */
/*         && disp->intreg->data->ny == disp->pida->piv_data->ny */

        ) {

/*         search_nearest_index(disp, x, y, &index_x, &index_y,  */
/*                              DATA_TYPE__INTREG); */
        search_nearest_index (disp, x, y, &index_x, &index_y, 
                             DATA_TYPE__PIV);

        
        if (m_select == SINGLE_AREA_MS) {
/*             if (disp->intreg->data->point_x[index_y][index_x] ==  */
/*                 disp->pida->piv_data->point_x[index_y][index_x] */
/*                 && disp->intreg->data->point_y[index_y][index_x] ==  */
/*                 disp->pida->piv_data->point_y[index_y][index_x] */
/*                 ) { */
                m_select_index_x = index_x;
                m_select_index_y = index_y;
/*                 gpiv_warning("canvas_display_button_press__SINGLE_AREA_MS:: index_x=%d index_y = %d", index_x, index_y); */
/*             } else { */
/*                 set__NO_MS(gpiv, disp); */
/*                 err_msg = "Interrogation area's have to be at the same positions \nas the already existing piv data"; */
/*                 warning_gpiv(err_msg); */
/*                 return err_msg; */
/*             } */
/*         g_message("x=%d point_x=%d y=%d point_x=%d */

        } else if (m_select == DRAG_MS) {
            m_select_index_x = index_x;
            m_select_index_y = index_y;
            disp->pida->piv_data->point_x[m_select_index_y]
                [m_select_index_x] = (float) x;
            disp->pida->piv_data->point_y[m_select_index_y]
                [m_select_index_x] = (float) y;
/* BUGFIXED ? */
            if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
                disp->intreg->data->point_x[m_select_index_y]
                    [m_select_index_x] = (float) x;
                disp->intreg->data->point_y[m_select_index_y]
                    [m_select_index_x] = (float) y;
            }
        } else {
            err_msg = "canvas_display_button_press__SINGLE_AREA_MS: should not arrive here";
            error_gpiv(err_msg);
        }
        
    }
    

    return err_msg;
}



static char *
canvas_display_button_release__SINGLE_AREA_MS (GpivConsole *gpiv,
                                               Display *disp,
                                               gdouble x, 
                                               gdouble y
                                               )
/*-----------------------------------------------------------------------------
* Performs action on mouse button release when SINGLE_AREA_MS has been enabled
*/
{
    char *err_msg = NULL;
    gint index_x = 0, index_y = 0;
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");
    

/*     g_message ("canvas_display_button_release__SINGLE_AREA_MS:: x = %f y = %f" */
/*                " xgrab_first = %f ygrab_first = %f " */
/*                "m_select_index_x = %d m_select_index_y = %d", */
/*                x, y, disp->xgrab_first, disp->ygrab_first, */
/*                m_select_index_x, m_select_index_y */
/*                ); */

    if (disp->pida->exist_piv
/*         && GTK_CHECK_MENU_ITEM(view_piv_display0)->active */
/*         && disp->intreg->data->nx == disp->pida->piv_data->nx */
/*         && disp->intreg->data->ny == disp->pida->piv_data->ny */

        ) {
/*
 * There will only be action if the pointer, during pressing the button (as
 * defined in canvas_display_button_press__SINGLE_AREA_MS), is at identical
 * position as during releasing the button->
 */
        search_nearest_index(disp, (gint) x, (gint) y, &index_x, &index_y, 
                             DATA_TYPE__PIV);
/*         gpiv_warning("canvas_display_button_release__SINGLE_AREA_MS:: index_x=%d index_y = %d", index_x, index_y); */



        if (m_select_index_x == index_x
            && m_select_index_y == index_y
            ) {
            gl_piv_par->int_point_col = m_select_index_x;
            gl_piv_par->int_point_row = m_select_index_y;
            disp->pida->piv_par = gpiv_piv_cp_parameters (gl_piv_par);

/*             g_message ("canvas_display_button_release__SINGLE_AREA_MS:: calling interrogate"); */

            disp->pida->piv_data = interrogate_img (disp->img->image, 
                                                    disp->pida->piv_par,
                                                    disp->pida->valid_par,
                                                    gpiv);

/*             exec_piv (gpiv); */
/*             g_message ("canvas_display_button_release__SINGLE_AREA_MS:: back from interrogate"); */
            if (disp->pida->gci_vector[m_select_index_y][m_select_index_x] 
                != NULL) {
                update_vector (disp->pida, m_select_index_y, m_select_index_x);
            } else {
                create_vector (disp->pida, m_select_index_y, m_select_index_x);
            }
            
/*             g_message ("canvas_display_button_release__SINGLE_AREA_MS:: display vector"); */
            /*                 disp->pida->exist_piv = TRUE; */
            disp->display_piv = TRUE;
            if (m_select == DRAG_MS) {
                if (disp->intreg->
                    gci_intreg1[m_select_index_y][m_select_index_x] 
                    != NULL)
                    update_intreg1 (disp, m_select_index_y, m_select_index_x);
                if (disp->intreg->
                    gci_intreg2[m_select_index_y][m_select_index_x] 
                    != NULL) 
                    update_intreg2 (disp, m_select_index_y, m_select_index_x);
            }
            
        } else {
            err_msg = "moved pointer too far away from \nintar to be interrogated";
            return err_msg;
        }
        
    } else {
/*         err_msg = "Interrogation area's and piv data must already exist \n and must be of identic quantity!"; */
        err_msg = "Piv data must exist!";
        warning_gpiv (err_msg);
        return err_msg;
    }
    
    
    if (!shift_pressed) set__NO_MS(gpiv, disp);    


    return err_msg;
}



static void
canvas_display_button_press__SINGLE_POINT_MS (GpivConsole *gpiv,
                                              Display *disp,
                                              gint x, 
                                              gint y
                                              )
/*-----------------------------------------------------------------------------
* Performs action on mouse button release when SINGLE_POINT_MS has 
* been enabled
*/
{
    GtkWidget *view_piv_display0 = 
                        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                                            "view_piv_display0");


    if (disp->pida->exist_piv) {
        destroy_all_vectors(disp->pida);
        gpiv_free_pivdata (disp->pida->piv_data);
        disp->pida->exist_piv = FALSE;
        if (disp->pida->scaled_piv) {
            gpiv_free_pivdata (disp->pida->piv_data_scaled);
            disp->pida->scaled_piv = FALSE;
        }
    }
    
    free_post_bufmems(disp);
    if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
        destroy_all_intregs(disp);
    }
    m_select_index_x = 0;
    m_select_index_y = 0;
    disp->pida->piv_par->int_point_col = (float) x;
    disp->pida->piv_par->int_point_row = (float) y;
    disp->pida->piv_data = gpiv_alloc_pivdata (1, 1);
    disp->pida->exist_piv = TRUE;
    
    disp->pida->piv_data->point_x[0][0] = (float) x;
    disp->pida->piv_data->point_y[0][0] = (float) y;
    
    disp->intreg->data->nx = 1; 
    disp->intreg->data->ny = 1; 
    create_all_intregs(disp);
    disp->intreg->exist_int = TRUE;
    
    disp->intreg->data->point_x[0][0] = (float) x; 
    disp->intreg->data->point_y[0][0] = (float) y;
    show_all_intregs(disp);
    disp->display_intregs = TRUE;
    
    disp->pida->piv_data = interrogate_img (disp->img->image, 
                                            disp->pida->piv_par,
                                            disp->pida->valid_par,
                                            gpiv);
    
    disp->display_piv = TRUE;
    
    create_all_vectors(disp->pida);
    gnome_canvas_update_now(GNOME_CANVAS(disp->canvas));
    
    if (!shift_pressed) set__NO_MS(gpiv, disp);
}



static void
canvas_display_button_release__HVLINE_MS (Display *disp, 
                                           GpivConsole *gpiv,
                                           GtkWidget *view_piv_display0, 
                                           gdouble x, 
                                           gdouble y
                                           )
/*-----------------------------------------------------------------------------
 * Performs action on mouse button release when H_LINE_MS or V_LINE_MS has 
 * been enabled
 */
{
    gdouble l_col_start,l_col_end, l_row_start, l_row_end;
    gboolean reset_display_intregs;


/*     g_message ("canvas_display_button_release__HVLINE_MS:: x = %f y = %f" */
/*                " xgrab_first = %f ygrab_first = %f", */
/*                x, y, disp->xgrab_first, disp->ygrab_first); */

    if (GTK_CHECK_MENU_ITEM (view_piv_display0)->active) {
/*
 * Remove exixting intreg contours
 * Disable displaying temporarly to avoid several calls to 
 * the 'on_spinbutton_piv_int' function
 */
        destroy_all_intregs (disp);
        gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM 
                                        (view_piv_display0),
                                            FALSE);
        reset_display_intregs = TRUE;
    }

/*     if (disp->pida->exist_piv) { */
/*         destroy_all_vectors(disp->pida); */
/*         disp->display_piv = FALSE; */
/*     } */
    
    
    m_select_index_x = 0;
    m_select_index_y = 0;
    
/*
 * Vertical line selected
 */
    if (m_select == V_LINE_MS) {
        if (y >= disp->ygrab_first) {
            l_row_start = (gdouble) (disp->ygrab_first / disp->zoom_factor);
            l_row_end = (gdouble) (y / disp->zoom_factor);
            disp->pida->piv_par->row_start = (gint) l_row_start;
            disp->pida->piv_par->row_end = (gint) l_row_end;
            disp->pida->piv_par->int_line_row_end = (gint) l_row_end;
        } else {
            l_row_start = (gdouble) (y / disp->zoom_factor);
            l_row_end = (gdouble) (disp->ygrab_first / disp->zoom_factor);
            disp->pida->piv_par->row_start = (gint) l_row_start;
            disp->pida->piv_par->row_end = (gint) l_row_end;
            disp->pida->piv_par->int_line_row_end = (gint) l_row_end;
       }
        
        l_col_start = (gdouble) (disp->xgrab_first / disp->zoom_factor -
                                 disp->pida->piv_par->int_size_i / 2 + 1);
        disp->pida->piv_par->col_start = (gint) l_col_start;
        
        l_col_end = (gdouble) (disp->xgrab_first / disp->zoom_factor +
                               disp->pida->piv_par->int_size_i / 2 + 
                               disp->pida->piv_par->pre_shift_col);
        disp->pida->piv_par->col_end = (gint) l_col_end;
        

/*
 * Horizontal line selected
 */
    } else if (m_select == H_LINE_MS) {
        if (x >= disp->xgrab_first) {
            l_col_start = (gdouble) (disp->xgrab_first / disp->zoom_factor);
            l_col_end = (gdouble) (x / disp->zoom_factor);
            disp->pida->piv_par->col_start = (gint)  l_col_start;
            disp->pida->piv_par->col_end = (gint) l_col_end;
            disp->pida->piv_par->int_line_col_end = (gint) l_col_end;
        } else {
            l_col_start = (gdouble) (x / disp->zoom_factor);
            l_col_end = (gdouble) (disp->xgrab_first / disp->zoom_factor);
            disp->pida->piv_par->col_start = (gint) l_col_start;
            disp->pida->piv_par->col_end = (gint) l_col_end;
            disp->pida->piv_par->int_line_col_end = (gint) l_col_end;
         }
        
        l_row_start = (gdouble) (disp->ygrab_first / disp->zoom_factor -
                                 disp->pida->piv_par->int_size_i / 2 + 1);
        disp->pida->piv_par->row_start = (gint) l_row_start;
        
        l_row_end = (gdouble) (disp->ygrab_first / disp->zoom_factor +
                               disp->pida->piv_par->int_size_i / 2 + 
                               disp->pida->piv_par->pre_shift_col);
        disp->pida->piv_par->row_end = (gint) l_row_end;


    } else {
        g_warning("canvas_display_button_release__HVLINE_MS: H_LINE_MS or V_LINE_MS inactive");
    }

    if (gci_line != NULL) destroy_line();
    if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
        create_all_intregs(disp);
    }

/*
 * updating entry's for col/row_start_end
 */
/*     g_message ("canvas_display_button_release__HVLINE_MS:: zoom_factor  = %f col_start = %f col_end = %f" */
/*                " row_start = %f row_end = %f", */
/*                disp->zoom_factor, l_col_start, l_col_end, l_row_start,  l_row_end); */

    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (gpiv->piveval->spinbutton_colstart), 
                               (gdouble) l_col_start);
 
    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (gpiv->piveval->spinbutton_rowstart), 
                               (gdouble) l_row_start);

    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (gpiv->piveval->spinbutton_colend), 
                               (gdouble) l_col_end);

/*
 * The last call to on_spinbutton_piv_int will update the contours of the intregs
 */
    if (reset_display_intregs) {
/*         g_message ("release__HV:: resetting"); */
        gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM 
                                        (view_piv_display0),
                                            TRUE);
    }

    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (gpiv->piveval->spinbutton_rowend), 
                               (gdouble) l_row_end);

    if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
/*         create_all_intregs (disp); */
    }

}



static void
canvas_display_button_press__DISABLE_POINT_MS (Display *disp, 
					       gint x, 
					       gint y
					       )
/*-----------------------------------------------------------------------------
 * Performs action on mouse button press when DISABLE_POINT_MS has 
 * been enabled
 */
{
  gchar *err_msg;
  gint index_x = 0, index_y = 0;
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");
    

    if ( m_select == DISABLE_POINT_MS ) {
        if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
            search_nearest_index(disp, x, y, &index_x, &index_y,
                                 DATA_TYPE__INTREG);
        } else {
            search_nearest_index(disp, x, y, &index_x, &index_y,
                                 DATA_TYPE__PIV);
        }

        disp->pida->piv_data->peak_no[index_y][index_x] = -1;
        disp->pida->piv_data->snr[index_y][index_x] = GPIV_SNR_DISABLE;
        
        if (disp->pida->post_par->set == TRUE) {
            disp->pida->piv_data->dx[index_y][index_x] = 
                disp->pida->post_par->set_dx;
            disp->pida->piv_data->dy[index_y][index_x] = 
                disp->pida->post_par->set_dx;
        }
        update_vector(disp->pida, index_y, index_x);
        
    } else if ( m_select == ENABLE_AREA_MS ) {
        enable_col_start = x;
        enable_row_start = y;
        assert( gci_aoi == NULL);
        gci_aoi = 
            gnome_canvas_item_new (gnome_canvas_root
                                   (GNOME_CANVAS(disp->canvas)),
                                   gnome_canvas_rect_get_type(),
                                   "x1", (double) x,
                                   "y1", (double) y,
                                   "x2", (double) x,
                                   "y2", (double) y,
                                   "outline_color", "yellow",
                                   "width_units", (double) THICKNESS,
                                   NULL);


    } else if ( m_select == DISABLE_AREA_MS) {
        enable_col_start = x;
        enable_row_start = y;
        assert( gci_aoi == NULL);
        gci_aoi = 
            gnome_canvas_item_new (gnome_canvas_root
                                   (GNOME_CANVAS(disp->canvas)),
                                   gnome_canvas_rect_get_type(),
                                   "x1", (double) x,
                                   "y1", (double) y,
                                   "x2", (double) x,
                                   "y2", (double) y,
                                   "outline_color", "yellow",
                                   "width_units", (double) THICKNESS,
                                   NULL);
    } else {
        err_msg = _("no image or piv data");
        warning_gpiv(err_msg);
    }
    
}



static void
set__NO_MS (GpivConsole *gpiv,
           Display *disp
           )
/*-----------------------------------------------------------------------------
 * Reset mouse selection to inactive
 */
{
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                (gpiv->piveval->radiobutton_mouse_1), 
                                TRUE);
    cursor = gdk_cursor_new (GDK_DOTBOX);
    gdk_window_set_cursor (disp->mwin->window, cursor);
}



static void
search_nearest_index (Display *disp, 
                      gint x, 
                      gint y, 
                      gint *index_x, 
                      gint *index_y,
                      gint data_type
                      )
/*-----------------------------------------------------------------------------
* Search nearest index belongin to x an y points
*/
{
    gint i, j, x_min = 10e4, y_min = 10e4, dif;


    if (data_type == DATA_TYPE__INTREG) {
        for (i = 0, j = 0; i < disp->intreg->data->ny; i++) {
            dif = abs(y - (int) disp->intreg->data->point_y[i][j]);
            if (dif < y_min) {
                y_min = dif;
                *index_y = i;
            }
        }

        for (i = 0, j = 0; j < disp->intreg->data->nx; j++) {
            dif = abs(x - (int) disp->intreg->data->point_x[i][j]);
            if (dif < x_min) {
                x_min = dif;
                *index_x = j;
            }
        }


    } else if (data_type == DATA_TYPE__PIV) {
        for (i = 0, j = 0; i < disp->pida->piv_data->ny; i++) {
            dif = abs(y - (int) disp->pida->piv_data->point_y[i][j]);
            if (dif < y_min) {
                y_min = dif;
                *index_y = i;
            }
        }

        for (i = 0, j = 0; j < disp->pida->piv_data->nx; j++) {
            dif = abs(x - (int) disp->pida->piv_data->point_x[i][j]);
            if (dif < x_min) {
                x_min = dif;
                *index_x = j;
            }
        }

    } else {
        g_warning("search_nearest_index: Inavalid Data Type");
    }
    
}



static void
highlight_intreg (gint index_y, 
                  gint index_x, 
                  Display *disp
                  )
/*-----------------------------------------------------------------------------
* Highlights first and second interrogation area's
*/
{
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");


    if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active
        && index_y <= disp->intreg->data->ny 
        && index_x <= disp->intreg->data->nx) {
        
        if (disp->intreg->gci_intreg1[disp->index_y_old][disp->index_x_old] 
            != NULL) {
            gnome_canvas_item_set(disp->intreg->gci_intreg1
                                  [disp->index_y_old][disp->index_x_old], 
                                  "outline_color", "red", 
                                  NULL);
        }
        
        if (disp->intreg->gci_intreg2[disp->index_y_old][disp->index_x_old] 
            != NULL) {
            gnome_canvas_item_set(disp->intreg->gci_intreg2
                                  [disp->index_y_old][disp->index_x_old],
                                  "outline_color", "blue", 
                                  NULL);
        }

        if (disp->intreg->gci_intreg1[index_y][index_x] != NULL) {
            gnome_canvas_item_set(disp->intreg->gci_intreg1[index_y][index_x],
                                  "outline_color", "yellow", 
                                  NULL);
            gnome_canvas_item_raise_to_top(disp->intreg->
                                           gci_intreg1[index_y][index_x]);
       }

        if (disp->intreg->gci_intreg2[index_y][index_x] != NULL) {
            gnome_canvas_item_set(disp->intreg->gci_intreg2[index_y][index_x],
                                  "outline_color", "green",
                                  NULL);
            gnome_canvas_item_raise_to_top(disp->intreg->
                                           gci_intreg2[index_y][index_x]);
        }
        
    }
}



static void
create_msg_display_with_pivdata (GpivPivData *piv_data, 
                                 gint index_y, 
                                 gint index_x,
                                 gint scale
                                 )
/*-----------------------------------------------------------------------------
 * Displays message with piv data values
 */
{

    if (scale) {
        if (piv_data->snr[index_y][index_x] == GPIV_SNR_NAN) {
            g_snprintf (msg_display, GPIV_MAX_CHARS, 
                        "xp=%3.2fmm yp=%3.2fmm %s",
                        piv_data->point_x[index_y][index_x] * 1e3,
                        piv_data->point_y[index_y][index_x] * 1e3,
                        _("Disabled: not a number"));
        } else if (piv_data->snr[index_y][index_x] == GPIV_SNR_DISABLE) {
            g_snprintf (msg_display, GPIV_MAX_CHARS, 
                        "xp=%3.2fmm yp=%3.2fmm %s",
                        piv_data->point_x[index_y][index_x] * 1e3,
                        piv_data->point_y[index_y][index_x] * 1e3,
                        _("Disabled manually"));
        } else {
            g_snprintf (msg_display, GPIV_MAX_CHARS, 
                        "xp=%3.2fmm yp=%3.2fmm U=%3.2fm/s V=%3.2fm/s"
                        " snr=%3.2f peak #%d",
                        piv_data->point_x[index_y][index_x] * 1e3,
                        piv_data->point_y[index_y][index_x] * 1e3,
                        piv_data->dx[index_y][index_x],
                        piv_data->dy[index_y][index_x],
                        piv_data->snr[index_y][index_x],
                        piv_data->peak_no[index_y][index_x]);
        }

    } else {
        if (piv_data->snr[index_y][index_x] == GPIV_SNR_NAN) {
            g_snprintf (msg_display, GPIV_MAX_CHARS, 
                        "xp=%3.0fpx yp=%3.0fpx %s",
                        piv_data->point_x[index_y][index_x],
                        piv_data->point_y[index_y][index_x],
                        _("Disabled: not a number"));
        } else if (piv_data->snr[index_y][index_x] == GPIV_SNR_DISABLE) {
            g_snprintf (msg_display, GPIV_MAX_CHARS, 
                        "xp=%3.0fpx yp=%3.0fpx %s",
                        piv_data->point_x[index_y][index_x],
                        piv_data->point_y[index_y][index_x],
                        _("Disabled manually"));
        } else {
            g_snprintf (msg_display, GPIV_MAX_CHARS, 
                        "xp=%3.0fpx yp=%3.0fpx dx=%3.2fpx dy=%3.2fpx"
                        " snr=%3.2f peak #%d",
                        piv_data->point_x[index_y][index_x],
                        piv_data->point_y[index_y][index_x],
                        piv_data->dx[index_y][index_x],
                        piv_data->dy[index_y][index_x],
                        piv_data->snr[index_y][index_x],
                        piv_data->peak_no[index_y][index_x]);
        }
    }

}



static void
create_msg_display_with_scdata (GpivScalarData *sc_data,
                                gchar *sc_type,
                                gint index_y, 
                                gint index_x,
                                gint scale
                                )
/*-----------------------------------------------------------------------------
 * Displays message with scalar data values
 */
{
    if (scale) {
        g_snprintf (msg_display, GPIV_MAX_CHARS, 
                    "xp=%3.2fmm yp=%3.2fmm %s=%3.2f1/s",
                    sc_data->point_x[index_y][index_x] * 1e3,
                    sc_data->point_y[index_y][index_x] * 1e3,
                    sc_type,
                    sc_data->scalar[index_y][index_x]);
    } else {
        g_snprintf (msg_display, GPIV_MAX_CHARS, 
                    "xp=%3.0fpx yp=%3.0fpx %s=%3.2f",
                    sc_data->point_x[index_y][index_x],
                    sc_data->point_y[index_y][index_x],
                    sc_type,
                    sc_data->scalar[index_y][index_x]);
    }
}



static void
create_msg_display_with_pivscdata (GpivPivData *piv_data,
                                   GpivScalarData *sc_data,
                                   gchar *sc_type,
                                   gint index_y, 
                                   gint index_x,
                                   gint scale
                                   )
/*-----------------------------------------------------------------------------
 * Displays message with piv and scalar data values
 */
{
    if (scale) {
        g_snprintf (msg_display, GPIV_MAX_CHARS, 
                    "xp=%3.2fmm yp=%3.2fmm U=%3.2fm/s V=%3.2fm/s "
                    "snr=%3.2f peak #%d %s=%3.2f1/s",
                    piv_data->point_x[index_y][index_x] * 1e3,
                    piv_data->point_y[index_y][index_x] * 1e3,
                    piv_data->dx[index_y][index_x],
                    piv_data->dy[index_y][index_x],
                    piv_data->snr[index_y][index_x],
                    piv_data->peak_no[index_y][index_x],
                    sc_type,
                    sc_data->scalar[index_y][index_x]);
    } else {
        g_snprintf (msg_display, GPIV_MAX_CHARS, 
                    "xp=%3.0fpx yp=%3.0fpx dx=%3.2fpx dy=%3.2fpx "
                    "snr=%3.2f peak #%d %s=%3.2f",
                    piv_data->point_x[index_y][index_x],
                    piv_data->point_y[index_y][index_x],
                    piv_data->dx[index_y][index_x],
                    piv_data->dy[index_y][index_x],
                    piv_data->snr[index_y][index_x],
                    piv_data->peak_no[index_y][index_x],
                    sc_type,
                    sc_data->scalar[index_y][index_x]);
    }
}



static void
create_line (Display *disp, 
             gint x1, 
             gint y1, 
             gint x2, 
             gint y2
             )
/*-----------------------------------------------------------------------------
 */
{
    GnomeCanvasPoints *points;
    points = gnome_canvas_points_new (2);
    
/*     g_message("CREATE_LINE:: x1 = %d y1 = %d x2 = %d y2 = %d", x1, y1, x2, y2); */

    if (disp != NULL
        && gci_line == NULL) {
    
        points->coords[0] = x1;
        points->coords[1] = y1;
        points->coords[2] = x2;
        points->coords[3] = y2;
        gci_line = 
            gnome_canvas_item_new (gnome_canvas_root(GNOME_CANVAS(disp->canvas)),
                                  gnome_canvas_line_get_type(),
                                  "points", points,
                                  "fill_color", "yellow",
                                  "width_units", (double) THICKNESS,
                                  NULL);
        
        gnome_canvas_points_free (points);
    }
}


static void
update_line (gint x1, 
             gint y1, 
             gint x2, 
             gint y2
             )
/*-----------------------------------------------------------------------------
 */
{
    GnomeCanvasPoints *points;
    points = gnome_canvas_points_new (2);
    
    /*     g_message("UPDATE_LINE:: x1 = %d y1 = %d x2 = %d y2 = %d", x1, y1, x2, y2); */

    if (gci_line != NULL) {
    
        points->coords[0] = x1;
        points->coords[1] = y1;
        points->coords[2] = x2;
        points->coords[3] = y2;
        
        gnome_canvas_item_set (GNOME_CANVAS_ITEM(/* disp-> */gci_line),
                               "points", points,
                               "fill_color", "yellow",
                               "width_units", (double) THICKNESS,
                               NULL);
        
        gnome_canvas_points_free (points);
    }
}


static void
destroy_line ()
/*-----------------------------------------------------------------------------
 */
{
    if (gci_line != NULL) {
    
        gtk_object_destroy(GTK_OBJECT (gci_line));
        gci_line = NULL;
    }
}



static void
create_rect (Display *disp, 
             gint x, 
             gint y
             )
/*-----------------------------------------------------------------------------
 */
{
    if (disp != NULL
        && gci_aoi == NULL) {
    
        /*     g_message("CREATE_RECT:: x = %d y = %d", x, y); */

        gci_aoi = 
            gnome_canvas_item_new (gnome_canvas_root
                                   (GNOME_CANVAS(disp->canvas)),
                                   gnome_canvas_rect_get_type(),
                                   "x1", (double) x,
                                   "y1", (double) y,
                                   "x2", (double) x,
                                   "y2", (double) y,
                                   "outline_color", "yellow",
                                   "width_units", (double) THICKNESS,
                                   NULL);
        
    }
}


static void
update_rect (gint x, 
             gint y
             )
/*-----------------------------------------------------------------------------
 */
{
    if (gci_aoi != NULL) {
    
        gnome_canvas_item_set (GNOME_CANVAS_ITEM(gci_aoi),
                               "x2", (double) x,
                               "y2", (double) y, 
                               NULL);
    }
}


static void
destroy_rect ()
/*-----------------------------------------------------------------------------
 */
{
    if (gci_aoi != NULL) {
    
        gtk_object_destroy (GTK_OBJECT (gci_aoi));
        gci_aoi = NULL;
    }
}


gdouble
gtk_adjustment_get_lower (GtkAdjustment *adjustment)
{
    /*   g_return_val_if_fail (GTK_IS_ADJUSTMENT (adjustment), 0.); */

    return adjustment->lower;
}



static void
pane_canvas (Display *disp,
             gfloat x, 
             gfloat y
             )
/*-----------------------------------------------------------------------------
 */
{
    pane_canvas_x (disp, x);
    pane_canvas_y (disp, y);
}



static void
pane_canvas_x (Display *disp,
               gfloat x
               )
/*-----------------------------------------------------------------------------
 */
{
    GtkAdjustment *adj = GTK_ADJUSTMENT (disp->hadj);
/*     gdouble adj_value = gtk_adjustment_get_value  */
        (GTK_ADJUSTMENT (disp->hadj));
    gdouble diff;
    gdouble adj_new; 

    diff = x - disp->xgrab_first;
/*     g_message ("PANE_CANVAS_X:: x = %f x_first = %f => diff = %f",  */
/*                x, disp->xgrab_first, diff); */

    adj_new = (adj->value - diff);
/*     g_message ("PANE_CANVAS_X:: value = %f lower = %f upper = %f page_s = %f => adj_new = %f",  */
/*                adj->value, adj->lower, adj->upper, adj->page_size, adj_new); */

    if (adj_new >= adj->lower
        && adj_new <  adj->upper -  adj->page_size) {
/*         g_message ("PANE_CANVAS_X:: passed if ()"); */

        gtk_adjustment_set_value (GTK_ADJUSTMENT (disp->hadj),
                                  adj_new
                                  );
    }

}



static void
pane_canvas_y (Display *disp,
               gfloat y
               )
/*-----------------------------------------------------------------------------
 */
{
    GtkAdjustment *adj = GTK_ADJUSTMENT (disp->vadj);
/*     gdouble adj_value = gtk_adjustment_get_value  */
/*         (GTK_ADJUSTMENT (disp->vadj)); */
    gdouble diff;
    gdouble adj_new; 

    diff = y - disp->ygrab_first;
    adj_new = (adj->value - diff);
    if (adj_new >= 0
        && adj_new < adj->upper -  adj->page_size) {
        gtk_adjustment_set_value (GTK_ADJUSTMENT (disp->vadj),
                                  adj_new
                                  );
    }

}
