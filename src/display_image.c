/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */
 
/*----------------------------------------------------------------------

  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

#include "gpiv_gui.h"
#include "display_image.h"


void 
create_img (Display *disp
            )
/* ----------------------------------------------------------------------------
* Creates pixbuf image in gnome canvas
* row stride; each row is a 4-byte buffer array
*/
{

/*     GpivImagePar image_par = disp->img->image->header; */
    guchar *pos1 = NULL, *pos2 = NULL;
    gint i, j;
/*     GdkPixbuf *pixbuf1 = NULL, *pixbuf2 = NULL; */
    GdkPixbuf *px;
    guint16 fact = 1;
    gint depth = 8;

    assert (disp != NULL);
    assert (disp->img->image->frame1[0] != NULL);
    assert (disp->img->exist_img);
    
    disp->img->pixbuf1 = NULL;
    disp->img->pixbuf2 = NULL;
    
    fact = fact << (disp->img->image->header->depth - depth);
/*     g_message ("create_img:: fact = %d", fact); */

/*
 * BUGFIX: this works for rowstride
 */
    disp->img->rgb_img_width = disp->img->image->header->ncolumns * 3;
    while ((disp->img->rgb_img_width) % 4 != 0) {
        disp->img->rgb_img_width++;
    } 
    
/*
 * this is a more formal way to do it
 */
    px = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                         FALSE,
                         depth,
                         disp->img->image->header->ncolumns,
                         disp->img->image->header->nrows);


#ifdef DEBUG
    g_message("create_img:: rowstride = %d, rgb_img_width = %d",
              gdk_pixbuf_get_rowstride(px),
              disp->img->rgb_img_width);
#endif

    disp->img->rgbbuf_img1 = g_malloc (gdk_pixbuf_get_rowstride(px) *
                                      disp->img->image->header->nrows);
    
    disp->img->pixbuf1 = gdk_pixbuf_new_from_data (disp->img->rgbbuf_img1,
                                                  GDK_COLORSPACE_RGB,
                                                  FALSE,          /* gboolean has_alpha */
                                                  depth,          /* image_par->depth */
                                                  disp->img->image->header->ncolumns, 
                                                  disp->img->image->header->nrows,
                                                  disp->img->rgb_img_width, /* rowstride */ 
                                                  NULL, 
                                                  NULL);
    
    if (disp->img->gci_img1 != NULL) {
        destroy_img(disp);
    }
    
    disp->img->gci_img1 = 
        gnome_canvas_item_new( gnome_canvas_root( GNOME_CANVAS
                                                  (disp->canvas)),
                               gnome_canvas_pixbuf_get_type (),
                               "pixbuf", disp->img->pixbuf1,
                               NULL);
    
    pos1 = disp->img->rgbbuf_img1;
    
    
    for (i = 0; i < disp->img->image->header->nrows; i++) {
	for (j = 0; j < disp->img->image->header->ncolumns; j++) {
	    *pos1++ =  (guchar) (disp->img->image->frame1[i][j] / fact);
	    *pos1++ =  (guchar) (disp->img->image->frame1[i][j] / fact);
	    *pos1++ =  (guchar) (disp->img->image->frame1[i][j] / fact);
	}
    }

    gdk_pixbuf_ref (disp->img->pixbuf1);


    if (disp->img->image->header->x_corr) {
        pos2 = NULL;
/* NEW */        
        disp->img->rgbbuf_img2 = g_malloc(gdk_pixbuf_get_rowstride(px) *
                                         disp->img->image->header->nrows);
/* OLD */
/*         disp->img->rgbbuf_img2 = g_malloc(disp->img->rgb_img_width * */
/*                                          disp->img->image->header->nrows); */
        
        disp->img->pixbuf2 = gdk_pixbuf_new_from_data (disp->img->rgbbuf_img2,
                                                      GDK_COLORSPACE_RGB,
                                                      FALSE,
                                                      depth,
                                                      disp->img->image->header->ncolumns, 
                                                      disp->img->image->header->nrows,
                                                      disp->img->rgb_img_width,
                                                      NULL, 
                                                      NULL);
        if (disp->img->gci_img2 != NULL) {
            destroy_img(disp);
        }
        
        disp->img->gci_img2 = 
            gnome_canvas_item_new( gnome_canvas_root( GNOME_CANVAS
                                                      (disp->canvas)),
                                   gnome_canvas_pixbuf_get_type (),
                                   "pixbuf", disp->img->pixbuf2,
                                   NULL);

        pos2 = disp->img->rgbbuf_img2;        
        for (i = 0; i < disp->img->image->header->nrows; i++) {
            for (j = 0; j < disp->img->image->header->ncolumns; j++) {
                *pos2++ = (guchar) (disp->img->image->frame2[i][j] / fact);
                *pos2++ = (guchar) (disp->img->image->frame2[i][j] / fact);
                *pos2++ = (guchar) (disp->img->image->frame2[i][j] / fact);
            }
        }
        
        gdk_pixbuf_ref (disp->img->pixbuf2);
        
    } else {
        disp->img->gci_img2 = disp->img->gci_img1;
    }
    
    gdk_pixbuf_unref (px);
}



void
hide_img1 (Display *disp
          )
/*-----------------------------------------------------------------------------
 */
{
    assert (disp != NULL);

    if (disp->img->exist_img && disp->img->gci_img1 != NULL) {
        gnome_canvas_item_hide (GNOME_CANVAS_ITEM (disp->img->gci_img1));
        disp->display_backgrnd = SHOW_BG_DARKBLUE;
    }
}



void
show_img1 (Display *disp
          )
/*-----------------------------------------------------------------------------
 */
{
    assert (disp != NULL);

    if (disp->img->exist_img && disp->img->gci_img1 != NULL) {
        gnome_canvas_item_show (GNOME_CANVAS_ITEM (disp->img->gci_img1));
        disp->display_backgrnd = SHOW_BG_IMG1;
    }
}



void
hide_img2 (Display *disp
          )
/*-----------------------------------------------------------------------------
 */
{
    assert (disp != NULL);

    if (disp->img->exist_img && disp->img->gci_img2 != NULL) {
        gnome_canvas_item_hide (GNOME_CANVAS_ITEM (disp->img->gci_img2));
        disp->display_backgrnd = SHOW_BG_DARKBLUE;
    }
}


void
show_img2 (Display *disp
          )
/*-----------------------------------------------------------------------------
 */
{
    assert (disp != NULL);

    if (disp->img->exist_img && disp->img->gci_img2 != NULL) {
        gnome_canvas_item_show (GNOME_CANVAS_ITEM (disp->img->gci_img2));
        disp->display_backgrnd = SHOW_BG_IMG2;
    }
}



void 
destroy_img (Display *disp
             )
/*-----------------------------------------------------------------------------
 */
{
    assert (disp->img->gci_img1 != NULL);
    gtk_object_destroy(GTK_OBJECT
                       (disp->img->gci_img1));
    /*     g_free(disp->img->gci_img1); */
    disp->img->gci_img1 = NULL;
    
    if(display_act->img->image->header->x_corr) {
        gtk_object_destroy(GTK_OBJECT
                           (disp->img->gci_img2));
        disp->img->gci_img2 = NULL;
    }
}



GnomeCanvasItem *
create_background (Display *disp
                   )
/* ----------------------------------------------------------------------------
 *  Displays backgroundcolor
 */
{
    GnomeCanvasItem *bg = NULL;
    gchar *color = NULL;


    if (gpiv_par->display__backgrnd == SHOW_BG_DARKBLUE) {
        color = "darkblue";
    } else if (gpiv_par->display__backgrnd == SHOW_BG_BLACK) {
        color = "black";
    }

    bg = 
        gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS(disp->canvas)),
                              gnome_canvas_rect_get_type(),
                              "x1", (double) 0,
                              "y1", (double) 0,
                              "x2", (double) disp->img->image->header->ncolumns,
                              "y2", (double) disp->img->image->header->nrows,
                              "fill_color", color,
                              "width_units", 1.0, 
                              NULL);


    return bg;
}



void 
destroy_background (GnomeCanvasItem *gci
                   )
/*-----------------------------------------------------------------------------
 */
{
    assert (gci != NULL);
    gtk_object_destroy(GTK_OBJECT
                       (gci));
    gci = NULL;
}

