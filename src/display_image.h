/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */
 
/*----------------------------------------------------------------------

  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

#ifndef DISPLAY_IMAGE_H
#define DISPLAY_IMAGE_H


/*
 * Public image and background functions
 * Image in a Gnome canvas
 */

void 
create_img (Display *disp);

void 
hide_img1 (Display *disp);

void 
show_img1 (Display *disp);

void 
hide_img2 (Display *disp);

void 
show_img2 (Display *disp);

void 
destroy_img (Display *disp);


GnomeCanvasItem *
create_background (Display *disp);

void 
destroy_background (GnomeCanvasItem *gci);


#endif /*  DISPLAY_IMAGE_H */
