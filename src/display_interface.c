/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * interface for create_display
 * $Log: display_interface.c,v $
 * Revision 1.19  2008-04-28 12:00:57  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.18  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.17  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.16  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.15  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.14  2007-02-16 17:09:48  gerber
 * added Gauss weighting on I.A. and SPOF filtering (on correlation function)
 *
 * Revision 1.13  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.12  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.11  2006-09-18 07:27:05  gerber
 * *** empty log message ***
 *
 * Revision 1.10  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.9  2005/06/15 15:03:54  gerber
 * Optional Anti Aliased canvas for viewer
 *
 * Revision 1.8  2005/06/15 09:40:40  gerber
 * debugged, optimized
 *
 * Revision 1.7  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.6  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.5  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.4  2003/09/01 11:17:14  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.3  2003/07/04 10:47:01  gerber
 * cleaning up
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#include "gpiv_gui.h"
#include "utils.h"
#include "display_interface.h"
#include "display_menus.h"
#include "display.h"

#define EVENT_METHOD(i, x) GTK_WIDGET_GET_CLASS(i)->x


Display *
create_display (gchar *fname, 
                GpivImage *image,
                guint display_id,
                GpivConsole *gpiv
                )
/*-----------------------------------------------------------------------------
 *
 * Layout:
 *
 *  main_vbox
 *     |
 *     +-- menubar
 *     |
 *     +-- vbox
 *            |
 *            +-- table
 *            |      |
 *            |      +-- origin
 *            |      +-- hruler
 *            |      +-- vruler
 *            |      +-- canvas
 *            |
 *            +-- statusbar
 *
 *
 -----------------------------------------------------------------------------*/
{

    Display *disp = g_new0 (Display, 1);
    Image *img = g_new0 (Image, 1);
    Intreg *intreg = g_new0 (Intreg, 1);
    GpivPivPar *intreg_pivp = g_new0 (GpivPivPar, 1);
    PivData *pd = g_new0 (PivData, 1);
    GpivValidPar *pd_valp = g_new0 (GpivValidPar, 1);
    GpivPostPar *pd_postp = g_new0 (GpivPostPar, 1);

    static gint width = 0, height = 0;
    static gchar title[GPIV_MAX_CHARS];
    static GdkPixbuf *icon = NULL;


    gint i = 0;

    static const gchar * zoom_label[8] = {
        "Zoom 0.25",
        "Zoom 0.5",
        "Zoom 0.83",
        "Zoom 1.0",
        "Zoom 1.3",
        "Zoom 1.6",
        "Zoom 2.0",
        "Zoom 4.0"
    };

    static const gchar * vector_scale_label[9] = {
        "1",
        "2",
        "4",
        "8",
        "16",
        "32",
        "64",
        "128",
        "256"
    };




    disp->zoom_group = NULL;
    disp->background_group = NULL;
    disp->scalar_group = NULL;
    disp->vector_scale_group = NULL;
    disp->vector_color_group = NULL;

    g_snprintf (title,
	     GPIV_MAX_CHARS,
	     "gpiv display #%d",
	     display_id);

    g_snprintf (disp->msg_display_default,
	     GPIV_MAX_CHARS,
	     "%s",
	     fname);

    disp->id = display_id;
    disp->zoom_index = gpiv_par->display__zoom_index;
    disp->zoom_factor = zfactor[disp->zoom_index];
    disp->zoom_factor_old = zfactor[disp->zoom_index];
/*         g_message ("CREATE_DISPLAY:: zoom_factor = %f zoom_factor_old = %f",  */
/*                    disp->zoom_factor, disp->zoom_factor_old); */
    disp->vector_scale = gpiv_par->display__vector_scale;
    disp->vector_color = gpiv_par->display__vector_color;
    disp->display_backgrnd = gpiv_par->display__backgrnd;
/*     disp->display_img1 = gpiv_par->display__img1; */
/*     disp->display_img2 = gpiv_par->display__img2; */
    disp->display_intregs = gpiv_par->display__intregs;
    disp->display_piv = gpiv_par->display__piv;
    disp->display_scalar = gpiv_par->display__scalar;
/*     disp->display_vor = gpiv_par->display__vor; */
/*     disp->display_sstrain = gpiv_par->display__sstrain; */
/*     disp->display_nstrain = gpiv_par->display__nstrain; */

    disp->file_uri_name = g_strdup_printf ("%s" , fname);


    disp->img = img;
    disp->img->exist_img = FALSE;
    disp->img->saved_img = FALSE;
    disp->img->rgb_img_width = 0;
    disp->img->img_mean = 0.0;

/*
 * PIV data and related do not exist, yet. 'Saved' is set TRUE to avoid
 * message of unsaved data while deleting display or console
 * Values of these boolean variables are updated in the 'exec_' functions.
 */
    disp->pida = pd;
    disp->pida->exist_piv = FALSE;
    disp->pida->saved_piv = TRUE;
    disp->pida->scaled_piv = FALSE;
    disp->pida->exist_histo = FALSE;
    disp->pida->saved_histo = TRUE;
    disp->pida->exist_valid = FALSE;
    disp->pida->exist_vor = FALSE;
    disp->pida->exist_vor_scaled = FALSE;
    disp->pida->saved_vor = TRUE;
    disp->pida->exist_sstrain = FALSE;
    disp->pida->exist_sstrain_scaled = FALSE;
    disp->pida->saved_sstrain = TRUE;
    disp->pida->exist_nstrain = FALSE;
    disp->pida->exist_nstrain_scaled = FALSE;
    disp->pida->saved_nstrain = TRUE;



/*
 * Initialising of parameter values for image, evaluation and interrogation regions
 */
    disp->img->image = image;
/*     disp->img->image->header = gpiv_img_cp_parameters (gl_image_par); */
    disp->pida->piv_par = gpiv_piv_cp_parameters (gl_piv_par);
    disp->pida->valid_par = pd_valp;
    disp->pida->post_par = pd_postp;


    if (disp->pida->piv_par->row_start > disp->img->image->header->nrows
	- disp->pida->piv_par->pre_shift_row - 1) {
      disp->pida->piv_par->row_start = 0;
    }
    disp->pida->piv_par->row_end =
      MIN (disp->img->image->header->nrows, gl_piv_par->row_end);
    if (disp->pida->piv_par->col_start > disp->img->image->header->ncolumns 
	- disp->pida->piv_par->pre_shift_col - 1) {
      disp->pida->piv_par->col_start = 0;
    }
    disp->pida->piv_par->col_end =
      MIN (disp->img->image->header->ncolumns, gl_piv_par->col_end);

/*
 * Copy _PART_ of piv_par to intreg for displaying Interrogation Area contours
 */
    disp->intreg = intreg;
    disp->intreg->exist_int = FALSE;
    disp->intreg->row_start_old = 0;
    disp->intreg->col_start_old = 0;

    disp->intreg->par = intreg_pivp;
    disp->intreg->par->row_start = disp->pida->piv_par->row_start;
    disp->intreg->par->row_end = disp->pida->piv_par->row_end;
    disp->intreg->par->col_start = disp->pida->piv_par->col_start;
    disp->intreg->par->col_end = disp->pida->piv_par->col_end;
    disp->intreg->par->int_size_f = disp->pida->piv_par->int_size_f;
    disp->intreg->par->int_size_i = disp->pida->piv_par->int_size_i;
    disp->intreg->par->int_shift = disp->pida->piv_par->int_shift;
    disp->intreg->par->pre_shift_row = disp->pida->piv_par->pre_shift_row;
    disp->intreg->par->pre_shift_col = disp->pida->piv_par->pre_shift_col;

    disp->intreg->par->row_start__set = TRUE;
    disp->intreg->par->row_end__set = TRUE;
    disp->intreg->par->col_start__set = TRUE;
    disp->intreg->par->col_end__set = TRUE;
    disp->intreg->par->int_size_f__set = TRUE;
    disp->intreg->par->int_size_i__set = TRUE;
    disp->intreg->par->int_shift__set = TRUE;
    disp->intreg->par->pre_shift_row__set = TRUE;
    disp->intreg->par->pre_shift_col__set = TRUE;



    width = (gint) (disp->zoom_factor * disp->img->image->header->ncolumns + VIEW_HMARGE);
    height = (gint) (disp->zoom_factor * disp->img->image->header->nrows + VIEW_VMARGE);
/*     disp->mwin = g_object_new (GTK_TYPE_WINDOW, */
/* 			       "default-height", height, */
/* 			       "default-width", width, */
/* 			       "title", title, */
/* 			       NULL */
/* 			       ); */



    disp->mwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (disp->mwin), title);
/*     gtk_window_set_default_size (GTK_WINDOW (disp->mwin), width, height); */
    gtk_window_set_default_size (GTK_WINDOW (disp->mwin), 20, 20);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"gpiv", 
			gpiv);

/*
 * DESTROING ITEM
 */
    g_signal_connect (GTK_OBJECT (disp->mwin), 
			"delete_event" ,
			G_CALLBACK (delete_display),
			NULL);

/*
 * focussing the actual display
 */
    g_signal_connect (GTK_OBJECT (disp->mwin),
		       "focus_in_event",
		       G_CALLBACK (on_display_set_focus),
		       NULL);
    gtk_window_set_resizable (GTK_WINDOW (disp->mwin), FALSE);

/*
 * icon setting
 */
    icon = gdk_pixbuf_new_from_file(PIXMAPSDIR "gpiv_logo.png", NULL);
    gtk_window_set_icon(GTK_WINDOW(disp->mwin), icon);


    disp->vbox = gtk_vbox_new ( FALSE /* TRUE */, 0);
    gtk_widget_show (disp->vbox);
    gtk_container_add (GTK_CONTAINER (disp->mwin), disp->vbox);
    
/*
 * Menu bar
 */
    disp->menubar = gtk_menu_bar_new ();
    gtk_box_pack_start (GTK_BOX (disp->vbox),
			disp->menubar,
			FALSE,
			FALSE,
			0);
    if (gpiv_par->display__view_menubar) {
        gtk_widget_show (GTK_WIDGET (disp->menubar));
    } else {
        gtk_widget_hide (GTK_WIDGET (disp->menubar));
    }


/*
 * View menu
 */
    disp->menuitem_view = 
      gtk_menu_item_new_with_mnemonic (_("_View"));
    gtk_widget_show (disp->menuitem_view);
    gtk_container_add (GTK_CONTAINER (disp->menubar), 
		       disp->menuitem_view);
/* NEW */
/*     gtk_object_set_data_full (GTK_OBJECT (disp->mwin), */
/* 			     "menuitem_view", */
/* 			     disp->menuitem_view, */
/* 			     (GtkDestroyNotify) gtk_widget_unref); */
 

    disp->menuitem_view_menu = gtk_menu_new ();
    gtk_menu_item_set_submenu (GTK_MENU_ITEM 
				     (disp->menuitem_view), 
				     disp->menuitem_view_menu);



    disp->view_menubar = 
      gtk_check_menu_item_new_with_mnemonic (_("View menu bar"));
    gtk_widget_show (disp->view_menubar);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_menu), 
		       disp->view_menubar);
    g_signal_connect ((gpointer) disp->view_menubar, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_MENUBAR);



    disp->view_rulers = 
      gtk_check_menu_item_new_with_mnemonic (_("View rulers"));
    gtk_widget_show (disp->view_rulers);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_menu), 
		       disp->view_rulers);
    g_signal_connect ((gpointer) disp->view_rulers, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_RULERS);

/*
 * Menu for automatic stretching the window when zooming in
 */
    disp->stretch_auto = 
      gtk_check_menu_item_new_with_mnemonic (_("Stretch auto"));
    gtk_widget_show (disp->stretch_auto);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_menu), 
		       disp->stretch_auto);
    g_signal_connect ((gpointer) disp->stretch_auto, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) STRETCH_AUTO);

/*
 * Menu for stretching the window
 */
    disp->stretch = 
        gtk_menu_item_new_with_label (_("Stretch display"));
    gtk_widget_show (disp->stretch);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_menu), 
		       disp->stretch);
    gtk_tooltips_set_tip (gpiv->tooltips, 
			  disp->stretch, 
			  _("Stretch or fit display window to the image area"),
			  NULL);
    g_signal_connect ((gpointer) disp->stretch, 
		      "activate",
		      G_CALLBACK (on_stretch_activate),
		      NULL);

/*
 * Separator
 */
    disp->separator_zoom = gtk_separator_menu_item_new ();
    gtk_widget_show (disp->separator_zoom);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_menu), 
		       disp->separator_zoom);

/*
 * Zoom menus
 */
    for (i = 0; i < 8; i++) {
/*         gchar *zoom_index  = g_strdup_printf ("zoom_%d", i) */
        disp->zoom_key[i] = g_strdup_printf ("zoom_menu_%d", i);
        disp->zoom_menu[i] = gtk_radio_menu_item_new_with_label
            (disp->zoom_group, zoom_label[i]);
        disp->zoom_group = gtk_radio_menu_item_get_group 
            (GTK_RADIO_MENU_ITEM (disp->zoom_menu[i]));
        gtk_widget_show (disp->zoom_menu[i]);
        gtk_container_add (GTK_CONTAINER (disp->menuitem_view_menu), 
                           disp->zoom_menu[i]);
        gtk_tooltips_set_tip (gpiv->tooltips, 
                              disp->zoom_menu[i], 
                              _("set zoom factor"),
                              NULL);
        gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                             disp->zoom_key[i], 
                             disp->zoom_menu[i]);
        g_signal_connect ((gpointer) disp->zoom_menu[i], 
                          "activate",
                          G_CALLBACK (on_zoom_activate),
                          (int *) i);


/*         if (disp->zoom_index == i) { */
/*             gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM  */
/*                                             (disp->zoom_menu[i]),  */
/*                                             TRUE); */
/*         } */
    }


/*
 * Background (image) menu
 */
    disp->menuitem_background = 
      gtk_menu_item_new_with_mnemonic (_("_Background"));
    gtk_widget_show (disp->menuitem_background);
    gtk_container_add (GTK_CONTAINER (disp->menubar), 
		       disp->menuitem_background);


    disp->menuitem_background_menu = gtk_menu_new ();
    gtk_menu_item_set_submenu (GTK_MENU_ITEM 
				     (disp->menuitem_background), 
				     disp->menuitem_background_menu);



    disp->view_blue = gtk_radio_menu_item_new_with_mnemonic 
      (disp->background_group, _("Blue"));
    disp->background_group = gtk_radio_menu_item_get_group 
      (GTK_RADIO_MENU_ITEM (disp->view_blue));
    gtk_widget_show (disp->view_blue);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_background_menu), 
		       disp->view_blue);
    gtk_tooltips_set_tip (gpiv->tooltips, 
			  disp->view_blue, 
			  _("set blue background"),
			  NULL);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "blue_background_menu", 
                         disp->view_blue);
    g_signal_connect ((gpointer) disp->view_blue, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_BLUE);



    disp->view_black = gtk_radio_menu_item_new_with_mnemonic 
      (disp->background_group, _("Black"));
    disp->background_group = gtk_radio_menu_item_get_group 
      (GTK_RADIO_MENU_ITEM (disp->view_black));
    gtk_widget_show (disp->view_black);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_background_menu), 
		       disp->view_black);
    gtk_tooltips_set_tip (gpiv->tooltips, 
			  disp->view_black, 
			  _("set black background"),
			  NULL);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "black_background_menu", 
                         disp->view_black);
    g_signal_connect ((gpointer) disp->view_black, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_BLACK);



    disp->view_image_a = gtk_radio_menu_item_new_with_mnemonic 
      (disp->background_group, _("Image_A"));
    disp->background_group = gtk_radio_menu_item_get_group 
      (GTK_RADIO_MENU_ITEM (disp->view_image_a));
    gtk_widget_show (disp->view_image_a);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_background_menu), 
		       disp->view_image_a);
    gtk_tooltips_set_tip (gpiv->tooltips, 
			  disp->view_image_a, 
			  _("set first image as background"),
			  NULL);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "image_a_background_menu", 
                         disp->view_image_a);
    g_signal_connect ((gpointer) disp->view_image_a, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_IMAGE_A);



    disp->view_image_b = gtk_radio_menu_item_new_with_mnemonic 
      (disp->background_group, ("Image_B"));
    disp->background_group = gtk_radio_menu_item_get_group 
      (GTK_RADIO_MENU_ITEM (disp->view_image_b));
    gtk_widget_show (disp->view_image_b);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_background_menu), 
		       disp->view_image_b);
    gtk_tooltips_set_tip (gpiv->tooltips, 
			  disp->view_image_b, 
			  _("set second image as background"),
			  NULL);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "image_b_background_menu", 
                         disp->view_image_b);
    g_signal_connect ((gpointer) disp->view_image_b, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_IMAGE_B);


/*
 * Piv display menu
 */
    disp->menuitem_view_piv_data = 
      gtk_menu_item_new_with_mnemonic (_("_Piv"));
    gtk_widget_show (disp->menuitem_view_piv_data);
    gtk_container_add (GTK_CONTAINER (disp->menubar), 
		       disp->menuitem_view_piv_data);


    disp->menuitem_view_piv_data_menu = gtk_menu_new ();
    gtk_menu_item_set_submenu (GTK_MENU_ITEM (disp->menuitem_view_piv_data), 
			       disp->menuitem_view_piv_data_menu);


    disp->view_interrogation_areas = 
      gtk_check_menu_item_new_with_mnemonic (_("Interrogation area's"));
    gtk_widget_show (disp->view_interrogation_areas);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_piv_data_menu), 
		       disp->view_interrogation_areas);
    g_signal_connect ((gpointer) disp->view_interrogation_areas, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_INTERROGATION_AREAS);


    disp->view_velocity_vectors = 
      gtk_check_menu_item_new_with_mnemonic (_("Velocity vectors"));
    gtk_widget_show (disp->view_velocity_vectors);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_piv_data_menu), 
		       disp->view_velocity_vectors);
    g_signal_connect ((gpointer) disp->view_velocity_vectors, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_VELOCITY_VECTORS);

/*
 * Scalar data display menu
 */

    disp->menuitem_view_scalar_data = 
      gtk_menu_item_new_with_mnemonic (_("_Scalar"));
    gtk_widget_show (disp->menuitem_view_scalar_data);
    gtk_container_add (GTK_CONTAINER (disp->menubar), disp->menuitem_view_scalar_data);

    disp->menuitem_view_scalar_data_menu = gtk_menu_new ();
    gtk_menu_item_set_submenu (GTK_MENU_ITEM (disp->menuitem_view_scalar_data),
			       disp->menuitem_view_scalar_data_menu);



    disp->view_none = 
      gtk_radio_menu_item_new_with_mnemonic (disp->scalar_group, _("None"));
    disp->scalar_group = 
      gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (disp->view_none));
    gtk_widget_show (disp->view_none);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_scalar_data_menu), 
		       disp->view_none);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "view_none_scalars", 
                         disp->view_none);
    g_signal_connect ((gpointer) disp->view_none, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_NONE_SCALARS);


    disp->view_vorticity = 
      gtk_radio_menu_item_new_with_mnemonic (disp->scalar_group, 
					     _("Vorticity"));
    disp->scalar_group = 
      gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (disp->view_vorticity));
    gtk_widget_show (disp->view_vorticity);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_scalar_data_menu),
		       disp->view_vorticity);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "view_vorticity", 
                         disp->view_vorticity);
    g_signal_connect ((gpointer) disp->view_vorticity, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_VORTICITY);


    disp->view_shear_strain = 
      gtk_radio_menu_item_new_with_mnemonic (disp->scalar_group, _("Shear strain"));
    disp->scalar_group = 
      gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (disp->view_shear_strain));
    gtk_widget_show (disp->view_shear_strain);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_scalar_data_menu), 
		       disp->view_shear_strain);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "view_shear_strain", 
                         disp->view_shear_strain);
    g_signal_connect ((gpointer) disp->view_shear_strain, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_SHEAR_STRAIN);


    disp->view_normal_strain = 
      gtk_radio_menu_item_new_with_mnemonic (disp->scalar_group, 
					     _("Normal strain"));
    disp->scalar_group = 
      gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (disp->view_normal_strain));
    gtk_widget_show (disp->view_normal_strain);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_view_scalar_data_menu), 
		       disp->view_normal_strain);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "view_normal_strain", 
                         disp->view_normal_strain);
    g_signal_connect ((gpointer) disp->view_normal_strain, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VIEW_NORMAL_STRAIN);

/*
 * Vector scale menu
 */
    disp->menuitem_vector_scale = 
      gtk_menu_item_new_with_mnemonic (_("Scal_e"));
    gtk_widget_show (disp->menuitem_vector_scale);
    gtk_container_add (GTK_CONTAINER (disp->menubar), 
		       disp->menuitem_vector_scale);

    disp->menuitem_vector_scale_menu = gtk_menu_new ();
    gtk_menu_item_set_submenu (GTK_MENU_ITEM (disp->menuitem_vector_scale), 
			       disp->menuitem_vector_scale_menu);


    for (i = 0; i < 9; i++) {
        disp->vector_scale_key[i] = g_strdup_printf ("vector_scale_menu_%d", i);

       disp->vector_scale_menu[i] = gtk_radio_menu_item_new_with_label
            (disp->vector_scale_group, vector_scale_label[i]);

        disp->vector_scale_group = gtk_radio_menu_item_get_group 
            (GTK_RADIO_MENU_ITEM (disp->vector_scale_menu[i]));

        gtk_widget_show (disp->vector_scale_menu[i]);

        gtk_container_add (GTK_CONTAINER (disp->menuitem_vector_scale_menu), 
                           disp->vector_scale_menu[i]);
        gtk_tooltips_set_tip (gpiv->tooltips, 
                              disp->vector_scale_menu[i], 
                              _("set vector_scale"),
                              NULL);
        gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                             disp->vector_scale_key[i], 
                             disp->vector_scale_menu[i]);
        g_signal_connect ((gpointer) disp->vector_scale_menu[i], 
                          "activate",
                          G_CALLBACK (on_vector_scale_activate),
                          (int *) i);
    }

/*
 * Vector color menu
 */
    disp->menuitem_vector_color = 
      gtk_menu_item_new_with_mnemonic (_("C_olor"));
    gtk_widget_show (disp->menuitem_vector_color);
    gtk_container_add (GTK_CONTAINER (disp->menubar), 
		       disp->menuitem_vector_color);

    disp->menuitem_vector_color_menu = gtk_menu_new ();
    gtk_menu_item_set_submenu (GTK_MENU_ITEM (disp->menuitem_vector_color), 
			       disp->menuitem_vector_color_menu);


    disp->vector_color_peak_nr = 
      gtk_radio_menu_item_new_with_mnemonic (disp->vector_color_group, 
					     ("Peak nr"));
    disp->vector_color_group = 
      gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM 
				     (disp->vector_color_peak_nr));
    gtk_widget_show (disp->vector_color_peak_nr);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_vector_color_menu), 
		       disp->vector_color_peak_nr);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vector_color_peak_nr", 
                         disp->vector_color_peak_nr);
    g_signal_connect ((gpointer) disp->vector_color_peak_nr, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VECTOR_COLOR_PEAK);


    disp->vector_color_snr = 
      gtk_radio_menu_item_new_with_mnemonic (disp->vector_color_group, 
					     ("Snr"));
/*     disp->vector_color_snr =  */
/*       gtk_radio_menu_item_new_with_mnemonic (disp->vector_color_group,  */
/* 					     ("SNR")); */
    disp->vector_color_group = 
      gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM 
				     (disp->vector_color_snr));
    gtk_widget_show (disp->vector_color_snr);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_vector_color_menu), 
		       disp->vector_color_snr);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vector_color_snr", 
                         disp->vector_color_snr);
    g_signal_connect ((gpointer) disp->vector_color_snr, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VECTOR_COLOR_SNR);


    disp->vector_color_magngray = 
      gtk_radio_menu_item_new_with_mnemonic (disp->vector_color_group, 
					     ("Magnitude gray"));
    disp->vector_color_group = 
      gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM 
				     (disp->vector_color_magngray));
    gtk_widget_show (disp->vector_color_magngray);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_vector_color_menu), 
		       disp->vector_color_magngray);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vector_color_magngray", 
                         disp->vector_color_magngray);
    g_signal_connect ((gpointer) disp->vector_color_magngray, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VECTOR_COLOR_MAGNGRAY);



    disp->vector_color_magncolor = 
      gtk_radio_menu_item_new_with_mnemonic (disp->vector_color_group, 
					     ("Magnitude color"));
    disp->vector_color_group = 
      gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM 
				     (disp->vector_color_magncolor));
    gtk_widget_show (disp->vector_color_magncolor);
    gtk_container_add (GTK_CONTAINER (disp->menuitem_vector_color_menu), 
		       disp->vector_color_magncolor);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vector_color_magncolor", 
                         disp->vector_color_magncolor);
    g_signal_connect ((gpointer) disp->vector_color_magncolor, "activate",
		      G_CALLBACK (on_menu_synchronize_popup),
		      (int *) VECTOR_COLOR_MAGNCOLOR);

/*
 * table
 */
    disp->table = gtk_table_new (4,
				 3,
				 FALSE);
    gtk_widget_ref (disp->table);
    gtk_object_set_data_full (GTK_OBJECT (disp->mwin),
			     "table",
			     disp->table,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_box_pack_start (GTK_BOX (disp->vbox),
		       disp->table,
		       TRUE,
		       TRUE,
		       0);
    gtk_widget_show (disp->table);


/*
 * Adjustments for scrollbars or scrolled window
 */
    disp->hadj =
	gtk_adjustment_new (0.0,
			   0.0,
			   (gdouble) disp->img->image->header->ncolumns - 1.0, 
			   1.0, 
			   1.0,
			   (gdouble) disp->img->image->header->ncolumns - 1.0
/*                             * disp->zoom_factor */
                            );
    gtk_object_set_data (GTK_OBJECT (disp->hadj),
                         "var_type",
                         "0");
    gtk_object_set_data (GTK_OBJECT (disp->hadj),
                         "disp",
                         disp);
    g_signal_connect (GTK_OBJECT (disp->hadj),
                      "value_changed",
                      G_CALLBACK
                      (on_adj_changed),
                      NULL);



    disp->vadj =
	gtk_adjustment_new (0.0,
                            0.0, 
                            (gdouble) disp->img->image->header->nrows - 1.0,
                            1.0,
                            1.0,
                            (gfloat) disp->img->image->header->nrows - 1.0
/*                             * disp->zoom_factor */
                            );

    gtk_object_set_data (GTK_OBJECT (disp->vadj),
                         "var_type",
                         "1");
    gtk_object_set_data (GTK_OBJECT (disp->vadj),
                         "disp",
                         disp);
    g_signal_connect (GTK_OBJECT (disp->vadj),
                      "value_changed",
                      G_CALLBACK
                      (on_adj_changed),
                      NULL);


/*
 * horizontal ruler
 */
    disp->hruler = gtk_hruler_new();
    gtk_widget_ref(disp->hruler);
    gtk_object_set_data_full(GTK_OBJECT(disp->mwin),
			     "hruler",
			     disp->hruler,
			     (GtkDestroyNotify) gtk_widget_unref);

    gtk_table_attach (GTK_TABLE (disp->table),
		     disp->hruler,
		     1,
		     2,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_SHRINK | GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);
    if (gpiv_par->display__view_rulers) {
        gtk_widget_show (GTK_WIDGET (disp->hruler));
    } else {
        gtk_widget_hide (GTK_WIDGET (disp->hruler));
    }


    gtk_ruler_set_range (GTK_RULER (disp->hruler),
                         0.0,
			 (gfloat) (disp->img->image->header->ncolumns - 1),
                         0.0,
			 (gfloat) (disp->img->image->header->ncolumns - 1));
/*    gtk_widget_set_size_request (disp->hruler, */
/* 				(gint) (disp->zoom_factor *  */
/* 					disp->img.image_par->ncolumns), */
/* 				RULER_WIDTH); */

/*
 * vertical ruler
 */
    disp->vruler = gtk_vruler_new ();
    gtk_widget_ref (disp->vruler);
    gtk_object_set_data_full (GTK_OBJECT (disp->mwin),
			     "vruler",
			     disp->vruler,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__view_rulers) {
        gtk_widget_show (GTK_WIDGET (disp->vruler));
    } else {
        gtk_widget_hide (GTK_WIDGET (disp->vruler));
    }
    gtk_table_attach (GTK_TABLE (disp->table),
		     disp->vruler,
		     0,
		     1,
		     1,
		     2,
		     (GtkAttachOptions) (GTK_EXPAND ),
		     (GtkAttachOptions) (GTK_EXPAND  | GTK_SHRINK | GTK_FILL),
		     0,
		     0);

   gtk_ruler_set_range (GTK_RULER (disp->vruler),
			0.0,
			 (gfloat) (disp->img->image->header->nrows - 1),
			0.0,
			 (gfloat) (disp->img->image->header->nrows - 1));
/*    gtk_widget_set_size_request (disp->vruler, */
/* 				RULER_WIDTH, */
/* 				(gint) (disp->zoom_factor *  */
/* 					disp->img.image_par->nrows)); */
/*
 * Scrollbars
 */

    disp->vscrollbar = 
        gtk_vscrollbar_new( GTK_ADJUSTMENT (disp->vadj));
    gtk_widget_ref (disp->vscrollbar);
    gtk_object_set_data_full (GTK_OBJECT (disp->mwin),
			     "vscrollbar",
			     disp->vscrollbar,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (disp->vscrollbar);
    gtk_table_attach (GTK_TABLE (disp->table),
                      disp->vscrollbar,
                      2 /* 1 */,
                      3,
                      1,
                      2,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      0,
                      0);

/* gtk_viewport_get_vadjustment (viewport) */

    disp->hscrollbar = 
        gtk_hscrollbar_new (GTK_ADJUSTMENT (disp->hadj));
    gtk_widget_ref (disp->hscrollbar);
    gtk_object_set_data_full (GTK_OBJECT (disp->mwin),
			     "hscrollbar",
			     disp->hscrollbar,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (disp->hscrollbar);
    gtk_table_attach (GTK_TABLE (disp->table),
                      disp->hscrollbar,
                      1,
                      2,
                      2,
                      3,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      0,
                      0);

/*
 * gnome canvas
 */
#ifdef CANVAS_AA
    gtk_widget_push_visual (gdk_rgb_get_visual ());
    gtk_widget_push_colormap (gdk_rgb_get_cmap ());
    disp->canvas = gnome_canvas_new_aa ();
#else
    gtk_widget_push_visual (gdk_imlib_get_visual ());
    gtk_widget_push_colormap (NULL);
    disp->canvas = gnome_canvas_new ();
#endif

    gtk_widget_pop_colormap ();
    gtk_widget_pop_visual ();
    gtk_widget_ref (disp->canvas);
    gtk_object_set_data_full (GTK_OBJECT (disp->mwin), 
			     "canvas",
			     disp->canvas,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (disp->canvas);
    gnome_canvas_set_scroll_region (GNOME_CANVAS (disp->canvas), 
				   0, 
				   0,
				   disp->img->image->header->ncolumns - 1, 
				   disp->img->image->header->nrows - 1);

    gtk_table_attach (GTK_TABLE (disp->table),
		     disp->canvas,
		     1,
		     2,
		     1,
		     2,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     0,
		     0);



    gtk_widget_set_events (GTK_WIDGET (disp->canvas),
			   GDK_LEAVE_NOTIFY_MASK
			   | GDK_BUTTON_PRESS_MASK  
			   | GDK_BUTTON_RELEASE_MASK 
			   | GDK_POINTER_MOTION_MASK
			   | GDK_POINTER_MOTION_HINT_MASK
			   | GDK_SHIFT_MASK
			   | GDK_SCROLL_MASK 
			   );

    gtk_object_set_data (GTK_OBJECT (disp->canvas), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (disp->canvas), 
			"gpiv", 
			gpiv);


/*
 * connect rulers to gnome_canvas drawing area
 */
    g_signal_connect_swapped (G_OBJECT (disp->canvas), 
			      "motion_notify_event",
                              G_CALLBACK (EVENT_METHOD (disp->hruler, motion_notify_event)),
			      G_OBJECT (disp->hruler));

    g_signal_connect_swapped (G_OBJECT (disp->canvas),
			      "motion_notify_event",
			      G_CALLBACK (GTK_WIDGET_GET_CLASS (disp->vruler)->motion_notify_event),
			      G_OBJECT (disp->vruler));


    g_signal_connect (G_OBJECT (disp->canvas), 
		       "enter_notify_event",
		       G_CALLBACK (canvas_display_enter_notify), 
		       NULL);
    g_signal_connect (G_OBJECT (disp->canvas), 
		       "motion_notify_event",
		       G_CALLBACK (canvas_display_motion_notify), 
		       NULL);    
    g_signal_connect (G_OBJECT (disp->canvas), 
		       "button_press_event",
		       G_CALLBACK (canvas_display_button_press), 
		       NULL);    
    g_signal_connect (G_OBJECT (disp->canvas), 
		       "button_release_event",
		       G_CALLBACK (canvas_display_button_release),
		       NULL);
    g_signal_connect (G_OBJECT (disp->canvas), 
		       "leave_notify_event",
		       G_CALLBACK (canvas_display_leave_notify), 
		       NULL);
    g_signal_connect (G_OBJECT (disp->canvas), 
		       "scroll_event",
		       G_CALLBACK (canvas_display_button_scroll), 
		       NULL);


/* 
 * application/status bar
 */
    disp->appbar = gnome_appbar_new (FALSE,
				    TRUE,
				    GNOME_PREFERENCES_NEVER);
    gtk_widget_ref (disp->appbar);
    gtk_object_set_data_full (GTK_OBJECT (disp->mwin),
			     "appbar",
			     disp->appbar,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_table_attach (GTK_TABLE (disp->table),
		     disp->appbar,
		     0,
		     3, 
		     3, 
		     4,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);
    gtk_widget_show (disp->appbar);
    gnome_appbar_set_default (GNOME_APPBAR (disp->appbar),
			     disp->msg_display_default);



    gtk_widget_show (disp->mwin);
    return disp;
}



GtkWidget  *
create_display_popupmenu (Display *disp
                          )
/*-----------------------------------------------------------------------------
 */
{
    GtkWidget *display_menu = NULL;


    display_menu = gtk_menu_new ();
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"display_menu",
			display_menu);
    gnome_app_fill_menu (GTK_MENU_SHELL (display_menu), 
			display_menu_uiinfo,
			NULL,
			FALSE,
			0);


/*
 * view menubar widget
 */
    gtk_widget_ref (display_menu_uiinfo[0].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
                              "view_menubar_popup_menu",
                              display_menu_uiinfo[0].widget,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
                         "view_menubar_popup_menu",
                         display_menu_uiinfo[0].widget);
    if (gpiv_par->display__view_menubar) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (display_menu_uiinfo[0].widget),
                                        TRUE);
    } else {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (display_menu_uiinfo[0].widget),
                                        FALSE);
    }
    gtk_object_set_data (GTK_OBJECT (display_menu_uiinfo[0].widget),
                         "disp",
                         disp);


/*
 * view rulers widget
 */
    gtk_widget_ref (display_menu_uiinfo[1].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
                              "view_rulers_popup_menu",
                              display_menu_uiinfo[1].widget,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
                         "view_rulers_popup_menu",
                         display_menu_uiinfo[1].widget);
    if (gpiv_par->display__view_rulers) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (display_menu_uiinfo[1].widget),
                                        TRUE);
    } else {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (display_menu_uiinfo[1].widget),
                                        FALSE);
    }
    gtk_object_set_data (GTK_OBJECT (display_menu_uiinfo[1].widget),
                         "disp",
                         disp);


/*
 * stretch when zoom automatic
 */
    gtk_widget_ref (display_menu_uiinfo[2].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
                              "stretch_auto_popup_menu",
                              display_menu_uiinfo[2].widget,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
                         "stretch_auto_popup_menu",
                         display_menu_uiinfo[2].widget);
    if (gpiv_par->display__stretch_auto) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (display_menu_uiinfo[2].widget),
                                        TRUE);
    } else {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (display_menu_uiinfo[2].widget),
                                        FALSE);
    }
    gtk_object_set_data (GTK_OBJECT (display_menu_uiinfo[2].widget),
                         "disp",
                         disp);


/*
 * zoom menu widgets
 */
    gtk_widget_ref (zoomscale_menu_display[0].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "zmv_0",
			     zoomscale_menu_display[0].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (disp->zoom_index == ZOOM_0) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (zoomscale_menu_display[0].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"zmv_0", 
			zoomscale_menu_display[0].widget);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[0].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[0].widget), 
			"zoom_index",
			(int *) ZOOM_0);



    gtk_widget_ref (zoomscale_menu_display[1].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "zmv_1",
			     zoomscale_menu_display[1].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (disp->zoom_index == ZOOM_1) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (zoomscale_menu_display[1].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"zmv_1", 
			zoomscale_menu_display[1].widget);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[1].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[1].widget), 
			"zoom_index",
			(int *) ZOOM_1);



    gtk_widget_ref (zoomscale_menu_display[2].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "zmv_2",
			     zoomscale_menu_display[2].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (disp->zoom_index == ZOOM_2) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (zoomscale_menu_display[2].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"zmv_2", 
			zoomscale_menu_display[2].widget);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[2].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[2].widget), 
			"zoom_index",
			(int *) ZOOM_2);



    gtk_widget_ref (zoomscale_menu_display[3].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "zmv_3",
			     zoomscale_menu_display[3].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (disp->zoom_index == ZOOM_3) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (zoomscale_menu_display[3].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"zmv_3", 
			zoomscale_menu_display[3].widget);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[3].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[3].widget), 
			"zoom_index",
			(int *) ZOOM_3 );



    gtk_widget_ref (zoomscale_menu_display[4].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "zmv_4",
			     zoomscale_menu_display[4].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (disp->zoom_index == ZOOM_4) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (zoomscale_menu_display[4].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"zmv_4", 
			zoomscale_menu_display[4].widget);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[4].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[4].widget), 
			"zoom_index",
			(int *) ZOOM_4);



    gtk_widget_ref (zoomscale_menu_display[5].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "zmv_5",
			     zoomscale_menu_display[5].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (disp->zoom_index == ZOOM_5) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (zoomscale_menu_display[5].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"zmv_5", 
			zoomscale_menu_display[5].widget);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[5].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[5].widget),
			"zoom_index",
			(int *) ZOOM_5);


    gtk_widget_ref (zoomscale_menu_display[6].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "zmv_6",
			     zoomscale_menu_display[6].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (disp->zoom_index == ZOOM_6) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (zoomscale_menu_display[6].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"zmv_6", 
			zoomscale_menu_display[6].widget);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[6].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[6].widget),
			"zoom_index",
			(int *) ZOOM_6);


    gtk_widget_ref (zoomscale_menu_display[7].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "zmv_7",
			     zoomscale_menu_display[7].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (disp->zoom_index == ZOOM_7) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (zoomscale_menu_display[7].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
			"zmv_7", 
			zoomscale_menu_display[7].widget);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[7].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (zoomscale_menu_display[7].widget),
			"zoom_index",
			(int *) ZOOM_7);


/*
 * background menu widgets
 */
    gtk_widget_ref (view_background_display[0].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_blue",
			     view_background_display[0].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__backgrnd == SHOW_BG_DARKBLUE) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_background_display[0].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
			"view_background_display0",
                        view_background_display[0].widget);
    gtk_object_set_data (GTK_OBJECT (view_background_display[0].widget), 
			"disp", 
			disp);



    gtk_widget_ref (view_background_display[1].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_black",
			     view_background_display[1].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__backgrnd == SHOW_BG_BLACK) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_background_display[1].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
                         "view_background_display1",
                         view_background_display[1].widget);
    gtk_object_set_data (GTK_OBJECT (view_background_display[1].widget), 
                         "disp", 
                         disp);



    gtk_widget_ref (view_background_display[2].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_img1",
			     view_background_display[2].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__backgrnd == SHOW_BG_IMG1) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_background_display[2].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
                         "view_background_display2",
                         view_background_display[2].widget);
    gtk_object_set_data (GTK_OBJECT (view_background_display[2].widget), 
			"disp", 
			disp);



    gtk_widget_ref (view_background_display[3].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_img2",
			     view_background_display[3].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__backgrnd == SHOW_BG_IMG2) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_background_display[3].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
                         "view_background_display3",
                         view_background_display[3].widget);
    gtk_object_set_data (GTK_OBJECT (view_background_display[3].widget), 
			"disp", 
			disp);


/*
 * view menu widgets to display PIV data and interrrogation area's
 */
    gtk_widget_ref (view_piv_display[0].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_intreg",
			     view_piv_display[0].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
			"view_piv_display0",
                        view_piv_display[0].widget);
    if (disp->display_intregs) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_piv_display[0].widget),
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (view_piv_display[0].widget),
                         "disp",
                         disp);



    gtk_widget_ref (view_piv_display[1].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
                              "view_piv",
                              view_piv_display[1].widget,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
			"view_piv_display1",
                        view_piv_display[1].widget);
    if (disp->display_piv) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (view_piv_display[1].widget),
                                        TRUE);
    } else {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (view_piv_display[1].widget),
                                        FALSE);
    }
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
                         "view_piv_display1",
                         view_piv_display[1].widget);

/*
 * Display scalar data
 */
    gtk_widget_ref (view_scalardata_display[0].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_none",
			     view_scalardata_display[0].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__scalar == SHOW_SC_NONE) {
    if (gpiv_par->verbose) g_message ("create_display_popupmenu:: display__scalar == SHOW_SC_NONE");
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_scalardata_display[0].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (view_scalardata_display[0].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
                         "view_scalardata_display0",
                         view_scalardata_display[0].widget);



    gtk_widget_ref (view_scalardata_display[1].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_vor",
			     view_scalardata_display[1].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__scalar == SHOW_SC_VORTICITY) {
    if (gpiv_par->verbose) g_message ("create_display_popupmenu:: display__scalar == SHOW_SC_VORTICITY");
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_scalardata_display[1].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (view_scalardata_display[1].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
			"view_scalardata_display1",
                        view_scalardata_display[1].widget);



    gtk_widget_ref (view_scalardata_display[2].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_sstrain",
			     view_scalardata_display[2].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__scalar == SHOW_SC_SSTRAIN) {
    if (gpiv_par->verbose) g_message ("create_display_popupmenu:: display__scalar == SHOW_SC_SSTRAIN");
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_scalardata_display[2].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (view_scalardata_display[2].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
			"view_scalardata_display2",
                        view_scalardata_display[2].widget);



    gtk_widget_ref (view_scalardata_display[3].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "view_nstrain",
			     view_scalardata_display[3].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    if (gpiv_par->display__scalar == SHOW_SC_NSTRAIN) {
    if (gpiv_par->verbose) g_message ("create_display_popupmenu:: display__scalar == SHOW_SC_NSTRAIN");
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (view_scalardata_display[3].widget), 
				       TRUE);
    }
    gtk_object_set_data (GTK_OBJECT (view_scalardata_display[3].widget), 
			"disp", 
			disp);
    gtk_object_set_data (GTK_OBJECT (disp->mwin),
			"view_scalardata_display3",
                        view_scalardata_display[3].widget);

/*
 * vectorscale menu widgets
 */
    gtk_widget_ref (vectorscale_menu_display[0].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
                              "vs_mv_0",
                              vectorscale_menu_display[0].widget,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_0", 
                         vectorscale_menu_display[0].widget);
    if (gpiv_par->display__vector_scale == 1) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (vectorscale_menu_display[0].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorscale_menu_display[1].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vs_mv_1",
			     vectorscale_menu_display[1].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_1", 
                         vectorscale_menu_display[1].widget);
    if (gpiv_par->display__vector_scale == 2) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorscale_menu_display[1].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorscale_menu_display[2].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vs_mv_2",
			     vectorscale_menu_display[2].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_2", 
                         vectorscale_menu_display[2].widget);
    if (gpiv_par->display__vector_scale == 4) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorscale_menu_display[2].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorscale_menu_display[3].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vs_mv_3",
			     vectorscale_menu_display[3].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_3", 
                         vectorscale_menu_display[3].widget);
    if (gpiv_par->display__vector_scale == 8) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorscale_menu_display[3].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorscale_menu_display[4].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vs_mv_4",
			     vectorscale_menu_display[0].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_4", 
                         vectorscale_menu_display[4].widget);
    if (gpiv_par->display__vector_scale == 16) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorscale_menu_display[4].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorscale_menu_display[5].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vs_mv_5",
			     vectorscale_menu_display[5].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_5", 
                         vectorscale_menu_display[5].widget);
    if (gpiv_par->display__vector_scale == 32) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorscale_menu_display[5].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorscale_menu_display[6].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vs_mv_6",
			     vectorscale_menu_display[6].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_6", 
                         vectorscale_menu_display[6].widget);
    if (gpiv_par->display__vector_scale == 64) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorscale_menu_display[6].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorscale_menu_display[7].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vs_mv_7",
			     vectorscale_menu_display[7].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_7", 
                         vectorscale_menu_display[7].widget);
    if (gpiv_par->display__vector_scale == 128) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorscale_menu_display[7].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorscale_menu_display[8].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vs_mv_8",
			     vectorscale_menu_display[8].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vs_mv_8", 
                         vectorscale_menu_display[8].widget);
    if (gpiv_par->display__vector_scale == 256) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorscale_menu_display[8].widget), 
				       TRUE);
    }


/*
 * vectorcolor menu widgets
 */
    gtk_widget_ref (vectorcolor_menu_display[0].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vc_mv_0",
			     vectorcolor_menu_display[0].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vectorcolor_menu_display0", 
                         vectorcolor_menu_display[0].widget);
    if (gpiv_par->display__vector_color == SHOW_PEAKNR) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorcolor_menu_display[0].widget), 
				       TRUE);
    }



     gtk_widget_ref (vectorcolor_menu_display[1].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vc_mv_1",
			     vectorcolor_menu_display[1].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vectorcolor_menu_display1", 
                         vectorcolor_menu_display[1].widget);
    if (gpiv_par->display__vector_color == SHOW_SNR) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorcolor_menu_display[1].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorcolor_menu_display[2].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vc_mv_2",
			     vectorcolor_menu_display[2].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vectorcolor_menu_display2", 
                         vectorcolor_menu_display[2].widget);
    if (gpiv_par->display__vector_color == SHOW_MAGNITUDE_GRAY) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorcolor_menu_display[2].widget), 
				       TRUE);
    }



    gtk_widget_ref (vectorcolor_menu_display[3].widget);
    gtk_object_set_data_full (GTK_OBJECT (display_menu),
			     "vc_mv_3",
			     vectorcolor_menu_display[3].widget,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (disp->mwin), 
                         "vectorcolor_menu_display3", 
                         vectorcolor_menu_display[3].widget);
    if (gpiv_par->display__vector_color == SHOW_MAGNITUDE) {
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
				       (vectorcolor_menu_display[3].widget), 
				       TRUE);
    }



   return display_menu;
}
