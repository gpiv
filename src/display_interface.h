/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
         libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * widgets prototypes of display
 * $Log: display_interface.h,v $
 * Revision 1.13  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.12  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.11  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.10  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.9  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.8  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.7  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.5  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.4  2003/09/01 11:17:14  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.3  2003/07/31 11:43:26  gerber
 * display images in gnome canvas (HOERAreset)
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef DISPLAY_INTERFACE_H
#define DISPLAY_INTERFACE_H

#include "console_interface.h"
/* #include "gpiv_gui.h" */


typedef struct _Image Image;
struct _Image {
    GpivImage *image;		/* image structure */

/*
 * Image is displayed at gnome canvas
 */
    GdkPixbuf *pixbuf1;		/* pixpbuf of first image frame */
    GdkPixbuf *pixbuf2;		/* pixbuf of first image frame */
    guchar *rgbbuf_img1;	/* graphical representation of first image frame */
    guchar *rgbbuf_img2;	/* graphical representation of first image frame */
    guint rgb_img_width;	/* straddled image width */
    gfloat img_mean;		/* average value of image intensity */
    GnomeCanvasItem *gci_img1;
    GnomeCanvasItem *gci_img2;
    gboolean exist_img;		/* flag if image exists */
    gboolean saved_img;		/* flag if image has been saved after modifying */

};

/*
 * Structure for displaying the Interrogation Region contours
 */
typedef struct _Intreg Intreg;
struct _Intreg {
    guint row_start_old;	/* Used to check if contours have to be updated */
    guint col_start_old;	/* Used to check if contours have to be updated */
    GpivPivPar *par;		
    GpivPivData *data;
    gboolean exist_int;
    GnomeCanvasItem *gci_intreg1[MAX_DATA][MAX_DATA];
    GnomeCanvasItem *gci_intreg2[MAX_DATA][MAX_DATA];
};


/*
 * Contains all PIV data, derived data and parameters
 */
typedef struct _PivData PivData;
struct _PivData {
    GpivPivPar *piv_par;
    GpivPivData *piv_data;
    GpivPivData *piv_data_scaled;
    gboolean exist_piv;
    gboolean saved_piv;
    gboolean scaled_piv;
    gboolean averaged_piv;
 
    GnomeCanvasItem *gci_vector[MAX_DATA][MAX_DATA];
    gboolean exist_vec;

    GpivCov *cov;		/* Used for displaying in Interrogation tab of console */
    gboolean exist_cov;

    gfloat **intreg1;		/* Used for displaying in Interrogation tab of console */
    gfloat **intreg2;		/* Used for displaying in Interrogation tab of console */

    GpivValidPar *valid_par;
    gboolean exist_valid;
    GpivBinData *klass;
    gboolean exist_histo;
    gboolean saved_histo;

    GpivPostPar *post_par;	/* Post processing parameters */
    GpivScalarData *vor_data;	/* Vorticity data, derived from PIV data */
    GpivScalarData *vor_data_scaled;		/* Time and spatial scaled vorticity data */
    gboolean exist_vor;
    gboolean exist_vor_scaled;
    gboolean saved_vor;
    GnomeCanvasItem *gci_scalar_vor[MAX_DATA][MAX_DATA];

    GpivScalarData *sstrain_data;	/* Shear strain data, derived from PIV data */
    GpivScalarData *sstrain_data_scaled;	/* Time and spatial scaled shear strain data */
    gboolean exist_sstrain;
    gboolean exist_sstrain_scaled;
    gboolean saved_sstrain;
    GnomeCanvasItem *gci_scalar_sstrain[MAX_DATA][MAX_DATA];

    GpivScalarData *nstrain_data;	/* Normal strain data, derived from PIV data */
    GpivScalarData *nstrain_data_scaled;	 /* Time and spatial scaled normal strain data */
    gboolean exist_nstrain;
    gboolean exist_nstrain_scaled;
    gboolean saved_nstrain;
    GnomeCanvasItem *gci_scalar_nstrain[MAX_DATA][MAX_DATA];

};

/*
 * The graphical interface of the display buffer
 */
typedef struct _Display Display;
struct _Display {
    char *file_uri_name;
    guint id;		/* array number and identity of display. 
			When created set to row number in buffer clist, 
			but may differ after deleting displays has taken place */
    GtkWidget *mwin;	/* main window for display */
    GtkWidget *vbox;

/*
 * Menu bar and menus
 */
    GtkWidget *menubar;
    GtkWidget *menuitem_view;
    GtkWidget *menuitem_view_menu;
    GtkWidget *view_menubar;
    GtkWidget *view_rulers;
    GtkWidget *stretch_auto;
    GtkWidget *stretch;
    GtkWidget *separator_zoom;
    GSList *zoom_group;
    gchar *zoom_key[8];
    GtkWidget *zoom_menu[8];

    GtkWidget *menuitem_background;
    GtkWidget *menuitem_background_menu;
    GSList *background_group;
    GtkWidget *view_blue;
    GtkWidget *view_black;
    GtkWidget *view_image_a;
    GtkWidget *view_image_b;

    GtkWidget *menuitem_view_piv_data;
    GtkWidget *menuitem_view_piv_data_menu;
    GtkWidget *view_interrogation_areas;
    GtkWidget *view_velocity_vectors;

    GtkWidget *menuitem_view_scalar_data;
    GtkWidget *menuitem_view_scalar_data_menu;
    GSList *scalar_group;
    GtkWidget *view_none;
    GtkWidget *view_vorticity;
    GtkWidget *view_shear_strain;
    GtkWidget *view_normal_strain;

    GtkWidget *menuitem_vector_scale;
    GtkWidget *menuitem_vector_scale_menu;
    GSList *vector_scale_group;
    gchar *vector_scale_key[9];
    GtkWidget *vector_scale_menu[9];

    GtkWidget *menuitem_vector_color;
    GtkWidget *menuitem_vector_color_menu;
    GSList *vector_color_group;
    GtkWidget *vector_color_peak_nr;
    GtkWidget *vector_color_snr;
    GtkWidget *vector_color_magngray;
    GtkWidget *vector_color_magncolor;

/*
 * Toolbar
 */
#ifdef DISPLAY_TOOLBAR
    GtkWidget *toolbar;
    GtkIconSize toolbar_icon_size;
    GtkWidget *toolitem_zoom_in;
    GtkWidget *button_zoom_in;
    GtkWidget *toolitem_zoom_out;
    GtkWidget *button_zoom_out;
    GtkWidget *toolitem_fit;
    GtkWidget *button_fit;
#endif

/*
 * Display area
 */
    GtkWidget *table;
/*     GtkWidget *button_origin; */
/*     GtkWidget *arrow_origin; */
    GtkObject *hadj;
    GtkObject *vadj;
    GtkWidget *hruler;
    GtkWidget *vruler;
    GtkWidget *vscrollbar;
    GtkWidget *hscrollbar;

    GtkWidget *canvas;
    GtkWidget *view_options;
    GtkWidget *appbar;
    GtkWidget *nav_ebox;

/*     gboolean stretch_window_tmp; */
    gint zoom_index;
    gfloat zoom_factor_old;
    gfloat zoom_factor;
    gint vector_scale;
    gint vector_color;
    gchar msg_display_default[GPIV_MAX_CHARS];

    GtkWidget *display_popupmenu;
    GnomeCanvasItem *gci_bg;

/*
 * Display data
 */
    Image *img;
    Intreg *intreg;
    PivData *pida;

   /*  enum ShowBackground */ guint display_backgrnd;
/*     gboolean display_img1; */
/*     gboolean display_img2; */
    gboolean display_intregs;
    gboolean display_piv;
   /*  enum ShowScalar */ guint display_scalar;
/*     gboolean display_vor; */
/*     gboolean display_sstrain; */
/*     gboolean display_nstrain; */

    gboolean index_old;
    gint index_x_old;
    gint index_y_old;
    gdouble xgrab_first;
    gdouble ygrab_first;

    gdouble x_adj_value;
/*     gdouble x_adj_value_init; */
    gdouble x_adj_lower;
    gdouble x_adj_upper;
    gdouble x_page_size;
    gdouble y_adj_value;
/*     gdouble y_adj_value_init; */
    gdouble y_adj_lower;
    gdouble y_adj_upper;
    gdouble y_page_size;
};


/*
 * creates display window
 */
Display *
create_display (gchar *fname, 
                GpivImage *image,
                guint buf, 
                GpivConsole *gpiv);

/*
 * creates display popup menu
 */
GtkWidget *
create_display_popupmenu (Display *disp);


#endif /*  DISPLAY_INTERFACE_H */
