/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------

  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

#include "gpiv_gui.h"
#include "display_intregs.h"

void 
create_all_intregs (Display *disp
                    )
/*-----------------------------------------------------------------------------
 *  Creates all interrogation areas
 */
{
    char *err_msg = NULL;
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");


    if (disp != NULL) {
/*         if (GTK_CHECK_MENU_ITEM(view_piv_display0)->disabled) { */
        if (!disp->intreg->exist_int) {
            if ((disp->intreg->data = create_intreg_data (disp)) == NULL) {
                g_warning ("create_all_intregs: failing create_intreg_data");
                return;
            }
        }

/*         disp->intreg->par->row_start_old = 0; */
/*         disp->intreg->par->row_start = disp->pida->eval_par->row_start; */
/*         disp->intreg->par->row_end = disp->pida->eval_par->row_end; */
/*         disp->intreg->par->col_start_old = 0; */
/*         disp->intreg->par->col_start = disp->pida->eval_par->col_start; */
/*         disp->intreg->par->col_end = disp->pida->eval_par->col_end; */
/*         disp->intreg->par->int_size_f = disp->pida->eval_par->int_size_f; */
/*         disp->intreg->par->int_size_i = disp->pida->eval_par->int_size_i; */
/*         disp->intreg->par->int_shift = disp->pida->eval_par->int_shift; */
/*         disp->intreg->par->pre_shift_row = disp->pida->eval_par->pre_shift_row; */
/*         disp->intreg->par->pre_shift_col = disp->pida->eval_par->pre_shift_col; */

/*         g_message ("CREATE_ALL_INTREGS:: x_first = %d x_last = %d y_first = %d y_last = %d",  */
/*                    disp->intreg->par->col_start, */
/*                    disp->intreg->par->col_end, */
/*                    disp->intreg->par->row_start, */
/*                    disp->intreg->par->row_end); */

        create_all_intregs1 (disp);
        create_all_intregs2 (disp);

    } else {
        error_gpiv ("create_all_intregs: disp != NULL failed");
    }
}



void 
destroy_all_intregs (Display *disp
                     )
/*-----------------------------------------------------------------------------
 * Destroys all interrogation areas
 */
{
    if (disp != NULL) {
        destroy_all_intregs1 (disp);
        destroy_all_intregs2 (disp);
        destroy_intreg_data (disp);
        disp->display_intregs = FALSE;
    }
}



void 
show_all_intregs(Display *disp
                 )
/*-----------------------------------------------------------------------------
 * Shows all interrogation areas
 */
{
    if (disp != NULL
        && !disp->intreg->exist_int) {
        create_all_intregs(disp);
    }
    show_all_intregs1(disp);
    show_all_intregs2(disp);
}



void 
hide_all_intregs (Display *disp
                  )
/*-----------------------------------------------------------------------------
 * Hides all interrogation areas
 */
{
    if (disp != NULL) {
        hide_all_intregs1(disp);
        hide_all_intregs2(disp);
    }
}



GpivPivData *
create_intreg_data (Display *disp
                    )
/*-----------------------------------------------------------------------------
 */
{
    char *err_msg = NULL;
    GpivPivData *data = NULL;
    guint nx = 0, ny = 0;


    g_return_if_fail (!disp->intreg->exist_int);
/*     g_message ("CREATE_INTREG_DATA:: print_parameters"); */
/*     gpiv_piv_fprint_parameters (NULL, disp->intreg->par); */

    if ((err_msg = 
         gpiv_piv_count_pivdata_fromimage (disp->img->image->header, 
                                           disp->intreg->par,
                                           &nx,
                                           &ny))
        != NULL) {
        g_warning ("create_intreg_data: %s", err_msg);
        return NULL;
    }

    if ((data = 
         gpiv_piv_gridgen (nx, ny, disp->img->image->header, disp->intreg->par))
        == NULL) error_gpiv ("%s: failing gpiv_piv_gridgen", RCSID);

    disp->intreg->exist_int = TRUE;
    return data;
}



void
destroy_intreg_data (Display *disp
                     )
/*-----------------------------------------------------------------------------
 */
{
    if (disp != NULL
        && disp->intreg->gci_intreg1[0][0] == NULL
        && disp->intreg->gci_intreg2[0][0] == NULL) {
        gpiv_free_pivdata (disp->intreg->data);
        disp->intreg->exist_int = FALSE;
    }
}



void 
create_all_intregs1 (Display *disp
                     )
/* ----------------------------------------------------------------------------
 * Displays all first interrogation areas
 */
{
    int i = 0, j;
    gint nx = 0;
    gint ny = 0;

    if (disp != NULL
        && !disp->intreg->exist_int)
        disp->intreg->data = create_intreg_data (disp);
    nx = disp->intreg->data->nx;
    ny = disp->intreg->data->ny;
    if (gpiv_par->verbose) 
        g_message ("create_all_intregs1: creating %d I.A.'s",  nx * ny);
    for (i = 0; i < ny; i++) {
        for (j = 0; j < nx ; j++) {
            create_intreg1 (disp, i, j);
        }
    }
}



void 
create_intreg1 (Display *disp,
                gint i, 
                gint j
                )
/* ----------------------------------------------------------------------------
 * Displays first interrogation area
 */
{
    int start_x = 0, start_y = 0, end_x = 0, end_y = 0;
    float x, y;

    if (disp != NULL
        && disp->intreg->exist_int) {
        x = disp->intreg->data->point_x[i][j];
        y = disp->intreg->data->point_y[i][j];


/*
 * Using centre points of interr regs
 */
        start_x = (int) x - disp->intreg->par->int_size_f / 2;
        start_y = (int) y - disp->intreg->par->int_size_f / 2;
        end_x = (int) x + disp->intreg->par->int_size_f / 2;
        end_y = (int) y + disp->intreg->par->int_size_f / 2;
        
        if (disp->intreg->gci_intreg1[i][j] != NULL) {
            destroy_intreg1 (disp, i, j);
        }
        
        disp->intreg->gci_intreg1[i][j] =
            gnome_canvas_item_new (gnome_canvas_root 
                                   (GNOME_CANVAS (disp->canvas)),
                                   gnome_canvas_rect_get_type(),
                                   "x1", (double) start_x,
                                   "y1", (double) start_y,
                                   "x2", (double) end_x,
                                   "y2", (double) end_y,
                                   "outline_color", "red",
                                   "width_units", (double) THICKNESS,
                                   NULL);
    }
}



void 
update_intreg1 (Display *disp,
                gint i, 
                gint j
                )
/* ----------------------------------------------------------------------------
 * Updates first interrogation area
 */
{
    int start_x = 0, start_y = 0, end_x = 0, end_y = 0;
    float x, y;

    if (disp != NULL
        && disp->intreg->gci_intreg1[i][j] != NULL) {
        x = disp->intreg->data->point_x[i][j];
        y = disp->intreg->data->point_y[i][j];
    
/*
 * Using centre points of interr regs
 */
        start_x = (int) x - disp->intreg->par->int_size_f / 2;
        start_y = (int) y - disp->intreg->par->int_size_f / 2;
        end_x = (int) x + disp->intreg->par->int_size_f / 2;
        end_y = (int) y + disp->intreg->par->int_size_f / 2;
        
        gnome_canvas_item_set (GNOME_CANVAS_ITEM
                               (disp->intreg->gci_intreg1[i][j]),
                               "x1", (double) start_x,
                               "y1", (double) start_y,
                               "x2", (double) end_x,
                               "y2", (double) end_y,
                               "outline_color", "red",
                               "width_units", (double) THICKNESS,
                               NULL);
    }
}



void
destroy_intreg1 (Display *disp,
                 gint i, 
                 gint j
                 )
/* ----------------------------------------------------------------------------
 * Destroys 1st interrogation area at index i, j
 */
{
    if (disp != NULL
        && disp->intreg->gci_intreg1[i][j] != NULL) {
        gtk_object_destroy (GTK_OBJECT
                            (disp->intreg->gci_intreg1[i][j]));
        disp->intreg->gci_intreg1[i][j] = NULL;
    }
}



void 
show_all_intregs1 (Display *disp
                   )
/* ----------------------------------------------------------------------------
 * Shows 1st interrogation areas
 */
{
    int i = 0, j = 0;
    gint nx = 0, ny = 0;

    if (disp != NULL
        && disp->intreg->exist_int) {
    
        /*     if (!disp->intreg->exist_int) */
/*         disp->intreg->data = create_intreg_data (disp); */
        nx = disp->intreg->data->nx;
        ny = disp->intreg->data->ny;

        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                assert (disp->intreg->gci_intreg1[i][j] != NULL);
                gnome_canvas_item_show (GNOME_CANVAS_ITEM
                                        (disp->intreg->gci_intreg1[i][j]));
            }
        }
    }
}



void 
hide_all_intregs1 (Display *disp
                   )
/* ----------------------------------------------------------------------------
 * Hides 1st interrogation areas
 */
{
    int i = 0, j = 0;
    gint nx = 0, ny = 0;

    if (disp != NULL
        && disp->intreg->exist_int) {
        nx = disp->intreg->data->nx;
        ny = disp->intreg->data->ny;
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                if (disp->intreg->gci_intreg1[i][j] != NULL) {
                   gnome_canvas_item_hide (GNOME_CANVAS_ITEM
                                           (disp->intreg->gci_intreg1[i][j]));
                }
            }
        }
    }
}


void 
destroy_all_intregs1 (Display *disp
                      )
/* ----------------------------------------------------------------------------
 * Destroys 1st interrogation areas
 */
{
    int i = 0, j = 0;
    int nx = 0, ny = 0;

    if (disp != NULL
        && disp->intreg->exist_int) {
        nx = disp->intreg->data->nx;
        ny = disp->intreg->data->ny;
        if (gpiv_par->verbose) 
            g_message("destroy_all_intregs1: destroying %d I.A.'s",  nx * ny);
        
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                destroy_intreg1(disp, i, j);
            }
        }
    }

}



void 
create_all_intregs2 (Display *disp
                     )
/* ----------------------------------------------------------------------------
 * Displays all second interrogation areas
 */
{
    int i, j;
    int nx, ny;

    if (disp != NULL
        && !disp->intreg->exist_int)
        disp->intreg->data = create_intreg_data (disp);

    nx = disp->intreg->data->nx;
    ny = disp->intreg->data->ny;
    if (gpiv_par->verbose) 
        g_message("create_all_intregs2: creating %d I.A.'s",  nx * ny);
    
    for (i = 0; i < ny; i++) {
        for (j = 0; j < nx; j++) {
            create_intreg2 (disp, i, j);
        }
    }
}



void 
create_intreg2 (Display *disp,
                gint i, 
                gint j
                )
/* ----------------------------------------------------------------------------
 - Displays second interrogation area
 */
{
    int start_x, start_y, end_x, end_y;
    float x, y;

    if (disp != NULL
        && disp->intreg->exist_int) {
        x = disp->intreg->data->point_x[i][j];
        y = disp->intreg->data->point_y[i][j];

/*
 * with lines
 */
        start_x = (int) x - disp->intreg->par->int_size_i / 2 + 
            disp->intreg->par->pre_shift_col;
        start_y = (int) y - disp->intreg->par->int_size_i / 2 + 
            disp->intreg->par->pre_shift_row;
        end_x = (int) x + disp->intreg->par->int_size_i / 2 + 
            disp->intreg->par->pre_shift_col;
        end_y = (int) y + disp->intreg->par->int_size_i / 2 + 
            disp->intreg->par->pre_shift_row;
        
        if (disp->intreg->gci_intreg2[i][j] != NULL) {
            destroy_intreg2(disp, i, j);
        }

        disp->intreg->gci_intreg2[i][j] =
            gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS
                                                    (disp->canvas)), 
                                  gnome_canvas_rect_get_type(),
                                  "x1", (double) start_x,
                                  "y1", (double) start_y,
                                  "x2", (double) end_x,
                                  "y2", (double) end_y,
                                  "outline_color", "blue",
                                  "width_units", (double) THICKNESS,
                                  NULL);
    }
}



void 
update_intreg2 (Display *disp,
                gint i, 
                gint j
                )
/* ----------------------------------------------------------------------------
 * Updates second interrogation area
 */
{
    int start_x, start_y, end_x, end_y;
    float x, y;

    if (disp != NULL
        && disp->intreg->gci_intreg1[i][j] != NULL) {

        x = disp->intreg->data->point_x[i][j] + disp->intreg->par->pre_shift_col;
        y = disp->intreg->data->point_y[i][j] + disp->intreg->par->pre_shift_row;

/*
 * Using centre points of interr regs
 */
        start_x = (int) x - disp->intreg->par->int_size_i / 2;
        start_y = (int) y - disp->intreg->par->int_size_i / 2;
        end_x = (int) x + disp->intreg->par->int_size_i / 2;
        end_y = (int) y + disp->intreg->par->int_size_i / 2;
        
        if (disp->intreg->gci_intreg2[i][j] != NULL) {
            gnome_canvas_item_set (GNOME_CANVAS_ITEM
                                   (disp->intreg->gci_intreg2[i][j]),
                                   "x1", (double) start_x,
                                   "y1", (double) start_y,
                                   "x2", (double) end_x,
                                   "y2", (double) end_y,
                                   "outline_color", "blue",
                                   "width_units", (double) THICKNESS,
                                   NULL);
        }
    }
}



void
destroy_intreg2 (Display *disp,
                 gint i, 
                 gint j
                 )
/* ----------------------------------------------------------------------------
 * Destroys 2nd interrogation area at index i, j
 */
{
    if (disp != NULL
        && disp->intreg->gci_intreg2[i][j] != NULL) {
        gtk_object_destroy(GTK_OBJECT
                           (disp->intreg->gci_intreg2[i][j]));
        disp->intreg->gci_intreg2[i][j] = NULL;
    }
}



void 
show_all_intregs2 (Display *disp
                   )
/* ----------------------------------------------------------------------------
 * Shows 2nd interrogation areas
 */
{
    int i, j;
    int nx, ny;

    if (disp != NULL
        && disp->intreg->exist_int) {

        nx = disp->intreg->data->nx;
        ny = disp->intreg->data->ny;
    
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                assert (disp->intreg->gci_intreg2[i][j] != NULL);
                gnome_canvas_item_show (GNOME_CANVAS_ITEM
                                        (disp->intreg->gci_intreg2[i][j]));
            }
        }
    }
}



void 
hide_all_intregs2 (Display *disp
                   )
/* ----------------------------------------------------------------------------
 * Hides 2nd interrogation areas
 */
{
    int i, j;
    int nx, ny;

    if (disp != NULL
        && disp->intreg->exist_int) {
        nx = disp->intreg->data->nx;
        ny = disp->intreg->data->ny;
    
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                assert (disp->intreg->gci_intreg2[i][j] != NULL);
                gnome_canvas_item_hide (GNOME_CANVAS_ITEM
                                        (disp->intreg->gci_intreg2[i][j]));
            }
        }
    }
}



void 
destroy_all_intregs2 (Display *disp
                      )
/* ----------------------------------------------------------------------------
 * Destroys 2nd interrogation areas
 */
{
    int i, j;
    int nx, ny;
    
    if (disp != NULL
        && disp->intreg->exist_int) {    
        nx = disp->intreg->data->nx;
        ny = disp->intreg->data->ny;
        if (gpiv_par->verbose) 
        g_message("destroy_all_intregs2: destroying %d I.A.'s",  nx * ny);
    
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                destroy_intreg2(disp, i, j);
            }
        }
    }
}
