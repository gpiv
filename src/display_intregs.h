/*
/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */
 
/*----------------------------------------------------------------------

  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

#ifndef DISPLAY_INTREGS_H
#define DISPLAY_INTREGS_H


/*
 * Interrogation regions of first and second image frame in a Gnome canvas
 */

void 
create_all_intregs (Display *disp);

void 
destroy_all_intregs (Display *disp);

GpivPivData *
create_intreg_data (Display *disp);

void
destroy_intreg_data (Display *disp);

void 
show_all_intregs (Display *disp);

void 
hide_all_intregs (Display *disp);

void 
create_all_intregs1 (Display *disp);

void 
create_intreg1 (Display *disp,
               gint i, 
               gint j);

void 
update_intreg1 (Display *disp,
               gint i, 
               gint j);

void 
destroy_intreg1 (Display *disp,
                gint i, 
                gint j);

void 
show_all_intregs1 (Display *disp);

void 
hide_all_intregs1 (Display *disp);

void 
destroy_all_intregs1 (Display *disp);

void 
create_all_intregs2 (Display *disp);

void 
create_intreg2 (Display *disp,
               gint i, 
               gint j);

void 
update_intreg2 (Display *disp,
               gint i, 
               gint j);   

void 
destroy_intreg2 (Display *disp,
                gint i, 
                gint j);

void 
show_all_intregs2 (Display *disp);

void 
hide_all_intregs2 (Display *disp);

void 
destroy_all_intregs2 (Display *disp);


#endif /* DISPLAY_INTREGS_H */
