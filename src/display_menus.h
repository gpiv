/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * menus of display
 * $Log: display_menus.h,v $
 * Revision 1.8  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.7  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.6  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.5  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.3  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.2  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef DISPLAY_MENUS_H
#define DISPLAY_MENUS_H

#include "display.h"
/*
 * Display menus
 */

static 
GnomeUIInfo zoomscale_menu_display[] = {
    GNOMEUIINFO_RADIOITEM_DATA (N_("0.25"), NULL, select_zoomscale, 
                                GUINT_TO_POINTER(0), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("0.5"), NULL, select_zoomscale, 
                                GUINT_TO_POINTER(1), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("0.83"), NULL, select_zoomscale, 
                                GUINT_TO_POINTER(2), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("1.0"), NULL, select_zoomscale, 
                                GUINT_TO_POINTER(3), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("1.3"), NULL, select_zoomscale, 
                                GUINT_TO_POINTER(4), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("1.6"), NULL, select_zoomscale, 
                                GUINT_TO_POINTER(5), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("2.0"), NULL, select_zoomscale, 
                                GUINT_TO_POINTER(6), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("4.0"), NULL, select_zoomscale, 
                                GUINT_TO_POINTER(7), NULL),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo view_background_display[] = {
    GNOMEUIINFO_RADIOITEM_DATA (N_("Blue background"), NULL, 
                                select_view_background, 
                                GUINT_TO_POINTER (0), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("Black background"), NULL, 
                                select_view_background, 
                                GUINT_TO_POINTER (1), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("Image A"), NULL,
                                select_view_background, 
                                GUINT_TO_POINTER (2), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("Image B"), NULL, 
                                select_view_background, 
                                GUINT_TO_POINTER (3), NULL),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo view_background_radiolist[] = {
    GNOMEUIINFO_RADIOLIST (view_background_display),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo view_piv_display[] = {
    GNOMEUIINFO_TOGGLEITEM (N_("Interrogation area's"), NULL, 
                            view_toggle_intregs, NULL),
    GNOMEUIINFO_TOGGLEITEM (N_("Velocity vectors"), NULL, view_toggle_piv, NULL),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo view_scalardata_display[] = {
    GNOMEUIINFO_RADIOITEM_DATA (N_("None"), NULL, 
                                select_view_scalardata, 
                                GUINT_TO_POINTER(0), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("Vorticity"), NULL, 
                                select_view_scalardata, 
                                GUINT_TO_POINTER(1), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("Shear strain"), NULL, 
                                select_view_scalardata, 
                                GUINT_TO_POINTER(2), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("Normal strain"), NULL, 
                                select_view_scalardata, 
                                GUINT_TO_POINTER(3), NULL),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo vectorscale_menu_display[] = {
    GNOMEUIINFO_RADIOITEM_DATA (N_("1"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_0 */ 0), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("2"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_1 */ 1), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("4"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_2 */ 2), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("8"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_3 */ 3), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("16"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_4 */ 4), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("32"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_5 */ 5), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("64"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_6 */ 6), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("128"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_7 */ 7), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("256"), NULL, select_vectorscale, 
                                GUINT_TO_POINTER(/* VECTOR_SCALE_8 */ 8), NULL),

    GNOMEUIINFO_END
};



static 
GnomeUIInfo vectorcolor_menu_display[] = {
    GNOMEUIINFO_RADIOITEM_DATA (N_("Peak nr"), NULL, select_vectorcolor, 
                                GUINT_TO_POINTER(SHOW_PEAKNR), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("SNR"), NULL, select_vectorcolor, 
                                GUINT_TO_POINTER(SHOW_SNR), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("Magnitude gray"), NULL, select_vectorcolor, 
                                GUINT_TO_POINTER(SHOW_MAGNITUDE_GRAY), NULL),
    GNOMEUIINFO_RADIOITEM_DATA (N_("Magnitude color"), NULL, select_vectorcolor, 
                                GUINT_TO_POINTER(SHOW_MAGNITUDE), NULL),

    GNOMEUIINFO_END
};



static 
GnomeUIInfo zoomscale_radiolist[] = {
    GNOMEUIINFO_RADIOLIST (zoomscale_menu_display),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo view_scalardata_radiolist[] = {
    GNOMEUIINFO_RADIOLIST (view_scalardata_display),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo vectorscale_radiolist[] = {
    GNOMEUIINFO_RADIOLIST (vectorscale_menu_display),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo vectorcolor_radiolist[] = {
    GNOMEUIINFO_RADIOLIST (vectorcolor_menu_display),
    GNOMEUIINFO_END
};



static 
GnomeUIInfo display_menu_uiinfo[] = {
    GNOMEUIINFO_TOGGLEITEM (N_("View menubar"), N_("Displays menubar in window"),
                            view_toggle_menubar, NULL),
    GNOMEUIINFO_TOGGLEITEM (N_("View rulers"), N_("Displays rulers in window"),
                            view_toggle_rulers, NULL),
    GNOMEUIINFO_TOGGLEITEM (N_("Stretch auto"), N_("Stretch display automatic when zooming"),
                            view_toggle_stretch_display_auto, NULL),
    GNOMEUIINFO_ITEM_NONE (N_("Stretch display"), 
                           N_("Stretch or fit display window to the image area"),
                           view_toggle_stretch_display),
    GNOMEUIINFO_SUBTREE (N_("Zoom in/out"), zoomscale_radiolist),  
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_SUBTREE (N_("View background"), 
                         view_background_radiolist),  
    GNOMEUIINFO_SUBTREE (N_("View piv data"), view_piv_display),  
    GNOMEUIINFO_SUBTREE (N_("View scalar data"), 
                         view_scalardata_radiolist),  
    GNOMEUIINFO_SUBTREE (N_("Vector scale"), vectorscale_radiolist),  
    GNOMEUIINFO_SUBTREE (N_("Vector color"), vectorcolor_radiolist),  
    GNOMEUIINFO_END
};


#endif /* DISPLAY_MENUS_H */
