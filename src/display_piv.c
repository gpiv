/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------

gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
  libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

This file is part of gpiv.

Gpiv is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

#include "gpiv_gui.h"
#include "display_piv.h"


static guint32
create_vector_color (gint peak_no, 
		     gfloat snr, 
		     gfloat dl
		     );


gfloat
dxdy_min (GpivPivData *piv_data
          )
/*-----------------------------------------------------------------------------
 * Calculates maximum particle displacement from dx and dy of a piv data-set
 */
{
    guint i, j;
    gfloat dl_abs = 0.0, dl_min = 10.0e+9;
    

    /*     assert (piv_data->nx != 0); */
    /*     assert (piv_data->ny != 0); */
    
    if (piv_data == NULL) {
        gpiv_error ("dxdy_min: piv_data == NULL");
    }

    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            if (piv_data->peak_no[i][j] >= 1) {
                dl_abs = sqrt (piv_data->dx[i][j] * piv_data->dx[i][j] + 
                               piv_data->dy[i][j] * piv_data->dy[i][j]);
                if (dl_abs < dl_min) {
                    dl_min = dl_abs;
                }
            }
        }
    }


    return dl_min;
}



gfloat
dxdy_max (GpivPivData *piv_data
          )
/*-----------------------------------------------------------------------------
 * Calculates maximum particle displacement from dx and dy of a piv data-set
 */
{
    guint i, j;
    gfloat dl_abs = 0.0, dl_max = 0.0;
    
/*     g_message ("dxdy_max:: 0 nx = %d ny = %d",  */
/*                piv_data->nx, piv_data->ny); */
    if (piv_data == NULL) {
        gpiv_error ("dxdy_max: piv_data == NULL");
    }

    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            if (piv_data->peak_no[i][j] >= 1) {
                dl_abs = sqrt (piv_data->dx[i][j] * piv_data->dx[i][j] + 
                               piv_data->dy[i][j] * piv_data->dy[i][j]);
                if (dl_abs > dl_max) {
                    dl_max = dl_abs;
                }
            }
        }
    }

    return dl_max;
}



void 
create_vector (PivData *pida,
               guint i, 
               guint j
               )
/* ----------------------------------------------------------------------------
 * Displays a single PIV vector on a Gnome canvas
 */
{
    GnomeCanvasPoints *points = NULL;
    Display *disp = display_act;

    gfloat **point_x = NULL;
    gfloat **point_y = NULL;
    gfloat **dx = NULL, **dy = NULL;
    gint **peak_no =  NULL;
    gfloat **snr = NULL;
    gfloat dl = 0.0;
    guint32 color_val = 0;


    if (pida->piv_data == NULL) return;
    point_x = pida->piv_data->point_x;
    point_y = pida->piv_data->point_y;
    dx = pida->piv_data->dx;
    dy = pida->piv_data->dy;
    peak_no =  pida->piv_data->peak_no;
    snr = pida->piv_data->snr;

    dl = sqrt (dx[i][j] * dx[i][j] + dy[i][j] * dy[i][j]);
    points = gnome_canvas_points_new (2);


    /*
     * Fill out the points
     */

    points->coords[0] = point_x[i][j];
    points->coords[1] = point_y[i][j];
    points->coords[2] = point_x[i][j] + dx[i][j] * gpiv_par->display__vector_scale;
    points->coords[3] = point_y[i][j] + dy[i][j] * gpiv_par->display__vector_scale;


    color_val = create_vector_color (peak_no[i][j], snr[i][j], dl);

    if (pida->gci_vector[i][j] != NULL) {
        /*         g_warning ("create_vector:: gci_vector[%d][%d] != NULL ==> destroying", */
        /*                     i, j); */
        destroy_vector (pida, i, j);
    }

    pida->gci_vector[i][j] =
        gnome_canvas_item_new (gnome_canvas_root
                               (GNOME_CANVAS (disp->canvas)),
                               gnome_canvas_line_get_type (),
                               "points", points,
                               "fill_color_rgba", color_val,
                               "width_units", (double) THICKNESS,
                               "last_arrowhead", TRUE,
                               "arrow_shape_a", (double) ARROW_LENGTH * 
                               ARROW_FACT * dl * gpiv_par->display__vector_scale + 
                               ARROW_ADD,
                               "arrow_shape_b", (double) ARROW_EDGE *
                               ARROW_FACT * dl * gpiv_par->display__vector_scale + 
                               ARROW_ADD,
                               "arrow_shape_c", (double) ARROW_WIDTH *
                               ARROW_FACT * dl * gpiv_par->display__vector_scale + 
                               ARROW_ADD,
                               NULL);

    gnome_canvas_points_free (points);
    
}



void 
update_vector (PivData *pida,
               guint i, 
               guint j
               )
/* ----------------------------------------------------------------------------
 * Updates a single PIV vector on a Gnome canvas
 */
{
    GnomeCanvasPoints *points;

    gfloat **point_x = NULL, **point_y = NULL;
    gfloat **dx = NULL, **dy = NULL;
    gfloat **snr = NULL;
    gint **peak_no = NULL;
    gfloat dl = 0.0;
    guint32 color_val = 0;


    if (pida->piv_data == NULL) return;
    point_x = pida->piv_data->point_x;
    point_y = pida->piv_data->point_y;
    dx = pida->piv_data->dx;
    dy = pida->piv_data->dy;
    peak_no =  pida->piv_data->peak_no;
    snr = pida->piv_data->snr;

    dl = sqrt (dx[i][j] * dx[i][j] + dy[i][j] * dy[i][j]);
    points = gnome_canvas_points_new (2);

    /*
     * Fill out the points
     */
    points->coords[0] = point_x[i][j];
    points->coords[1] = point_y[i][j];
    points->coords[2] = point_x[i][j] + dx[i][j] * gpiv_par->display__vector_scale;
    points->coords[3] = point_y[i][j] + dy[i][j] * gpiv_par->display__vector_scale;

    color_val = create_vector_color (peak_no[i][j], snr[i][j], dl);

    if (pida->gci_vector[i][j] != NULL) {
        gnome_canvas_item_set (GNOME_CANVAS_ITEM (pida->gci_vector[i][j]),
                               "points", points,
                               "fill_color_rgba", color_val,
                               "width_units", (double) THICKNESS,
                               "last_arrowhead", TRUE,
                               "arrow_shape_a", (double) ARROW_LENGTH *
                               ARROW_FACT * dl * gpiv_par->display__vector_scale
                               + ARROW_ADD,
                               "arrow_shape_b", (double) ARROW_EDGE *
                               ARROW_FACT * dl * gpiv_par->display__vector_scale
                               + ARROW_ADD,
                               "arrow_shape_c", (double) ARROW_WIDTH * 
                               ARROW_FACT * dl * gpiv_par->display__vector_scale
                               + ARROW_ADD,
                               NULL);
    }

    gnome_canvas_points_free (points);
}



void 
destroy_vector (PivData *pida,
                guint i, 
                guint j
                )
/* ----------------------------------------------------------------------------
 * Detroys a single PIV vector on a Gnome canvas
 */
{
    if (pida->gci_vector[i][j] != NULL) {
        gtk_object_destroy (GTK_OBJECT (pida->gci_vector[i][j]));
        pida->gci_vector[i][j] = NULL;
    }
}



void 
create_all_vectors (PivData *pida
                    )
/* ---------------------------------------------------------------------------
 * Displays all PIV vectors on a Gnome canvas
 */
{
    guint i, j;
    guint nx = 0, ny = 0;


    if (pida->piv_data == NULL) return;

    nx = pida->piv_data->nx;
    ny = pida->piv_data->ny;

    if (pida->exist_vec) {
        destroy_all_vectors (pida);
    }

    gpiv_var->dl_max = dxdy_max (pida->piv_data);
    gpiv_var->dl_min = dxdy_min (pida->piv_data);
    for (i = 0; i < ny; i++) {
	for (j = 0; j < nx; j++) {
	    create_vector (pida, i, j);
            
	}
    }

    pida->exist_vec = TRUE;
}



void 
show_all_vectors (PivData *pida
                  )
/* ----------------------------------------------------------------------------
 * Shows all PIV vectors on a Gnome canvas
 */
{
    guint i, j;
    guint nx = 0, ny = 0;


    if (pida->piv_data == NULL) return;
    nx = pida->piv_data->nx;
    ny = pida->piv_data->ny;

    for (i = 0; i < ny; i++) {
	for (j = 0; j < nx; j++) {
            if (pida->gci_vector[i][j] != NULL) {
                gnome_canvas_item_show (GNOME_CANVAS_ITEM
                                        (pida->gci_vector[i][j]));
            }
	}
    }
}



void 
hide_all_vectors (PivData *pida
                  )
/* ----------------------------------------------------------------------------
 * Hides all PIV vectors on a Gnome canvas
 */
{
    guint i, j;
    guint nx = 0, ny = 0;


    if (pida->piv_data == NULL) return;
    nx = pida->piv_data->nx;
    ny = pida->piv_data->ny;


    for (i = 0; i < ny; i++) {
	for (j = 0; j < nx; j++) {
            if (pida->gci_vector[i][j] != NULL) {
                gnome_canvas_item_hide (GNOME_CANVAS_ITEM
                                        (pida->gci_vector[i][j]));
            }
	}
    }
}



void 
update_all_vectors (PivData *pida
                    )
/* ----------------------------------------------------------------------------
 * Scales PIV vectors for Gnome canvas
 */
{
    guint i, j;
    guint nx = 0, ny = 0;


    if (pida->piv_data == NULL) return;
    nx = pida->piv_data->nx;
    ny = pida->piv_data->ny;

    gpiv_var->dl_max = dxdy_max (pida->piv_data);
    gpiv_var->dl_min = dxdy_min (pida->piv_data);
    for (i = 0; i < ny; i++) {
	for (j = 0; j < nx; j++) {
	    update_vector (pida, i, j);
	}
    }
}



void 
destroy_all_vectors (PivData *pida
                     )
/* ----------------------------------------------------------------------------
 * Destroys all PIV vectors on a Gnome canvas
 */
{
    guint i, j;
    guint nx = 0, ny = 0;


    if (pida->piv_data == NULL) return;
    nx = pida->piv_data->nx;
    ny = pida->piv_data->ny;

    if (pida->exist_vec) {
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                destroy_vector (pida, i, j);
            }
        }
        /*     } else { */
        /*         g_warning ("destroy_all_vectors: exist_vec = FALSE"); */
    }
    pida->exist_vec = FALSE;
}


/*
 * Local functions
 */
#ifdef CANVAS_AA
static guint32
create_vector_color (gint peak_no, 
                     gfloat snr, 
                     gfloat dl
                     )
/* ----------------------------------------------------------------------------
 * Create vector color for in canvas
 */
{
    guint32 color = 0;
    GdkColor *color_val = NULL;
    guint shift_factor;
    Display *disp = display_act;

    color_val = (GdkColor *)g_malloc (sizeof (GdkColor));

    if (gpiv_par->display__vector_color == SHOW_PEAKNR) {
        if (peak_no == -1) {
            color_val->red = BYTEVAL;
            color_val->green = 0;
            color_val->blue = 0;

        } else if (peak_no == 0) {
            color_val->red = 0;
            color_val->green = 0;
            color_val->blue = BYTEVAL;

        } else if (peak_no == 1) {
            color_val->red = 0;
            color_val->green = BYTEVAL;
            color_val->blue = 0;

        } else if (peak_no == 2) {
            color_val->red = 0;
            color_val->green = BYTEVAL;
            color_val->blue = BYTEVAL;

        } else  {
            /* if (peak_no[i][j] == 3) */
            color_val->red = BYTEVAL / 2;
            color_val->green = BYTEVAL / 2;
            color_val->blue = BYTEVAL / 2;
        }


        color = GNOME_CANVAS_COLOR (color_val->red, 
                                    color_val->green, 
                                    color_val->blue);

    } else if (gpiv_par->display__vector_color == SHOW_SNR) {
        if (snr >= disp->pida->valid_par->residu_max) {
            /*             color = "red"; */
            color_val->red = BYTEVAL;
            color_val->green = 0;
            color_val->blue = 0;
        } else {
            /*             color = "green";     */
            color_val->red = 0;
            color_val->green = BYTEVAL;
            color_val->blue = 0;
        }

        color = GNOME_CANVAS_COLOR (color_val->red, 
                                    color_val->green, 
                                    color_val->blue);

    } else if (gpiv_par->display__vector_color == SHOW_MAGNITUDE_GRAY) {
        if (peak_no >= 1) {
            color_val->red = (gint) (BYTEVAL * (dl - gpiv_var->dl_min) / 
                                     (gpiv_var->dl_max - gpiv_var->dl_min));
            color_val->green = (gint) (BYTEVAL * (dl - gpiv_var->dl_min) / 
                                       (gpiv_var->dl_max - gpiv_var->dl_min));
            color_val->blue = (gint) (BYTEVAL * (dl - gpiv_var->dl_min) / 
                                      (gpiv_var->dl_max - gpiv_var->dl_min));
        } else {
            color_val->red = 0;
            color_val->green = 0;
            color_val->blue = 0;
        }

        color = GNOME_CANVAS_COLOR (color_val->red, 
                                    color_val->green, 
                                    color_val->blue);

    } else if (gpiv_par->display__vector_color == SHOW_MAGNITUDE) {
        if (peak_no >= 1) {
            shift_factor = (gint) (28.0 * (dl - gpiv_var->dl_min) / 
                                   (gpiv_var->dl_max - gpiv_var->dl_min));
            color = (( 0xFFFF << shift_factor) | 0xFF);


        } else {
            color = GNOME_CANVAS_COLOR (0, 0, 0);
        }
    }
    
    g_free (color_val);
    return color;
}


#else /* CANVAS_AA */
static guint32
create_vector_color (gint peak_no, 
                     gfloat snr, 
                     gfloat dl
                     )
/* ----------------------------------------------------------------------------
 * Create vector color for in canvas
 */
{
    guint32 color_val = 0;
    gint shift_factor;

    if (gpiv_par->display__vector_color == SHOW_PEAKNR) {
        if (peak_no == -1) {
            /* 	      color = "red"; */
            color_val =  (gint) (BYTEVAL);
            color_val =  (color_val << BITSHIFT_RED);
        } else if (peak_no == 0) {
            /* 	      color = "lightblue"; */
            color_val =  (gint) (BYTEVAL);
            color_val =  (color_val << BITSHIFT_BLUE);
        } else if (peak_no == 1) {
            /* 	      color = "green"; */
            color_val =  (gint) (BYTEVAL);
            color_val =  (color_val << BITSHIFT_GREEN);
        } else if (peak_no == 2) {
            /* 	      color = "yellow"; */
            color_val =  (gint) (BYTEVAL);
            color_val =  (color_val << BITSHIFT_GREEN) 
                + (color_val << BITSHIFT_BLUE);
        } else  {
            /* if (peak_no == 3) */
            /* 	      color = "gray";     */
            color_val =  (gint) (127);
            color_val =  (color_val << BITSHIFT_RED)
                + (color_val << BITSHIFT_GREEN) 
                + (color_val << BITSHIFT_BLUE);
        }


    } else if (gpiv_par->display__vector_color == SHOW_SNR) {
        if (snr >= gl_valid_par->residu_max) {
            /*             color = "red"; */
            color_val =  (gint) (BYTEVAL);
            color_val =  (color_val << BITSHIFT_RED);
        } else {
            /*             color = "green";     */
            color_val =  (gint) (BYTEVAL);
            color_val =  (color_val << BITSHIFT_GREEN);
        }

    } else if (gpiv_par->display__vector_color == SHOW_MAGNITUDE_GRAY) {
        if (peak_no >= 1) {
            color_val =  (gint) (BYTEVAL * (dl - gpiv_var->dl_min) / 
                                 (gpiv_var->dl_max - gpiv_var->dl_min));
            color_val =  (color_val << BITSHIFT_RED) + 
                (color_val << BITSHIFT_GREEN) + (color_val << BITSHIFT_BLUE);
        } else {
            color_val =  0;
        }

    } else if (gpiv_par->display__vector_color == SHOW_MAGNITUDE) {
        if (peak_no >= 1) {
            shift_factor = (gint) (28.0 * (dl - gpiv_var->dl_min) / 
                                   (gpiv_var->dl_max - gpiv_var->dl_min));
            color_val = ( 0xFFFF << shift_factor);
        } else {
            color_val =  0;
        }
    }


    return color_val;
}

#endif /* CANVAS_AA */


