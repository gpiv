/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */
 
/*----------------------------------------------------------------------

  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

#ifndef DISPLAY_PIV_H
#define DISPLAY_PIV_H

/*
 * PIV vectors in a Gnome canvas
 */
/* Determines maximum for scaling */
gfloat
dxdy_min (GpivPivData *piv_data);

gfloat
dxdy_max (GpivPivData *piv_data);

/* Displays a single PIV vector */
void 
create_vector (PivData *pida,
	       guint i, 
	       guint j);  

/* Updates a single PIV vector */
void 
update_vector (PivData *pida,
	       guint i, 
	       guint j); 

/* Destroys a single PIV vector  */
void 
destroy_vector (PivData *pida,
		guint i, 
		guint j); 

/* Displays all PIV vectors*/
void 
create_all_vectors (PivData *pida);

/* Shows all PIV vectors */
void 
show_all_vectors (PivData *pida);

/* Hides all PIV vectors */
void 
hide_all_vectors (PivData *pida); 

/* Updates all PIV vectors */
void 
update_all_vectors (PivData *pida);

/* Destroys all PIV vectors */
void 
destroy_all_vectors (PivData *pida);


#endif /* DISPLAY_PIV_H */
