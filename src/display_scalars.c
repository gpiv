/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------

gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
  libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

This file is part of gpiv.

Gpiv is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

#include "gpiv_gui.h"
#include "display_scalars.h"

#ifdef CANVAS_AA
static guint
create_scalar_color (gint flag,
		     gfloat sc_min, 
		     gfloat sc_max, 
		     gfloat sc, 
		     gfloat scale_factor
		     );

#else /* CANVAS_AA */

static guint
create_scalar_color (gint flag,
		     gfloat sc_min, 
		     gfloat sc_max, 
		     gfloat sc, 
		     gfloat scale_factor
		     );
#endif /* CANVAS_AA */


static void
destroy_filledrect (GnomeCanvasItem *filled_rect
		    );

static void
show_filledrect (GnomeCanvasItem *filled_rect
		 );

static void
hide_filledrect (GnomeCanvasItem *filled_rect
		 );

static void 
create_vor (Display *disp,
	    gint i, 
	    gint j,
	    guint col_val
	    );

static void 
create_sstrain (Display *disp,
		gint i, 
		gint j,
		guint col_val
		);

static void 
create_nstrain (Display *disp,
		gint i, 
		gint j,
		guint col_val
		);

/*
 * Public functions
 */
void 
create_all_scalars(Display *disp,
                   gint type
                   )
/* ----------------------------------------------------------------------------
 * Displays all scalar gnome canvas items
 */
{
    int i, j;
    int nx = 0, ny = 0, **flag = NULL;

    float **sc = NULL;
    float sc_min = 10000.0, sc_max = -10000.0;
    guint col_val = 0;
    GpivScalarData *scalar_data;    
    GtkWidget *view_piv_display0 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display0");
    GtkWidget * view_piv_display1 = 
        gtk_object_get_data(GTK_OBJECT(disp->mwin), 
                            "view_piv_display1");
    float scale_factor = 0.0;


     if (type == GPIV_VORTICITY) {
       scalar_data = disp->pida->vor_data;
        sc = scalar_data->scalar;
        nx = disp->pida->vor_data->nx;
        ny = disp->pida->vor_data->ny;
        flag = disp->pida->vor_data->flag;
/*         scalar_colval(disp); */

    } else if (type == GPIV_S_STRAIN) {
        scalar_data = disp->pida->sstrain_data;
        sc = scalar_data->scalar;
        nx = disp->pida->sstrain_data->nx;
        ny = disp->pida->sstrain_data->ny;
        flag = disp->pida->sstrain_data->flag;

    } else if (type == GPIV_N_STRAIN) {
        scalar_data = disp->pida->nstrain_data;
        sc = scalar_data->scalar;
        nx = disp->pida->nstrain_data->nx;
        ny = disp->pida->nstrain_data->ny;
        flag = disp->pida->nstrain_data->flag;
    } else {
        g_warning(_("create_all_scalars: unexisting type"));
    }

/*
 * normalizing data between 0 and 1 to define color value col_val
 */
/* scalar_colval(Display * disp) { */
    for (i = 0; i < ny; i++) {
        for (j = 0; j < nx; j++) {
            if (flag[i][j] != -1) {
                if (sc[i][j] < sc_min) sc_min = sc[i][j];
                if (sc[i][j] > sc_max) sc_max =  sc[i][j];
            }
        }
    }

    if (sc_min < 0) {
        if (-sc_min >= sc_max) {
            scale_factor = 1.0 / -sc_min;
        } else {
            scale_factor = 1.0 / sc_max;
        }
    } else {
        scale_factor = 1.0 / sc_max;
    }


     for (i = 0; i < ny; i++) {
	  for (j = 0; j < nx; j++) {
              col_val = create_scalar_color(flag[i][j], sc_min, sc_max, 
                                            sc[i][j], scale_factor);
              if (type == GPIV_VORTICITY) {
                  create_vor(disp, i, j, col_val);
              } else if (type == GPIV_S_STRAIN) {
                  create_sstrain(disp, i, j, col_val);
              } else if (type == GPIV_N_STRAIN) {
                  create_nstrain(disp, i, j, col_val);
              } else {
                  g_warning(_("create_all_scalars: unexisting type"));
              }
          }
     }
     
     if (GTK_CHECK_MENU_ITEM(view_piv_display0)->active) {
         for (i = 0; i < ny; i++) {
             for (j = 0; j < nx; j++) {
                 gnome_canvas_item_raise_to_top
                     (disp->intreg->gci_intreg1[i][j]);
                 gnome_canvas_item_raise_to_top
                     (display_act->intreg->gci_intreg2[i][j]);
             }
         }
     }

     if (GTK_CHECK_MENU_ITEM(view_piv_display1)->active) {
         if (disp->pida->exist_piv && disp->display_piv) {
             for (i = 0; i < ny; i++) {
                 for (j = 0; j < nx; j++) {
                     gnome_canvas_item_raise_to_top
                         (disp->pida->gci_vector[i][j]);
                 }
             }
         }
     }

}



void 
show_all_scalars(Display *disp,
                 gint type
                 )
/* ----------------------------------------------------------------------------
 * Shows scalar gnome canvas items
 */
{
    guint i = 0, j = 0;
    guint nx = 0, ny = 0;

    if (disp->pida->vor_data == NULL) return;

    if (type == GPIV_VORTICITY
        && disp->pida->vor_data != NULL) {
        nx = disp->pida->vor_data->nx;
        ny = disp->pida->vor_data->ny;
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
/*               if (i ==0 && j==0)  */
                show_filledrect(display_act->pida->gci_scalar_vor[i][j]);
            }
        }
        display_act->display_scalar = SHOW_SC_VORTICITY;
    }

    if (type == GPIV_S_STRAIN
        && disp->pida->sstrain_data != NULL) {
        nx = disp->pida->sstrain_data->nx;
        ny = disp->pida->sstrain_data->ny;
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                show_filledrect(display_act->pida->gci_scalar_sstrain[i][j]);
            }
        }
        display_act->display_scalar = SHOW_SC_SSTRAIN;
    }

    if (type == GPIV_N_STRAIN
        && disp->pida->nstrain_data != NULL) {
        nx = disp->pida->nstrain_data->nx;
        ny = disp->pida->nstrain_data->ny;
        for (i = 0; i < ny; i++) {
            for (j = 0; j < nx; j++) {
                show_filledrect(display_act->pida->gci_scalar_nstrain[i][j]);
            }
        }
        display_act->display_scalar = SHOW_SC_NSTRAIN;
    }

}



void 
hide_all_scalars (Display *disp,
                 gint type
                 )
/* ----------------------------------------------------------------------------
 * Hides all scalar gnome canvas items
 */
{
    guint i, j;
/*     int nx = disp->pida->scalar_data->nx, ny = disp->pida->scalar_data->ny; */


    if (disp->pida->vor_data == NULL) return;

    if (type == GPIV_VORTICITY
        && disp->pida->vor_data != NULL) {
        for (i = 0; i < disp->pida->vor_data->ny; i++) {
            for (j = 0; j < disp->pida->vor_data->nx; j++) {
/*               if (i ==0 && j==0)  */
                hide_filledrect(disp->pida->gci_scalar_vor[i][j]);
            }
        }
        disp->display_scalar = SHOW_SC_NONE;
    }

    if (type == GPIV_S_STRAIN
        && disp->pida->sstrain_data != NULL) {
        for (i = 0; i < disp->pida->sstrain_data->ny; i++) {
            for (j = 0; j < disp->pida->sstrain_data->nx; j++) {
                hide_filledrect(disp->pida->gci_scalar_sstrain[i][j]);
           }
        }
        disp->display_scalar = SHOW_SC_NONE;
    }

    if (type == GPIV_N_STRAIN
        && disp->pida->nstrain_data != NULL) {
        for (i = 0; i < disp->pida->nstrain_data->ny; i++) {
            for (j = 0; j < disp->pida->nstrain_data->nx; j++) {
                hide_filledrect(disp->pida->gci_scalar_nstrain[i][j]);
            }
        }
        disp->display_scalar = SHOW_SC_NONE;
      }


}



void 
destroy_all_scalars(Display *disp,
                    gint type
                    )
/* ----------------------------------------------------------------------------
 * Destroys scalar canvas items
 */
{
    guint i, j;


    if (disp->pida->vor_data == NULL) return;

    if (type == GPIV_VORTICITY
        && disp->pida->vor_data != NULL) {
        for (i = 0; i < disp->pida->vor_data->ny; i++) {
            for (j = 0; j < disp->pida->vor_data->nx; j++) {
/*               if (i ==0 && j==0)  */
                destroy_filledrect(disp->pida->gci_scalar_vor[i][j]);
            }
        }

    } else if (type == GPIV_S_STRAIN
        && disp->pida->sstrain_data != NULL) {
        for (i = 0; i < disp->pida->sstrain_data->ny; i++) {
            for (j = 0; j < disp->pida->sstrain_data->nx; j++) {
                destroy_filledrect(disp->pida->gci_scalar_sstrain[i][j]);
            }
        }

    } else if (type == GPIV_N_STRAIN
        && disp->pida->nstrain_data != NULL) {
        for (i = 0; i < disp->pida->nstrain_data->ny; i++) {
            for (j = 0; j < disp->pida->nstrain_data->nx; j++) {
                destroy_filledrect(disp->pida->gci_scalar_nstrain[i][j]);
            }
        }
    } else {
        g_warning(_("destroy_all_scalars: unexisting type"));
    }
    
    
}


/*
 * Local functions
 */
#ifdef CANVAS_AA
/*
 * BUGFIX repair color representation for canvas_aa
 */

static guint
create_scalar_color (gint flag,
                    gfloat sc_min, 
                    gfloat sc_max, 
                    gfloat sc, 
                    gfloat scale_factor
                    )
/* ----------------------------------------------------------------------------
 * Create scalar color for in canvas
 */
{
    guint color = 0;
    GdkColor *color_val = NULL;

    color_val = (GdkColor *)g_malloc(sizeof(GdkColor));
    if (flag != -1) {
        if (sc_min < 0) {
            if (sc <0) {
                color_val->red = (gint) (-sc * BYTEVAL * scale_factor);
                color_val->green = 0;
                color_val->blue =  0;
            } else {
                color_val->red =  0;
                color_val->green = 0;
                color_val->blue = (int) (sc * BYTEVAL * scale_factor);
            }
        } else {
            color_val->red = 0;
            color_val->green = 0;
            color_val->blue =  (int) (sc * BYTEVAL * scale_factor);
        }
    } else {
                color_val->red = 0;
                color_val->green = BYTEVAL / 2;
                color_val->blue = 0;
    }

    color = GNOME_CANVAS_COLOR(color_val->red, 
                               color_val->green, 
                               color_val->blue); 
    g_free(color_val);
    return color;
}


#else /* CANVAS_AA */

static guint
create_scalar_color (gint flag,
                    gfloat sc_min, 
                    gfloat sc_max, 
                    gfloat sc, 
                    gfloat scale_factor
                    )
/* ----------------------------------------------------------------------------
 * Create scalar color for in canvas
 */
{
    guint color_val = 0;

    if (flag != -1) {
        if (sc_min < 0) {
            if (sc <0) {
                color_val =  (int) (-sc * BYTEVAL * scale_factor);
                color_val =  (color_val << BITSHIFT_RED);
            } else {
                color_val =  (int) (sc * BYTEVAL * scale_factor);
                color_val =  (color_val << BITSHIFT_BLUE);
            }
        } else {
            color_val =  (int) (sc * BYTEVAL * scale_factor);
            color_val =  (color_val << BITSHIFT_BLUE);
        }
    } else {
        color_val = 128 << BITSHIFT_GREEN;
    }


    return color_val;
}


#endif /* CANVAS_AA */


static void
destroy_filledrect(GnomeCanvasItem *filled_rect
                   )
/* ----------------------------------------------------------------------------
 * Destroys a single filled rectangular canvas item
 */
{
    if (filled_rect != NULL) {
        gtk_object_destroy(GTK_OBJECT(filled_rect));
        filled_rect = NULL;
    }
}



static void
show_filledrect(GnomeCanvasItem *filled_rect
                )
/* ----------------------------------------------------------------------------
 * Shows a single filled rectangular canvas item
 */
{
    if (filled_rect != NULL) {
        gnome_canvas_item_show
            (GNOME_CANVAS_ITEM(filled_rect));
    }
}



static void
hide_filledrect(GnomeCanvasItem *filled_rect
                )
/* ----------------------------------------------------------------------------
 * Hides a single filled rectangular canvas item
 */
{
    if (filled_rect != NULL) {
        gnome_canvas_item_hide
            (GNOME_CANVAS_ITEM(filled_rect));
    }
}



static void 
create_vor (Display *disp,
           gint i, 
           gint j,
           guint col_val
           )
/* ----------------------------------------------------------------------------
 * Creates vorticity gnome canvas item by coloring the interrogation area
 */
{
    float **x, **y;
    int start_x, start_y, end_x, end_y;
 
    GnomeCanvasPoints *points;
    points = gnome_canvas_points_new(5);


/*
 * Using centre points of interr regs
 */
    x = disp->pida->vor_data->point_x;
    y = disp->pida->vor_data->point_y;
    start_x = (int) x[i][j] - disp->pida->piv_par->int_size_f / 2;
    start_y = (int) y[i][j] - disp->pida->piv_par->int_size_f / 2;
    end_x = (int) x[i][j] + disp->pida->piv_par->int_size_f / 2;
    end_y = (int) y[i][j] + disp->pida->piv_par->int_size_f / 2;
    
    disp->pida->gci_scalar_vor[i][j] = 
        gnome_canvas_item_new(gnome_canvas_root
                              (GNOME_CANVAS(display_act->canvas)),
                              gnome_canvas_rect_get_type(),
                              "x1", (double) start_x,
                              "y1", (double) start_y,
                              "x2", (double) end_x,
                              "y2", (double) end_y,
                              "fill_color_rgba", col_val,
                              NULL);

    gnome_canvas_points_free(points);
}



static void 
create_sstrain (Display *disp,
               gint i, 
               gint j,
               guint col_val
               )
/* ----------------------------------------------------------------------------
 * Creates shear strain gnome canvas item by coloring the interrogation area
 */
{
    float **x, **y;
    int start_x, start_y, end_x, end_y;
 
    GnomeCanvasPoints *points;
    points = gnome_canvas_points_new(5);


/*
 * Using centre points of interr regs
 */
    x = disp->pida->sstrain_data->point_x;
    y = disp->pida->sstrain_data->point_y;
    start_x = (int) x[i][j] - disp->pida->piv_par->int_size_f / 2;
    start_y = (int) y[i][j] - disp->pida->piv_par->int_size_f / 2;
    end_x = (int) x[i][j] + disp->pida->piv_par->int_size_f / 2;
    end_y = (int) y[i][j] + disp->pida->piv_par->int_size_f / 2;
    
    disp->pida->gci_scalar_sstrain[i][j] = 
        gnome_canvas_item_new(gnome_canvas_root
                              (GNOME_CANVAS(display_act->canvas)),
                              gnome_canvas_rect_get_type(),
                              "x1", (double) start_x,
                              "y1", (double) start_y,
                              "x2", (double) end_x,
                              "y2", (double) end_y,
                              "fill_color_rgba", col_val,
                              NULL);
    
    gnome_canvas_points_free(points);
}



static void 
create_nstrain (Display *disp,
               gint i, 
               gint j,
               guint col_val
               )
/* ----------------------------------------------------------------------------
 * Creates normal strain gnome canvas item by coloring the interrogation area
 */
{
    float **x, **y;
    int start_x, start_y, end_x, end_y;
 
    GnomeCanvasPoints *points;
    points = gnome_canvas_points_new(5);


/*
 * Using centre points of interr regs
 */

    x = disp->pida->nstrain_data->point_x;
    y = disp->pida->nstrain_data->point_y;
    start_x = (int) x[i][j] - disp->pida->piv_par->int_size_f / 2;
    start_y = (int) y[i][j] - disp->pida->piv_par->int_size_f / 2;
    end_x = (int) x[i][j] + disp->pida->piv_par->int_size_f / 2;
    end_y = (int) y[i][j] + disp->pida->piv_par->int_size_f / 2;
    
    disp->pida->gci_scalar_nstrain[i][j] = 
        gnome_canvas_item_new(gnome_canvas_root
                              (GNOME_CANVAS(display_act->canvas)),
                              gnome_canvas_rect_get_type(),
                              "x1", (double) start_x,
                              "y1", (double) start_y,
                              "x2", (double) end_x,
                              "y2", (double) end_y,
                              "fill_color_rgba", col_val,
                              NULL);

    gnome_canvas_points_free(points);    
}
