/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------

gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
  libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

This file is part of gpiv.

Gpiv is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

#include "gpiv_gui.h"
#include "display.h"
#include "display_zoom.h"


void
set__canvas_size (Display *disp
		  );

void
set__adjusters (Display *disp
                );

gboolean
canvas_display_button_scroll (GtkWidget *widget, 
                              GdkEventScroll *event,
                              gpointer data
                              )
/*-----------------------------------------------------------------------------
 */
{
    Display * disp = gtk_object_get_data(GTK_OBJECT(widget), "disp");
    GpivConsole * gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    gint x, y;

    if (event->direction == 0) {
        disp->zoom_factor *= DISPLAY_ZOOMFACTOR_STEP;
    }

    if (event->direction == 1) {
        disp->zoom_factor /= DISPLAY_ZOOMFACTOR_STEP;
    }

    check__zoom_factor (&disp->zoom_factor);
    set__canvas_size (disp);
    if (gpiv_par->display__stretch_auto == TRUE) {
        stretch_window (disp);
    }
    set__adjusters (disp);
    set__hrulerscale (disp);
    set__vrulerscale (disp);
}


void 
check__zoom_factor (gfloat *factor
                    )
/*-----------------------------------------------------------------------------
 */
{
    if (*factor < DISPLAY_ZOOMFACTOR_MIN) {
        *factor = DISPLAY_ZOOMFACTOR_MIN;
    }

    if (*factor > DISPLAY_ZOOMFACTOR_MAX) {
        *factor = DISPLAY_ZOOMFACTOR_MAX;
    }
}


void
set__canvas_size (Display *disp
                  )
/*-----------------------------------------------------------------------------
 */
{
    gnome_canvas_set_pixels_per_unit (GNOME_CANVAS (disp->canvas), 
                                      disp->zoom_factor);
/*     gnome_canvas_set_center_scroll_region (GNOME_CANVAS(disp->canvas), TRUE); */

    if (disp->zoom_factor < disp->zoom_factor_old) {
        gtk_widget_set_size_request (disp->canvas, 
                                     (gint) (disp->zoom_factor * 
                                             disp->img->image->header->ncolumns), 
                                     (gint) (disp->zoom_factor * 
                                             disp->img->image->header->nrows));
            
        disp->zoom_factor_old = disp->zoom_factor;
    }

}


void
set__hrulerscale (Display *disp
                  )
/*-----------------------------------------------------------------------------
 */
{
    gfloat x_start;
    gfloat x_end;

    if (disp->pida->exist_piv && display_act->pida->scaled_piv) {
        x_start = (gfloat) (disp->x_adj_value - 1) 
            / (gfloat) disp->zoom_factor
            * disp->img->image->header->s_scale
            * 10e-4
            + disp->img->image->header->z_off_x
            ;

        x_end =  (gfloat) x_start
            + (disp->img->image->header->s_scale * (gfloat) disp->x_page_size 
               / (gfloat) disp->zoom_factor)
            * 10e-4;

    } else {
        x_start = (gfloat) (disp->x_adj_value - 1) / (gfloat) disp->zoom_factor;
        x_end = (gfloat) x_start 
            + (gfloat) disp->x_page_size / (gfloat) disp->zoom_factor;

    }

/*     g_message ("set__hrulerscale::  s_scale = %f z_off_x = %f x_start = %f x_end = %f",  */
/*                disp->img->image->header->s_scale,  */
/*                disp->img->image->header->z_off_x, */
/*                x_start,  */
/*                x_end); */
    gtk_ruler_set_range (GTK_RULER (disp->hruler),
                         x_start,
                         x_end,
                         x_start,
                         x_end);
}


void
set__vrulerscale (Display *disp
                  )
/*-----------------------------------------------------------------------------
 * Identic to display.c: on_adj_changed__adapt_vruler
 */
{
    gfloat y_start;
    gfloat y_end;

    if (disp->pida->exist_piv && display_act->pida->scaled_piv) {
        y_start = (gfloat) (disp->y_adj_value - 1) 
            / (gfloat) disp->zoom_factor
            * disp->img->image->header->s_scale
            * 10e-4
            + disp->img->image->header->z_off_y
            ;

       y_end =  (gfloat) y_start
            + (disp->img->image->header->s_scale * (gfloat) disp->y_page_size 
               / (gfloat) disp->zoom_factor)
            * 10e-4;

    } else {
        y_start = (gfloat) (disp->y_adj_value - 1) / (gfloat) disp->zoom_factor;
        y_end = (gfloat) y_start 
            + (gfloat) disp->y_page_size /(gfloat)  disp->zoom_factor;
    }

    gtk_ruler_set_range (GTK_RULER (disp->vruler),
                         y_start,
                         y_end,
                         y_start,
                         y_end);
}


void
set__adjusters (Display *disp
                )
/*-----------------------------------------------------------------------------
 */
{
    GtkAdjustment * hadj = GTK_ADJUSTMENT (disp->hadj);
    GtkAdjustment * vadj = GTK_ADJUSTMENT (disp->vadj) ;

    vadj->lower = 0.0;
    vadj->upper = (gdouble) (disp->img->image->header->nrows) 
        * disp->zoom_factor - 1.0;
    vadj->page_size = (gdouble) (disp->img->image->header->nrows) 
                                 * disp->zoom_factor_old;
    gtk_adjustment_set_value( GTK_ADJUSTMENT (vadj), 
                              (vadj->lower + vadj->upper - vadj->page_size)
                              / 2.0);


    hadj->lower = 0.0;
    hadj->upper = (gdouble) (disp->img->image->header->ncolumns)
        * disp->zoom_factor - 1.0;
    hadj->page_size = (gdouble) (disp->img->image->header->ncolumns)
                                 * disp->zoom_factor_old;
    gtk_adjustment_set_value (GTK_ADJUSTMENT (hadj), 
                              (hadj->lower + hadj->upper - hadj->page_size)
                              / 2.0);

/*     g_message ("set__adjusters:: HOR: lower = %f upper = %f size = %f", */
/*                hadj->lower, hadj->upper, hadj->page_size); */

    g_signal_emit_by_name (G_OBJECT (hadj), "changed");
    g_signal_emit_by_name (G_OBJECT (vadj), "changed");

}


void
stretch_window (Display *disp
                )
/*-----------------------------------------------------------------------------
 */
{
    GtkAdjustment * hadj = GTK_ADJUSTMENT (disp->hadj);
    GtkAdjustment * vadj = GTK_ADJUSTMENT (disp->vadj);
    gint screen_width = gdk_screen_width();
    gint screen_height = gdk_screen_height();

    gint display_width = (gint) (disp->zoom_factor * 
                                 disp->img->image->header->ncolumns
/*                                  + VIEW_HMARGE */
                                 );
    gint display_height = (gint) (disp->zoom_factor * 
                                  disp->img->image->header->nrows
/*                                   + VIEW_VMARGE */
                                  );

    if (display_width >= screen_width/*  - VIEW_HMARGE */) {
        display_width = screen_width - VIEW_HMARGE;
    }

    if (display_height >= screen_height - VIEW_VMARGE) {
        display_height = screen_height/*  - VIEW_VMARGE */;
    }

 
    gtk_widget_set_size_request (disp->canvas, 
                                 (gint) display_width/*  - VIEW_HMARGE */, 
                                 (gint) display_height/*  - VIEW_VMARGE */);

    disp->zoom_factor_old = disp->zoom_factor;
 
    vadj->lower = 0.0;
    vadj->upper = (gdouble) disp->img->image->header->nrows - 1;
    vadj->page_size = (gdouble) (disp->img->image->header->nrows);

    hadj->lower = 0.0;
    hadj->upper = (gdouble) disp->img->image->header->ncolumns - 1;
    hadj->page_size = (gdouble) (disp->img->image->header->ncolumns);

    g_signal_emit_by_name (G_OBJECT (hadj), "changed");
    g_signal_emit_by_name (G_OBJECT (vadj), "changed");
}


void
zoom_display (Display *disp, 
              gint zoom_index
              )
/*-----------------------------------------------------------------------------
 */
{
    gint width = 0, height = 0;
    gint new_width = 0, new_height = 0;
 
    if (disp != NULL) {
        disp->zoom_factor = zfactor[zoom_index];
        check__zoom_factor (&disp->zoom_factor);
        set__canvas_size (disp);
        if (gpiv_par->display__stretch_auto == TRUE) {
            stretch_window (disp);
        }
        set__adjusters (disp);
        set__hrulerscale (disp);
        set__vrulerscale (disp);
    }

}

