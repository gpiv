/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Graphical User Interface for gpiv
 * $Log: gpiv_gui.h,v $
 * Revision 1.6  2008-10-09 14:43:37  gerber
 * paralellized with OMP and MPI
 *
 * Revision 1.5  2008-09-25 13:32:22  gerber
 * Adapted for use on cluster (using MPI/OMP) parallelised gpiv_rr from gpivtools)
 *
 * Revision 1.4  2008-09-16 10:13:56  gerber
 * 2nd update because of cvs conflict
 *
 * Revision 1.3  2008-09-16 10:11:02  gerber
 * Updated because of cvs conflict
 *
 * Revision 1.2  2005-06-15 15:06:22  gerber
 * Optional Anti Aliased canvas for viewer
 *
 * Revision 1.1  2005/06/15 09:40:40  gerber
 * debugged, optimized
 *
 * Revision 1.9  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.8  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.7  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.6  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 * Revision 1.5  2003/09/01 11:17:14  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.4  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.3  2003/07/25 15:40:23  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef GPIV_H
#define GPIV_H

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h> /* for the "pause" function: */
#include <time.h>
#include <string.h>
#include <assert.h>
#include <math.h> /* for isnan */
#include <fftw3.h>
#include <gpiv.h>

#include <gnome.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#ifdef HAVE_GNOME_PRINT
#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-print-job.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprintui/gnome-print-dialog.h>
#include <libgnomeprintui/gnome-print-job-preview.h>
#endif /* HAVE_GNOME_PRINT */


/*  Revision Control System (RCS) version */
#define RCSID "$Id: gpiv_gui.h,v 1.6 2008-10-09 14:43:37 gerber Exp $" 
#define ERR_IVAL = 99           /* Some arbitrary integer error value */
#define ADJ_MIN -50.0	        /* minimum value for adjustment */
#define ADJ_MAX 50.0	        /* maximum value for adjustment */
#define ADJ_STEP 0.001          /* Step increment for adjustment */
#define ADJ_PAGE 0.1            /* Page increment for adjustment */
#define ADJ_ISTEP 1             /* Step increment for adjustment for integers */
#define ADJ_IPAGE 10            /* Page increment for adjustment for integers*/

#define MAX_LIST 5

#ifdef ENABLE_MPI
#define MPIRUN_CMD "mpirun -np"
#endif

/*
 * SVGA: 1280 x 1024
 * Sony PCR-100 video camera: 1152 x 864
 */
#ifndef IMAGE_WIDTH_MAX
#define IMAGE_WIDTH_MAX     GPIV_MAX_IMG_SIZE /* Maximum allowed image width */
#endif /* IMAGE_WIDTH_MAX */

#ifndef IMAGE_HEIGHT_MAX
#define IMAGE_HEIGHT_MAX    GPIV_MAX_IMG_SIZE /* Maximum allowed image height */
#endif  /* IMAGE_HEIGHT_MAX */

#ifndef IMAGE_DEPTH_MAX
#define IMAGE_DEPTH_MAX     GPIV_MAX_IMG_DEPTH /* Maximum allowed image depth */
#endif  /* IMAGE_DEPTH_MAX */


gchar IMAGE_CORRELATION_LABEL[GPIV_MAX_CHARS];
gchar IMAGE_WIDTH_LABEL[GPIV_MAX_CHARS];
gchar IMAGE_HEIGHT_LABEL[GPIV_MAX_CHARS];
gchar IMAGE_DEPTH_LABEL[GPIV_MAX_CHARS];

/*
 * Definitions for conssole
 */
#define CONSOLE_WIDTH 100
#define CONSOLE_HEIGHT 400
/*
 * Extra marge for display
 */
/* #define VIEW_HMARGE 38 */
/* #define VIEW_VMARGE 62 */
#define VIEW_HMARGE 19 /* horizontal marge in the display window */
#define VIEW_VMARGE 31 /* vertical marge in the display window */
/* #define VIEW_HMARGE 0 */
/* #define VIEW_VMARGE 0 */
#define RULER_WIDTH 17  /* width of the rulers in the display window */

#define MAX_DATA (IMAGE_WIDTH_MAX / GPIV_MIN_INTERR_SIZE)    /* Maximum number of data/estimators/Interr. Areas per row */
#define MAX_BUFS GPIV_NIMG_MAX  /* maximum number of buffers (=displays) */

/* #define MAX_SWEEP 6 */

#define CANVAS_AA               /* Use Anti Aliased canvas for viewer */
/* BUGFIX: Color presenation for AA */
#define THICKNESS 1		/* line thickness to be drawn in te canvases */
#define ARROW_LENGTH 0.4	/* length of (piv) vector arrow head */
#define ARROW_EDGE 0.6 		/* edge of (piv) vector arrow head */
#define ARROW_WIDTH 0.2 	/* width of (piv) vector arrow head */
#define ARROW_ADD 2.0		/* additional quantity for arrow head */
#define ARROW_FACT 0.2		/* magnification factor of vector_scale for arrow head */

#define BYTEVAL 255
#define BITSHIFT_RED 24         /* bitshift for color red value (used in display_all_scalar_intregs) */
#define BITSHIFT_GREEN 16       /* bitshift for color green value */
#define BITSHIFT_BLUE 8         /* bitshift for color blue value */

#define ENABLE_IMGPROC
#undef IMGPROC_SAVE_IMG
#ifdef ENABLE_IMGPROC
#define IMG_FILTERS 5           /* number of image processes or filters in create_imgproc */
#endif /* ENABLE_IMGPROC
 */
#define DISPLAY_ZOOMFACTOR_MIN 0.25    /* Minimum zoom factor for display viewer */
#define DISPLAY_ZOOMFACTOR_MAX 4.0     /* Maximum zoom factor for display viewer */
#define DISPLAY_ZOOMFACTOR_STEP 1.1    /* Increase / decrease change in zoomfactor */

#include "console_interface.h"
#include "display_interface.h"


/*
 * Image data / header will be stored to this format
 */
enum ImgFmt {
    IMG_FMT_PNG,
    IMG_FMT_HDF,
    IMG_FMT_RAW
};

enum TablabelPage {
#ifdef ENABLE_DAC
    PAGE_DAC,
#endif

    PAGE_IMGH,

#ifdef ENABLE_IMGPROC
    PAGE_IMGPROC,
#endif

    PAGE_PIVEVAL,
    PAGE_PIVVALID,
    PAGE_PIVPOST
};


enum  ShowBackground {
    SHOW_BG_DARKBLUE,
    SHOW_BG_BLACK,
    SHOW_BG_IMG1,
    SHOW_BG_IMG2,
};


enum  ShowScalar {
    SHOW_SC_NONE,
    SHOW_SC_VORTICITY,
    SHOW_SC_SSTRAIN,
    SHOW_SC_NSTRAIN
};


enum ZoomIndex {
    ZOOM_0,
    ZOOM_1,
    ZOOM_2,
    ZOOM_3,
    ZOOM_4,
    ZOOM_5,
    ZOOM_6,
    ZOOM_7
};


static float zfactor[] = {
    0.25, 
    0.5, 
    0.83, 
    1.0, 
    1.3, 
    1.6, 
    2.0,
    4.0
};


enum  VectorScale {
    VECTOR_SCALE_0 = 1,
    VECTOR_SCALE_1 = 2,
    VECTOR_SCALE_2 = 4,
    VECTOR_SCALE_3 = 8,
    VECTOR_SCALE_4 = 16,
    VECTOR_SCALE_5 = 32,
    VECTOR_SCALE_6 = 64,
    VECTOR_SCALE_7 = 128,
    VECTOR_SCALE_8 = 256
};


enum VectorColor {
    SHOW_PEAKNR,
    SHOW_SNR,
    SHOW_MAGNITUDE_GRAY,
    SHOW_MAGNITUDE
} v_color;


enum WidgetSet {
    DAC,
    DAC_TRIG,
    DAC_TIMING,
    DAC_CAM,
    IMG,
    IMGPROC,
    EVAL,
    INTREGS,
    VALID,
    POST
};


enum MouseSelect {
    NO_MS,
    AOI_MS,
    SINGLE_AREA_MS,
    SINGLE_POINT_MS,
    DRAG_MS,
    V_LINE_MS,
    H_LINE_MS,
    ENABLE_POINT_MS,
    DISABLE_POINT_MS,
    ENABLE_AREA_MS,
    DISABLE_AREA_MS,
    SPANLENGTH_MS,
    V_SPANLENGTH_MS,
    H_SPANLENGTH_MS
} m_select;


enum ClistPut {
    PREPEND,
    INSERT,
    APPEND
};


typedef struct __GpivVar Var;
/*
 * Variables of gpiv that will be stored for future sessions, but are not
 * defined as command line keys during launching gpiv:
 */
struct __GpivVar {
    enum TablabelPage tab_pos;  /* page of the notebook to be shown */
    guint number_fnames_last;
    gchar *fn_last[MAX_LIST];   /* last image name that has been loaded */
    gboolean fname_last__set;   /* flag if fname_last has been set */
    gchar *fname_last;          /* last image name that has been loaded */
    gchar *fname_last_print;    /* last filename that has been printed to */
    gboolean fname_date;        /* Extends the name with current date */
    gboolean fname_time;        /* Extends the name with current time */
    gfloat img_span_px;         /* spanned length that takes N mm in the img */
    gfloat img_length_mm;       /* length in mm to be measured in img */
    gboolean piv_disproc;       /* displaying Interrogation Areas and PIV estimator during interrogation */
    gfloat piv_disproc_zoom;    /* magnification factor for displaying correlation in PIV evaluation tabulator */
    guint piv_disproc_vlength;  /* vector length of PIV estimator in PIV evaluation tabulator */
    gfloat dl_min;              /* minimum displacement of a piv data-set */
    gfloat dl_max;              /* maximum displacement of a piv data-set */
#ifdef ENABLE_IMGPROC
    guint imgproc_count;        /* number of image processes */
#endif

    gboolean residu_stats;      /* perform residual statistics on PIV data while validating */
    gboolean residu_stats__set;
    gboolean auto_thresh;       /* perform auto thresholding while validaton PIV data on outlyers */
    gboolean auto_thresh__set;
} *gpiv_var;


/*
 * Parameters of gpiv:
 */
typedef struct __GpivPar Par;
struct __GpivPar {
/*
 * General parameters
 */
    gboolean x_corr;                            /* images for cross correlation */
    gboolean x_corr__set;                       /* images for cross correlation */
 
    enum ImgFmt img_fmt;                        /* Image data / header will be stored to this format */
    gboolean img_fmt__set;

    gboolean hdf;                               /* store data in hdf 5 format, with .h5 extension */
    gboolean hdf__set;                          /* store data in hdf 5 format, with .h5 extension */

    gboolean print_par;                         /* Prints parameters to stdout */
    gboolean print_par__set;

    gboolean verbose;                           /* Prints behaviour to stdout */
    gboolean verbose__set;

/*
 * Console related parameters
 */
    gboolean console__show_tooltips;	        /* flag to show tooltips or hints (in pop-up window) */
    gboolean console__show_tooltips__set;       /* flag to show tooltips or hints (in pop-up window) */

    gboolean console__view_tabulator__set;      /* display tabulator of process parameters in main window */
    gboolean console__view_tabulator;           /* display tabulator of process parameters in main window */

    gboolean console__view_gpivbuttons;         /* display gpiv buttons in main window */
    gboolean console__view_gpivbuttons__set;    /* display gpiv buttons in main window */

    gint console__nbins;                        /* number of bins to display histograms */
    gboolean console__nbins__set;               /* number of bins to display histograms */

    gboolean console__trigger_cam;              /* enable/disable camera and laser triggering */
    gboolean console__trigger_cam__set;         /* enable/disable camera and laser triggering */

/*
 * Display viewer related parameters
 */
    gboolean display__stretch_auto;             /* stretch automatic window after zooming in */
    gboolean display__stretch_auto__set;        /* stretch automatic window after sooming in has been defined */

    enum ZoomIndex display__zoom_index;         /* index for displayed zooming */
    gboolean display__zoom_index__set;          /* index for displayed zooming has been defined */

    enum  VectorScale display__vector_scale;    /* scale of vectors to be displayed */
    gboolean display__vector_scale__set;        /* scale of vectors to be displayed has been defined */

    enum VectorColor display__vector_color;     /* color representation of vectors */
    gboolean display__vector_color__set;        /* color representation of vectors has been defined */

    gboolean display__stretch_window;           /* stretch display window for image area */
    gboolean display__stretch_window__set;      /* stretch display window for image area has been defined */

    gboolean display__view_menubar;             /* view menubar of the buffer display */
    gboolean display__view_menubar__set;        /* view menubar of the buffer display has been defined */

    gboolean display__view_rulers;              /* view rulers of the buffer display */
    gboolean display__view_rulers__set;         /* view rulers of the buffer display has been defined */

    gboolean display__stretch__set;             /* stretch buffer display when zooming in */
    gboolean display__stretch;                  /* stretch buffer display when zooming in has been defined */

    enum  ShowBackground display__backgrnd;     /* display background color */
    gboolean display__backgrnd__set;            /* display background color has been defined */

    gboolean display__intregs;                  /* display interrogation regions in display */
    gboolean display__intregs__set;             /* display interrogation regions in display has been defined */

    gboolean display__piv;                      /* display piv vectors in display */
    gboolean display__piv__set;                 /* display piv vectors in display has been defined */

    enum ShowScalar display__scalar;            /* displaying of scalar data */
    gboolean display__scalar__set;              /* displaying of scalar data has been defined */
 
    gboolean display__process;	                /* display interrogation areas and covariance function during image analyzing process  */
    gboolean display__process__set;             /* display interrogation areas and covariance function during image analyzing process  */
    
/*
 * Chain processing related parameters
 */
    gboolean process__cam;                      /* used by gpiv toolbar to run camera */
    gboolean process__cam__set;                 /* used by gpiv toolbar to run camera */

    gboolean process__trig;                     /* used by gpiv toolbar to run trigger */
    gboolean process__trig__set;                /* used by gpiv toolbar to run trigger */

    gboolean process__imgproc;                  /* used by gpiv toolbar to run image processing */
    gboolean process__imgproc__set;             /* used by gpiv toolbar to run image processing */

    gboolean process__piv;                      /* used by gpiv toolbar to run piv */
    gboolean process__piv__set;                 /* used by gpiv toolbar to run piv */

    gboolean process__gradient;                 /* used by gpiv toolbar to run gradient */
    gboolean process__gradient__set;            /* used by gpiv toolbar to run gradient */

    gboolean process__resstats;                 /* used by gpiv toolbar to run resstats */
    gboolean process__resstats__set;            /* used by gpiv toolbar to run resstats */

    gboolean process__errvec;                   /* used by gpiv toolbar to run errvec */
    gboolean process__errvec__set;              /* used by gpiv toolbar to run errvec */

    gboolean process__peaklock;                 /* used by gpiv toolbar to run peaklock */
    gboolean process__peaklock__set;            /* used by gpiv toolbar to run peaklock */

    gboolean process__average;                  /* used by gpiv toolbar to run average */
    gboolean process__average__set;             /* used by gpiv toolbar to run average */

    gboolean process__scale;                    /* used by gpiv toolbar to run scale */
    gboolean process__scale__set;               /* used by gpiv toolbar to run scale */

    gboolean process__subtract;                 /* used by gpiv toolbar to run subtract */
    gboolean process__subtract__set;            /* used by gpiv toolbar to run subtract */

    gboolean process__vorstra;                  /* used by gpiv toolbar to run vorstra */
    gboolean process__vorstra__set;             /* used by gpiv toolbar to run vorstra */
    
#ifdef ENABLE_MPI
    gint mpi_nodes;                             /* number of cluster nodes used for parallel MPI execution */
    gboolean mpi_nodes__set;                    /* number of cluster nodes used for parallel MPI execution */
#endif /* ENBALE MPI */

}  gp, *default_par, *gpiv_par;


/*
 * Global parameters and variables for each process:
 * ImagePar should be unique for each image
 */
#ifdef ENABLE_CAM
GpivCamPar *gl_cam_par;         /* global camera parameters */
GpivCamVar *cam_var;
#endif /* ENABLE_CAM */

#ifdef ENABLE_TRIG
GpivTrigPar *gl_trig_par;               /* global trigger parameters */
#endif /* ENABLE_TRIG */

#ifdef ENABLE_IMGPROC
GpivImageProcPar *gl_imgproc_par;         /* global image processing parameters */
#endif /* ENABLE_IMGPROC */

GpivImagePar *gl_image_par;             /* global image parameters */
GpivPivPar *gl_piv_par;                 /* global PIV evaluation / interrogation parameters */
GpivValidPar *gl_valid_par;             /* global PIV validation parameters */
GpivPostPar *gl_post_par;               /* global PIV post processing parameters */


/*
 * Other global variables
 */
GnomeProgram *Gpiv_app;
GnomeCanvasItem *gci_aoi, *gci_line;

gchar fname[GPIV_MAX_CHARS];
gboolean exec_process, cancel_process;

Display *display[MAX_BUFS], *display_act;
guint nbufs;

guint camera_act;
gchar *msg_default, msg_display[40];
gchar c_line[GPIV_MAX_LINES_C][GPIV_MAX_CHARS];
guint var_scale, display_intregs;
guint m_select_index_y, m_select_index_x;

/* flag to suppres creating intregs during load_buffer; after displaying 
 * results into correct effect */
gboolean view_toggle_intregs_flag;


#endif /* GPIV_H */
