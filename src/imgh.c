/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

This file is part of gpiv.

Gpiv is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Image header window/tabulator callbacks
 * $Log: imgh.c,v $
 * Revision 1.12  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.11  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.10  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.9  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.7  2005/02/26 09:43:30  gerber
 * parameter flags (parameter_logic) defined as gboolean
 *
 * Revision 1.6  2005/01/19 15:53:41  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.5  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.4  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.3  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.2  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */


#include "gpiv_gui.h"
#include "imgh.h"

void
on_radiobutton_imgh_mouse_1_enter (GtkWidget *widget, 
                                   GtkWidget *entry
                                   )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("No action");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_radiobutton_imgh_mouse_2_enter (GtkWidget *widget, 
                                   GtkWidget *entry
                                   )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Points the length spanned by a ruler in the image");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_radiobutton_imgh_mouse_3_enter (GtkWidget *widget, 
                                   GtkWidget *entry
                                   )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Points the vertical length by a ruler in the image");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_radiobutton_imgh_mouse_4_enter (GtkWidget *widget, 
                                   GtkWidget *entry
                                   )
/*-----------------------------------------------------------------------------
 * Main program of gpiv
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Points the horizontal length by a ruler in the image");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_radiobutton_imgh_mouse (GtkWidget *widget, 
                           GtkWidget *entry
                           )
/*-----------------------------------------------------------------------------
 */
{
    m_select = atoi (gtk_object_get_data (GTK_OBJECT (widget),
                                          "mouse_select"));

    if (m_select == NO_MS) {
	gl_piv_par->int_geo = GPIV_AOI;
    } else if (m_select == SPANLENGTH_MS) {
    } else if (m_select == V_SPANLENGTH_MS) {
    } else if (m_select == H_SPANLENGTH_MS) {
    } else {
        g_warning ("on_radiobutton_imgh_mouse: should not arrive here");
    }

}



void
on_spinbutton_post_scale_px (GtkSpinButton *widget, 
                             GtkWidget *entry
                             )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");

    gpiv_var->img_span_px = gtk_spin_button_get_value_as_float (widget);
    gnome_config_push_prefix ("/gpiv/RuntimeVariables/");
    gnome_config_set_float ("span", gpiv_var->img_span_px);
    gnome_config_pop_prefix ();
    gnome_config_sync ();

    gl_image_par->s_scale = 
        gpiv_var->img_length_mm / gpiv_var->img_span_px;
    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (gpiv->imgh->spinbutton_sscale), 
                               gl_image_par->s_scale);

        
    if (display_act != NULL) {
        display_act->img->image->header->s_scale = gl_image_par->s_scale;
        display_act->img->image->header->s_scale__set = TRUE;
        display_act->img->saved_img = FALSE;
    }
}



void
on_spinbutton_post_scale_mm (GtkSpinButton *widget, 
                             GtkWidget *entry
                             )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");

    gpiv_var->img_length_mm = gtk_spin_button_get_value_as_float (widget);
    gnome_config_set_float ("length", gpiv_var->img_length_mm);
    gnome_config_sync ();

    gl_image_par->s_scale = 
        gpiv_var->img_length_mm / gpiv_var->img_span_px;
    gtk_spin_button_set_value (GTK_SPIN_BUTTON
                               (gpiv->imgh->spinbutton_sscale), 
                               gl_image_par->s_scale);
        
    if (display_act != NULL) {
        display_act->img->image->header->s_scale = gl_image_par->s_scale;
        display_act->img->image->header->s_scale__set = TRUE;
        display_act->img->saved_img = FALSE;
    }
}


void
on_entry_imgh_title (GtkSpinButton *widget, 
                     GtkWidget *entry
                     )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->title, GPIV_MAX_CHARS, "%s", 
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_imgtitle)));
    gl_image_par->title__set = TRUE;
        
    if  (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->title__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->title, GPIV_MAX_CHARS,
                        "%s", gl_image_par->title);
            display_act->img->image->header->title__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_crdate (GtkSpinButton *widget, 
                      GtkWidget *entry
                      )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");

    g_snprintf (gl_image_par->creation_date, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_crdate)));
    gl_image_par->creation_date__set = TRUE;
        
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->creation_date__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->creation_date, 
                        GPIV_MAX_CHARS, "%s", gl_image_par->creation_date);
            display_act->img->image->header->creation_date__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_location (GtkSpinButton *widget, 
                        GtkWidget *entry
                        )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");

    g_snprintf (gl_image_par->location, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_location)));
    gl_image_par->location__set = TRUE;
        
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->location__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->location, 
                        GPIV_MAX_CHARS,"%s", gl_image_par->location);
            display_act->img->image->header->location__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_author (GtkSpinButton *widget, 
		      GtkWidget *entry
                      )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");

    g_snprintf (gl_image_par->author, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_author)));
    gl_image_par->author__set = TRUE;

    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->author__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->author, GPIV_MAX_CHARS, 
                        "%s", gl_image_par->author);
            display_act->img->image->header->author__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_software (GtkSpinButton *widget, 
                        GtkWidget *entry
                        )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->software, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_software)));
    gl_image_par->software__set = TRUE;
        
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->software__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->software, GPIV_MAX_CHARS, 
                        "%s", gl_image_par->software);
            display_act->img->image->header->software__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_source (GtkSpinButton *widget, 
		      GtkWidget *entry
                      )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");

    g_snprintf (gl_image_par->source, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_source)));
    gl_image_par->source__set = TRUE;
        
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->source__set = FALSE;
        } else {
            g_snprintf(display_act->img->image->header->source, GPIV_MAX_CHARS, 
                       "%s", gl_image_par->source);
            display_act->img->image->header->source__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_usertext (GtkSpinButton *widget, 
                        GtkWidget *entry
                        )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->usertext, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_usertext)));
    gl_image_par->usertext__set = TRUE;
        
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->usertext__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->usertext, 
                        GPIV_MAX_CHARS, "%s", gl_image_par->usertext);
            display_act->img->image->header->usertext__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_warning (GtkSpinButton *widget, 
                       GtkWidget *entry
                       )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->warning, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_warning)));
    gl_image_par->warning__set = TRUE;
        
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->warning__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->warning, 
                        GPIV_MAX_CHARS, "%s", gl_image_par->warning);
            display_act->img->image->header->warning__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_disclaimer (GtkSpinButton *widget, 
                          GtkWidget *entry
                          )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->disclaimer, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_disclaimer)));
    gl_image_par->disclaimer__set = TRUE;
    
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->disclaimer__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->disclaimer, 
                        GPIV_MAX_CHARS, "%s", gl_image_par->disclaimer);
            display_act->img->image->header->disclaimer__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_comment (GtkSpinButton *widget, 
                       GtkWidget *entry
                       )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->comment, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_comment)));
    gl_image_par->comment__set = TRUE;
    
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->comment__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->comment, 
                        GPIV_MAX_CHARS, "%s", gl_image_par->comment);
            display_act->img->image->header->comment__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_copyright (GtkSpinButton *widget, 
                         GtkWidget *entry
                         )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->copyright, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_copyright)));
    gl_image_par->copyright__set = TRUE;
    
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->copyright__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->copyright, 
                        GPIV_MAX_CHARS, "%s", gl_image_par->copyright);
            display_act->img->image->header->copyright__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_email (GtkSpinButton *widget, 
                     GtkWidget *entry
                     )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->email, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_email)));
    gl_image_par->email__set = TRUE;
    
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->email__set = FALSE;
        } else {
            g_snprintf(display_act->img->image->header->email, 
                       GPIV_MAX_CHARS, "%s", gl_image_par->email);
            display_act->img->image->header->email__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


void
on_entry_imgh_url (GtkSpinButton *widget, 
                   GtkWidget *entry
                   )
/*-----------------------------------------------------------------------------
 */
{
    Imgheader *imgh = gtk_object_get_data (GTK_OBJECT (widget), "imgh");
    
    g_snprintf (gl_image_par->url, GPIV_MAX_CHARS, "%s",
                gtk_entry_get_text (GTK_ENTRY (imgh->entry_url)));
    gl_image_par->url__set = TRUE;
    
    if (display_act != NULL) {
        gint length = GTK_ENTRY (entry)->text_length;
        if (length == 0) {
            display_act->img->image->header->url__set = FALSE;
        } else {
            g_snprintf (display_act->img->image->header->url, 
                        GPIV_MAX_CHARS, "%s", gl_image_par->url);
            display_act->img->image->header->url__set = TRUE;
            display_act->img->saved_img = FALSE;
        }
    }
}


