/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (callback) functions for image header
 * $Log: imgh.h,v $
 * Revision 1.6  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.5  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.4  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.3  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.2  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef IMGH_H
#define IMGH_H

void
on_radiobutton_imgh_mouse_1_enter (GtkWidget *widget, 
                                   GtkWidget *entry);

void
on_radiobutton_imgh_mouse_2_enter (GtkWidget *widget, 
                                   GtkWidget *entry);

void
on_radiobutton_imgh_mouse_3_enter (GtkWidget *widget, 
                                   GtkWidget *entry);

void
on_radiobutton_imgh_mouse_4_enter (GtkWidget *widget, 
                                   GtkWidget *entry);

void
on_radiobutton_imgh_mouse (GtkWidget *widget, 
                           GtkWidget *entry);

void
on_spinbutton_post_scale_px (GtkSpinButton *widget, 
                             GtkWidget *entry);

void
on_spinbutton_post_scale_mm (GtkSpinButton *widget, 
                             GtkWidget *entry);

void
on_entry_imgh_title (GtkSpinButton *widget, 
                     GtkWidget *entry);

void
on_entry_imgh_crdate (GtkSpinButton *widget, 
		      GtkWidget *entry);

void
on_entry_imgh_location (GtkSpinButton *widget, 
                        GtkWidget *entry);

void
on_entry_imgh_author (GtkSpinButton *widget, 
		      GtkWidget *entry);

void
on_entry_imgh_software (GtkSpinButton *widget, 
                        GtkWidget *entry);

void
on_entry_imgh_source (GtkSpinButton *widget, 
		      GtkWidget *entry);

void
on_entry_imgh_usertext (GtkSpinButton *widget, 
                        GtkWidget *entry);

void
on_entry_imgh_warning (GtkSpinButton *widget, 
                       GtkWidget *entry);

void
on_entry_imgh_disclaimer (GtkSpinButton *widget, 
                          GtkWidget *entry);

void
on_entry_imgh_comment (GtkSpinButton *widget, 
                       GtkWidget *entry);

void
on_entry_imgh_copyright (GtkSpinButton *widget, 
                         GtkWidget *entry);

void
on_entry_imgh_email (GtkSpinButton *widget, 
                     GtkWidget *entry);

void
on_entry_imgh_url (GtkSpinButton *widget, 
                   GtkWidget *entry);

#endif  /* IMGH_H */
