/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Image header interface
 * $Log: imgh_interface.c,v $
 * Revision 1.13  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.12  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.11  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.10  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.9  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.8  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.6  2005/02/26 09:17:13  gerber
 * structured of interrogate function by using gpiv_piv_isiadapt
 *
 * Revision 1.5  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.4  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.3  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.2  2003/07/25 15:40:23  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gpiv_gui.h"
#include "imgh_interface.h"
#include "utils.h"
#include "console.h"
#include "imgh.h"
#include "pivpost.h"



Imgheader *
create_imgh (GnomeApp *main_window, 
	     GtkWidget *container
             )
/*-----------------------------------------------------------------------------
 * Image Info window with data from header
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (main_window), "gpiv");
    Imgheader *imgh = g_new0 (Imgheader, 1);


    imgh->vbox_label = gtk_vbox_new (FALSE,
				    0);
    gtk_widget_ref (imgh->vbox_label);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_vbox_label",
			     imgh->vbox_label,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->vbox_label);
    gtk_container_add (GTK_CONTAINER (container),
		      imgh->vbox_label);



    imgh->label_title = gtk_label_new(_("Image information and settings"));
    gtk_widget_ref(imgh->label_title);
    gtk_object_set_data_full(GTK_OBJECT(main_window),
			     "imgh->label_title",
			     imgh->label_title,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_title);
    gtk_box_pack_start (GTK_BOX (imgh->vbox_label),
		       imgh->label_title,
		       FALSE,
		       FALSE,
		       0);



/*
 * Scrolled window
 */
    imgh->vbox_scroll = gtk_vbox_new (FALSE,
				     0);
    gtk_widget_ref (imgh->vbox_scroll);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_vbox_scroll",
			     imgh->vbox_scroll,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->vbox_scroll);
    gtk_box_pack_start (GTK_BOX (imgh->vbox_label),
		       imgh->vbox_scroll,
		       TRUE,
		       TRUE,
		       0);



    imgh->scrolledwindow = gtk_scrolled_window_new (NULL,
						   NULL);
    gtk_widget_ref (imgh->scrolledwindow);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_scrolledwindow",
			     imgh->scrolledwindow,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->scrolledwindow);
    gtk_box_pack_start (GTK_BOX (imgh->vbox_scroll),
		       imgh->scrolledwindow,
		       TRUE,
		       TRUE,
		       0);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				   (imgh->scrolledwindow),
				   GTK_POLICY_NEVER,
				   GTK_POLICY_AUTOMATIC);



    imgh->viewport = gtk_viewport_new (NULL,
                                       NULL);
    gtk_widget_ref (imgh->viewport);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_viewport",
			     imgh->viewport,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->viewport);
    gtk_container_add (GTK_CONTAINER (imgh->scrolledwindow),
		      imgh->viewport);

/*
 * main table for image header table/window
 */
    imgh->vbox1 = gtk_vbox_new (FALSE,
			       0);
    gtk_widget_ref (imgh->vbox1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh->vbox1",
			     imgh->vbox1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->vbox1);
    gtk_container_add (GTK_CONTAINER (imgh->viewport),
		      imgh->vbox1);


/*
 * buffer number
*/
    imgh->hbox_bufno = gtk_hbox_new (FALSE,
				    0);
    gtk_widget_ref (imgh->hbox_bufno);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_hbox_bufno",
			     imgh->hbox_bufno,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->hbox_bufno);
    gtk_box_pack_start (GTK_BOX (imgh->vbox1),
		       imgh->hbox_bufno, 
		       TRUE,
		       TRUE,
		       0);



    imgh->label_label_bufno = gtk_label_new ( _("buffer #: "));
    gtk_widget_ref (imgh->label_label_bufno);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_label_bufno",
			     imgh->label_label_bufno,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_label_bufno);
    gtk_box_pack_start (GTK_BOX (imgh->hbox_bufno),
		       imgh->label_label_bufno, 
		       FALSE,
		       FALSE,
		       0);



    imgh->label_bufno = gtk_label_new ("");
    gtk_widget_ref (imgh->label_bufno);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imh_label_bufno",
			     imgh->label_bufno,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_bufno);
    gtk_box_pack_start (GTK_BOX (imgh->hbox_bufno),
		       imgh->label_bufno,
		       FALSE,
		       FALSE,
		       0);



/*
 * buffer name
*/
    imgh->hbox_name = gtk_hbox_new (FALSE,
				   0);
    gtk_widget_ref (imgh->hbox_name);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh->hbox_name",
			     imgh->hbox_name,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->hbox_name);
    gtk_box_pack_start (GTK_BOX (imgh->vbox1),
		       imgh->hbox_name,
		       TRUE,
		       TRUE,
		       0);



    imgh->label_label_name = gtk_label_new ( _("file: "));
    gtk_widget_ref (imgh->label_label_name);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_label_name",
			     imgh->label_label_name,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_label_name);
    gtk_box_pack_start (GTK_BOX (imgh->hbox_name),
		       imgh->label_label_name,
		       FALSE,
		       FALSE,
		       0);



    imgh->label_name = gtk_label_new ("");
    gtk_widget_ref (imgh->label_name);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_name",
			     imgh->label_name,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_name);
    gtk_box_pack_start (GTK_BOX (imgh->hbox_name),
		       imgh->label_name,
		       FALSE,
		       FALSE,
		       0);



    imgh->table5 = gtk_table_new (9,
                                  2,
                                  FALSE);
    gtk_widget_ref (imgh->table5);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh->table5", 
                             imgh->table5,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->table5);
    gtk_box_pack_start (GTK_BOX (imgh->vbox1), 
		       imgh->table5,
		       TRUE,
		       FALSE,
		       0);
/* viewport_imgh */

/*
 * label for correlation
 */
    imgh->label_label_correlation = gtk_label_new ( _("correlation type: "));
    gtk_widget_ref (imgh->label_label_correlation);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_label_correlation",
			     imgh->label_label_correlation,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_label_correlation);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_label_correlation, 
		     0, 
		     1, 
		     0, 
		     1,
		     (GtkAttachOptions) (GTK_EXPAND),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->label_correlation = gtk_label_new (IMAGE_CORRELATION_LABEL);
    gtk_widget_ref (imgh->label_correlation);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_correlation",
			     imgh->label_correlation,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_correlation);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_correlation, 
		     1, 
		     2, 
		     0, 
		     1,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);


/*
 * label for ncols
 */
    imgh->label_label_ncols = gtk_label_new ( _("number of columns (pixels): "));
    gtk_widget_ref (imgh->label_label_ncols);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_label_ncols",
			     imgh->label_label_ncols,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_label_ncols);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_label_ncols, 
		     0, 
		     1, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_EXPAND),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->label_ncols = gtk_label_new (IMAGE_WIDTH_LABEL);
    gtk_widget_ref (imgh->label_ncols);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_ncols",
			     imgh->label_ncols,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_ncols);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_ncols, 
		     1, 
		     2, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);


/*
 * label for nrows
 */
    imgh->label_label_nrows = gtk_label_new ( _("number of rows (pixels): "));
    gtk_widget_ref (imgh->label_label_nrows);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_label_nrows",
			     imgh->label_label_nrows,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_label_nrows);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_label_nrows, 
		     0, 
		     1, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->label_nrows = gtk_label_new (IMAGE_HEIGHT_LABEL);
    gtk_widget_ref (imgh->label_nrows);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_nrows",
			     imgh->label_nrows,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_nrows);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_nrows, 
		     1, 
		     2, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);




/*
 * label for depth
 */
    imgh->label_label_depth = gtk_label_new ( _("image depth (bits): "));
    gtk_widget_ref (imgh->label_label_depth);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imh_label_label_depth",
			     imgh->label_label_depth,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_label_depth);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_label_depth, 
		     0, 
		     1, 
		     3, 
		     4,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->label_depth = gtk_label_new (IMAGE_DEPTH_LABEL);
    gtk_widget_ref (imgh->label_depth);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_depth",
			     imgh->label_depth,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_depth);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_depth, 
		     1, 
		     2, 
		     3, 
		     4,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);




/*
 * frame, table and spinners for spatial scale "sscale"
 *
 * radio buttons and spinners defining spatial scale interactively 
 * with pointer in image
 */
    imgh->frame_sscale = gtk_frame_new ( _("Define spatial scale"));
    gtk_widget_ref (imgh->frame_sscale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_frame_sscale",
			     imgh->frame_sscale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->frame_sscale);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->frame_sscale, 
		     0, 
		     1, 
		     4, 
		     5,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->table_sscale = gtk_table_new (4,
                                       2,
                                       FALSE);
    gtk_widget_ref (imgh->table_sscale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_table_sscale",
			     imgh->table_sscale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->table_sscale);
    gtk_container_add (GTK_CONTAINER (imgh->frame_sscale),
		      imgh->table_sscale);




    imgh->vbox_sscale = gtk_vbox_new (FALSE,
					  0);
    gtk_widget_ref (imgh->vbox_sscale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_sscale",
			     imgh->vbox_sscale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->vbox_sscale);
    gtk_table_attach (GTK_TABLE (imgh->table_sscale),
		     imgh->vbox_sscale,
		     1,
		     2,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) 0,
		     0,
		     0);


    imgh->radiobutton_mouse_1 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("None"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (imgh->radiobutton_mouse_1));
    gtk_widget_ref (imgh->radiobutton_mouse_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_1",
			     imgh->radiobutton_mouse_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->radiobutton_mouse_1);
    gtk_box_pack_start (GTK_BOX (imgh->vbox_sscale),
		       imgh->radiobutton_mouse_1,
		       FALSE,
		       FALSE,
		       0);
 
    gtk_object_set_data (GTK_OBJECT (imgh->radiobutton_mouse_1),
			"gpiv",
			gpiv);
    gtk_object_set_data (GTK_OBJECT (imgh->radiobutton_mouse_1),
			"mouse_select",
			"0" /* NO_MS */);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_1),
		       "enter",
		       G_CALLBACK (on_radiobutton_imgh_mouse_1_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_1),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_1),
		       "toggled",
		       G_CALLBACK (on_radiobutton_imgh_mouse),
		       NULL);



    imgh->radiobutton_mouse_2 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Spanned length"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (imgh->radiobutton_mouse_2));
    gtk_widget_ref (imgh->radiobutton_mouse_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_2",
			     imgh->radiobutton_mouse_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->radiobutton_mouse_2);
    gtk_box_pack_start (GTK_BOX (imgh->vbox_sscale),
		       imgh->radiobutton_mouse_2,
		       FALSE,
		       FALSE,
		       0);
 
    gtk_object_set_data (GTK_OBJECT (imgh->radiobutton_mouse_2),
			"gpiv",
			gpiv);
    gtk_object_set_data (GTK_OBJECT (imgh->radiobutton_mouse_2),
			"mouse_select",
			"11" /* SPANLENGTH */);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_2),
		       "enter",
		       G_CALLBACK (on_radiobutton_imgh_mouse_2_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_2),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_2),
		       "toggled",
		       G_CALLBACK (on_radiobutton_imgh_mouse),
		       NULL);


    imgh->radiobutton_mouse_3 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Vertical spanned length"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (imgh->radiobutton_mouse_3));
    gtk_widget_ref (imgh->radiobutton_mouse_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_3",
			     imgh->radiobutton_mouse_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->radiobutton_mouse_3);
    gtk_box_pack_start (GTK_BOX (imgh->vbox_sscale),
		       imgh->radiobutton_mouse_3,
		       FALSE,
		       FALSE,
		       0);
 
    gtk_object_set_data (GTK_OBJECT (imgh->radiobutton_mouse_3),
			"gpiv",
			gpiv);
    gtk_object_set_data (GTK_OBJECT (imgh->radiobutton_mouse_3),
			"mouse_select",
			"12" /* V_SPANLENGTH */);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_3),
		       "enter",
		       G_CALLBACK (on_radiobutton_imgh_mouse_3_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_3),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_3),
		       "toggled",
		       G_CALLBACK (on_radiobutton_imgh_mouse),
		       NULL);



    imgh->radiobutton_mouse_4 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Horizontal spanned length"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (imgh->radiobutton_mouse_4));
    gtk_widget_ref (imgh->radiobutton_mouse_4);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_4",
			     imgh->radiobutton_mouse_4,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->radiobutton_mouse_4);
    gtk_box_pack_start (GTK_BOX (imgh->vbox_sscale),
		       imgh->radiobutton_mouse_4,
		       FALSE,
		       FALSE,
		       0);
 
    gtk_object_set_data (GTK_OBJECT (imgh->radiobutton_mouse_4),
			"gpiv",
			gpiv);
    gtk_object_set_data (GTK_OBJECT (imgh->radiobutton_mouse_4),
			"mouse_select",
			"13" /* H_SPANLENGTH */);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_4),
		       "enter",
		       G_CALLBACK (on_radiobutton_imgh_mouse_4_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_4),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgh->radiobutton_mouse_4),
		       "toggled",
		       G_CALLBACK (on_radiobutton_imgh_mouse),
		       NULL);




    imgh->label_sscale_px = gtk_label_new ( _("span (pixels): "));
    gtk_widget_ref (imgh->label_sscale_px);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_sscale_px",
			     imgh->label_sscale_px,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_sscale_px);
    gtk_table_attach (GTK_TABLE (imgh->table_sscale),
		     imgh->label_sscale_px, 
		     0, 
		     1, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);




    imgh->spinbutton_adj_sscale_px =
	gtk_adjustment_new (gpiv_var->img_span_px,
			   1, 
			   IMAGE_WIDTH_MAX, 
			   1,
			   100,
			   IMAGE_WIDTH_MAX);

    imgh->spinbutton_sscale_px =
	gtk_spin_button_new (GTK_ADJUSTMENT (imgh->spinbutton_adj_sscale_px),
			    1,
			    4);
    gtk_widget_ref (imgh->spinbutton_sscale_px);
    gtk_widget_show (imgh->spinbutton_sscale_px);
    gtk_table_attach (GTK_TABLE (imgh->table_sscale),
		     imgh->spinbutton_sscale_px, 
		     1, 
		     2, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (imgh->spinbutton_sscale_px),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (imgh->spinbutton_sscale_px),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (imgh->spinbutton_sscale_px),
			"gpiv",
			gpiv);
    g_signal_connect (GTK_OBJECT (imgh->spinbutton_sscale_px),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale_px),
		       imgh->spinbutton_sscale_px);



    imgh->label_sscale_mm = gtk_label_new ( _("length (mm): "));
    gtk_widget_ref (imgh->label_sscale_mm);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_sscale_mm",
			     imgh->label_sscale_mm,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_sscale_mm);
    gtk_table_attach (GTK_TABLE (imgh->table_sscale),
		     imgh->label_sscale_mm, 
		     0, 
		     1, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);




    imgh->spinbutton_adj_sscale_mm =
	gtk_adjustment_new (gpiv_var->img_length_mm,
			   0.0001, 
			   500.0, 
			   ADJ_STEP,
			   10.0,
			   500.0);

    imgh->spinbutton_sscale_mm =
	gtk_spin_button_new (GTK_ADJUSTMENT (imgh->spinbutton_adj_sscale_mm),
			    1,
			    4);
    gtk_widget_ref (imgh->spinbutton_sscale_mm);
    gtk_widget_show (imgh->spinbutton_sscale_mm);
    gtk_table_attach (GTK_TABLE (imgh->table_sscale),
		     imgh->spinbutton_sscale_mm, 
		     1, 
		     2, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (imgh->spinbutton_sscale_mm),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (imgh->spinbutton_sscale_mm),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (imgh->spinbutton_sscale_mm),
			"gpiv",
			gpiv);
    g_signal_connect (GTK_OBJECT (imgh->spinbutton_sscale_mm),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale_mm),
		       imgh->spinbutton_sscale_mm);



/*
 * spinner for spatial scale "sscale"
 */
    imgh->label_sscale = gtk_label_new ( _("spatial scale (mm/pixels): "));
    gtk_widget_ref (imgh->label_sscale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_sscale",
			     imgh->label_sscale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_sscale);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_sscale, 
		     0, 
		     1, 
		     5, 
		     6,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);




    imgh->spinbutton_adj_sscale =
	gtk_adjustment_new (gl_image_par->s_scale,
			   0, 
			   ADJ_MAX, 
			   ADJ_STEP,
			   ADJ_PAGE,
			   ADJ_MAX);

    imgh->spinbutton_sscale =
	gtk_spin_button_new (GTK_ADJUSTMENT (imgh->spinbutton_adj_sscale),
			    1,
			    4);
    gtk_widget_ref (imgh->spinbutton_sscale);
    gtk_widget_show (imgh->spinbutton_sscale);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->spinbutton_sscale, 
		     1, 
		     2, 
		     5, 
		     6,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (imgh->spinbutton_sscale),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (imgh->spinbutton_sscale),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (imgh->spinbutton_sscale),
			"var_type",
			"3");
    g_signal_connect (GTK_OBJECT (imgh->spinbutton_sscale),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale),
		       imgh->spinbutton_sscale);



/*
 * spinner for time scale "tscale"
 */
    imgh->label_tscale = gtk_label_new ( _("time scale (ms): "));
    gtk_widget_ref (imgh->label_tscale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_tscale",
			     imgh->label_tscale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_tscale);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_tscale, 
		     0,
		     1,
		     6,
		     7,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->spinbutton_adj_tscale =
	gtk_adjustment_new (gl_image_par->t_scale,
			   0,
			   ADJ_MAX,
			   ADJ_STEP,
			   ADJ_PAGE,
			   ADJ_MAX);



    imgh->spinbutton_tscale =
	gtk_spin_button_new (GTK_ADJUSTMENT (imgh->spinbutton_adj_tscale),
			    1,
			    4);
    gtk_widget_ref (imgh->spinbutton_tscale);
    gtk_widget_show (imgh->spinbutton_tscale);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->spinbutton_tscale, 
		     1, 
		     2, 
		     6, 
		     7,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (imgh->spinbutton_tscale),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (imgh->spinbutton_tscale),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (imgh->spinbutton_tscale),
			"var_type",
			"4");
    g_signal_connect (GTK_OBJECT (imgh->spinbutton_tscale),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale),
		       imgh->spinbutton_tscale);



    imgh->table2 = gtk_table_new (16,
				 2,
				 FALSE);
    gtk_widget_ref (imgh->table2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh->table2",
			     imgh->table2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->table2);
    gtk_box_pack_end (GTK_BOX (imgh->vbox1),
		     imgh->table2,
		     TRUE, 
		     FALSE,
		     0);

/*
 * spinner for column position
 */
    imgh->label_colpos = gtk_label_new ( _("position of column #0 (m): "));
    gtk_widget_ref (imgh->label_colpos);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_colpos",
			     imgh->label_colpos,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_colpos);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_colpos, 
		     0, 
		     1, 
		     7, 
		     8,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);




    imgh->spinbutton_adj_colpos =
	gtk_adjustment_new (gl_image_par->z_off_x,
			   ADJ_MIN,
			   ADJ_MAX,
			   ADJ_STEP,
			   ADJ_PAGE,
			   ADJ_MAX);



    imgh->spinbutton_colpos =
	gtk_spin_button_new (GTK_ADJUSTMENT (imgh->spinbutton_adj_colpos),
			    1,
			    4);
    gtk_widget_ref (imgh->spinbutton_colpos);
    gtk_widget_show (imgh->spinbutton_colpos);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->spinbutton_colpos, 
		     1, 
		     2, 
		     7, 
		     8,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (imgh->spinbutton_colpos),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (imgh->spinbutton_colpos),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (imgh->spinbutton_colpos),
			"var_type",
			"1");
    g_signal_connect (GTK_OBJECT (imgh->spinbutton_colpos),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale),
		       imgh->spinbutton_colpos);



/*
 * spinner for row position
 */
     imgh->label_rowpos = gtk_label_new ( _("position of row #0 (m): "));
    gtk_widget_ref (imgh->label_rowpos);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_rowpos",
			     imgh->label_rowpos,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_rowpos);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->label_rowpos, 
		     0, 
		     1, 
		     8, 
		     9,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->spinbutton_adj_rowpos =
	gtk_adjustment_new (gl_image_par->z_off_y,
			   ADJ_MIN,
			   ADJ_MAX,
			   ADJ_STEP,
			   ADJ_PAGE,
			   ADJ_MAX);



    imgh->spinbutton_rowpos =
	gtk_spin_button_new (GTK_ADJUSTMENT (imgh->spinbutton_adj_rowpos),
			    1,
			    4);
    gtk_widget_ref (imgh->spinbutton_rowpos);
    gtk_widget_show (imgh->spinbutton_rowpos);
    gtk_table_attach (GTK_TABLE (imgh->table5),
		     imgh->spinbutton_rowpos, 
		     1, 
		     2, 
		     8, 
		     9,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (imgh->spinbutton_rowpos),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (imgh->spinbutton_rowpos),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (imgh->spinbutton_rowpos),
			"var_type",
			"2");
    g_signal_connect (GTK_OBJECT (imgh->spinbutton_rowpos),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale),
		       imgh->spinbutton_rowpos);



/*
 * entry for project
 */
    imgh->label_imgtitle = gtk_label_new ( _("Title: "));
    gtk_widget_ref (imgh->label_imgtitle);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_imgtitle",
			     imgh->label_imgtitle,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_imgtitle);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_imgtitle, 
		     0, 
		     1, 
		     0, 
		     1,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_misc_set_padding (GTK_MISC (imgh->label_imgtitle),
			 14,
			 0);



    imgh->entry_imgtitle = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_imgtitle);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_imgtitle",
			     imgh->entry_imgtitle,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_imgtitle);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_imgtitle, 
		     1, 
		     2, 
		     0, 
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_imgtitle),
			"imgh", 
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_imgtitle),
		       "changed",
		       G_CALLBACK (on_entry_imgh_title),
		       imgh->entry_imgtitle);
    if (gl_image_par->title__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_imgtitle), 
                           gl_image_par->title);
    }

/*
 * entry for creation date
 */
    imgh->label_crdate = gtk_label_new ( _("Creation date: "));
    gtk_widget_ref (imgh->label_crdate);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_crdate",
			     imgh->label_crdate,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_crdate);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_crdate, 
		     0,
		     1, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_misc_set_alignment (GTK_MISC (imgh->label_crdate),
			   2.98023e-07,
			   0.5);
    gtk_misc_set_padding (GTK_MISC (imgh->label_crdate),
			 11,
			 0);



    imgh->entry_crdate = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_crdate);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_crdate",
			     imgh->entry_crdate,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_crdate);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_crdate, 
		     1, 
		     2, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_crdate),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_crdate),
		       "changed",
		       G_CALLBACK (on_entry_imgh_crdate),
		       imgh->entry_crdate);
    if (gl_image_par->creation_date__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_crdate), 
                           gl_image_par->creation_date);
    }

/*
 * entry for location
 */
    imgh->label_location = gtk_label_new ( _("Place: "));
    gtk_widget_ref (imgh->label_location);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_location",
			     imgh->label_location,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_location);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_location, 
		     0, 
		     1, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_location = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_location);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_location",
			     imgh->entry_location,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_location);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_location, 
		     1, 
		     2, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_location),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_location),
		       "changed",
		       G_CALLBACK (on_entry_imgh_location),
		       imgh->entry_location);
    if (gl_image_par->location__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_location), 
                           gl_image_par->location);
    }

/*
 * entry for author
 */
    imgh->label_author = gtk_label_new ( _("Author: "));
    gtk_widget_ref (imgh->label_author);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_author",
			     imgh->label_author,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_author);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_author, 
		     0, 
		     1, 
		     3, 
		     4,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_author = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_author);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_author",
			     imgh->entry_author,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_author);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_author, 
		     1, 
		     2, 
		     3, 
		     4,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_author),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_author),
		       "changed",
		       G_CALLBACK (on_entry_imgh_author),
		       imgh->entry_author);
    if (gl_image_par->author__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_author), 
                           gl_image_par->author);
    }

/*
 * entry for software
 */
    imgh->label_software = gtk_label_new ( _("Software: "));
    gtk_widget_ref (imgh->label_software);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_software",
			     imgh->label_software,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_software);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_software, 
		     0, 
		     1, 
		     4, 
		     5,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_software = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_software);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_software",
			     imgh->entry_software,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_software);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_software, 
		     1, 
		     2, 
		     4, 
		     5,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_software),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_software),
		       "changed",
		       G_CALLBACK (on_entry_imgh_software),
		       imgh->entry_software);
    if (gl_image_par->software__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_software), 
                           gl_image_par->software);
    }

/*
 * entry for source
 */
    imgh->label_source = gtk_label_new ( _("Source: "));
    gtk_widget_ref (imgh->label_source);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_source",
			     imgh->label_source,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_source);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_source, 
		     0, 
		     1, 
		     5, 
		     6,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_source = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_source);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_source",
			     imgh->entry_source,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_source);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_source, 
		     1, 
		     2, 
		     5, 
		     6,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_source),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_source),
		       "changed",
		       G_CALLBACK (on_entry_imgh_source),
		       imgh->entry_source);
    if (gl_image_par->source__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_source), 
                           gl_image_par->source);
    }

/*
 * entry for usertext
 */
    imgh->label_usertext = gtk_label_new ( _("Usertext: "));
    gtk_widget_ref (imgh->label_usertext);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_usertext",
			     imgh->label_usertext,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_usertext);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_usertext, 
		     0, 
		     1, 
		     6, 
		     7,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_usertext = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_usertext);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_usertext",
			     imgh->entry_usertext,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_usertext);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_usertext, 
		     1, 
		     2, 
		     6, 
		     7,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_usertext),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_usertext),
		       "changed",
		       G_CALLBACK (on_entry_imgh_usertext),
		       imgh->entry_usertext);
    if (gl_image_par->usertext__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_usertext), 
                           gl_image_par->usertext);
    }

/*
 * entry for warning
 */
    imgh->label_warning = gtk_label_new ( _("Warning: "));
    gtk_widget_ref (imgh->label_warning);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_warning",
			     imgh->label_warning,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_warning);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_warning, 
		     0, 
		     1, 
		     7, 
		     8,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_warning = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_warning);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_warning",
			     imgh->entry_warning,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_warning);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_warning, 
		     1, 
		     2, 
		     7, 
		     8,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_warning),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_warning),
		       "changed",
		       G_CALLBACK (on_entry_imgh_warning),
		       imgh->entry_warning);
    if (gl_image_par->warning__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_warning), 
                           gl_image_par->warning);
    }

/*
 * entry for disclaimer
 */
    imgh->label_disclaimer = gtk_label_new ( _("Disclaimer: "));
    gtk_widget_ref (imgh->label_disclaimer);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_disclaimer",
			     imgh->label_disclaimer,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_disclaimer);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_disclaimer, 
		     0, 
		     1, 
		     8, 
		     9,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_disclaimer = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_disclaimer);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_disclaimer",
			     imgh->entry_disclaimer,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_disclaimer);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_disclaimer, 
		     1, 
		     2, 
		     8, 
		     9,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_disclaimer),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_disclaimer),
		       "changed",
		       G_CALLBACK (on_entry_imgh_disclaimer),
		       imgh->entry_disclaimer);
    if (gl_image_par->disclaimer__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_disclaimer), 
                           gl_image_par->disclaimer);
    }

/*
 * entry for comment
 */
    imgh->label_comment = gtk_label_new ( _("Comment: "));
    gtk_widget_ref (imgh->label_comment);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_comment",
			     imgh->label_comment,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_comment);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_comment, 
		     0, 
		     1, 
		     9, 
		     10,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_comment = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_comment);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_comment",
			     imgh->entry_comment,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_comment);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_comment, 
		     1, 
		     2, 
		     9, 
		     10,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_comment),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_comment),
		       "changed",
		       G_CALLBACK (on_entry_imgh_comment),
		       imgh->entry_comment);
    if (gl_image_par->comment__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_comment), 
                           gl_image_par->comment);
    }

/*
 * entry for copyright
 */
    imgh->label_copyright = gtk_label_new ( _("Copyright: "));
    gtk_widget_ref (imgh->label_copyright);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_copyright",
			     imgh->label_copyright,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_copyright);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_copyright, 
		     0, 
		     1, 
		     11, 
		     12,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_copyright = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_copyright);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_copyright",
			     imgh->entry_copyright,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_copyright);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_copyright, 
		     1, 
		     2, 
		     11, 
		     12,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_copyright),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_copyright),
		       "changed",
		       G_CALLBACK (on_entry_imgh_copyright),
		       imgh->entry_copyright);
    if (gl_image_par->copyright__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_copyright), 
                           gl_image_par->copyright);
    }

/*
 * entry for email
 */
    imgh->label_email = gtk_label_new ( _("Email: "));
    gtk_widget_ref (imgh->label_email);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_email",
			     imgh->label_email,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_email);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_email, 
		     0, 
		     1, 
		     13, 
		     14,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_email = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_email);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_email",
			     imgh->entry_email,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_email);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_email, 
		     1, 
		     2, 
		     13, 
		     14,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_email),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_email),
		       "changed",
		       G_CALLBACK (on_entry_imgh_email),
		       imgh->entry_email);
    if (gl_image_par->email__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_email), 
                           gl_image_par->email);
    }

/*
 * entry for url
 */
    imgh->label_url = gtk_label_new ( _("Url: "));
    gtk_widget_ref (imgh->label_url);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_label_url",
			     imgh->label_url,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->label_url);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->label_url, 
		     0, 
		     1, 
		     15, 
		     16,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    imgh->entry_url = gtk_entry_new ();
    gtk_widget_ref (imgh->entry_url);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgh_entry_url",
			     imgh->entry_url,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgh->entry_url);
    gtk_table_attach (GTK_TABLE (imgh->table2),
		     imgh->entry_url, 
		     1, 
		     2, 
		     15, 
		     16,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

    gtk_object_set_data (GTK_OBJECT (imgh->entry_url),
			"imgh",
			imgh);
    g_signal_connect (GTK_OBJECT (imgh->entry_url),
		       "changed",
		       G_CALLBACK (on_entry_imgh_url),
		       imgh->entry_url);
    if (gl_image_par->url__set) {
        gtk_entry_set_text(GTK_ENTRY(imgh->entry_url), 
                           gl_image_par->url);
    }

    return imgh;
}
