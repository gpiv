
/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Image header tab
 * $Log: imgh_interface.h,v $
 * Revision 1.7  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.6  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.5  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.4  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.3  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.2  2003/07/25 15:40:23  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */
#ifndef GPIV_IMGH_INTERFACE_H
#define GPIV_IMGH_INTERFACE_H


typedef struct _Imgheader Imgheader;
struct _Imgheader {
  GtkWidget *vbox_label;
  GtkWidget *label_title;

  GtkWidget *vbox_scroll;
  GtkWidget *scrolledwindow;
  GtkWidget *viewport;
  GtkWidget *vbox1;

  GtkWidget *hbox_bufno;
  GtkWidget *label_label_bufno;
  GtkWidget *label_bufno;
  GtkWidget *hbox_name;
  GtkWidget *label_label_name;
  GtkWidget *label_name;
  GtkWidget *table5;
  
  GtkWidget *label_label_correlation;
  GtkWidget *label_correlation;  
  GtkWidget *label_label_ncols;
  GtkWidget *label_ncols;
  GtkWidget *label_label_nrows;
  GtkWidget *label_nrows;
  GtkWidget *label_label_depth;
  GtkWidget *label_depth;
  
  GtkWidget *label_colpos;
  GtkObject *spinbutton_adj_colpos;
  GtkWidget *spinbutton_colpos;
  
  GtkWidget *label_rowpos;
  GtkObject *spinbutton_adj_rowpos;
  GtkWidget *spinbutton_rowpos;
  
  GtkWidget *frame_sscale;
  GtkWidget *table_sscale;
  GtkWidget *label_sscale_px;
  GtkWidget *vbox_sscale;
  GtkWidget *radiobutton_mouse_1;
  GtkWidget *radiobutton_mouse_2;
  GtkWidget *radiobutton_mouse_3;
  GtkWidget *radiobutton_mouse_4;
  GtkObject *spinbutton_adj_sscale_px;
  GtkWidget *spinbutton_sscale_px;
  GtkWidget *label_sscale_mm;
  GtkObject *spinbutton_adj_sscale_mm;
  GtkWidget *spinbutton_sscale_mm;
  GtkWidget *label_sscale;
  GtkObject *spinbutton_adj_sscale;
  GtkWidget *spinbutton_sscale;

  GtkWidget *label_tscale;
  GtkObject *spinbutton_adj_tscale;
  GtkWidget *spinbutton_tscale;
  
  GtkWidget *table2;
  GtkWidget *label_imgtitle;
  GtkWidget *entry_imgtitle;
  
  GtkWidget *label_crdate;
  GtkWidget *entry_crdate;
  
  GtkWidget *label_location;
  GtkWidget *entry_location;
  
  GtkWidget *label_author;
  GtkWidget *entry_author;
  
  GtkWidget *label_software;
  GtkWidget *entry_software;
  
  GtkWidget *label_source;
  GtkWidget *entry_source;
  
  GtkWidget *label_usertext;
  GtkWidget *entry_usertext;
  
  GtkWidget *label_warning;
  GtkWidget *entry_warning;
  
  GtkWidget *label_disclaimer;
  GtkWidget *entry_disclaimer;
  
  GtkWidget *label_comment;
  GtkWidget *entry_comment;

  GtkWidget *label_copyright;
  GtkWidget *entry_copyright;

  GtkWidget *label_email;
  GtkWidget *entry_email;

  GtkWidget *label_url;
  GtkWidget *entry_url;
};

Imgheader *
create_imgh(GnomeApp *main_window, 
	    GtkWidget *container);

#endif /* GPIV_IMGH_INTERFACE_H */
