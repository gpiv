/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
libraries.

Copyright (C) 2007, 2008 Gerber van der Graaf

This file is part of gpiv.

Gpiv is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Image processing window/tabulator callbacks
 * $Log: imgproc.c,v $
 * Revision 1.1  2008-09-16 11:04:47  gerber
 * added imgproc routines
 *
 */

#include "gpiv_gui.h"
#include "imgproc.h"
/* #include "imgproc_interface.h" */

/*
 * Prototyping
 */
gchar *
exec_imgproc_smooth (void);

gchar *
exec_imgproc_hilo (void);

gchar *
exec_imgproc_clip (void);

/*
 * Callback functions
 */
void
on_button_filter_enter(GtkWidget *widget, 
                         gpointer data
                         )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");

    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), data);
}


void
on_button_filter(GtkWidget *widget, 
                 gpointer data
                 )
/*-----------------------------------------------------------------------------
  Adjust/ sets variables for a image processing button, its relatives and 
  variables of other imgproc buttons if adeqaute.

       imf_inf:                current widget which is calling the callback function
       imgproc->imf_inf[]:     (element of) array widgets belonging to imgproc
 */
{
    Imgprocess *imgproc = gtk_object_get_data (GTK_OBJECT (widget), "imgproc");
    ImgfilterInterface *imf_inf =  gtk_object_get_data (GTK_OBJECT (widget), 
                                                        "imf_inf");
    ImgfilterVar *ivar = gtk_object_get_data (GTK_OBJECT (widget), "ivar");
/*     gint filter_id = (int) data; */
    gchar label[GPIV_MAX_CHARS];
    gint i;

    if (GTK_TOGGLE_BUTTON(widget)->active == TRUE) {
        gpiv_var->imgproc_count++;
        g_snprintf (label, GPIV_MAX_CHARS,
                    "Step #: %d", gpiv_var->imgproc_count);
        ivar->label_step_filter_label = label;
        gtk_label_set_text (GTK_LABEL (imf_inf->label_step_filter), 
                            (char *) ivar->label_step_filter_label);
        ivar->count_nr = gpiv_var->imgproc_count;

    } else {
        for (i = 0; i < IMG_FILTERS; i++) {
            if (imgproc->ivar[i]->count_nr > ivar->count_nr) {
                imgproc->ivar[i]->count_nr--;
                g_snprintf(label, GPIV_MAX_CHARS, "Step #: %d", 
                           imgproc->ivar[i]->count_nr);
                imgproc->ivar[i]->label_step_filter_label = label;
                gtk_label_set_text (GTK_LABEL (imgproc->imf_inf[i]->label_step_filter), 
                                    (char *) imgproc->ivar[i]->label_step_filter_label);
            }
        }

        gpiv_var->imgproc_count--;
        ivar->count_nr = 0;
        g_snprintf(label, GPIV_MAX_CHARS, "Step #: %d", ivar->count_nr);
        ivar->label_step_filter_label = label;
        gtk_label_set_text (GTK_LABEL (imf_inf->label_step_filter), 
                            (char *) ivar->label_step_filter_label);
    }

}


void
on_spinbutton_filtervar (GtkSpinButton *widget, 
                         GtkWidget *entry
                         )
/* ----------------------------------------------------------------------------
 */
{
    ImgfilterVar *ivar = gtk_object_get_data (GTK_OBJECT (widget), "ivar");

    ivar->value = gtk_spin_button_get_value_as_int (widget);

    if (ivar->filter_id == GPIV_IMGFI_SUBACK) {
        /* gpiv_var->imgproc_smooth_window =  gtk_spin_button_get_value_as_int (widget);*/
/*         g_message ("on_spinbutton_filtervar:: GPIV_IMGFI_SUBACK val = %d",  */
/*                    ivar->value); */
    } else if (ivar->filter_id == GPIV_IMGFI_HILO) {
/*         g_message ("on_spinbutton_filtervar:: GPIV_IMGFI_HILO val = %d",  */
/*                    ivar->value); */
    } else if (ivar->filter_id == GPIV_IMGFI_SMOOTH) {
/*         g_message ("on_spinbutton_filtervar:: GPIV_IMGFI_SMOOTH val = %d",  */
/*                    ivar->value); */
    } else if (ivar->filter_id == GPIV_IMGFI_CLIP) {
/*         g_message ("on_spinbutton_filtervar:: GPIV_IMGFI_CLIP val = %d",  */
/*                    ivar->value); */
    } else if (ivar->filter_id == GPIV_IMGFI_HIGHPASS) {
/*         g_message ("on_spinbutton_filtervar:: GPIV_IMGFI_HIGHPASS val = %d",  */
/*                    ivar->value); */
    }
}


void
on_button_imgproc_enter (GtkWidget *widget, 
                         gpointer data
                         )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Processes image");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}


void
on_button_imgproc (GtkWidget *widget, 
                   gpointer data
                   )
/* ----------------------------------------------------------------------------
 */
{
/*     Imgprocess *imgproc = gtk_object_get_data (GTK_OBJECT (widget), "imgproc"); */
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");

    cancel_process = FALSE;
    exec_imgproc (gpiv);
}

/*
 * Local functions
 */
gchar *
set_imgproc_filtervar (GpivConsole *gpiv,
                       gint id,
                       gint value)
/* ----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;
    gint i;


    for (i = 0; i < IMG_FILTERS; i++) {
        if (gpiv->imgproc->ivar[i]->filter_id == id /* GPIV_IMGFI_SUBACK */ ) {
            gpiv->imgproc->ivar[i]->value = value;
            gtk_spin_button_set_value 
                (GTK_SPIN_BUTTON (gpiv->imgproc->imf_inf[i]->spinbutton_filtervar), 
                 (gdouble) value);
            if (gpiv_par->verbose) g_message ("set_imgproc_filtervar:: i = %d id = %d value = %d", 
                       i, id, value);
        }
    }


    return err_msg;
}


gchar *
exec_imgproc_subtract (ImgfilterVar *ivar)
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;

    if (gpiv_par->verbose) g_message ("exec_imgproc_subtract:: value = %d", ivar->value);
    if (ivar->value > nbufs - 1) {
        err_msg = "exec_imgproc_subtract: unvalid value: larger than number of buffers";
        warning_gpiv ("%s", err_msg);
        return err_msg;
    }

    if (ivar->value == display_act->id) {
        err_msg = "exec_imgproc_subtract: subtracting intensities from own buffer image";
        return err_msg;
    }

    if ((err_msg =
         gpiv_imgproc_subtractimg (display[ivar->value]->img->image, 
                                   display_act->img->image
                                   ))
        != NULL) {
        g_message("exec_imgproc_subtract: %s", err_msg);
        return err_msg;
    }


    return err_msg;
}



gchar *
exec_imgproc_hilo (void)
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;


/* BUGFIX: there is an error in the procedure? */
    if (gpiv_par->verbose) g_message ("exec_imgproc_hilo:: 0");
    if ((err_msg = 
         gpiv_imgproc_highlow (display_act->img->image, gl_imgproc_par))
        != NULL) gpiv_warning ("exec_imgproc_hilo: %s", err_msg);
    if (gpiv_par->verbose) g_message ("exec_imgproc_hilo:: 1");
    
    
    if (gpiv_par->verbose) g_message ("exec_imgproc_hilo:: 2/2");
    return (err_msg);        
}



gchar *
exec_imgproc_clip (void)
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;


    if ((err_msg = 
         gpiv_imgproc_clip (display_act->img->image, gl_imgproc_par))
        != NULL) gpiv_warning ("exec_imgproc_clip: %s", err_msg);


    return (err_msg);        
}



gchar *
exec_imgproc_smooth (void)
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;


     if ((err_msg = 
         gpiv_imgproc_smooth (display_act->img->image, gl_imgproc_par))
        != NULL) gpiv_warning ("exec_imgproc_smooth: %s", err_msg);


    return (err_msg);        
}



gchar *
exec_imgproc_highpass (void)
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;


    if ((err_msg = 
         gpiv_imgproc_highpass (display_act->img->image, gl_imgproc_par))
        != NULL) gpiv_warning ("exec_imgproc_highpass: %s", err_msg);


    return (err_msg);        
}



void
exec_imgproc (GpivConsole * gpiv
              /*               Imgprocess *imgproc */
              )
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;
    gint i, j;

    for (i = 1; i <= gpiv_var->imgproc_count; i++) {
        for (j = 0; j < IMG_FILTERS; j++) {
            if (gpiv->imgproc->ivar[j]->count_nr == i) {
                if (gpiv->imgproc->ivar[j]->filter_id == GPIV_IMGFI_SUBACK) {
                    if ((err_msg = exec_imgproc_subtract (gpiv->imgproc->ivar[j]))
                        != NULL) {
                        g_warning ("exec_imgproc: %s", err_msg);
                    }
                } else if (gpiv->imgproc->ivar[j]->filter_id == GPIV_IMGFI_HILO) {
                    gl_imgproc_par->window = gpiv->imgproc->ivar[j]->value;
                    exec_imgproc_hilo ();
                } else if (gpiv->imgproc->ivar[j]->filter_id == GPIV_IMGFI_CLIP) {
                    gl_imgproc_par->threshold = gpiv->imgproc->ivar[j]->value;
                    exec_imgproc_clip ();
                } else if (gpiv->imgproc->ivar[j]->filter_id == GPIV_IMGFI_SMOOTH) {
                    exec_imgproc_smooth ();
                } else if (gpiv->imgproc->ivar[j]->filter_id == GPIV_IMGFI_HIGHPASS) {
                    gl_imgproc_par->window = gpiv->imgproc->ivar[j]->value;
                    exec_imgproc_highpass ();
                }
            }

        }
    }

/* #ifdef IMGPROC_SAVE_IMG */
    if (display_act != NULL) {
        destroy_img (display_act);
        create_img (display_act);
        if (gpiv_par->verbose) g_message ("EXEC_IMGPROC:: img.saved_img = FALSE");
        display_act->img->saved_img = FALSE; 
    }
/* #endif */
}


