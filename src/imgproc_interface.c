/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2007, 2008 Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Image processing interface
 * $Log: imgproc_interface.c,v $
 * Revision 1.1  2008-09-16 10:16:08  gerber
 * added imgproc routines
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gpiv_gui.h"
#include "console.h"
#include "imgproc_interface.h"
#include "imgproc.h"
#include "utils.h"

#ifdef ENABLE_IMGPROC


static ImgfilterVar *
create_filter_var (gint filter_id,
                   gchar *button_label,
                   gchar *label_filtervar_label,
                   gdouble value,
                   gdouble upper,
                   gchar *label_step_filter_label,
                   gint step,
                   gchar *appbar_msg
                   );

static ImgfilterInterface *
create_filter_interface (GnomeApp *main_window,
                         ImgfilterVar *ivar
                         );


Imgprocess *
create_imgproc(GnomeApp *main_window, 
	       GtkWidget *container)
/*-----------------------------------------------------------------------------
 * Image processing window
 */
{
    GpivConsole * gpiv = gtk_object_get_data (GTK_OBJECT (main_window), "gpiv");
    Imgprocess * imgproc = g_new0 (Imgprocess, 1);

    gint i;
    gchar *bufno;


    gpiv_var->imgproc_count = 0;

    imgproc->vbox_label = gtk_vbox_new (FALSE,
				    0);
    gtk_widget_ref (imgproc->vbox_label);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgproc__vbox_label",
			     imgproc->vbox_label,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgproc->vbox_label);
    gtk_container_add (GTK_CONTAINER (container),
		      imgproc->vbox_label);



    imgproc->label_title = gtk_label_new(_("Image processing"));
    gtk_widget_ref(imgproc->label_title);
    gtk_object_set_data_full(GTK_OBJECT(main_window),
			     "imgproc__label_title",
			     imgproc->label_title,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgproc->label_title);
    gtk_box_pack_start (GTK_BOX (imgproc->vbox_label),
		       imgproc->label_title,
		       FALSE,
		       FALSE,
		       0);

/*
 * Scrolled window
 */
    imgproc->vbox_scroll = gtk_vbox_new (FALSE,
				     0);
    gtk_widget_ref (imgproc->vbox_scroll);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgproc__vbox_scroll",
			     imgproc->vbox_scroll,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgproc->vbox_scroll);
    gtk_box_pack_start (GTK_BOX (imgproc->vbox_label),
		       imgproc->vbox_scroll,
		       TRUE,
		       TRUE,
		       0);



    imgproc->scrolledwindow = gtk_scrolled_window_new (NULL,
						   NULL);
    gtk_widget_ref (imgproc->scrolledwindow);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgproc__scrolledwindow",
			     imgproc->scrolledwindow,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgproc->scrolledwindow);
    gtk_box_pack_start (GTK_BOX (imgproc->vbox_scroll),
		       imgproc->scrolledwindow,
		       TRUE,
		       TRUE,
		       0);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				   (imgproc->scrolledwindow),
				   GTK_POLICY_NEVER,
				   GTK_POLICY_AUTOMATIC);



    imgproc->viewport = gtk_viewport_new (NULL,
                                       NULL);
    gtk_widget_ref (imgproc->viewport);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgproc__viewport",
			     imgproc->viewport,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgproc->viewport);
    gtk_container_add (GTK_CONTAINER (imgproc->scrolledwindow),
		      imgproc->viewport);

/*
 * main vbox and table for image processing table/window
 */
    imgproc->vbox = gtk_vbox_new (FALSE,
			       0);
    gtk_widget_ref (imgproc->vbox);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgproc__vbox",
			     imgproc->vbox,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgproc->vbox);
    gtk_container_add (GTK_CONTAINER (imgproc->viewport),
		      imgproc->vbox);



    imgproc->table = gtk_table_new (6,
                                    6,
                                    FALSE);
    gtk_widget_ref (imgproc->table);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "imgproc__table", 
                             imgproc->table,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgproc->table);
    gtk_box_pack_start (GTK_BOX (imgproc->vbox), 
		       imgproc->table,
		       TRUE,
		       FALSE,
		       0);
/*
 * Create filter buttons and attributes
 */
/*     g_message ("create imgproc tab of zo:: window = %d threshold = %d", */
/*                gl_imgproc_par->window, gl_imgproc_par->threshold); */
    imgproc->ivar[0] = 
        create_filter_var (GPIV_IMGFI_SUBACK,
                           N_("Subtract background"),
                           N_("Buffer #:"),
                           -1,
                           MAX_BUFS,
                           _("Step #: 0"),
                           0,
                           N_("Subtracts background from current image")
                           );
   
    imgproc->ivar[1] = 
        create_filter_var (GPIV_IMGFI_HILO,
                           N_("High/low filter"),
                           N_("Window:"),
                           gl_imgproc_par->window,
                           GPIV_IMGPROCPAR_MAX__WINDOW,
                           _("Step #: 0"),
                           0,
                           N_("High/low filtering: expands local contrast")
                           );

    imgproc->ivar[2] =  
        create_filter_var (GPIV_IMGFI_CLIP,
                           N_("Clip"),
                           N_("Threshold:"),
                           gl_imgproc_par->threshold,
                           GPIV_IMGPROCPAR_MAX__THRESHOLD,
/*                            img_top = (img_top << gl_image_par->depth) - 1, */
                           _("Step #: 0"),
                           0,
                           N_("Set image intensity below threshold to zero")
                           );
/*     g_message ("create_imgproc or so:: depth = %d top = %d", gl_image_par->depth, img_top); */

    imgproc->ivar[3] = 
        create_filter_var (GPIV_IMGFI_SMOOTH,
                           N_("Smooth"),
                           N_("Window:"),
                           gl_imgproc_par->window,
                           GPIV_IMGPROCPAR_MAX__WINDOW,
                           _("Step #: 0"),
                           0,
                           N_("Averages over window")
                           );

    imgproc->ivar[4] = 
        create_filter_var (GPIV_IMGFI_HIGHPASS,
                           N_("High-pass filter"),
                           N_("Window:"),
                           gl_imgproc_par->threshold,
                           GPIV_IMGPROCPAR_MAX__THRESHOLD,
                           _("Step #: 0"),
                           0,
                           N_("High-pass filter")
                           );


    for (i = 0; i < IMG_FILTERS; i++) {
/*         g_message(":: val[%d] = %f upper[%d] = %f",  */
/*                   i, imgproc->ivar[i]->value, */
/*                   i, imgproc->ivar[i]->upper); */
        imgproc->imf_inf[i] = 
            create_filter_interface (main_window,
                                     imgproc->ivar[i]
                                     );


        gtk_object_set_data(GTK_OBJECT(imgproc->imf_inf[i]->button_filter),
                            "imgproc",
                            imgproc);
        gtk_table_attach (GTK_TABLE (imgproc->table),
                          imgproc->imf_inf[i]->button_filter, 
                          0, 
                          1, 
                          i, 
                          i+1,
                          (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                          (GtkAttachOptions) (0),
                          0,
                          0);


        gtk_table_attach (GTK_TABLE (imgproc->table),
                          imgproc->imf_inf[i]->label_filtervar, 
                          1, 
                          2, 
                          i, 
                          i+1,
                          (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                          (GtkAttachOptions) (0),
                          0,
                          0);


        gtk_table_attach (GTK_TABLE (imgproc->table),
                          imgproc->imf_inf[i]->spinbutton_filtervar, 
                          2, 
                          3, 
                          i, 
                          i+1,
                          (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                          (GtkAttachOptions) (0),
                          0,
                          0);


        gtk_table_attach (GTK_TABLE (imgproc->table),
                          imgproc->imf_inf[i]->label_step_filter, 
                          3, 
                          4, 
                          i, 
                          i+1,
                          (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                          (GtkAttachOptions) (0),
                          0,
                          0);
    }

/*
 * Button to execute image processing
 */
    imgproc->button = gtk_button_new_with_label ( _("process image"));
    gtk_widget_ref (imgproc->button);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "button",
			     imgproc->button,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imgproc->button);
    gtk_box_pack_start (GTK_BOX (imgproc->vbox), 
		       imgproc->button,
		       TRUE,
		       FALSE,
		       0);

    gtk_tooltips_set_tip(gpiv->tooltips,
			 imgproc->button,
			  _("Executes the image process sequence in the order of the step numbers"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (imgproc->button),
			"imgproc",
			imgproc);
    g_signal_connect (GTK_OBJECT (imgproc->button),
		       "enter",
		       G_CALLBACK (on_button_imgproc_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgproc->button),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (imgproc->button),
		       "clicked",
		       G_CALLBACK (on_button_imgproc),
		       NULL);

/*
 * End of create_imgproc
 */
    return imgproc;
}



static ImgfilterVar *
create_filter_var (gint filter_id,
                   gchar *button_label,
                   gchar *label_filtervar_label,
                   gdouble value,
                   gdouble upper,
                   gchar *label_step_filter_label,
                   gint count_nr,
                   gchar *appbar_msg
                   )
/*-----------------------------------------------------------------------------
 */
{
    ImgfilterVar *ivar = g_new0 (ImgfilterVar, 1);

    ivar->filter_id = filter_id;
    ivar->button_label = button_label;
    ivar->label_filtervar_label = label_filtervar_label;
    ivar->value = value;
    ivar->upper = upper;
    ivar->label_step_filter_label = label_step_filter_label;
    ivar->count_nr = count_nr;
    ivar->appbar_msg = appbar_msg;

    return ivar;
}



static ImgfilterInterface *
create_filter_interface (GnomeApp *main_window,
                         ImgfilterVar *ivar
                         )
/*-----------------------------------------------------------------------------
 */
{
    ImgfilterInterface *imf_inf = g_new0 (ImgfilterInterface, 1);


    imf_inf->button_filter = 
        gtk_check_button_new_with_label(ivar->button_label);
    gtk_widget_ref(imf_inf->button_filter);
    gtk_object_set_data_full(GTK_OBJECT(main_window),
                             "imf_inf__button_filter",
                             imf_inf->button_filter,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (imf_inf->button_filter),
			"ivar",
			ivar);
    gtk_object_set_data (GTK_OBJECT (imf_inf->button_filter),
			"imf_inf",
			imf_inf);
    gtk_widget_show(imf_inf->button_filter);


    g_signal_connect(GTK_OBJECT(imf_inf->button_filter), 
                     "enter",
                     G_CALLBACK (on_button_filter_enter),
                     (gchar *) ivar->appbar_msg);
    g_signal_connect(GTK_OBJECT(imf_inf->button_filter), 
                     "leave",
                     G_CALLBACK (on_widget_leave),
                     NULL);
    g_signal_connect(GTK_OBJECT(imf_inf->button_filter), 
                     "clicked",
                     G_CALLBACK (on_button_filter),
                     (int *) ivar->filter_id);
    /*     gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON */
    /*                                 imf_inf->button_filter), */
    /*                                 gpiv_par->process_subtract); */



    imf_inf->label_filtervar = gtk_label_new(ivar->label_filtervar_label);
    gtk_widget_ref(imf_inf->label_filtervar);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
                             "imf_inf__label_filtervar",
                             imf_inf->label_filtervar,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(imf_inf->label_filtervar);
    gtk_misc_set_padding(GTK_MISC(imf_inf->label_filtervar),
                         2,
                         0);



    imf_inf->spinbutton_adj_filtervar =
        gtk_adjustment_new ((gdouble) ivar->value, 
                            0, 
                            (gdouble) ivar->upper, 
                            1,
                            100, 
                            10);

    imf_inf->spinbutton_filtervar =
        gtk_spin_button_new (GTK_ADJUSTMENT (imf_inf->spinbutton_adj_filtervar), 
                             1,
                             0);
    gtk_widget_ref (imf_inf->spinbutton_filtervar);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
                              "imf_inf__spinbutton_filtervar",
                              imf_inf->spinbutton_filtervar,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (imf_inf->spinbutton_filtervar);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (imf_inf->spinbutton_filtervar),
                                 TRUE);
    gtk_object_set_data (GTK_OBJECT (imf_inf->spinbutton_filtervar),
			"ivar",
			ivar);
    g_signal_connect (GTK_OBJECT (imf_inf->spinbutton_filtervar), 
                      "changed",
                      G_CALLBACK (on_spinbutton_filtervar), 
                      imf_inf->spinbutton_filtervar);
    
    /*
     * Step number or pass
     */
    imf_inf->label_step_filter = 
        gtk_label_new(ivar->label_step_filter_label);
    gtk_widget_ref(imf_inf->label_step_filter);
    gtk_object_set_data_full(GTK_OBJECT (main_window),
                             "imf_inf__step_label_filter",
                             imf_inf->label_step_filter,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(imf_inf->label_step_filter);
    gtk_misc_set_padding(GTK_MISC(imf_inf->label_step_filter),
                         2,
                         0);
return imf_inf;
}

#endif /* ENABLE_IMGPROC */
