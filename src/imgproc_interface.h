
/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2007, 2008
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * Image processing tab
 * $Log: imgproc_interface.h,v $
 * Revision 1.1  2008-09-16 10:16:08  gerber
 * added imgproc routines
 *
 */
#ifndef GPIV_IMGPROC_INTERFACE_H
#define GPIV_IMGPROC_INTERFACE_H
/* #ifdef ENABLE_IMGPROC */

typedef struct _ImgfilterVar ImgfilterVar;
struct _ImgfilterVar {
  gint filter_id;
  gchar *button_label;
  gchar *label_filtervar_label;
  gint value;
  gint upper;
  gchar *label_step_filter_label;
  gint count_nr;
  gchar *appbar_msg;
};


typedef struct _ImgfilterInterface ImgfilterInterface;
struct _ImgfilterInterface {
  GtkWidget *button_filter;
  GtkWidget *label_filtervar;
  GtkObject *spinbutton_adj_filtervar;
  GtkWidget *spinbutton_filtervar;
  GtkWidget *label_step_filter;
};


typedef struct _Imgprocess Imgprocess;
struct _Imgprocess {
  GtkWidget *vbox_label;
  GtkWidget *label_title;

  GtkWidget *vbox_scroll;
  GtkWidget *scrolledwindow;
  GtkWidget *viewport;
  GtkWidget *vbox;
  GtkWidget *table;

  ImgfilterVar *ivar[IMG_FILTERS];
  ImgfilterInterface *imf_inf[IMG_FILTERS];

  GtkWidget *button;

};

Imgprocess *
create_imgproc(GnomeApp *main_window, 
	       GtkWidget *container);

/* #endif */ /* ENABLE_IMGPROC */
#endif /* GPIV_IMGPROC_INTERFACE_H */
