/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2007, 2008 Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/
/*
 * Program wide input / output functions (no callbacks)
 * $Log: io.c,v $
 * Revision 1.1  2008-09-16 11:21:27  gerber
 * added io
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "support.h"
#include "gpiv_gui.h"
#include "console.h"
#include "utils.h"
#include "display_image.h"

#define BYTES_TO_PROCESS 8192

static gint
select_action_from_uri (GpivConsole *gpiv, 
                        GnomeVFSURI *uri);

static void
select_action_from_fname (GpivConsole *gpiv, 
                          gchar *fname_in);

static GpivImage *
open_img (char *fname_in
          );

static int
open_piv (char *fname_in, 
          PivData *pida
          );

static gchar *
load_buffer (GpivConsole *gpiv,
             gchar *fname_in,
             gchar *fname_base,
             gchar *fname_ext,
             guint ibuf, 
             enum ClistPut clist_put
             );

static void
write_from_tmp_to_uri (const gchar *uri_string_tmp, 
                       const gchar *uri_string_out);

static void
move_to_uri (GnomeVFSURI *uri_out, 
             const gchar *fname_out);

static void
write_ascii_scalardata (GnomeVFSURI *uri_out,
                        GpivScalarData *scalar_data,
                        const char *fname_out_nosuf,
                        const gchar *suffix);


/*
 * Program-wide public functions
 */

/*
 * Retrieving functions
 */
void
select_action_from_name (GpivConsole *gpiv, 
                         gchar *name)
/* ----------------------------------------------------------------------------
 *  Checks on validity of uri and whether local file or not. 
 *  Calls apropiate functions.
 */
{
    gint result;
    gchar *text_uri;
    GnomeVFSURI* uri = NULL;


    text_uri = gnome_vfs_make_uri_from_shell_arg (name);
    g_message("select_action_from_name:: text_uri = %s", text_uri);
    uri = gnome_vfs_uri_new (text_uri);
    g_free (text_uri);
    if (gnome_vfs_uri_exists (uri)) {
        if (gnome_vfs_uri_is_local (uri)) {
            const gchar *path =  gnome_vfs_uri_get_path (uri);
            select_action_from_fname (gpiv, (gchar *) path);
        } else {
            if ((result = select_action_from_uri (gpiv, uri)) != 0) {
                g_error("select_action_from_name: select_action_from_uri");
            }

        }
    } else {
        warning_gpiv ("Uri or filename of input image does not exist");
    }
}



/*
 * Saving functions
 */
void
write_hdf_img_data (const gchar *fname_out_nosuf,
                    GnomeVFSURI *uri_out)
/*------------------------------------------------------------------------------
 */
{
    char *err_msg = NULL;
    gchar *fname_out = g_strdup_printf ("%s%s" , fname_out_nosuf, 
                                        GPIV_EXT_GPIV);


    if (gpiv_par->verbose) g_message ("writing to %s", fname_out);

    if ((err_msg = 
         gpiv_fcreate_hdf5 (fname_out)) 
        != NULL) error_gpiv ("%s", err_msg);

    if (gpiv_par->img_fmt == IMG_FMT_HDF
        && display_act->img->saved_img == FALSE) {
        if ((err_msg = 
             gpiv_img_fwrite_hdf5_parameters (fname_out, 
                                              display_act->img->image->header))
            != NULL) error_gpiv ("%s", err_msg);
            
        if ((err_msg = 
             gpiv_fwrite_hdf5_image (fname_out, display_act->img->image, FALSE))
            != NULL) error_gpiv ("%s", err_msg);
        display_act->img->saved_img = TRUE;
    }

            
    if (display_act->pida->exist_piv) {
        if (display_act->pida->scaled_piv) {
            if ((err_msg = 
                 gpiv_fwrite_hdf5_pivdata (fname_out, 
                                           display_act->pida->piv_data_scaled, 
                                           "PIV",
                                           FALSE))
                != NULL) error_gpiv ("%s", err_msg);
                
        } else {
            if ((err_msg = 
                 gpiv_fwrite_hdf5_pivdata (fname_out, 
                                           display_act->pida->piv_data, 
                                           "PIV", 
                                           FALSE))
                != NULL) error_gpiv ("%s", err_msg);
                
        }
            
        display_act->pida->saved_piv = TRUE;
            
        if ((err_msg = 
             gpiv_piv_fwrite_hdf5_parameters (fname_out, 
                                              display_act->pida->piv_par))
            != NULL) error_gpiv ("%s", err_msg);
    }
            

    if (display_act->pida->exist_valid) {
        if ((err_msg = 
             gpiv_valid_fwrite_hdf5_parameters (fname_out, 
                                                display_act->pida->valid_par))
            != NULL) error_gpiv ("%s", err_msg);
    }


    if (display_act->pida->exist_vor) {
        if (display_act->pida->scaled_piv ) {
            if ((err_msg = 
                 gpiv_fwrite_hdf5_scdata (fname_out, 
                                          display_act->pida->vor_data_scaled, 
                                          "VORTICITY",
                                          FALSE))
                != NULL) error_gpiv ("%s", err_msg);
        } else {
            if ((err_msg = 
                 gpiv_fwrite_hdf5_scdata (fname_out, 
                                          display_act->pida->vor_data, 
                                          "VORTICITY",
                                          FALSE))
                != NULL) error_gpiv ("%s", err_msg);
        }
        display_act->pida->saved_vor = TRUE;
    }


    if (display_act->pida->exist_sstrain) {
        if (display_act->pida->scaled_piv ) {
            if ((err_msg = 
                 gpiv_fwrite_hdf5_scdata (fname_out, 
                                          display_act->pida->sstrain_data_scaled, 
                                          "SHEAR_STRAIN",
                                          FALSE))
                != NULL) error_gpiv ("%s", err_msg);
        } else {
            if ((err_msg = 
                 gpiv_fwrite_hdf5_scdata (fname_out, 
                                          display_act->pida->sstrain_data, 
                                          "SHEAR_STRAIN",
                                          FALSE))
                != NULL) error_gpiv ("%s", err_msg);
        }
        display_act->pida->saved_sstrain = TRUE;
    }
        
        
    if (display_act->pida->exist_nstrain) {
        if (display_act->pida->scaled_piv ) {
            if ((err_msg = 
                 gpiv_fwrite_hdf5_scdata (fname_out, 
                                          display_act->pida->nstrain_data_scaled, 
                                          "NORMAL_STRAIN",
                                          FALSE))
                != NULL) error_gpiv ("%s", err_msg);
        } else {
            if ((err_msg = 
                 gpiv_fwrite_hdf5_scdata (fname_out, 
                                          display_act->pida->nstrain_data, 
                                          "NORMAL_STRAIN",
                                          FALSE))
                != NULL) error_gpiv ("%s", err_msg);
        }
        display_act->pida->saved_nstrain = TRUE;
    }
        
        
    if ((err_msg = 
         gpiv_post_fwrite_hdf5_parameters (fname_out, 
                                           display_act->pida->post_par))
        != NULL) error_gpiv ("%s", err_msg);
        
            
    move_to_uri (uri_out, fname_out);
    g_free (fname_out);
}



void
write_img (const gchar *fname_out_nosuf,
           GnomeVFSURI *uri_out)
/*------------------------------------------------------------------------------
 * Store image to RAW, HDF or PNG format
 */
{
    char *err_msg = NULL;
    gchar *fname_out;       
    FILE *fp_out;


    if (display_act->img->saved_img == FALSE) {
        if (gpiv_par->img_fmt == IMG_FMT_PNG) {
            fname_out = g_strdup_printf ("%s%s" , 
                                         replace_tilde_with_home_dir (fname_out_nosuf), 
                                         GPIV_EXT_PNG_IMAGE);

            if ((fp_out = fopen (fname_out, "wb")) == NULL) {
                err_msg = "fopen";
                error_gpiv ("gpiv: write_img: fopen (%s, \"wb\")) == NULL", 
                            fname_out);
            }
            if ((err_msg = gpiv_write_png_image (fp_out, 
                                                 display_act->img->image, 
                                                 FALSE))
                != NULL) {
                gpiv_error ("gpiv: write_img: %s\n", err_msg);
            }
            fclose (fp_out);
            move_to_uri (uri_out, fname_out);
            g_free (fname_out);

        } else if (gpiv_par->img_fmt == IMG_FMT_HDF) {
            fname_out = g_strdup_printf ("%s%s" , 
                                         replace_tilde_with_home_dir (fname_out_nosuf), 
                                         GPIV_EXT_GPIV);
            if (gpiv_par->verbose) g_message ("writing image to %s", fname_out);
            if ((err_msg = 
                 gpiv_fcreate_hdf5 (fname_out)) 
                != NULL) error_gpiv ("%s", err_msg);
            if ((err_msg = 
                 gpiv_img_fwrite_hdf5_parameters (fname_out, 
                                                  display_act->img->image->header))
                != NULL) error_gpiv ("%s", err_msg);
            if ((err_msg = 
                 gpiv_fwrite_hdf5_image (fname_out, 
                                         display_act->img->image, 
                                         FALSE))
                != NULL) error_gpiv ("%s", err_msg);
            move_to_uri (uri_out, fname_out);
            g_free (fname_out);
                
        } else if (gpiv_par->img_fmt == IMG_FMT_RAW) {
            /*
             * Saving ASCII header
             */
            FILE *fp_header;
            gchar *fname_header = g_strdup_printf ("%s%s" , 
                                                   replace_tilde_with_home_dir (fname_out_nosuf), 
                                                   GPIV_EXT_HEADER);
            if (gpiv_par->verbose) g_message ("writing header to %s", fname_header);
            if ((fp_header = fopen (fname_header, "w")) == NULL) {
                error_gpiv (_("Failure opening image header for output"));
            }
            /*        fprintf(fp_par, "#%s\n", gl_image_par->software);
             */
            gpiv_img_print_header (fp_header, display_act->img->image->header);
            fclose (fp_header);
            move_to_uri (uri_out, fname_header);
            g_free (fname_header);
            /*
             * BUGFIX: Saving RAW image
             */
        }
        display_act->img->saved_img = TRUE;
    }
}



void
write_ascii_parameters (const char *fname_out_nosuf,
			GnomeVFSURI *uri_out,
                        const gchar *suffix)
/*-----------------------------------------------------------------------------
 * Saves parameters in ascii format
 */
{
    FILE *fp;
    gint flag_vor = 0;
    gchar *fname = g_strdup_printf ("%s%s" , 
                                    replace_tilde_with_home_dir (fname_out_nosuf), 
                                    suffix);


    if ((fp = fopen (fname, "w")) == NULL) {
        error_gpiv (_("Failure opening parameter file for output"));
    }            
 
    /*
     * Saving image header
     */
    if (gl_image_par->software__set) {
        fprintf (fp, "# Software: %s\n", gl_image_par->software);
    }
    gpiv_img_print_header (fp, display_act->img->image->header);

    /*
     * Saving PIV parameters
     */
    if (display_act->pida->exist_piv) {
        gpiv_piv_print_parameters (fp, display_act->pida->piv_par);
        if (display_act->pida->exist_valid) {
            gpiv_valid_print_parameters (fp, display_act->pida->valid_par);
        }

    }

    /*
     * Saving scalar parameters for vorticity
     */
    if (display_act->pida->exist_vor) {
        if (flag_vor == 0) {
            gpiv_post_print_parameters (stdout, display_act->pida->post_par);
            gpiv_post_print_parameters (fp, display_act->pida->post_par);
            flag_vor = 1;
        }
    }

    /*
     * Saving scalar parameters for shear strain
     */
    if (display_act->pida->exist_sstrain) {
        if (flag_vor == 0) {
            gpiv_post_print_parameters (fp, display_act->pida->post_par);
            flag_vor = 1;
        }
    }

    /*
     * Saving scalar parameters for normal strain
     */
    if (display_act->pida->exist_nstrain) {
        if (flag_vor == 0) {
            gpiv_post_print_parameters (fp, display_act->pida->post_par);
            flag_vor = 1;
        }
    }

    fclose (fp);
    move_to_uri (uri_out, fname);
    g_free (fname);
}



void
write_ascii_data (const gchar *fname_out_nosuf,
                  GnomeVFSURI *uri_out)
/*------------------------------------------------------------------------------
 * Store resulting (PIV and derived) data in ASCII format at
 *  different files
 */
{
    char *err_msg = NULL;
    gchar *fname;
    FILE *fp = NULL;  


    /*
     * Saving PIV data
     */
    if (display_act->pida->exist_piv) {
        fname = g_strdup_printf ("%s%s", 
                                      replace_tilde_with_home_dir (fname_out_nosuf), 
                                     GPIV_EXT_PIV);

        if ((fp = fopen (fname, "wb")) == NULL) {
            warning_gpiv ("write_ascii_data: failing fopen %s", fname);
            return;
        }

        if (display_act->pida->scaled_piv) {
            if ((err_msg = 
                 gpiv_write_pivdata (fp, 
                                     display_act->pida->piv_data_scaled, 
                                     FALSE))
                != NULL) error_gpiv ("%s", err_msg);
        } else {
            if ((err_msg = 
                 gpiv_write_pivdata (fp, 
                                     display_act->pida->piv_data, 
                                     FALSE))
                != NULL) error_gpiv ("%s", err_msg);
        }

        fclose (fp);   
        display_act->pida->saved_piv = TRUE;
        move_to_uri (uri_out, fname);
        g_free (fname);
    }

    /*
     * Saving histogram
     */
    if (display_act->pida->exist_histo) {
        fname = g_strdup_printf ("%s%s" , 
                                     replace_tilde_with_home_dir (fname_out_nosuf), 
                                     GPIV_EXT_UVHISTO);
        if (gpiv_par->verbose) g_message ("writing HISTO data to %s", fname);

        if ((fp = fopen (fname, "wb")) == NULL) {
            warning_gpiv ("write_ascii_data: failing fopen %s", fname);
            return;
        }

         gpiv_print_histo (fp, 
                           display_act->pida->klass, 
                          FALSE);

        fclose (fp);   
        display_act->pida->saved_histo = TRUE;
        move_to_uri (uri_out, fname);
        g_free (fname);
    }

    /*
     * Saving scalar vorticity data
     */
    if (display_act->pida->exist_vor_scaled) {
        write_ascii_scalardata (uri_out, display_act->pida->vor_data_scaled,
                                fname_out_nosuf, GPIV_EXT_VOR);
        display_act->pida->saved_vor = TRUE;
    } else if (display_act->pida->exist_vor) {
        write_ascii_scalardata (uri_out, display_act->pida->vor_data,
                                fname_out_nosuf, GPIV_EXT_VOR);
        display_act->pida->saved_vor = TRUE;
    }


    /*
     * Saving scalar shear strain data
     */
    if (display_act->pida->exist_sstrain_scaled) {
        write_ascii_scalardata (uri_out, display_act->pida->sstrain_data_scaled,
                                fname_out_nosuf, GPIV_EXT_SSTR);
        display_act->pida->saved_sstrain = TRUE;
    } else if (display_act->pida->exist_sstrain) {
        write_ascii_scalardata (uri_out, display_act->pida->sstrain_data,
                                fname_out_nosuf, GPIV_EXT_SSTR);
        display_act->pida->saved_sstrain = TRUE;
    }

    /*
     * Saving scalar normal strain data
     */
    if (display_act->pida->exist_nstrain_scaled) {
        write_ascii_scalardata (uri_out, display_act->pida->nstrain_data_scaled,
                                fname_out_nosuf, GPIV_EXT_NSTR);
        display_act->pida->saved_nstrain = TRUE;
    } else if (display_act->pida->exist_nstrain) {
        write_ascii_scalardata (uri_out, display_act->pida->nstrain_data,
                                fname_out_nosuf, GPIV_EXT_NSTR);
        display_act->pida->saved_nstrain = TRUE;
    }

}



/*
 * Local functions on retrieving data
 */

static gint
select_action_from_uri (GpivConsole *gpiv, 
                        GnomeVFSURI *uri)
/* ----------------------------------------------------------------------------
 * checks filename extension on valid image/data name, on existence of buffer,
 * loads data
 * As i/o from libgpiv (which does not have libgnomevfs) will have to be
 * be used (applying libpng funcionality), the image (data) are copied to a 
 * tmp directory and subsequtially be loaded with libgpiv i/o
 * functions.
 */
{
    GnomeVFSHandle   *read_handle, *write_handle;
    GnomeVFSResult    result, result_while;
    GnomeVFSFileSize  bytes_read, bytes_written;
    guint             buffer[BYTES_TO_PROCESS];
    gchar            *output_uri_string;

    const gchar      *tmp_dir = g_get_tmp_dir ();
    const gchar      *user_name = g_get_user_name ();
    const gchar      *local_tmp_dirname = g_strdup_printf ("%s/%s", 
                                                           tmp_dir, user_name); 
    FILE *fp;

    gchar *dirname = gnome_vfs_uri_extract_dirname (uri);
    gchar *basename = gnome_vfs_uri_extract_short_name (uri);
    gchar *command;


    g_mkdir(local_tmp_dirname, 0777);
    if (gpiv_par->verbose) {
        g_message ("select_action_from_uri:: tmp_dir = %s", tmp_dir);
        g_message ("select_action_from_uri:: user_name = %s", user_name);
        g_message ("select_action_from_uri:: local_tmp_dirname  = %s", 
                   local_tmp_dirname);
        g_message ("select_action_from_uri:: dirname = %s basename = %s", 
                   dirname, basename);
    }

    output_uri_string = g_strdup_printf ("%s/%s/%s" ,tmp_dir, 
                                         user_name, basename);
    if (gpiv_par->verbose) {
        g_message ("select_action_from_uri:: output_uri_string = %s", 
                   output_uri_string);
    }

    if ((result = gnome_vfs_open_uri (&read_handle, uri, GNOME_VFS_OPEN_READ))
        != GNOME_VFS_OK) 
        gpiv_error ("select_action_from_uri: gnome_vfs_open_uri");
    
    if ((result = gnome_vfs_create (&write_handle, output_uri_string, 
                                    GNOME_VFS_OPEN_WRITE, FALSE, 0777))
        != GNOME_VFS_OK) gpiv_error ("select_action_from_uri: gnome_vfs_create");

    /* read data from the input uri */
    while (result == GNOME_VFS_OK) {
        result = gnome_vfs_read (read_handle, buffer, BYTES_TO_PROCESS, 
                                 &bytes_read);

        if ((result_while = gnome_vfs_seek (write_handle, GNOME_VFS_SEEK_END, 0))
            != GNOME_VFS_OK) 
            gpiv_error ("select_action_from_uri: gnome_vfs_seek");

        if ((result_while = gnome_vfs_write (write_handle, buffer, bytes_read, 
                                             &bytes_written))
            != GNOME_VFS_OK)  
            gpiv_error ("select_action_from_uri: gnome_vfs_write");
    }

/*
 * add to uri to fname_last;
 * Reading image from temporarly storage
 * put correct name in Image tab
 * Cleaning up tmp
 */
    gpiv_var->fname_last = gnome_vfs_uri_to_string (uri, TRUE);
    gnome_config_push_prefix ("/gpiv/RuntimeVariables/");
    gnome_config_set_string ("fname_last", gpiv_var->fname_last);
    gnome_config_pop_prefix ();
    gnome_config_sync ();
    push_list_lastfnames (gpiv_var->fname_last);

    gpiv_var->fname_last__set = TRUE;
    select_action_from_fname (gpiv, output_uri_string);
    gpiv_var->fname_last__set = FALSE;

    display_act->file_uri_name = 
        g_strdup_printf ("%s", 
                         strtok (gnome_vfs_uri_to_string (uri, TRUE), "."));

    update_imgh_entries (gpiv, display_act->img->image->header);

    command = g_strdup_printf ("rm %s", output_uri_string);
    if (gpiv_par->verbose) {
        g_message ("command = %s", command);
    }
    if (system (command) != 0) {
        g_warning ("select_action_from_uri: could not exec shell command: %s",
                   command);
    }
    

    g_free (output_uri_string);
    g_free (local_tmp_dirname);
    g_free (dirname);
    g_free (basename);
    g_free (command);
    return 0;
}


static void
select_action_from_fname (GpivConsole *gpiv, 
                          gchar *fname_in)
/* ----------------------------------------------------------------------------
 * checks filename suffux on valid image/data name, on existence of buffer,
 * loads data
 */
{
    gchar *err_msg = NULL;
    gint ibuf, return_val;
    gchar *suf,        /* filename suffix */
        *dirname,      /* directory name */
        *fname_base,   /* Image name without suffix and dirname */
        *fname_nosuf,  /* Image name without suffix */
        *fname_home;
    gboolean exist_buf = FALSE;
    gchar command[2 * GPIV_MAX_CHARS];
/*     gchar *command; */


    suf = g_strdup (strrchr (fname_in, '.'));
/* add to push_list_lastfnames (fname_base); */
    if (!gpiv_var->fname_last__set) {
        gpiv_var->fname_last = g_strdup (fname_in);
        gnome_config_push_prefix ("/gpiv/RuntimeVariables/");
        gnome_config_set_string ("fname_last", gpiv_var->fname_last);
        gnome_config_pop_prefix ();
        gnome_config_sync ();
    }
/*
 * Stripping file name
 * We need them for displaying in the image header tab and on the buffer list
 */
    fname_home = replace_home_dir_with_tilde (fname_in);
    dirname = g_strdup (g_path_get_dirname (fname_home));
/*                         (replace_home_dir_with_tilde (fname_in))); */
    g_free (fname_home);
    fname_base = g_strdup (g_path_get_basename (fname_in));
    strtok (fname_base, ".");
    if (!gpiv_var->fname_last__set) {
        push_list_lastfnames (fname_base);
    }
    fname_nosuf = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S,
                                       fname_base, NULL));

/*
 * reading image data from raw, PNG, hdf or Davis(tm) format
 * Ensure that the tabulator widgets are enabled
 */
    if (g_str_has_suffix (fname_in, GPIV_EXT_RAW_IMAGE)
        || g_str_has_suffix (fname_in, GPIV_EXT_PNG_IMAGE)
        || g_str_has_suffix (fname_in, GPIV_EXT_PNG_IMAGE_UPCASE)
        || g_str_has_suffix (fname_in, GPIV_EXT_GPIV)
        || g_str_has_suffix (fname_in, GPIV_EXT_DAVIS)
        || g_str_has_suffix (fname_in, GPIV_EXT_PGM_IMAGE)
        || g_str_has_suffix (fname_in, GPIV_EXT_PGM_IMAGE_UPCASE)
        || g_str_has_suffix (fname_in, ".gif")
        || g_str_has_suffix (fname_in, ".GIF")
        || g_str_has_suffix (fname_in, ".tif")
        || g_str_has_suffix (fname_in, ".TIF")
        || g_str_has_suffix (fname_in, ".bmp")
        || g_str_has_suffix (fname_in, ".BMP")
        ) {


	exist_buf = FALSE;
	if (nbufs == 0) {
	    sensitive (gpiv, IMG, TRUE);
 	    sensitive (gpiv, EVAL, TRUE);
	} else {
            
/*
 * Check if file basename (and buffer/display) already exists
 * or if there is a gap in the numbering of the buffers
 */
	    for (ibuf = 0; ibuf < nbufs; ibuf++) {
                display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                      ibuf);
                if (ibuf == display_act->id
                    && strcmp (fname_nosuf, display_act->file_uri_name) == 0 ) {
                    if ((display_act->img->image = open_img (fname_in)) == NULL) {
                        return;
                    }

                    exist_buf = TRUE;
                    break;

                } else if (ibuf < display_act->id) {
                    if ((err_msg = 
                         load_buffer (gpiv, fname_in, fname_base, fname_nosuf, 
                                      ibuf, INSERT)) != NULL) {
                        g_warning ("select_action_from_fname: failing load_buffer");
                        return;
                    }
                    exist_buf = TRUE;
                    nbufs++;
                    break;
                } else {
                    /* g_warning ("select_action_from_fname: should not arrive here"); */
                }
            }
        }

/*
 * All buffers with id between 0 and nbufs already exist.
 * Create a new buffer and launches a display if file basename doesn't exist
 * and append to the list
 */
        if (!exist_buf) {
	    nbufs++;
	    ibuf = nbufs - 1;
            load_buffer (gpiv, fname_in, fname_base, fname_nosuf,
                         ibuf, APPEND);
        }

    } else {
	warning_gpiv (_("select_action_from_fname: non-valid file name"));
    }

    gtk_clist_set_selection_mode (GTK_CLIST (gpiv->clist_buf),
                                  GTK_SELECTION_EXTENDED);

#ifdef ENABLE_IMGPROC
    set_imgproc_filtervar (gpiv, GPIV_IMGFI_SUBACK, nbufs - 1);
#endif

    g_free (suf);
    g_free (dirname); 
    g_free (fname_base); 
    g_free (fname_nosuf);
}



static GpivImage *
open_img (char *fname_in
          )
/*-----------------------------------------------------------------------------
 * Opens an image from png, hdf or raw-data file (<=> open_dac_img)
 */
{
    GpivImage *image = NULL;
    gchar *err_msg = NULL;


    if ((image = gpiv_fread_image (fname_in)) == NULL) {
        warning_gpiv ("OPEN_IMG: failing gpiv_fread_image %s", fname_in);
        return;
    }

    display_act->img->exist_img = TRUE;

/*
 * The loaded image has not yet been modified, so there is no need to save 
 * image or header information
 */
    display_act->img->saved_img = TRUE; 


    return image;
}



static gint
open_piv (char *fname_in, 
          PivData *pida
          )
/*-----------------------------------------------------------------------------
 * Opens piv data file and displays
 */
{
    char *err_msg = NULL, RCSID_DATA[GPIV_MAX_CHARS];
    char *ext = g_strdup (strrchr (fname_in, '.'));
    gint var_scale = 0, scale = 0;


/*
 * parameter initializing of piv data
 */
/*     pida->piv_par->autokey = 1; */
    gpiv_piv_parameters_set (pida->piv_par, FALSE);

/*
 * Reads hdf format
 */
    if (strcmp (ext, GPIV_EXT_GPIV) == 0) {
        if ((pida->piv_data = 
             gpiv_fread_hdf5_pivdata (fname_in, "PIV"))
            == NULL) {
            warning_gpiv ("OPEN_PIV: failing piv_fread_hdf5_pivdata %s", 
                          fname_in);
            return -1;
        }
        pida->exist_piv = TRUE;
        pida->averaged_piv = FALSE;
        
        
/*
 * Reads raw data format
 */
    } else if (strcmp (ext, GPIV_EXT_RAW_IMAGE) == 0
               || strcmp (ext, GPIV_EXT_DAVIS) == 0
               ) {
        FILE *fp;
        gchar *dirname = g_strdup (g_dirname (fname_in)),
            *fname_base = g_strdup (g_basename (fname_in)),
            *fname_ext,
            *fname_piv;

/* BUGFIX
 * check on existance of piv data file
 */
        strtok (fname_base, ".");
        fname_ext = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S,
                                           fname_base, NULL));
        fname_piv = g_strdup (g_strconcat (fname_ext, GPIV_EXT_PIV, NULL));
        
        if ((fp = fopen (fname_piv, "r")) == NULL) {
            warning_gpiv ("OPEN_PIV: failing fopen %s", fname_piv);
        }
        
        if ((pida->piv_data = gpiv_read_pivdata (fp)) == NULL) {
            warning_gpiv ("OPEN_PIV: failing gpiv_read_pivdata");
            return -1;
        }
        
        fclose (fp);
        pida->exist_piv = TRUE;
        pida->averaged_piv = FALSE;

        g_free (dirname);
        g_free (fname_base); 
        g_free (fname_ext); 
        g_free (fname_piv); 
        
    
    } else {
         if (gpiv_par->verbose) g_message ("open_piv: no PIV data");
         return -1;
    }


    g_free (ext);
    return 0;
}



static gchar *
load_buffer (GpivConsole *gpiv,
             gchar *fname_in,
             gchar *fname_base,
             gchar *fname_ext,
             guint ibuf, 
             enum ClistPut clist_put
             )
/*-----------------------------------------------------------------------------
 * create display and its (popup) menu, load image, piv data and puts 
 * buffername into clist
 */
{
    gint return_val = 0;
    gchar *err_msg = NULL;
    gchar *clist_buf_txt[MAX_BUFS][2], cl_int[3];
    gboolean local_stretch_window = gpiv_par->display__stretch_window;
    GpivImage *image = NULL;


/*
 * We need to know the image dimensions before creating the buffer display
 * So, first load the image
 */
    if ((image = gpiv_fread_image (fname_in)) == NULL) {
        warning_gpiv ("load_buffer: failing gpiv_fread_image %s", fname_in);
        err_msg = "failing gpiv_fread_image";
        return err_msg;
    }

/*
 * Creating buffer and display
 */
    display[ibuf] = create_display (fname_ext, image, ibuf, gpiv);
    display_act = display[ibuf];
    display_act->img->exist_img = TRUE;
    display_act->img->saved_img = TRUE; 

/*
 * Load eventually existing PIV data
 */
    open_piv (fname_in, display[ibuf]->pida);

/* 
 * Suppress creating intregs during load_buffer by toggle menu;
 * after displaying it will result into the correct effect
 */
    view_toggle_intregs_flag = FALSE;
    gpiv_par->display__stretch_window = TRUE;

    display_act->display_popupmenu = create_display_popupmenu (display_act);
    gtk_signal_connect_object (GTK_OBJECT (display_act->mwin), 
                               "button_press_event",
                               GTK_SIGNAL_FUNC (on_my_popup_handler), 
                               GTK_OBJECT (display_act->display_popupmenu));
    gpiv_par->display__stretch_window = local_stretch_window;
    stretch_window (display_act);

/*
 * Creating pixbuf image for displaying of image data
 */
    if ( display_act->gci_bg != NULL) {
        destroy_background(display_act->gci_bg);
    }
   display_act->gci_bg = create_background (display_act);

   create_img (display_act);
    if (display_act->img->image->header->x_corr
        && display_act->img->image->frame2
        && display_act->display_backgrnd == SHOW_BG_IMG1) {
/*         hide_img2 (display_act); */
        show_img1 (display_act);
    } else if (display_act->display_backgrnd == SHOW_BG_IMG2) {
/*         hide_img1 (display_act); */
        show_img2 (display_act);
    } else if (display_act->display_backgrnd == SHOW_BG_DARKBLUE) {
        gchar *color = "darkblue";
/*         hide_img1 (display_act); */
/*         hide_img2 (display_act); */
        if (display_act->gci_bg != NULL) {
            gnome_canvas_item_set (GNOME_CANVAS_ITEM (display_act->gci_bg),
                                   "fill_color", color,
                                   NULL);
        }
    } else {
        gchar *color = "black";
        if (display_act->gci_bg != NULL) {
            gnome_canvas_item_set (GNOME_CANVAS_ITEM (display_act->gci_bg),
                                   "fill_color", color,
                                   NULL);
        }
    }


/*
 * Creating PIV vectors
 */
    if (display_act->display_piv) {
        create_all_vectors (display_act->pida);
    }

    display_act->img->img_mean = 
        image_mean (display_act->img->image->frame1,
                    display_act->img->image->header->ncolumns,
                    display_act->img->image->header->nrows);
    

    if (gpiv_par->display__intregs == 1) {
        show_all_intregs (display_act);
    }
    view_toggle_intregs_flag = TRUE;

/*
 * Add buffer to clist; the column int the console which shows the names of 
 * images
 */
    g_snprintf (cl_int, 3, "%d", ibuf);
    clist_buf_txt[ibuf][0] = (gchar *) cl_int;
    clist_buf_txt[ibuf][1] = fname_base;
    if (clist_put == PREPEND) {
        gtk_clist_prepend (GTK_CLIST (gpiv->clist_buf), 
                           clist_buf_txt[ibuf]);
    } else if (clist_put == INSERT) {
        gtk_clist_insert (GTK_CLIST (gpiv->clist_buf), ibuf, 
                          clist_buf_txt[ibuf]);
    } else if (clist_put == APPEND) {
        gtk_clist_append (GTK_CLIST (gpiv->clist_buf), 
                          clist_buf_txt[ibuf]);
    } else {
        error_gpiv ("non-existent CLIST enumerate");
    }
    
    gtk_clist_set_row_data (GTK_CLIST (gpiv->clist_buf), ibuf, 
                            display_act);
 

    gtk_widget_set_size_request (display_act->canvas, 
                                 (gint) (display_act->zoom_factor * 
                                         display_act->img->image->header->ncolumns), 
                                 (gint) (display_act->zoom_factor * 
                                         display_act->img->image->header->nrows));
    
#ifdef ENABLE_IMGPROC
    set_imgproc_filtervar (gpiv, GPIV_IMGFI_SUBACK, nbufs - 1);
#endif
    return err_msg;
}



/*
 * Local functions on writing
 */

static void
write_from_tmp_to_uri (const gchar *uri_string_tmp, 
                       const gchar *uri_string_out)
/*-----------------------------------------------------------------------------
 */
{
    GnomeVFSHandle   *read_handle, *write_handle;
    GnomeVFSResult    result, result_while;
    GnomeVFSFileSize  bytes_read, bytes_written;
    guint             buffer[BYTES_TO_PROCESS];
    gchar            *command;


    if ((result = gnome_vfs_open (&read_handle, uri_string_tmp, 
                                      GNOME_VFS_OPEN_READ))
        != GNOME_VFS_OK) gpiv_error ("write_from_tmp_to_uri: gnome_vfs_open_uri");

    if ((result = gnome_vfs_create (&write_handle, uri_string_out, 
                                    GNOME_VFS_OPEN_WRITE, FALSE, 0777))
        != GNOME_VFS_OK) gpiv_error ("write_from_tmp_to_uri: gnome_vfs_create");

    while (result == GNOME_VFS_OK) {
        result = gnome_vfs_read (read_handle, buffer, BYTES_TO_PROCESS, 
                                 &bytes_read);
                
        if ((result_while = gnome_vfs_write (write_handle, buffer, bytes_read, 
                                             &bytes_written))
            != GNOME_VFS_OK)  gpiv_error ("write_from_tmp_to_uri: gnome_vfs_write");
    }


    command = g_strdup_printf ("rm %s", gnome_vfs_uri_get_path 
                               (gnome_vfs_uri_new (uri_string_tmp)));
    if (system (command) != 0) {
        warning_gpiv ("write_from_tmp_to_uri: could not exec shell command: %s",
                   command);
    }


    g_free (command);
    return;
}


static void
move_to_uri (GnomeVFSURI *uri_out, 
             const gchar *fname_out)
/*-----------------------------------------------------------------------------
 * Move file to uri if not local
 */
{
    gchar *suffix = g_strdup (strrchr (fname_out, '.'));


    if (!gnome_vfs_uri_is_local (uri_out)) {
        gchar *uri_string_tmp = gnome_vfs_make_uri_from_input(fname_out);
        gchar *uri_string_out =  gnome_vfs_make_uri_from_input 


            (g_strdup_printf ("%s%s" ,
                              display_act->file_uri_name, 
                              suffix));
        write_from_tmp_to_uri (uri_string_tmp, uri_string_out);
        g_free (uri_string_tmp);
        g_free (uri_string_out);
    }

    g_free (suffix);
}


static void
write_ascii_scalardata (GnomeVFSURI *uri_out,
                        GpivScalarData *scalar_data,
                        const char *fname_out_nosuf,
                        const gchar *suffix)
/*-----------------------------------------------------------------------------
 * Saves scalar data in ascii format
 */
{
    gchar *err_msg = NULL;
    gchar *fname_out = g_strdup_printf ("%s%s" , 
                                        replace_tilde_with_home_dir 
                                        (fname_out_nosuf), 
                                        suffix);
    FILE *fp = NULL;


    if (gpiv_par->verbose) g_message ("writing ascii data to %s", fname_out);

    if ((fp = fopen (fname_out, "w")) == NULL) {
        warning_gpiv ("write_ascii_data: failing fopen %s", fname_out);
        return;
    }


    if ((err_msg = 
         gpiv_write_scdata (fp, scalar_data, FALSE))
        != NULL) error_gpiv ("%s", err_msg);


    fclose (fp);
    move_to_uri (uri_out, fname_out);
    g_free (fname_out);
}



