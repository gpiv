/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */


/*-----------------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/
/*
 * Program wide input / output functions (no callbacks)
 * $Log: io.h,v $
 * Revision 1.1  2008-09-16 11:21:27  gerber
 * added io
 *static void
 */

#ifndef IO_H
#define IO_H
void
select_action_from_name (GpivConsole *gpiv, 
                          gchar *name);

void 
write_hdf_img_data (const gchar *fname_out_nosuf,
                    GnomeVFSURI *uri_out);

void
write_img (const gchar *fname_out_nosuf,
           GnomeVFSURI *uri_out);

void
write_ascii_parameters (const char *fname_out_nosuf,
			GnomeVFSURI *uri_out,
                        const gchar *suffix);

void 
write_ascii_data (const gchar *fname_out_nosuf,
		  GnomeVFSURI *uri_out);

#endif /* IO_H */
