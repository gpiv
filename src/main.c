/* -*- Mode1: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * main routines of gpiv
 * $Log: main.c,v $
 * Revision 1.30  2008-10-09 14:43:37  gerber
 * paralellized with OMP and MPI
 *
 * Revision 1.29  2008-04-28 12:00:57  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.28  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.27  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.26  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.25  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.24  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.23  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.22  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.21  2006/01/31 15:15:33  gerber
 * version 0.3.0b; minor change
 *
 * Revision 1.20  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.18  2005/03/04 12:52:01  gerber
 * print message if gpiv_scan_resourcefiles returns with err_msg != NULL ==>
 * this is not an error, but defaults are read
 *
 * Revision 1.17  2005/03/01 15:23:22  gerber
 * removed warning message
 *
 * Revision 1.16  2005/03/01 14:43:46  gerber
 * updated documentation
 *
 * Revision 1.15  2005/02/26 09:43:30  gerber
 * parameter flags (parameter_logic) defined as gboolean
 *
 * Revision 1.14  2005/02/26 09:17:13  gerber
 * structured of interrogate function by using gpiv_piv_isiadapt
 *
 * Revision 1.13  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.12  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.11  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.10  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 * Revision 1.9  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.8  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.7  2003/07/25 15:40:23  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.6  2003/07/13 14:38:18  gerber
 * changed error handling of libgpiv
 *
 * Revision 1.5  2003/07/12 21:21:16  gerber
 * changed error handling libgpiv
 *
 * Revision 1.3  2003/07/10 11:56:07  gerber
 * added man page
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */


/* 
 * some header inclusions for gtk/gnome (by glade)
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "support.h"

/*
 * all variables are read by a header
 */
#include "gpiv_gui.h"
#include "console.h"
/* #include "console_interface.h" */
#include "io.h"
#include "utils.h"
#include "utils_par.h"
#include "main.h"


/* #define USAGE "\ */
/* Usage: gpiv-gui [-f filename][-s N][-t] \n\ */
/* \n\ */
/* keys: \n\ */
/* -f filename: loads project \n\ */
/* -s N:        vector scale N \n\ */
/* -t:          show tooltips \n" */

/* [-p] */
/* -p:          print parameters and other info to stdout \n\ */



/* #define HELP "\n\ */
/* gpiv processes and analyzes images for (Digital) Particle Image  \n\ */
/* Velocimetry, post-processes the PIV data and visualizes its \n\ */
/* results in a graphical way. \n" */


static Par *
scan_parameters()
/*-----------------------------------------------------------------------------
 * Obtains parameter settings of the Graphic User Interface
 */
{
    gchar *err_msg;
    gboolean used_default = FALSE;
    Par *ldp = g_new (Par, 1); /* Local default parameter */
/*     Par *lgp = g_new (Par, 1); */


/*     default_par = ldp; */

    gnome_config_push_prefix ("/gpiv/General/");

    ldp->print_par = 
        gnome_config_get_bool_with_default ("print_par=FALSE", &used_default);
    if (used_default) {
        g_message ("print_par = %d from default", ldp->print_par);
        gnome_config_set_bool ("print_par", ldp->print_par);
    }
    
    ldp->verbose = 
        gnome_config_get_bool_with_default ("verbose=FALSE", &used_default);
    if (used_default) {
        g_message ("verbose = %d from default", ldp->verbose);
        gnome_config_set_bool ("verbose", ldp->verbose);
    }


    ldp->console__show_tooltips = 
        gnome_config_get_bool_with_default ("console__show_tooltips=TRUE", 
                                           &used_default);
    ldp->console__show_tooltips__set = TRUE;
    if (used_default) {
        g_message ("console__show_tooltips = %d from default", 
                  ldp->console__show_tooltips);
        gnome_config_set_bool ("console__show_tooltips", ldp->console__show_tooltips);
    }
    

    ldp->console__view_gpivbuttons = 
        gnome_config_get_bool_with_default ("console__view_gpivbuttons=TRUE", 
                                           &used_default);
    ldp->console__view_gpivbuttons__set = TRUE;
    if (used_default) {
        g_message ("console__view_gpivbuttons = %d from default", 
                  ldp->console__view_gpivbuttons);
        gnome_config_set_bool ("view__gpivbuttons", 
                              ldp->console__view_gpivbuttons);
    }


    ldp->console__view_tabulator = 
        gnome_config_get_bool_with_default ("console__view_tabulator=TRUE", 
                                           &used_default);
    ldp->console__view_tabulator__set = TRUE;
    if (used_default) {
        g_message ("console__view_tabulator = %d from default", 
                  ldp->console__view_tabulator);
        gnome_config_set_bool ("view__tabulator", 
                              ldp->console__view_tabulator);
    }
    

    ldp->console__nbins = 
        gnome_config_get_int_with_default ("console__nbins=10", &used_default);
    ldp->console__nbins__set = TRUE;
    if (used_default) {
        g_message ("console__nbins = %d from default", ldp->console__nbins);
        gnome_config_set_int ("console__nbins", ldp->console__nbins);
    }
    
 
   ldp->img_fmt = 
        gnome_config_get_int_with_default ("img_fmt=0", &used_default);
   ldp->img_fmt__set = TRUE;
    if (used_default) {
        g_message ("img_fmt = %d from default ??", ldp->img_fmt);
        gnome_config_set_bool ("img_fmt", ldp->img_fmt);
    }


    ldp->hdf = 
        gnome_config_get_bool_with_default ("hdf=FALSE", &used_default);
    ldp->hdf__set = TRUE;
    if (used_default) {
        g_message ("hdf = %d from default ??", ldp->hdf);
        gnome_config_set_bool ("hdf", ldp->hdf);
    }
    
#ifdef ENABLE_MPI
    ldp->mpi_nodes = 
        gnome_config_get_int_with_default ("mpi_nodes=4", &used_default);
    ldp->mpi_nodes__set = TRUE;
    if (used_default) {
        g_message ("mpi_nodes = %d from default", ldp->mpi_nodes);
        gnome_config_set_int ("mpi_nodes", ldp->mpi_nodes);
    }
#endif /* ENABLE_MPI */



    gnome_config_push_prefix ("/gpiv/Image/");

    ldp->x_corr = 
        gnome_config_get_bool_with_default ("x_corr=TRUE", &used_default);
    ldp->x_corr__set = TRUE;
    if (used_default) {
        g_message ("x_corr = %d from default", ldp->x_corr);
        gnome_config_set_bool ("x_corr", ldp->x_corr);
    }
    
    gnome_config_push_prefix ("/gpiv/Processes/");
    
#ifdef ENABLE_CAM
    ldp->process__cam = 
        gnome_config_get_bool_with_default ("process__cam=FALSE", &used_default);
    ldp->process__cam__set = TRUE;
    if (used_default) {
        g_message ("process__cam = %d from default", ldp->process__cam);
        gnome_config_set_bool ("process__cam", ldp->process__cam);
    }
#endif /* ENABLE_CAM */
    
#ifdef ENABLE_TRIG
    ldp->process__trig = 
        gnome_config_get_bool_with_default ("process__trig=FALSE", &used_default);
    ldp->process__trig__set = TRUE;
    if (used_default) {
        g_message ("process__trig = %d from default", ldp->process__trig);
        gnome_config_set_bool ("process__trig", ldp->process__trig);
    }
#endif /* ENABLE_TRIG */
    
#ifdef ENABLE_IMGPROC
    ldp->process__imgproc = 
        gnome_config_get_bool_with_default ("process__imgproc=FALSE", &used_default);
    ldp->process__imgproc__set = TRUE;
    if (used_default) {
        g_message ("process__imgproc = %d from default", ldp->process__imgproc);
        gnome_config_set_bool ("process__imgproc", ldp->process__imgproc);
    }
#endif /* ENABLE_IMGPROC */

    ldp->process__piv = 
        gnome_config_get_bool_with_default ("process__piv=TRUE", &used_default);
    ldp->process__piv__set = TRUE;
    if (used_default) {
        g_message ("process__piv = %d from default", ldp->process__piv);
        gnome_config_set_bool ("process__piv", ldp->process__piv);
    }

    
    ldp->process__gradient = 
        gnome_config_get_bool_with_default ("process__gradient=FALSE", 
                                           &used_default);
    ldp->process__gradient__set = TRUE;
    if (used_default) {
        g_message ("process__gradient = %d from default", 
                  ldp->process__gradient);
        gnome_config_set_bool ("process__gradient", 
                              ldp->process__gradient);
    }
    

    ldp->process__resstats = 
        gnome_config_get_bool_with_default ("process__resstats=FALSE", 
                                           &used_default);
    ldp->process__resstats__set = TRUE;
    if (used_default) {
        g_message ("process__resstats = %d from default", 
                  ldp->process__resstats);
        gnome_config_set_bool ("process__resstats", 
                              ldp->process__resstats);
    }
    

    ldp->process__errvec = 
        gnome_config_get_bool_with_default ("process__errvec=FALSE", 
                                           &used_default);
    ldp->process__errvec__set = TRUE;
    if (used_default) {
        g_message ("process__errvec = %d from default", 
                  ldp->process__errvec);
        gnome_config_set_bool ("process__errvec", 
                              ldp->process__errvec);
    }
    

    ldp->process__peaklock = 
        gnome_config_get_bool_with_default ("process__peaklock=FALSE", 
                                           &used_default);
    ldp->process__peaklock__set = TRUE;
    if (used_default) {
        g_message ("process__peaklock = %d from default", 
                  ldp->process__peaklock);
        gnome_config_set_bool ("process__peaklock", 
                              ldp->process__peaklock);
    }
    

    ldp->process__average = 
        gnome_config_get_bool_with_default ("process__average=FALSE", 
                                           &used_default);
    ldp->process__average__set = TRUE;
    if (used_default) {
        g_message ("process__average = %d from default", 
                  ldp->process__average);
        gnome_config_set_bool ("process__average", 
                              ldp->process__average);
    }
    

    ldp->process__scale = 
        gnome_config_get_bool_with_default ("process__scale=FALSE", 
                                           &used_default);
    ldp->process__scale__set = TRUE;
    if (used_default) {
        g_message ("process__scale = %d from default", 
                  ldp->process__scale);
        gnome_config_set_bool ("process__scale", 
                              ldp->process__scale);
    }
    

    ldp->process__subtract = 
        gnome_config_get_bool_with_default ("process__subtract=FALSE", 
                                           &used_default);
    ldp->process__subtract__set = TRUE;
    if (used_default) {
        g_message ("process__subtract = %d from default", 
                  ldp->process__subtract);
        gnome_config_set_bool ("process__subtract", 
                              ldp->process__subtract);
    }
    

    ldp->process__vorstra = 
        gnome_config_get_bool_with_default ("process__vorstra=FALSE", 
                                           &used_default);
    ldp->process__vorstra__set = TRUE;
    if (used_default) {
        g_message ("process__vorstra = %d from default", 
                  ldp->process__vorstra);
        gnome_config_set_bool ("process__vorstra", ldp->process__vorstra);
    }
    
    
    
    gnome_config_push_prefix ("/gpiv/Display/");
    
    ldp->display__view_menubar = 
        gnome_config_get_bool_with_default ("display__view_menubar=TRUE", 
                                           &used_default);
    ldp->display__view_menubar__set = TRUE;
    if (used_default) {
        g_message ("display__view_menubar = %d from default", 
                  ldp->display__view_menubar);
        gnome_config_set_bool ("display__view_menubar", 
                              ldp->display__view_menubar);
    }
    

    ldp->display__view_rulers = 
        gnome_config_get_bool_with_default ("display__view_rulers=TRUE", 
                                           &used_default);
    ldp->display__view_rulers__set = TRUE;
    if (used_default) {
        g_message ("display__view_rulers = %d from default", 
                  ldp->display__view_rulers);
        gnome_config_set_bool ("display__view_rulers", 
                              ldp->display__view_rulers);
    }
    
 
   ldp->display__stretch_auto = 
        gnome_config_get_bool_with_default ("display__stretch_auto=TRUE", 
                                           &used_default);
    ldp->display__stretch_auto__set = TRUE;
    if (used_default) {
        g_message ("display__stretch_auto = %d from default", 
                  ldp->display__stretch_auto);
        gnome_config_set_bool ("display__stretch_auto", 
                              ldp->display__stretch_auto);
    }
    

    ldp->display__backgrnd = 
        gnome_config_get_int_with_default ("display__background=0", &used_default);
    ldp->display__backgrnd__set = TRUE;
    if (used_default) {
        g_message ("display__background = %d from default", ldp->display__backgrnd);
        gnome_config_set_int ("display__background", ldp->display__backgrnd);
    }
    

    ldp->display__zoom_index = 
        gnome_config_get_int_with_default ("display__zoom_index=1", &used_default);
    ldp->display__zoom_index__set = TRUE;
    if (used_default) {
        g_message ("display__zoom_index = %d from default", ldp->display__zoom_index);
        gnome_config_set_int ("display__zoom_index", ldp->display__zoom_index);
    }
    

    ldp->display__vector_scale = 
        gnome_config_get_int_with_default ("display__vector_scale=8", &used_default);
    ldp->display__vector_scale__set = TRUE;
    if (used_default) {
        g_message ("display__vector_scale = %d from default", ldp->display__vector_scale);
        gnome_config_set_int ("display__vector_scale", ldp->display__vector_scale);
    }

    
    ldp->display__vector_color = 
        gnome_config_get_int_with_default ("display__vector_color=SHOW_PEAKNR", 
                                          &used_default);
    ldp->display__vector_color__set = TRUE;
    if (used_default) {
        g_message ("display__vector_color = %d from default", ldp->display__vector_color);
        gnome_config_set_int ("display__vector_color", ldp->display__vector_color);
    }
    
/*     ldp->stretch_window =  */
/*         gnome_config_get_int_with_default ("stretch_window=0", &used_default); */
/*     if (used_default) { */
/*         g_message ("stretch_window = %d from default",  */
/*                   ldp->stretch_window); */
/*         gnome_config_set_int ("stretch_window", ldp->stretch_window); */
/*     } */
    

    ldp->display__backgrnd = 
        gnome_config_get_int_with_default ("display__backgrnd=SHOW_BG_IMG1", 
                                           &used_default);
    ldp->display__backgrnd__set = TRUE;
    if (used_default) {
        g_message ("display__backgrnd = %d from default", ldp->display__backgrnd);
        gnome_config_set_int ("display__backgrnd", ldp->display__backgrnd);
    }
    

    ldp->display__scalar = 
        gnome_config_get_int_with_default ("display__scalar=SHOW_SC_NONE", 
                                           &used_default);
    ldp->display__scalar__set = TRUE;
    if (used_default) {
        g_message ("display__scalar = %d from default", ldp->display__scalar);
        gnome_config_set_int ("display__scalar", ldp->display__scalar);
    }
    

    ldp->display__intregs = 
        gnome_config_get_bool_with_default ("display__intregs=TRUE", 
                                           &used_default);
    ldp->display__intregs__set = TRUE;
    if (used_default) {
        g_message ("display__intregs = %d from default", 
                   ldp->display__intregs);
        gnome_config_set_bool ("display__intregs", ldp->display__intregs);
    }
    

    ldp->display__piv = 
        gnome_config_get_bool_with_default ("display__piv=TRUE", &used_default);
    ldp->display__piv__set = TRUE;
    if (used_default) {
        g_message ("display__piv = %d from default", ldp->display__piv);
        gnome_config_set_bool ("display__piv", ldp->display__piv);
    }


    gnome_config_pop_prefix ();
    gnome_config_sync();
    return ldp;
}



static Var *
scan_variables()
/*-----------------------------------------------------------------------------
 * Obtains additional variables, stored from previous sessions.
 * Not all structure elements of gpiv_var are stored.
 */
{
    gboolean used_default = FALSE;
    gchar fname_nr[GPIV_MAX_CHARS], fname_nr_default[GPIV_MAX_CHARS];
    gint i = 0;
    Var *lgv = g_new0 (Var, 1);


    gnome_config_push_prefix ("/gpiv/RuntimeVariables/");
    
    lgv->tab_pos = 
        gnome_config_get_int_with_default ("tab_pos=1", &used_default);
    if (used_default) {
        g_message ("tab_pos = %d from default", 
                  lgv->tab_pos);
        gnome_config_set_int ("tab_pos", lgv->tab_pos);
    }
    


    lgv->number_fnames_last = 
        gnome_config_get_int_with_default ("number_fnames_last=0", 
                                          &used_default);
    if (used_default) {
        g_message ("number_fnames_last = %d from default", 
                  lgv->number_fnames_last);
        gnome_config_set_int ("number_fnames_last", 
                             lgv->number_fnames_last);
    }
    
    
    for (i = 0; i < lgv->number_fnames_last; i++) {
        g_snprintf(fname_nr, GPIV_MAX_CHARS,"fname_last_%d", i);
        g_snprintf(fname_nr_default, GPIV_MAX_CHARS,"fname_last_%d=./", i);
        
        lgv->fn_last[i] = 
            gnome_config_get_string_with_default (fname_nr_default, 
                                                 &used_default);
        if (used_default) {
            g_message ("fnames_nr_default = %s from default", 
                      lgv->fn_last[i]);
            gnome_config_set_string(fname_nr, lgv->fn_last[i]);
        }
    }


    lgv->fname_date = 
        gnome_config_get_bool_with_default ("fname_date=FALSE", 
                                           &used_default);
    if (used_default) {
        g_message ("fname_date = %d from default", 
                  lgv->fname_date);
        gnome_config_set_bool ("fname_date", lgv->fname_date);
    }
    

    lgv->fname_time = 
        gnome_config_get_bool_with_default ("fname_time=FALSE", 
                                           &used_default);
    if (used_default) {
        g_message ("fname_time = %d from default", 
                  lgv->fname_time);
        gnome_config_set_bool ("fname_time", lgv->fname_time);
    }
    

    lgv->fname_last = 
        gnome_config_get_string_with_default ("fname_last=./", &used_default);
    if (used_default) {
        g_message ("fname_last = %s from default", lgv->fname_last);
        gnome_config_set_string("fname_last", lgv->fname_last);
    }


    lgv->img_span_px = 
        gnome_config_get_float_with_default ("span=1.0", &used_default);
    if (used_default) {
        g_message ("img_span_px = %f from default", lgv->img_span_px);
        gnome_config_set_float("span", lgv->img_span_px);
    }
    
    lgv->img_length_mm = 
        gnome_config_get_float_with_default ("length=1.0", &used_default);
    if (used_default) {
        g_message ("img_length_mm = %f from default", lgv->img_length_mm);
        gnome_config_set_float("length", lgv->img_length_mm);
    }
    
    lgv->piv_disproc_zoom = 
        gnome_config_get_float_with_default ("zoom_factor=1.0", &used_default);
    if (used_default) {
        g_message ("piv_disproc_zoom = %f from default", 
                  lgv->piv_disproc_zoom);
        gnome_config_set_float("piv_disproc_zoom", lgv->piv_disproc_zoom);
    }
    
    lgv->piv_disproc_vlength = 
        gnome_config_get_int_with_default ("piv_disproc_length=1", &used_default);
    if (used_default) {
        g_message ("piv_disproc_length = %d from default", 
                  lgv->piv_disproc_vlength);
        gnome_config_set_int ("piv_disproc_length", lgv->piv_disproc_vlength);
    }


    gnome_config_pop_prefix ();
    gnome_config_sync();
    return lgv;
}



static void
par_init (void)
/*-----------------------------------------------------------------------------
 * Reading parameters and variables from gnome parameter file (~/.gnome2/gpiv)
 * See: http://developer.gnome.org/doc/GGAD/z79.html
 *
 * This workaround is because popt table is read when 'gnome_program_init' is.
 * executed. The parameter structure loaded by popt (gp) needs to be compared 
 * with the values before reading the table, to find which parameters 
 * have been used as command arg. Therefore gp has been copied 
 * to gpiv_par before reading the table and is compared here.
 * If not defined by command arg, defaults will be used. Defaults only can be 
 * read after 'gnome_program_init' as it uses gnome_config_get_*. 
 *
 * Finally, the program variables are loaded here as well.
 */
{
    gchar *err_msg = NULL;


    set_parameters_ifdiff (&gp, gpiv_par);

    default_par = scan_parameters ();
/*     cp_undef_parameters (default_par, gpiv_par); */
    if ((err_msg =  cp_undef_parameters (default_par, gpiv_par)) != NULL) {
        gpiv_error ("par_init_args: failing cp_undef_parameters");
    }

    gpiv_var = scan_variables ();
}



static void
img_par_init (void)
/*-----------------------------------------------------------------------------
 * Setting and reading image parameters. 
 */
{
    char *err_msg = NULL;
    GpivImagePar *lo_image_par = g_new0 (GpivImagePar, 1);


    gl_image_par = lo_image_par;
    gpiv_img_parameters_set (gl_image_par, FALSE);
    if ((err_msg = 
         gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, gl_image_par, gpiv_par->print_par))
        != NULL) g_message ("%s: %s", RCSID, err_msg);

    g_snprintf (gl_image_par->software, GPIV_MAX_CHARS, "%s %s", PACKAGE, VERSION);
    gl_image_par->software__set = TRUE;

    g_snprintf (gl_image_par->author, GPIV_MAX_CHARS, "%s", g_get_real_name ());
    gl_image_par->author__set = TRUE;

    gpiv_img_default_parameters (gl_image_par, FALSE);

    if (gpiv_par->x_corr) {
        g_snprintf(IMAGE_CORRELATION_LABEL, GPIV_MAX_CHARS, "cross-correlation");
    } else {
        g_snprintf(IMAGE_CORRELATION_LABEL, GPIV_MAX_CHARS, "auto-correlation");
    }

    g_snprintf(IMAGE_WIDTH_LABEL, GPIV_MAX_CHARS, "%d", gl_image_par->ncolumns);
    g_snprintf(IMAGE_HEIGHT_LABEL, GPIV_MAX_CHARS, "%d", gl_image_par->nrows);
    g_snprintf(IMAGE_DEPTH_LABEL, GPIV_MAX_CHARS, "%d", gl_image_par->depth);
}



#ifdef ENABLE_CAM
static void
cam_par_init (gboolean cam_trig)
/*-----------------------------------------------------------------------------
 * parameter and variables initializing of data acquisition 
 * (trigger timings and cameraam)
 */
{
    gchar *err_msg;
    GpivCamPar *lo_cam_par = g_new0 (GpivCamPar, 1);


    gl_cam_par = lo_cam_par;
    gpiv_cam_parameters_set (gl_cam_par, FALSE);
    if ((err_msg = 
         gpiv_scan_resourcefiles (GPIV_CAMPAR_KEY, gl_cam_par, gpiv_par->print_par))
        != NULL) message_gpiv ("%s: %s", RCSID, err_msg);
    gpiv_cam_check_parameters_read (gl_cam_par, NULL);

    if ((cam_var = gpiv_cam_get_camvar (gpiv_par->verbose)) == NULL) {
        if (gpiv_par->verbose) 
            warning_gpiv("cam_par_init: failing gpiv_cam_get_camvar");
        cam_trig = FALSE;
    }

    if (cam_var->numCameras > 0) {
        if (dc1394_setup_capture (cam_var->camera[0].handle, 
                                 cam_var->camera[0].id, 
                                 cam_var->misc_info[0].iso_channel, 
                                 cam_var->misc_info[0].format, 
                                 cam_var->misc_info[0].mode, 
                                 cam_var->maxspeed,
                                 /* cam_var->misc_info[0].framerate */ FRAMERATE_7_5, 
                                 cam_var->capture[0])
            != DC1394_SUCCESS) {
            dc1394_release_camera (cam_var->handle, cam_var->capture[0]);
            raw1394_destroy_handle (cam_var->handle);
            g_warning("unable to setup camera-\n\
check line %d of %s to make sure\n\
that the video mode,framerate and format are\n\
supported by your camera\n",
                      __LINE__,__FILE__);
            return;
        }
        gl_image_par->ncolumns = cam_var->capture[0].frame_width;
        gl_image_par->nrows = cam_var->capture[0].frame_height;
        gl_image_par->ncolumns__set = TRUE;
        gl_image_par->nrows__set = TRUE;
        raw1394_destroy_handle(cam_var->handle);
        if (gpiv_par->verbose) {
            g_message ("cam_par_init:: from camera: nrows = %d ncolumns = %d", 
                       gl_image_par->nrows, gl_image_par->ncolumns);
        }
    }
}
#endif /* ENABLE_CAM */



#ifdef ENABLE_TRIG
static void
trig_par_init (void)
/*-----------------------------------------------------------------------------
 * parameter and variables initializing of data acquisition 
 * (trigger timings)
 */
{
    GpivTrigPar *lo_trig_par = g_new0 (GpivTrigPar, 1);


    gl_trig_par = lo_trig_par;
    gpiv_itrig_parameters_set (gl_trig_par, FALSE);
    gpiv_trig_parameters_set(gl_trig_par, FALSE);
    if ((err_msg = 
         gpiv_scan_resourcefiles (GPIV_TRIGPAR_KEY, gl_trig_par, gpiv_par->print_par))
        != NULL) message_gpiv ("%s: %s", RCSID, err_msg); 
    gpiv_trig_check_parameters_read (gl_trig_par, NULL);
}
#endif /* ENABLE_TRIG */



#ifdef ENABLE_IMGPROC
static void
imgproc_par_init (void)
/*-----------------------------------------------------------------------------
 * Setting and reading image processing parameters. 
 */
{
    char *err_msg = NULL;
    GpivImageProcPar *lo_imgproc_par = g_new0 (GpivImageProcPar, 1);


    gl_imgproc_par = lo_imgproc_par;
    gpiv_imgproc_parameters_set (gl_imgproc_par, FALSE);
    if ((err_msg = 
    gpiv_scan_resourcefiles (GPIV_IMGPROCPAR_KEY, gl_imgproc_par, 
                             gpiv_par->print_par))
       != NULL) g_message ("%s: %s", RCSID, err_msg);
    gpiv_imgproc_check_parameters_read (gl_imgproc_par, NULL);
}
#endif /* ENABLE_IMGPROC */



static void
piv_par_init (void)
/*-----------------------------------------------------------------------------
 * Reading piv interrogation parameters.
 */
{
    char *err_msg = NULL;
    GpivPivPar *lo_piv_par = g_new0 (GpivPivPar, 1);


    gl_piv_par = lo_piv_par;
    gpiv_piv_parameters_set (gl_piv_par, FALSE);
    if ((err_msg = 
         gpiv_scan_resourcefiles (GPIV_PIVPAR_KEY, gl_piv_par, gpiv_par->print_par))
        != NULL) g_message ("%s: %s", RCSID, err_msg);
    gpiv_piv_check_parameters_read(gl_piv_par, NULL);

    if (gl_piv_par->col_start > gl_image_par->ncolumns - gl_piv_par->pre_shift_col - 1) {
        gl_piv_par->col_start = 0;
        if (gpiv_par->verbose) 
            g_warning ("col_start larger than ncolumns; \nset to 0");
    }

    if (gl_piv_par->col_end > gl_image_par->ncolumns - gl_piv_par->pre_shift_col - 1) {
        gl_piv_par->col_end = gl_image_par->ncolumns - gl_piv_par->pre_shift_col - 1;
        if (gpiv_par->verbose) 
            g_warning("col_end larger than ncolumns; \nset to ncolumns %d",
                      gl_image_par->ncolumns - 1);
    }

    if (gl_piv_par->row_start > gl_image_par->nrows - gl_piv_par->pre_shift_row - 1) {
        gl_piv_par->row_start = 0;
        if (gpiv_par->verbose) 
            g_warning(_("row_start larger than nrows; \nset to zero"));
    }

    if (gl_piv_par->row_end > gl_image_par->nrows - gl_piv_par->pre_shift_row - 1) {
        gl_piv_par->row_end = gl_image_par->nrows - gl_piv_par->pre_shift_row - 1;
        if (gpiv_par->verbose) 
            g_warning("row_end larger than nrows; \nset to nrows %d",
                      gl_image_par->nrows - 1);
    }

}



static void
valid_par_init (void)
/*-----------------------------------------------------------------------------
 * Reading piv validation parameters.
 */
{
    char *err_msg = NULL;
    GpivValidPar *lo_valid_par = g_new0 (GpivValidPar, 1);


    gl_valid_par = lo_valid_par;
    gpiv_valid_parameters_set (gl_valid_par, FALSE);
    if ((err_msg = 
         gpiv_scan_resourcefiles (GPIV_VALIDPAR_KEY, gl_valid_par, gpiv_par->print_par))
        != NULL) g_message ("%s: %s", RCSID, err_msg); 
    gpiv_valid_check_parameters_read (gl_valid_par, NULL);
}



static void
post_par_init (void)
/*-----------------------------------------------------------------------------
 * Reading piv post-processing parameters.
 */
{
    char *err_msg = NULL;
    GpivPostPar *lo_post_par = g_new0 (GpivPostPar, 1);


    gl_post_par = lo_post_par;
    gpiv_post_parameters_set (gl_post_par, FALSE);
    if ((err_msg = 
         gpiv_scan_resourcefiles (GPIV_POSTPAR_KEY, gl_post_par, gpiv_par->print_par))
        != NULL) g_message ("%s: %s", RCSID, err_msg);
    gpiv_post_check_parameters_read (gl_post_par, NULL);
}



static void
load_images_from_commandline (GnomeProgram *program,
                              GpivConsole *gpiv)	
/*-----------------------------------------------------------------------------
 * Loading the images from the command line while launching gpiv.
 */
{
    gint i, result;
    GValue value = { 0, };
    char **args;
    poptContext ctx;

    g_value_init (&value, G_TYPE_POINTER);
    g_object_get_property (G_OBJECT (program), GNOME_PARAM_POPT_CONTEXT, 
                           &value);
    ctx = g_value_get_pointer (&value);
    g_value_unset (&value);

    args = (char**) poptGetArgs(ctx);
    if (args) {
        for (i = 0; args[i]; i++) {
            select_action_from_name (gpiv, args[i]);
        }
    }

}



int main (int argc, 
          char *argv[]
          )
/*-----------------------------------------------------------------------------
 * Main routine of gpiv
 */
{
    GnomeProgram *program;
/*     GpivConsole *gpiv = g_new0 (GpivConsole, 1); */
    GtkWidget *window1;

    char *err_msg = NULL;
    gchar *msg = "Welcome to gpiv.";
    GtkWidget *menu_gpiv_popup = NULL;
    GpivConsole *gpiv = NULL;
    gboolean trig_trig = TRUE, cam_trig = TRUE;
    gint trig_init, trig_start, trig_stop, trig_error;
    gint i;

    gboolean restored = FALSE;

#ifdef ENABLE_NLS
    setlocale (LC_ALL, "");
    bindtextdomain(PACKAGE, PACKAGE_LOCALE_DIR);
    textdomain(PACKAGE);
#endif

/* 
 * parameter initializing of gpiv-gui, image
 */
    msg_default = "";
    m_select = 0;
    nbufs = 0;
    exec_process = FALSE;
    cancel_process = FALSE;
    display_act = NULL;
    gci_aoi = NULL;
    gci_line = NULL; 


    if ((gpiv_par = cp_parameters (&gp)) == NULL) {
        gpiv_error ("main: failing cp_parameters");
    }

/* Initialize gnome program
 * command line keys: using POPT
 */
    Gpiv_app = NULL;
    program = gnome_program_init (PACKAGE, VERSION, 
                                  LIBGNOMEUI_MODULE, argc, argv,
                                  GNOME_PARAM_POPT_TABLE, options,
                                  GNOME_PARAM_HUMAN_READABLE_NAME, 
                                  _("Image analyser for Particle Image Velocimetry"),
                                  GNOME_PROGRAM_STANDARD_PROPERTIES, 
                                  NULL);

/*
 * Used for authentification when loading/storing images from remote system
 * with gnome_vfs 
 */
    gnome_authentication_manager_init ();

/*
 * Reading program parameters, variables and process parameters 
 */
    gdk_rgb_init ();
    par_init ();
    img_par_init ();

#ifdef ENABLE_CAM
    cam_par_init (cam_trig);
#endif

#ifdef ENABLE_TRIG
    trig_par_init ();
#endif

#ifdef ENABLE_IMGPROC
    imgproc_par_init ();
#endif

    piv_par_init ();
    valid_par_init ();
    post_par_init ();


    if (gl_image_par->ncolumns <= IMAGE_WIDTH_MAX 
        && gl_image_par->nrows <= IMAGE_HEIGHT_MAX
        && gl_image_par->depth <= IMAGE_DEPTH_MAX
        ) {
/*
 * creating the Graphic interface
 */
        gpiv = create_gpiv ();
        gtk_widget_show (GTK_WIDGET (gpiv->console));        
        menu_gpiv_popup = create_menu_gpiv_popup (gpiv);
        gtk_signal_connect_object (GTK_OBJECT(gpiv->console), 
                                   "button_press_event",
                                   GTK_SIGNAL_FUNC (on_my_popup_handler), 
                                   GTK_OBJECT (menu_gpiv_popup));
        
        gnome_appbar_set_default (GNOME_APPBAR(gpiv->appbar), msg_default);
        gnome_appbar_push (GNOME_APPBAR(gpiv->appbar), msg);
        
        if (gpiv_par->console__show_tooltips) {
            gtk_tooltips_enable(gpiv->tooltips);
        } else {
            gtk_tooltips_disable(gpiv->tooltips);
        }
        
#ifdef ENABLE_TRIG
/*
 * Widgets will be enabled / disabled if trigger kernel modules
 * are loaded and accessible.
 * An info message will be displayed if features are absent
 */
        if (!gpiv_trig_openrtfs (&trig_init, &trig_start, &trig_stop, 
                                 &trig_error)) {
            message_gpiv (_("Trigger system not available"));
            trig_trig = FALSE;
        }
        exec_trigger_stop ();
        sensitive (gpiv, DAC_TRIG, trig_trig);
        sensitive (gpiv, DAC_TIMING, trig_trig);
#endif /* ENABLE_TRIG */

#ifdef ENABLE_CAM
/*
 * Widgets will be enabled / disabled if camera is connected.
 * An info message will be displayed if features are absent
 */
        if (cam_var->numCameras > 0) { 
            sensitive (gpiv, DAC_CAM, TRUE);
            if (cam_trig) {
                sensitive (gpiv, DAC_TIMING, TRUE);
            }
        } else {
            sensitive (gpiv, DAC_CAM, FALSE);
            if (cam_trig) {
                sensitive (gpiv, DAC_TIMING, FALSE);
            }
            message_gpiv (_("No camera connected"));
        }
#endif /* ENABLE_CAM */
        
        sensitive (gpiv, IMG, TRUE);
        sensitive (gpiv, EVAL, TRUE);
        sensitive (gpiv, INTREGS, FALSE);
        sensitive (gpiv, VALID, TRUE);
        sensitive (gpiv, POST, TRUE);

        load_images_from_commandline (program, gpiv);

    } else {
        error_gpiv(_("Image dimensions or depth are larger than %dx%dx%d."),
                  (int)IMAGE_WIDTH_MAX, (int)IMAGE_HEIGHT_MAX, 
                  (int)IMAGE_DEPTH_MAX);
    }

    gtk_main ();
    exit(0);
}
