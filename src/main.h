/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/
/*         {NULL, 'q', POPT_ARG_NONE, &q, 0,  */
/*          N_("Explain gpiv"), NULL}, */


static const struct poptOption options [] = {
    {"cross", 'x', POPT_ARG_NONE, &gp.x_corr, 0, 
     N_("cross-correlation image"), N_("XCORR")},

    {"img_fmt", '\0', POPT_ARG_INT, &gp.img_fmt, 0, 
     N_("store image and header in PNG (0), raw (1) or HDF5 (2) format"), NULL},

    {"hdf", '\0', POPT_ARG_NONE, &gp.hdf, 0, 
     N_("store resulting data in HDF5 format with .h5 extension"), NULL},
        
#ifdef ENABLE_MPI
    {"mpi_nodes", '\0', POPT_ARG_INT, &gp.mpi_nodes, 0, 
     N_("number of nodes for parallel execution on cluster using MPI"), N_("INT")},
#endif /* ENABLE_MPI */

    {"print", 'p', POPT_ARG_NONE, &gp.print_par, 0, 
     N_("print parameters and other info to stdout"), NULL},
        
    {"verbose", 'V', POPT_ARG_NONE, &gp.verbose, 0, 
     N_("more verbose behaviour to stdout"), NULL},
        
    {"console__view_gpivbuttons", '\0', POPT_ARG_NONE, &gp.console__view_gpivbuttons, 0, 
     N_("view the GPIV processing buttons"), NULL},
        
    {"console__view_tabulator", '\0', POPT_ARG_NONE, &gp.console__view_tabulator, 0, 
     N_("view the tabulator of the application"), NULL},

    {"console__tooltips", '\0', POPT_ARG_NONE, &gp.console__show_tooltips, 0, 
     N_("show tooltips"), NULL},
        
    {"console__bins", '\0', POPT_ARG_INT, &gp.console__nbins, 0, 
     N_("number of bins for histograms"), N_("BINS")},
        
    {"display__view_menubar", '\0', POPT_ARG_NONE, &gp.display__view_menubar, 0, 
     N_("view the menubar of the display"), 
     NULL},
        
    {"display__view_rulers", '\0', POPT_ARG_NONE, &gp.display__view_rulers, 0, 
     N_("view the rulers of the display"), 
     NULL},
        
    {"display__stretch", '\0', POPT_ARG_NONE, &gp.display__stretch_auto, 0, 
     N_("automatic stretching of the display when zooming"), 
     NULL},
        
    {"display__background", '\0', POPT_ARG_INT, &gp.display__backgrnd, 0, 
     N_("display background: 0:darkblue 1:black 2:image1 3:image2"), 
     NULL},
        
    {"display__intregs", '\0', POPT_ARG_NONE, &gp.display__intregs, 0, 
     N_("display interrogation region contours"), NULL},
        
    {"display__piv", '\0', POPT_ARG_NONE, &gp.display__piv, 0, 
     N_("display PIV data"), NULL},
        
    {"display__scalar", '\0', POPT_ARG_INT, &gp.display__scalar, 0, 
     N_("display scalar data derived from PIV: 0:none 1:vorticity 2:shear strain 3:normal strain"), NULL},
        
    {"display__zoomscale", '\0', POPT_ARG_INT, &gp.display__zoom_index, 0, 
     N_("display zoom index: 0:0.5, 1:0.83, 2:1.0, 3:1.3, 4:1.6, 5:2.0"), 
     N_("SCALE")},
        
    {"display__vectorscale", '\0', POPT_ARG_INT, &gp.display__vector_scale, 0, 
     N_("vector length scale"), N_("SCALE")},

#ifdef ENABLE_CAM
    {"process__cam", '\0', POPT_ARG_NONE, &gp.process__cam, 0, 
     N_("executes image recording"), NULL},
#endif /* ENABLE_CAM */
        
#ifdef ENABLE_TRIG
    {"process__trig", '\0', POPT_ARG_NONE, &gp.process__trig, 0, 
     N_("executes triggering of light source and camera"), NULL},
#endif /* ENABLE_TRIG */
        
#ifdef ENABLE_IMGPROC
    {"process__imgproc", '\0', POPT_ARG_NONE, &gp.process__imgproc, 0, 
     N_("executes image manipulation"), NULL},
#endif /* ENABLE_IMGPROC */

    {"process__piv", '\0', POPT_ARG_NONE, &gp.process__piv, 0, 
     N_("executes piv interrogation"), NULL},
        
    {"process__gradient", '\0', POPT_ARG_NONE, &gp.process__gradient, 0, 
     N_("executes gradient test"), NULL},
        
    {"process__resstats", '\0', POPT_ARG_NONE, &gp.process__resstats, 0, 
     N_("executes calculation of residu statistics"), NULL},
        
    {"process__errvec", '\0', POPT_ARG_NONE, &gp.process__errvec, 0, 
     N_("executes PIV data validation"), NULL},
        
    {"process__peaklock", '\0', POPT_ARG_NONE, &gp.process__peaklock, 0, 
     N_("executes peaklock testing"), NULL},
        
    {"process__average", '\0', POPT_ARG_NONE, &gp.process__average, 0, 
     N_("executes average calculation"), NULL},
        
    {"process__scale", '\0', POPT_ARG_NONE, &gp.process__scale, 0, 
     N_("executes time and spatial scaling"), NULL},
        
    {"process__subtract", '\0', POPT_ARG_NONE, &gp.process__subtract, 0, 
     N_("executes subtracting the mean"), NULL},
        
    {"process__vorstra", '\0', POPT_ARG_NONE, &gp.process__vorstra, 0, 
     N_("executes vorticity / strain calculation"), NULL},

    {NULL, '\0', 0, NULL, 0}
};
