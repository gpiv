/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (callback) functions for Piv evaluation window/tabulator
 * $Log: piveval.c,v $
 * Revision 1.23  2008-04-28 12:00:57  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.22  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.21  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.20  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.19  2007-02-16 17:09:48  gerber
 * added Gauss weighting on I.A. and SPOF filtering (on correlation function)
 *
 * Revision 1.18  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.17  2007-01-29 11:27:43  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.16  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.15  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.13  2005/02/26 09:43:30  gerber
 * parameter flags (parameter_logic) defined as gboolean
 *
 * Revision 1.12  2005/02/26 09:17:14  gerber
 * structured of interrogate function by using gpiv_piv_isiadapt
 *
 * Revision 1.11  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.10  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.9  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.8  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.7  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.6  2003/07/13 14:38:18  gerber
 * changed error handling of libgpiv
 *
 * Revision 1.5  2003/07/12 21:21:16  gerber
 * changed error handling libgpiv
 *
 * Revision 1.3  2003/07/10 11:56:07  gerber
 * added man page
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */


#include "gpiv_gui.h"
#include "utils.h"
#include "piveval.h"
#include "piveval_interrogate.h"
#include "display.h"


/*
 * Private piv evaluation functions
 */


static void
display_img (float **img,
             gint int_size,
             gint *int_size_old,
             gint i_min, 
             gint i_max, 
             gint j_min, 
             gint j_max,
             gint rgb_width,
             guchar **rgbbuf,
             GtkWidget *canvas,
             GnomeCanvasItem **gci,
             double affine[6],
             gboolean scale
             )
/* ----------------------------------------------------------------------------
 * Displaying routine in gnome canvas
 */
{
    guchar *pos;
    gint i, j;
    float min = 1000.0, max = -1000.0;

    GdkPixbuf *pixbuf = NULL;
    guint16 fact = 1;
    gint depth = 8;    

    assert (img[0] != NULL);

    fact = fact << (gl_image_par->depth - depth);
/*     g_message ("DISPLAY_IMG:: fact = %d", fact); */

    if (*rgbbuf != NULL) {
        g_free(*rgbbuf);
        *rgbbuf = NULL;        
    }

    if (*rgbbuf == NULL) {
/*
 * row stride; each row is a 4-byte buffer array
 */
        rgb_width = (i_max - i_min) * 3;
        while ((rgb_width) % 4 != 0) {
            rgb_width++;
        } 

        *rgbbuf = g_malloc (rgb_width * 3 * (j_max - j_min));
    }


    if (*gci != NULL) {
        gtk_object_destroy (GTK_OBJECT(*gci));
        *gci = NULL;
    } 
    

    pixbuf = gdk_pixbuf_new_from_data (*rgbbuf,
                                       GDK_COLORSPACE_RGB,
                                       FALSE,
                                       depth,
                                       i_max - i_min, 
                                       j_max - j_min,
                                       rgb_width,
                                       NULL, 
                                       NULL);



    *gci = gnome_canvas_item_new (gnome_canvas_root( GNOME_CANVAS
                                                     (canvas)),
                                  gnome_canvas_pixbuf_get_type (),
                                  "pixbuf", pixbuf,
                                  NULL);
/* BUGFIX: new (check mem) */
/*     gdk_pixbuf_unref (pixbuf); */


    affine[4] = (GPIV_MAX_INTERR_SIZE - gpiv_var->piv_disproc_zoom 
                 * int_size) / 2.0;
    affine[5] = (GPIV_MAX_INTERR_SIZE - gpiv_var->piv_disproc_zoom 
                 * int_size) / 2.0;
    gnome_canvas_item_affine_absolute (*gci, affine);

/* 
 * As the mean has been subtracted from the image data, image intensities
 * will have to be tilted above zero
 */
    if (scale) {
        for (j = j_min; j < j_max; j++) {
            for (i = i_min; i < i_max; i++) {
                if (img[j][i] < min)
                    min = img[j][i];
                if (img[j][i] > max)
                    max = img[j][i];
            }
        }

        pos = *rgbbuf;
        for (j = j_min; j < j_max; j++) {
            for (i = i_min; i < i_max; i++) {
                img[j][i] -= min;
                img[j][i] = (255.0 / (max - min)) * img[j][i];
                *pos++ = (unsigned char) img[j][i];
                *pos++ = (unsigned char) img[j][i];
                *pos++ = (unsigned char) img[j][i];
            }
            *pos = *pos++;
            *pos = *pos++;
            *pos = *pos++;
        }


    } else {
        for (j = j_min; j < j_max; j++) {
            for (i = i_min; i < i_max; i++) {
                if (img[j][i] < min)
                    min = img[j][i];
            }
        }
        
/*         if ( min < display_act->img.img_mean) */
/*             display_act->img.img_mean = min; */


        pos = *rgbbuf;
        for (j = j_min; j < j_max; j++) {
            for (i = i_min; i < i_max; i++) {
/*                 *pos++ = (unsigned char) img[j][i] - (unsigned char) min */
/*                     + (unsigned char) display_act->img.img_mean; */
/*                 *pos++ = (unsigned char) img[j][i] - (unsigned char) min */
/*                     + (unsigned char) display_act->img.img_mean; */
/*                 *pos++ = (unsigned char) img[j][i] - (unsigned char) min */
/*                     + (unsigned char) display_act->img.img_mean; */
/*
 * BUGFIX: display image intars with: 8 < depth < 16
 * 6 March 2004
 */
	    *pos++ =  (guchar) ((img[j][i] - /* (guchar) */ min + 
                                /* (guchar) */ display_act->img->img_mean) 
                                / fact);
	    *pos++ =  (guchar) ((img[j][i] - /* (guchar) */ min + 
                                /* (guchar) */ display_act->img->img_mean) 
                                / fact);
	    *pos++ =  (guchar) ((img[j][i] - /* (guchar) */ min + 
                                /* (guchar) */ display_act->img->img_mean) 
                                / fact);
            }
        }
    }


    gdk_pixbuf_unref (pixbuf);
    *int_size_old = int_size;
/*     g_free(rgbbuf); */
/*     rgbbuf = NULL; */
}



static void
alloc_bufmem_per_intarea (int index_y, 
                         int index_x, 
                         PivData *pida, 
                         GpivImagePar image_par, 
                         int int_size_0
                         )
/* ----------------------------------------------------------------------------
 * Memory allocation of covariance in a packed interrogation area array
 */ 
{ 
}



static void
free_bufmem_per_intarea (int index_y, 
                         int index_x, 
                         PivData *pida, 
                         GpivImagePar image_par, 
                         int int_size_0
                         )
/*-----------------------------------------------------------------------------
 */
{ 
}



static void
adjust_radio_button_intsize_f (GpivPivPar *piv_par, 
                               PivEval *eval
                               )
/* ----------------------------------------------------------------------------
 * Adjust radio button interrogation size 1 in PIV interrogation tab
 */
{
    if (piv_par->int_size_f == 16) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_f_2), TRUE);   
    } else if (piv_par->int_size_f == 32) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_f_3), TRUE);
    } else if (piv_par->int_size_f == 64) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_f_4), TRUE);
    } else if (piv_par->int_size_f == 128) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_f_5), TRUE);
    }
}



static void
adjust_radio_button_intsize_i (GpivPivPar *piv_par, 
                               PivEval *eval
                               )
/* ----------------------------------------------------------------------------
 * Adjust radio button interrogation size 1 in PIV interrogation tab
 */
{
    if (piv_par->int_size_i == 16) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_i_2), TRUE);
    } else if (piv_par->int_size_i == 32) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_i_3), TRUE);
    } else if (piv_par->int_size_i == 64) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_i_4), TRUE);
    } else if (piv_par->int_size_i == 128) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_i_5), TRUE);
    }
}   




static void
adjust_radio_button_intshift (GpivPivPar *piv_par, 
                              PivEval *eval)
/* ----------------------------------------------------------------------------
 * Adjust radio button interrogation area shift in PIV interrogation tab
 */
{
    if (piv_par->int_shift == 8) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intshift_1), TRUE);
    } else if (piv_par->int_shift == 16) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intshift_2), TRUE);
    } else if (piv_par->int_shift == 32) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intshift_3), TRUE);
    } else if (piv_par->int_shift == 64) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intshift_4), TRUE);
    } else if (piv_par->int_shift == 128) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intshift_5), TRUE);
    }
}



/*
 * Display functions in PIV tabulator canvas
 */


void 
display_piv_vector (guint i, 
                    guint j, 
                    GpivPivData *piv_data, 
                    PivEval *eval
                    )
/* ----------------------------------------------------------------------------
 * Displays values in PIV tabulator of the GUI
 */
{
/*     float **dx = piv_data->dx, **dy = piv_data->dy; */
/*     int **peak_no = piv_data->peak_no; */
    GnomeCanvasPoints *points;
    float dl = sqrt (piv_data->dx[i][j] * piv_data->dx[i][j] + 
                     piv_data->dy[i][j] * piv_data->dy[i][j]);
    gchar *color;
    points = gnome_canvas_points_new (2);

    eval->monitor->pi_da->dx[0][0] = piv_data->dx[i][j];
    eval->monitor->pi_da->dy[0][0] = piv_data->dy[i][j];
    eval->monitor->pi_da->peak_no[0][0] = piv_data->peak_no[i][j];

    if (eval->monitor->pi_da->peak_no[0][0] == -1) {
        color="red";
    } else if (eval->monitor->pi_da->peak_no[0][0] == 0) {
        color="lightblue";
    } else if (eval->monitor->pi_da->peak_no[0][0] == 1) {
        color="green";
    } else if (eval->monitor->pi_da->peak_no[0][0] == 2) {
        color="yellow";
    } else  {
/* if (eval->monitor->pi_da->peak_no[i][j] == 3) */
            color="gray";
    }

    points->coords[0] = GPIV_MAX_INTERR_SIZE / 2;
    points->coords[1] = GPIV_MAX_INTERR_SIZE / 2;
    points->coords[2] = GPIV_MAX_INTERR_SIZE / 2 + eval->monitor->pi_da->dx[0][0]
        * gpiv_var->piv_disproc_vlength;
    points->coords[3] =  GPIV_MAX_INTERR_SIZE / 2 + eval->monitor->pi_da->dy[0][0]
        * gpiv_var->piv_disproc_vlength;


    if (eval->monitor->gci_vec != NULL) {
        gnome_canvas_item_set (eval->monitor->gci_vec,
			       "points", points,
			       "fill_color", color,
			       "width_units", (double) THICKNESS,
			       "first_arrowhead", FALSE,
			       "last_arrowhead", TRUE,
                               "arrow_shape_a", (double) ARROW_LENGTH * 
                               ARROW_FACT *dl * gpiv_par->display__vector_scale + 
                               ARROW_ADD,
                               "arrow_shape_b", (double) ARROW_EDGE *
                               ARROW_FACT * dl * gpiv_par->display__vector_scale + 
                               ARROW_ADD,
                               "arrow_shape_c", (double) ARROW_WIDTH * 
                               ARROW_FACT * dl * gpiv_par->display__vector_scale + 
                               ARROW_ADD,
 			       NULL);
    } else {
        eval->monitor->gci_vec =
            gnome_canvas_item_new (gnome_canvas_root
                                   (GNOME_CANVAS(eval->canvas_monitor_vec)),
                                   gnome_canvas_line_get_type(), 
                                   "points", points, 
                                   "fill_color", color, 
                                   "width_units",(double) THICKNESS, 
                                   "first_arrowhead", TRUE,
                                   "arrow_shape_a", (double) ARROW_LENGTH * 
                                   ARROW_FACT *dl * gpiv_par->display__vector_scale + 
                                   ARROW_ADD,
                                   "arrow_shape_b", (double) ARROW_EDGE *
                                   ARROW_FACT * dl * gpiv_par->display__vector_scale + 
                                   ARROW_ADD,
                                   "arrow_shape_c", (double) ARROW_WIDTH * 
                                   ARROW_FACT * dl * gpiv_par->display__vector_scale + 
                                   ARROW_ADD,
 
/* 			      "arrow_shape_a", (double) THICKNESS,  */
			      NULL);

    }

    gnome_canvas_points_free (points);
}



void 
display_img_intreg1 (float **intreg1, 
                     guint int_size,
                     PivEval *eval
                     )
/* ----------------------------------------------------------------------------
 * Displays image of intreg1 for drawing area
 * row stride; each row is a 4-byte buffer array
 */
{
    display_img (intreg1, 
                 int_size,
                 &eval->monitor->int_size_old,
                 0, 
                 int_size, 
                 0, 
                 int_size, 
                 eval->monitor->rgb_int_width,
                 &eval->monitor->rgbbuf_int1,
                 eval->canvas_monitor_int1,
                 &eval->monitor->gci_int1,
                 eval->monitor->affine,
                 FALSE);
}



void display_img_intreg2 (float **intreg2, 
                          guint int_size,
                          PivEval *eval
                          )
/* ----------------------------------------------------------------------------
 * Displays image of intreg2 for drawing area
 */
{
    display_img (intreg2, 
                 int_size,
                 &eval->monitor->int_size_old,
                 0, 
                 int_size, 
                 0, 
                 int_size, 
                 eval->monitor->rgb_int_width,
                 &eval->monitor->rgbbuf_int2,
                 eval->canvas_monitor_int2,
                 &eval->monitor->gci_int2,
                 eval->monitor->affine,
                 FALSE);

}



void display_img_cov (GpivCov *cov, 
                      guint int_size, 
                      PivEval *eval
                      )
/* ----------------------------------------------------------------------------
 * Displays image of intreg1 for drawing area
 */
{
    display_img (cov->z, 
                 int_size,
                 &eval->monitor->cov_size_old,
                 cov->z_cl, 
                 cov->z_ch, 
                 cov->z_rl, 
                 cov->z_rh, 
                 eval->monitor->rgb_cov_width,
                 &eval->monitor->rgbbuf_cov,
                 eval->canvas_monitor_cov,
                 &eval->monitor->gci_cov,
                 eval->monitor->affine,
                 TRUE);
}



/*
 * Piv evaluation window/tabulator callbacks
 */


void 
on_radiobutton_piv_mouse (GtkWidget *widget, 
                          gpointer data
                          )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(widget), "gpiv");
    m_select = atoi(gtk_object_get_data(GTK_OBJECT(widget),
					"mouse_select"));

/*     g_message ("on_radiobutton_piv_mouse:: 0 m_select = %d", m_select); */
    if (m_select == NO_MS) {
	gl_piv_par->int_geo = GPIV_AOI;

    } else if  (m_select == AOI_MS) {
	gl_piv_par->int_geo = GPIV_AOI;
    } else if (m_select == SINGLE_POINT_MS) {
	gl_piv_par->int_geo = GPIV_POINT;
    } else if (m_select == V_LINE_MS) {
/* 	gl_piv_par->int_geo =  GPIV_LINE_C; */
	gl_piv_par->int_geo =  GPIV_AOI;
    } else if (m_select == H_LINE_MS) {
/* 	gl_piv_par->int_geo =  GPIV_LINE_R; */
	gl_piv_par->int_geo =  GPIV_AOI;
    } else if (m_select == SINGLE_AREA_MS ||
               m_select == DRAG_MS) {
	gl_piv_par->int_geo = GPIV_POINT;
    }

/*
 * (Re)setting interrogation scheme if necessary
 */
    if ((m_select == SINGLE_AREA_MS 
         || m_select == DRAG_MS
         || m_select == SINGLE_POINT_MS
         || m_select == V_LINE_MS
         || m_select == H_LINE_MS)
        && gl_piv_par->int_scheme == GPIV_IMG_DEFORM
        ) {
        char message[2 * GPIV_MAX_CHARS];
        g_snprintf (message, 2 * GPIV_MAX_CHARS, 
                    _("Image deformation is impossibe with this Mouse select.\n\
Setting Interrogation scheme to Central difference.\n\
This will be reset automatically after interrogating."));
        warning_gpiv (message);
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->piveval->radiobutton_centraldiff), 
                                     TRUE);
        int_scheme_autochanged = TRUE;
    } else {
        if (int_scheme_autochanged) {
            gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                         (gpiv->piveval->radiobutton_imgdeform), 
                                         TRUE);
            int_scheme_autochanged = FALSE;
        }
    }
}



void 
on_radiobutton_piv_mouse1_enter (GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("No mouse activity within displayer");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_radiobutton_piv_mouse2_enter (GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Selects an area within the image to be analyzed");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_radiobutton_piv_mouse3_enter (GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Piv evaluation at a single interrogation area. "
                   "Conserves other existing data");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_radiobutton_piv_mouse4_enter (GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Piv evaluation at a single point in the image. "
                   "Rejects all existing data!!");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_radiobutton_piv_mouse5_enter (GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Displaces a single interrogation area and analyzes");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_radiobutton_piv_mouse6_enter (GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Evaluation at a vertical line. ");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_radiobutton_piv_mouse7_enter (GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Evaluation at a horizontal line. ");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_spinbutton_piv_int (GtkSpinButton *widget, 
                       GtkWidget *entry
                       )
/*-----------------------------------------------------------------------------
 */
{
    char *err_msg = NULL;
    PivEval *eval = gtk_object_get_data(GTK_OBJECT (widget), "eval");
    guint nx = 0, ny = 0;
    guint nx_img = 0, ny_img = 0;
    gboolean renew = TRUE, lo_display_intregs = FALSE;

    enum VariableType {
	COL_START = 0,
	COL_END = 1,
	PRESHIFT_COL = 2,
	ROWSTART = 3,
	ROWEND = 4,
	PRESHIFT_ROW = 5,
	INT_SIZE_F = 6,
	INT_SIZE_I = 7,
	INT_SHIFT = 8
    } var_type;


/*     eval = gtk_object_get_data(GTK_OBJECT(widget), "eval"); */
/*
 * Locale vars
 */
/*     Display display_act; */
/*     display_act = gtk_object_get_data(widget, display_act); */

/*
 * Select which variable has to be modified
 */
    setby_spinbutton = 1;
    var_type = atoi(gtk_object_get_data (GTK_OBJECT (widget), "var_type"));

    if (var_type == COL_START) {
        gl_piv_par->col_start = gtk_spin_button_get_value_as_int (widget);

    } else if (var_type == COL_END) {
        gl_piv_par->col_end = gtk_spin_button_get_value_as_int (widget);

    } else if (var_type == PRESHIFT_COL) {
        gl_piv_par->pre_shift_col = gtk_spin_button_get_value_as_int (widget);

    } else if (var_type == ROWSTART) {
        gl_piv_par->row_start = gtk_spin_button_get_value_as_int (widget);

    } else if (var_type == ROWEND) {
	gl_piv_par->row_end = gtk_spin_button_get_value_as_int (widget);

    } else if (var_type == PRESHIFT_ROW) {
	gl_piv_par->pre_shift_row = gtk_spin_button_get_value_as_int (widget);

    } else if (var_type == INT_SIZE_F) {
	gl_piv_par->int_size_f = gtk_spin_button_get_value_as_int (widget);

/*
 * Adapt spinbutton of int_size_i
 */
        GTK_ADJUSTMENT(eval->spinbutton_adj_intsize_i)->lower = 
            (gfloat) gl_piv_par->int_size_f;
        GTK_ADJUSTMENT(eval->spinbutton_adj_intsize_i)->upper = 
            (gfloat) 128.0;
        gtk_adjustment_changed (GTK_ADJUSTMENT(eval->spinbutton_adj_intsize_i));
        gtk_adjustment_set_value (GTK_ADJUSTMENT(eval->spinbutton_adj_intsize_i),
                                 (gfloat) gl_piv_par->int_size_f);

/*
 * Adjust radio buttons for final interrogation size
 */
        adjust_radio_button_intsize_f (gl_piv_par, eval);

    } else if (var_type == INT_SIZE_I) {
        char message[GPIV_MAX_CHARS];

	gl_piv_par->int_size_i = gtk_spin_button_get_value_as_int (widget);
        if ((gl_piv_par->int_scheme == GPIV_NO_CORR
             || gl_piv_par->int_scheme == GPIV_LK_WEIGHT)
            && gl_piv_par->int_size_i >  gl_piv_par->int_size_f) {
            gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                        (eval->radiobutton_imgdeform),
                                        TRUE);
            g_snprintf (message, GPIV_MAX_CHARS, 
                       _("Int Size 2 > Int Size 1 \nSetting Interrogation scheme to Image deformation"));
            warning_gpiv(message);

/*         } else { */
/*             gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON */
/*                                         (eval->radiobutton_zerooff),  */
/*                                         FALSE); */
        }

/*
 * Adjust radio buttons for initial interrogation size
 */
        adjust_radio_button_intsize_i (gl_piv_par, eval);

    } else if (var_type == INT_SHIFT) {
	gl_piv_par->int_shift = gtk_spin_button_get_value_as_int (widget);

/*
 * Adjust radio buttons for interrogation area shift
 */
        adjust_radio_button_intshift (gl_piv_par, eval);
    }
    
/*
 * Update eventually interrogation region contours.
 */
    if (display_act != NULL) {
        if ((err_msg = 
             gpiv_piv_testadjust_parameters (display_act->img->image->header, 
                                       gl_piv_par))
            != NULL) {
            warning_gpiv ("%s", err_msg);
            return;
        }

        display_act->intreg->par->row_start = gl_piv_par->row_start;
        display_act->intreg->par->row_end = gl_piv_par->row_end;
        display_act->intreg->par->col_start = gl_piv_par->col_start;
        display_act->intreg->par->col_end = gl_piv_par->col_end;
        display_act->intreg->par->int_shift =  gl_piv_par->int_shift;
        display_act->intreg->par->int_size_f =  gl_piv_par->int_size_f;
        display_act->intreg->par->int_size_i =  gl_piv_par->int_size_i;
        display_act->intreg->par->pre_shift_row = gl_piv_par->pre_shift_row;
        display_act->intreg->par->pre_shift_col = gl_piv_par->pre_shift_col;

        display_act->intreg->par->row_start__set = TRUE;
        display_act->intreg->par->row_end__set = TRUE;
        display_act->intreg->par->col_start__set = TRUE;
        display_act->intreg->par->col_end__set = TRUE;
        display_act->intreg->par->int_size_f__set = TRUE;
        display_act->intreg->par->int_size_i__set = TRUE;
        display_act->intreg->par->int_shift__set = TRUE;
        display_act->intreg->par->pre_shift_row__set = TRUE;
        display_act->intreg->par->pre_shift_col__set = TRUE;

        if (display_act->intreg->exist_int) {
            nx = display_act->intreg->data->nx;
            ny = display_act->intreg->data->ny;

            if ((err_msg = 
                 gpiv_piv_count_pivdata_fromimage (display_act->img->image->header, 
                                                   display_act->intreg->par,
                                                   &nx_img, 
                                                   &ny_img))
                != NULL) {
                warning_gpiv ("on_spinbutton_piv_int: %s", err_msg);
                g_warning ("on_spinbutton_piv_int: %s", err_msg);
                return;
            }
/*
 * For some operations the intregs have to be renewed unconditionally,
 * for others only in case if the number of intregs has been changed
 */
            if (var_type == COL_START || var_type == COL_END
                || var_type == ROWSTART || var_type == ROWEND) {
                if (nx == nx_img 
                    && ny == ny_img) {
                    renew = FALSE;
                } else {
                    renew = TRUE;
                }
            }
        
            if (renew) {
                lo_display_intregs = display_act->display_intregs;
                destroy_all_intregs (display_act);
                create_all_intregs (display_act);
                display_act->display_intregs = lo_display_intregs;
                if (display_act->display_intregs) show_all_intregs(display_act);
            }

        }
    }

    setby_spinbutton = 0;
}



void 
on_radiobutton_piv_int (GtkWidget *widget, 
                        gpointer data
                        )
/*-----------------------------------------------------------------------------
 */
{
    gint int_size_f_tmp;
    gboolean lo_display_intregs = FALSE;
    gint nx_tmp = 0, ny_tmp = 0;
    /*     gint setby_spinbutton = atoi(gtk_object_get_data(GTK_OBJECT(widget),  */
    /*                                                 "setby_spinbutton")); */
    PivEval *eval = gtk_object_get_data(GTK_OBJECT (widget), "eval");
    Display *disp = display_act;
    int nx, ny;

    enum VariableType {
	SIZE_F = 0,
	SIZE_I = 1,
	SHIFT = 2
    } var_type;


    if (disp != NULL
        && disp->intreg->exist_int) {
        nx = disp->intreg->data->nx;
        ny = disp->intreg->data->ny;
    } else {
        nx = 0;
        ny = 0;
    }

    if (GTK_TOGGLE_BUTTON(widget)->active) {
	var_type = atoi(gtk_object_get_data (GTK_OBJECT(widget),
                                             "var_type"));

/*
 * Select which variable has to be modified
 */
	if (var_type == SIZE_F) {
            int_size_f_tmp = gl_piv_par->int_size_f;
	    gl_piv_par->int_size_f = atoi (gtk_object_get_data
                                           (GTK_OBJECT(widget),
                                            "intsize_f"));
/*
 * switch off temporarly display_intregs
 */
            if (disp != NULL) {
                lo_display_intregs = disp->display_intregs;
                disp->display_intregs = FALSE;
            }
	    nx_tmp = nx;
	    ny_tmp = ny;
	    adjust_radiobutton_piv_int (eval, gl_piv_par->int_size_f);

/*
 * return display_intregs to original value
 */
            if (disp != NULL) {
                disp->display_intregs = lo_display_intregs;
            }
	    nx = nx_tmp;
	    ny = ny_tmp;
/*
 * BUGFIX: REMOVE Reset weight and zero_off
 */
            if (gl_piv_par->int_size_i > int_size_f_tmp) {
/*             gtk_widget_set_sensitive(eval->radiobutton_zerooff, TRUE); */
/*             gtk_widget_set_sensitive(checkbutton_piv_weightkernel, TRUE); */

/*
                if (int_scheme_tmp != GPIV_ZERO_OFF_FORWARD
                    && int_scheme_tmp != GPIV_ZERO_OFF_CENTRAL) {
                    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                                (eval->radiobutton_zerooff), 
                                                FALSE);
                    gtk_widget_set_sensitive(eval->radiobutton_zerooff, TRUE);
                    gtk_widget_set_sensitive(eval->radiobutton_weightkernel, 
                                             TRUE);
                    if (int_scheme_tmp == GPIV_LK_WEIGHT) {
                        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                                    (eval->radiobutton_weightkernel), 
                                                    FALSE);
                        gtk_widget_set_sensitive(eval->radiobutton_zerooff, 
                                                 TRUE);
                        gtk_widget_set_sensitive(eval->radiobutton_weightkernel, TRUE);
                    } else {
                        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                                    (eval->radiobutton_weightkernel), 
                                                    TRUE);
                        gtk_widget_set_sensitive(eval->radiobutton_zerooff, 
                                                 FALSE);
                        gtk_widget_set_sensitive(eval->radiobutton_weightkernel, 
                                                 TRUE);
                    }
                } else {
                    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                                (eval->radiobutton_zerooff), 
                                                TRUE);
                    gtk_widget_set_sensitive(eval->radiobutton_zerooff, TRUE);
                    gtk_widget_set_sensitive(eval->radiobutton_weightkernel, 
                                             FALSE);
                }
*/
            }

/*
 * Also calls on_spinbutton_piv_int, which, on its turn, may call this function
 * again resulting into an infinite loop
*/
            gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                       (eval->spinbutton_intsize_f), 
                                       gl_piv_par->int_size_f);


	} else if (var_type == SIZE_I) {
	    gl_piv_par->int_size_i = 
                atoi (gtk_object_get_data (GTK_OBJECT(widget), "intsize_i"));

/*
 * Adjust spinner
 */
            gtk_adjustment_set_value (GTK_ADJUSTMENT
                                      (eval->spinbutton_adj_intsize_i),
                                      (gfloat) gl_piv_par->int_size_i);
/*
 * Save origanal settings of weight and zero_off for later resetting
 */
            if (gl_piv_par->int_size_i >  gl_piv_par->int_size_f) {
                int_scheme_tmp = gl_piv_par->int_scheme;
/*
                gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                            (eval->radiobutton_weightkernel), FALSE);
                gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                            (eval->radiobutton_zerooff), TRUE);
                gtk_widget_set_sensitive(eval->radiobutton_zerooff, FALSE);
*/

/*
 * Reset weight and zero_off
 */
            } else {
/*                if (zero_off_tmp == 0) {
                    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                                (eval->radiobutton_zerooff), FALSE);
                    if (weight_tmp == 0) {
                        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                                    (eval->radiobutton_weightkernel), FALSE);
                    } else {
                        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                                    (eval->radiobutton_weightkernel), TRUE);
                    }
                } else {
                    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                                (eval->radiobutton_zerooff), TRUE);
                }
	    gtk_widget_set_sensitive(eval->radiobutton_zerooff, TRUE);
*/
            }


	} else if (var_type == SHIFT) {
	    gl_piv_par->int_shift = 
                atoi (gtk_object_get_data (GTK_OBJECT(widget),
                                           "intshift"));
/*
 * Adjust spinner
 */
            gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                       (eval->spinbutton_intshift), 
                                       gl_piv_par->int_shift);
	}

	if (disp != NULL 
            && disp->intreg->exist_int
            ) {
            lo_display_intregs = disp->display_intregs;
            destroy_all_intregs (disp);
            create_all_intregs (disp);
            disp->display_intregs = lo_display_intregs;
            if (disp->display_intregs) show_all_intregs(disp);
        }

    }


}



void 
on_radiobutton_fit_enter (GtkWidget *widget, 
                          gpointer data
                          )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Interpolation scheme for sub-pixel estimation");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);

}



void 
on_radiobutton_peak_enter (GtkWidget *widget, 
                           gpointer data
                           )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Chooses n-th highest top number of correlation as estimator");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);

}



void 
on_radiobutton_interrogatescheme_enter (GtkWidget *widget, 
                                        gpointer data
                                        )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Bias correction scheme");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);

}



void 
on_radiobutton_interrogatescheme_imgdeform_enter (GtkWidget *widget, 
                                                  gpointer data
                                                  )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT(widget), "gpiv");
    gchar *msg = _("Deforms image from PIV estimators");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);

}



void 
on_toggle_piv (GtkWidget *widget, 
               gpointer data
               )
/*-----------------------------------------------------------------------------
 */
{
    PivEval *eval = gtk_object_get_data(GTK_OBJECT (widget), "eval");
    enum VariableType {
	IFIT,
	PEAK_NR,
	SCHEME,
	CORRELATION
    } var_type;

/*
 * Select which variable has to be modified
 */
    var_type = atoi (gtk_object_get_data (GTK_OBJECT(widget), "var_type"));

    if (var_type == IFIT) {
	gl_piv_par->ifit = atoi (gtk_object_get_data (GTK_OBJECT(widget), 
                                                      "ifit"));

    } else if (var_type == PEAK_NR) {
	gl_piv_par->peak = atoi (gtk_object_get_data (GTK_OBJECT(widget), 
                                                      "peak"));


    } else if (var_type == SCHEME) {
        gl_piv_par->int_scheme = atoi(gtk_object_get_data(GTK_OBJECT(widget), 
                                                          "scheme"));

    } else if (var_type == CORRELATION) {
	gl_image_par->x_corr = atoi (gtk_object_get_data (GTK_OBJECT(widget), 
                                                         "x_corr"));
    }

}



void 
on_checkbutton_weight_ia_enter (GtkWidget *widget, 
                           gpointer data
                           )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT (widget), "gpiv");
    gchar *msg =
	_("Gauss weighting on the Interrogation Area");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_checkbutton_weight_ia (GtkWidget *widget, 
                         gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    PivEval *eval = gtk_object_get_data (GTK_OBJECT(widget), "eval");


    if (GTK_TOGGLE_BUTTON(widget)->active) {
	gl_piv_par->gauss_weight_ia = TRUE;
    } else {
	gl_piv_par->gauss_weight_ia = FALSE;
    }
}


void 
on_checkbutton_spof_enter (GtkWidget *widget, 
                           gpointer data
                           )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT (widget), "gpiv");
    gchar *msg =
	_("Symmetric Phase Only Filtering on the FT images");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_checkbutton_spof (GtkWidget *widget, 
                         gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    PivEval *eval = gtk_object_get_data (GTK_OBJECT(widget), "eval");


    if (GTK_TOGGLE_BUTTON(widget)->active) {
	gl_piv_par->spof_filter = TRUE;
    } else {
	gl_piv_par->spof_filter = FALSE;
    }
}


void 
adjust_radiobutton_piv_int (PivEval *eval,
                           guint int_size_f
                           )
/* ----------------------------------------------------------------------------
 * adjusting and disabling int_size_i and its toggle buttons
 */
{
/*     if (int_size_f == 8) { */
/* 	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON */
/* 				    (eval->radiobutton_intsize_i_1), TRUE); */
    /* } else  */

    if (int_size_f == 16) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_i_2), TRUE);

    } else if (int_size_f == 32) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_i_3), TRUE);

    } else if (int_size_f == 64) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_i_4), TRUE);

    } else if (int_size_f >= 128) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_intsize_i_5), TRUE);
    }


    if (int_size_f <= 64) {
	gtk_widget_set_sensitive (GTK_WIDGET(eval->radiobutton_intsize_i_4),
                                  TRUE);
	if (int_size_f <= 32) {
	    gtk_widget_set_sensitive (GTK_WIDGET
                                      (eval->radiobutton_intsize_i_3), 
                                      TRUE);
	    if (int_size_f <= 16) {
		gtk_widget_set_sensitive (GTK_WIDGET
                                          (eval->radiobutton_intsize_i_2),
                                          TRUE);
/* 		if (int_size_f <= 8) { */
/* 		    gtk_widget_set_sensitive(GTK_WIDGET */
/* 					     (eval->radiobutton_intsize_i_1), */
/* 					     TRUE); */
/* 		} */
	    }
	}
    }



    if (int_size_f >= 16) {
/* 	gtk_widget_set_sensitive(GTK_WIDGET(eval->radiobutton_intsize_i_1), */
/* 				 FALSE); */
	if (int_size_f >= 32) {
	    gtk_widget_set_sensitive (GTK_WIDGET
                                      (eval->radiobutton_intsize_i_2), 
                                      FALSE);
	    if (int_size_f >= 64) {
		gtk_widget_set_sensitive (GTK_WIDGET
                                          (eval->radiobutton_intsize_i_3),
                                          FALSE);
		if (int_size_f >= 128) {
		    gtk_widget_set_sensitive (GTK_WIDGET
                                              (eval->radiobutton_intsize_i_4),
                                              FALSE);
		}
	    }
	}
    }

}



void 
on_checkbutton_piv_monitor_enter (GtkWidget *widget, 
                                  gpointer data
                                  )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT (widget), "gpiv");
    gchar *msg =
	_("Displays subimages, correlation function and PIV vector");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_checkbutton_piv_monitor (GtkWidget *widget, 
                            gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    PivEval *eval = gtk_object_get_data (GTK_OBJECT(widget), "eval");


    if (GTK_TOGGLE_BUTTON(widget)->active) {
	gpiv_var->piv_disproc = TRUE;

        eval->monitor->gci_vec_background = 
            gnome_canvas_item_new (gnome_canvas_root
                                   (GNOME_CANVAS(eval->canvas_monitor_vec)),
                                   gnome_canvas_rect_get_type(),
                                   "x1", (double) 0,
                                   "y1", (double) 0,
                                   "x2", (double) GPIV_MAX_INTERR_SIZE,
                                   "y2", (double) GPIV_MAX_INTERR_SIZE,
                                   "fill_color", "darkblue",
                                   "width_units", 1.0, 
                                   NULL);


        eval->monitor->gci_int1_background = 
            gnome_canvas_item_new (gnome_canvas_root
                                   (GNOME_CANVAS(eval->canvas_monitor_int1)),
                                   gnome_canvas_rect_get_type(),
                                   "x1", (double) 0,
                                   "y1", (double) 0,
                                   "x2", (double) GPIV_MAX_INTERR_SIZE,
                                   "y2", (double) GPIV_MAX_INTERR_SIZE,
                                   "fill_color", "darkgreen",
                                   "width_units", 1.0, 
                                   NULL);


        eval->monitor->gci_int2_background = 
            gnome_canvas_item_new (gnome_canvas_root
                                   (GNOME_CANVAS(eval->canvas_monitor_int2)),
                                   gnome_canvas_rect_get_type(),
                                   "x1", (double) 0,
                                   "y1", (double) 0,
                                   "x2", (double) GPIV_MAX_INTERR_SIZE,
                                   "y2", (double) GPIV_MAX_INTERR_SIZE,
                                   "fill_color", "darkgreen",
                                   "width_units", 1.0, 
                                   NULL);


        eval->monitor->gci_background_cov = 
            gnome_canvas_item_new (gnome_canvas_root
                                   (GNOME_CANVAS(eval->canvas_monitor_cov)),
                                   gnome_canvas_rect_get_type(),
                                   "x1", (double) 0,
                                   "y1", (double) 0,
                                   "x2", (double) GPIV_MAX_INTERR_SIZE,
                                   "y2", (double) GPIV_MAX_INTERR_SIZE,
                                   "fill_color", "darkred",
                                   "width_units", 1.0, 
                                   NULL);
    } else {
	gpiv_var->piv_disproc = FALSE;
/*
 * Destroy intreg1 image
 */
        if (eval->monitor->gci_int1_background != NULL) {
            gtk_object_destroy (GTK_OBJECT(eval->monitor->
                                          gci_int1_background));
            eval->monitor->gci_int1_background = NULL;
        }

        if (eval->monitor->rgbbuf_int1 != NULL) {
            g_free (eval->monitor->rgbbuf_int1);
            eval->monitor->rgbbuf_int1 = NULL;
        }

        if (eval->monitor->gci_int1 != NULL) {
            gtk_object_destroy (GTK_OBJECT(eval->monitor->gci_int1));
            eval->monitor->gci_int1 = NULL;
        }

/*
 * Destroy intreg2 image
 */
        if (eval->monitor->gci_int2_background != NULL) {
            gtk_object_destroy (GTK_OBJECT(eval->monitor->
                                          gci_int2_background));
            eval->monitor->gci_int2_background = NULL;
        }

        if (eval->monitor->rgbbuf_int2 != NULL) {
            g_free (eval->monitor->rgbbuf_int2);
            eval->monitor->rgbbuf_int2 = NULL;
        }

        if (eval->monitor->gci_int2 != NULL) {
            gtk_object_destroy (GTK_OBJECT(eval->monitor->gci_int2));
            eval->monitor->gci_int2 = NULL;
        }

/*
 * Destroy correlation image
 */
        if (eval->monitor->gci_background_cov != NULL) {
            gtk_object_destroy (GTK_OBJECT(eval->monitor->gci_background_cov));
            eval->monitor->gci_background_cov = NULL;
        }

        if (eval->monitor->rgbbuf_cov != NULL) {
            g_free (eval->monitor->rgbbuf_cov);
            eval->monitor->rgbbuf_cov = NULL;
        }

        if (eval->monitor->gci_cov != NULL) {
            gtk_object_destroy (GTK_OBJECT(eval->monitor->gci_cov));
            eval->monitor->gci_cov = NULL;
        }

/*
 * Destroy vector
 */
        if (eval->monitor->gci_vec_background != NULL) {
            gtk_object_destroy (GTK_OBJECT(eval->monitor->gci_vec_background));
            eval->monitor->gci_vec_background = NULL;
        }


        if (eval->monitor->gci_vec != NULL) {
            gtk_object_destroy (GTK_OBJECT(eval->monitor->gci_vec));
            eval->monitor->gci_vec = NULL;
        }
    }

}



void
on_spinbutton_piv_monitor_zoom (GtkSpinButton *widget, 
                                gpointer data
                                )
/*-----------------------------------------------------------------------------
 */
{
    PivEval *eval = gtk_object_get_data (GTK_OBJECT(widget), "eval");
    /* gfloat piv_disproc_zoom */

    gpiv_var->piv_disproc_zoom = gtk_spin_button_get_value_as_float (widget);
    gnome_config_push_prefix("/gpiv/RuntimeVariables/");
    gnome_config_set_float ("zoom_factor", gpiv_var->piv_disproc_zoom);
    gnome_config_pop_prefix ();
    gnome_config_sync();

    eval->monitor->affine[0] = gpiv_var->piv_disproc_zoom;
    eval->monitor->affine[3] = gpiv_var->piv_disproc_zoom;
    eval->monitor->affine[4] = 
        (GPIV_MAX_INTERR_SIZE - gpiv_var->piv_disproc_zoom 
         * gl_piv_par->int_size_f) /2.0;
    eval->monitor->affine[5] = 
        (GPIV_MAX_INTERR_SIZE - gpiv_var->piv_disproc_zoom 
         * gl_piv_par->int_size_f) /2.0;

    if(eval->monitor->gci_int1 != NULL) {
        gnome_canvas_item_affine_absolute (eval->monitor->gci_int1,
                                           eval->monitor->affine);
    }

    if(eval->monitor->gci_int2 != NULL) {
        gnome_canvas_item_affine_absolute (eval->monitor->gci_int2,
                                           eval->monitor->affine);
    }

 
    if(eval->monitor->gci_cov != NULL) {
        gnome_canvas_item_affine_absolute (eval->monitor->gci_cov,
                                           eval->monitor->affine);
    }

 
}



void
on_spinbutton_piv_monitor_vectorscale (GtkSpinButton *widget, 
                                       gpointer data
                                       )
/*-----------------------------------------------------------------------------
 */
{
    PivEval *eval = gtk_object_get_data (GTK_OBJECT (widget), "eval");
    /* gint piv_disproc_vlength */

    gpiv_var->piv_disproc_vlength = gtk_spin_button_get_value_as_int (widget);
    gnome_config_set_int ("vector_length", gpiv_var->piv_disproc_vlength);
    gnome_config_sync ();

/*
 * Try to re-scale vector
 */
    if(eval->monitor->gci_vec != NULL) {
        GnomeCanvasPoints *points = gnome_canvas_points_new (2);
        
        points->coords[0] = GPIV_MAX_INTERR_SIZE / 2;
        points->coords[1] = GPIV_MAX_INTERR_SIZE / 2;
        points->coords[2] = GPIV_MAX_INTERR_SIZE / 2 + eval->monitor->pi_da->dx[0][0] * 
            gpiv_var->piv_disproc_vlength;
        points->coords[3] =  GPIV_MAX_INTERR_SIZE / 2 + eval->monitor->pi_da->dy[0][0] * 
            gpiv_var->piv_disproc_vlength;
        
        gnome_canvas_item_set (eval->monitor->gci_vec,
                               "points", points,
                               NULL);
        
        gnome_canvas_points_free (points);
    }
}



void 
on_button_piv_enter (GtkWidget *widget, 
                     gpointer data
                     )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Analyses a PIV image (pair)");
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), msg);
}



void 
on_button_piv (GtkWidget *widget, 
               gpointer data
               )
/* ----------------------------------------------------------------------------
 * The actual calculation of particle image displacements
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;

    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                  row);
            ibuf = display_act->id;
            if (display[ibuf] != NULL 
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin))) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }
            
            exec_piv (gpiv);
        }
    }
}



