/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (callback) functions for Piv evaluation window/tabulator
 * $Log: piveval.h,v $
 * Revision 1.11  2008-04-28 12:00:57  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.10  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.9  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.8  2007-02-16 17:09:48  gerber
 * added Gauss weighting on I.A. and SPOF filtering (on correlation function)
 *
 * Revision 1.7  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.6  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.5  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.4  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.3  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.2  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef PIVEVAL_H
#define PIVEVAL_H

gint int_scheme_tmp, zero_off_tmp, weight_tmp; /* used in on_radiobutton_piv_int */
gint setby_spinbutton; /* used in on_spinbutton_piv_int and on_radiobutton_piv_int */ 
gboolean int_scheme_autochanged;


/*
 * Public piv evaluation functions
 */

void 
interrogate_ORG (GpivPivData *piv_data,	        /* output piv data from image analysis */
                 guint16 **img_1,	        /* raw (binary) image data of first piv image  */
                 guint16 **img_2,	        /* raw (binary) image data of second piv image  */
                 GpivConsole *gpiv);            /* console widgets structure */

void 
display_piv_vector (guint i, 
                    guint j, 
                    GpivPivData *piv_data,
                    PivEval *piveval);

void 
display_img_intreg1 (float **intreg1, 
                     guint int_size,
                     PivEval *piveval);

void 
display_img_intreg2 (float **intreg2, 
                     guint int_size,
                     PivEval *piveval);

void 
display_img_cov (GpivCov *cov, 
                 guint int_size,
                 PivEval *piveval);


/*
 * Piv evaluation window/tabulator callbacks
 */

void
on_radiobutton_piv_mouse (GtkWidget *widget, 
                          gpointer data);

void 
on_radiobutton_piv_mouse1_enter (GtkWidget *widget, 
                                 gpointer data);
void 
on_radiobutton_piv_mouse2_enter (GtkWidget *widget, 
                                 gpointer data);
void 
on_radiobutton_piv_mouse3_enter (GtkWidget *widget, 
                                 gpointer data);
void 
on_radiobutton_piv_mouse4_enter (GtkWidget *widget, 
                                 gpointer data);
void 
on_radiobutton_piv_mouse5_enter (GtkWidget *widget, 
                                 gpointer data);
void 
on_radiobutton_piv_mouse6_enter (GtkWidget *widget, 
                                 gpointer data);
void 
on_radiobutton_piv_mouse7_enter (GtkWidget *widget, 
                                 gpointer data);


/*
 * entries for first, last, and pre-shift columns and rows
 */

void
on_spinbutton_piv_int (GtkSpinButton *widget, 
                       GtkWidget *entry);


/*
 * radio buttons of first, second interrogation sizes and shift (adjacent 
 * distances
 */

void
on_radiobutton_piv_int (GtkWidget *widget, 
                        gpointer data);

void 
on_radiobutton_fit_enter (GtkWidget *widget, 
                          gpointer data);

void 
on_radiobutton_peak_enter (GtkWidget *widget, 
                           gpointer data);

void 
on_radiobutton_interrogatescheme_enter (GtkWidget *widget, 
                                        gpointer data);

void 
on_radiobutton_interrogatescheme_imgdeform_enter (GtkWidget *widget, 
                                                  gpointer data);

void
on_toggle_piv (GtkWidget *widget, 
               gpointer data);

void 
on_checkbutton_weight_ia_enter (GtkWidget *widget, 
                                gpointer data);
void 
on_checkbutton_weight_ia (GtkWidget *widget, 
                          gpointer data);

void 
on_checkbutton_spof_enter (GtkWidget *widget, 
                           gpointer data);
void 
on_checkbutton_spof (GtkWidget *widget, 
                     gpointer data);

void
on_button_piv (GtkWidget *widget, 
               gpointer data);

void
on_toolbar_chackbutton_piv (GtkWidget *widget, 
                            gpointer data);

/*
 * showing interrogation area's, covariance function and 
 * displacement vector
 */
gboolean
on_darea_piv_monitor_int1_expose (GtkWidget *widget,
                                  GdkEventExpose *event,
                                  gpointer user_data);

gboolean
on_darea_piv_monitor_int2_expose (GtkWidget *widget,
                                  GdkEventExpose *event,
                                  gpointer user_data);
gboolean
on_darea_piv_monitor_cov_expose (GtkWidget *widget,
                                 GdkEventExpose *event,
                                 gpointer user_data);

void
on_checkbutton_piv_monitor_enter (GtkWidget *widget, 
                                  gpointer data);

void
on_checkbutton_piv_monitor (GtkWidget *widget, 
                            gpointer data);

void
on_spinbutton_piv_monitor_zoom (GtkSpinButton *widget, 
                                gpointer data);

void
on_spinbutton_piv_monitor_vectorscale (GtkSpinButton *widget, 
                                       gpointer data);

void 
on_button_piv_enter (GtkWidget *widget, 
                     gpointer data);

void
adjust_radiobutton_piv_int (PivEval *piveval,
                            guint int_size_f);


#endif /* PIVEVAL_H */
