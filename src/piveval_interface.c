/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * PIV evaluation interface
 * $Log: piveval_interface.c,v $
 * Revision 1.16  2008-04-28 12:00:57  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.15  2007-11-23 16:24:07  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.14  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.13  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.12  2007-02-16 17:09:49  gerber
 * added Gauss weighting on I.A. and SPOF filtering (on correlation function)
 *
 * Revision 1.11  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.10  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.8  2005/02/12 14:12:12  gerber
 * Changed tabular names and titles
 *
 * Revision 1.7  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.6  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.5  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.4  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.3  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.2  2003/07/25 15:40:24  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gpiv_gui.h"
#include "utils.h"
#include "piveval_interface.h"
#include "piveval.h"



PivEval *
create_piveval (GnomeApp *main_window, 
		GtkWidget *container
                )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (main_window), "gpiv");
    PivEval *eval = g_new0 (PivEval, 1);

    eval->monitor = g_new0 (Monitor, 1);
    eval->monitor->pi_da = gpiv_alloc_pivdata (1, 1);

    eval->monitor->int_size_old = 0;
    eval->monitor->affine[0] = gpiv_var->piv_disproc_zoom;
    eval->monitor->affine[1] = 0.0;
    eval->monitor->affine[2] = 0.0;
    eval->monitor->affine[3] = gpiv_var->piv_disproc_zoom;
    eval->monitor->affine[4] = 0.0;
    eval->monitor->affine[5] = 0.0;

    eval->monitor->int_size_old = 0;
    eval->monitor->affine_vl[0] = gpiv_var->piv_disproc_vlength;
    eval->monitor->affine_vl[1] = 0.0;
    eval->monitor->affine_vl[2] = 0.0;
    eval->monitor->affine_vl[3] = gpiv_var->piv_disproc_vlength;
    eval->monitor->affine_vl[4] = 0.0;
    eval->monitor->affine_vl[5] = 0.0;


    eval->vbox_label = gtk_vbox_new (FALSE,
				    0);
    gtk_widget_ref (eval->vbox_label);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_label",
			     eval->vbox_label,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox_label);
    gtk_container_add (GTK_CONTAINER (container),
		      eval->vbox_label);

    eval->label_title = gtk_label_new(_("Piv image interrogation"));
    gtk_widget_ref(eval->label_title);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_title",
			     eval->label_title,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_title);
    gtk_box_pack_start (GTK_BOX (eval->vbox_label),
		       eval->label_title,
		       FALSE,
		       FALSE,
		       0);

/*
 * Scrolled window
 */
    eval->vbox_scroll = gtk_vbox_new (FALSE,
				     0);
    gtk_widget_ref (eval->vbox_scroll);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_scroll",
			     eval->vbox_scroll,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox_scroll);
    gtk_box_pack_start (GTK_BOX (eval->vbox_label),
		       eval->vbox_scroll, TRUE,
		       TRUE,
		       0);

    eval->scrolledwindow = gtk_scrolled_window_new (NULL,
						   NULL);
    gtk_widget_ref (eval->scrolledwindow);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "scrolledwindow",
			     eval->scrolledwindow,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->scrolledwindow);
    gtk_box_pack_start (GTK_BOX (eval->vbox_scroll),
		       eval->scrolledwindow,
		       TRUE,
		       TRUE,
		       0);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (eval->scrolledwindow),
				   GTK_POLICY_NEVER,
				   GTK_POLICY_AUTOMATIC);

    eval->viewport = gtk_viewport_new (NULL,
				      NULL);
    gtk_widget_ref (eval->viewport);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "viewport",
			     eval->viewport,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->viewport);
    gtk_container_add (GTK_CONTAINER (eval->scrolledwindow),
		      eval->viewport);
    gtk_widget_set_size_request (GTK_WIDGET (eval->scrolledwindow),
                                 410,
                                 375);

/* 
 * main table for PIV
 */
    eval->table = gtk_table_new (2, 
				8, 
				FALSE);
    gtk_widget_ref (eval->table);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "table",
			     eval->table,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->table);
    gtk_container_add (GTK_CONTAINER (eval->viewport),
		      eval->table);


/*
 * table for entries of first, last and pre-shift columns/rows
 */
    eval->table_aoi = gtk_table_new (4, 
				    3, 
				    FALSE);
    gtk_widget_ref (eval->table_aoi);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
			     "table_aoi",
			     eval->table_aoi,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->table_aoi);
    gtk_table_attach (GTK_TABLE (eval->table), 
		     eval->table_aoi, 
		     0, 
		     1, 
		     0, 
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 
		     0, 
		     0);


    
    eval->label_colstart = gtk_label_new ( _("first col:"));
    gtk_widget_ref (eval->label_colstart);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_colstart", 
			     eval->label_colstart,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_colstart);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->label_colstart, 
		     0, 
		     1, 
		     0, 
		     1,
		     (GtkAttachOptions) (0), 
		     (GtkAttachOptions) (0), 
		     0, 
		     0);



    eval->label_colend = gtk_label_new ( _("last col:"));
    gtk_widget_ref (eval->label_colend);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
			     "label_colend",
			     eval->label_colend,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_colend);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->label_colend, 
		     1, 
		     2, 
		     0, 
		     1,
		     (GtkAttachOptions) (0), 
		     (GtkAttachOptions) (0), 
		     0, 
		     0);



    eval->label_preshiftcol = gtk_label_new ( _("pre-shift col:"));
    gtk_widget_ref (eval->label_preshiftcol);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_preshiftcol",
			     eval->label_preshiftcol,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_preshiftcol);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->label_preshiftcol, 
		     2, 
		     3, 
		     0,
		     1, 
		     (GtkAttachOptions) (0), 
		     (GtkAttachOptions) (0), 
		     0,
		     0);



    eval->label_rowstart = gtk_label_new ( _("first row:"));
    gtk_widget_ref (eval->label_rowstart);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_rowstart", 
			     eval->label_rowstart,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_rowstart);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->label_rowstart, 
		     0, 
		     1, 
		     2, 
		     3,
		     (GtkAttachOptions) (0), 
		     (GtkAttachOptions) (0), 
		     0, 
		     0);
    


    eval->label_rowend = gtk_label_new ( _("last row:"));
    gtk_widget_ref (eval->label_rowend);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
			     "label_rowend",
			     eval->label_rowend,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_rowend);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->label_rowend, 
		     1, 
		     2, 
		     2, 
		     3,
		     (GtkAttachOptions) (0), 
		     (GtkAttachOptions) (0), 
		     0, 
		     0);



    eval->label_preshiftrow = gtk_label_new ( _("pre-shift row:"));
    gtk_widget_ref (eval->label_preshiftrow);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_preshiftrow",
			     eval->label_preshiftrow,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_preshiftrow);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->label_preshiftrow, 
		     2, 
		     3, 
		     2,
		     3, 
		     (GtkAttachOptions) (0), 
		     (GtkAttachOptions) (0), 
		     0,
		     0);
    

/*
 * spinner for colstart
 */
    eval->spinbutton_adj_colstart =
	gtk_adjustment_new (gl_piv_par->col_start, 
			   0, 
			   gl_image_par->ncolumns - 1, 
			   1,
			   10, 
			   10);
    eval->spinbutton_colstart =
      gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_colstart), 
			  1,
			  0);
    gtk_widget_ref (eval->spinbutton_colstart);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_colstart",
			     eval->spinbutton_colstart,
			     (GtkDestroyNotify) gtk_widget_unref);

    gtk_widget_show (eval->spinbutton_colstart);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->spinbutton_colstart, 
		     0, 
		     1,
		     1, 
		     2, 
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0), 
		     0, 
		     0);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (eval->spinbutton_colstart),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_colstart), 
			"eval", 
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_colstart),
			"var_type", 
			"0" /* COL_START */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_colstart), 
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_int), 
		       eval->spinbutton_colstart);

/*
 * spinner for colend
 */
    eval->spinbutton_adj_colend =
	gtk_adjustment_new (gl_piv_par->col_end, 
			   0, 
			   gl_image_par->ncolumns - 1, 
			   1, 
			   10,
			   10);
    eval->spinbutton_colend =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_colend), 
			    1,
			    0);
    gtk_widget_ref (eval->spinbutton_colend);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_colend",
			     eval->spinbutton_colend,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->spinbutton_colend);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->spinbutton_colend, 
		     1, 
		     2, 
		     1,
		     2, 
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (0), 
                      0, 
                      0);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (eval->spinbutton_colend),
                                 TRUE);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_colend), 
                         "eval", 
                         eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_colend), 
                         "var_type",
                         "1" /* COL_END */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_colend), 
                      "changed",
                      G_CALLBACK (on_spinbutton_piv_int), 
                      eval->spinbutton_colend);


/*
 * spinner for preshiftcol
 */
     eval->spinbutton_adj_preshiftcol =
        gtk_adjustment_new (gl_piv_par->pre_shift_col, 
                            (gint) -(gl_image_par->ncolumns - 1),
                            gl_image_par->ncolumns - 1, 
                            1, 
                            10, 
                            10);
    eval->spinbutton_preshiftcol =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_preshiftcol),
			    1, 
			    0);
    gtk_widget_ref (eval->spinbutton_preshiftcol);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_preshiftcol",
			     eval->spinbutton_preshiftcol,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->spinbutton_preshiftcol);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->spinbutton_preshiftcol, 
		     2,
		     3, 
		     1, 
		     2, 
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0), 
		     0, 
		     0);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON
				 (eval->spinbutton_preshiftcol), 
				TRUE);
 
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_preshiftcol), 
			"eval", 
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_preshiftcol), 
			"var_type",
			"2" /* PRE_SHIFT_COL */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_preshiftcol), 
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_int), 
		       eval->spinbutton_preshiftcol);


/*
 * spinner for rowstart
 */
    eval->spinbutton_adj_rowstart =
	gtk_adjustment_new (gl_piv_par->row_start, 
			   0, 
			   gl_image_par->nrows - 1, 
			   1,
			   10, 
			   10);
    eval->spinbutton_rowstart =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_rowstart), 
			    1,
			    0);
    gtk_widget_ref (eval->spinbutton_rowstart);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_rowstart",
			     eval->spinbutton_rowstart,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->spinbutton_rowstart);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->spinbutton_rowstart, 
		     0, 
		     1,
		     3, 
		     4, 
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0), 
		     0, 
		     0);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (eval->spinbutton_rowstart),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_rowstart), 
			"eval", 
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_rowstart), 
			"var_type",
			"3"/*  (int *) ROWSTART */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_rowstart), 
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_int), 
		       eval->spinbutton_rowstart);


/*
 * spinner for rowend
 */
    eval->spinbutton_adj_rowend =
	gtk_adjustment_new (gl_piv_par->row_end, 
			   0, 
			   gl_image_par->nrows - 1,
			   1,
			   10, 
			   10);
    eval->spinbutton_rowend =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_rowend), 
			    1,
			    0);
    gtk_widget_ref (eval->spinbutton_rowend);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_rowend",
			     eval->spinbutton_rowend,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->spinbutton_rowend);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->spinbutton_rowend, 
		     1, 
		     2, 
		     3,
		     4, 
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0), 
		     0, 
		     0);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_rowend), 
			"eval", 
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_rowend), 
			"var_type",
			"4"  /* ROWEND */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_rowend), 
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_int), 
		       eval->spinbutton_rowend);


/*
 * spinner for preshiftrow
 */
    eval->spinbutton_adj_preshiftrow =
	gtk_adjustment_new (gl_piv_par->pre_shift_row, 
                            (gint) - (gl_image_par->ncolumns - 1),
                            gl_image_par->ncolumns - 1, 
                            1, 
                            10, 
                            10);
    eval->spinbutton_preshiftrow =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_preshiftrow),
			    1, 
			    0);
    gtk_widget_ref (eval->spinbutton_preshiftrow);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_preshiftrow",
			     eval->spinbutton_preshiftrow,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->spinbutton_preshiftrow);
    gtk_table_attach (GTK_TABLE (eval->table_aoi), 
		     eval->spinbutton_preshiftrow, 
		     2,
		     3, 
		     3, 
		     4, 
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0), 
		     0, 
		     0);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_preshiftrow), 
			"eval", 
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_preshiftrow), 
			"var_type",
			"5" /* PRESHIFTROW */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_preshiftrow), 
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_int), 
		       eval->spinbutton_preshiftrow);


/*
 * radio buttons and spinners for interrogation size 1
 */

    eval->hbox_intreg = gtk_hbox_new (FALSE, 0);
    gtk_widget_ref (eval->hbox_intreg);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
			     "hbox_intreg",
			     eval->hbox_intreg,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->hbox_intreg);
    gtk_table_attach (GTK_TABLE (eval->table), eval->hbox_intreg, 
		     0, 
		     1, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);

    eval->frame_2 = gtk_frame_new ( _("Final Int Size"));
    gtk_widget_ref (eval->frame_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
			     "frame_2",
			     eval->frame_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_2);
    gtk_box_pack_start (GTK_BOX (eval->hbox_intreg), 
		       eval->frame_2, 
		       TRUE, 
		       TRUE,
		       0);

    eval->vbox_intsize_f = gtk_vbox_new (FALSE, 0);
    gtk_widget_ref (eval->vbox_intsize_f);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
			     "vbox_intsize_f",
			     eval->vbox_intsize_f,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox_intsize_f);
    gtk_container_add (GTK_CONTAINER (eval->frame_2), 
		      eval->vbox_intsize_f);


/*
 * spinner for interrogation size 1
 */
    eval->spinbutton_adj_intsize_f =
	gtk_adjustment_new (gl_piv_par->int_size_f, 
			   GPIV_MIN_INTERR_SIZE, 
                           GPIV_MAX_INTERR_SIZE, 
			   1,
			   10, 
			   10);
    eval->spinbutton_intsize_f =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_intsize_f),
			    1,
			    0);
    gtk_widget_ref (eval->spinbutton_intsize_f);
/*     gtk_spin_button_set_numeric (eval->spinbutton_intsize_f, */
/*                                  GTK_TOGGLE_BUTTON (eval->spinbutton_intsize_f)->active); */
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_intsize_f",
			     eval->spinbutton_intsize_f,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->spinbutton_intsize_f);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_f), 
		       eval->spinbutton_intsize_f,
		       FALSE, FALSE, 0);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_intsize_f), 
			"eval", 
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_intsize_f), 
			"var_type",
                         "6" /* INT_SIZE_F */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_intsize_f), 
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_int), 
		       eval->spinbutton_intsize_f);




/*
 * radio buttons for interrogation size 1
 */

/*     eval->radiobutton_intsize_f_1 = */
/* 	gtk_radio_button_new_with_label (eval->int_size_f_group,  _("8")); */
/*     eval->int_size_f_group = */
/* 	gtk_radio_button_group (GTK_RADIO_BUTTON */
/* 			       (eval->radiobutton_intsize_f_1)); */
/*     gtk_widget_ref (eval->radiobutton_intsize_f_1); */
/*     gtk_object_set_data_full (GTK_OBJECT (main_window), */
/* 			     "radiobutton_intsize_f_1", */
/* 			     eval->radiobutton_intsize_f_1, */
/* 			     (GtkDestroyNotify) gtk_widget_unref); */
/*     gtk_widget_show (eval->radiobutton_intsize_f_1); */
/*     gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_f), */
/* 		       eval->radiobutton_intsize_f_1, FALSE, FALSE, 0); */

/*     gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_1), "intsize_f", */
/* 			"8"); */
/*     gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_1), "var_type", */
/* 			"0"); */
/*     g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_f_1), "toggled", */
/* 		       G_CALLBACK (on_eval->radiobutton_int), NULL); */


    eval->radiobutton_intsize_f_2 =
      gtk_radio_button_new_with_label (eval->int_size_f_group, 
				       _("16"));
    eval->int_size_f_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intsize_f_2));
    gtk_widget_ref (eval->radiobutton_intsize_f_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intsize_f_2",
			     eval->radiobutton_intsize_f_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intsize_f_2);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_f),
		       eval->radiobutton_intsize_f_2, 
		       FALSE,
		       FALSE, 
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_2), 
			"eval", 
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_2), 
			"intsize_f",
			"16");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_2), 
			"var_type",
			"0");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_f_2), 
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int), 
		       NULL);


 
    eval->radiobutton_intsize_f_3 =
	gtk_radio_button_new_with_label (eval->int_size_f_group, 
					 _("32"));
    eval->int_size_f_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intsize_f_3));
    gtk_widget_ref (eval->radiobutton_intsize_f_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intsize_f_3",
			     eval->radiobutton_intsize_f_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intsize_f_3);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_f),
		       eval->radiobutton_intsize_f_3, 
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_3), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_3),
			"intsize_f",
			"32");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_3),
			"var_type",
			"0");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_f_3),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);



    eval->radiobutton_intsize_f_4 =
	gtk_radio_button_new_with_label (eval->int_size_f_group,
					 _("64"));
    eval->int_size_f_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intsize_f_4));
    gtk_widget_ref (eval->radiobutton_intsize_f_4);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intsize_f_4",
			     eval->radiobutton_intsize_f_4,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intsize_f_4);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_f),
		       eval->radiobutton_intsize_f_4,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_4), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_4),
			"intsize_f",
			"64");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_4),
			"var_type",
			"0");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_f_4),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);



    eval->radiobutton_intsize_f_5 =
	gtk_radio_button_new_with_label (eval->int_size_f_group,
					 _("128"));
    eval->int_size_f_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intsize_f_5));
    gtk_widget_ref (eval->radiobutton_intsize_f_5);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intsize_f_5",
			     eval->radiobutton_intsize_f_5,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intsize_f_5);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_f),
		       eval->radiobutton_intsize_f_5,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_5), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_5),
			"intsize_f",
			"128");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_f_5),
			"var_type",
			"0");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_f_5),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);


/*
 * radio buttons for interrogation size 2
 */

    eval->frame_3 = gtk_frame_new ( _("Initial Int Size"));
    gtk_widget_ref (eval->frame_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_3",
			     eval->frame_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_3);
    gtk_box_pack_start (GTK_BOX (eval->hbox_intreg),
		       eval->frame_3,
		       TRUE,
		       TRUE,
		       0);


    eval->vbox_intsize_i = gtk_vbox_new (FALSE,
				       0);
    gtk_widget_ref (eval->vbox_intsize_i);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_intsize_i",
			     eval->vbox_intsize_i,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox_intsize_i);
    gtk_container_add (GTK_CONTAINER (eval->frame_3),
		      eval->vbox_intsize_i);


/*
 * spinner for interrogation size 2
 */
    eval->spinbutton_adj_intsize_i =
	gtk_adjustment_new (gl_piv_par->int_size_i,
			   GPIV_MIN_INTERR_SIZE, 
                           GPIV_MAX_INTERR_SIZE,
			   1,
			   10,
			   10);
    eval->spinbutton_intsize_i =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_intsize_i),
			    1,
			    0);
    gtk_widget_ref (eval->spinbutton_intsize_i);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_intsize_i",
			     eval->spinbutton_intsize_i,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->spinbutton_intsize_i);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_i),
		       eval->spinbutton_intsize_i,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_intsize_i),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_intsize_i),
			"var_type",
			"7" /* INT_SIZE_I */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_intsize_i),
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_int), 
		       eval->spinbutton_intsize_i);




/*     eval->radiobutton_intsize_i_1 = */
/* 	gtk_radio_button_new_with_label (eval->int_size_i_group,  _("8")); */
/*     eval->int_size_i_group = */
/* 	gtk_radio_button_group (GTK_RADIO_BUTTON */
/* 			       (eval->radiobutton_intsize_i_1)); */
/*     gtk_widget_ref (eval->radiobutton_intsize_i_1); */
/*     gtk_object_set_data_full (GTK_OBJECT (main_window), */
/* 			     "radiobutton_intsize_i_1", */
/* 			     eval->radiobutton_intsize_i_1, */
/* 			     (GtkDestroyNotify) gtk_widget_unref); */
/*     gtk_widget_show (eval->radiobutton_intsize_i_1); */
/*     gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_i), */
/* 		       eval->radiobutton_intsize_i_1, FALSE, FALSE, 0); */

/*     gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_1), 
       "eval", eval); */
/*     gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_1), "intsize_i", */
/* 			"8"); */
/*     gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_1), "var_type", */
/* 			"1"); */
/*     g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_i_1), "toggled", */
/* 		       G_CALLBACK (on_radiobutton_piv_int), NULL); */


    eval->radiobutton_intsize_i_2 =
	gtk_radio_button_new_with_label (eval->int_size_i_group,
					 _("16"));
    eval->int_size_i_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intsize_i_2));
    gtk_widget_ref (eval->radiobutton_intsize_i_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intsize_i_2",
			     eval->radiobutton_intsize_i_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intsize_i_2);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_i),
		       eval->radiobutton_intsize_i_2,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_2), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_2),
			"intsize_i",
			"16");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_2),
			"var_type",
			"1");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_i_2),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);



    eval->radiobutton_intsize_i_3 =
      gtk_radio_button_new_with_label (eval->int_size_i_group,
				       _("32"));
    eval->int_size_i_group =
      gtk_radio_button_group (GTK_RADIO_BUTTON
			     (eval->radiobutton_intsize_i_3));
    gtk_widget_ref (eval->radiobutton_intsize_i_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intsize_i_3",
			     eval->radiobutton_intsize_i_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intsize_i_3);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_i),
		       eval->radiobutton_intsize_i_3,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_3), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_3),
			"intsize_i",
			"32");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_3),
			"var_type",
			"1");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_i_3),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);



    eval->radiobutton_intsize_i_4 =
	gtk_radio_button_new_with_label (eval->int_size_i_group,
					 _("64"));
    eval->int_size_i_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intsize_i_4));
    gtk_widget_ref (eval->radiobutton_intsize_i_4);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intsize_i_4",
			     eval->radiobutton_intsize_i_4,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intsize_i_4);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_i),
		       eval->radiobutton_intsize_i_4,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_4), 
			"eval", eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_4),
			"intsize_i",
			"64");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_4),
			"var_type",
			"1");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_i_4),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);



    eval->radiobutton_intsize_i_5 =
	gtk_radio_button_new_with_label (eval->int_size_i_group,
					 _("128"));
    eval->int_size_i_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intsize_i_5));
    gtk_widget_ref (eval->radiobutton_intsize_i_5);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intsize_i_5",
			     eval->radiobutton_intsize_i_5,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intsize_i_5);
    gtk_box_pack_start (GTK_BOX (eval->vbox_intsize_i),
		       eval->radiobutton_intsize_i_5, 
		       FALSE, 
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_5), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_5),
			"intsize_i",
			"128");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intsize_i_5)
			, "var_type",
			"1");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intsize_i_5),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);


/* 
 * radio buttons for shifted distance of interrogation areas
 */

    eval->frame_4 = gtk_frame_new ( _("Shift"));
    gtk_widget_ref (eval->frame_4);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_4",
			     eval->frame_4,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_4);
    gtk_box_pack_start (GTK_BOX (eval->hbox_intreg),
		       eval->frame_4,
		       TRUE,
		       TRUE,
		       0);

    eval->vbox_shift = gtk_vbox_new (FALSE,
				    0);
    gtk_widget_ref (eval->vbox_shift);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_shift",
			     eval->vbox_shift,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox_shift);
    gtk_container_add (GTK_CONTAINER (eval->frame_4),
		      eval->vbox_shift);


/*
 * spinner for interrogation shift
 */
    eval->spinbutton_adj_intshift =
	gtk_adjustment_new(gl_piv_par->int_shift,
			   GPIV_MIN_INTERR_SIZE / 2, 
			   GPIV_MAX_INTERR_SIZE,
			   1,
			   10,
			   10);
    eval->spinbutton_intshift =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_intshift),
			    1,
			    0);
    gtk_widget_ref (eval->spinbutton_intshift);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_intshift",
			     eval->spinbutton_intshift,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->spinbutton_intshift);
    gtk_box_pack_start (GTK_BOX (eval->vbox_shift), eval->spinbutton_intshift,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_intshift),
			"eval"
                         , eval);
    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_intshift),
                         "var_type",
                         "8" /* INT_SHIFT */);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_intshift),
                      "changed",
                      G_CALLBACK (on_spinbutton_piv_int),
                      eval->spinbutton_intshift);



    eval->radiobutton_intshift_1 =
	gtk_radio_button_new_with_label (eval->int_shift_group,
					 _("8"));
    eval->int_shift_group =
      gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intshift_1));
    gtk_widget_ref (eval->radiobutton_intshift_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),

			     "radiobutton_intshift_1",
			     eval->radiobutton_intshift_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intshift_1);
    gtk_box_pack_start (GTK_BOX (eval->vbox_shift),
		       eval->radiobutton_intshift_1,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_1), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_1),
			"intshift",
			"8");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_1),
			"var_type",
			"2");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intshift_1),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);



    eval->radiobutton_intshift_2 =
	gtk_radio_button_new_with_label (eval->int_shift_group,
					 _("16"));
    eval->int_shift_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intshift_2));
    gtk_widget_ref (eval->radiobutton_intshift_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intshift_2",
			     eval->radiobutton_intshift_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intshift_2);
    gtk_box_pack_start (GTK_BOX (eval->vbox_shift),
		       eval->radiobutton_intshift_2,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_2), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_2),
			"intshift",
			"16");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_2),
			"var_type",
			"2");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intshift_2),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);



    eval->radiobutton_intshift_3 =
	gtk_radio_button_new_with_label (eval->int_shift_group,
					 _("32"));
    eval->int_shift_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intshift_3));
    gtk_widget_ref (eval->radiobutton_intshift_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intshift_3",
			     eval->radiobutton_intshift_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intshift_3);
    gtk_box_pack_start (GTK_BOX (eval->vbox_shift),
		       eval->radiobutton_intshift_3,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_3), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_3),
			"intshift",
			"32");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_3),
			"var_type",
			"2");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intshift_3),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);
    


    eval->radiobutton_intshift_4 =
	gtk_radio_button_new_with_label (eval->int_shift_group,
					 _("64"));
    eval->int_shift_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intshift_4));
    gtk_widget_ref (eval->radiobutton_intshift_4);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intshift_4",
			     eval->radiobutton_intshift_4,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intshift_4);
    gtk_box_pack_start (GTK_BOX (eval->vbox_shift),
		       eval->radiobutton_intshift_4,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_4), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_4),
			"intshift",
			"64");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_4),
			"var_type",
			"2");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intshift_4),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);



    eval->radiobutton_intshift_5 =
	gtk_radio_button_new_with_label (eval->int_shift_group,
					 _("128"));
    eval->int_shift_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_intshift_5));
    gtk_widget_ref (eval->radiobutton_intshift_5);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_intshift_5",
			     eval->radiobutton_intshift_5,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_intshift_5);
    gtk_box_pack_start (GTK_BOX (eval->vbox_shift),
		       eval->radiobutton_intshift_5,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_5), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_5),
			"intshift",
			"128");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_intshift_5),
			"var_type",
			"2");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_intshift_5),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_int),
		       NULL);


/*
 * radio button for mouse selecting
 */

    eval->frame_1 = gtk_frame_new ( _("Mouse select"));
    gtk_widget_ref (eval->frame_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_1",
			     eval->frame_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_1);
    gtk_table_attach (GTK_TABLE (eval->table),
		     eval->frame_1, 
		     1, 
		     2, 
		     0, 
		     2,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);


    eval->vbox_mouseselect = gtk_vbox_new (FALSE,
					  0);
    gtk_widget_ref (eval->vbox_mouseselect);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_mouseselect",
			     eval->vbox_mouseselect,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox_mouseselect);
    gtk_container_add (GTK_CONTAINER (eval->frame_1),
		      eval->vbox_mouseselect);


    eval->radiobutton_mouse_0 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("None"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_mouse_0));
    gtk_widget_ref (eval->radiobutton_mouse_0);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_0",
			     eval->radiobutton_mouse_0,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_mouse_0);
    gtk_box_pack_start (GTK_BOX (eval->vbox_mouseselect),
		       eval->radiobutton_mouse_0,
		       FALSE,
		       FALSE,
		       0);
 
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_0),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_0),
			"mouse_select",
			"0" /* NO_MS */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_0),
		       "enter",
		       G_CALLBACK (on_radiobutton_piv_mouse1_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_0),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_0),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_mouse),
		       NULL);



    eval->radiobutton_mouse_1 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Area"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_mouse_1));
    gtk_widget_ref (eval->radiobutton_mouse_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_1",
			     eval->radiobutton_mouse_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_mouse_1);
    gtk_box_pack_start (GTK_BOX (eval->vbox_mouseselect),
		       eval->radiobutton_mouse_1,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_1),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_1),
			"mouse_select",
			"1" /* AOI_MS */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_1),
		       "enter",
		       G_CALLBACK (on_radiobutton_piv_mouse2_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_1),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_1),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_mouse),
		       NULL);



    eval->radiobutton_mouse_2 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Single int."));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_mouse_2));
    gtk_widget_ref (eval->radiobutton_mouse_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_2",
			     eval->radiobutton_mouse_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_mouse_2);
    gtk_box_pack_start (GTK_BOX (eval->vbox_mouseselect),
		       eval->radiobutton_mouse_2,
		       FALSE,
		       FALSE,
                        0);
    gtk_widget_hide (GTK_WIDGET (eval->radiobutton_mouse_2));

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_2),
                         "eval",
                         eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_2),
			"mouse_select",
			"2" /* SINGLE_AREA_MS */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_2),
		       "enter",
		       G_CALLBACK (on_radiobutton_piv_mouse3_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_2),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_2),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_mouse),
		       NULL);



    eval->radiobutton_mouse_3 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Single point"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_mouse_3));
    gtk_widget_ref (eval->radiobutton_mouse_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_3",
			     eval->radiobutton_mouse_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_mouse_3);
    gtk_box_pack_start (GTK_BOX (eval->vbox_mouseselect),
		       eval->radiobutton_mouse_3,
		       FALSE,
		       FALSE,
		       0);
    gtk_widget_hide (GTK_WIDGET (eval->radiobutton_mouse_3));

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_3),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_3),
			"mouse_select",
			"3" /* SINGLE_POINT_MS */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_3),
		       "enter",
		       G_CALLBACK (on_radiobutton_piv_mouse4_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_3),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_3),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_mouse),
		       NULL);



    eval->radiobutton_mouse_4 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Drag int."));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_mouse_4));
    gtk_widget_ref (eval->radiobutton_mouse_4);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_4",
			     eval->radiobutton_mouse_4,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_mouse_4);
    gtk_box_pack_start (GTK_BOX (eval->vbox_mouseselect),
		       eval->radiobutton_mouse_4,
		       FALSE,
		       FALSE,
		       0);
    gtk_widget_hide (GTK_WIDGET (eval->radiobutton_mouse_4));

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_4),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_4),
			"mouse_select",
			"4" /* DRAG_MS */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_4),
		       "enter",
		       G_CALLBACK (on_radiobutton_piv_mouse5_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_4),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_4),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_mouse),
		       NULL);



    eval->radiobutton_mouse_5 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Vert. Line"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_mouse_5));
    gtk_widget_ref (eval->radiobutton_mouse_5);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_5",
			     eval->radiobutton_mouse_5,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_mouse_5);
    gtk_box_pack_start (GTK_BOX (eval->vbox_mouseselect),
		       eval->radiobutton_mouse_5,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_5),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_5),
			"mouse_select",
			"5" /* V_LINE_MS */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_5),
		       "enter",
		       G_CALLBACK (on_radiobutton_piv_mouse6_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_5),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_5),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_mouse),
		       NULL);



    eval->radiobutton_mouse_6 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("Hor. Line"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_mouse_6));
    gtk_widget_ref (eval->radiobutton_mouse_6);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_mouse_6",
			     eval->radiobutton_mouse_6,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_mouse_6);
    gtk_box_pack_start (GTK_BOX (eval->vbox_mouseselect),
		       eval->radiobutton_mouse_6,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_6),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_mouse_6),
			"mouse_select",
			"6" /* H_LINE_MS */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_6),
		       "enter",
		       G_CALLBACK (on_radiobutton_piv_mouse7_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_6),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_mouse_6),
		       "toggled",
		       G_CALLBACK (on_radiobutton_piv_mouse),
		       NULL);


/*
 * radio buttons for sub-pixel interpolation scheme
 */

    eval->frame_5 = gtk_frame_new ( _("Sub-pixel Interpolation"));
    gtk_widget_ref (eval->frame_5);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_5",
			     eval->frame_5,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_5);
    gtk_table_attach (GTK_TABLE (eval->table),
		     eval->frame_5, 
		     0, 
		     1, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     0,
		     0);


    eval->vbox10 = gtk_vbox_new (FALSE,
				0);
    gtk_widget_ref (eval->vbox10);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox10",
			     eval->vbox10,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox10);
    gtk_container_add (GTK_CONTAINER (eval->frame_5),
		      eval->vbox10);


    eval->radiobutton_fit_none =
	gtk_radio_button_new_with_label (eval->vbox10_group,
					 _("None"));
    eval->vbox10_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_fit_none));
    gtk_widget_ref (eval->radiobutton_fit_none);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_fit_none",
			     eval->radiobutton_fit_none,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_fit_none);
    gtk_box_pack_start (GTK_BOX (eval->vbox10),
		       eval->radiobutton_fit_none,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_none),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_none),
			"ifit",
			"0");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_none),
			"var_type",
			"0");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_none),
		       "enter",
		       G_CALLBACK (on_radiobutton_fit_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_none),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);

    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_none),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



    eval->radiobutton_fit_gauss =
	gtk_radio_button_new_with_label (eval->vbox10_group,
					 _("Gauss"));
    eval->vbox10_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_fit_gauss));
    gtk_widget_ref (eval->radiobutton_fit_gauss);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_fit_gauss",
			     eval->radiobutton_fit_gauss,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_fit_gauss);
    gtk_box_pack_start (GTK_BOX (eval->vbox10),
		       eval->radiobutton_fit_gauss,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_gauss),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_gauss),
			"ifit",
			"1");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_gauss),
			"var_type",
			"0");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_gauss),
		       "enter",
		       G_CALLBACK (on_radiobutton_fit_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_gauss),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_gauss),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



    eval->radiobutton_fit_power =
	gtk_radio_button_new_with_label (eval->vbox10_group,
					 _("Power"));
    eval->vbox10_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_fit_power));
    gtk_widget_ref (eval->radiobutton_fit_power);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_fit_power",
			     eval->radiobutton_fit_power,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_fit_power);
    gtk_box_pack_start (GTK_BOX (eval->vbox10),
		       eval->radiobutton_fit_power,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_power),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_power),
			"ifit",
			"2");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_power),
			"var_type",
			"0");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_power),
		       "enter",
		       G_CALLBACK (on_radiobutton_fit_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_power),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_power),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



    eval->radiobutton_fit_gravity =
	gtk_radio_button_new_with_label (eval->vbox10_group,
					 _("Gravity"));
    eval->vbox10_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (eval->radiobutton_fit_gravity));
    gtk_widget_ref (eval->radiobutton_fit_gravity);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_fit_gravity",
			     eval->radiobutton_fit_gravity,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_fit_gravity);
    gtk_box_pack_start (GTK_BOX (eval->vbox10),
		       eval->radiobutton_fit_gravity,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_gravity), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_gravity),
			"ifit",
			"3");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_gravity),
			"var_type",
			"0");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_gravity),
		       "enter",
		       G_CALLBACK (on_radiobutton_fit_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_gravity),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_gravity),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



/*   eval->radiobutton_fit_marquardt = gtk_radio_button_new_with_label (eval->vbox10_group,
      _("Marquardt")); */
/*   eval->vbox10_group = gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_fit_marquardt)); */
/*   gtk_widget_ref (eval->radiobutton_fit_marquardt); */
/*   gtk_object_set_data_full (GTK_OBJECT (main_window),
     "radiobutton_fit_marquardt",
     eval->radiobutton_fit_marquardt, */
/*                             (GtkDestroyNotify) gtk_widget_unref); */
/*   gtk_widget_show (eval->radiobutton_fit_marquardt); */
/*   gtk_box_pack_start (GTK_BOX (eval->vbox10),
     eval->radiobutton_fit_marquardt,
     FALSE,
     FALSE,
     0); */

/*   gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_marquardt),  */
/* 		      "ifit",
		      "4"); */
/*   gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_marquardt),
     "eval",
     eval); */
/*   gtk_object_set_data (GTK_OBJECT (eval->radiobutton_fit_marquardt),  */
/* 		      "var_type",
		      "0"); */
/*   g_signal_connect (GTK_OBJECT (eval->radiobutton_fit_marquardt),
     "toggled", */
/*                       G_CALLBACK (on_toggle_piv), */
/*                       NULL); */



/*
 * radio buttons for correlation peak number to be detected
 */
    eval->frame_6 = gtk_frame_new ( _("Peak #"));
    gtk_widget_ref (eval->frame_6);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_6",
			     eval->frame_6,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_6);
    gtk_table_attach (GTK_TABLE (eval->table),
		     eval->frame_6, 
		     1, 
		     2, 
		     2, 
		     3,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);

    eval->vbox11 = gtk_vbox_new (FALSE,
				0);
    gtk_widget_ref (eval->vbox11);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox11",
			     eval->vbox11,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox11);
    gtk_container_add (GTK_CONTAINER (eval->frame_6),
		      eval->vbox11);

    eval->radiobutton_peak_1 =
	gtk_radio_button_new_with_label (eval->vbox11_group,
					 _("1"));
    eval->vbox11_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_peak_1));
    gtk_widget_ref (eval->radiobutton_peak_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_peak_1",
			     eval->radiobutton_peak_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_peak_1);
    gtk_box_pack_start (GTK_BOX (eval->vbox11),
		       eval->radiobutton_peak_1,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_1),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_1),
			"peak",
			"1");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_1),
			"var_type",
			"1");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_1),
		       "enter",
		       G_CALLBACK (on_radiobutton_peak_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_1),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_1),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



    eval->radiobutton_peak_2 =
	gtk_radio_button_new_with_label (eval->vbox11_group,
					 _("2"));
    eval->vbox11_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_peak_2));
    gtk_widget_ref (eval->radiobutton_peak_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_peak_2",
			     eval->radiobutton_peak_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_peak_2);
    gtk_box_pack_start (GTK_BOX (eval->vbox11),
		       eval->radiobutton_peak_2,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_2),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_2),
			"peak",
			"2");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_2),
			"var_type",
			"1");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_2),
		       "enter",
		       G_CALLBACK (on_radiobutton_peak_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_2),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_2),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



    eval->radiobutton_peak_3 =
	gtk_radio_button_new_with_label (eval->vbox11_group,
					 _("3"));
    eval->vbox11_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_peak_3));
    gtk_widget_ref (eval->radiobutton_peak_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_peak_3",
			     eval->radiobutton_peak_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_peak_3);
    gtk_box_pack_start (GTK_BOX (eval->vbox11),
		       eval->radiobutton_peak_3,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_3),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_3),
			"peak",
			"3");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_peak_3),
			"var_type",
			"1");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_3),
		       "enter",
		       G_CALLBACK (on_radiobutton_peak_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_3),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_peak_3),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);


/*
 * additonal calculation schemes for correlation detection
 */

    eval->frame_7 = gtk_frame_new ( _("Interrogation scheme"));
    gtk_widget_ref (eval->frame_7);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_7",
			     eval->frame_7,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_7);
    gtk_table_attach (GTK_TABLE (eval->table),
		     eval->frame_7, 
		     0, 
		     1, 
		     4, 
		     5,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);


    eval->vbox12 = gtk_vbox_new (FALSE,
				0);
    gtk_widget_ref (eval->vbox12);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox12",
			     eval->vbox12,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox12);
    gtk_container_add (GTK_CONTAINER (eval->frame_7),
		      eval->vbox12);


/*----------------------------------------------------------------------------
 * Change order! */

    eval->radiobutton_imgdeform =
	gtk_radio_button_new_with_label (eval->vbox12_group,
                                         _("Image deformation"));
    eval->vbox12_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON 
                                (eval->radiobutton_imgdeform));
    gtk_widget_ref (eval->radiobutton_imgdeform);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_imgdeform",
			     eval->radiobutton_imgdeform,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_imgdeform);
    gtk_box_pack_start (GTK_BOX (eval->vbox12),
		       eval->radiobutton_imgdeform,
		       FALSE,
		       FALSE,
		       0); 

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_imgdeform), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_imgdeform),
			"var_type",
			"2" /* SCHEME */);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_imgdeform),
			"scheme",
			"4" /* GPIV_IMG_DEFORM */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_imgdeform),
		       "enter",
		       G_CALLBACK 
                      (on_radiobutton_interrogatescheme_imgdeform_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_imgdeform),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_imgdeform),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);


    eval->radiobutton_centraldiff =
	gtk_radio_button_new_with_label (eval->vbox12_group,
                                         _("Central differential"));
    eval->vbox12_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON 
                                (eval->radiobutton_centraldiff));
    gtk_widget_ref (eval->radiobutton_centraldiff);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_centraldiff",
			     eval->radiobutton_centraldiff,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_centraldiff);
    gtk_box_pack_start (GTK_BOX (eval->vbox12),
		       eval->radiobutton_centraldiff,
		       FALSE,
		       FALSE,
		       0); 

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_centraldiff), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_centraldiff),
			"var_type",
			"2" /* SCHEME */);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_centraldiff),
			"scheme",
			"3" /* (GPIV_ZERO_OFF_CENTRAL) */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_centraldiff),
		       "enter",
		       G_CALLBACK (on_radiobutton_interrogatescheme_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_centraldiff),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_centraldiff),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);


    eval->radiobutton_zerooff =
	gtk_radio_button_new_with_label (eval->vbox12_group,
                                         _("Zero offset"));
    eval->vbox12_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_zerooff));
    gtk_widget_ref (eval->radiobutton_zerooff);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_zerooff",
			     eval->radiobutton_zerooff,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_zerooff);
    gtk_box_pack_start (GTK_BOX (eval->vbox12),
		       eval->radiobutton_zerooff,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_zerooff),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_zerooff),
			"var_type",
			"2" /* SCHEME */);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_zerooff),
			"scheme",
			"2" /* GPIV_ZERO_OFF_FORWARD */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_zerooff),
		       "enter",
		       G_CALLBACK (on_radiobutton_interrogatescheme_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_zerooff),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_zerooff),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



    eval->radiobutton_weightkernel =
	gtk_radio_button_new_with_label (eval->vbox12_group,
                                         _("Weight Kernel"));
    eval->vbox12_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_weightkernel));
    gtk_widget_ref (eval->radiobutton_weightkernel);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_weightkernel",
			     eval->radiobutton_weightkernel,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_weightkernel);
    gtk_box_pack_start (GTK_BOX (eval->vbox12),
		       eval->radiobutton_weightkernel,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_weightkernel), 
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_weightkernel),
			"var_type",
			"2" /* SCHEME */);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_weightkernel),
			"scheme",
			"1" /* GPIV_LK_WEIGHT */);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_weightkernel),
		       "enter",
		       G_CALLBACK (on_radiobutton_interrogatescheme_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_weightkernel),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->radiobutton_weightkernel),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



/*
 * radio buttons for cross-or auto correlation
 * defined in image header, not user definable
 */

    eval->frame_8 = gtk_frame_new ( _("Correlation"));
    gtk_widget_ref (eval->frame_8);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_8",
			     eval->frame_8,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_8);
    gtk_table_attach (GTK_TABLE (eval->table),
		     eval->frame_8, 
		     1, 
		     2, 
		     4, 
		     5,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);

    eval->vbox13 = gtk_vbox_new (FALSE,
				0);
    gtk_widget_ref (eval->vbox13);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox13",
			     eval->vbox13,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->vbox13);
    gtk_container_add (GTK_CONTAINER (eval->frame_8),
		      eval->vbox13);



    eval->radiobutton_cross_1 =
	gtk_radio_button_new_with_label (eval->vbox13_group,
					 _("Auto"));
    eval->vbox13_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_cross_1));
    gtk_widget_ref (eval->radiobutton_cross_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_cross_1",
			     eval->radiobutton_cross_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_cross_1);
    gtk_box_pack_start (GTK_BOX (eval->vbox13),
		       eval->radiobutton_cross_1,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_cross_1),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_cross_1),
			"x_corr",
			"0");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_cross_1),
			"var_type",
			"5");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_cross_1),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);



    eval->radiobutton_cross_2 =
	gtk_radio_button_new_with_label (eval->vbox13_group,
					 _("Cross"));
    eval->vbox13_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (eval->radiobutton_cross_2));
    gtk_widget_ref (eval->radiobutton_cross_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_cross_2",
			     eval->radiobutton_cross_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->radiobutton_cross_2);
    gtk_box_pack_start (GTK_BOX (eval->vbox13),
		       eval->radiobutton_cross_2,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_cross_2),
			"eval",
			eval);
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_cross_2),
			"x_corr",
			"1");
    gtk_object_set_data (GTK_OBJECT (eval->radiobutton_cross_2),
			"var_type",
			"5");
    g_signal_connect (GTK_OBJECT (eval->radiobutton_cross_2),
		       "toggled",
		       G_CALLBACK (on_toggle_piv),
		       NULL);


/*
 * Checkbutton for I.A. Gauss weighting
 */
    eval->checkbutton_weight_ia =
	gtk_check_button_new_with_label ( _("Gauss weighting"));
    gtk_widget_ref (eval->checkbutton_weight_ia);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "checkbutton_weight_ia",
			     eval->checkbutton_weight_ia,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->checkbutton_weight_ia);
    gtk_box_pack_start (GTK_BOX (eval->vbox13),
		       eval->checkbutton_weight_ia,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->checkbutton_weight_ia),
			"eval",
			eval);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_weight_ia),
		       "enter",
		       G_CALLBACK (on_checkbutton_weight_ia_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_weight_ia),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_weight_ia),
		       "toggled",
		       G_CALLBACK (on_checkbutton_weight_ia),
		       NULL);



/*
 * Checkbutton for spof filtering
 */
    eval->checkbutton_spof =
	gtk_check_button_new_with_label ( _("SPOF filtering"));
    gtk_widget_ref (eval->checkbutton_spof);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "checkbutton_spof",
			     eval->checkbutton_spof,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->checkbutton_spof);
    gtk_box_pack_start (GTK_BOX (eval->vbox13),
		       eval->checkbutton_spof,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->checkbutton_spof),
			"eval",
			eval);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_spof),
		       "enter",
		       G_CALLBACK (on_checkbutton_spof_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_spof),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_spof),
		       "toggled",
		       G_CALLBACK (on_checkbutton_spof),
		       NULL);



/*
 * Monitoring the process by displaying interrogation area's in correlation
 * function
 */
    eval->frame_monitor =
	gtk_frame_new ( _("Interrogation areas and correlation"));
    gtk_widget_ref (eval->frame_monitor);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_monitor",
			     eval->frame_monitor,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_monitor);
    gtk_table_attach (GTK_TABLE (eval->table),
		     eval->frame_monitor, 
		     0, 
		     2, 
		     5,
		     6, 
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     0,
		     0);


    eval->table_monitor = gtk_table_new (2,
                                         3,
                                         FALSE);
    gtk_widget_ref (eval->table_monitor);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "table_monitor",
			     eval->table_monitor,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->table_monitor);
    gtk_container_add (GTK_CONTAINER (eval->frame_monitor),
		      eval->table_monitor);


/*
 * gnome canvas interrogation area1
 */
    eval->frame_monitor_int1 = gtk_frame_new ( _("Int. area 1"));
    gtk_widget_ref (eval->frame_monitor_int1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_monitor_int1",
			     eval->frame_monitor_int1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_monitor_int1);
    gtk_table_attach (GTK_TABLE (eval->table_monitor),
		     eval->frame_monitor_int1,
		     0,
		     1,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     0,
		     0);
    gtk_widget_set_size_request (GTK_WIDGET (eval->frame_monitor_int1),
                                 GPIV_MAX_INTERR_SIZE,
                                 GPIV_MAX_INTERR_SIZE + 25);



#ifdef CANVAS_AA
    gtk_widget_push_visual (gdk_rgb_get_visual ());
    gtk_widget_push_colormap (gdk_rgb_get_cmap ());
    eval->canvas_monitor_int1 = gnome_canvas_new_aa ();
#else
    gtk_widget_push_visual (gdk_imlib_get_visual ());
    gtk_widget_push_colormap (NULL);
    eval->canvas_monitor_int1 = gnome_canvas_new ();
#endif

    gtk_widget_pop_colormap ();
    gtk_widget_pop_visual ();
    gtk_widget_ref (eval->canvas_monitor_int1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
                              "canvas_monitor_int1",
                              eval->canvas_monitor_int1,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->canvas_monitor_int1);
    gtk_container_add (GTK_CONTAINER (eval->frame_monitor_int1 ),
                       eval->canvas_monitor_int1);
    


/*
 * gnome canvas interrogation area2
 */
    eval->frame_monitor_int2 = gtk_frame_new ( _("Int. area 2"));
    gtk_widget_ref (eval->frame_monitor_int2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_monitor_int2",
			     eval->frame_monitor_int2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_monitor_int2);
    gtk_table_attach (GTK_TABLE (eval->table_monitor),
		     eval->frame_monitor_int2,
		     1,
		     2,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     0,
		     0);
    gtk_widget_set_size_request (GTK_WIDGET (eval->frame_monitor_int2),
                                 GPIV_MAX_INTERR_SIZE,
                                 GPIV_MAX_INTERR_SIZE + 25);




/*     gtk_widget_push_visual (gdk_rgb_get_visual ()); */
/*     gtk_widget_push_colormap (gdk_rgb_get_cmap ()); */
/*     eval->canvas_monitor_int2 = gnome_canvas_new_aa (); */

#ifdef CANVAS_AA
    gtk_widget_push_visual (gdk_rgb_get_visual ());
    gtk_widget_push_colormap (gdk_rgb_get_cmap ());
    eval->canvas_monitor_int2 = gnome_canvas_new_aa ();
#else
  gtk_widget_push_visual (gdk_imlib_get_visual ());
  gtk_widget_push_colormap (NULL);
  eval->canvas_monitor_int2 = gnome_canvas_new ();
#endif

    gtk_widget_pop_colormap ();
    gtk_widget_pop_visual ();
    gtk_widget_ref (eval->canvas_monitor_int2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
                              "canvas_monitor_int2",
                              eval->canvas_monitor_int2,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->canvas_monitor_int2);
    gtk_container_add (GTK_CONTAINER (eval->frame_monitor_int2),
                       eval->canvas_monitor_int2);


/* 
 * Gnome canvas display corr function
 */
    eval->frame_monitor_cov = gtk_frame_new ( _("Correlation"));
    gtk_widget_ref (eval->frame_monitor_cov);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_monitor_cov",
			     eval->frame_monitor_cov,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_monitor_cov);
    gtk_table_attach (GTK_TABLE (eval->table_monitor),
		     eval->frame_monitor_cov,
		     0,
		     1,
		     1,
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     0,
		     0);
    gtk_widget_set_size_request (GTK_WIDGET (eval->frame_monitor_cov),
                                 GPIV_MAX_INTERR_SIZE,
                                 GPIV_MAX_INTERR_SIZE + 25);



#ifdef CANVAS_AA
    gtk_widget_push_visual (gdk_rgb_get_visual ());
    gtk_widget_push_colormap (gdk_rgb_get_cmap ());
    eval->canvas_monitor_cov = gnome_canvas_new_aa ();
#else
  gtk_widget_push_visual (gdk_imlib_get_visual ());
  gtk_widget_push_colormap (NULL);
  eval->canvas_monitor_cov = gnome_canvas_new ();
#endif

  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();
  gtk_widget_ref (eval->canvas_monitor_cov);
  gtk_object_set_data_full (GTK_OBJECT (main_window),
     "canvas_monitor_cov",
     eval->canvas_monitor_cov,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (eval->canvas_monitor_cov);
  gtk_container_add (GTK_CONTAINER (eval->frame_monitor_cov),
     eval->canvas_monitor_cov);


/*
 * gnome canvas display values
 */
    eval->frame_monitor_vec = gtk_frame_new ( _("Estimator"));
    gtk_widget_ref (eval->frame_monitor_vec);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_monitor_vec",
			     eval->frame_monitor_vec,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->frame_monitor_vec);
    gtk_table_attach (GTK_TABLE (eval->table_monitor),
		     eval->frame_monitor_vec,
		     1, 
		     2,
		     1,
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     0,
		     0);
     gtk_widget_set_size_request (GTK_WIDGET (eval->frame_monitor_vec),
                                  GPIV_MAX_INTERR_SIZE,
                                  GPIV_MAX_INTERR_SIZE + 25);


#ifdef CANVAS_AA
    gtk_widget_push_visual (gdk_rgb_get_visual ());
    gtk_widget_push_colormap (gdk_rgb_get_cmap ());
    eval->canvas_monitor_vec = gnome_canvas_new_aa ();
#else
    gtk_widget_push_visual (gdk_imlib_get_visual ());
    gtk_widget_push_colormap (NULL);
    eval->canvas_monitor_vec = gnome_canvas_new ();
#endif
    gtk_widget_pop_colormap ();
    gtk_widget_pop_visual ();
    gtk_widget_ref (eval->canvas_monitor_vec);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "canvas_monitor_vec",
			     eval->canvas_monitor_vec,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->canvas_monitor_vec);
    gtk_container_add (GTK_CONTAINER (eval->frame_monitor_vec),
		      eval->canvas_monitor_vec);


/*
 * Define button to switched on/off
 */
    eval->hbox_monitor = gtk_hbox_new (FALSE, 0);
    gtk_widget_ref (eval->hbox_monitor);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
			     "hbox_monitor",
			     eval->hbox_monitor,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->hbox_monitor);
    gtk_table_attach (GTK_TABLE (eval->table_monitor),
		     eval->hbox_monitor,
		     0,
		     2,
		     2,
		     3,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);

    eval->checkbutton_monitor =
	gtk_check_button_new_with_label ( _("Monitor"));
    gtk_widget_ref (eval->checkbutton_monitor);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "checkbutton_monitor",
			     eval->checkbutton_monitor,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->checkbutton_monitor);
    gtk_box_pack_start (GTK_BOX (eval->hbox_monitor), 
		       eval->checkbutton_monitor, 
		       TRUE, 
		       TRUE,
		       0);

    gtk_object_set_data (GTK_OBJECT (eval->checkbutton_monitor), 
			"eval",
			eval);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_monitor),
		       "enter",
		       G_CALLBACK (on_checkbutton_piv_monitor_enter), 
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_monitor),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->checkbutton_monitor),
		       "clicked",
		       G_CALLBACK (on_checkbutton_piv_monitor),
		       NULL);



/*
 * spinner for zoom
 */
     eval->label_monitor_zoom = gtk_label_new ( _("Zoom:"));
    gtk_widget_ref (eval->label_monitor_zoom);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "eval_label_monitor_zoom",
			     eval->label_monitor_zoom,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_monitor_zoom);
    gtk_box_pack_start (GTK_BOX (eval->hbox_monitor), 
		       eval->label_monitor_zoom, 
		       TRUE, 
		       TRUE,
		       0);



    eval->spinbutton_adj_monitor_zoom =
	gtk_adjustment_new (gpiv_var->piv_disproc_zoom,
			   0.5,
			   8,
			   0.5,
			   2,
			   8);



    eval->spinbutton_monitor_zoom =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_monitor_zoom),
			    1,
			    1);
    gtk_widget_ref (eval->spinbutton_monitor_zoom);
    gtk_widget_show (eval->spinbutton_monitor_zoom);
    gtk_box_pack_start (GTK_BOX (eval->hbox_monitor), 
		       eval->spinbutton_monitor_zoom, 
		       TRUE, 
		       TRUE,
		       0);
    gtk_entry_set_editable (GTK_ENTRY (eval->spinbutton_monitor_zoom),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (eval->spinbutton_monitor_zoom),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_monitor_zoom), 
			"eval",
			eval);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_monitor_zoom),
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_monitor_zoom),
		       eval->spinbutton_monitor_zoom);



/*
 * spinner for vector length
 */
     eval->label_monitor_vectorscale = gtk_label_new ( _("Vector scale:"));
    gtk_widget_ref (eval->label_monitor_vectorscale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "eval_label_monitor_vectorscale",
			     eval->label_monitor_vectorscale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->label_monitor_vectorscale);
    gtk_box_pack_start (GTK_BOX (eval->hbox_monitor), 
		       eval->label_monitor_vectorscale, 
		       TRUE, 
		       TRUE,
		       0);



    eval->spinbutton_adj_monitor_vectorscale =
	gtk_adjustment_new (gpiv_var->piv_disproc_vlength,
			   1,
			   32,
			   1,
			   4,
			   32);



    eval->spinbutton_monitor_vectorscale =
	gtk_spin_button_new (GTK_ADJUSTMENT (eval->spinbutton_adj_monitor_vectorscale),
			    1,
			    0);
    gtk_widget_ref (eval->spinbutton_monitor_vectorscale);
    gtk_widget_show (eval->spinbutton_monitor_vectorscale);
    gtk_box_pack_start (GTK_BOX (eval->hbox_monitor), 
		       eval->spinbutton_monitor_vectorscale, 
		       TRUE, 
		       TRUE,
		       0);
    gtk_entry_set_editable (GTK_ENTRY (eval->spinbutton_monitor_vectorscale),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (eval->spinbutton_monitor_vectorscale),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (eval->spinbutton_monitor_vectorscale), 
			"eval",
			eval);
    g_signal_connect (GTK_OBJECT (eval->spinbutton_monitor_vectorscale),
		       "changed",
		       G_CALLBACK (on_spinbutton_piv_monitor_vectorscale),
		       eval->spinbutton_monitor_vectorscale);



/*
 * PIV activating button
 */
    eval->button = gtk_button_new_with_label ( _("piv"));
    gtk_widget_ref (eval->button);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "button",
			     eval->button,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (eval->button);
    gtk_table_attach (GTK_TABLE (eval->table),
		     eval->button, 
		     0, 
		     2, 
		     7, 
		     8,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 eval->button,
			  _("Interrogates a PIV image (pair) resulting into estimators of the mean displacement of the particle images within each interrogation area"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (eval->button),
			"eval",
			eval);
    g_signal_connect (GTK_OBJECT (eval->button),
		       "enter",
		       G_CALLBACK (on_button_piv_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->button),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (eval->button),
		       "clicked",
		       G_CALLBACK (on_button_piv),
		       NULL);


/*
 * Setting initial values
 */


/*     if (gl_piv_par->int_size_f == 8) { */
/* 	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON */
/* 				    (eval->radiobutton_intsize_f_1),
				    TRUE); */
/*     } */


    if (gl_piv_par->int_size_f == 16) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intsize_f_2),
                                    TRUE);
    }


     if (gl_piv_par->int_size_f == 32) {
       gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				   (eval->radiobutton_intsize_f_3),
				   TRUE);
     }


    if (gl_piv_par->int_size_f == 64) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intsize_f_4),
				    TRUE);
    }


    if (gl_piv_par->int_size_f == 128) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intsize_f_5),
				    TRUE);
    }



/*     if (gl_piv_par->int_size_i == 8) { */
/* 	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON */
/* 				    (eval->radiobutton_intsize_i_1),
				    TRUE); */
/*     } */


    if (gl_piv_par->int_size_i == 16) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intsize_i_2),
				    TRUE);
    }


    if (gl_piv_par->int_size_i == 32) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intsize_i_3),
				    TRUE);
    }


    if (gl_piv_par->int_size_i == 64) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intsize_i_4),
				    TRUE);
    }


    if (gl_piv_par->int_size_i == 128) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intsize_i_5),
				    TRUE);
    }




    if (gl_piv_par->int_shift == 8) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intshift_1),
				    TRUE);
    }


    if (gl_piv_par->int_shift == 16) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intshift_2),
				    TRUE);
    }


    if (gl_piv_par->int_shift == 32) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intshift_3),
				    TRUE);
    }


    if (gl_piv_par->int_shift == 64) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intshift_4),
				    TRUE);
    }


    if (gl_piv_par->int_shift == 128) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_intshift_5),
				    TRUE);
    }


    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				 (eval->radiobutton_mouse_0), 
				 TRUE);


    if (gl_piv_par->ifit == 0) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_fit_none),
				    TRUE);
    }


    if (gl_piv_par->ifit == 1) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_fit_gauss),
				    TRUE);
    }


    if (gl_piv_par->ifit == 2) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_fit_power),
				    TRUE);
    }


    if (gl_piv_par->ifit == 3) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (eval->radiobutton_fit_gravity),
				    TRUE);
    }


/*   if (ifit == -1) { */
/*     gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (eval->radiobutton_fit_marquardt),
       TRUE); */
/*   } */


    if (gl_piv_par->peak == 1) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_peak_1),
                                     TRUE);
    }
    
    
    if (gl_piv_par->peak == 2) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_peak_2),
                                     TRUE);
    }
    
    if (gl_piv_par->peak == 3) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_peak_3),
                                     TRUE);
    }
    
    if (gl_piv_par->int_scheme == GPIV_LK_WEIGHT) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_weightkernel),
                                     TRUE);
    }
    
    if (gl_piv_par->int_scheme == GPIV_ZERO_OFF_FORWARD) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_zerooff),
                                     TRUE);
    }
    
    if (gl_piv_par->int_scheme == GPIV_ZERO_OFF_CENTRAL) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_centraldiff),
                                     TRUE);
    }
    
    if (gl_piv_par->int_scheme == GPIV_IMG_DEFORM) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_imgdeform),
                                     TRUE);
    }
    
    if (gl_image_par->x_corr == 0) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_cross_1),
                                     TRUE);
    } else {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (eval->radiobutton_cross_2),
                                     TRUE);
    }
    

    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                 (eval->checkbutton_weight_ia),
                                 gl_piv_par->gauss_weight_ia);

    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                 (eval->checkbutton_spof),
                                 gl_piv_par->spof_filter);

    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                 (eval->checkbutton_monitor),
                                 gpiv_par->display__process);
    

    return eval;
}
