/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * PIV evaluation
 *  $Log: piveval_interface.h,v $
 *  Revision 1.12  2008-04-28 12:00:57  gerber
 *  hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 *  Revision 1.11  2007-11-23 16:24:08  gerber
 *  release 0.5.0: Kafka
 *
 *  Revision 1.10  2007-06-06 17:00:48  gerber
 *  Retreives images/data from URI using Gnome Virtual File System.
 *
 *  Revision 1.9  2007-02-16 17:09:49  gerber
 *  added Gauss weighting on I.A. and SPOF filtering (on correlation function)
 *
 *  Revision 1.8  2007-01-29 11:27:44  gerber
 *  added image formats png, gif, tif png, bmp, improved buffer display
 *
 *  Revision 1.7  2006/01/31 14:28:12  gerber
 *  version 0.3.0
 *
 *  Revision 1.5  2005/01/19 15:53:42  gerber
 *  Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 *  by using RTAI and Realtime Linux, recording images from IEEE1394
 *  (Firewire) IIDC compliant camera's
 *
 *  Revision 1.4  2004/10/15 19:24:05  gerber
 *  GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 *  Revision 1.3  2004/06/14 21:19:23  gerber
 *  Image depth up to 16 bits.
 *  Improvement "single int" and "drag int" in Eval tab.
 *  Viewer's pop-up menu.
 *  Adaption for gpiv_matrix_* and gpiv_vector_*.
 *  Resizing console.
 *  See Changelog for further info.
 *
 *  Revision 1.2  2003/09/01 11:17:15  gerber
 *  improved monitoring of interrogation process
 *
 *  Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 *  Imported gpiv
 *
 */

#ifndef GPIV_PIVEVAL_INTERFACE_H
#define GPIV_PIVEVAL_INTERFACE_H

typedef struct _Monitor Monitor;
struct _Monitor {
  gint int_size_old;
  gint rgb_int_width;
  
  guchar *rgbbuf_int1;
  GnomeCanvasItem *gci_int1;
  GnomeCanvasItem *gci_int1_background;
  
  guchar *rgbbuf_int2;
  GnomeCanvasItem *gci_int2;
  GnomeCanvasItem *gci_int2_background;
  
  gint cov_size_old;
  gint rgb_cov_width;
  guchar *rgbbuf_cov;
  GnomeCanvasItem *gci_cov;
  GnomeCanvasItem *gci_background_cov;
  
  GpivPivData *pi_da;
  GnomeCanvasItem *gci_vec_background;
  GnomeCanvasItem *gci_vec;
  
  gint vector_scale;
  gfloat zoom_factor;
  /* const  */double affine[6];
  /* const  */double affine_vl[6];
};



typedef struct _PivEval PivEval;
struct _PivEval {
  GtkWidget *vbox_label;
  GtkWidget *label_title;

  GtkWidget *vbox_scroll;
  GtkWidget *scrolledwindow;
  GtkWidget *viewport;

  GtkWidget *vbox_viewport1;
  GtkWidget *table;
  GtkWidget *table_aoi;
  GtkWidget *label_colstart;
  GtkWidget *label_colend;
  GtkWidget *label_preshiftcol;
  GtkWidget *label_rowstart;
  GtkWidget *label_rowend;
  GtkWidget *label_preshiftrow;
  GtkObject *spinbutton_adj_colstart;
  GtkWidget *spinbutton_colstart;
  GtkObject *spinbutton_adj_colend;
  GtkWidget *spinbutton_colend;
  GtkObject *spinbutton_adj_preshiftcol;
  GtkWidget *spinbutton_preshiftcol;
  GtkObject *spinbutton_adj_rowstart;
  GtkWidget *spinbutton_rowstart;
  GtkObject *spinbutton_adj_rowend;
  GtkWidget *spinbutton_rowend;
  GtkObject *spinbutton_adj_preshiftrow;
  GtkWidget *spinbutton_preshiftrow;

  GtkWidget *hbox_intreg;
  GtkWidget *frame_2;
  GtkWidget *vbox_intsize_f;
  GSList *int_size_f_group;
  GtkObject *spinbutton_adj_intsize_f;
  GtkWidget *spinbutton_intsize_f;
/* GtkWidget *radiobutton_intsize_f_1; */
  GtkWidget *radiobutton_intsize_f_2;
  GtkWidget *radiobutton_intsize_f_3;
  GtkWidget *radiobutton_intsize_f_4;
  GtkWidget *radiobutton_intsize_f_5;
  
  GtkWidget *frame_3;
  GtkWidget *vbox_intsize_i;
  GSList *int_size_i_group/*  = NULL */;
  GtkObject *spinbutton_adj_intsize_i;
  GtkWidget *spinbutton_intsize_i;
  /* GtkWidget *radiobutton_intsize_i_1; */
  GtkWidget *radiobutton_intsize_i_2;
  GtkWidget *radiobutton_intsize_i_3;
  GtkWidget *radiobutton_intsize_i_4;
  GtkWidget *radiobutton_intsize_i_5;
  
  GtkWidget *frame_4;
  GtkWidget *vbox_shift;
  GSList *int_shift_group;
  GtkObject *spinbutton_adj_intshift;
  GtkWidget *spinbutton_intshift;
  GtkWidget *radiobutton_intshift_1;
  GtkWidget *radiobutton_intshift_2;
  GtkWidget *radiobutton_intshift_3;
  GtkWidget *radiobutton_intshift_4;
  GtkWidget *radiobutton_intshift_5;

  GtkWidget *frame_1;
  GtkWidget *vbox_mouseselect;
/*
 * GSList *mouse_sel_group; moved to console
 */
  GtkWidget *radiobutton_mouse_0;
  GtkWidget *radiobutton_mouse_1;
  GtkWidget *radiobutton_mouse_2;
  GtkWidget *radiobutton_mouse_3;
  GtkWidget *radiobutton_mouse_4;
  GtkWidget *radiobutton_mouse_5;
  GtkWidget *radiobutton_mouse_6;
  
/*----------------------------------------------------------------
 * Monitor 
 */
  Monitor *monitor;
  GtkWidget *frame_monitor;
  GtkWidget *table_monitor;

  GtkWidget *frame_monitor_int1;
  GtkWidget *canvas_monitor_int1;

  GtkWidget *frame_monitor_int2;
  GtkWidget *canvas_monitor_int2;

  GtkWidget *frame_monitor_cov;
  GtkWidget *canvas_monitor_cov;

  GtkWidget *frame_monitor_vec;
  GtkWidget *canvas_monitor_vec;


  GtkWidget *hbox_monitor;
  GtkWidget *checkbutton_monitor;
  GtkWidget *label_monitor_zoom;
  GtkObject *spinbutton_adj_monitor_zoom;
  GtkWidget *spinbutton_monitor_zoom;
  GtkWidget *label_monitor_vectorscale;
  GtkObject *spinbutton_adj_monitor_vectorscale;
  GtkWidget *spinbutton_monitor_vectorscale;

/*----------------------------------------------------------------*/
  
  GtkWidget *frame_5;
  GtkWidget *vbox10;
  GSList *vbox10_group;
  GtkWidget *radiobutton_fit_none;
  GtkWidget *radiobutton_fit_gauss;
  GtkWidget *radiobutton_fit_power;
  GtkWidget *radiobutton_fit_gravity;
  GtkWidget *frame_6;
  GtkWidget *vbox11;
  GSList *vbox11_group;
  GtkWidget *radiobutton_peak_1;
  GtkWidget *radiobutton_peak_2;
  GtkWidget *radiobutton_peak_3;
  GtkWidget *frame_7;
  GtkWidget *vbox12;
  GSList *vbox12_group;
  GtkWidget *radiobutton_imgdeform;
  GtkWidget *radiobutton_centraldiff;
  GtkWidget *radiobutton_zerooff;
  GtkWidget *radiobutton_weightkernel;
  GtkWidget *frame_8;
  GtkWidget *vbox13;
  GSList *vbox13_group;
  GtkWidget *radiobutton_cross_1;
  GtkWidget *radiobutton_cross_2;
  GtkWidget *checkbutton_weight_ia;
  GtkWidget *checkbutton_spof;
  GtkWidget *button;
};


PivEval *
create_piveval (GnomeApp *main_window, 
		GtkWidget *container);


#endif /* GPIV_PIVEVAL_INTERFACE_H */
