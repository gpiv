/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/* $Log: piveval_interrogate.c,v $
/* Revision 1.4  2008-10-09 15:19:03  gerber
/* Release 0.6.0 using paralellized code
/*
/* Revision 1.3  2008-10-09 14:43:37  gerber
/* paralellized with OMP and MPI
/*
/* Revision 1.2  2008-09-25 13:32:22  gerber
/* Adapted for use on cluster (using MPI/OMP) paralellised gpiv_rr from gpivtools)
/*
/* Revision 1.1  2008-09-16 10:17:36  gerber
/* added piveval_interrogate routines
/*
 */

#include "gpiv_gui.h"
/* #include "utils.h" */
#include "piveval.h"
#include "piveval_interrogate.h"

#undef USE_MTRACE
#ifdef USE_MTRACE
#include <mcheck.h>
#endif


static void
report_progress (const guint index_y,
                 const guint index_x,
                 gdouble *progress_value_prev,
                 const GpivPivData *piv_data,
                 const GpivPivPar *piv_par,
                 const guint sweep,
                 const gfloat cum_residu,
                 const GpivConsole *gpiv,
                 const Display *disp
                 );

static GpivPivData *
alloc_pivdata_gridgen (const GpivImagePar *image_par, 
                       const GpivPivPar *piv_par
                       );

static gchar *
update_pivdata_imgdeform_zoff (const GpivImage *image, 
                               GpivImage *lo_image, 
                               const GpivPivPar *piv_par, 
                               const GpivValidPar *valid_par, 
                               GpivPivData *piv_data, 
                               GpivPivData *lo_piv_data, 
                               gfloat *cum_residu, 
                               gboolean *cum_residu_reached,
                               gfloat *sum_dxdy, 
                               gfloat *sum_dxdy_old,
                               gboolean isi_last,
                               gboolean grid_last,
                               gboolean sweep_last,
                               gboolean verbose
                               );

#ifdef ENABLE_MPI
static GpivPivData *
exec_piv_mpi (GpivImage *image, 
              GpivPivPar *piv_par, 
              GpivValidPar *valid_par,
              GpivConsole *gpiv
              );
#endif /* ENABLE_MPI */

/*
 * Program-wide public piv interrogation functions
 */


void 
exec_piv (GpivConsole *gpiv
         )
/*-----------------------------------------------------------------------------
 * Performs the execution of image interrogation for PIV
 */
{
    char *err_msg = NULL;
    char message[2 * GPIV_MAX_CHARS];
    guint nx = 0, ny = 0;


    if (display_act == NULL 
        || display_act->img->exist_img == FALSE 
        || cancel_process) {
	err_msg =
	    _("At first, open an image. \n"
              "Than we'll further see what will happen.");
	g_warning (err_msg);
	warning_gpiv (err_msg);
        return;
    }

/*
 * Free memory of pivdata and clean the display from its vectors
 */
    if (display_act->pida->exist_piv
/*             && (m_select != SINGLE_AREA_MS */
/*                 || m_select != DRAG_AREA_MS) */
        ) {
        destroy_all_vectors (display_act->pida);
        gpiv_free_pivdata (display_act->pida->piv_data);
        display_act->pida->exist_piv = FALSE;
        display_act->pida->averaged_piv = FALSE;
        if (display_act->pida->scaled_piv) {
            gpiv_free_pivdata (display_act->pida->piv_data_scaled);
            display_act->pida->scaled_piv = FALSE;
        }
    }

/*
 * Free eventually existing memory of vor_data and cleanup the display 
 * as they do not belong to the piv data anymore
 */
    free_post_bufmems (display_act);

    exec_process = TRUE;

/*
 * Set mouse selection to None for using correct AOI
 */
/*         if (m_select != NO_MS) { */
/*             gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON */
/*                                          (gpiv->piveval->radiobutton_mouse_1),  */
/*                                          TRUE); */
/*         } */

/*
 * Setting interrogation scheme to GPIV_ZERO_OFF_CENTRAL if image deformation 
 * is impossible with too small (initial or final) grid.
 */
    if (gl_piv_par->int_scheme == GPIV_IMG_DEFORM) {
        gpiv_piv_count_pivdata_fromimage (display_act->img->image->header,
                                          gl_piv_par, &nx, &ny);
        if (nx  < 2 || ny < 2) {
            g_snprintf (message, 2 * GPIV_MAX_CHARS, 
                        _("Image deformation is impossibe with grid of nx = %d ny =%d.\n\
Setting Interrogation scheme to Central difference.\n                   \
This will be reset automatically."),
                        nx, ny);
            warning_gpiv ("%s", message);
            gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                         (gpiv->piveval->radiobutton_centraldiff), 
                                         TRUE);
            int_scheme_autochanged = TRUE;
        }
    }

/*
 * checking parameters and interrogating image by local function
 */
    display_act->pida->piv_par = gpiv_piv_cp_parameters (gl_piv_par);
    if ((err_msg = 
         gpiv_piv_testadjust_parameters (display_act->img->image->header,
                                         display_act->pida->piv_par))
        != NULL) {
        warning_gpiv ("%s", err_msg);
        return;
    }
    
    display_act->pida->valid_par = gpiv_valid_cp_parameters (gl_valid_par);
    gpiv_valid_print_parameters (stdout, display_act->pida->valid_par);
    if ((err_msg = 
         gpiv_valid_testadjust_parameters (display_act->pida->valid_par))
        != NULL) {
        warning_gpiv ("%s", err_msg);
        return;
    }
    gpiv_valid_print_parameters (stdout, display_act->pida->valid_par);

#ifdef USE_LIBGPIV_INTERR
#undef USE_LIBGPIV_INTERR
#endif

    if ((display_act->pida->piv_data =
#ifdef ENABLE_MPI
/* Calling extern tool gpiv_rr from gpivtools with MPI enabled */
         exec_piv_mpi(display_act->img->image, 
                      display_act->pida->piv_par, 
                      display_act->pida->valid_par,
                      gpiv)

#elif USE_LIBGPIV_INTERR
/* Libgpiv's function for image interrogation */
         gpiv_piv_interrogate_img (display_act->img->image, 
                                   display_act->pida->piv_par, 
                                   display_act->pida->valid_par, 
                                   TRUE)
#else
/* Gpiv's function for image interrogation */
         interrogate_img (display_act->img->image, 
                          display_act->pida->piv_par, 
                          display_act->pida->valid_par, 
                          gpiv)
#endif
         ) == NULL) {
        warning_gpiv ("exec_piv: failing interrogate_img");
        return;
    }

#ifdef USE_LIBGPIV_INTERR
#undef USE_LIBGPIV_INTERR
#endif

    display_act->pida->exist_piv = TRUE;

/*
 *  Adding some comment to piv_data
 */
    display_act->pida->piv_data->comment = 
        g_strdup_printf ("# Software: %s %s\n", PACKAGE, VERSION);
    display_act->pida->piv_data->comment = 
        gpiv_add_datetime_to_comment (display_act->pida->piv_data->comment);
    display_act->pida->piv_data->comment = 
        g_strconcat (display_act->pida->piv_data->comment, 
                     "# Data type: Particle Image Velocities\n", NULL);

/*
 * Drawing and displaying PIV vectors
 */
    if (gl_piv_par->int_geo == GPIV_POINT
        || m_select == SINGLE_AREA_MS
        || m_select == SINGLE_POINT_MS 
        || m_select == DRAG_MS) {
    } else {
        if (display_act->display_piv) {
            create_all_vectors (display_act->pida);
        }
        display_act->display_piv = TRUE;
    }

/*
 * Some settings for displaying features
 * Update vectors to correct for colors/gray-scale
 */
    display_act->pida->scaled_piv = FALSE;
    display_act->pida->saved_piv = FALSE;
    display_act->pida->averaged_piv = FALSE;
    display_act->pida->exist_cov = TRUE;
    update_all_vectors (display_act->pida);
    exec_process = FALSE;

/*
 * Resetting interrogation scheme
 */
    if (int_scheme_autochanged) {
        int_scheme_autochanged = FALSE;
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (gpiv->piveval->radiobutton_imgdeform), 
                                     TRUE);
    }
    
    gtk_progress_set_value (GTK_PROGRESS (gnome_appbar_get_progress
                                          (GNOME_APPBAR (gpiv->appbar))), 
                            0.0);

}


GpivPivData *
interrogate_img (const GpivImage *image, 
                 const GpivPivPar *piv_par,
                 const GpivValidPar *valid_par,
                 GpivConsole *gpiv
                 )
/* ----------------------------------------------------------------------------
 * PIV interrogation of an image pair at an entire grid or single point
 * Similar to gpiv_piv_interr_img, but adapted for the GUI
 */
{
    GpivPivData *piv_data = NULL;       /* piv data to be returned */
    gchar *err_msg = NULL;              /* error message */
    guint index_x = 0, index_y = 0;     /* array indices */

    /*
     * Local variables with prefix lo_ to distinguish from global or from 
     * parameter list
     */
    GpivImage *lo_image = NULL;         /* local image that might be deformed */
    GpivPivData *lo_piv_data = NULL;    /* local piv data */
    GpivPivPar *lo_piv_par = NULL;      /* local piv parameters */
    
    gfloat **intreg1 = display_act->pida->intreg1; /* first interrogation area */
    gfloat **intreg2 = display_act->pida->intreg2; /* second interrogation area */
    guint int_size_0;                   /* zero-padded interrogation area size */

    GpivCov *cov = NULL;                /* covariance */
    guint sweep = 1;                    /* itaration counter */
    gboolean grid_last = FALSE;         /* flag if final grid refinement has been reached */
    gboolean isi_last = FALSE;          /* flag if final interrogation area shift has been reached */
    gboolean cum_residu_reached = FALSE;/* flag if max. cumulative residu has been reached */
    gboolean sweep_last = FALSE;        /* perform the last iteration sweep */
    gboolean sweep_stop = FALSE;        /* stop the current iteration at the end */
    gfloat sum_dxdy = 0.0, sum_dxdy_old = 0.0;  /* */
    gfloat cum_residu = 914.6;          /* initial, large, arbitrary cumulative residu */
    gdouble progress = 0.0;             /* for monitoring calculation progress */


    /*
     * Testing parameters on consistency and initializing derived 
     * parameters/variables
     */
    if ((err_msg = 
         gpiv_piv_testonly_parameters (image->header, piv_par))
        != NULL) {
        warning_gpiv ("%s", err_msg);
        return NULL;
    }

    if ((err_msg = 
         gpiv_valid_testonly_parameters (valid_par))
        != NULL) {
        warning_gpiv ("%s", err_msg);
        return NULL;
    }

    /*
     * Local (actualized) parameters
     * Setting initial parameters and variables for adaptive grid and 
     * Interrogation Area dimensions
     */
    lo_piv_par = gpiv_piv_cp_parameters (piv_par);

    if (lo_piv_par->int_scheme == GPIV_ZERO_OFF_FORWARD
        || lo_piv_par->int_scheme == GPIV_ZERO_OFF_CENTRAL
	|| lo_piv_par->int_scheme == GPIV_IMG_DEFORM
        || lo_piv_par->int_size_i > lo_piv_par->int_size_f) {
        lo_piv_par->int_size_f = lo_piv_par->int_size_i;
        sweep_last = FALSE;
    } else {
        sweep_last = TRUE;
    }
    
    if (lo_piv_par->int_shift < lo_piv_par->int_size_i / GPIV_SHIFT_FACTOR) {
        lo_piv_par->int_shift = lo_piv_par->int_size_i / GPIV_SHIFT_FACTOR;
    }
    
    /*
     * A copy of the image and PIV data are needed when image deformation is used.
     * To keep the algorithm simple, copies are made unconditionally.
     */
    lo_image = gpiv_cp_img (image);
    piv_data = alloc_pivdata_gridgen (image->header, lo_piv_par);
    lo_piv_data = gpiv_cp_pivdata (piv_data);
    gpiv_0_pivdata (lo_piv_data);
    
    /*
     * Reads eventually existing fftw wisdom
     */
    gpiv_fread_fftw_wisdom(1);
    gpiv_fread_fftw_wisdom(-1);

#ifdef USE_MTRACE
    mtrace();
#endif

    while (sweep <= GPIV_MAX_PIV_SWEEP 
           && !sweep_stop 
           && !cancel_process) {

        /*
         * Memory allocation of interrogation area's and covariance. 
         * These memory chunks are allocated here to optimize calculation 
         * speed and for eventually monitoring their contents.
         */
        int_size_0 = GPIV_ZEROPAD_FACT * lo_piv_par->int_size_i;
	intreg1 = gpiv_matrix (int_size_0, int_size_0);
	intreg2 = gpiv_matrix (int_size_0, int_size_0);
        cov = gpiv_alloc_cov (int_size_0, image->header->x_corr);
        display_act->pida->cov = cov;
        
        /*
         * Interrogates a single interrogation area
         */
        if (m_select != SINGLE_AREA_MS 
            && m_select != DRAG_MS) {
            destroy_all_vectors (display_act->pida);
            gnome_canvas_update_now (GNOME_CANVAS (display_act->canvas));
        }
        
        if (lo_piv_par->int_geo == GPIV_POINT
            || m_select == SINGLE_AREA_MS
            || m_select == SINGLE_POINT_MS 
            || m_select == DRAG_MS) {

            if ((err_msg = 
                 gpiv_piv_interrogate_ia (m_select_index_y,
                                          m_select_index_x,
                                          lo_image,
                                          lo_piv_par,
                                          sweep, 
                                          sweep_last, 
                                          intreg1,
                                          intreg2,
                                          cov,
                                          lo_piv_data
                                          ))
                != NULL) {
                gpiv_free_img (lo_image);
                gpiv_free_pivdata (lo_piv_data);
                gpiv_free_pivdata (piv_data);
 	        gpiv_free_matrix (intreg1);
	        gpiv_free_matrix (intreg2);
                gpiv_free_cov (cov);
                error_gpiv ("interrogate_img: %s", err_msg);
            }

            /*
             * display piv values, draw interrogation areas and 
             * covariance function
             */
            if (gpiv_var->piv_disproc == TRUE) {
                display_piv_vector (m_select_index_y, 
                                    m_select_index_x, 
                                    piv_data, 
                                    gpiv->piveval);
                display_img_intreg1 (intreg1,
                                     lo_piv_par->int_size_i,
                                     gpiv->piveval);
                display_img_intreg2 (intreg2, 
                                     lo_piv_par->int_size_i, 
                                     gpiv->piveval);
                display_img_cov (cov,
                                 lo_piv_par->int_size_i, 
                                 gpiv->piveval);
            }


	} else {

            /*
             * Interrogates at a rectangular grid of points within the Area Of
             * Interest of the image
             */
            for (index_y = 0; index_y < lo_piv_data->ny; index_y++) {
                for (index_x = 0; index_x < lo_piv_data->nx; index_x++) {
                    if (cancel_process) break;

                    /*
                     * Interrogates a single interrogation area.
                     */
                    if ((err_msg = 
                         gpiv_piv_interrogate_ia (index_y, 
                                                  index_x, 
                                                  lo_image,
                                                  lo_piv_par,
                                                  sweep, 
                                                  sweep_last,
                                                  intreg1,
                                                  intreg2,
                                                  cov,
                                                  lo_piv_data
                                                  ))
                        != NULL) {
                        gpiv_free_img (lo_image);
                        gpiv_free_pivdata (lo_piv_data);
                        gpiv_free_pivdata (piv_data);
 	                gpiv_free_matrix (intreg1);
	                gpiv_free_matrix (intreg2);
                        gpiv_free_cov (cov);
                        error_gpiv ("interrogate_img: %s", err_msg);
                    }

#ifdef DEBUG
                    gpiv_warning("interrogate_img:: back from gpiv_piv_interr_ia: sweep=%d x[%d][%d]=%f y[%d][%d]=%f dx[%d][%d]=%f dy[%d][%d]=%f snr=%f p_no=%d",
                                 sweep,
                                 index_y, index_x, lo_piv_data->point_x[index_y][index_x],
                                 index_y, index_x, lo_piv_data->point_y[index_y][index_x],
                                 index_y, index_x, lo_piv_data->dx[index_y][index_x],
                                 index_y, index_x, lo_piv_data->dy[index_y][index_x],
                                 lo_piv_data->snr[index_y][index_x],
                                 lo_piv_data->peak_no[index_y][index_x]
                                 );
#endif
                    /*
                     * Printing the progress of processing 
                     */
                    report_progress (index_y,
                                     index_x,
                                     &progress,
                                     piv_data,
                                     lo_piv_par,
                                     sweep,
                                     cum_residu,
                                     gpiv,
                                     display_act
                                     );
                    /*
                     * Draw interrogation areas, covariance function, 
                     * display piv vector to monitor the process.
                     * Includes report_rogress to include 
                     * as an argument in gpiv_piv_interr_img
                     */
                    if (gpiv_var->piv_disproc == TRUE) {
                        display_piv_vector (index_y, 
                                            index_x,
                                            piv_data, 
                                            gpiv->piveval);
                        display_img_intreg1 (intreg1,
                                             lo_piv_par->int_size_i, 
                                             gpiv->piveval);
                        display_img_intreg2 (intreg2,
                                             lo_piv_par->int_size_i, 
                                             gpiv->piveval);
                        display_img_cov (cov,
                                         lo_piv_par->int_size_i, 
                                         gpiv->piveval);
                    }


                }
            }

        }

        /*
         * De-allocating memory: other (smaller) sizes are eventually needed 
         * for a next iteration sweep
         */
	gpiv_free_matrix (intreg1);
	gpiv_free_matrix (intreg2);
        gpiv_free_cov (cov);

	if (sweep_last) {
            sweep_stop = TRUE;
        }

        if ((lo_piv_par->int_scheme == GPIV_IMG_DEFORM
             || lo_piv_par->int_scheme == GPIV_ZERO_OFF_FORWARD
             || lo_piv_par->int_scheme == GPIV_ZERO_OFF_CENTRAL)
            /* BUGFIX: crashes with single point */
            && (lo_piv_par->int_geo != GPIV_POINT
                && m_select != SINGLE_AREA_MS
                && m_select != SINGLE_POINT_MS 
                && m_select != DRAG_MS)
            ) {

            if ((err_msg = 
                 update_pivdata_imgdeform_zoff (image, lo_image,
                                                lo_piv_par, valid_par, 
                                                piv_data, lo_piv_data, 
                                                &cum_residu, 
                                                &cum_residu_reached,
                                                &sum_dxdy, &sum_dxdy_old,
                                                isi_last, grid_last, 
                                                sweep_last, gpiv_par->verbose))
                != NULL) {
                g_warning ("GPIV_PIV_INTERR_IMG: %s", err_msg);
                gpiv_free_img (lo_image);
                gpiv_free_pivdata (lo_piv_data);
                gpiv_free_pivdata (piv_data);
                return NULL;
            }

	} else {

            /*
             * Apply results to output piv_data
             */
            gpiv_free_pivdata (piv_data);
            piv_data = gpiv_cp_pivdata (lo_piv_data);
            cum_residu_reached = TRUE;
        }

        /*
         * Adapt grid. 
         * If final grid has been reached, grid_last will be set.
         */
        if (lo_piv_par->int_shift > piv_par->int_shift
            && !sweep_stop) {
            GpivPivData *pd = NULL;

            pd = gpiv_piv_gridadapt (image->header,
                                     piv_par, 
                                     lo_piv_par,
                                     piv_data, 
                                     sweep, 
                                     &grid_last);
            gpiv_free_pivdata (piv_data);
            piv_data = gpiv_cp_pivdata (pd);
            gpiv_free_pivdata (pd);

            gpiv_free_pivdata (lo_piv_data);
            lo_piv_data = gpiv_cp_pivdata (piv_data);

            if (lo_piv_par->int_scheme == GPIV_IMG_DEFORM) {
                gpiv_0_pivdata (lo_piv_data);
            }

        } else {
            grid_last = TRUE;
        }

        /*
         *  Adapt interrogation area size. 
         *  If final size has been reached, isi_last will be set.
         */
        gpiv_piv_isizadapt (piv_par, 
                            lo_piv_par, 
                            &isi_last);
        
        if (cum_residu_reached && isi_last && grid_last) {
            sweep_last = TRUE;
/*             if (lo_piv_par->int_scheme == GPIV_ZERO_OFF_FORWARD */
/*                 || lo_piv_par->int_scheme == GPIV_ZERO_OFF_CENTRAL) { */
/*                 lo_piv_par->ifit = piv_par->ifit; */
/*             } */
        }
        sweep++;
    }

    /*
     * Writes existing fftw wisdom
     */
    gpiv_fwrite_fftw_wisdom (1);
    gpiv_fwrite_fftw_wisdom (-1);
    fftw_forget_wisdom();
    fftw_cleanup ();

#ifdef USE_MTRACE
    muntrace();
#endif
    gpiv_free_img (lo_image);
    gpiv_free_pivdata (lo_piv_data);


    return piv_data;
}



/*
 * Private piv interrogation functions
 */
static GpivPivData *
alloc_pivdata_gridgen (const GpivImagePar *image_par, 
                       const GpivPivPar *piv_par
                       )
/*-----------------------------------------------------------------------------
 * Determining the number of grid points, allocating memory for output data
 * and calculate locations of Interrogation Area's. If ad_int is enabled, a
 * course grid is started with and adapted for subsequent sweeps.
 ----------------------------------------------------------------------------*/
{
    GpivPivData *piv_data = NULL;
    gchar *err_msg = NULL;
    GpivPivPar *lo_piv_par = NULL;
    guint nx, ny;


    if ((lo_piv_par = gpiv_piv_cp_parameters (piv_par)) == NULL) {
        gpiv_error ("alloc_pivdata_gridgen: failing gpiv_piv_cp_parameters");
    }

    if (piv_par->int_size_i > piv_par->int_size_f
        && piv_par->int_shift < piv_par->int_size_i / GPIV_SHIFT_FACTOR) { 
        lo_piv_par->int_shift = lo_piv_par->int_size_i / GPIV_SHIFT_FACTOR;
    }

    if ((err_msg = 
         gpiv_piv_count_pivdata_fromimage (image_par, lo_piv_par, &nx, &ny))
        != NULL) {
        warning_gpiv ("%s", err_msg);
        return NULL;
    }


    if ((piv_data = gpiv_piv_gridgen (nx, ny, image_par, lo_piv_par)) 
        == NULL) error_gpiv ("%s: %s", RCSID, err_msg);


    return piv_data;
}



static void
report_progress (const guint index_y,
                 const guint index_x,
                 gdouble *progress_value_prev,
                 const GpivPivData *piv_data,
                 const GpivPivPar *piv_par,
                 const guint sweep,
                 const gfloat cum_residu,
                 const GpivConsole *gpiv,
                 const Display *disp
                 )
/*-----------------------------------------------------------------------------
 * Calculates progress of interrogation processing and other variables 
 * and prints to message bar of the console if progress value has been changed
 */
{
    gchar progress_string[GPIV_MAX_CHARS];
    gdouble progress_value = 100 * (index_y * piv_data->nx + index_x +1) / 
        (piv_data->nx * piv_data->ny);


    if (progress_value != *progress_value_prev) {
        *progress_value_prev = progress_value;
        if (piv_par->int_scheme == GPIV_ZERO_OFF_FORWARD
            || piv_par->int_scheme == GPIV_ZERO_OFF_CENTRAL
            || piv_par->int_scheme == GPIV_IMG_DEFORM) {
            if (piv_par->int_size_i > piv_par->int_size_f /* substitutes ad_int == TRUE */
                ) {

                g_snprintf (progress_string, GPIV_MAX_CHARS, 
                            "Interrogating image #%d:"
                            " sweep #%d"
                            " size=%d"
                            " shift=%d" 
                            " residu=%.3f"
                            ,
                            disp->id,
                            sweep, 
                            piv_par->int_size_i, 
                            piv_par->int_shift,
                            cum_residu
                            );

            } else {

                g_snprintf (progress_string, GPIV_MAX_CHARS, 
                            "Interrogating image #%d:"
                            " sweep #%d"
                            " shift=%d"
                            " residu=%.3f"
                            ,
                            disp->id,
                            sweep,
                            piv_par->int_shift,
                            cum_residu
                            );
                        
            }
            

        } else {

            g_snprintf (progress_string, GPIV_MAX_CHARS, 
                        "Interrogating image #%d: "
                        " shift=%d "
                        ,
                        disp->id,
                        piv_par/* _dest */ ->int_shift
                        );
        }
        
        while (g_main_context_iteration (NULL, FALSE));
        gtk_progress_set_value (GTK_PROGRESS (gnome_appbar_get_progress
                                              (GNOME_APPBAR (gpiv->appbar))), 
                                progress_value);
        
        gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), 
                                 progress_string);
    }

}


static gchar *
update_pivdata_imgdeform_zoff (const GpivImage *image, 
                               GpivImage *lo_image, 
                               const GpivPivPar *piv_par, 
                               const GpivValidPar *valid_par, 
                               GpivPivData *piv_data, 
                               GpivPivData *lo_piv_data, 
                               gfloat *cum_residu, 
                               gboolean *cum_residu_reached,
                               gfloat *sum_dxdy, 
                               gfloat *sum_dxdy_old,
                               gboolean isi_last,
                               gboolean grid_last,
                               gboolean sweep_last,
                               gboolean verbose
                               )
/*-----------------------------------------------------------------------------
 * Validates and updates / renews pivdata and some other variables when image 
 * deformation or zero-offset interrogation scheme is used
 */
{

    gchar *err_msg = NULL;


    /*
     * Test on outliers
     */
    if ((err_msg = 
         gpiv_valid_errvec (lo_piv_data, 
                            image, 
                            piv_par,
                            valid_par, 
                            TRUE))
        != NULL) {
        return err_msg;
    }
            

    if (piv_par->int_scheme == GPIV_IMG_DEFORM) {

        /*
         * Update PIV estimators with those from the last interrogation
         * Resetting local PIV estimators for eventual next interrogation
         */
        if ((err_msg = gpiv_add_dxdy_pivdata (lo_piv_data, piv_data))
            != NULL) {
            return err_msg;
        }

        if ((err_msg = gpiv_0_pivdata (lo_piv_data))
            != NULL) {
            return err_msg;
        }

        /*
         * Deform image with updated PIV estimators.
         * First, copy local from original image.
         * Deform with newly updated PIV estimators.
         * Eventually write deformed image.
         */

        if ((err_msg = gpiv_cp_img_data (image, lo_image))
            != NULL) {
            return err_msg;
        }

        if ((err_msg = gpiv_imgproc_deform (lo_image, piv_data))
            != NULL) {
            return err_msg;
        }
/* #define DEBUG */
#ifdef DEBUG
        if (sweep_last && verbose) {
            printf ("\n");
            if ((err_msg = 
                 gpiv_piv_write_deformed_image (lo_image))
                != NULL) {
                return err_msg;
            }
        }
#endif /* DEBUG */
/* #undef DEBUG */

    } else {

        /*
         * Renew PIV estimators with those from the last interrogation
         */
        if ((err_msg = gpiv_0_pivdata (piv_data))
            != NULL) {
            return err_msg;
        }
        if ((err_msg = gpiv_add_dxdy_pivdata (lo_piv_data, piv_data))
            != NULL) {
            return err_msg;
        }
    }

    /*
     * Checking the relative cumulative residu for convergence
     * if final residu has been reached, cum_residu_reached will be set.
     */
    if (isi_last && grid_last) {
        *sum_dxdy_old = *sum_dxdy;
        *sum_dxdy = 0.0;
        gpiv_sum_dxdy_pivdata (piv_data, sum_dxdy);
        *cum_residu = fabsf ((*sum_dxdy - *sum_dxdy_old) / 
                            ((gfloat)piv_data->nx * (gfloat)piv_data->ny));
        if (*cum_residu < GPIV_CUM_RESIDU_MIN) {
            *cum_residu_reached = TRUE;
        }
    }


    return err_msg;
}


#ifdef ENABLE_MPI
static GpivPivData *
exec_piv_mpi (GpivImage *image, 
              GpivPivPar *piv_par, 
              GpivValidPar *valid_par,
              GpivConsole *gpiv
              )
/*-----------------------------------------------------------------------------
 * Interrogates an image on a distributed memory (Beowulf) cluster 
 * using gpiv_rr from the gpivtools package
 */
{
    GpivPivData *pd = NULL;
    FILE *fp = NULL;
    gchar *command = NULL;
    const gchar      *tmp_dir = g_get_tmp_dir ();
    const gchar      *username = g_get_user_name ();
    const gchar      *basename = g_get_prgname ();

    gchar progress_string[GPIV_MAX_CHARS];

    /*
     * Save image at tmp directory
     */
    gchar            *image_name = 
        g_strdup_printf ("%s/%s__%s%s" , tmp_dir, basename, username,
                         GPIV_EXT_PNG_IMAGE);
    gchar            *pivdata_name =
        g_strdup_printf ("%s/%s__%s%s" , tmp_dir, basename, username,
                         GPIV_EXT_PIV);
    gchar            *par_name =
        g_strdup_printf ("%s/%s__%s%s" , tmp_dir, basename, username,
                         GPIV_EXT_PAR);

/*
 * prints to message console bar
 */
    g_snprintf (progress_string, GPIV_MAX_CHARS, 
                "Interrogating image #%d using %s %d gpiv_rr",
                display_act->id, MPIRUN_CMD, gpiv_par->mpi_nodes);
    gnome_appbar_set_status (GNOME_APPBAR(gpiv->appbar), 
                             progress_string);
/*     gtk_progress_set_value (GTK_PULSE? (gnome_appbar_get_progress */
/*                                           (GNOME_APPBAR (gpiv->appbar))),  */
/*                             progress_value); */

    if (gpiv_par->verbose) g_message ("exec_piv_mpi: image name = %s, piv name = %s", 
               image_name, pivdata_name);

   if ((fp = fopen (image_name, "wb")) == NULL) {
        error_gpiv ("exec_piv_mpi: failing fopen %s\n probably user directory under %s does not exist", tmp_dir, image_name);
    }
    gpiv_write_png_image (fp, image, FALSE);
    fclose (fp);

    /* 
     * Run MPI-paralellised gpiv_rr
     */
    command = 
        g_strdup_printf ("%s %d gpiv_rr -p \
--cf %d --cl %d --cp %d \
--rf %d --rl %d --rp %d \
--ia_size_i %d --ia_size_f %d \
--ia_shift %d --ischeme %d \
--ifit %d --peak %d \
--val_r %d --val_s %d --val_t %f", 
                         MPIRUN_CMD, gpiv_par->mpi_nodes,
                         piv_par->col_start, piv_par->col_end, piv_par->pre_shift_col, 
                         piv_par->row_start, piv_par->row_end, piv_par->pre_shift_row,
                         piv_par->int_size_i, piv_par->int_size_f, 
                         piv_par->int_shift, piv_par->int_scheme, 
                         piv_par->ifit, piv_par->peak, 
                         valid_par->residu_type, valid_par->subst_type, valid_par->residu_max);

    if (piv_par->spof_filter) {
        command = g_strconcat (command, " --spof", NULL);
    }
    if (gpiv_par->verbose) {
        command = g_strconcat (command, " -p", NULL);
    }
    command = g_strconcat (command, " ", image_name, NULL);
    if (gpiv_par->verbose) g_message ("exec_piv_mpi: command = %s", command);

    if (system (command) != 0) {
        error_gpiv ("exec_piv_mpi: could not exec shell command:\n %s", command);
    }
    g_free (command);

    /*
     * Obtain PIV-data from tmp/
     */
    pd = gpiv_fread_pivdata(pivdata_name);

    /*
     * Cleanup image and data from tmp/
     */
    command = g_strdup_printf ("rm %s %s %s", image_name, pivdata_name, par_name);
    if (system (command) != 0) {
        error_gpiv ("exec_piv_mpi: could not exec shell command:\n %s", command);
    }

    g_free (command);
    g_free (image_name);
    g_free (pivdata_name);
    g_free (par_name);
    return pd;
}
#endif /* ENABLE_MPI */
