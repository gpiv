/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: piveval_interrogate.h,v $
 * Revision 1.1  2008-09-16 10:17:36  gerber
 * added piveval_interrogate routines
 *
 */

#ifndef GPIV_PIVEVAL_INTERROGATE_H
#define GPIV_PIVEVAL_INTERROGATE_H

void 
exec_piv (GpivConsole *gpiv
	  );

GpivPivData *
interrogate_img (const GpivImage *image, 
		 const GpivPivPar *piv_par,
		 const GpivValidPar *valid_par,
		 GpivConsole *gpiv
		 );


#endif /* GPIV_PIVEVAL_INTERROGATE_H */
