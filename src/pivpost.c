/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

/*
 * (callback) functions for Piv post processing window/tabulator
 * $Log: pivpost.c,v $
 * Revision 1.18  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.17  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.16  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.15  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.14  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.13  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.11  2005/02/26 09:43:30  gerber
 * parameter flags (parameter_logic) defined as gboolean
 *
 * Revision 1.10  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.9  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.8  2003/07/13 14:38:18  gerber
 * changed error handling of libgpiv
 *
 * Revision 1.7  2003/07/12 21:21:16  gerber
 * changed error handling libgpiv
 *
 * Revision 1.5  2003/07/10 11:56:07  gerber
 * added man page
 *
 * Revision 1.4  2003/07/04 10:47:01  gerber
 * cleaning up
 *
 * Revision 1.3  2003/07/03 17:08:02  gerber
 * display ruler adjusted for scaled data
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */


#include "gpiv_gui.h"
#include "utils.h"
#include "pivpost.h"
#include "display.h"


/*
 * Public post-processing functions
 */

void 
exec_scale (PivPost *pivpost
            )
/*-----------------------------------------------------------------------------
 * Scales PIV data on a new data set, adapts rulers
 */
{
    gchar *err_msg = NULL;

    if (display_act->pida->scaled_piv) 
        gpiv_free_pivdata (display_act->pida->piv_data_scaled);

    if ((display_act->pida->piv_data_scaled = 
        gpiv_cp_pivdata (display_act->pida->piv_data)) == NULL) {
        gpiv_error ("exec_scale: failing gpiv_alloc_pivdata");
    }

    if (display_act->pida->exist_piv && !cancel_process) {
        if ((err_msg = gpiv_post_scale (display_act->pida->piv_data_scaled,
                                        display_act->img->image->header)) 
            != NULL) {
            error_gpiv(_("exec_scale: Failure calling gpiv_post_scale"));
        }
        display_act->pida->scaled_piv = TRUE;
        
/*
 *  Adding some comment to the scaled piv_data
 */
    display_act->pida->piv_data_scaled->comment = 
        g_strdup_printf ("# Software: %s %s\n", PACKAGE, VERSION);
    display_act->pida->piv_data_scaled->comment = 
        gpiv_add_datetime_to_comment (display_act->pida->piv_data_scaled->comment);
    display_act->pida->piv_data_scaled->comment = 
        g_strconcat (display_act->pida->piv_data_scaled->comment, 
                     "# Data type: time and spatial scaled Particle Image Velocities\n", NULL);


#ifdef Rulers
/*
 * Adjusting display rulers
 */
        set__hrulerscale (display_act);
        set__vrulerscale (display_act);
#endif
        
        free_post_bufmems (display_act);
    } else {
        warning_gpiv (_("no PIV data"));
    }

}



void 
exec_savg (PivPost *pivpost
           )
/*-----------------------------------------------------------------------------
 * Calculates spatial average
 */
{
    gchar *err_msg = NULL;


    display_act->pida->averaged_piv = TRUE;

    display_act->pida->piv_data->mean_dx = 0.0;
    display_act->pida->piv_data->sdev_dx = 0.0; 
    display_act->pida->piv_data->min_dx = 0.0;
    display_act->pida->piv_data->max_dx = 0.0;

    display_act->pida->piv_data->mean_dy = 0.0;
    display_act->pida->piv_data->sdev_dy = 0.0; 
    display_act->pida->piv_data->min_dy = 0.0;
    display_act->pida->piv_data->max_dy = 0.0;


    if (display_act->pida->piv_data_scaled != NULL) {
        display_act->pida->piv_data_scaled->mean_dx = 0.0;
        display_act->pida->piv_data_scaled->sdev_dx = 0.0; 
        display_act->pida->piv_data_scaled->min_dx = 0.0;
        display_act->pida->piv_data_scaled->max_dx = 0.0;

        display_act->pida->piv_data_scaled->mean_dy = 0.0;
        display_act->pida->piv_data_scaled->sdev_dy = 0.0; 
        display_act->pida->piv_data_scaled->min_dy = 0.0;
        display_act->pida->piv_data_scaled->max_dy = 0.0;
    }


    if (display_act->pida->exist_piv && !cancel_process) {
        exec_process = TRUE;
        gl_post_par->subtract = 0;

        if (display_act->pida->scaled_piv) {
            if ((err_msg = gpiv_post_savg (display_act->pida->piv_data_scaled, 
                                          gl_post_par)) != NULL) {
            error_gpiv ("exec_savg: failing gpiv_post_savg");
        }

#ifdef DEBUG
            g_warning("exec_savg:: SCALED: mean_dx = %f sdev_dx = %f min_dx = %f max_dx = %f", 
                   display_act->pida->piv_data_scaled->mean_dx, 
                      display_act->pida->piv_data_scaled->sdev_dx, 
                      display_act->pida->piv_data_scaled->min_dx,  
                      display_act->pida->piv_data_scaled->max_dx);
            g_warning("exec_savg:: SCALED: mean_dy = %f sdev_dy = %f min_dy = %f max_dy = %f\n", 
                   display_act->pida->piv_data_scaled->mean_dy,
                      display_act->pida->piv_data_scaled->sdev_dy, 
                      display_act->pida->piv_data_scaled->min_dy, 
                      display_act->pida->piv_data_scaled->max_dy);
#endif /*  DEBUG */

            gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                       (pivpost->spinbutton_suavg), 
                                       (gfloat) display_act->
                                       pida->piv_data_scaled->mean_dx);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                       (pivpost->spinbutton_svavg), 
                                       (gfloat) display_act->
                                       pida->piv_data_scaled->mean_dy);

        } else {
            gpiv_post_savg (display_act->pida->piv_data, gl_post_par);
#ifdef DEBUG            
            g_warning("exec_savg:: mean_dx = %f sdev_dx = %f min_dx = %f max_dx = %f", 
                      display_act->pida->piv_data->mean_dx,
                      display_act->pida->piv_data->sdev_dx,
                      display_act->pida->piv_data->min_dx,  
                      display_act->pida->piv_data->max_dx);
            g_warning("exec_savg:: mean_dy = %f sdev_dy = %f min_dy = %f max_dy = %f\n", 
                      display_act->pida->piv_data->mean_dy,
                      display_act->pida->piv_data->sdev_dy,
                      display_act->pida->piv_data->min_dy,  
                      display_act->pida->piv_data->max_dy);
#endif /*  DEBUG */
            
            
            gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                       (pivpost->spinbutton_suavg), 
                                       (gfloat) display_act->
                                       pida->piv_data->mean_dx);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                       (pivpost->spinbutton_svavg), 
                                       (gfloat) display_act->
                                       pida->piv_data->mean_dy);
            
        }

        exec_process = FALSE;
    } else {
        warning_gpiv (_("no PIV data"));
    }
}



void 
exec_subavg (PivPost * pivpost
            )
/*-----------------------------------------------------------------------------
 * Subtracts average or zero offsets for particle image displacements or 
 * scaled velocities
 */
{
    guint i, j;


    if  (display_act->pida->exist_piv && !cancel_process) {
        exec_process = TRUE;
        gl_post_par->subtract = 1;

        if (display_act->pida->scaled_piv) {

            if (display_act->pida->averaged_piv) {
            gpiv_post_subtract_dxdy (display_act->pida->piv_data_scaled, 
                                    display_act->pida->piv_data_scaled->mean_dx, 
                                    display_act->pida->piv_data_scaled->mean_dy);

/*
 * Copy parameters in Buffer structure for saving and, eventual,
 * later use
 */
            display_act->pida->post_par->z_off_dx = 
                display_act->pida->piv_data_scaled->mean_dx;
            display_act->pida->post_par->z_off_dy = 
                display_act->pida->piv_data_scaled->mean_dy;

            } else {
            gpiv_post_subtract_dxdy (display_act->pida->piv_data_scaled, 
                                    gl_post_par->z_off_dx, 
                                    gl_post_par->z_off_dx);
            }


/*
 * Subtract from piv_data, only for displaying purposes
 */
            if (display_act->pida->averaged_piv) {
                gpiv_post_subtract_dxdy (display_act->pida->piv_data, 
                                        display_act->pida->piv_data->mean_dx, 
                                        display_act->pida->piv_data->mean_dy);
            } else {
/*
 * apply inverse scale factor for offsetting of partimcle image displacements
 */
                gpiv_post_subtract_dxdy (display_act->pida->piv_data, 
                                        gl_post_par->z_off_dx
                                        / gl_image_par->s_scale 
                                        * gl_image_par->t_scale, 
                                        gl_post_par->z_off_dy
                                        / gl_image_par->s_scale 
                                        * gl_image_par->t_scale
                                        );
            }

            update_all_vectors (display_act->pida);



/*
 * Only apply offseting for non-scaled pivdata with values from entry 
 * or z_off_dx and z_off_dy
 */
        } else {
            if (display_act->pida->averaged_piv) {
            gpiv_post_subtract_dxdy (display_act->pida->piv_data, 
                                    display_act->pida->piv_data->mean_dx, 
                                    display_act->pida->piv_data->mean_dy);
/*
 * Copy parameters in Buffer structure for saving and, eventual,
 * later use
 */
            display_act->pida->post_par->z_off_dx = 
                display_act->pida->piv_data->mean_dx;
            display_act->pida->post_par->z_off_dy = 
                display_act->pida->piv_data->mean_dy;

            } else {
                gl_post_par->z_off_dx = 
                    gtk_spin_button_get_value_as_float
                    (GTK_SPIN_BUTTON (pivpost->spinbutton_suavg));
                gl_post_par->z_off_dy = 
                    gtk_spin_button_get_value_as_float
                     (GTK_SPIN_BUTTON (pivpost->spinbutton_svavg));
                gpiv_post_subtract_dxdy (display_act->pida->piv_data, 
                                        gl_post_par->z_off_dx, 
                                        gl_post_par->z_off_dy);
            }
            
            update_all_vectors (display_act->pida);
        }

        display_act->pida->post_par->z_off_dx__set = TRUE;
        display_act->pida->post_par->z_off_dy__set = TRUE;
        exec_process = FALSE;

    } else {
        warning_gpiv (_("no PIV data"));
    }

/*    g_warning("exec_savg:: entering"); */
/*     g_warning(_("not connected, yet")); */
}



void 
exec_vorstra (void
             )
/*-----------------------------------------------------------------------------
 */
{
    char *err_msg = NULL;
    GtkWidget *view_scalardata_display1 = 
        gtk_object_get_data (GTK_OBJECT (display_act->mwin),
                            "view_scalardata_display1");
    GtkWidget *view_scalardata_display2 = 
        gtk_object_get_data (GTK_OBJECT (display_act->mwin),
                            "view_scalardata_display2");
    GtkWidget *view_scalardata_display3 = 
        gtk_object_get_data (GTK_OBJECT (display_act->mwin),
                            "view_scalardata_display3");


    if  (display_act->pida->exist_piv && !cancel_process) {
        exec_process = TRUE;


/*
 * Vorticity calculation and displaying
 */
        if (gl_post_par->operator_vorstra == GPIV_VORTICITY) {
            if (display_act->pida->exist_vor) {
                destroy_all_scalars (display_act, GPIV_VORTICITY);
                gpiv_free_scdata (display_act->pida->vor_data);
                display_act->pida->exist_vor = FALSE;
            }
            
            if ((display_act->pida->vor_data = 
                 gpiv_post_vorstra (display_act->pida->piv_data, 
                                    gl_post_par))
                == NULL) error_gpiv ("%s: %s", RCSID, err_msg);

            create_all_scalars (display_act, GPIV_VORTICITY);
/*
 * Adding comment to the vorticity data
 */
            display_act->pida->vor_data->comment = 
                g_strdup_printf ("# Software: %s%s\n", PACKAGE, VERSION);
            display_act->pida->vor_data->comment = 
                gpiv_add_datetime_to_comment (display_act->pida->vor_data->comment);
            display_act->pida->vor_data->comment =  
                g_strconcat (display_act->pida->vor_data->comment, 
                             "# Data type: vorticity [1/s]\n", 
                             NULL);


/*
 * Vorticity data exist but are unsaved
 * and are displayed as scalar data
 */
            display_act->pida->exist_vor = TRUE;
            display_act->pida->saved_vor = FALSE;
/*             gtk_check_menu_item_set_active */
/*                 (GTK_CHECK_MENU_ITEM (view_scalardata_display1),  */
/*                  TRUE); */

/*
 * Repete vorticity calculation process for scaled piv_data
 */
            if (display_act->pida->scaled_piv) {
                if ((display_act->pida->vor_data_scaled = 
                     gpiv_post_vorstra (display_act->pida->piv_data_scaled, 
                                       gl_post_par))
                    == NULL) error_gpiv ("%s: %s", RCSID, err_msg);

            display_act->pida->exist_vor_scaled = TRUE;
            display_act->pida->vor_data_scaled->scale = TRUE;

/*
 * Adding comment to the scaled vorticity data
 */
            display_act->pida->vor_data_scaled->comment = 
                g_strdup_printf ("# Software: %s%s\n", PACKAGE, VERSION);
            display_act->pida->vor_data_scaled->comment = 
                gpiv_add_datetime_to_comment (display_act->pida->vor_data_scaled->comment);
            display_act->pida->vor_data_scaled->comment =  
                g_strconcat (display_act->pida->vor_data_scaled->comment, 
                             "# Data type: time and spatial scaled vorticity [1/s]\n", 
                             NULL);

            }


/*
 * Shear strain calculation and displaying
 */
        } else if (gl_post_par->operator_vorstra == GPIV_S_STRAIN) {
            if (display_act->pida->exist_sstrain) {
                destroy_all_scalars (display_act, GPIV_S_STRAIN);
                gpiv_free_scdata (display_act->pida->sstrain_data);
                display_act->pida->exist_sstrain = FALSE;
            }
            
            if ((display_act->pida->sstrain_data = 
                 gpiv_post_vorstra (display_act->pida->piv_data, 
                                    gl_post_par))
                == NULL) error_gpiv ("%s: %s", RCSID, err_msg);

            create_all_scalars (display_act, GPIV_S_STRAIN);

/*
 * Adding comment to the sstrain data
 */
            display_act->pida->sstrain_data->comment = 
                g_strdup_printf ("# Software: %s%s\n", PACKAGE, VERSION);
            display_act->pida->sstrain_data->comment = 
                gpiv_add_datetime_to_comment (display_act->pida->sstrain_data->comment);
            display_act->pida->sstrain_data->comment =  
                g_strconcat (display_act->pida->sstrain_data->comment, 
                             "# Data type: shear strain [1/s]\n", 
                             NULL);

/*
 * Shear strain data exist but are unsaved
 * and are displayed as scalar data
 */
            display_act->pida->exist_sstrain = TRUE;
            display_act->pida->saved_sstrain = FALSE;
            gtk_check_menu_item_set_active
                (GTK_CHECK_MENU_ITEM (view_scalardata_display2), 
                 TRUE);
/*
 * Repete shear strain calculation process for scaled piv_data
 */
            if (display_act->pida->scaled_piv) {
                if ((display_act->pida->sstrain_data_scaled = 
                     gpiv_post_vorstra (display_act->pida->piv_data_scaled, 
                                        gl_post_par))
                    == NULL) error_gpiv ("%s: %s", RCSID, err_msg);

                display_act->pida->exist_sstrain_scaled = TRUE;
                display_act->pida->sstrain_data_scaled->scale = TRUE;

/*
 * Adding comment to the scaled sstrain data
 */
                display_act->pida->sstrain_data_scaled->comment = 
                    g_strdup_printf ("# Software: %s%s\n", PACKAGE, VERSION);
                display_act->pida->sstrain_data_scaled->comment = 
                    gpiv_add_datetime_to_comment (display_act->pida->sstrain_data_scaled->comment);
                display_act->pida->sstrain_data_scaled->comment =  
                    g_strconcat (display_act->pida->sstrain_data_scaled->comment, 
                                 "# Data type: time and spatial scaled shear strain [1/s]\n", 
                                 NULL);
            }

/*
 * Normal strain calculation and displaying
 */
        } else if (gl_post_par->operator_vorstra == GPIV_N_STRAIN) {
            if (display_act->pida->exist_nstrain) {
                /*                 destroy_all_scalars (&display_act->pida->nstrain_data, GPIV_N_STRAIN); */
                destroy_all_scalars (display_act, GPIV_N_STRAIN);
                gpiv_free_scdata (display_act->pida->nstrain_data);
                display_act->pida->exist_nstrain = FALSE;
            }
            
            if ((display_act->pida->nstrain_data = 
                 gpiv_post_vorstra (display_act->pida->piv_data, 
                                    gl_post_par))
                == NULL) error_gpiv ("%s: %s", RCSID, err_msg);

            create_all_scalars (display_act, GPIV_N_STRAIN);

/*
 * Adding comment to the nstrain data
 */
            display_act->pida->nstrain_data->comment = 
                g_strdup_printf ("# Software: %s%s\n", PACKAGE, VERSION);
            display_act->pida->nstrain_data->comment = 
                gpiv_add_datetime_to_comment (display_act->pida->nstrain_data->comment);
            display_act->pida->nstrain_data->comment =  
                g_strconcat (display_act->pida->nstrain_data->comment, 
                             "# Data type: normal strain [1/s]\n", 
                             NULL);

/*
 * Normal strain data exist but are unsaved
 * and are displayed as scalar data
 */
            display_act->pida->exist_nstrain = TRUE;
            display_act->pida->saved_nstrain = FALSE;
            gtk_check_menu_item_set_active
                (GTK_CHECK_MENU_ITEM (view_scalardata_display3), 
                 TRUE);
/*
 * Repete normal strain calculation process for scaled piv_data
 */
            if (display_act->pida->scaled_piv) {
                if ((display_act->pida->nstrain_data_scaled = 
                     gpiv_post_vorstra (display_act->pida->piv_data_scaled, 
                                        gl_post_par))
                    == NULL) error_gpiv ("%s: %s", RCSID, err_msg);

                display_act->pida->exist_nstrain_scaled = TRUE;
                display_act->pida->nstrain_data_scaled->scale = TRUE;

/*
 * Adding comment to the scaled nstrain data
 */
            display_act->pida->nstrain_data_scaled->comment = 
                g_strdup_printf ("# Software: %s%s\n", PACKAGE, VERSION);
            display_act->pida->nstrain_data_scaled->comment = 
                gpiv_add_datetime_to_comment (display_act->pida->nstrain_data_scaled->comment);
            display_act->pida->nstrain_data_scaled->comment =  
                g_strconcat (display_act->pida->nstrain_data_scaled->comment, 
                             "# Data type: time and spatial scaled normal strain [1/s]\n", 
                             NULL);
            }



       }  else {
        warning_gpiv (_("exec_vorstra: non valid operation"));
       }
     exec_process = FALSE;

   } else {
        warning_gpiv (_("no PIV data"));
    }

/*
 * Copy parameters in Buffer structure for saving and, eventual,
 * later use
 */

    if (display_act->pida->exist_vor ||  display_act->pida->exist_sstrain || 
        display_act->pida->exist_nstrain) {

        display_act->pida->post_par->diff_type = gl_post_par->diff_type;
        display_act->pida->post_par->operator_vorstra = 
            gl_post_par->operator_vorstra;
        
        display_act->pida->post_par->diff_type__set = TRUE;
        display_act->pida->post_par->operator_vorstra__set = TRUE;
        
        gpiv_post_print_parameters (stdout, display_act->pida->post_par);
    }


/* 	  gpiv_write_scdata ( &display_act->pida->vor_data, c_line, nc_lines,  */
/* 			     scale, RCSID); */

}




/*
 * Piv post-processing callback functions
 */

void
on_spinbutton_post_scale (GtkSpinButton * widget, 
                         GtkWidget * entry
/* (GtkWidget *widget, gpointer data) */
                         )
/*-----------------------------------------------------------------------------
 */
{
    enum VariableType {
	COL_POS = 1,
	ROW_POS = 2,
	S_SCALE = 3,
	T_SCALE = 4
    } var_type;

    var_type = atoi (gtk_object_get_data (GTK_OBJECT (widget), "var_type"));

    if (var_type == COL_POS) {
	gl_image_par->z_off_x = gtk_spin_button_get_value_as_float (widget);
	gl_image_par->z_off_x__set = TRUE;
        if (display_act != NULL) {
            display_act->img->image->header->z_off_x = gl_image_par->z_off_x;
            display_act->img->image->header->z_off_x__set = TRUE;
         }

    } else if (var_type == ROW_POS) {
	gl_image_par->z_off_y = gtk_spin_button_get_value_as_float (widget);
	gl_image_par->z_off_y__set = TRUE;
        if (display_act != NULL) {
            display_act->img->image->header->z_off_y = gl_image_par->z_off_y;
            display_act->img->image->header->z_off_y__set = TRUE;
        }

    } else if (var_type == S_SCALE) {
	gl_image_par->s_scale = gtk_spin_button_get_value_as_float (widget);
	gl_image_par->s_scale__set = TRUE;
        if (display_act != NULL) {
            display_act->img->image->header->s_scale = gl_image_par->s_scale;
            display_act->img->image->header->s_scale__set = TRUE;
        }

    } else if (var_type == T_SCALE) {
	gl_image_par->t_scale = gtk_spin_button_get_value_as_float (widget);
        gl_image_par->t_scale__set = TRUE;
        if (display_act != NULL) {
            display_act->img->image->header->t_scale = gl_image_par->t_scale;
            display_act->img->image->header->t_scale__set = TRUE;
        }

    } else  {
	gpiv_warning (_("on_spinbutton_post_scale: should not arrive here"));
    }

}



void
on_button_post_scale_enter (GtkWidget *widget, 
                           gpointer data
                           )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Calculates scaled locations and velocities");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_post_scale (GtkWidget *widget, 
                      gpointer data
                      )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                 row);
            ibuf = display_act->id;
            if (display[ibuf] != NULL
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }
            exec_scale (gpiv->pivpost);
        }
    }
}



void 
on_spinbutton_post_suavg (GtkSpinButton *widget, 
                          GtkWidget *entry
                          )
/*-----------------------------------------------------------------------------
 */
{
    gl_post_par->z_off_dx = gtk_spin_button_get_value_as_float (widget);
/*     g_warning("on_spinbutton_post_suavg:: z_off_dx = %f",  */
/*               post_par->z_off_dx); */
    display_act->pida->averaged_piv = FALSE;
}



void 
on_spinbutton_post_svavg (GtkSpinButton *widget, 
                          GtkWidget *entry
                          )
/*-----------------------------------------------------------------------------
 */
{
    gl_post_par->z_off_dy = gtk_spin_button_get_value_as_float (widget);
/*     g_warning ("n_spinbutton_post_svavg:: z_off_dy = %f",  */
/*               post_par->z_off_dy); */
    display_act->pida->averaged_piv = FALSE;
}



void 
on_button_post_savg_enter (GtkWidget *widget, 
                           gpointer data
                           )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Calculates spatial average displacements");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_post_savg (GtkWidget *widget, 
                     gpointer data
                     )
/*-----------------------------------------------------------------------------
 */
{
    PivPost * post = gtk_object_get_data (GTK_OBJECT (widget), "post");
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                 row);
            ibuf = display_act->id;
            if (display[ibuf] != NULL
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }
            exec_savg (post);
        }
    }
}



void 
on_button_post_subavg_enter (GtkWidget *widget, 
                             gpointer data
                             )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Subtracts mean displacements / offset values from PIV data");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_post_subavg (GtkWidget *widget, 
                       gpointer data
                       )
/*-----------------------------------------------------------------------------
 */
{
    PivPost * post = gtk_object_get_data (GTK_OBJECT (widget), "post");
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                 row);
            ibuf = display_act->id;
            if (display[ibuf] != NULL
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }
            exec_subavg (post);
        }
    }
}



void 
on_radiobutton_post_vorstra_output_enter (GtkWidget *widget, 
                                          gpointer data
                                          )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Selects the differential quantity");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_radiobutton_post_vorstra_output (GtkWidget *widget, 
                                    gpointer data
                                    )
/*-----------------------------------------------------------------------------
 */
{
    PivPost * post = gtk_object_get_data (GTK_OBJECT (widget), "post");
    gl_post_par->operator_vorstra = atoi (gtk_object_get_data
                                          (GTK_OBJECT (widget),
					"operator"));
/*     fprintf (stderr, "on_radiobutton_post_vorstra_output:: operator_vorstra = %d\n", gl_post_par->operator_vorstra); */

    if (gl_post_par->operator_vorstra == GPIV_VORTICITY) {
	gtk_widget_set_sensitive (post->radiobutton_vorstra_diffscheme_4,
				 TRUE);
    } else {
        if (gl_post_par->diff_type == GPIV_CIRCULATION) {
            gl_post_par->diff_type = GPIV_CENTRAL;
            gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                        (post->radiobutton_vorstra_diffscheme_1), 
                                        TRUE);
        }
        gtk_widget_set_sensitive (post->radiobutton_vorstra_diffscheme_4,
				 FALSE);
    }
}



void 
on_radiobutton_post_vorstra_diffscheme_enter (GtkWidget *widget, 
                                              gpointer data
                                              )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Selects the type of differential scheme");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_radiobutton_post_vorstra_diffscheme (GtkWidget *widget, 
                                        gpointer data
                                        )
/*-----------------------------------------------------------------------------
 */
{
    gl_post_par->diff_type = atoi (gtk_object_get_data (GTK_OBJECT (widget),
					"diff_type"));
/*     fprintf (stderr, "diff_type = %d\n", post_par->diff_type); */

}



void 
on_button_post_vorstra_enter (GtkWidget *widget, 
                              gpointer data
                              )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Calculates differential quantity");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_button_post_vorstra (GtkWidget *widget, 
                        gpointer data
                        )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                 row);
            ibuf = display_act->id;
            if (display[ibuf] != NULL 
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }
            exec_vorstra ();
        }
    }
}


