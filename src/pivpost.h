/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */
 
/*---------------------------------------------------------------------
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (callback) functions for Piv post-processing window/tabulator
 * $Log: pivpost.h,v $
 * Revision 1.5  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.4  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.3  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.2  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef PIVPOST_H
#define PIVPOST_H


/*
 * Public post-processing functions
 */

void 
exec_scale (PivPost *pivpost);

void 
exec_savg (PivPost *pivpost);

void 
exec_subavg (PivPost *pivpost);

void 
exec_vorstra (void);


/*
 * Piv post-processing callback functions
 */


void
on_spinbutton_post_scale (GtkSpinButton *widget, 
                          GtkWidget *entry);

/* void */
/* on_checkbutton_post_scale_enter (GtkWidget *widget,  */
/* 				gpointer data); */

/* void */
/* on_checkbutton_post_scale (GtkWidget *widget,  */
/* 			  gpointer data); */


void 
on_button_post_scale_enter (GtkWidget *widget, 
                            gpointer data);

void
on_button_post_scale (GtkWidget *widget, 
                      gpointer data);

void 
on_button_post_savg_enter (GtkWidget *widget, 
                           gpointer data);

void 
on_spinbutton_post_suavg (GtkSpinButton *widget, 
                          GtkWidget *entry);

void 
on_spinbutton_post_svavg (GtkSpinButton *widget, 
                          GtkWidget *entry);

void
on_button_post_savg (GtkWidget *widget, 
                     gpointer data);

void 
on_button_post_subavg_enter (GtkWidget *widget, 
                             gpointer data);

void
on_button_post_subavg (GtkWidget *widget, 
                       gpointer data);

void 
on_radiobutton_post_vorstra_output_enter (GtkWidget *widget, 
                                          gpointer data);

void
on_radiobutton_post_vorstra_output (GtkWidget *widget, 
                                    gpointer data);

void 
on_radiobutton_post_vorstra_diffscheme_enter (GtkWidget *widget, 
                                              gpointer data);

void
on_radiobutton_post_vorstra_diffscheme (GtkWidget *widget, 
                                        gpointer data);

void 
on_button_post_vorstra_enter (GtkWidget *widget, 
                              gpointer data);

void
on_button_post_vorstra (GtkWidget *widget, 
                        gpointer data);



#endif /* PIVPOST_H */
