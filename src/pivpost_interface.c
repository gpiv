/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * PIV Post-processing interface
 * $Log: pivpost_interface.c,v $
 * Revision 1.10  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.9  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.8  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.7  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.5  2005/02/12 14:12:12  gerber
 * Changed tabular names and titles
 *
 * Revision 1.4  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.3  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.2  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gpiv_gui.h"
/* #include "console.h" */
#include "utils.h"
#include "pivpost_interface.h"
#include "pivpost.h"

PivPost* 
create_pivpost (GnomeApp *main_window, 
		GtkWidget *container
		)
/*-----------------------------------------------------------------------------
 */
{
    PivPost * post = g_new0 (PivPost, 1);
    GpivConsole * gpiv = gtk_object_get_data (GTK_OBJECT (main_window), "gpiv");



    post->vbox_label = gtk_vbox_new (FALSE,
				    0);
    gtk_widget_ref (post->vbox_label);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_vbox_label",
			     post->vbox_label,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->vbox_label);
    gtk_container_add (GTK_CONTAINER (container),
		      post->vbox_label);

    post->label_title = gtk_label_new(_("Piv data post processing"));
    gtk_widget_ref(post->label_title);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_label_title",
			     post->label_title,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->label_title);
    gtk_box_pack_start (GTK_BOX (post->vbox_label),
		       post->label_title, FALSE,
		       FALSE, 
		       0);


/*
 * Scrolled window
 */
    post->vbox_scroll = gtk_vbox_new (FALSE,
				     0);
    gtk_widget_ref (post->vbox_scroll);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_vbox_scroll",
			     post->vbox_scroll,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->vbox_scroll);
    gtk_box_pack_start (GTK_BOX (post->vbox_label),
		       post->vbox_scroll,
		       TRUE,
		       TRUE,
		       0);



    post->scrolledwindow = gtk_scrolled_window_new (NULL,
						   NULL);
    gtk_widget_ref (post->scrolledwindow);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_scrolledwindow",
			     post->scrolledwindow,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->scrolledwindow);
    gtk_box_pack_start (GTK_BOX (post->vbox_scroll),
		       post->scrolledwindow,
		       TRUE,
		       TRUE, 
		       0);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				   (post->scrolledwindow),
				   GTK_POLICY_NEVER,
				   GTK_POLICY_NEVER
				   /* GTK_POLICY_AUTOMATIC */ );



    post->viewport = gtk_viewport_new (NULL,
				      NULL);
    gtk_widget_ref (post->viewport);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_viewport",
			     post->viewport,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->viewport);
    gtk_container_add (GTK_CONTAINER (post->scrolledwindow),
		      post->viewport);

/* 
 * main table for PIVPOST
 */
    post->table = gtk_table_new (2,
				 2,
				FALSE);
    gtk_widget_ref (post->table);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_table",
			     post->table,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->table);
    gtk_container_add (GTK_CONTAINER (post->viewport),
		      post->table);



/*
 * Scale frame
 */
/*
 * Spinners use the adjustment from spinbutton_adj_imgh_*
 */
    post->frame_scale = gtk_frame_new ( _("Scaling"));
    gtk_widget_ref (post->frame_scale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_frame_scale",
			     post->frame_scale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->frame_scale);
    gtk_table_attach (GTK_TABLE (post->table),
		     post->frame_scale, 
                     0,
		     1,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    post->table_scale = gtk_table_new (5,
				      2,
				      FALSE);
    gtk_widget_ref (post->table_scale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_table_scale",
			     post->table_scale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->table_scale);
    gtk_container_add (GTK_CONTAINER (post->frame_scale),
		      post->table_scale);


/*
 *spinner for spatial scale "sscale"
 */
    post->label_sscale = gtk_label_new ( _("spatial scale (mm/pixels): "));
    gtk_widget_ref (post->label_sscale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_label_sscale",
			     post->label_sscale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->label_sscale);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->label_sscale, 
		     0,
		     1,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

 

/*
 * KEEP DISABLED
 */
/*      spinbutton_adj_post_sscale = */
/* 	gtk_adjustment_new (gl_post_par.s_scale, 0, 1279, 1, 100, 100); */
/*
 * end of KEEP DISABLED
 */
    post->spinbutton_sscale =
	gtk_spin_button_new (GTK_ADJUSTMENT (gpiv->imgh->spinbutton_adj_sscale),
			    1,
			    4);
    gtk_widget_ref (post->spinbutton_sscale);
    gtk_widget_show (post->spinbutton_sscale);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->spinbutton_sscale, 
		     1,
		     2,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (post->spinbutton_sscale),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (post->spinbutton_sscale),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (post->spinbutton_sscale), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->spinbutton_sscale),
			"var_type",
			"3");
    g_signal_connect (GTK_OBJECT (post->spinbutton_sscale),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale),
		       post->spinbutton_sscale);


/*
 *spinner for time scale "tscale"
 */
  post->label_tscale = gtk_label_new ( _("time scale (ms): "));
    gtk_widget_ref (post->label_tscale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_label_tscale",
			     post->label_tscale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->label_tscale);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->label_tscale, 
		     0,
		     1,
		     1,
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



/*
 * KEEP DISABLED
 */
/*     spinbutton_adj_post_tscale = */
/* 	gtk_adjustment_new (gl_post_par.t_scale, 0, 1279, 1, 100, 100); */
/*
 * end of KEEP DISABLED
 */
    post->spinbutton_tscale =
	gtk_spin_button_new (GTK_ADJUSTMENT (gpiv->imgh->spinbutton_adj_tscale),
			    1,
			    4);
    gtk_widget_ref (post->spinbutton_tscale);
    gtk_widget_show (post->spinbutton_tscale);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->spinbutton_tscale, 
                     1,
		     2,
		     1,
		     2,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (post->spinbutton_tscale),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (post->spinbutton_tscale),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (post->spinbutton_tscale), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->spinbutton_tscale),
			"var_type",
			"4");
    g_signal_connect (GTK_OBJECT (post->spinbutton_tscale),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale),
		       post->spinbutton_tscale);



/*
 * spinner for column position
 */
    post->label_colpos = gtk_label_new ( _("pos. of col #0 (m): "));
    gtk_widget_ref (post->label_colpos);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_label_colpos",
			     post->label_colpos,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->label_colpos);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->label_colpos, 
                     0,
		     1,
		     2,
		     3,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



/*
 * KEEP DISABLED
 */
/*     spinbutton_adj_post_colpos = */
/* 	gtk_adjustment_new (gl_post_par.z_off_x, 0, Z_OFF_MAX, 1, 100, */
/* 			   100); */
/*
 * end of KEEP DISABLED
 */
    post->spinbutton_colpos =
	gtk_spin_button_new (GTK_ADJUSTMENT (gpiv->imgh->spinbutton_adj_colpos),
			    1,
			    4);
    gtk_widget_ref (post->spinbutton_colpos);
    gtk_widget_show (post->spinbutton_colpos);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->spinbutton_colpos, 
                     1,
		     2,
		     2,
		     3,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (post->spinbutton_colpos),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (post->spinbutton_colpos),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (post->spinbutton_colpos), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->spinbutton_colpos),
			"var_type",
			"1");
    g_signal_connect (GTK_OBJECT (post->spinbutton_colpos),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale),
		       post->spinbutton_colpos);



/*
 * spinner for row position
 */
    post->label_rowpos = gtk_label_new ( _("pos. of row #0 (m): "));
    gtk_widget_ref (post->label_rowpos);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_label_rowpos",
			     post->label_rowpos,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->label_rowpos);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->label_rowpos, 
                     0,
		     1,
		     3,
		     4,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



/*
 * KEEP DISABLED
 */
/*     spinbutton_adj_post_rowpos = */
/* 	gtk_adjustment_new (gl_post_par.z_off_y, 0, Z_OFF_MAX, 1, 100, */
/* 			   100); */
/*
 * end of KEEP DISABLED
 */
    post->spinbutton_rowpos =
	gtk_spin_button_new (GTK_ADJUSTMENT (gpiv->imgh->spinbutton_adj_rowpos),
			    1,
			    4);
    gtk_widget_ref (post->spinbutton_rowpos);
    gtk_widget_show (post->spinbutton_rowpos);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->spinbutton_rowpos, 
                     1,
		     2,
		     3,
		     4,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (post->spinbutton_rowpos),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (post->spinbutton_rowpos),
				TRUE);

    gtk_object_set_data (GTK_OBJECT (post->spinbutton_rowpos), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->spinbutton_rowpos),
			"var_type",
			"2");
    g_signal_connect (GTK_OBJECT (post->spinbutton_rowpos),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_scale),
		       post->spinbutton_rowpos);



    post->button_scale = gtk_button_new_with_label ( _("scale"));
    gtk_widget_ref (post->button_scale);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_button_scale",
			     post->button_scale,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->button_scale);
    gtk_table_attach (GTK_TABLE (post->table_scale),
		     post->button_scale, 
		     0,
		     2,
		     4,
		     5,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (GTK_FILL),
		     0,
		     0);
    gtk_tooltips_set_tip(gpiv->tooltips, 
			 post->button_scale,
			  _("Calculates time and spatial scaled particle "
"displacements (i.e. velocities) from a PIV displacement field, and their "
"scaled positions"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (post->button_scale), 
			"post",
			post);
    g_signal_connect (GTK_OBJECT (post->button_scale),
		       "enter",
		       G_CALLBACK
		       (on_button_post_scale_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->button_scale),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->button_scale),
		       "clicked",
		       G_CALLBACK (on_button_post_scale),
		       NULL);


/*
 * Spatial average frame
 */

    post->frame_savg = gtk_frame_new ( _("Offset / Spatial statistics"));
    gtk_widget_ref (post->frame_savg);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_frame_savg",
			     post->frame_savg,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->frame_savg);
    gtk_table_attach (GTK_TABLE (post->table),
		     post->frame_savg, 
                     0,
		     1, 
		     1, 
		     2,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0, 
		     0);



    post->table_savg = gtk_table_new (2,
				     4,
				     FALSE);
    gtk_widget_ref (post->table_savg);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_table_savg",
			     post->table_savg,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->table_savg);
    gtk_container_add (GTK_CONTAINER (post->frame_savg),
		      post->table_savg);


/*
 *spinner for spatial average horizontal velocity U-avg
 */
    post->label_suavg = gtk_label_new ( _("dx (px), U (m/s): "));
    gtk_widget_ref (post->label_suavg);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_label_suavg",
			     post->label_suavg,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->label_suavg);
    gtk_table_attach (GTK_TABLE (post->table_savg),
		     post->label_suavg, 
		     0,
		     1,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

 

     post->spinbutton_adj_suavg =
/* ARG1: gl_post_par.s_savg */
/* 	gtk_adjustment_new (0. , */
/* 			   0, */
/* 			   1, */
/* 			   0.01, */
/* 			   0.1, */
/* 			   10.0); */
	gtk_adjustment_new (0.0 ,
			   -100,
			   100,
			   0.01,
			   1.0,
			   10.0);
    post->spinbutton_suavg =
	gtk_spin_button_new (GTK_ADJUSTMENT (post->spinbutton_adj_suavg),
			    1,
			    4);
    gtk_widget_ref (post->spinbutton_suavg);
    gtk_widget_show (post->spinbutton_suavg);
    gtk_table_attach (GTK_TABLE (post->table_savg),
		     post->spinbutton_suavg, 
		     1,
		     2,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (post->spinbutton_suavg),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (post->spinbutton_suavg),
				TRUE);
/*
 * KEEP DISABLED
 */
    gtk_object_set_data (GTK_OBJECT (post->spinbutton_suavg), 
			"post", post);
    g_signal_connect (GTK_OBJECT (post->spinbutton_suavg),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_suavg),
		       post->spinbutton_suavg);
/*
 * end of KEEP DISABLED
 */



/*
 * spinner for spatial average vertical velocity V-avg
 */
    post->label_svavg = gtk_label_new ( _("dy (px), V (m/s): "));
    gtk_widget_ref (post->label_svavg);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_label_svavg",
			     post->label_svavg,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->label_svavg);
    gtk_table_attach (GTK_TABLE (post->table_savg),
		     post->label_svavg, 
		     0,
		     1,
		     1,
		     2,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);

 

     post->spinbutton_adj_svavg =
/* ARG1: gl_post_par.s_savg */
	gtk_adjustment_new (0.0 ,
			   -100,
			   100,
			   0.01,
			   1.0,
			   10.0);
    post->spinbutton_svavg =
	gtk_spin_button_new (GTK_ADJUSTMENT (post->spinbutton_adj_svavg),
			    1,
			    4);
    gtk_widget_ref (post->spinbutton_svavg);
    gtk_widget_show (post->spinbutton_svavg);
    gtk_table_attach (GTK_TABLE (post->table_savg),
		     post->spinbutton_svavg, 
		     1,
		     2,
		     1,
		     2,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_entry_set_editable (GTK_ENTRY (post->spinbutton_svavg),
			   TRUE);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (post->spinbutton_svavg),
				TRUE);
/*
 * KEEP DISABLED
 */
    gtk_object_set_data (GTK_OBJECT (post->spinbutton_svavg), 
			"post", post);
    g_signal_connect (GTK_OBJECT (post->spinbutton_svavg),
		       "changed",
		       G_CALLBACK (on_spinbutton_post_svavg),
		       post->spinbutton_svavg);
/*
 * end of KEEP DISABLED
 */


/*
 * button to calculate averages
 */
    post->button_savg = gtk_button_new_with_label ( _("average"));
    gtk_widget_ref (post->button_savg);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_button_savg",
			     post->button_savg,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->button_savg);
    gtk_table_attach (GTK_TABLE (post->table_savg),
		     post->button_savg, 
		     0,
		     1,
		     2,
		     3,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_tooltips_set_tip(gpiv->tooltips, post->button_savg,
			 _("Calculates spatial average particle displacements "
"or velocities from a velocity field, obtained from PIV data"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (post->button_savg), 
			"post",
			post);
    g_signal_connect (GTK_OBJECT (post->button_savg),
		       "enter",
		       G_CALLBACK (on_button_post_savg_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->button_savg),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->button_savg),
		       "clicked",
		       G_CALLBACK (on_button_post_savg), 
		       NULL);


/*
 * button to subtract averages or zero offset displacements / velocities
 */
    post->button_subavg = gtk_button_new_with_label ( _("subtract"));
    gtk_widget_ref (post->button_subavg);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_button_subavg",
			     post->button_subavg,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->button_subavg);
    gtk_table_attach (GTK_TABLE (post->table_savg),
		     post->button_subavg, 
		     1,
		     2,
		     2,
		     3,
		     (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 post->button_subavg,
			  _("Subtracts the spatial averages or offset "
"displacements / velocities, from each estimator"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (post->button_subavg), 
			"post",
			post);
    g_signal_connect (GTK_OBJECT (post->button_subavg),
		       "enter",
		       G_CALLBACK (on_button_post_subavg_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->button_subavg),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->button_subavg),
		       "clicked",
		       G_CALLBACK (on_button_post_subavg),
		       NULL);


/*
 * Vorstra frame
 */
    post->frame_vorstra = gtk_frame_new ( _("Vorticity & strain"));
    gtk_widget_ref (post->frame_vorstra);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_frame_vorstra",
			     post->frame_vorstra,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->frame_vorstra);
    gtk_table_attach (GTK_TABLE (post->table),
		     post->frame_vorstra,
		     1,
		     2,
		     0,
		     2,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (0),
		     0,
		     0);



    post->vbox_vorstra = gtk_vbox_new (FALSE,
				      0);
    gtk_widget_ref (post->vbox_vorstra);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_vbox_vorstra", 
                             post->vbox_vorstra,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->vbox_vorstra);
    gtk_container_add (GTK_CONTAINER (post->frame_vorstra),
		      post->vbox_vorstra);



    post->frame_vorstra_output = gtk_frame_new ( _("Output:"));
    gtk_widget_ref (post->frame_vorstra_output);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_frame_vorstra_output",
			     post->frame_vorstra_output,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->frame_vorstra_output);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra),
		       post->frame_vorstra_output, 
                       TRUE, 
                       TRUE,
		       0);
    gtk_frame_set_shadow_type (GTK_FRAME (post->frame_vorstra_output),
			      GTK_SHADOW_NONE);



    post->vbox_vorstra_output = gtk_vbox_new (FALSE,
					     0);
    gtk_widget_ref (post->vbox_vorstra_output);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_vbox_vorstra_output",
			     post->vbox_vorstra_output,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->vbox_vorstra_output);
    gtk_container_add (GTK_CONTAINER (post->frame_vorstra_output),
		      post->vbox_vorstra_output);



    post->radiobutton_vorstra_output_1 =
	gtk_radio_button_new_with_label (post->vbox_vorstra_output_group, 
                                         _("Vorticity"));
    post->vbox_vorstra_output_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (post->radiobutton_vorstra_output_1));
    gtk_widget_ref (post->radiobutton_vorstra_output_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_radiobutton_vorstra_output_1",
			     post->radiobutton_vorstra_output_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->radiobutton_vorstra_output_1);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra_output), 
		       post->radiobutton_vorstra_output_1,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_output_1), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_output_1),
			"operator",
			"0");
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_1),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_output_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_1),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_1),
		       "toggled",
		       G_CALLBACK (on_radiobutton_post_vorstra_output),
		       NULL);




    post->radiobutton_vorstra_output_2 =
	gtk_radio_button_new_with_label (post->vbox_vorstra_output_group, 
                                         _("Shear strain"));
    post->vbox_vorstra_output_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (post->radiobutton_vorstra_output_2));
    gtk_widget_ref (post->radiobutton_vorstra_output_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_radiobutton_vorstra_output_2",
			     post->radiobutton_vorstra_output_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->radiobutton_vorstra_output_2);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra_output), 
		       post->radiobutton_vorstra_output_2,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_output_2), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_output_2),
			"operator",
			"1");
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_2),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_output_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_2),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_2),
		       "toggled",
		       G_CALLBACK (on_radiobutton_post_vorstra_output),
		       NULL);




    post->radiobutton_vorstra_output_3 =
	gtk_radio_button_new_with_label (post->vbox_vorstra_output_group, 
                                         _("Normal strain"));
    post->vbox_vorstra_output_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (post->radiobutton_vorstra_output_3));
    gtk_widget_ref (post->radiobutton_vorstra_output_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_radiobutton_vorstra_output_3",
			     post->radiobutton_vorstra_output_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->radiobutton_vorstra_output_3);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra_output),
		       post->radiobutton_vorstra_output_3,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_output_3), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_output_3),
			"operator",
			"2");
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_3),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_output_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_3),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_output_3),
		       "toggled",
		       G_CALLBACK (on_radiobutton_post_vorstra_output),
		       NULL);



    post->frame_vorstra_diffscheme = gtk_frame_new ( _("Differential scheme:"));
    gtk_widget_ref (post->frame_vorstra_diffscheme);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_frame_vorstra_diffscheme",
			     post->frame_vorstra_diffscheme,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->frame_vorstra_diffscheme);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra),
		       post->frame_vorstra_diffscheme,
		       TRUE,
		       TRUE,
		       0);
    gtk_frame_set_shadow_type (GTK_FRAME (post->frame_vorstra_diffscheme),
			      GTK_SHADOW_NONE);




    post->vbox_vorstra_diffscheme = gtk_vbox_new (FALSE,
						 0);
    gtk_widget_ref (post->vbox_vorstra_diffscheme);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_vbox_vorstra_diffscheme", 
			     post->vbox_vorstra_diffscheme,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->vbox_vorstra_diffscheme);
    gtk_container_add (GTK_CONTAINER (post->frame_vorstra_diffscheme),
		      post->vbox_vorstra_diffscheme);




    post->radiobutton_vorstra_diffscheme_1 =
	gtk_radio_button_new_with_label (post->vbox_vorstra_diffscheme_group,
					 _("Central"));
    post->vbox_vorstra_diffscheme_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (post->radiobutton_vorstra_diffscheme_1));
    gtk_widget_ref (post->radiobutton_vorstra_diffscheme_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_radiobutton_vorstra_diffscheme_1",
			     post->radiobutton_vorstra_diffscheme_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->radiobutton_vorstra_diffscheme_1);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra_diffscheme),
		       post->radiobutton_vorstra_diffscheme_1,
		       FALSE, 
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_1), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_1),
			"diff_type",
			"0");
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_1),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_diffscheme_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_1),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_1),
		       "toggled",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_diffscheme),
		       NULL);



    post->radiobutton_vorstra_diffscheme_2 =
	gtk_radio_button_new_with_label (post->vbox_vorstra_diffscheme_group,
					 _("Least squares"));
    post->vbox_vorstra_diffscheme_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (post->radiobutton_vorstra_diffscheme_2));
    gtk_widget_ref (post->radiobutton_vorstra_diffscheme_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_radiobutton_vorstra_diffscheme_2",
			     post->radiobutton_vorstra_diffscheme_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->radiobutton_vorstra_diffscheme_2);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra_diffscheme),
		       post->radiobutton_vorstra_diffscheme_2,
		       FALSE, 
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_2), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_2),
			"diff_type",
			"1");
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_2),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_diffscheme_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_2),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_2),
		       "toggled",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_diffscheme),
		       NULL);




    post->radiobutton_vorstra_diffscheme_3 =
	gtk_radio_button_new_with_label (post->vbox_vorstra_diffscheme_group, 
                                         _("Richardson"));
    post->vbox_vorstra_diffscheme_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (post->radiobutton_vorstra_diffscheme_3));
    gtk_widget_ref (post->radiobutton_vorstra_diffscheme_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_radiobutton_vorstra_diffscheme_3",
			     post->radiobutton_vorstra_diffscheme_3,
			     (GtkDestroyNotify) gtk_widget_unref);
/*
 *Define which button is switched on at start up time
 */
    if (gl_post_par->diff_type == 2) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (post->radiobutton_vorstra_diffscheme_3),
				    TRUE);
    }
    gtk_widget_show (post->radiobutton_vorstra_diffscheme_3);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra_diffscheme),
		       post->radiobutton_vorstra_diffscheme_3,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_3), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_3),
			"diff_type",
			"2");
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_3),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_diffscheme_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_3),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_3),
		       "toggled",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_diffscheme),
		       NULL);




    post->radiobutton_vorstra_diffscheme_4 =
	gtk_radio_button_new_with_label (post->vbox_vorstra_diffscheme_group,
					 _("Circulation method"));
    post->vbox_vorstra_diffscheme_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (post->radiobutton_vorstra_diffscheme_4));
    gtk_widget_ref (post->radiobutton_vorstra_diffscheme_4);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_radiobutton_vorstra_diffscheme_4",
			     post->radiobutton_vorstra_diffscheme_4,
			     (GtkDestroyNotify) gtk_widget_unref);
/*
 *Define which button is switched on at start up time
 */
    if (gl_post_par->diff_type == 3) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (post->radiobutton_vorstra_diffscheme_4),
				    TRUE);
    }
    gtk_widget_show (post->radiobutton_vorstra_diffscheme_4);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra_diffscheme),
		       post->radiobutton_vorstra_diffscheme_4,
		       FALSE, 
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_4), 
			"post",
			post);
    gtk_object_set_data (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_4),
			"diff_type",
			"3");
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_4),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_diffscheme_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_4),
		       "leave",
		       G_CALLBACK (on_widget_leave), 
		       NULL);
    g_signal_connect (GTK_OBJECT (post->radiobutton_vorstra_diffscheme_4),
		       "toggled",
		       G_CALLBACK
		       (on_radiobutton_post_vorstra_diffscheme),
		       NULL);


/*
 * Define which button is switched on at start up time; moved to the
 * end of the tabulator as diff_scheme button Circulation might be
 * enabled or disabled
 */
    if (gl_post_par->operator_vorstra == 0) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (post->radiobutton_vorstra_output_1),
				    TRUE);
    } else if (gl_post_par->operator_vorstra == 1) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (post->radiobutton_vorstra_output_2),
				    TRUE);
    } else if (gl_post_par->operator_vorstra == 2) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (post->radiobutton_vorstra_output_3),
				    TRUE);
    }


    if (gl_post_par->diff_type == 0) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (post->radiobutton_vorstra_diffscheme_1),
				    TRUE);
    }
    if (gl_post_par->diff_type == 1) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (post->radiobutton_vorstra_diffscheme_2),
				    TRUE);
    } else if (gl_post_par->diff_type == 2) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (post->radiobutton_vorstra_diffscheme_3),
				    TRUE);
    }




    post->button_vorstra = gtk_button_new_with_label ( _("vorticity"));
    gtk_widget_ref (post->button_vorstra);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "post_button_vorstra",
			     post->button_vorstra,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (post->button_vorstra);
    gtk_box_pack_start (GTK_BOX (post->vbox_vorstra),
		       post->button_vorstra, 
		       FALSE, 
		       FALSE,
		       0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 post->button_vorstra,
			  _("Calculates vorticity or strain magnitudes from a velocity field, obtained by PIV"),
			 NULL);
    gtk_object_set_data (GTK_OBJECT (post->button_vorstra), 
			"post",
			post);
    g_signal_connect (GTK_OBJECT (post->button_vorstra),
		       "enter",
		       G_CALLBACK (on_button_post_vorstra_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->button_vorstra),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (post->button_vorstra),
		       "clicked",
		       G_CALLBACK (on_button_post_vorstra),
		       NULL);


    return post;
}
