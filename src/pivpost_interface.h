/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * PIV post processing
 * $Log: pivpost_interface.h,v $
 * Revision 1.6  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.5  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.4  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.3  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.2  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef GPIV_PIVPOST_INTERFACE_H
#define GPIV_PIVPOST_INTERFACE_H


typedef struct _PivPost PivPost;
struct _PivPost {
  GtkWidget *vbox_label;
  GtkWidget *label_title;
  GtkWidget *vbox_scroll;
  GtkWidget *scrolledwindow;
  GtkWidget *viewport;
  GtkWidget *table;

  GtkWidget *frame_scale;
  GtkWidget *table_scale;
  GtkWidget *label_sscale;
/* GtkObject *spinbutton_adj_sscale; */
  GtkWidget *spinbutton_sscale;
  GtkWidget *label_tscale;
/* GtkObject *spinbutton_adj_tscale; */
  GtkWidget *spinbutton_tscale;
  GtkWidget *label_colpos;
/* GtkObject *spinbutton_adj_colpos; */
  GtkWidget *spinbutton_colpos;
  GtkWidget *label_rowpos;
/* GtkObject *spinbutton_adj_rowpos; */
  GtkWidget *spinbutton_rowpos;
  GtkWidget *button_scale;

  GtkWidget *frame_savg;
  GtkWidget *table_savg;
  GtkWidget *label_suavg;
  GtkObject *spinbutton_adj_suavg;
  GtkWidget *spinbutton_suavg;
  GtkWidget *label_svavg;
  GtkObject *spinbutton_adj_svavg;
  GtkWidget *spinbutton_svavg;
  GtkWidget *button_savg;
  GtkWidget *button_subavg;

  GtkWidget *frame_vorstra;
  GtkWidget *vbox_vorstra;
  GtkWidget *frame_vorstra_output;
  GtkWidget *vbox_vorstra_output;
  GSList *vbox_vorstra_output_group;

  GtkWidget *radiobutton_vorstra_output_1;
  GtkWidget *radiobutton_vorstra_output_2;
  GtkWidget *radiobutton_vorstra_output_3;
  GtkWidget *frame_vorstra_diffscheme;
  GtkWidget *vbox_vorstra_diffscheme;
  GSList *vbox_vorstra_diffscheme_group/*  = NULL */;
  GtkWidget *radiobutton_vorstra_diffscheme_1;
  GtkWidget *radiobutton_vorstra_diffscheme_2;
  GtkWidget *radiobutton_vorstra_diffscheme_3;
  GtkWidget *radiobutton_vorstra_diffscheme_4;
  GtkWidget *button_vorstra;
};

PivPost *
create_pivpost (GnomeApp *main_window, 
		GtkWidget *container);

#endif /* GPIV_PIVPOST_INTERFACE_H */
