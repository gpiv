/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-----------------------------------------------------------------------------*/

/*
 * (callback) functions for Piv validation window/tabulator
 * $Log: pivvalid.c,v $
 * Revision 1.16  2008-04-28 12:00:57  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.15  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.14  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.13  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.12  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.11  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.10  2006/01/31 14:28:12  gerber
 * version 0.3.0
 *
 * Revision 1.9  2005/06/15 09:40:40  gerber
 * debugged, optimized
 *
 * Revision 1.8  2005/02/26 09:43:31  gerber
 * parameter flags (parameter_logic) defined as gboolean
 *
 * Revision 1.7  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.6  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.5  2003/07/13 14:38:18  gerber
 * changed error handling of libgpiv
 *
 * Revision 1.4  2003/07/12 21:21:16  gerber
 * changed error handling libgpiv
 *
 * Revision 1.2  2003/07/10 11:56:07  gerber
 * added man page
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */


#include "gpiv_gui.h"
#include "utils.h"
#include "pivvalid.h"
#include "display.h"


/*
 * Public piv validation functions
 */

void 
exec_gradient(void
              )
/* ----------------------------------------------------------------------------
 * Testing on gradient of displacements/veocity within Int. Area
 */
{
    if  (display_act->pida->exist_piv && !cancel_process) {
        exec_process = TRUE;
        gpiv_valid_gradient (gl_piv_par, 
                             display_act->pida->piv_data);
        update_all_vectors (display_act->pida);
        exec_process = FALSE;
    } else {
        warning_gpiv(_("no PIV data"));
    }
}



void 
exec_errvec (PivValid * valid
             )
/*-----------------------------------------------------------------------------
 * Calculates histogram of residus and resulting threshold displacement
 */
{
    char *err_msg = NULL;
    guint i;

    GnomeCanvasItem *bg, *bar, *fitline;
    GnomeCanvasPoints *points;
    double canvas_margin = 20.;
    
    double canvas_startx = -50.;
    double canvas_endx = 150.;
    double disp_width = canvas_endx - canvas_startx - 2 * canvas_margin;
    
    double canvas_starty = -50.;
    double canvas_endy = 150.;
    double disp_height = canvas_endy - canvas_starty - 2 * canvas_margin;
    
    double canvas_x1 = canvas_startx + canvas_margin;
    double canvas_y1 = canvas_starty + canvas_margin + disp_height;
    
    double x_val, y_val, x_normf, y_normf;
    


    points = gnome_canvas_points_new (2);

    if (display_act->pida->exist_piv && !cancel_process) {
        exec_process = TRUE;

        if (gpiv_var->residu_stats == TRUE) {
/*
 * Calculate histogram
 * normalizing histo between 0 and 1 and displaying histo and estimated 
 * line curve
 */
            GpivBinData *klass = NULL;
            GpivLinRegData *linreg = g_new0 (GpivLinRegData, 1);
            double centre_max = -10.0E+9;


            if ((err_msg = 
                 gpiv_valid_residu (display_act->pida->piv_data, gl_valid_par, 
                                    TRUE))
                != NULL) {
                gpiv_error ("exec_errvec: failing gpiv_valid_residu");
            }

            if ((klass = 
                 gpiv_valid_residu_stats (display_act->pida->piv_data,
                                          display_act->pida->piv_data->nx 
                                          * display_act->pida->piv_data->ny 
                                          / GPIV_NBINS_DEFAULT,
                                          linreg))
                == NULL) {
                gpiv_error ("exec_errvec: failing gpiv_valid_residu_stats");
            }
        

            for (i = 0; i < klass->nbins; i++) {
                if (klass->centre[i] > centre_max) 
                    centre_max = klass->centre[i];
            }
             
            x_normf =  (double) disp_width  / (klass->max - klass->min);
            y_normf = (double) centre_max;


            bg = gnome_canvas_item_new (gnome_canvas_root
                                        (GNOME_CANVAS(valid->canvas_histo)),
                                        gnome_canvas_rect_get_type(),
                                        "x1", (double) canvas_startx ,
                                        "y1", (double) canvas_starty,
                                        "x2", (double) canvas_endx ,
                                        "y2", (double) canvas_endy,
                                        "fill_color", "darkblue",
                                        "outline_color", "red",
                                        "width_units", 2.0, 
                                        NULL);
            
            for (i = 0; i < klass->nbins - 1; i++) {
                x_val = (double) (klass->bound[i]) / x_normf;
                y_val = (double) klass->centre[i] / y_normf;
                bar = 
                    gnome_canvas_item_new (gnome_canvas_root
                                           (GNOME_CANVAS (valid->canvas_histo)),
                                           gnome_canvas_rect_get_type(),
                                           "x1", canvas_x1 +
                                           (double) klass->bound[i]  * x_normf,
                                           "y1", canvas_y1 - 
                                           (double) disp_height * y_val,
                                           "x2", canvas_x1 + 
                                           (double)  klass->bound[i + 1] * x_normf,
                                           "y2", (double) canvas_y1,
                                           "fill_color", "darkgreen",
                                           "outline_color", "blue",
                                           "width_units", 2.0, 
                                           NULL);
            }
            
            gl_valid_par->residu_max = 
                gpiv_valid_threshold (display_act->pida->piv_par, 
                                      gl_valid_par, linreg);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON
                                       (valid->spinbutton_errvec_res), 
                                       gl_valid_par->residu_max);
#ifdef DEBUG            
       g_message("gpiv_valid_errvec:: c1 = %f int_size_f = %d yield = %f residu_max = %f", 
                  linreg->c1,
                  gl_piv_par->int_size_f,
                  gl_valid_par->data_yield,
                  gl_valid_par->residu_max);
#endif            
            
            points->coords[0] = (double) canvas_x1;
            points->coords[1] = (double) canvas_y1
                - (double) linreg->c0 / y_normf * (double) disp_height
                ;
            points->coords[2] = (double) canvas_x1
                + (double) disp_width;
            points->coords[3] = (double) canvas_y1
                - (double) linreg->c0 / y_normf * (double) disp_height
                - (double) linreg->c1 * (double) (disp_width) * (double) (disp_height)
                / ( x_normf * y_normf)
                ;


            fitline = 
                gnome_canvas_item_new (gnome_canvas_root
                                       (GNOME_CANVAS(valid->canvas_histo)),
                                       gnome_canvas_line_get_type(),
                                       "points", points,
                                       "fill_color", "red",
                                       "width_units", (double) 2.0,
                                       NULL);
            
            gpiv_free_bindata (klass);


        } else {
            if ((err_msg = 
                 gpiv_valid_errvec (display_act->pida->piv_data, 
                                    display_act->img->image,
                                    gl_piv_par, 
                                    gl_valid_par, 
                                    FALSE))
                != NULL) gpiv_error ("%s: %s", RCSID, err_msg);
            update_all_vectors (display_act->pida);
            
        }
        
        display_act->pida->exist_valid = TRUE;
        
/*
 * Copy parameters in Buffer structure for saving and, eventual,
 * later use
 */
        display_act->pida->valid_par->residu_max = gl_valid_par->residu_max;
        display_act->pida->valid_par->data_yield = gl_valid_par->data_yield;
        display_act->pida->valid_par->residu_type = gl_valid_par->residu_type;
        display_act->pida->valid_par->subst_type = gl_valid_par->subst_type;
        display_act->pida->valid_par->histo_type = gl_valid_par->histo_type;
        
        display_act->pida->valid_par->residu_max__set = TRUE;
        display_act->pida->valid_par->data_yield__set = 
            gl_valid_par->data_yield__set;
        display_act->pida->valid_par->residu_type__set = TRUE;
        display_act->pida->valid_par->subst_type__set = TRUE;
        display_act->pida->valid_par->histo_type__set = TRUE;
        exec_process = FALSE;
        
    } else {
        warning_gpiv(_("no PIV data"));
    }
    
    gnome_canvas_points_free(points);
    
}



void 
exec_peaklock (PivValid * valid
               )
/*-----------------------------------------------------------------------------
 * Calculates and displays peaklocking histogram of a single PIV data set
 */
{

     guint i;
     GnomeCanvasItem *bg, *bar;
     double canvas_margin = 20.;
     
     double canvas_startx = -50.;
     double canvas_endx = 150.;
     double d_width = canvas_endx - canvas_startx - 2 * canvas_margin;
     
     double canvas_starty = -50.;
     double canvas_endy = 150.;
     double d_height = canvas_endy - canvas_starty - 2 * canvas_margin;
     
     double canvas_x1 = canvas_startx + canvas_margin;
     double canvas_y1 = canvas_starty + canvas_margin + d_height;
     
     double y_val;
     gint count_max = -10000;
     GpivBinData *klass = NULL;
  


     if  (display_act->pida->exist_piv && !cancel_process) {
         exec_process = TRUE;
	  
/*
 * Calculates and normalizes klass data between 0 and 1
 */
	  if ((klass = gpiv_valid_peaklck (display_act->pida->piv_data, 
                                           gpiv_par->console__nbins))
              == NULL) {
              gpiv_error ("exec_peaklock: failing gpiv_valid_peaklck");
          }


	  for (i = 0; i < klass->nbins; i++) {
              if ((double) klass->count[i] > count_max) {
                   count_max = klass->count[i];
              }
	  }
	  

/*
 * Displaying background and histogram
 */
	  bg = 
              gnome_canvas_item_new (gnome_canvas_root
                                     (GNOME_CANVAS(valid->canvas_histo)),
				     gnome_canvas_rect_get_type(),
				     "x1", (double) canvas_startx ,
				     "y1", (double) canvas_starty,
				     "x2", (double) canvas_endx ,
				     "y2", (double) canvas_endy,
				     "fill_color", "darkgreen",
				     "outline_color", "red",
				     "width_units", 2.0, 
				     NULL);

	  for (i = 0; i < klass->nbins; i++) {
              if (count_max != 0) {
                  y_val = (double) klass->count[i] / (double) count_max;
              } else {
                  y_val = 0.0;
              }


              bar = 
                  gnome_canvas_item_new (gnome_canvas_root
                                         (GNOME_CANVAS(valid->canvas_histo)),
                                         gnome_canvas_rect_get_type(),
                                         "x1", canvas_x1 + 
                                         (double) klass->bound[i] * d_width,
                                         "y1", canvas_y1 - 
                                         (double) d_height * y_val,
                                         "x2", canvas_x1 + 
                                         (double)  klass->bound[i+1] * d_width,
                                         "y2", canvas_y1,
                                         "fill_color", "yellow",
                                         "outline_color", "blue",
                                         "width_units", 2.0, 
                                         NULL);
	  }
	  
          gpiv_free_bindata (klass);
          exec_process = FALSE;

     } else {
	  warning_gpiv (_("no PIV data"));
     }    
     
}



void 
exec_uvhisto (PivValid * valid,
              enum GpivVelComponent velcomp
              )
/*-----------------------------------------------------------------------------
 */
{
     guint i;
     GnomeCanvasItem *bg, *bar;
     double canvas_margin = 20.;
     
     double canvas_startx = -50.;
     double canvas_endx = 150.;
     double d_width = canvas_endx - canvas_startx - 2 * canvas_margin;
     
     double canvas_starty = -50.;
     double canvas_endy = 150.;
     double d_height = canvas_endy - canvas_starty - 2 * canvas_margin;
     
     double canvas_x1 = canvas_startx + canvas_margin;
     double canvas_y1 = canvas_starty + canvas_margin + d_height;
     
     double y_val;
     gint count_max = -10000;
/*      char c_line[GPIV_MAX_LINES_C][GPIV_MAX_CHARS]; */



     if (display_act->pida->exist_histo == TRUE) {
/*          g_message("exec_uvhisto:: calling gpiv_free_bindata"); */
         gpiv_free_bindata (display_act->pida->klass);
     }

     if  (display_act->pida->exist_piv && !cancel_process) {
         exec_process = TRUE;
	  
	  
/* BUGFIX: quick hack to display u velocities */
/*          g_message("::will process uhisto"); */

          if ((display_act->pida->klass = 
              gpiv_post_uvhisto (display_act->pida->piv_data, 
                                 gpiv_par->console__nbins,
                                 velcomp))
              == NULL) {
              gpiv_error ("exec_uvhisto: failing gpiv_post_uvhisto");
          }
/*           gpiv_print_histo(display_act->pida->klass,  */
/*                            c_line,  */
/*                            0, */
/*                            display_act->pida->scaled_piv,  */
/*                            VERSION); */

/*
 * normalizing data between 0 and 1
 */
	  for (i = 0; i < display_act->pida->klass->nbins; i++) {
              if ((double) display_act->pida->klass->count[i] > count_max) {
                  count_max = display_act->pida->klass->count[i];
              }
	  }
	  
/*
 * Displaying
 */
	  bg = 
              gnome_canvas_item_new(gnome_canvas_root
                                    (GNOME_CANVAS(valid->canvas_histo)),
				     gnome_canvas_rect_get_type(),
				     "x1", (double) canvas_startx ,
				     "y1", (double) canvas_starty,
				     "x2", (double) canvas_endx ,
				     "y2", (double) canvas_endy,
				     "fill_color", "darkgreen",
				     "outline_color", "red",
				     "width_units", 2.0, 
				     NULL);

	  for (i = 0; i < display_act->pida->klass->nbins; i++) {
              if (count_max != 0) {
                  y_val = (double) display_act->pida->klass->count[i] / (double) count_max;
              } else {
                  y_val = 0.0;
              }

              bar = 
                   gnome_canvas_item_new(gnome_canvas_root
                                         (GNOME_CANVAS(valid->canvas_histo)),
                                         gnome_canvas_rect_get_type(),
                                         "x1", canvas_x1 + 
                                         (double) (d_width / display_act->pida->klass->nbins * i),
                                         "y1", canvas_y1 - 
                                         (double) d_height * y_val,
                                         "x2", canvas_x1 + 
                                         (double)  (d_width/ display_act->pida->klass->nbins * (i+1)),
                                         "y2", canvas_y1,
                                         "fill_color", "yellow",
                                         "outline_color", "blue",
                                         "width_units", 2.0, 
                                         NULL);


	  }
	  
/*           gpiv_free_bindata(display_act->pida->klass); */
         display_act->pida->exist_histo = TRUE;
         display_act->pida->saved_histo = FALSE;
         exec_process = FALSE;

     } else {
	  warning_gpiv(_("no PIV data"));
     }    
     
}



/*
 * Piv validation window/tabulator callbacks
 */

void 
on_button_valid_gradient_enter (GtkWidget * widget, 
                                gpointer data
                                )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Examines PIV data on velocity gradients");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);

}



void 
on_button_valid_gradient (GtkWidget * widget, 
                          gpointer data)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                  row); 

            ibuf = display_act->id;
            if (display[ibuf] != NULL 
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }

            exec_gradient ();
        }
    }
}



void 
on_radiobutton_valid_disable_0_enter (GtkWidget * widget, 
                                      gpointer data
                                      )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("No mouse activity within displayer");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
    
}



void 
on_radiobutton_valid_disable_1_enter (GtkWidget * widget, 
                                      gpointer data
                                      )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Enables a single PIV data-point");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
    
}



void 
on_radiobutton_valid_disable_2_enter (GtkWidget * widget, 
                                      gpointer data
                                      )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Disables a single PIV data-point");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
    
}



void 
on_radiobutton_valid_disable_3_enter (GtkWidget * widget, 
                                      gpointer data
                                      )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Enables an area containing PIV data");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
    
}



void 
on_radiobutton_valid_disable_4_enter (GtkWidget * widget, 
                                      gpointer data
                                      )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Disables an area containing PIV data");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
    
}



void 
on_radiobutton_valid_disable (GtkWidget * widget, 
                              gpointer data
                              )
/*-----------------------------------------------------------------------------
 */
{
    m_select = atoi (gtk_object_get_data (GTK_OBJECT (widget),
                                          "mouse_select"));
}



void 
on_radiobutton_valid_errvec_residu_enter (GtkWidget * widget, 
                                          gpointer data
                                          )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Defines residu type to examine data on");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_radiobutton_valid_errvec_residu (GtkWidget * widget, 
                                    gpointer data
                                    )
/*-----------------------------------------------------------------------------
 */
{
    gl_valid_par->residu_type = atoi (gtk_object_get_data (GTK_OBJECT (widget),
                                                           "residu"));
}



void
on_button_valid_errvec_resstats_enter (GtkWidget * widget, 
                                       gpointer data
                                       )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Displays inverse cumulative histogram of residus");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_button_valid_errvec_resstats (GtkWidget * widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    PivValid * valid = gtk_object_get_data (GTK_OBJECT (widget), "valid");
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    gpiv_var->residu_stats = TRUE;

    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                  row); 
            
            ibuf = display_act->id;
            if (display[ibuf] != NULL 
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }
            exec_errvec (valid);
        }
        
        gpiv_var->residu_stats = FALSE;
    }
}



void
on_spinbutton_valid_errvec_neighbors (GtkSpinButton *widget, 
                                      GtkWidget *entry
                                      )
/*-----------------------------------------------------------------------------
 */
{
    gl_valid_par->neighbors  = gtk_spin_button_get_value_as_int (widget); 
}



void
on_spinbutton_valid_errvec_yield (GtkSpinButton *widget, 
                                  GtkWidget *entry
                                  )
/*-----------------------------------------------------------------------------
 */
{
    gl_valid_par->data_yield  = gtk_spin_button_get_value_as_float (widget); 
}



void
on_spinbutton_valid_errvec_res (GtkSpinButton *widget, 
                                GtkWidget *entry
                                )
/*-----------------------------------------------------------------------------
 */
{
    gl_valid_par->residu_max  = gtk_spin_button_get_value_as_float (widget); 
    if (v_color == SHOW_SNR && display_act->pida->exist_piv) {
        update_all_vectors (display_act->pida);
    }
}



void
on_checkbutton_valid_errvec_disres_enter (GtkWidget * widget, 
                                          gpointer data
                                          )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Display PIV vector colors related to residu or SNR value");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void
on_checkbutton_valid_errvec_disres (GtkSpinButton *widget, 
                                    GtkWidget *entry
                                    )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON (widget)->active) {
        v_color = SHOW_SNR;
    } else {
        v_color = SHOW_PEAKNR;
    }


    if (display_act->pida->exist_piv && display_act->display_piv) {
        update_all_vectors (display_act->pida);
    }
}



void 
on_radiobutton_valid_errvec_subst_enter (GtkWidget * widget, 
                                         gpointer data
                                         )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = NULL;
    gint ltype = atoi (gtk_object_get_data (GTK_OBJECT (widget), "substitute"));

    if (ltype == GPIV_VALID_SUBSTYPE__NONE) {
        msg = _("Only sets peak_no (flag) to zero");
    } else if (ltype ==  GPIV_VALID_SUBSTYPE__L_MEAN) {
        msg = _("mean from surroundings");
    } else if (ltype ==  GPIV_VALID_SUBSTYPE__MEDIAN) {
        msg = _("Median (middle value) from surroundings");
    } else if (ltype ==  GPIV_VALID_SUBSTYPE__COR_PEAK) {
        msg = _("Re-interrogates and uses next corr. peak as estimator");
    } else {
        g_error ("NO valid ltype\n");
    }

    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_radiobutton_valid_errvec_subst (GtkWidget * widget, 
                                   gpointer data
                                   )
/*-----------------------------------------------------------------------------
 */
{
    gl_valid_par->subst_type = atoi (gtk_object_get_data (GTK_OBJECT (widget), 
                                                          "substitute"));
}



void 
on_button_valid_errvec_enter (GtkWidget * widget, 
                              gpointer data
                              )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Examines PIV data on outliers and substitutes");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_button_valid_errvec (GtkWidget * widget, 
                        gpointer data
                        )
/*-----------------------------------------------------------------------------
 */
{
    PivValid * valid = gtk_object_get_data (GTK_OBJECT (widget), "valid");
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    gpiv_var->residu_stats = FALSE;

    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 
                                                  row); 
            
            ibuf = display_act->id;
            if (display[ibuf] != NULL 
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }
            
            exec_errvec (valid);
        }
    }
}



void 
on_spinbutton_valid_peaklck_bins (GtkSpinButton * widget, 
                                  GtkWidget * entry
                                  )
/*-----------------------------------------------------------------------------
 */
{
    gpiv_par->console__nbins = gtk_spin_button_get_value_as_int (widget);
}




void 
on_button_valid_peaklck_enter (GtkWidget * widget, 
                               gpointer data
                               )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Calculates a histogram of sub-pixel displacements");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_button_valid_peaklck (GtkWidget * widget, 
                         gpointer data
                         )
/*-----------------------------------------------------------------------------
 */
{
    PivValid * valid = gtk_object_get_data (GTK_OBJECT (widget), "valid");
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf),
                                                  row); 

            ibuf = display_act->id;
            if (display[ibuf] != NULL 
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }

            exec_peaklock (valid);
        }
    }
}



void 
on_button_valid_uhisto_enter (GtkWidget * widget, 
                              gpointer data
                              )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _ ("Calculates a histogram of horizontal displacements.");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_button_valid_uhisto (GtkWidget * widget, 
                        gpointer data
                        )
/*-----------------------------------------------------------------------------
 */
{
    PivValid * valid = gtk_object_get_data (GTK_OBJECT (widget), "valid");
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf),
                                                  row); 

            ibuf = display_act->id;
            if (display[ibuf] != NULL 
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }

            exec_uvhisto (valid, GPIV_U);
        }
    }
}



void 
on_button_valid_vhisto_enter (GtkWidget * widget, 
                              gpointer data
                              )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gchar *msg = _("Calculates a histogram of vertical displacements.");
    gnome_appbar_set_status (GNOME_APPBAR (gpiv->appbar), msg);
}



void 
on_button_valid_vhisto (GtkWidget * widget, 
                        gpointer data
                        )
/*-----------------------------------------------------------------------------
 */
{
    PivValid * valid = gtk_object_get_data (GTK_OBJECT (widget), "valid");
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (widget), "gpiv");
    gint row, ibuf;

    cancel_process = FALSE;
    if (nbufs > 0) {
        for (row = gpiv->first_selected_row; row <= gpiv->last_selected_row; 
             row++) {
            display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf),
                                                  row); 

            ibuf = display_act->id;
            if (display[ibuf] != NULL 
                && display_act->mwin != NULL 
                && GTK_WIDGET_VISIBLE (GTK_WIDGET (display_act->mwin)) ) { 
                gdk_window_show (GTK_WIDGET (display_act->mwin)->window);
                gdk_window_raise (GTK_WIDGET (display_act->mwin)->window);
            }

            exec_uvhisto (valid, GPIV_V);
        }
    }
}
