/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * (callback) functions for Piv validation window/tabulator
 * $Log: pivvalid.h,v $
 * Revision 1.5  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.4  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.3  2006/01/31 14:28:13  gerber
 * version 0.3.0
 *
 * Revision 1.2  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef PIVVALID_H
#define PIVVALID_H

/*
 * Public piv validation functions
 */

void 
exec_gradient (void);

void 
exec_errvec (PivValid *valid);

void 
exec_peaklock (PivValid *valid);

void 
exec_uvhisto (PivValid *valid,
              enum GpivVelComponent velcomp);


/*
 * Piv validation callbacks functions
 */


void 
on_button_valid_gradient_enter (GtkWidget * idget, 
                                gpointer data);


void 
on_button_valid_gradient (GtkWidget *widget, 
                          gpointer data);


void 
on_radiobutton_valid_disable_0_enter (GtkWidget *widget, 
                                      gpointer data);

void 
on_radiobutton_valid_disable_1_enter (GtkWidget *widget, 
                                      gpointer data);

void 
on_radiobutton_valid_disable_2_enter (GtkWidget *widget, 
                                      gpointer data);

void 
on_radiobutton_valid_disable_3_enter (GtkWidget *widget, 
                                      gpointer data);

void 
on_radiobutton_valid_disable_4_enter (GtkWidget *widget, 
                                      gpointer data);

void 
on_radiobutton_valid_disable (GtkWidget *widget, 
                              gpointer data);

void 
on_radiobutton_valid_errvec_residu_enter (GtkWidget *widget, 
                                          gpointer data);


void 
on_radiobutton_valid_errvec_residu (GtkWidget *widget, 
                                    gpointer data);

void
on_button_valid_errvec_resstats_enter (GtkWidget *widget, 
                                       gpointer data);

void 
on_button_valid_errvec_resstats (GtkWidget *widget, 
                                 gpointer data);

void
on_spinbutton_valid_errvec_yield (GtkSpinButton *widget, 
                                  GtkWidget *entry);

void
on_spinbutton_valid_errvec_neighbors (GtkSpinButton *widget, 
                                      GtkWidget *entry);

void
on_spinbutton_valid_errvec_res (GtkSpinButton *widget, 
                                GtkWidget *entry);

void
on_checkbutton_valid_errvec_disres_enter (GtkWidget *widget, 
                                          gpointer data);

void
on_checkbutton_valid_errvec_disres (GtkSpinButton *widget, 
                                    GtkWidget *entry);

void 
on_radiobutton_valid_errvec_subst_enter (GtkWidget *widget, 
                                         gpointer data);


void 
on_radiobutton_valid_errvec_subst (GtkWidget *widget, 
                                   gpointer data);

void 
on_button_valid_errvec_enter (GtkWidget *widget, 
                              gpointer data);


void 
on_button_valid_errvec (GtkWidget *widget, 
                        gpointer data);

void
on_spinbutton_valid_peaklck_bins (GtkSpinButton *widget, 
                                  GtkWidget *entry);

void 
on_button_valid_peaklck_enter (GtkWidget *widget, 
                               gpointer data);


void 
on_button_valid_peaklck (GtkWidget *widget, 
                         gpointer data);


void 
on_button_valid_uhisto_enter (GtkWidget *widget, 
                              gpointer data);


void 
on_button_valid_uhisto (GtkWidget *widget, 
			gpointer data);


void 
on_button_valid_vhisto_enter (GtkWidget *widget, 
                              gpointer data);

void 
on_button_valid_vhisto (GtkWidget *widget, 
			gpointer data);


#endif /* PIVVALID_H */
