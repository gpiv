
/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * PIV Validation interface
 * $Log: pivvalid_interface.c,v $
 * Revision 1.11  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.10  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.9  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.8  2006/01/31 14:28:13  gerber
 * version 0.3.0
 *
 * Revision 1.6  2005/02/12 14:12:12  gerber
 * Changed tabular names and titles
 *
 * Revision 1.5  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.4  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.3  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.2  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gpiv_gui.h"
/* #include "console.h" */
#include "utils.h"
#include "pivvalid_interface.h"
#include "pivvalid.h"

PivValid *
create_pivvalid (GnomeApp *main_window, 
		GtkWidget *container
		 )
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole * gpiv = gtk_object_get_data (GTK_OBJECT (main_window), "gpiv");
    PivValid *valid = g_new0(PivValid, 1);



    valid->vbox_label = gtk_vbox_new (FALSE,
				     0);
    gtk_widget_ref(valid->vbox_label);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_label",
			     valid->vbox_label,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(valid->vbox_label);
    gtk_container_add (GTK_CONTAINER (container),
		      valid->vbox_label);

    valid->label_title = gtk_label_new( _("Piv data validation"));
    gtk_widget_ref(valid->label_title);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_title",
			     valid->label_title,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->label_title);
    gtk_box_pack_start (GTK_BOX (valid->vbox_label),
		       valid->label_title,
		       FALSE,
		       FALSE,
		       0);


/*
 * Scrolled window
 */
    valid->vbox_scroll = gtk_vbox_new (FALSE,
				      0);
    gtk_widget_ref (valid->vbox_scroll);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_scroll",
			     valid->vbox_scroll,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->vbox_scroll);
    gtk_box_pack_start (GTK_BOX (valid->vbox_label),
		       valid->vbox_scroll,
		       TRUE,
		       TRUE,
		       0);


    valid->scrolledwindow = gtk_scrolled_window_new (NULL, 
						    NULL);
    gtk_widget_ref (valid->scrolledwindow);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "scrolledwindow",
			     valid->scrolledwindow,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->scrolledwindow);
    gtk_box_pack_start (GTK_BOX (valid->vbox_scroll),
		       valid->scrolledwindow,
		       TRUE, 
		       TRUE,
		       0);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				   (valid->scrolledwindow),
				   GTK_POLICY_NEVER,
				   GTK_POLICY_AUTOMATIC);




    valid->viewport = gtk_viewport_new (NULL,
				       NULL);
    gtk_widget_ref (valid->viewport);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "viewport",
			     valid->viewport,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->viewport);
    gtk_container_add (GTK_CONTAINER (valid->scrolledwindow),
		      valid->viewport);

/*
 * main table for validation
 */
    valid->table = gtk_table_new (2, 
				 2, 
				 FALSE);
    gtk_widget_ref (valid->table);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "table", 
                             valid->table,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->table);
    gtk_container_add (GTK_CONTAINER (valid->viewport),
		      valid->table);



/*
 * Enable / Disable frame
 */

    valid->frame_disable = gtk_frame_new ( _("Disable data"));
    gtk_widget_ref (valid->frame_disable);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_disable",
			     valid->frame_disable,
			     (GtkDestroyNotify)
			     gtk_widget_unref);
    gtk_widget_show (valid->frame_disable);
    gtk_table_attach (GTK_TABLE (valid->table),
		     valid->frame_disable,
		     0,
		     1,
		     0,
		     1,
		     (GtkAttachOptions) (GTK_FILL),
		     (GtkAttachOptions) (GTK_SHRINK),
		     0,
		     0);



    valid->vbox_disable = gtk_vbox_new (FALSE,
				       0);
    gtk_widget_ref (valid->vbox_disable);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_disable",
			     valid->vbox_disable,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->vbox_disable);
    gtk_container_add (GTK_CONTAINER (valid->frame_disable),
		      valid->vbox_disable);



    valid->radiobutton_disable_0 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group,
					 _("None"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_disable_0));
    gtk_widget_ref (valid->radiobutton_disable_0);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_disable_0",
			     valid->radiobutton_disable_0,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->radiobutton_disable_0);
    gtk_box_pack_start (GTK_BOX (valid->vbox_disable),
		       valid->radiobutton_disable_0,
		       FALSE,
		       FALSE,
		       0);


    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_0), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_0),
			"mouse_select",
			"0" /* NO_MS */);

    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_0),
		       "enter",
		       G_CALLBACK (on_radiobutton_valid_disable_0_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_0),
		       "leave",
		       G_CALLBACK (on_widget_leave), 
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_0),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_disable),
		       NULL);




    valid->radiobutton_disable_1 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group, 
                                         _("Enable point"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_disable_1));
    gtk_widget_ref (valid->radiobutton_disable_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_valid_disable_1",
			     valid->radiobutton_disable_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->radiobutton_disable_1);
    gtk_box_pack_start (GTK_BOX (valid->vbox_disable),
		       valid->radiobutton_disable_1,
		       FALSE, 
		       FALSE,
		       0);

   gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_1), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_1),
			"mouse_select",
			"7" /* ENABLE_POINT_MS */);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_1),
		       "enter",
		       G_CALLBACK (on_radiobutton_valid_disable_1_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_1),
		       "leave", 
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_1),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_disable),
		       NULL);



    valid->radiobutton_disable_2 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group, 
                                         _("Disable point"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_disable_2));
    gtk_widget_ref (valid->radiobutton_disable_2);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_disable_2",
			     valid->radiobutton_disable_2,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->radiobutton_disable_2);
    gtk_box_pack_start (GTK_BOX (valid->vbox_disable),
		       valid->radiobutton_disable_2,
		       FALSE, 
		       FALSE, 
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_2), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_2),
			"mouse_select",
			"8" /* DISABLE_POINT_MS */);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_2),
		       "enter",
		       G_CALLBACK (on_radiobutton_valid_disable_2_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_2),
		       "leave",
		       G_CALLBACK (on_widget_leave), 
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_2),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_disable),
		       NULL);



    valid->radiobutton_disable_3 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group, 
                                         _("Enable area"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_disable_3));
    gtk_widget_ref (valid->radiobutton_disable_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_disable_3",
			     valid->radiobutton_disable_3,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->radiobutton_disable_3);
    gtk_box_pack_start (GTK_BOX (valid->vbox_disable),
		       valid->radiobutton_disable_3,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_3), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_3),
			"mouse_select",
			"9" /* ENABLE_AREA_MS */);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_3),
		       "enter",
		       G_CALLBACK (on_radiobutton_valid_disable_3_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_3),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_3),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_disable),
		       NULL);



    valid->radiobutton_disable_4 =
	gtk_radio_button_new_with_label (gpiv->mouse_sel_group, 
                                         _("Disable area"));
    gpiv->mouse_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_disable_4));
    gtk_widget_ref (valid->radiobutton_disable_4);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_disable_4",
			     valid->radiobutton_disable_4,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->radiobutton_disable_4);
    gtk_box_pack_start (GTK_BOX (valid->vbox_disable),
		       valid->radiobutton_disable_4,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_4), 
			"valid",
			valid);
     gtk_object_set_data (GTK_OBJECT (valid->radiobutton_disable_4),
			"mouse_select",
			 "10" /* DISABLE_AREA_MS */);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_4),
		       "enter",
		       G_CALLBACK (on_radiobutton_valid_disable_4_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_4),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_disable_4),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_disable),
		       NULL);



/*
 * Button for testing on velocity gradients
 */
    valid->button_gradient =
	gtk_button_new_with_label ( _("validate on velocity gradient "));
    gtk_widget_ref (valid->button_gradient);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "button_gradient",
			     valid->button_gradient,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->button_gradient);
    gtk_table_attach (GTK_TABLE (valid->table),
		     valid->button_gradient,
		     1,
		     2,
		     0,
		     1,
		     (GtkAttachOptions)(GTK_SHRINK /*FILL*/),
		     (GtkAttachOptions)(GTK_SHRINK), 
		     0,
		     0);

    gtk_tooltips_set_tip(gpiv->tooltips,
			 valid->button_gradient,
			  _("Disables PIV data that have a too large velocity "
"gradient over the Interrogation Area"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (valid->button_gradient), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->button_gradient),
		       "enter",
		       G_CALLBACK (on_button_valid_gradient_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->button_gradient),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->button_gradient),
		       "clicked",
		       G_CALLBACK (on_button_valid_gradient),
		       NULL);




/*
 * Frame for histogram of sub-pixel values (used to test on peak-locking 
 * effect)
 */

    valid->frame_histo = gtk_frame_new ( _("Histograms"));
    gtk_widget_ref (valid->frame_histo);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_histo",
			     valid->frame_histo,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->frame_histo);
    gtk_table_attach (GTK_TABLE (valid->table),
		     valid->frame_histo,
		     0,
		     1,
		     1,
		     2,
		    (GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
		     (GtkAttachOptions) /* (GTK_SHRINK) */
                     (GTK_EXPAND | GTK_FILL),
		     0,
		     0);



    valid->vbox_histo = gtk_vbox_new (FALSE,
				       0);
    gtk_widget_ref (valid->vbox_histo);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_histo",
			     valid->vbox_histo,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->vbox_histo);
    gtk_container_add (GTK_CONTAINER (valid->frame_histo), 
                      valid->vbox_histo);




    valid->hbox_histo_spin = gtk_hbox_new (FALSE,
					    0);
    gtk_widget_ref (valid->hbox_histo_spin);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "hbox_histo_spin",
			     valid->hbox_histo_spin,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->hbox_histo_spin);
    gtk_box_pack_start (GTK_BOX (valid->vbox_histo),
		       valid->hbox_histo_spin,
		       FALSE, 
		       FALSE,
		       0);



    valid->label_histo_bins = gtk_label_new ( _("# bins: "));
    gtk_widget_ref (valid->label_histo_bins);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_histo_bins",
			     valid->label_histo_bins,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->label_histo_bins);
    gtk_box_pack_start (GTK_BOX (valid->hbox_histo_spin),
		       valid->label_histo_bins,
		       FALSE,
		       FALSE,
		       0);



/*
 * NBINS_DEFAULT defined in gpiv.h
 */
    valid->spinbutton_adj_histo_bins = 
      gtk_adjustment_new (GPIV_NBINS_DEFAULT,
			 0,
			 100,
			 1, 
			 10, 
			 100);
    valid->spinbutton_histo_bins = 
      gtk_spin_button_new (GTK_ADJUSTMENT
			  (valid->spinbutton_adj_histo_bins),
			  1, 
			  0);
    gtk_widget_ref (valid->spinbutton_histo_bins);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_histo_bins",
			     valid->spinbutton_histo_bins,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->spinbutton_histo_bins);
    gtk_box_pack_start (GTK_BOX (valid->hbox_histo_spin),
		       valid->spinbutton_histo_bins,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->spinbutton_histo_bins), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->spinbutton_histo_bins),
		       "changed",
		       G_CALLBACK (on_spinbutton_valid_peaklck_bins),
		       valid->spinbutton_histo_bins);





/*
 * Gnome2:
 */
/*     gtk_widget_push_colormap (GdkColormap *cmap) */
/*     gtk_widget_push_visual (gdk_imlib_get_visual ()); */
/*     gtk_widget_push_colormap (gdk_imlib_get_colormap ()); */
    valid->canvas_histo = gnome_canvas_new ();
    gtk_widget_pop_colormap ();
    gtk_widget_pop_visual ();
    gtk_widget_ref (valid->canvas_histo);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "canvas_histo",
			     valid->canvas_histo,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_box_pack_start (GTK_BOX (valid->vbox_histo),
		       valid->canvas_histo,
		       TRUE,
		       TRUE,
		       0);
    gtk_widget_show (valid->canvas_histo);




    valid->hbox_histo_buttons = gtk_hbox_new (FALSE,
					       0);
    gtk_widget_ref (valid->hbox_histo_buttons);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "hbox_histo_buttons",
			     valid->hbox_histo_buttons,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->hbox_histo_buttons);
    gtk_box_pack_start (GTK_BOX (valid->vbox_histo),
		       valid->hbox_histo_buttons,
		       FALSE,
		       FALSE,
		       0);




    valid->button_errvec_resstats =
	gtk_button_new_with_label ( _("residu"));
    gtk_widget_ref (valid->button_errvec_resstats);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "button_errvec_resstats",
			     valid->button_errvec_resstats,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->button_errvec_resstats);
    gtk_box_pack_start (GTK_BOX (valid->hbox_histo_buttons),
		       valid->button_errvec_resstats,
		       FALSE, 
		       FALSE, 
		       0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 valid->button_errvec_resstats,
			  _("calculates residus of displacements "
"and displays in a histogram"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (valid->button_errvec_resstats), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->button_errvec_resstats),
		       "enter",
		       G_CALLBACK (on_button_valid_errvec_resstats_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->button_errvec_resstats),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->button_errvec_resstats),
		       "clicked",
		       G_CALLBACK (on_button_valid_errvec_resstats),
		       NULL);



    valid->button_peaklck = gtk_button_new_with_label ( _("sub-pixel"));
    gtk_widget_ref (valid->button_peaklck);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "button_peaklck",
			     valid->button_peaklck,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->button_peaklck);
    gtk_box_pack_start (GTK_BOX (valid->hbox_histo_buttons),
		       valid->button_peaklck,
		       FALSE, 
		       FALSE, 
		       0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 valid->button_peaklck,
			  _("shows histogram of sub-pixel displacements "
"to check on peak-locking effects"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (valid->button_peaklck), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->button_peaklck),
		       "enter",
		       G_CALLBACK (on_button_valid_peaklck_enter),
		       valid->button_peaklck);
    g_signal_connect (GTK_OBJECT (valid->button_peaklck),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       valid->button_peaklck);
    g_signal_connect (GTK_OBJECT (valid->button_peaklck),
		       "clicked",
		       G_CALLBACK (on_button_valid_peaklck),
		       valid->button_peaklck);




    valid->button_uhisto = gtk_button_new_with_label ( _("U-comp"));
    gtk_widget_ref (valid->button_uhisto);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "button_uhisto",
			     valid->button_uhisto,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->button_uhisto);
    gtk_box_pack_start (GTK_BOX (valid->hbox_histo_buttons),
		       valid->button_uhisto,
		       FALSE, 
		       FALSE, 
		       0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 valid->button_uhisto,
			  _("shows histogram of horizontal displacements.\n\
Only this histogram will be saved"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (valid->button_uhisto), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->button_uhisto),
		       "enter",
		       G_CALLBACK (on_button_valid_uhisto_enter),
		       valid->button_uhisto);
    g_signal_connect (GTK_OBJECT (valid->button_uhisto),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       valid->button_uhisto);
    g_signal_connect (GTK_OBJECT (valid->button_uhisto),
		       "clicked",
		       G_CALLBACK (on_button_valid_uhisto),
		       valid->button_uhisto);



    valid->button_vhisto = gtk_button_new_with_label ( _("V-comp"));
    gtk_widget_ref (valid->button_vhisto);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "button_vhisto",
			     valid->button_vhisto,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->button_vhisto);
    gtk_box_pack_start (GTK_BOX (valid->hbox_histo_buttons),
		       valid->button_vhisto,
		       FALSE, 
		       FALSE, 
		       0);
    gtk_tooltips_set_tip(gpiv->tooltips,
			 valid->button_vhisto,
			  _("shows histogram of vertical displacements.\n\
Only this histogram will be saved"),
			 NULL);

    gtk_object_set_data (GTK_OBJECT (valid->button_vhisto), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->button_vhisto),
		       "enter",
		       G_CALLBACK (on_button_valid_vhisto_enter),
		       valid->button_vhisto);
    g_signal_connect (GTK_OBJECT (valid->button_vhisto),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       valid->button_vhisto);
    g_signal_connect (GTK_OBJECT (valid->button_vhisto),
		       "clicked",
		       G_CALLBACK (on_button_valid_vhisto),
		       valid->button_vhisto);




/*
 * Errvec frame
 */
    valid->frame_errvec = gtk_frame_new ( _("Outliers"));
    gtk_widget_ref (valid->frame_errvec);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_errvec",
			     valid->frame_errvec,
			     (GtkDestroyNotify)
			     gtk_widget_unref);
    gtk_widget_show (valid->frame_errvec);
    gtk_table_attach (GTK_TABLE (valid->table),
		     valid->frame_errvec,
		     1,
		     2,
		     1,
		     2,
		     (GtkAttachOptions)(GTK_FILL),
		     (GtkAttachOptions)(GTK_SHRINK),
		     0,
		     0);



    valid->vbox_errvec = gtk_vbox_new (FALSE,
				      0);
    gtk_widget_ref (valid->vbox_errvec);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
                             "vbox_errvec", 
                             valid->vbox_errvec,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->vbox_errvec);
    gtk_container_add (GTK_CONTAINER (valid->frame_errvec), 
                      valid->vbox_errvec);



/*
 * Spinner for array length of neighboring PIV data
 */
    valid->hbox_errvec_neighbors_spin = gtk_hbox_new (FALSE,
						 0);
    gtk_widget_ref (valid->hbox_errvec_neighbors_spin);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "hbox_errvec_neighbors_spin",
			     valid->hbox_errvec_neighbors_spin,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->hbox_errvec_neighbors_spin);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec), 
                               valid->hbox_errvec_neighbors_spin,
		       TRUE,
		       TRUE,
		       0);



    valid->label_errvec_neighbors = gtk_label_new ( _("Neighbors: "));
    gtk_widget_ref (valid->label_errvec_neighbors);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_errvec_neighbors",
			     valid->label_errvec_neighbors,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->label_errvec_neighbors);
    gtk_box_pack_start (GTK_BOX (valid->hbox_errvec_neighbors_spin),
		       valid->label_errvec_neighbors,
		       FALSE,
		       FALSE, 
		       0);



    valid->spinbutton_adj_errvec_neighbors = 
        gtk_adjustment_new (gl_valid_par->neighbors,
			    3,
			    GPIV_VALIDPAR_MAX__NEIGHBORS,
			    2,
			    2,
			    2);
    valid->spinbutton_errvec_neighbors = 
        gtk_spin_button_new (GTK_ADJUSTMENT (valid->spinbutton_adj_errvec_neighbors),
       		            2, 
			    0);
    gtk_widget_ref (valid->spinbutton_errvec_neighbors);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_errvec_neighbors",
			     valid->spinbutton_errvec_neighbors,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->spinbutton_errvec_neighbors);
    gtk_box_pack_start (GTK_BOX (valid->hbox_errvec_neighbors_spin),
		       valid->spinbutton_errvec_neighbors,
		       TRUE,
		       TRUE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->spinbutton_errvec_neighbors), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->spinbutton_errvec_neighbors), 
                      "changed", 
		      G_CALLBACK (on_spinbutton_valid_errvec_neighbors), 
                      NULL);




/*
 * Spinner for data yield
 */
    valid->hbox_errvec_yield_spin = gtk_hbox_new (FALSE,
						 0);
    gtk_widget_ref (valid->hbox_errvec_yield_spin);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "hbox_errvec_yield_spin",
			     valid->hbox_errvec_yield_spin,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->hbox_errvec_yield_spin);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec), 
                               valid->hbox_errvec_yield_spin,
		       TRUE,
		       TRUE,
		       0);



    valid->label_errvec_yield = gtk_label_new ( _("Data yield: "));
    gtk_widget_ref (valid->label_errvec_yield);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_errvec_yield",
			     valid->label_errvec_yield,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->label_errvec_yield);
    gtk_box_pack_start (GTK_BOX (valid->hbox_errvec_yield_spin),
		       valid->label_errvec_yield,
		       FALSE,
		       FALSE, 
		       0);



    valid->spinbutton_adj_errvec_yield = 
        gtk_adjustment_new (gl_valid_par->data_yield,
			   0.0,
			   1.0,
			   0.01,
			   0.1,
			   0.1);
    valid->spinbutton_errvec_yield = 
        gtk_spin_button_new (GTK_ADJUSTMENT (valid->spinbutton_adj_errvec_yield),
       		            0.1, 
			    3);
    gtk_widget_ref (valid->spinbutton_errvec_yield);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_errvec_yield",
			     valid->spinbutton_errvec_yield,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->spinbutton_errvec_yield);
    gtk_box_pack_start (GTK_BOX (valid->hbox_errvec_yield_spin),
		       valid->spinbutton_errvec_yield,
		       TRUE,
		       TRUE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->spinbutton_errvec_yield), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->spinbutton_errvec_yield), 
                      "changed", 
		      G_CALLBACK (on_spinbutton_valid_errvec_yield), 
                      NULL);




/*
 * Spinner for threshold residu
 */    valid->hbox_errvec_residu_spin = gtk_hbox_new (FALSE,
						  0);
    gtk_widget_ref (valid->hbox_errvec_residu_spin);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "hbox_errvec_residu_spin",
			     valid->hbox_errvec_residu_spin,
			    (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->hbox_errvec_residu_spin);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec), 
		       valid->hbox_errvec_residu_spin,
		       TRUE,
		       TRUE,
		       0);



    valid->label_errvec_res = gtk_label_new ( _("Threshold: "));
    gtk_widget_ref (valid->label_errvec_res);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "label_errvec_res",
			     valid->label_errvec_res,
			    (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->label_errvec_res);
    gtk_box_pack_start (GTK_BOX (valid->hbox_errvec_residu_spin),
		       valid->label_errvec_res,
		       FALSE,
		       FALSE, 
		       0);



    valid->spinbutton_adj_errvec_res = 
        gtk_adjustment_new (gl_valid_par->residu_max,
			   0., 
			   THRESHOLD_MAX,
			   0.1, 
			   1.0, 
			   1.0);
    valid->spinbutton_errvec_res = 
        gtk_spin_button_new (GTK_ADJUSTMENT (valid->spinbutton_adj_errvec_res),
       		            0.1,
			    3);
    gtk_widget_ref (valid->spinbutton_errvec_res);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "spinbutton_errvec_res",
			     valid->spinbutton_errvec_res,
			    (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->spinbutton_errvec_res);
    gtk_box_pack_start (GTK_BOX (valid->hbox_errvec_residu_spin),
		       valid->spinbutton_errvec_res,
		       TRUE, 
		       TRUE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->spinbutton_errvec_res), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->spinbutton_errvec_res), 
                      "changed", 
		      G_CALLBACK (on_spinbutton_valid_errvec_res), 
                      NULL);




    valid->checkbutton_errvec_disres =
	gtk_check_button_new_with_label ( _("Show SNR values"));
    gtk_widget_ref (valid->checkbutton_errvec_disres);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "checkbutton_errvec_disres",
			     valid->checkbutton_errvec_disres,
			    (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->checkbutton_errvec_disres);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec),
		       valid->checkbutton_errvec_disres,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->checkbutton_errvec_disres), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->checkbutton_errvec_disres),
		       "enter",
		       G_CALLBACK
		      (on_checkbutton_valid_errvec_disres_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->checkbutton_errvec_disres),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->checkbutton_errvec_disres),
		       "toggled",
		       G_CALLBACK (on_checkbutton_valid_errvec_disres),
		       NULL);
    if (v_color == SHOW_SNR) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				   (valid->checkbutton_errvec_disres),
				    TRUE);
    }



    valid->frame_errvec_residu = gtk_frame_new ( _("Residu type:"));
    gtk_widget_ref (valid->frame_errvec_residu);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_errvec_residu", 
                              valid->frame_errvec_residu,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->frame_errvec_residu);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec), 
                       valid->frame_errvec_residu,
		       FALSE,
		       FALSE,
		       0);
    gtk_frame_set_shadow_type (GTK_FRAME (valid->frame_errvec_residu),
			      GTK_SHADOW_NONE);



    valid->vbox_errvec_residu = gtk_vbox_new (FALSE,
					     0);
    gtk_widget_ref (valid->vbox_errvec_residu);
    gtk_object_set_data_full (GTK_OBJECT (main_window), 
                             "vbox_errvec_residu", 
                             valid->vbox_errvec_residu,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->vbox_errvec_residu);
    gtk_container_add (GTK_CONTAINER (valid->frame_errvec_residu), 
                      valid->vbox_errvec_residu);



    valid->radiobutton_errvec_residu_normmedian =
	gtk_radio_button_new_with_label (valid->vbox_errvec_residu_group, 
                                        _("Normalized median"));
    valid->vbox_errvec_residu_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_errvec_residu_normmedian));
    gtk_widget_ref (valid->radiobutton_errvec_residu_normmedian);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_errvec_residu_normmedian",
			     valid->radiobutton_errvec_residu_normmedian,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_residu_normmedian), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_residu_normmedian),
			"residu",
			"2");
     gtk_widget_show (valid->radiobutton_errvec_residu_normmedian);
     gtk_box_pack_start (GTK_BOX (valid->vbox_errvec_residu), 
			valid->radiobutton_errvec_residu_normmedian,
			FALSE,
			FALSE,
			0);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_normmedian),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_valid_errvec_residu_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_normmedian),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_normmedian),
		       "clicked",
		       G_CALLBACK (on_radiobutton_valid_errvec_residu),
		       NULL);
    if (gl_valid_par->residu_type == 2) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (valid->radiobutton_errvec_residu_normmedian),
				    TRUE);
    }



    valid->radiobutton_errvec_residu_median =
	gtk_radio_button_new_with_label (valid->vbox_errvec_residu_group, 
                                        _("Median"));
    valid->vbox_errvec_residu_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_errvec_residu_median));
    gtk_widget_ref (valid->radiobutton_errvec_residu_median);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_errvec_residu_median",
			     valid->radiobutton_errvec_residu_median,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_residu_median), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_residu_median),
			"residu",
			"1");
     gtk_widget_show (valid->radiobutton_errvec_residu_median);
     gtk_box_pack_start (GTK_BOX (valid->vbox_errvec_residu), 
			valid->radiobutton_errvec_residu_median,
			FALSE,
			FALSE,
			0);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_median),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_valid_errvec_residu_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_median),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_median),
		       "clicked",
		       G_CALLBACK (on_radiobutton_valid_errvec_residu),
		       NULL);
    if (gl_valid_par->residu_type == 1) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (valid->radiobutton_errvec_residu_median),
				    TRUE);
    }



    valid->radiobutton_errvec_residu_snr =
	gtk_radio_button_new_with_label (valid->vbox_errvec_residu_group,
					 _("Snr"));
    valid->vbox_errvec_residu_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_errvec_residu_snr));
    gtk_widget_ref (valid->radiobutton_errvec_residu_snr);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_errvec_residu_snr",
			     valid->radiobutton_errvec_residu_snr,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->radiobutton_errvec_residu_snr);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec_residu), 
                       valid->radiobutton_errvec_residu_snr,
		       FALSE,
		       FALSE, 
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_residu_snr), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_residu_snr),
			"residu",
			"0");
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_snr),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_valid_errvec_residu_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_snr),
		       "leave",
		       G_CALLBACK (on_widget_leave), 
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_residu_snr),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_errvec_residu),
		       NULL);
    if (gl_valid_par->residu_type == 0) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (valid->radiobutton_errvec_residu_snr),
				    TRUE);
    }



/*
 * A long time ago, the GtkWidget  valid->button_errvec_resstats was *here*.
 * Though it felt in love with valid->button_peaklock.
 * Now they are living  happily cheeck tot cheeck.
 * No, they haven't got child widgets, yet. But maybe, one day ...
 */


/*
 * Radio buttons for substitution type
 */
    valid->frame_errvec_subst = gtk_frame_new ( _("Substituted by:"));
    gtk_widget_ref (valid->frame_errvec_subst);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "frame_errvec_subst",
			     valid->frame_errvec_subst,
			    (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->frame_errvec_subst);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec),
		       valid->frame_errvec_subst,
		       FALSE,
		       FALSE, 
		       0);
    gtk_frame_set_shadow_type (GTK_FRAME (valid->frame_errvec_subst),
			      GTK_SHADOW_NONE);



    valid->vbox_errvec_subst = gtk_vbox_new (FALSE,
					    0);
    gtk_widget_ref (valid->vbox_errvec_subst);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "vbox_errvec_subst",
			     valid->vbox_errvec_subst,
			    (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->vbox_errvec_subst);
    gtk_container_add (GTK_CONTAINER (valid->frame_errvec_subst),
		      valid->vbox_errvec_subst);




    valid->radiobutton_errvec_subst_0 =
	gtk_radio_button_new_with_label(valid->vbox_errvec_subst_group,
					 _("Nothing"));
    valid->vbox_errvec_subst_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			       (valid->radiobutton_errvec_subst_0));
    gtk_widget_ref(valid->radiobutton_errvec_subst_0);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_errvec_subst_0",
			     valid->radiobutton_errvec_subst_0,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(valid->radiobutton_errvec_subst_0);
    gtk_box_pack_start (GTK_BOX(valid->vbox_errvec_subst),
		       valid->radiobutton_errvec_subst_0,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_subst_0), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_subst_0),
			"substitute",
			"0" /* GPIV_VALID_SUBSTYPE__NONE */);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_0),
		       "enter",
		       G_CALLBACK 
		       (on_radiobutton_valid_errvec_subst_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_0),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_0),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_errvec_subst),
		       NULL);
    if (gl_valid_par->subst_type == GPIV_VALID_SUBSTYPE__NONE) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (valid->radiobutton_errvec_subst_0),
				    TRUE);
    }



/*
 * Substitute by mean of surroundings
 */
    valid->radiobutton_errvec_subst_1 =
	gtk_radio_button_new_with_label (valid->vbox_errvec_subst_group,
					 _("Mean of surroundings"));
    valid->vbox_errvec_subst_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			      (valid->radiobutton_errvec_subst_1));
    gtk_widget_ref (valid->radiobutton_errvec_subst_1);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_errvec_subst_1",
			     valid->radiobutton_errvec_subst_1,
			     (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(valid->radiobutton_errvec_subst_1);
    gtk_box_pack_start (GTK_BOX(valid->vbox_errvec_subst),
		       valid->radiobutton_errvec_subst_1,
		       FALSE,
		       FALSE,
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_subst_1), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_subst_1),
			"substitute",
			"1" /* GPIV_VALID_SUBSTYPE__L_MEAN */);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_1),
		       "enter",
		       G_CALLBACK
		      (on_radiobutton_valid_errvec_subst_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_1),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_1),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_errvec_subst),
		       NULL);



/*
 * Substitute by median of surroundings
 */
    valid->radiobutton_errvec_subst_2 = 
      gtk_radio_button_new_with_label (valid->vbox_errvec_subst_group,
				        _("Median"));
    valid->vbox_errvec_subst_group = 
      gtk_radio_button_group  (GTK_RADIO_BUTTON 
			      (valid->radiobutton_errvec_subst_2));
    gtk_widget_ref (valid->radiobutton_errvec_subst_2);
    gtk_object_set_data_full  (GTK_OBJECT (main_window),
			      "radiobutton_errvec_subst_2",
			      valid->radiobutton_errvec_subst_2,
			      (GtkDestroyNotify) gtk_widget_unref);
   gtk_widget_show (valid->radiobutton_errvec_subst_2);
   gtk_box_pack_start  (GTK_BOX (valid->vbox_errvec_subst),
		       valid->radiobutton_errvec_subst_2, 
		       FALSE,
		       FALSE,
		       0);

   gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_subst_2), 
		       "valid",
		       valid);
   gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_subst_2),
		       "substitute",
		       "2" /* GPIV_VALID_SUBSTYPE__MEDIAN */);  
   g_signal_connect  (GTK_OBJECT (valid->radiobutton_errvec_subst_2),
		       "enter", 
		       G_CALLBACK(on_radiobutton_valid_errvec_subst_enter),
		       NULL);
   g_signal_connect  (GTK_OBJECT (valid->radiobutton_errvec_subst_2),
		       "leave", 
		       G_CALLBACK (on_widget_leave),
		       NULL);
   g_signal_connect  (GTK_OBJECT (valid->radiobutton_errvec_subst_2),
		       "toggled", 
		       G_CALLBACK (on_radiobutton_valid_errvec_subst),
		       NULL);
   if (gl_valid_par->subst_type == GPIV_VALID_SUBSTYPE__MEDIAN ) {  
     gtk_toggle_button_set_state  (GTK_TOGGLE_BUTTON 
				  (valid->radiobutton_errvec_subst_2),
				  TRUE);
   }



    valid->radiobutton_errvec_subst_3 =
	gtk_radio_button_new_with_label (valid->vbox_errvec_subst_group,
					 _("Next highest corr. peak"));
    valid->vbox_errvec_subst_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON
			      (valid->radiobutton_errvec_subst_3));
    gtk_widget_ref (valid->radiobutton_errvec_subst_3);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "radiobutton_errvec_subst_3",
			     valid->radiobutton_errvec_subst_3,
			    (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (valid->radiobutton_errvec_subst_3);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec_subst),
		       valid->radiobutton_errvec_subst_3,
		       FALSE,
		       FALSE, 
		       0);

    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_subst_3), 
			"valid",
			valid);
    gtk_object_set_data (GTK_OBJECT (valid->radiobutton_errvec_subst_3),
			"substitute",
			"3" /* GPIV_VALID_SUBSTYPE__COV_PEAK */);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_3),
		       "enter",
		       G_CALLBACK
		       (on_radiobutton_valid_errvec_subst_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_3),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->radiobutton_errvec_subst_3),
		       "toggled",
		       G_CALLBACK (on_radiobutton_valid_errvec_subst),
		       NULL);
    if (gl_valid_par->subst_type == GPIV_VALID_SUBSTYPE__COR_PEAK) {
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
				    (valid->radiobutton_errvec_subst_3),
				    TRUE);
    }



    valid->button_errvec =
	gtk_button_new_with_label ( _("validate on outliers"));
    gtk_widget_ref (valid->button_errvec);
    gtk_object_set_data_full (GTK_OBJECT (main_window),
			     "button_errvec",
			     valid->button_errvec,
			     (GtkDestroyNotify) gtk_widget_unref);
   gtk_widget_show (valid->button_errvec);
    gtk_box_pack_start (GTK_BOX (valid->vbox_errvec),
		       valid->button_errvec,
		       FALSE, 
		       FALSE,
		       0);
    gtk_tooltips_set_tip(gpiv->tooltips, 
			 valid->button_errvec,
			  _("substitutes outliers"), 
			 NULL);

    gtk_object_set_data (GTK_OBJECT (valid->button_errvec), 
			"valid",
			valid);
    g_signal_connect (GTK_OBJECT (valid->button_errvec),
		       "enter",
		       G_CALLBACK (on_button_valid_errvec_enter),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->button_errvec),
		       "leave",
		       G_CALLBACK (on_widget_leave),
		       NULL);
    g_signal_connect (GTK_OBJECT (valid->button_errvec),
		       "clicked",
		       G_CALLBACK (on_button_valid_errvec),
		       NULL);


    return valid;
}




