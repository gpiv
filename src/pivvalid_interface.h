
/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>
   Copyright (C) 2002 Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * PIV validation tab
 * $Log: pivvalid_interface.h,v $
 * Revision 1.7  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.6  2006/01/31 14:28:13  gerber
 * version 0.3.0
 *
 * Revision 1.4  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.3  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.2  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef GPIV_PIVVALID_INTERFACE_H
#define GPIV_PIVVALID_INTERFACE_H

#define THRESHOLD_MAX 32


typedef struct _PivValid PivValid;
struct _PivValid {
  GtkWidget *vbox_label;
  GtkWidget *label_title;

  GtkWidget *vbox_scroll;
  GtkWidget *scrolledwindow;
  GtkWidget *viewport;
  GtkWidget *table;
  
  GtkWidget *frame_disable;
  GtkWidget *vbox_disable;
/*   GSList *vbox_disable_group; */
/*   GSList *disable_group; */
  GtkWidget *radiobutton_disable_0;
  GtkWidget *radiobutton_disable_1;
  GtkWidget *radiobutton_disable_2;
  GtkWidget *radiobutton_disable_3;
  GtkWidget *radiobutton_disable_4;

  GtkWidget *button_gradient;
  
  GtkWidget *frame_errvec;
  GtkWidget *vbox_errvec;
  GtkWidget *frame_errvec_residu;
  GtkWidget *vbox_errvec_residu;
  GSList *vbox_errvec_residu_group;
  GtkWidget *radiobutton_errvec_residu_snr;
  GtkWidget *radiobutton_errvec_residu_median;
  GtkWidget *radiobutton_errvec_residu_normmedian;
  GtkWidget *button_errvec_resstats;
  GtkWidget *hbox_errvec_neighbors_spin;
  GtkWidget *label_errvec_neighbors;
  GtkObject *spinbutton_adj_errvec_neighbors;
  GtkWidget *spinbutton_errvec_neighbors;
  GtkWidget *hbox_errvec_yield_spin;
  GtkWidget *label_errvec_yield;
  GtkObject *spinbutton_adj_errvec_yield;
  GtkWidget *spinbutton_errvec_yield;

  GtkWidget *hbox_errvec_residu_spin;
  GtkWidget *label_errvec_res;
  GtkObject *spinbutton_adj_errvec_res;
  GtkWidget *spinbutton_errvec_res;
  GtkWidget *checkbutton_errvec_disres;
  GtkWidget *frame_errvec_subst;
  GtkWidget *vbox_errvec_subst;
  GSList *vbox_errvec_subst_group;
  
  GtkWidget *radiobutton_errvec_subst_0;
  GtkWidget *radiobutton_errvec_subst_1;
  GtkWidget *radiobutton_errvec_subst_2;
  GtkWidget *radiobutton_errvec_subst_3;
  GtkWidget *button_errvec;

  GtkWidget *frame_histo;
  GtkWidget *vbox_histo;
  GtkWidget *hbox_histo_spin;
  GtkWidget *label_histo_bins;
  GtkObject *spinbutton_adj_histo_bins;
  GtkWidget *spinbutton_histo_bins;
  GtkWidget *hbox_histo_buttons;
  GtkWidget *button_peaklck;
  GtkWidget *button_uhisto;
  GtkWidget *button_vhisto;
  GtkWidget *canvas_histo;
  GtkWidget *label;
};


PivValid *
create_pivvalid (GnomeApp *main_window, 
		 GtkWidget *container);

#endif /* GPIV_PIVVALID_INTERFACE_H */
