/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: preferences.c,v $
 * Revision 1.18  2008-10-09 14:43:37  gerber
 * paralellized with OMP and MPI
 *
 * Revision 1.17  2008-04-28 12:00:58  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.16  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.15  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.14  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.13  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.12  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.11  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.10  2006/01/31 14:28:13  gerber
 * version 0.3.0
 *
 * Revision 1.8  2005/01/19 15:53:42  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.7  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.6  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.5  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.4  2003/08/22 15:24:52  gerber
 * interactive spatial scaling
 *
 * Revision 1.3  2003/07/25 15:40:24  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif /* HAVE_CONFIG_H */
#include "support.h"

#include "gpiv_gui.h"
#include "utils.h"
#include "preferences.h"
#include "preferences_interface.h"


static void
apply_gpivpar_console (GpivConsole * gpiv);

static void
apply_gpivpar_display ();


void 
on_checkbutton_gpivbuttons_activate(GtkWidget *widget, 
                                         gpointer data
                                         )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->console__view_gpivbuttons = 1;
    } else {
	default_par->console__view_gpivbuttons = 0;
    }
}



void 
on_checkbutton_tab_activate(GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->console__view_tabulator = 1;
    } else {
	default_par->console__view_tabulator = 0;
    }
}



void 
on_checkbutton_hdf_activate(GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->hdf = 1;
    } else {
	default_par->hdf = 0;
    }
}



void 
on_radiobutton_datafmt(GtkWidget *widget,
                      gpointer data
                      )
/*-----------------------------------------------------------------------------
 */
{
    gint select = atoi(gtk_object_get_data(GTK_OBJECT(widget),
                                               "data_format"));
    default_par->hdf = select;
        if (gpiv_par->verbose) g_message("on_radiobutton_datafmt:: %d", default_par->hdf);
}



void 
on_radiobutton_imgfmt(GtkWidget *widget,
                      gpointer data
                      )
/*-----------------------------------------------------------------------------
 */
{
    gint fmt_select = atoi(gtk_object_get_data(GTK_OBJECT(widget),
                                               "image_format"));
    gpiv_par->img_fmt = fmt_select;

    if (fmt_select == IMG_FMT_PNG) {
        if (gpiv_par->verbose) g_message("on_radiobutton_imgfmt:: img_fmt = IMG_FMT_PNG");
    } else if (fmt_select == IMG_FMT_HDF) {
        if (gpiv_par->verbose) g_message("on_radiobutton_imgfmt:: img_fmt = IMG_FMT_HDF");
    } else if (fmt_select == IMG_FMT_RAW) {
        if (gpiv_par->verbose) g_message("on_radiobutton_imgfmt:: img_fmt = IMG_FMT_RAW");
    } else {
        g_message("INVALID IMG_FMT");
    }
}



void 
on_checkbutton_xcorr_activate(GtkWidget *widget, 
                                   gpointer data
                                   )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->x_corr = 1;
    } else {
	default_par->x_corr = 0;
    }
}



void 
on_checkbutton_tooltips_activate(GtkWidget *widget, 
                                      gpointer data
                                      )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->console__show_tooltips = 1;
    } else {
	default_par->console__show_tooltips = 0;
    }
}



#ifdef ENABLE_CAM
void 
on_checkbutton_process_cam_activate(GtkWidget *widget, 
                                         gpointer data
                                         )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__cam = TRUE;
    } else {
	default_par->process__cam = FALSE;
    }
}
#endif /* ENABLE_CAM */


#ifdef ENABLE_TRIG
void 
on_checkbutton_process_trig_activate(GtkWidget *widget, 
                                     gpointer data
                                     )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__trig = TRUE;
    } else {
	default_par->process__trig = FALSE;
    }
}
#endif /* ENABLE_TRIG */


#ifdef ENABLE_IMGPROC
void 
on_checkbutton_process_imgproc_activate(GtkWidget *widget, 
                                     gpointer data
                                     )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__imgproc = TRUE;
    } else {
	default_par->process__imgproc = FALSE;
    }
}
#endif /* ENABLE_IMGPROC */


void 
on_checkbutton_process_piv_activate(GtkWidget *widget, 
                                         gpointer data
                                         )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__piv = TRUE;
    } else {
	default_par->process__piv = FALSE;
    }
}



void 
on_checkbutton_process_gradient_activate(GtkWidget *widget, 
                                              gpointer data
                                              )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__gradient = TRUE;
    } else {
	default_par->process__gradient = FALSE;
    }
}



void 
on_checkbutton_process_resstats_activate(GtkWidget *widget, 
                                              gpointer data
                                              )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__resstats = TRUE;
    } else {
	default_par->process__resstats = FALSE;
    }
}



void 
on_checkbutton_process_errvec_activate(GtkWidget *widget, 
                                            gpointer data
                                            )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__errvec = TRUE;
    } else {
	default_par->process__errvec = FALSE;
    }
}



void 
on_checkbutton_process_peaklck_activate(GtkWidget *widget, 
                                             gpointer data
                                             )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__peaklock = TRUE;
    } else {
	default_par->process__peaklock = FALSE;
    }
}



void 
on_checkbutton_process_subtract_activate(GtkWidget *widget, 
                                              gpointer data
                                              )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__subtract = TRUE;
    } else {
	default_par->process__subtract = FALSE;
    }
}



void 
on_checkbutton_process_scale_activate(GtkWidget *widget, 
                                           gpointer data
                                           )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__scale = TRUE;
    } else {
	default_par->process__scale = FALSE;
    }
}



void 
on_checkbutton_process_avg_activate(GtkWidget *widget, 
                                         gpointer data
                                         )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__average = TRUE;
    } else {
	default_par->process__average = FALSE;
    }
}



void 
on_checkbutton_process_vorstra_activate(GtkWidget *widget, 
                                             gpointer data
                                             )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->process__vorstra = TRUE;
    } else {
	default_par->process__vorstra = FALSE;
    }
}



void 
on_spinbutton_bins_activate(GtkWidget *widget, 
		  gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{}



#ifdef ENABLE_MPI
void 
on_spinbutton_nodes(GtkSpinButton *widget, 
                    GtkWidget *entry
                    )
/*-----------------------------------------------------------------------------
 */
{
    default_par->mpi_nodes = gtk_spin_button_get_value_as_int (widget);
}
#endif


void
on_radiobutton_display_vecscale(GtkWidget *widget, 
                                     gpointer data
                                     )
/*-----------------------------------------------------------------------------
 */
{
    default_par->display__vector_scale = 
        atoi(gtk_object_get_data(GTK_OBJECT(widget), 
                                 "vscale"));
}



void
on_radiobutton_display_veccolor(GtkWidget *widget, 
                                     gpointer data
                                     )
/*-----------------------------------------------------------------------------
 */
{
    default_par->display__vector_color = 
        atoi(gtk_object_get_data(GTK_OBJECT(widget), 
                                 "vcolor"));
}



void
on_radiobutton_display_zoomscale(GtkWidget *widget, 
                                 gpointer data
                                 )
/*-----------------------------------------------------------------------------
 */
{
    default_par->display__zoom_index = 
        atoi(gtk_object_get_data(GTK_OBJECT(widget), 
                                 "zscale"));
}



void
on_radiobutton_display_background(GtkWidget *widget, 
                                  gpointer data
                                  )
/*-----------------------------------------------------------------------------
 */
{
    default_par->display__backgrnd =
        atoi (gtk_object_get_data (GTK_OBJECT (widget), "bgcolor"));
    if (gpiv_par->verbose) g_message ("on_radiobutton_display_background:: display__backgrnd = %d",
               default_par->display__backgrnd);
}



void
on_checkbutton_display_display_intregs(GtkWidget *widget, 
                                       gpointer data
                                       )
/*-----------------------------------------------------------------------------
 */

{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->display__intregs = TRUE;
    } else {
	default_par->display__intregs = FALSE;
    }
}



void
on_checkbutton_display_display_piv(GtkWidget *widget, 
                                   gpointer data
                                   )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->display__piv = TRUE;
    } else {
	default_par->display__piv = FALSE;
    }
}



void
on_radiobutton_display_scalar(GtkWidget *widget, 
                                  gpointer data
                                  )
/*-----------------------------------------------------------------------------
 */
{
    default_par->display__scalar =
        atoi (gtk_object_get_data (GTK_OBJECT (widget), "scalar"));
    if (gpiv_par->verbose) g_message ("on_radiobutton_display_scalar:: display__scalar = %d",
               default_par->display__scalar);
}



void 
on_checkbutton_display_view_menubar_activate (GtkWidget *widget, 
                                              gpointer data
                                              )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->display__view_menubar = 1;
    } else {
	default_par->display__view_menubar = 0;
    }
}



void 
on_checkbutton_display_view_rulers_activate (GtkWidget *widget, 
                                              gpointer data
                                              )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->display__view_rulers = 1;
    } else {
	default_par->display__view_rulers = 0;
    }
}



void 
on_checkbutton_display_stretch_auto_activate (GtkWidget *widget, 
                                              gpointer data
                                              )
/*-----------------------------------------------------------------------------
 */
{
    if (GTK_TOGGLE_BUTTON(widget)->active) {
	default_par->display__stretch_auto = TRUE;
    } else {
	default_par->display__stretch_auto = FALSE;
    }
}



void
on_preferences_response(GtkDialog *dialog,
			gint response,
			gpointer data
			)
/*-----------------------------------------------------------------------------
 */
{
    GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(dialog), "gpiv");
    Pref *pref = gtk_object_get_data(GTK_OBJECT(dialog), "pref");
    g_assert( response == GTK_RESPONSE_OK
              || response == GTK_RESPONSE_APPLY
              || response == GTK_RESPONSE_CANCEL);

    switch (response) {
    case GTK_RESPONSE_OK:
        apply_gpivpar(gpiv);
        store_defaultpar();
        gtk_widget_destroy(GTK_WIDGET (dialog));
        break;
 
   case GTK_RESPONSE_APPLY:
       apply_gpivpar(gpiv);
       break;

    case GTK_RESPONSE_CANCEL:
        gtk_widget_destroy(GTK_WIDGET (dialog));
        break;

    default:
        g_warning("on_preferences_response: should not arrive here");
        break;
    }

}



void
apply_gpivpar (GpivConsole * gpiv)
/* ----------------------------------------------------------------------------
 * Updates actual parameters
 */
{
    gint i;

    apply_gpivpar_console(gpiv);

    for (i = 0; i < nbufs; i++) {
        display_act = display[i];
        if (display_act != NULL) {
            if (gpiv_par->verbose) g_message("apply_gpivpar:: ibuf = %d apply_gpivpar_display", i);
            apply_gpivpar_display ();
        }
    }
}

static void
apply_gpivpar_console(GpivConsole *gpiv)
/* ----------------------------------------------------------------------------
 * Updates actual parameters for the console
 */
{
    GtkWidget *settings_menu_gpiv0 = 
        gtk_object_get_data (GTK_OBJECT (gpiv->console), "settings_menu_gpiv0");
    GtkWidget *settings_menu_gpiv1 = 
        gtk_object_get_data (GTK_OBJECT (gpiv->console), "settings_menu_gpiv1");
    GtkWidget *help_menu_gpiv0 = 
        gtk_object_get_data (GTK_OBJECT (gpiv->console), "help_menu_gpiv0");
    GtkWidget *help_menu_gpiv_popup0 = 
        gtk_object_get_data (GTK_OBJECT (gpiv->console), "help_menu_gpiv_popup0");


    parameters_set(gpiv_par, FALSE);
    cp_undef_parameters(default_par, gpiv_par);


    if (default_par->console__view_gpivbuttons == 1) {
        gtk_widget_show(gpiv->handlebox1);
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM
				       (settings_menu_gpiv0), TRUE);
    } else {
        gtk_widget_hide(gpiv->handlebox1);
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM
				       (settings_menu_gpiv0), FALSE);
    }
    
    if (default_par->console__view_tabulator == 1) {
        gtk_widget_show(gpiv->notebook);
        gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM
                                       (settings_menu_gpiv1), TRUE);
    } else {
        gtk_widget_hide(gpiv->notebook);
 	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM
				       (settings_menu_gpiv1), FALSE);
    }

    if (default_par->console__show_tooltips == 1) {
        gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM
                                       (help_menu_gpiv0), TRUE);
        gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM
                                       (help_menu_gpiv_popup0), TRUE);
    } else {
        gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM
                                       (help_menu_gpiv0), FALSE);
        gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM
                                       (help_menu_gpiv_popup0), FALSE);
    }
    
    gtk_spin_button_set_value(GTK_SPIN_BUTTON
                              (gpiv->pivvalid->spinbutton_histo_bins), 
                              default_par->console__nbins);
    

    
    
#ifdef ENABLE_CAM
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                 (gpiv->button_toolbar_cam), 
                                 default_par->process__cam);
#endif /* ENABLE_CAM */

#ifdef ENABLE_TRIG
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                 (gpiv->button_toolbar_trig), 
                                 default_par->process__trig);
#endif /* ENABLE_TRIG */

#ifdef ENABLE_IMGPROC
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                 (gpiv->button_toolbar_imgproc), 
                                 default_par->process__imgproc);
#endif /* ENABLE_IMGPROC */

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                 (gpiv->button_toolbar_piv), 
                                 default_par->process__piv);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_gradient), 
                                default_par->process__gradient);
   
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_resstats), 
                                default_par->process__resstats);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_errvec), 
                                default_par->process__errvec);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_peaklock), 
                                default_par->process__peaklock);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_scale), 
                                default_par->process__scale);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_average), 
                                default_par->process__average);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_subavg), 
                                default_par->process__subtract);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (gpiv->button_toolbar_vorstra), 
                                default_par->process__vorstra);
    
}


static void
apply_gpivpar_display (void)
/* ----------------------------------------------------------------------------
 * Updates actual parameters for the actual display
 */
{
    GtkWidget *view_menubar_popup_menu = 
        gtk_object_get_data(GTK_OBJECT(display_act->mwin), 
                            "view_menubar_popup_menu");
    GtkWidget *view_rulers_popup_menu = 
        gtk_object_get_data(GTK_OBJECT(display_act->mwin), 
                            "view_rulers_popup_menu");

    GtkWidget *stretch_auto_popup_menu = 
        gtk_object_get_data(GTK_OBJECT(display_act->mwin), 
                            "stretch_auto_popup_menu");



    /*     for (i = 0; i < nbufs; i++) { */
    /*         display_act = display[i]; */
    /*         if (display_act != NULL) { */
    if (display_act->pida->exist_piv
        && display_act->display_piv) {
        update_all_vectors(display_act->pida);
    }
    

    if (display_act->display_intregs) {
        if (default_par->display__intregs == 1) {
            create_all_intregs2(display_act);
            create_all_intregs1(display_act);
        } else {
            hide_all_intregs1(display_act);
            hide_all_intregs2(display_act);
        }
    }
        
        
    if (display_act->pida->exist_piv) {
        if (default_par->display__piv) {
            create_all_vectors(display_act->pida);
        } else {
            hide_all_vectors(display_act->pida);
        }
    }
        
        
    if (display_act->pida->exist_vor) {
        if (default_par->display__scalar == SHOW_SC_VORTICITY) {
            create_all_scalars (display_act, GPIV_VORTICITY);
        } else {
            hide_all_scalars (display_act, GPIV_VORTICITY);
        }
    }
        
        
    if (display_act->pida->exist_sstrain) {
        if (default_par->display__scalar == SHOW_SC_SSTRAIN) {
            create_all_scalars (display_act, GPIV_S_STRAIN);
        } else {
            hide_all_scalars (display_act, GPIV_S_STRAIN);
        }
    }
        
        
    if (display_act->pida->exist_nstrain) {
        if (default_par->display__scalar == SHOW_SC_NSTRAIN) {
            create_all_scalars (display_act, GPIV_N_STRAIN);
        } else {
            hide_all_scalars (display_act, GPIV_N_STRAIN);
        }
    }

    if (default_par->display__view_menubar == 1) {
        gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (view_menubar_popup_menu),
                                        TRUE);
    } else {
        gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (view_menubar_popup_menu),
                                        FALSE);
    }
    
    if (default_par->display__view_rulers == 1) {
        gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (view_rulers_popup_menu),
                                        TRUE);
    } else {
        gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (view_rulers_popup_menu),
                                        FALSE);
    }
    
    if (default_par->display__stretch_auto == TRUE) {
        gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (stretch_auto_popup_menu),
                                        TRUE);
    } else {
        gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM
                                        (stretch_auto_popup_menu),
                                        FALSE);
    }

        
}


void
store_defaultpar (void
                  )
/* ----------------------------------------------------------------------------
 * Store parameters as defaults
 */
{
    gnome_config_push_prefix ("/gpiv/General/");
/*     gnome_config_set_bool ("print_par", print_par); */
    gnome_config_set_bool ("console__show_tooltips", default_par->console__show_tooltips);
    gnome_config_set_bool ("console__view_gpivbuttons", default_par->console__view_gpivbuttons);
    gnome_config_set_bool ("console__view_tabulator", default_par->console__view_tabulator);
    gnome_config_set_int ("console__nbins", default_par->console__nbins);
    gnome_config_set_bool ("img_fmt", default_par->img_fmt);
    gnome_config_set_bool ("hdf", default_par->hdf);
#ifdef ENABLE_MPI
    gnome_config_set_int ("mpi_nodes", default_par->mpi_nodes);
#endif /* ENABLE_MPI */


    gnome_config_pop_prefix ();
    gnome_config_push_prefix ("/gpiv/Image/");
    gnome_config_set_bool ("x_corr", default_par->x_corr);
    

    gnome_config_pop_prefix ();
    gnome_config_push_prefix ("/gpiv/Processes/");
#ifdef ENABLE_CAM
    gnome_config_set_bool ("process__cam", default_par->process__cam);
#endif /* ENABLE_CAM */
#ifdef ENABLE_TRIG
    gnome_config_set_bool ("process__trig", default_par->process__trig);
#endif /* ENABLE_TRIG */
#ifdef ENABLE_IMGPROC
    gnome_config_set_bool ("process__imgproc", default_par->process__imgproc);
#endif /* ENABLE_IMGPROC */
    gnome_config_set_bool ("process__piv", default_par->process__piv);
    gnome_config_set_bool ("process__gradient", default_par->process__gradient);
    gnome_config_set_bool ("process__resstats", default_par->process__resstats);
    gnome_config_set_bool ("process__errvec", default_par->process__errvec);
    gnome_config_set_bool ("process__peaklock", default_par->process__peaklock);
    gnome_config_set_bool ("process__scale", default_par->process__scale);
    gnome_config_set_bool ("process__average", default_par->process__average);
    gnome_config_set_bool ("process__subtract", default_par->process__subtract);
    gnome_config_set_bool ("process__vorstra", default_par->process__vorstra);
    

    gnome_config_pop_prefix ();
    gnome_config_push_prefix ("/gpiv/Display/");
    gnome_config_set_bool ("display__view_menubar", default_par->display__view_menubar);
    gnome_config_set_bool ("display__view_rulers", default_par->display__view_rulers);
    gnome_config_set_bool ("display__stretch_auto", default_par->display__stretch_auto);
    gnome_config_set_int ("display__vector_scale", default_par->display__vector_scale);
    gnome_config_set_int ("display__vector_color", default_par->display__vector_color);
    gnome_config_set_int ("display__zoom_index", default_par->display__zoom_index);
    gnome_config_set_int ("display__backgrnd", default_par->display__backgrnd);
    gnome_config_set_bool ("display__intregs", default_par->display__intregs);
    gnome_config_set_bool ("display__piv", default_par->display__piv);
    gnome_config_set_int ("display__scalar", default_par->display__scalar);
    

    gnome_config_pop_prefix ();
    gnome_config_sync ();

}

