/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: preferences.h,v $
 * Revision 1.13  2008-10-09 14:43:37  gerber
 * paralellized with OMP and MPI
 *
 * Revision 1.12  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.11  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.10  2007/02/05 15:17:09  gerber
 * auto stretching, broadcast display settings to buffers from preferences
 *
 * Revision 1.9  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.8  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.7  2006/01/31 14:28:13  gerber
 * version 0.3.0
 *
 * Revision 1.5  2005/01/19 15:53:43  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.4  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.3  2003/08/22 15:24:53  gerber
 * interactive spatial scaling
 *
 * Revision 1.2  2003/07/25 15:40:24  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef PREFERENCES_H
#define PREFERENCES_H
/* #ifdef HAVE_CONFIG_H */
/* #  include <config.h> */
/* #endif */

#include "gpiv_gui.h"

/*------------------------------------------------------------------------
 * widgets from preferences
 */

GtkDialog *
create_preferences (GpivConsole *gpiv);


void
on_checkbutton_gpivbuttons_activate (GtkWidget *widget, 
				     gpointer data);

void
on_checkbutton_tab_activate (GtkWidget *widget, 
			     gpointer data);

void
on_radiobutton_imgfmt (GtkWidget *widget, 
		       gpointer data
		       );
void
on_radiobutton_datafmt (GtkWidget *widget, 
			gpointer data
			);
/* void */
/* on_checkbutton_hdf_activate(GtkWidget *widget,  */
/* 				 gpointer data); */

void
on_checkbutton_xcorr_activate (GtkWidget *widget, 
			       gpointer data);



void
on_checkbutton_tooltips_activate (GtkWidget *widget, 
				  gpointer data);

#ifdef ENABLE_CAM
void
on_checkbutton_process_cam_activate (GtkWidget *widget, 
				     gpointer data);
#endif /* ENABLE_CAM */

#ifdef ENABLE_TRIG
void
on_checkbutton_process_trig_activate (GtkWidget *widget, 
				      gpointer data);
#endif /* ENABLE_TRIG */

#ifdef ENABLE_IMGPROC
void
on_checkbutton_process_imgproc_activate (GtkWidget *widget, 
				      gpointer data);
#endif /* ENABLE_IMGPROC */

void
on_checkbutton_process_piv_activate (GtkWidget *widget, 
				     gpointer data);

void
on_checkbutton_process_gradient_activate (GtkWidget *widget, 
					  gpointer data);

void
on_checkbutton_process_resstats_activate (GtkWidget *widget, 
					  gpointer data);

void
on_checkbutton_process_errvec_activate (GtkWidget *widget, 
					gpointer data);

void
on_checkbutton_process_peaklck_activate (GtkWidget *widget, 
					 gpointer data);

void
on_checkbutton_process_scale_activate (GtkWidget *widget, 
				       gpointer data);

void
on_checkbutton_process_avg_activate (GtkWidget *widget, 
				     gpointer data);

void
on_checkbutton_process_subtract_activate (GtkWidget *widget, 
					  gpointer data);

void
on_checkbutton_process_vorstra_activate (GtkWidget *widget, 
					 gpointer data);

void
on_spinbutton_bins_activate (GtkWidget *widget, 
			     gpointer data);

#ifdef ENABLE_MPI
void
on_spinbutton_nodes (GtkSpinButton *widget, 
		     GtkWidget *entry);
#endif /* ENABLE_MPI */

void
on_radiobutton_display_zoomscale (GtkWidget *widget, 
				  gpointer data);

void
on_radiobutton_display_background (GtkWidget *widget, 
				   gpointer data);

void
on_radiobutton_display_vecscale (GtkWidget *widget, 
				 gpointer data);

void
on_radiobutton_display_veccolor (GtkWidget *widget, 
				 gpointer data);

void
on_checkbutton_display_display_intregs (GtkWidget *widget, 
					gpointer data);

void
on_checkbutton_display_display_piv (GtkWidget *widget, 
				    gpointer data);

void
on_radiobutton_display_scalar(GtkWidget *widget, 
			      gpointer data
			      );

void
on_checkbutton_display_view_menubar_activate (GtkWidget *widget, 
					      gpointer data);

void
on_checkbutton_display_view_rulers_activate (GtkWidget *widget, 
					     gpointer data);

void
on_checkbutton_display_stretch_auto_activate (GtkWidget *widget, 
					      gpointer data);

void
on_preferences_response (GtkDialog *dialog,
			 gint response,
			 gpointer data);

void
apply_gpivpar (GpivConsole * gpiv);


void
store_defaultpar (void);

#endif /* PREFERENCES_H */
