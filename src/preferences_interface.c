/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Id: preferences_interface.c,v 1.2 2008-10-09 14:43:37 gerber Exp $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include "support.h"

#include "gpiv_gui.h"
#include "preferences.h"
#include "preferences_interface.h"
#include "console_menus.h"
#include "display.h"



GtkDialog *
create_preferences (GpivConsole *gpiv
                    )
/*-----------------------------------------------------------------------------
 */
{
    Pref *pref = g_new0 (Pref, 1);
    GdkPixbuf *logo = NULL;


    logo = gdk_pixbuf_new_from_file(PIXMAPSDIR "gpiv_logo.png", NULL);
    pref->preferences = GTK_DIALOG( gtk_dialog_new_with_buttons( 
                        "gpiv preferences",
			NULL,
                        GTK_DIALOG_DESTROY_WITH_PARENT,
                        GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                        GTK_STOCK_APPLY, GTK_RESPONSE_APPLY,
                        GTK_STOCK_OK, GTK_RESPONSE_OK,
                        NULL
			));
    gtk_widget_show(GTK_WIDGET (pref->preferences)); 
    gtk_object_set_data (GTK_OBJECT (pref->preferences),
                         "gpiv",
                         gpiv);
    g_signal_connect( pref->preferences,
                      "response",
                      G_CALLBACK(on_preferences_response),
                      NULL);
    gtk_window_set_icon(GTK_WINDOW( pref->preferences), logo);
    if (logo != NULL) g_object_unref (logo);



    pref->notebook = gtk_notebook_new ();
    gtk_widget_ref (pref->notebook);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "notebook",
                              pref->notebook,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->notebook);
    gtk_container_add (GTK_CONTAINER (GTK_DIALOG(pref->preferences)->vbox),
                       pref->notebook);

/*
 * Interface tabulator
 */

    pref->table1 = gtk_table_new (2,
                            3,
                            FALSE);
    gtk_widget_ref (pref->table1);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "table1",
                              pref->table1,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->table1);
    gtk_container_add (GTK_CONTAINER (pref->notebook),
                       pref->table1);
    
    
    
    pref->frame_view = gtk_frame_new (_("View options"));
    gtk_widget_ref (pref->frame_view);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_view", 
                              pref->frame_view,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_view);
    gtk_table_attach (GTK_TABLE (pref->table1),
                      pref->frame_view,
                      1,
                      2,
                      0,
                      1,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL),
                      0,
                      0);
    
    
    
    pref->vbox1 = gtk_vbox_new (FALSE, 0);
    gtk_widget_ref (pref->vbox1);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "vbox1", 
                              pref->vbox1,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->vbox1);
    gtk_container_add (GTK_CONTAINER (pref->frame_view),
                       pref->vbox1);
    
    
    
    pref->checkbutton_gpivbuttons = 
        gtk_check_button_new_with_label (_("gpiv buttons"));
    gtk_widget_ref (pref->checkbutton_gpivbuttons);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_gpivbuttons", 
                              pref->checkbutton_gpivbuttons,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->console__view_gpivbuttons == 1) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_gpivbuttons),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_gpivbuttons),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_gpivbuttons);
    gtk_box_pack_start (GTK_BOX (pref->vbox1),
                        pref->checkbutton_gpivbuttons,
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_gpivbuttons), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_gpivbuttons_activate),
                      NULL);
    
    
    
    pref->checkbutton_tab = gtk_check_button_new_with_label (_("tabulator"));
    gtk_widget_ref (pref->checkbutton_tab);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_tab", 
                              pref->checkbutton_tab,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->console__view_tabulator == 1) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_tab),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_tab),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_tab);
    gtk_box_pack_start (GTK_BOX (pref->vbox1),
                        pref->checkbutton_tab, 
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_tab), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_tab_activate),
                      NULL);
    
    

/* */
    pref->frame_io = gtk_frame_new (_("In/output"));
    gtk_widget_ref (pref->frame_io);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_io", 
                              pref->frame_io,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_io);
    gtk_table_attach (GTK_TABLE (pref->table1),
                      pref->frame_io,
                      2,
                      3,
                      0,
                      1,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL),
                      0,
                      0);



    pref->vbox_io = gtk_vbox_new (FALSE, 0);
    gtk_widget_ref (pref->vbox_io);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "vbox_io", 
                              pref->vbox_io,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->vbox_io);
    gtk_container_add (GTK_CONTAINER (pref->frame_io),
                       pref->vbox_io);


/*
 * Image format
 */
    pref->radiobutton_imgfmt_0 = 
        gtk_radio_button_new_with_label (pref->imgformat_sel_group,
                                         _("PNG image format (.png)"));
    pref->imgformat_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (pref->radiobutton_imgfmt_0));
    gtk_widget_ref (pref->radiobutton_imgfmt_0);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_imgfmt_0", 
                              pref->radiobutton_imgfmt_0,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->radiobutton_imgfmt_0);
    gtk_box_pack_start (GTK_BOX (pref->vbox_io),
                        pref->radiobutton_imgfmt_0, 
                        FALSE, 
                        FALSE, 
                        0);
    
    if (default_par->img_fmt == IMG_FMT_PNG) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_imgfmt_0),
                                    TRUE);
    }
/*     g_snprintf(data, sizeof(IMG_FMT_PNG), "%s", IMG_FMT_PNG); */
    gtk_object_set_data (GTK_OBJECT (pref->radiobutton_imgfmt_0),
			"image_format",
                         (int *) IMG_FMT_PNG);
    g_signal_connect (GTK_OBJECT (pref->radiobutton_imgfmt_0), 
                      "toggled",
                      G_CALLBACK (on_radiobutton_imgfmt),
                      NULL);



    pref->radiobutton_imgfmt_1 = 
        gtk_radio_button_new_with_label (pref->imgformat_sel_group,
                                         _("HDF5 image format (.h5)"));
    pref->imgformat_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (pref->radiobutton_imgfmt_1));
    gtk_widget_ref (pref->radiobutton_imgfmt_1);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_imgfmt_1", 
                              pref->radiobutton_imgfmt_1,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->radiobutton_imgfmt_1);
    gtk_box_pack_start (GTK_BOX (pref->vbox_io),
                        pref->radiobutton_imgfmt_1, 
                        FALSE, 
                        FALSE, 
                        0);
    
    if (default_par->img_fmt == IMG_FMT_HDF) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_imgfmt_1),
                                    TRUE);
    }
/*     g_snprintf(data, sizeof(IMG_FMT_PNG), "%s", IMG_FMT_PNG); */
    gtk_object_set_data (GTK_OBJECT (pref->radiobutton_imgfmt_1),
			"image_format",
                         (int *) IMG_FMT_HDF);
    g_signal_connect (GTK_OBJECT (pref->radiobutton_imgfmt_1), 
                      "toggled",
                      G_CALLBACK (on_radiobutton_imgfmt),
                      NULL);



    pref->radiobutton_imgfmt_2 = 
        gtk_radio_button_new_with_label (pref->imgformat_sel_group,
                                         _("RAW image format (.h .r)"));
    pref->imgformat_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (pref->radiobutton_imgfmt_2));
    gtk_widget_ref (pref->radiobutton_imgfmt_2);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_imgfmt_2", 
                              pref->radiobutton_imgfmt_2,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->radiobutton_imgfmt_2);
    gtk_box_pack_start (GTK_BOX (pref->vbox_io),
                        pref->radiobutton_imgfmt_2, 
                        FALSE, 
                        FALSE, 
                        0);
    
    if (default_par->img_fmt == IMG_FMT_RAW) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_imgfmt_2),
                                    TRUE);
    }
/*     g_snprintf(data, sizeof(IMG_FMT_PNG), "%s", IMG_FMT_PNG); */
    gtk_object_set_data (GTK_OBJECT (pref->radiobutton_imgfmt_2),
			"image_format",
                         (int *) IMG_FMT_RAW);
    g_signal_connect (GTK_OBJECT (pref->radiobutton_imgfmt_2), 
                      "toggled",
                      G_CALLBACK (on_radiobutton_imgfmt),
                      NULL);


/*
 * Image parameters
 */
    pref->checkbutton_xcorr = 
        gtk_check_button_new_with_label (_("cross-correlation"));
    gtk_widget_ref (pref->checkbutton_xcorr);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_xcorr", 
                              pref->checkbutton_xcorr,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->x_corr == 1) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_xcorr),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_xcorr),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_xcorr);
    gtk_box_pack_start (GTK_BOX (pref->vbox_io),
                        pref->checkbutton_xcorr, 
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_xcorr), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_xcorr_activate),
                      NULL);


/*
 * PIV and PIV-derived Data format
 */
    pref->radiobutton_datafmt_0 = 
        gtk_radio_button_new_with_label (pref->dataformat_sel_group,
                                         _("ASCII data format (.piv)"));
    pref->dataformat_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (pref->radiobutton_datafmt_0));
    gtk_widget_ref (pref->radiobutton_datafmt_0);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_datafmt_0", 
                              pref->radiobutton_datafmt_0,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->radiobutton_datafmt_0);
    gtk_box_pack_start (GTK_BOX (pref->vbox_io),
                        pref->radiobutton_datafmt_0, 
                        FALSE, 
                        FALSE, 
                        0);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (pref->radiobutton_datafmt_0),
                                default_par->hdf);
    gtk_object_set_data (GTK_OBJECT (pref->radiobutton_datafmt_0),
                         "data_format",
                         "0" /* (int *) FALSE */);
    g_signal_connect (GTK_OBJECT (pref->radiobutton_datafmt_0), 
                      "toggled",
                      G_CALLBACK (on_radiobutton_datafmt),
                      NULL);



    pref->radiobutton_datafmt_1 = 
        gtk_radio_button_new_with_label (pref->dataformat_sel_group,
                                         _("HDF data format (.h5)"));
    pref->dataformat_sel_group =
	gtk_radio_button_group (GTK_RADIO_BUTTON (pref->radiobutton_datafmt_1));
    gtk_widget_ref (pref->radiobutton_datafmt_1);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_datafmt_1", 
                              pref->radiobutton_datafmt_1,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->radiobutton_datafmt_1);
    gtk_box_pack_start (GTK_BOX (pref->vbox_io),
                        pref->radiobutton_datafmt_1, 
                        FALSE, 
                        FALSE, 
                        0);
    
    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                (pref->radiobutton_datafmt_1),
                                default_par->hdf);
    gtk_object_set_data (GTK_OBJECT (pref->radiobutton_datafmt_1),
			"data_format",
                         "1" /* TRUE */);
    g_signal_connect (GTK_OBJECT (pref->radiobutton_datafmt_1), 
                      "toggled",
                      G_CALLBACK (on_radiobutton_datafmt),
                      NULL);


/*
 * General; tooltips, histogram bins, cluster nodes
 */
    pref->checkbutton_tooltips = gtk_check_button_new_with_label (_("tooltips"));
    gtk_widget_ref (pref->checkbutton_tooltips);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_tooltips", 
                              pref->checkbutton_tooltips,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->console__show_tooltips == 1) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_tooltips),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_tooltips),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_tooltips);
    gtk_table_attach (GTK_TABLE (pref->table1),
                      pref->checkbutton_tooltips,
                      0, 
                      1, 
                      1, 
                      2,
                      (GtkAttachOptions) (GTK_FILL),
                      (GtkAttachOptions) (0),
                      0,
                      0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_tooltips), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_tooltips_activate),
                      NULL);
    



    pref->frame_processes = gtk_frame_new (_("processes"));
    gtk_widget_ref (pref->frame_processes);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_processes", 
                              pref->frame_processes,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_processes);
    gtk_table_attach (GTK_TABLE (pref->table1),
                      pref->frame_processes,
                      0,
                      1,
                      0,
                      1,
                      (GtkAttachOptions) (GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL),
                      0,
                      0);



    pref->vbox2 = gtk_vbox_new (FALSE,
                          0);
    gtk_widget_ref (pref->vbox2);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                            "vbox2",
                              pref->vbox2,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->vbox2);
    gtk_container_add (GTK_CONTAINER (pref->frame_processes),
                       pref->vbox2);
    
    
    

#ifdef ENABLE_CAM
    pref->checkbutton_process_cam = gtk_check_button_new_with_label (_("camera"));
    gtk_widget_ref (pref->checkbutton_process_cam);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_cam", 
                              pref->checkbutton_process_cam,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__cam) {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_process_cam),
                                     TRUE);
    } else {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_process_cam),
                                   FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_cam);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_cam,
                        FALSE,
                        FALSE, 
                        0);
    
    gtk_signal_connect (GTK_OBJECT (pref->checkbutton_process_cam), 
                        "clicked",
                        GTK_SIGNAL_FUNC (on_checkbutton_process_cam_activate),
                        NULL);
#endif /* ENABLE_CAM */    
    
    
    
#ifdef ENABLE_TRIG
    pref->checkbutton_process_trig = gtk_check_button_new_with_label (_("trigger"));
    gtk_widget_ref (pref->checkbutton_process_trig);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_trig", 
                              pref->checkbutton_process_trig,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__trig) {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_process_trig),
                                     TRUE);
    } else {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_process_trig),
                                   FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_trig);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_trig,
                        FALSE,
                        FALSE, 
                        0);
    
    gtk_signal_connect (GTK_OBJECT (pref->checkbutton_process_trig), 
                        "clicked",
                        GTK_SIGNAL_FUNC (on_checkbutton_process_trig_activate),
                        NULL);
#endif /* ENABLE_TRIG */    
    
    
    
#ifdef ENABLE_IMGPROC
    pref->checkbutton_process_imgproc = gtk_check_button_new_with_label (_("image processing"));
    gtk_widget_ref (pref->checkbutton_process_imgproc);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_imgproc", 
                              pref->checkbutton_process_imgproc,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__imgproc) {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_process_imgproc),
                                     TRUE);
    } else {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_process_imgproc),
                                   FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_imgproc);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_imgproc,
                        FALSE,
                        FALSE, 
                        0);
    
    gtk_signal_connect (GTK_OBJECT (pref->checkbutton_process_imgproc), 
                        "clicked",
                        GTK_SIGNAL_FUNC (on_checkbutton_process_imgproc_activate),
                        NULL);
#endif /* ENABLE_IMGPROC */    
    
    
    
    pref->checkbutton_process_piv = gtk_check_button_new_with_label (_("piv"));
    gtk_widget_ref (pref->checkbutton_process_piv);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_piv", 
                              pref->checkbutton_process_piv,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__piv) {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_process_piv),
                                     TRUE);
    } else {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_process_piv),
                                   FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_piv);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_piv,
                        FALSE,
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_piv), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_piv_activate),
                      NULL);
    
    
    
    
    pref->checkbutton_process_gradient = 
        gtk_check_button_new_with_label (_("gradient"));
    gtk_widget_ref (pref->checkbutton_process_gradient);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                            "checkbutton_process_gradient", 
                              pref->checkbutton_process_gradient,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__gradient) {
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                  (pref->checkbutton_process_gradient),
                                  TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                  (pref->checkbutton_process_gradient),
                                    FALSE);
  }
    gtk_widget_show (pref->checkbutton_process_gradient);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_gradient,
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_gradient), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_gradient_activate),
                      NULL);
    
    
    
    
    pref->checkbutton_process_resstats = 
        gtk_check_button_new_with_label (_("residu stats"));
    gtk_widget_ref (pref->checkbutton_process_resstats);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_resstats", 
                              pref->checkbutton_process_resstats,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__resstats) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_resstats),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_resstats),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_resstats);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_resstats,
                        FALSE,
                        FALSE,
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_resstats), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_resstats_activate),
                      NULL);
    
    
    
    
    pref->checkbutton_process_errvec = 
        gtk_check_button_new_with_label (_("validate"));
    gtk_widget_ref (pref->checkbutton_process_errvec);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_errvec", 
                              pref->checkbutton_process_errvec,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__errvec) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_errvec),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_errvec),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_errvec);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_errvec,
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_errvec), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_errvec_activate),
                      NULL);
    
    

    
    pref->checkbutton_process_peaklck = 
        gtk_check_button_new_with_label (_("peaklock"));
    gtk_widget_ref (pref->checkbutton_process_peaklck);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_peaklck", 
                              pref->checkbutton_process_peaklck,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__peaklock) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_peaklck),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_peaklck),
                                  FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_peaklck);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_peaklck,
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_peaklck), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_peaklck_activate),
                      NULL);



    pref->checkbutton_process_scale = 
        gtk_check_button_new_with_label (_("scaling"));
    gtk_widget_ref (pref->checkbutton_process_scale);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_scale", 
                              pref->checkbutton_process_scale,
                            (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__scale) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_scale),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_scale),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_scale);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_scale, 
                        FALSE,
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_scale), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_scale_activate),
                      NULL);
    



    pref->checkbutton_process_avg = 
        gtk_check_button_new_with_label (_("average"));
    gtk_widget_ref (pref->checkbutton_process_avg);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_avg", 
                              pref->checkbutton_process_avg,
                            (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__average) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_avg),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_avg),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_avg);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_avg, 
                        FALSE,
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_avg),
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_avg_activate),
                      NULL);
    



    pref->checkbutton_process_subtract = 
        gtk_check_button_new_with_label (_("subtract"));
    gtk_widget_ref (pref->checkbutton_process_subtract);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_subtract", 
                              pref->checkbutton_process_subtract,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__subtract) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_subtract),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_subtract),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_process_subtract);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_subtract,
                        FALSE, 
                        FALSE,
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_subtract), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_subtract_activate),
                      NULL);
    



    pref->checkbutton_process_vorstra = 
        gtk_check_button_new_with_label (_("vorticity"));
    gtk_widget_ref (pref->checkbutton_process_vorstra);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_process_vorstra", 
                              pref->checkbutton_process_vorstra,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->process__vorstra) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_process_vorstra),
                                    TRUE);
  } else {
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                  (pref->checkbutton_process_vorstra),
                                  FALSE);
  }
    gtk_widget_show (pref->checkbutton_process_vorstra);
    gtk_box_pack_start (GTK_BOX (pref->vbox2),
                        pref->checkbutton_process_vorstra,
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_process_vorstra), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_process_vorstra_activate),
                      NULL);
    
    
    

    pref->hbox1 = gtk_hbox_new (FALSE,
                          0);
    gtk_widget_ref (pref->hbox1);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "hbox1",
                              pref->hbox1,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->hbox1);
    gtk_table_attach (GTK_TABLE (pref->table1),
                      pref->hbox1,
                      1,
                      3,
                      1,
                      2,
                      (GtkAttachOptions) (GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL),
                      0,
                      0);
    


    pref->label_spinner_bins = gtk_label_new (_("number of histogram bins: "));
    gtk_widget_ref (pref->label_spinner_bins);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "label_spinner_bins",
                              pref->label_spinner_bins,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->label_spinner_bins);
    gtk_box_pack_start (GTK_BOX (pref->hbox1),
                        pref->label_spinner_bins,
                        FALSE,
                        FALSE, 
                        0);



    pref->spinbutton_bins_adj = gtk_adjustment_new (10,
                                                   0,
                                                   100,
                                                   1,
                                                   10,
                                                   10);
    pref->spinbutton_bins = 
        gtk_spin_button_new (GTK_ADJUSTMENT (pref->spinbutton_bins_adj),
                             1,
                             0);
    gtk_widget_ref (pref->spinbutton_bins);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "spinbutton_bins",
                            pref->spinbutton_bins,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->spinbutton_bins);
    gtk_box_pack_start (GTK_BOX (pref->hbox1),
                        pref->spinbutton_bins,
                        TRUE,
                        TRUE,
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->spinbutton_bins), 
                      "changed",
                      G_CALLBACK (on_spinbutton_bins_activate),
                      NULL);
    


#ifdef ENABLE_MPI
    pref->hbox2 = gtk_hbox_new (FALSE,
                          0);
    gtk_widget_ref (pref->hbox2);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "hbox2",
                              pref->hbox2,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->hbox2);
    gtk_table_attach (GTK_TABLE (pref->table1),
                      pref->hbox2,
                      1,
                      3,
                      2,
                      3,
                      (GtkAttachOptions) (GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL),
                      0,
                      0);

    pref->label_spinner_nodes = gtk_label_new (_("number of cluster nodes: "));
    gtk_widget_ref (pref->label_spinner_nodes);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "label_spinner_nodes",
                              pref->label_spinner_nodes,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->label_spinner_nodes);
    gtk_box_pack_start (GTK_BOX (pref->hbox2),
                        pref->label_spinner_nodes,
                        FALSE,
                        FALSE, 
                        0);



    pref->spinbutton_nodes_adj = gtk_adjustment_new (4,
                                                     0,
                                                     100,
                                                     1,
                                                     10,
                                                     10);
    pref->spinbutton_nodes = 
        gtk_spin_button_new (GTK_ADJUSTMENT (pref->spinbutton_nodes_adj),
                             1,
                             0);
    gtk_widget_ref (pref->spinbutton_nodes);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "spinbutton_nodes",
                            pref->spinbutton_nodes,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->spinbutton_nodes);
    gtk_box_pack_start (GTK_BOX (pref->hbox2),
                        pref->spinbutton_nodes,
                        TRUE,
                        TRUE,
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->spinbutton_nodes), 
                      "changed",
                      G_CALLBACK (on_spinbutton_nodes),
                      NULL);
#endif /* ENABLE_MPI



    pref->tab1 = gtk_label_new (_("console"));
    gtk_widget_ref (pref->tab1);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "pref_tab1",
                              pref->tab1,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->tab1);
    gtk_notebook_set_tab_label (GTK_NOTEBOOK (pref->notebook),
                                gtk_notebook_get_nth_page 
                                (GTK_NOTEBOOK (pref->notebook),
                                 0),
                                pref->tab1);



/*
 * Display tabulator
 */

    pref->table2 = gtk_table_new (3,
                            3,
                            FALSE);
    gtk_widget_ref (pref->table2);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences),
                              "table2",
                              pref->table2,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->table2);
    gtk_container_add (GTK_CONTAINER (pref->notebook),
                       pref->table2);

/*
 * Vector scale
 */

    pref->frame_display_vecscale = gtk_frame_new (_("vector scale"));
    gtk_widget_ref (pref->frame_display_vecscale);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_display_vecscale", 
                              pref->frame_display_vecscale,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_display_vecscale);
    gtk_table_attach (GTK_TABLE (pref->table2),
                      pref->frame_display_vecscale,
                      0,
                      1,
                      0,
                      2,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      0,
                      0);
    

    pref->vbox_display_vecscale = gtk_vbox_new(FALSE,
                                              0);
    gtk_widget_ref(pref->vbox_display_vecscale);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console), 
                             "vbox_display_vecscale",
                             pref->vbox_display_vecscale,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(pref->vbox_display_vecscale);
    gtk_container_add(GTK_CONTAINER(pref->frame_display_vecscale), 
                      pref->vbox_display_vecscale);
    
    

    pref->radiobutton_display_vecscale1 =
        gtk_radio_button_new_with_label(pref->vecscale_group,
                                        _("1"));
    pref->vecscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_vecscale1));
    gtk_widget_ref(pref->radiobutton_display_vecscale1);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_vecscale1",
                             pref->radiobutton_display_vecscale1,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_scale == 1) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale1),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale1),
                                    FALSE);
        
    }
    gtk_widget_show(pref->radiobutton_display_vecscale1);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_vecscale),
                       pref->radiobutton_display_vecscale1,
                       FALSE,
                       FALSE,
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_vecscale1),
                        "vscale",
                        "1");
    



    pref->radiobutton_display_vecscale2 =
        gtk_radio_button_new_with_label(pref->vecscale_group, _("2"));
    pref->vecscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_vecscale2));
    gtk_widget_ref(pref->radiobutton_display_vecscale2);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_vecscale2",
                             pref->radiobutton_display_vecscale2,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_scale == 2) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale2), 
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale2), 
                                    FALSE);
    }
    gtk_widget_show(pref->radiobutton_display_vecscale2);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_vecscale),
                       pref->radiobutton_display_vecscale2, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_vecscale2), 
                        "vscale",
                        "2");
    
    

    
    pref->radiobutton_display_vecscale3 =
        gtk_radio_button_new_with_label(pref->vecscale_group, _("4"));
    pref->vecscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_vecscale3));
    gtk_widget_ref(pref->radiobutton_display_vecscale3);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_vecscale3",
                             pref->radiobutton_display_vecscale3,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_scale == 4) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale3), 
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale3), 
                                    FALSE);
    }
    gtk_widget_show(pref->radiobutton_display_vecscale3);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_vecscale),
                       pref->radiobutton_display_vecscale3, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_vecscale3), 
                        "vscale",
                        "4");




    pref->radiobutton_display_vecscale4 =
        gtk_radio_button_new_with_label(pref->vecscale_group, _("8"));
    pref->vecscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_vecscale4));
    gtk_widget_ref(pref->radiobutton_display_vecscale4);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_vecscale4",
                             pref->radiobutton_display_vecscale4,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_scale == 8) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale4), 
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale4), 
                                    FALSE);
    }
    gtk_widget_show(pref->radiobutton_display_vecscale4);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_vecscale),
                       pref->radiobutton_display_vecscale4, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_vecscale4), 
                        "vscale",
                        "8");




    pref->radiobutton_display_vecscale5 =
        gtk_radio_button_new_with_label(pref->vecscale_group, _("16"));
    pref->vecscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_vecscale5));
    gtk_widget_ref(pref->radiobutton_display_vecscale5);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_vecscale5",
                             pref->radiobutton_display_vecscale5,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_scale == 16) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale5), 
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale5), 
                                    FALSE);
    }
    gtk_widget_show(pref->radiobutton_display_vecscale5);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_vecscale),
                       pref->radiobutton_display_vecscale5, FALSE, FALSE, 0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_vecscale5), 
                        "vscale",
                        "16");




    pref->radiobutton_display_vecscale6 =
        gtk_radio_button_new_with_label(pref->vecscale_group, _("32"));
    pref->vecscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_vecscale6));
    gtk_widget_ref(pref->radiobutton_display_vecscale6);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_vecscale6",
                             pref->radiobutton_display_vecscale6,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_scale == 32) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale6), 
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale6), 
                                    FALSE);
    }
    gtk_widget_show(pref->radiobutton_display_vecscale6);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_vecscale),
                       pref->radiobutton_display_vecscale6, FALSE, FALSE, 0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_vecscale6), 
                        "vscale",
                        "32");




    pref->radiobutton_display_vecscale7 =
        gtk_radio_button_new_with_label(pref->vecscale_group, _("64"));
    pref->vecscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_vecscale7));
    gtk_widget_ref(pref->radiobutton_display_vecscale7);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_vecscale7",
                             pref->radiobutton_display_vecscale7,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_scale == 64) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale7), 
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_vecscale7), 
                                    FALSE);
    }
    gtk_widget_show(pref->radiobutton_display_vecscale7);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_vecscale),
                       pref->radiobutton_display_vecscale7, FALSE, FALSE, 0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_vecscale7), 
                        "vscale",
                        "64");

/*
 * Vector color
 */

    pref->frame_display_veccolor = gtk_frame_new (_("vector color"));
    gtk_widget_ref (pref->frame_display_veccolor);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_display_veccolor", 
                              pref->frame_display_veccolor,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_display_veccolor);
    gtk_table_attach (GTK_TABLE (pref->table2),
                      pref->frame_display_veccolor,
                      0,
                      1,
                      2,
                      3,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      0,
                      0);
    

    pref->vbox_display_veccolor = gtk_vbox_new(FALSE,
                                              0);
    gtk_widget_ref(pref->vbox_display_veccolor);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console), 
                             "vbox_display_veccolor",
                             pref->vbox_display_veccolor,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(pref->vbox_display_veccolor);
    gtk_container_add(GTK_CONTAINER(pref->frame_display_veccolor), 
                      pref->vbox_display_veccolor);
    
    

    pref->radiobutton_display_veccolor1 =
        gtk_radio_button_new_with_label(pref->veccolor_group,
                                        _("Peak nr"));
    pref->veccolor_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_veccolor1));
    gtk_widget_ref(pref->radiobutton_display_veccolor1);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_veccolor1",
                             pref->radiobutton_display_veccolor1,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_color == SHOW_PEAKNR) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_veccolor1),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_veccolor1),
                                    FALSE);
        
    }
    gtk_widget_show(pref->radiobutton_display_veccolor1);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_veccolor),
                       pref->radiobutton_display_veccolor1,
                       FALSE,
                       FALSE,
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_veccolor1),
                        "vcolor",
                        (int *) SHOW_PEAKNR);
    



    pref->radiobutton_display_veccolor2 =
        gtk_radio_button_new_with_label(pref->veccolor_group,
                                        _("SNR"));
    pref->veccolor_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_veccolor2));
    gtk_widget_ref(pref->radiobutton_display_veccolor2);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_veccolor2",
                             pref->radiobutton_display_veccolor2,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_color == SHOW_SNR) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_veccolor2),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_veccolor2),
                                    FALSE);
        
    }
    gtk_widget_show(pref->radiobutton_display_veccolor2);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_veccolor),
                       pref->radiobutton_display_veccolor2,
                       FALSE,
                       FALSE,
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_veccolor2),
                        "vcolor",
                        (int *) SHOW_SNR);
    



    pref->radiobutton_display_veccolor3 =
        gtk_radio_button_new_with_label(pref->veccolor_group,
                                        _("Magnitude gray"));
    pref->veccolor_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_veccolor3));
    gtk_widget_ref(pref->radiobutton_display_veccolor3);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_veccolor3",
                             pref->radiobutton_display_veccolor3,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_color == SHOW_MAGNITUDE_GRAY) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_veccolor3),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_veccolor3),
                                    FALSE);
        
    }
    gtk_widget_show(pref->radiobutton_display_veccolor3);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_veccolor),
                       pref->radiobutton_display_veccolor3,
                       FALSE,
                       FALSE,
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_veccolor3),
                        "vcolor",
                        (int *) SHOW_MAGNITUDE_GRAY);
    



    pref->radiobutton_display_veccolor4 =
        gtk_radio_button_new_with_label(pref->veccolor_group,
                                        _("Magnitude color"));
    pref->veccolor_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_veccolor4));
    gtk_widget_ref(pref->radiobutton_display_veccolor4);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_veccolor4",
                             pref->radiobutton_display_veccolor4,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__vector_color == SHOW_MAGNITUDE) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_veccolor4),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_veccolor4),
                                    FALSE);
        
    }
    gtk_widget_show(pref->radiobutton_display_veccolor4);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_veccolor),
                       pref->radiobutton_display_veccolor4,
                       FALSE,
                       FALSE,
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_veccolor4),
                        "vcolor",
                        (int *) SHOW_MAGNITUDE);
    



/*
 * Zoom scale
 */
    pref->frame_display_zoomscale = gtk_frame_new (_("zoom scale"));
    gtk_widget_ref (pref->frame_display_zoomscale);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_display_zoomscale", 
                              pref->frame_display_zoomscale,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_display_zoomscale);
    gtk_table_attach (GTK_TABLE (pref->table2), pref->frame_display_zoomscale, 
                      1, 
                      2, 
                      0, 
                      2,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 
                      0, 
                      0);
    
    
    pref->vbox_display_zoomscale = gtk_vbox_new(FALSE, 0);
    gtk_widget_ref(pref->vbox_display_zoomscale);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console), 
                             "vbox_display_zoomscale",
                             pref->vbox_display_zoomscale,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(pref->vbox_display_zoomscale);
    gtk_container_add(GTK_CONTAINER(pref->frame_display_zoomscale), 
                      pref->vbox_display_zoomscale);
    



    pref->radiobutton_display_zoomscale0 =
        gtk_radio_button_new_with_label(pref->zoomscale_group, _("0.25"));
    pref->zoomscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_zoomscale0));
    gtk_widget_ref(pref->radiobutton_display_zoomscale0);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_zoomscale0",
                             pref->radiobutton_display_zoomscale0,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__zoom_index == ZOOM_0) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_zoomscale0), 
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_zoomscale0);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_zoomscale),
                       pref->radiobutton_display_zoomscale0, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_zoomscale0), 
                        "zscale", 
                        (int *) ZOOM_0);




    pref->radiobutton_display_zoomscale1 =
        gtk_radio_button_new_with_label(pref->zoomscale_group, _("0.5"));
    pref->zoomscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_zoomscale1));
    gtk_widget_ref(pref->radiobutton_display_zoomscale1);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_zoomscale1",
                             pref->radiobutton_display_zoomscale1,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__zoom_index == ZOOM_1) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_zoomscale1), 
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_zoomscale1);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_zoomscale),
                       pref->radiobutton_display_zoomscale1, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_zoomscale1), 
                        "zscale",
                        (int *) ZOOM_1);




    pref->radiobutton_display_zoomscale2 =
        gtk_radio_button_new_with_label(pref->zoomscale_group, _("0.83"));
    pref->zoomscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_zoomscale2));
    gtk_widget_ref(pref->radiobutton_display_zoomscale2);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_zoomscale2",
                             pref->radiobutton_display_zoomscale2,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__zoom_index == ZOOM_2) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_zoomscale2), 
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_zoomscale2);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_zoomscale),
                       pref->radiobutton_display_zoomscale2, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_zoomscale2), 
                        "zscale",
                        (int *) ZOOM_2);




    pref->radiobutton_display_zoomscale3 =
        gtk_radio_button_new_with_label(pref->zoomscale_group, _("1.0"));
    pref->zoomscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_zoomscale3));
    gtk_widget_ref(pref->radiobutton_display_zoomscale3);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_zoomscale3",
                             pref->radiobutton_display_zoomscale3,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__zoom_index == ZOOM_3) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_zoomscale3), 
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_zoomscale3);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_zoomscale),
                       pref->radiobutton_display_zoomscale3, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_zoomscale3), 
                        "zscale",
                        (int *) ZOOM_3);




    pref->radiobutton_display_zoomscale4 =
        gtk_radio_button_new_with_label(pref->zoomscale_group, _("1.3"));
    pref->zoomscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_zoomscale4));
    gtk_widget_ref(pref->radiobutton_display_zoomscale4);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_zoomscale4",
                             pref->radiobutton_display_zoomscale4,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__zoom_index == ZOOM_4) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_zoomscale4), 
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_zoomscale4);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_zoomscale),
                       pref->radiobutton_display_zoomscale4, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_zoomscale4), 
                        "zscale",
                        (int *) ZOOM_4);




    pref->radiobutton_display_zoomscale5 =
        gtk_radio_button_new_with_label(pref->zoomscale_group, _("1.6"));
    pref->zoomscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_zoomscale5));
    gtk_widget_ref(pref->radiobutton_display_zoomscale5);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_zoomscale5",
                             pref->radiobutton_display_zoomscale5,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__zoom_index == ZOOM_5) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_zoomscale5), 
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_zoomscale5);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_zoomscale),
                       pref->radiobutton_display_zoomscale5, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_zoomscale5), 
                        "zscale",
                        (int *) ZOOM_5);




    pref->radiobutton_display_zoomscale6 =
        gtk_radio_button_new_with_label(pref->zoomscale_group, _("2.0"));
    pref->zoomscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_zoomscale6));
    gtk_widget_ref(pref->radiobutton_display_zoomscale6);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_zoomscale6",
                             pref->radiobutton_display_zoomscale6,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__zoom_index == ZOOM_6) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_zoomscale6), 
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_zoomscale6);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_zoomscale),
                       pref->radiobutton_display_zoomscale6, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_zoomscale6), 
                        "zscale",
                        (int *) ZOOM_6);


    pref->radiobutton_display_zoomscale7 =
        gtk_radio_button_new_with_label(pref->zoomscale_group, _("4.0"));
    pref->zoomscale_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_zoomscale7));
    gtk_widget_ref(pref->radiobutton_display_zoomscale7);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_zoomscale7",
                             pref->radiobutton_display_zoomscale7,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__zoom_index == ZOOM_7) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_zoomscale7), 
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_zoomscale7);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_zoomscale),
                       pref->radiobutton_display_zoomscale7, 
                       FALSE, 
                       FALSE, 
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_zoomscale7), 
                        "zscale",
                        (int *) ZOOM_7);


/*
 * Background
 */

    pref->frame_display_backgroundcolor = gtk_frame_new (_("background"));
    gtk_widget_ref (pref->frame_display_backgroundcolor);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_display_backgroundcolor", 
                              pref->frame_display_backgroundcolor,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_display_backgroundcolor);
    gtk_table_attach (GTK_TABLE (pref->table2),
                      pref->frame_display_backgroundcolor,
                      1,
                      2,
                      2,
                      3,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      0,
                      0);
    

    pref->vbox_display_backgroundcolor = gtk_vbox_new(FALSE,
                                              0);
    gtk_widget_ref(pref->vbox_display_backgroundcolor);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console), 
                             "vbox_display_backgroundcolor",
                             pref->vbox_display_backgroundcolor,
                             (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show(pref->vbox_display_backgroundcolor);
    gtk_container_add(GTK_CONTAINER(pref->frame_display_backgroundcolor), 
                      pref->vbox_display_backgroundcolor);
    
    

    pref->radiobutton_display_backgroundcolor1 =
        gtk_radio_button_new_with_label(pref->backgroundcolor_group,
                                        _("Blue"));
    pref->backgroundcolor_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_backgroundcolor1));
    gtk_widget_ref(pref->radiobutton_display_backgroundcolor1);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_backgroundcolor1",
                             pref->radiobutton_display_backgroundcolor1,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__backgrnd == SHOW_BG_DARKBLUE) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_backgroundcolor1),
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_backgroundcolor1);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_backgroundcolor),
                       pref->radiobutton_display_backgroundcolor1,
                       FALSE,
                       FALSE,
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_backgroundcolor1),
                        "bgcolor",
                        (int *) SHOW_BG_DARKBLUE);



    pref->radiobutton_display_backgroundcolor2 =
        gtk_radio_button_new_with_label(pref->backgroundcolor_group,
                                        _("Black"));
    pref->backgroundcolor_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_backgroundcolor2));
    gtk_widget_ref(pref->radiobutton_display_backgroundcolor2);
    gtk_object_set_data_full(GTK_OBJECT(gpiv->console),
                             "radiobutton_display_backgroundcolor2",
                             pref->radiobutton_display_backgroundcolor2,
                             (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__backgrnd == SHOW_BG_BLACK) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_backgroundcolor2),
                                    TRUE);
    }
    gtk_widget_show(pref->radiobutton_display_backgroundcolor2);
    gtk_box_pack_start(GTK_BOX(pref->vbox_display_backgroundcolor),
                       pref->radiobutton_display_backgroundcolor2,
                       FALSE,
                       FALSE,
                       0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_backgroundcolor2),
                        "bgcolor",
                        (int *) SHOW_BG_BLACK);



    pref->radiobutton_display_background_img1 = 
        gtk_radio_button_new_with_label(pref->backgroundcolor_group,
                                        _("image A"));
    pref->backgroundcolor_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_background_img1));
    gtk_widget_ref (pref->radiobutton_display_background_img1);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_display_background_img1", 
                              pref->radiobutton_display_background_img1,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__backgrnd == SHOW_BG_IMG1) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_background_img1), 
                                    TRUE);
    }
    gtk_widget_show (pref->radiobutton_display_background_img1);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_backgroundcolor), 
                        pref->radiobutton_display_background_img1, 
                        FALSE, 
                        FALSE, 
                        0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_background_img1),
                        "bgcolor",
                        (int *) SHOW_BG_IMG1);



    pref->radiobutton_display_background_img2 = 
        gtk_radio_button_new_with_label(pref->backgroundcolor_group,
                                        _("image B"));
    pref->backgroundcolor_group =
        gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_background_img2));
    gtk_widget_ref (pref->radiobutton_display_background_img2);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_display_background_img2", 
                              pref->radiobutton_display_background_img2,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__backgrnd == SHOW_BG_IMG2) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_background_img2), 
                                    TRUE);
    }
    gtk_widget_show (pref->radiobutton_display_background_img2);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_backgroundcolor), 
                        pref->radiobutton_display_background_img2, 
                        FALSE, 
                        FALSE, 
                        0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_background_img2),
                        "bgcolor",
                        (int *) SHOW_BG_IMG2);



/*
 * Hide and display PIV data
 */

    pref->frame_display_display = gtk_frame_new (_("hide/display PIV data"));
    gtk_widget_ref (pref->frame_display_display);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_display_display", 
                              pref->frame_display_display,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_display_display);
    gtk_table_attach (GTK_TABLE (pref->table2), 
                      pref->frame_display_display, 
                      2, 
                      3, 
                      0, 
                      1,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL), 
                      0, 
                      0);

    pref->vbox_display_display = gtk_vbox_new (FALSE, 0);
    gtk_widget_ref (pref->vbox_display_display);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "vbox_display_display", 
                              pref->vbox_display_display,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->vbox_display_display);
    gtk_container_add (GTK_CONTAINER (pref->frame_display_display), 
                       pref->vbox_display_display);



    pref->checkbutton_display_display_intregs = 
        gtk_check_button_new_with_label (_("Interrogation area's"));
    gtk_widget_ref (pref->checkbutton_display_display_intregs);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_display_display_intregs", 
                              pref->checkbutton_display_display_intregs,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__intregs == 1) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_display_display_intregs),
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_display_display_intregs),
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_display_display_intregs);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_display), 
                        pref->checkbutton_display_display_intregs, 
                        FALSE, 
                        FALSE, 
                        0);



    pref->checkbutton_display_display_piv = 
        gtk_check_button_new_with_label (_("Velocity vectors"));
    gtk_widget_ref (pref->checkbutton_display_display_piv);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_display_display_piv", 
                              pref->checkbutton_display_display_piv,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__piv) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_display_display_piv), 
                                    TRUE);
    } else {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->checkbutton_display_display_piv), 
                                    FALSE);
    }
    gtk_widget_show (pref->checkbutton_display_display_piv);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_display), 
                        pref->checkbutton_display_display_piv, 
                        FALSE, 
                        FALSE, 
                        0);



/*
 * Hide and display PIV-derived data
 */

    pref->frame_display_pivderived_display = gtk_frame_new (_("PIV-derived data"));
    gtk_widget_ref (pref->frame_display_pivderived_display);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_display_pivderived_display", 
                              pref->frame_display_pivderived_display,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_display_pivderived_display);
    gtk_table_attach (GTK_TABLE (pref->table2), 
                      pref->frame_display_pivderived_display, 
                      2, 
                      3, 
                      1, 
                      2,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL), 
                      0, 
                      0);

/*     gtk_box_pack_start (GTK_BOX (pref->vbox_display_display),  */
/*                         pref->frame_display_pivderived_display,  */
/*                         FALSE,  */
/*                         FALSE,  */
/*                         0); */



    pref->vbox_display_pivderived_display = gtk_vbox_new (FALSE, 0);
    gtk_widget_ref (pref->vbox_display_pivderived_display);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "vbox_display_pivderived_display", 
                              pref->vbox_display_pivderived_display,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->vbox_display_pivderived_display);
    gtk_container_add (GTK_CONTAINER (pref->frame_display_pivderived_display), 
                       pref->vbox_display_pivderived_display);



    pref->radiobutton_display_display_none = 
        gtk_radio_button_new_with_label (pref->scalardata_group,
                                         _("None"));
    pref->scalardata_group = gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_display_none));
    gtk_widget_ref (pref->radiobutton_display_display_none);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_display_display_none", 
                              pref->radiobutton_display_display_none,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__scalar == SHOW_SC_NONE) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_display_none), 
                                    TRUE);
    }
    gtk_widget_show (pref->radiobutton_display_display_none);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_pivderived_display), 
                        pref->radiobutton_display_display_none, 
                        FALSE, 
                        FALSE, 
                        0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_display_none),
                        "scalar",
                        (int *) SHOW_SC_NONE);



    pref->radiobutton_display_display_vor = 
        gtk_radio_button_new_with_label (pref->scalardata_group,
                                         _("Vorticity"));
    pref->scalardata_group = gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_display_vor));
    gtk_widget_ref (pref->radiobutton_display_display_vor);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_display_display_vor", 
                              pref->radiobutton_display_display_vor,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__scalar == SHOW_SC_VORTICITY) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_display_vor), 
                                    TRUE);
    }
    gtk_widget_show (pref->radiobutton_display_display_vor);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_pivderived_display), 
                        pref->radiobutton_display_display_vor, 
                        FALSE, 
                        FALSE, 
                        0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_display_vor),
                        "scalar",
                        (int *) SHOW_SC_VORTICITY);



    pref->radiobutton_display_display_sstrain = 
        gtk_radio_button_new_with_label (pref->scalardata_group,
                                         _("Shear strain"));
    pref->scalardata_group = gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_display_sstrain));
    gtk_widget_ref (pref->radiobutton_display_display_sstrain);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_display_display_sstrain", 
                              pref->radiobutton_display_display_sstrain,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__scalar == SHOW_SC_SSTRAIN) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_display_sstrain), 
                                    TRUE);
    }
    gtk_widget_show (pref->radiobutton_display_display_sstrain);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_pivderived_display), 
                        pref->radiobutton_display_display_sstrain, 
                        FALSE, 
                        FALSE, 
                        0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_display_sstrain),
                        "scalar",
                        (int *) SHOW_SC_SSTRAIN);
    
    

    pref->radiobutton_display_display_nstrain = 
        gtk_radio_button_new_with_label (pref->scalardata_group,
                                         _("Normal strain"));
    pref->scalardata_group = gtk_radio_button_group(GTK_RADIO_BUTTON
                               (pref->radiobutton_display_display_nstrain));
    gtk_widget_ref (pref->radiobutton_display_display_nstrain);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "radiobutton_display_display_nstrain", 
                              pref->radiobutton_display_display_nstrain,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__scalar == SHOW_SC_NSTRAIN) {
        gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
                                    (pref->radiobutton_display_display_nstrain), 
                                    TRUE);
    }
    gtk_widget_show (pref->radiobutton_display_display_nstrain);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_pivderived_display), 
                        pref->radiobutton_display_display_nstrain, 
                        FALSE, 
                        FALSE, 
                        0);
    gtk_object_set_data(GTK_OBJECT(pref->radiobutton_display_display_nstrain),
                        "scalar",
                        (int *) SHOW_SC_NSTRAIN);




/*
 * View menubar and rulers in the display
 */

    pref->frame_display_view = gtk_frame_new (_("View options"));
    gtk_widget_ref (pref->frame_display_view);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "frame_display_view", 
                              pref->frame_display_view,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->frame_display_view);
    gtk_table_attach (GTK_TABLE (pref->table2), 
                      pref->frame_display_view, 
                      2, 
                      3, 
                      2, 
                      3,
                      (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                      (GtkAttachOptions) (GTK_FILL), 
                      0, 
                      0);



    pref->vbox_display_view = gtk_vbox_new (FALSE, 0);
    gtk_widget_ref (pref->vbox_display_view);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "vbox_display_view", 
                              pref->vbox_display_view,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->vbox_display_view);
    gtk_container_add (GTK_CONTAINER (pref->frame_display_view), 
                       pref->vbox_display_view);





    pref->checkbutton_display_view_menubar = 
        gtk_check_button_new_with_label (_("menubar"));
    gtk_widget_ref (pref->checkbutton_display_view_menubar);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_display_view_menubar", 
                              pref->checkbutton_display_view_menubar,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__view_menubar == 1) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_display_view_menubar),
                                     TRUE);
    } else {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_display_view_menubar),
                                     FALSE);
    }
    gtk_widget_show (pref->checkbutton_display_view_menubar);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_view),
                        pref->checkbutton_display_view_menubar,
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_display_view_menubar), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_display_view_menubar_activate),
                      NULL);
    
    
    
    pref->checkbutton_display_view_rulers = 
        gtk_check_button_new_with_label (_("rulers"));
    gtk_widget_ref (pref->checkbutton_display_view_rulers);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_display_view_rulers", 
                              pref->checkbutton_display_view_rulers,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__view_rulers == 1) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_display_view_rulers),
                                     TRUE);
    } else {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_display_view_rulers),
                                     FALSE);
    }
    gtk_widget_show (pref->checkbutton_display_view_rulers);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_view),
                        pref->checkbutton_display_view_rulers,
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_display_view_rulers), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_display_view_rulers_activate),
                      NULL);
    
    
    
    pref->checkbutton_display_stretch_auto = 
        gtk_check_button_new_with_label (_("stretch auto"));
    gtk_widget_ref (pref->checkbutton_display_stretch_auto);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "checkbutton_display_stretch_auto", 
                              pref->checkbutton_display_stretch_auto,
                              (GtkDestroyNotify) gtk_widget_unref);
    if (default_par->display__stretch_auto == 1) {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_display_stretch_auto),
                                     TRUE);
    } else {
        gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON
                                     (pref->checkbutton_display_stretch_auto),
                                     FALSE);
    }
    gtk_widget_show (pref->checkbutton_display_stretch_auto);
    gtk_box_pack_start (GTK_BOX (pref->vbox_display_view),
                        pref->checkbutton_display_stretch_auto,
                        FALSE, 
                        FALSE, 
                        0);
    
    g_signal_connect (GTK_OBJECT (pref->checkbutton_display_stretch_auto), 
                      "clicked",
                      G_CALLBACK (on_checkbutton_display_stretch_auto_activate),
                      NULL);
    
    
    
/*
 * Tabulator for display
 */
    pref->tab2 = gtk_label_new (_("display"));
    gtk_widget_ref (pref->tab2);
    gtk_object_set_data_full (GTK_OBJECT (pref->preferences), 
                              "tab2", 
                              pref->tab2,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (pref->tab2);
    gtk_notebook_set_tab_label (GTK_NOTEBOOK (pref->notebook), 
                                gtk_notebook_get_nth_page 
                                (GTK_NOTEBOOK (pref->notebook), 1), 
                                pref->tab2);



/*
 * Font selection tabulator
 */

/*   fontselection = gtk_font_selection_new (); */
/*   gtk_widget_ref (fontselection); */
/*   gtk_object_set_data_full (GTK_OBJECT (pref->preferences), "fontselection", fontselection, */
/*                             (GtkDestroyNotify) gtk_widget_unref); */
/*   gtk_widget_show (fontselection); */
/*   gtk_container_add (GTK_CONTAINER (pref->notebook), fontselection); */

/*   pref_tab3 = gtk_label_new (_("fonts")); */
/*   gtk_widget_ref (pref_tab3); */
/*   gtk_object_set_data_full (GTK_OBJECT (pref->preferences), "pref_tab3", pref_tab3, */
/*                             (GtkDestroyNotify) gtk_widget_unref); */
/*   gtk_widget_show (pref_tab3); */
/*   gtk_notebook_set_tab_label (GTK_NOTEBOOK (pref->notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), 2), pref_tab3); */


/*
 * Dialog area with "OK", "Apply and "Cancel" buttons
 */

/*     pref->dialog_action_area1 = GTK_DIALOG (pref->preferences)->action_area; */
/*     gtk_object_set_data (GTK_OBJECT (pref->preferences),  */
/*                          "dialog_action_area1",  */
/*                          pref->dialog_action_area1); */
/*     gtk_widget_show (pref->dialog_action_area1); */
/*     gtk_button_box_set_layout (GTK_BUTTON_BOX (pref->dialog_action_area1),  */
/*                                GTK_BUTTONBOX_END); */
/*     gtk_button_box_set_spacing (GTK_BUTTON_BOX (pref->dialog_action_area1),  */
/*                                 8); */



/*     button_ok =  */
/*         (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-ok"); */
/*     gtk_box_pack_start (GTK_BOX (GTK_DIALOG (pref->preferences)->action_area), */
/*                         button_ok, TRUE, TRUE, 0); */
/*     gtk_widget_ref (button_ok); */
/*     gtk_object_set_data_full (GTK_OBJECT (pref->preferences),  */
/*                               "button_ok",  */
/*                               button_ok, */
/*                               (GtkDestroyNotify) gtk_widget_unref); */
/*     gtk_widget_show (button_ok); */
/*     GTK_WIDGET_SET_FLAGS (button_ok,  */
/*                           GTK_CAN_DEFAULT); */
/*     gtk_tooltips_set_tip(gpiv->tooltips, button_ok, */
/* 			 _("Updates parameters and save as defaults"), */
/* 			 NULL); */

/*     gtk_object_set_data(GTK_OBJECT(button_ok),  */
/*                         "gpiv",  */
/*                         gpiv); */
/*     g_signal_connect (GTK_OBJECT (button_ok), "clicked", */
/*                         G_CALLBACK (on_button_ok), */
/*                         NULL); */



/*     button_apply =  */
/*         (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-apply"); */
/*     gtk_box_pack_start (GTK_BOX (GTK_DIALOG (pref->preferences)->action_area), */
/*                         button_apply, TRUE, TRUE, 0); */
/*     gtk_widget_ref (button_apply); */
/*     gtk_object_set_data_full (GTK_OBJECT (pref->preferences),  */
/*                               "button_apply",  */
/*                               button_apply, */
/*                               (GtkDestroyNotify) gtk_widget_unref); */
/*     gtk_widget_show (button_apply); */
/*     GTK_WIDGET_SET_FLAGS (button_apply, GTK_CAN_DEFAULT); */
/*     gtk_tooltips_set_tip(gpiv->tooltips, button_apply, */
/* 			 _("Updates actual parameters, do not store as defaults"), */
/* 			 NULL); */

/*     gtk_object_set_data(GTK_OBJECT(button_apply),  */
/*                         "gpiv",  */
/*                         gpiv); */
/*     g_signal_connect (GTK_OBJECT (button_apply), "clicked", */
/*                         G_CALLBACK (on_button_apply), */
/*                       NULL); */



/*     button_cancel =  */
/*         (GtkWidget*) gtk_tool_button_new_from_stock ("gtk-cancel"); */
/*     gtk_box_pack_start (GTK_BOX (GTK_DIALOG (pref->preferences)->action_area), */
/*                         button_cancel, TRUE, TRUE, 0); */
/*     gtk_widget_ref (button_cancel); */
/*     gtk_object_set_data_full (GTK_OBJECT (pref->preferences),  */
/*                               "button_cancel",  */
/*                               button_cancel, */
/*                               (GtkDestroyNotify) gtk_widget_unref); */
/*     gtk_widget_show (button_cancel); */
/*     GTK_WIDGET_SET_FLAGS (button_cancel,  */
/*                           GTK_CAN_DEFAULT); */
/*     gtk_tooltips_set_tip(gpiv->tooltips,  */
/*                          button_cancel, */
/* 			 _("Close pref->preferences window"), */
/* 			 NULL); */

/*     g_signal_connect (GTK_OBJECT (button_cancel),  */
/*                         "clicked", */
/*                         G_CALLBACK (on_button_cancel), */
/*                         NULL); */


/*
 * Display callback functions
 */


    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_vecscale1), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_vecscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_vecscale2), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_vecscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_vecscale3), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_vecscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_vecscale4), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_vecscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_vecscale5), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_vecscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_vecscale6), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_vecscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_vecscale7), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_vecscale), 
                        NULL);




    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_veccolor1), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_veccolor), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_veccolor2), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_veccolor), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_veccolor3), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_veccolor), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_veccolor4), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_veccolor), 
                        NULL);



    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale0), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale1), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale2), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale3), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale4), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale5),
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale6),
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale6),
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_zoomscale7),
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_zoomscale), 
                        NULL);




    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_backgroundcolor1), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_background), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_backgroundcolor2), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_background), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_background_img1), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_background), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_background_img2), 
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_background), 
                        NULL);



/*     g_signal_connect (GTK_OBJECT(checkbutton_display_display_img1), */
/*                         "toggled", */
/*                         G_CALLBACK(on_checkbutton_display_display_img1),  */
/*                         NULL); */
/*     g_signal_connect (GTK_OBJECT(checkbutton_display_display_img2), */
/*                         "toggled", */
/*                         G_CALLBACK(on_checkbutton_display_display_img2),  */
/*                         NULL); */
    g_signal_connect (GTK_OBJECT(pref->checkbutton_display_display_intregs), "toggled",
                        G_CALLBACK(on_checkbutton_display_display_intregs), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->checkbutton_display_display_piv), 
                        "toggled",
                        G_CALLBACK(on_checkbutton_display_display_piv), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_display_none),
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_scalar), 
                        NULL);  
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_display_vor),
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_scalar), 
                        NULL);  
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_display_sstrain),
                        "toggled",
                        G_CALLBACK(on_radiobutton_display_scalar), 
                        NULL);
    g_signal_connect (GTK_OBJECT(pref->radiobutton_display_display_nstrain),
                      "toggled",
                      G_CALLBACK(on_radiobutton_display_scalar), 
                      NULL);
    
    return pref->preferences;
}




