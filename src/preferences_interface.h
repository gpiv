/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: preferences_interface.h,v $
 * Revision 1.2  2008-10-09 14:43:37  gerber
 * paralellized with OMP and MPI
 *
 * Revision 1.1  2008-09-16 11:23:03  gerber
 * added preferences_interface
 *
 */

#ifndef PREFERENCES_INTERFACE_H
#define PREFERENCES_INTERFACE_H

#include "gpiv_gui.h"



typedef struct _Pref Pref;
struct _Pref {
    GtkDialog  *preferences;
    GtkWidget *action_area;
    GtkWidget *applybutton;
    GtkWidget *cancelbutton;
    GtkWidget *okbutton;
    GtkWidget *notebook;
    GtkWidget *table1;
    GtkWidget *frame_view;
    GtkWidget *vbox1;
    GtkWidget *checkbutton_gpivbuttons;
    GtkWidget *checkbutton_tab;
    GtkWidget *frame_io;
    GtkWidget *vbox_io;
    GSList *imgformat_sel_group;
    GtkWidget *radiobutton_imgfmt_0;
    GtkWidget *radiobutton_imgfmt_1;
    GtkWidget *radiobutton_imgfmt_2;
    GSList *dataformat_sel_group;
    GtkWidget *radiobutton_datafmt_0;
    GtkWidget *radiobutton_datafmt_1;
/*     GtkWidget *checkbutton_hdf; */
    GtkWidget *checkbutton_xcorr;


    GtkWidget *checkbutton_tooltips;
    GtkWidget *frame_processes;
    GtkWidget *vbox2;
#ifdef ENABLE_CAM
    GtkWidget *checkbutton_process_cam;
#endif /* ENABLE_CAM */
#ifdef ENABLE_TRIG
    GtkWidget *checkbutton_process_trig;
#endif /* ENABLE_TRIG */
#ifdef ENABLE_IMGPROC
    GtkWidget *checkbutton_process_imgproc;
#endif /* ENABLE_ENABLE_IMGPROC */
    GtkWidget *checkbutton_process_piv;
    GtkWidget *checkbutton_process_gradient;
    GtkWidget *checkbutton_process_resstats;
    GtkWidget *checkbutton_process_errvec;
    GtkWidget *checkbutton_process_peaklck;
    GtkWidget *checkbutton_process_scale;
    GtkWidget *checkbutton_process_avg;
    GtkWidget *checkbutton_process_subtract;
    GtkWidget *checkbutton_process_vorstra;
    GtkWidget *hbox1;
    GtkWidget *label_spinner_bins;
    GtkObject *spinbutton_bins_adj;
    GtkWidget *spinbutton_bins;
#ifdef ENABLE_MPI
    GtkWidget *hbox2;
    GtkWidget *label_spinner_nodes;
    GtkObject *spinbutton_nodes_adj;
    GtkWidget *spinbutton_nodes;
#endif /* ENABLE_MPI */


    GtkWidget *tab1;
    GtkWidget *table2;

    GtkWidget *frame_display_vecscale;
    GtkWidget *vbox_display_vecscale;
    GSList *vecscale_group/*  = NULL */;
    GtkWidget *radiobutton_display_vecscale1;
    GtkWidget *radiobutton_display_vecscale2;
    GtkWidget *radiobutton_display_vecscale3;
    GtkWidget *radiobutton_display_vecscale4;
    GtkWidget *radiobutton_display_vecscale5;
    GtkWidget *radiobutton_display_vecscale6;
    GtkWidget *radiobutton_display_vecscale7;
    


    GtkWidget *frame_display_veccolor;
    GtkWidget *vbox_display_veccolor;
    GSList *veccolor_group/*  = NULL */;
    GtkWidget *radiobutton_display_veccolor1;
    GtkWidget *radiobutton_display_veccolor2;
    GtkWidget *radiobutton_display_veccolor3;
    GtkWidget *radiobutton_display_veccolor4;



    GtkWidget *frame_display_zoomscale;
    GtkWidget *vbox_display_zoomscale;
    GSList *zoomscale_group/*  = NULL */;
    GtkWidget *radiobutton_display_zoomscale0;
    GtkWidget *radiobutton_display_zoomscale1;
    GtkWidget *radiobutton_display_zoomscale2;
    GtkWidget *radiobutton_display_zoomscale3;
    GtkWidget *radiobutton_display_zoomscale4;
    GtkWidget *radiobutton_display_zoomscale5;
    GtkWidget *radiobutton_display_zoomscale6;
    GtkWidget *radiobutton_display_zoomscale7;



    GtkWidget *frame_display_backgroundcolor;
    GtkWidget *vbox_display_backgroundcolor;
    GSList *backgroundcolor_group/*  = NULL */;
    GtkWidget *radiobutton_display_backgroundcolor1; /* _blue */
    GtkWidget *radiobutton_display_backgroundcolor2; /* _black */
    GtkWidget *radiobutton_display_background_img1;
    GtkWidget *radiobutton_display_background_img2;


    GtkWidget *frame_display_display;
    GtkWidget *vbox_display_display;
    GtkWidget *checkbutton_display_display_intregs;
    GtkWidget *checkbutton_display_display_piv;

    GSList *scalardata_group/*  = NULL */;
    GtkWidget *frame_display_pivderived_display;
    GtkWidget *vbox_display_pivderived_display;
    GtkWidget *radiobutton_display_display_none;
    GtkWidget *radiobutton_display_display_vor;
    GtkWidget *radiobutton_display_display_sstrain;
    GtkWidget *radiobutton_display_display_nstrain;

    GtkWidget *frame_display_view;
    GtkWidget *vbox_display_view;
    GtkWidget *checkbutton_display_view_menubar;
    GtkWidget *checkbutton_display_view_rulers;
    GtkWidget *checkbutton_display_stretch_auto;

    GtkWidget *tab2;

/*   GtkWidget *fontselection; */
/*   GtkWidget *tab3; */

/*     GtkWidget *dialog_action_area1; */
/*     GtkWidget *button_ok; */
/*     GtkWidget *button_apply; */
/*     GtkWidget *button_cancel; */

};

#endif /* PREFERENCES_INTERFACE_H */
