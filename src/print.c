/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: print.c,v $
 * Revision 1.9  2008-04-28 12:00:58  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.8  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.7  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.6  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.5  2006/01/31 14:28:13  gerber
 * version 0.3.0
 *
 * Revision 1.4  2005/06/15 09:40:40  gerber
 * debugged, optimized
 *
 * Revision 1.3  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.2  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.1  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include "support.h"

#include "gpiv_gui.h"
#include "utils.h"
#include "print.h"
/* #include "console_menus.h" */
/* #include "display.h" */


GtkDialog *
create_print_dialog (GpivConsole *gpiv
		     )
/*-----------------------------------------------------------------------------
ttp://developer.gnome.org/doc/API/2.0/gtk/GtkDialog.html#GtkDialogFlags
 */
{
	PrintDialog * pri = g_new0(PrintDialog, 1);
/*     g_snprintf(pri->var.label_printer_state, GPIV_MAX_CHARS, "Printer is ready to print"); */
/*     g_snprintf(pri->var.label_printer_state, GPIV_MAX_CHARS, "Printer is offline"); */
	g_snprintf(pri->var.label_printer_state, 
		   GPIV_MAX_CHARS, 
		   "Printer state is unknown");
	pri->var.print_to_printer = FALSE;
	pri->var.select_range = TRUE;


/* 
 * Main window of print dialog
 */
	if (gpiv_par->verbose) g_warning("create_print_dialog:: 1");


/*
 * GTK2:
 */

	pri->dialog = GTK_DIALOG( gtk_dialog_new_with_buttons 
				  ("gpiv print",
				   /* GNOME_APP ( */ /* gpiv->console */ NULL,
				   GTK_DIALOG_DESTROY_WITH_PARENT, 
				   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				   GTK_STOCK_PRINT_PREVIEW, GTK_RESPONSE_APPLY,
				   GTK_STOCK_PRINT, GTK_RESPONSE_ACCEPT,
				   NULL));
	
	gtk_dialog_set_default_response (pri->dialog,
					 GTK_RESPONSE_REJECT);
	g_signal_connect_swapped (pri->dialog,
				  "response", 
				  G_CALLBACK (gtk_widget_destroy),
				  pri->dialog);
	g_signal_connect (pri->dialog,
			  "response", 
			  G_CALLBACK (on_print_response),
			  NULL);

	gtk_object_set_data (GTK_OBJECT (pri->dialog),
			     "print_dialog",
			     pri->dialog);
/*
 * Gnome2:
 */
	gtk_window_set_policy (GTK_WINDOW (pri->dialog),
			       FALSE,
			       FALSE,
			       FALSE);
	if (gpiv_par->verbose) g_warning("create_print_dialog:: 1.3");
	gtk_window_set_wmclass (GTK_WINDOW (pri->dialog),
				"gpiv print", 
				"");

	if (gpiv_par->verbose) g_warning("create_print_dialog:: 1.4");


/*
 * Main table
 */

/* 	pri->vbox_dialog = GTK_DIALOG (pri->dialog)->vbox; */
/* 	gtk_object_set_data (GTK_OBJECT (pri->dialog), */
/* 			     "vbox_dialog", */
/* 			     pri->vbox_dialog); */
/* 	gtk_widget_show (pri->vbox_dialog); */



	pri->frame_print = gtk_frame_new (_("Select printer"));
	gtk_widget_ref (pri->frame_print);
	gtk_object_set_data_full (GTK_OBJECT (pri->dialog), 
				  "frame_print", 
				  pri->frame_print,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (pri->frame_print);
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(pri->dialog)->vbox),
                           pri->frame_print);

/* 	gtk_box_pack_start( GTK_BOX (pri->vbox_dialog),  */
/* 			    pri->frame_print, */
/* 			    TRUE,  */
/* 			    TRUE, */
/* 			    0); */
    
    
    

	pri->table_print = gtk_table_new (2,
					  3,
					  FALSE);
	gtk_widget_ref (pri->table_print);
	gtk_object_set_data_full (GTK_OBJECT (pri->dialog),
				  "table_print",
				  pri->table_print,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (pri->table_print);
	gtk_container_add (GTK_CONTAINER (pri->frame_print),
			   pri->table_print);
    
    
    
	pri->radiobutton_printer =
		gtk_radio_button_new_with_label(pri->printer_group,
						_("Printer"));
	pri->printer_group =
		gtk_radio_button_group(GTK_RADIO_BUTTON
				       (pri->radiobutton_printer));
	gtk_widget_ref(pri->radiobutton_printer);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "radiobutton_printer",
				 pri->radiobutton_printer,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->radiobutton_printer);
	gtk_table_attach (GTK_TABLE (pri->table_print),
			  pri->radiobutton_printer,
			  0,
			  1,
			  0,
			  1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL),
			  0,
			  0);
	
	gtk_object_set_data(GTK_OBJECT(pri->radiobutton_printer),
			    "pri",
			    pri);
	



	pri->entry_printer = gtk_entry_new();
	gtk_widget_ref(pri->entry_printer);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "entry_printer",
				 pri->entry_printer,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->entry_printer);
	gtk_table_attach(GTK_TABLE(pri->table_print),
			 pri->entry_printer, 
			 1, 
			 3, 
			 0, 
			 1,
			 (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			 (GtkAttachOptions) (0),
			 0,
			 0);
	gtk_entry_set_text(GTK_ENTRY (pri->entry_printer), DEFAULT_PRINT_CMD);
	gtk_object_set_data(GTK_OBJECT(pri->entry_printer),
			    "pri",
			    pri);
	
	

	
	
	pri->radiobutton_file =
		gtk_radio_button_new_with_label(pri->printer_group,
						_("File"));
	pri->printer_group =
		gtk_radio_button_group(GTK_RADIO_BUTTON
				       (pri->radiobutton_file));
	gtk_widget_ref(pri->radiobutton_file);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "radiobutton_file",
				 pri->radiobutton_file,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->radiobutton_file);
	gtk_table_attach (GTK_TABLE (pri->table_print),
			  pri->radiobutton_file,
			  0,
			  1,
			  1,
			  2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL),
			  0,
			  0);
	
	gtk_object_set_data(GTK_OBJECT(pri->radiobutton_file),
			    "pri",
			    pri);
    



	pri->entry_file = gtk_entry_new();
	gtk_widget_ref(pri->entry_file);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "entry_file",
				 pri->entry_file,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->entry_file);
	gtk_table_attach(GTK_TABLE(pri->table_print),
			 pri->entry_file, 
			 1, 
			 2, 
			 1, 
			 2,
			 (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			 (GtkAttachOptions) (0),
			 0,
			 0);
	gtk_entry_set_text(GTK_ENTRY (pri->entry_file), DEFAULT_FNAME_PRINT);
/*     gtk_object_set_data(GTK_OBJECT(pri->entry_file), */
/*                         "pri", */
/*                         pri); */





	pri->button_browse = gtk_button_new_with_label(_("Browse"));
	gtk_widget_ref(pri->button_browse);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "button_browse",
				 pri->button_browse,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->button_browse);
	gtk_table_attach(GTK_TABLE(pri->table_print),
			 pri->button_browse, 
			 2, 
			 3, 
			 1, 
			 2,
			 (GtkAttachOptions) (GTK_FILL),
			 (GtkAttachOptions) (GTK_FILL),
			 0,
			 0);
	gtk_tooltips_set_tip(gpiv->tooltips,
			     pri->button_browse,
			     _("Launches a file selector to browse a file"),
			     NULL);
	gtk_object_set_data(GTK_OBJECT(pri->button_browse),
			    "pri",
			    pri);



	pri->label_label_printerstate = gtk_label_new(_("State: "));
	gtk_widget_ref(pri->label_label_printerstate);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "label_label_printerstate",
				 pri->label_label_printerstate,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->label_label_printerstate);
	gtk_table_attach(GTK_TABLE(pri->table_print),
			 pri->label_label_printerstate, 
			 0, 
			 1, 
			 2, 
			 3,
			 (GtkAttachOptions) (GTK_FILL),
			 (GtkAttachOptions) (GTK_FILL),
			 0,
			 0);



	pri->label_printerstate = gtk_label_new(pri->var.label_printer_state);
	gtk_widget_ref(pri->label_printerstate);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "label_printerstate",
				 pri->label_printerstate,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->label_printerstate);
	gtk_table_attach(GTK_TABLE(pri->table_print),
			 pri->label_printerstate, 
			 1, 
			 2, 
			 2, 
			 3,
			 (GtkAttachOptions) (GTK_FILL),
			 (GtkAttachOptions) (GTK_FILL),
			 0,
			 0);



/*
 * Print range frame
 */
	pri->frame_range = gtk_frame_new (_("Print range"));
	gtk_widget_ref (pri->frame_range);
	gtk_object_set_data_full (GTK_OBJECT (pri->dialog), 
				  "frame_range", 
				  pri->frame_range,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (pri->frame_range);
        gtk_container_add (GTK_CONTAINER (GTK_DIALOG(pri->dialog)->vbox),
                           pri->frame_range);

/* 	gtk_box_pack_start( GTK_BOX (pri->vbox_dialog),  */
/* 			    pri->frame_range, */
/* 			    TRUE,  */
/* 			    TRUE, */
/* 			    0); */
    
    
    

	pri->table_range = gtk_table_new (2,
					  5,
					  FALSE);
	gtk_widget_ref (pri->table_range);
	gtk_object_set_data_full (GTK_OBJECT (pri->dialog),
				  "table_range",
				  pri->table_range,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (pri->table_range);
	gtk_container_add (GTK_CONTAINER (pri->frame_range),
			   pri->table_range);
    
    


	pri->radiobutton_all =
		gtk_radio_button_new_with_label(pri->range_group,
						_("All"));
	pri->range_group =
		gtk_radio_button_group(GTK_RADIO_BUTTON
				       (pri->radiobutton_all));
	gtk_widget_ref(pri->radiobutton_all);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "radiobutton_all",
				 pri->radiobutton_all,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->radiobutton_all);
	gtk_table_attach (GTK_TABLE (pri->table_range),
			  pri->radiobutton_all,
			  0,
			  1,
			  0,
			  1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL),
			  0,
			  0);
	gtk_object_set_data(GTK_OBJECT(pri->radiobutton_all), 
			    "pri", 
			    pri);
	
    


	pri->radiobutton_range = 
		gtk_radio_button_new_with_label(pri->range_group, 
						_("Buffers")); 
	pri->range_group = 
		gtk_radio_button_group(GTK_RADIO_BUTTON 
				       (pri->radiobutton_range)); 
	gtk_widget_ref(pri->radiobutton_range); 
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog), 
				 "radiobutton_range", 
				 pri->radiobutton_range, 
				 (GtkDestroyNotify) gtk_widget_unref); 
	gtk_widget_show(pri->radiobutton_range); 
	gtk_table_attach (GTK_TABLE (pri->table_range), 
			  pri->radiobutton_range, 
			  0, 
			  1, 
			  1, 
			  2, 
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 
			  (GtkAttachOptions) (GTK_FILL), 
			  0,
			  0);
	gtk_object_set_data(GTK_OBJECT(pri->radiobutton_range), 
			    "pri", 
			    pri);

    


	pri->label_range_from = gtk_label_new(_("From: "));
	gtk_widget_ref(pri->label_range_from);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "label_range_from",
				 pri->label_range_from,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->label_range_from);
	gtk_table_attach(GTK_TABLE(pri->table_range),
			 pri->label_range_from, 
			 1, 
			 2, 
			 1, 
			 2,
			 (GtkAttachOptions) (GTK_FILL),
			 (GtkAttachOptions) (GTK_FILL),
			 0,
			 0);
	
	
	
	pri->spinbutton_adj_range_start =
		gtk_adjustment_new(gpiv->first_selected_row,
				   1, 
				   nbufs - 1, 
				   1,
				   2,
				   nbufs - 1);
	
	pri->spinbutton_range_start =
		gtk_spin_button_new(GTK_ADJUSTMENT(pri->spinbutton_adj_range_start),
				    gpiv->first_selected_row,
				    0);
	gtk_widget_ref(pri->spinbutton_range_start);
	gtk_widget_show(pri->spinbutton_range_start);
	gtk_table_attach (GTK_TABLE (pri->table_range), 
                      pri->spinbutton_range_start, 
			  2, 
			  3, 
			  1, 
			  2, 
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 
			  (GtkAttachOptions) (GTK_FILL), 
			  0,
			  0);
	gtk_entry_set_editable(GTK_ENTRY(pri->spinbutton_range_start),
			       TRUE);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(pri->spinbutton_range_start),
				    TRUE);
	gtk_object_set_data(GTK_OBJECT(pri->spinbutton_range_start),
			    "pri",
			    pri);
	

	
	pri->label_range_end = gtk_label_new(_("To: "));
	gtk_widget_ref(pri->label_range_end);
	gtk_object_set_data_full(GTK_OBJECT(pri->dialog),
				 "label_range_end",
				 pri->label_range_end,
				 (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show(pri->label_range_end);
	gtk_table_attach(GTK_TABLE(pri->table_range),
			 pri->label_range_end, 
			 3, 
			 4, 
			 1, 
			 2,
			 (GtkAttachOptions) (GTK_FILL),
			 (GtkAttachOptions) (GTK_FILL),
			 0,
			 0);
	
	
	
	pri->spinbutton_adj_range_end =
		gtk_adjustment_new(gpiv->last_selected_row,
				   1, 
				   nbufs - 1, 
				   1,
				   2,
				   nbufs - 1);
	
	pri->spinbutton_range_end =
		gtk_spin_button_new(GTK_ADJUSTMENT(pri->spinbutton_adj_range_end),
				    gpiv->last_selected_row,
				    0);
	gtk_widget_ref(pri->spinbutton_range_end);
	gtk_widget_show(pri->spinbutton_range_end);
	gtk_table_attach (GTK_TABLE (pri->table_range), 
			  pri->spinbutton_range_end, 
			  4, 
			  5, 
			  1, 
			  2, 
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 
			  (GtkAttachOptions) (GTK_FILL), 
			  0,
			  0);
	gtk_entry_set_editable(GTK_ENTRY(pri->spinbutton_range_end),
			       TRUE);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(pri->spinbutton_range_end),
				    TRUE);
	gtk_object_set_data(GTK_OBJECT(pri->spinbutton_range_end),
			    "pri",
			    pri);
    


/*
 * Dialog area with "Print", "Preview" and "Cancel" buttons
 */

/* 	pri->dialog_action_area = GTK_DIALOG (pri->dialog)->action_area; */
/* 	gtk_object_set_data (GTK_OBJECT (pri->dialog),  */
/* 			     "dialog_action_area",  */
/* 			     pri->dialog_action_area); */
/* 	gtk_widget_show (pri->dialog_action_area); */
/* 	gtk_button_box_set_layout (GTK_BUTTON_BOX (pri->dialog_action_area),  */
/* 				   GTK_BUTTONBOX_END); */
/* 	gtk_button_box_set_spacing (GTK_BUTTON_BOX (pri->dialog_action_area),  */
/* 				    8); */
	


/* 	gnome_dialog_append_button (GNOME_DIALOG (pri->dialog),  */
/* 				    "Print"); */
/* 	pri->button_print =  */
/* 		GTK_WIDGET (g_list_last (GNOME_DIALOG (pri->dialog)->buttons)->data); */


/* 	pri->button_print =  */
/* 		(GtkWidget*) gtk_tool_button_new_from_stock ("gtk-print"); */
/* 	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (pri)->action_area), */
/* 			    pri->button_print, TRUE, TRUE, 0); */

/* 	gtk_widget_ref (pri->button_print); */
/* 	gtk_object_set_data_full (GTK_OBJECT (pri->dialog),  */
/* 				  "button_print",  */
/* 				  pri->button_print, */
/* 				  (GtkDestroyNotify) gtk_widget_unref); */
/* 	gtk_widget_show (pri->button_print); */
/* 	GTK_WIDGET_SET_FLAGS (pri->button_print,  */
/* 			      GTK_CAN_DEFAULT); */
/* 	gtk_tooltips_set_tip(gpiv->tooltips, pri->button_print, */
/* 			     _("Prints the content of the actual buffer display"), */
/* 			     NULL); */
	
/* 	gtk_object_set_data(GTK_OBJECT(pri->button_print),  */
/* 			    "pri",  */
/* 			    pri); */
/* 	g_signal_connect (GTK_OBJECT (pri->button_print), "clicked", */
/* 			    G_CALLBACK (on_button_print), */
/* 			    NULL); */
/* 	gtk_widget_set_sensitive(pri->button_print, FALSE); */
	
	
	

/* 	pri->button_preview =  */
/* 		(GtkWidget*) gtk_tool_button_new_from_stock ("gtk-print_preview"); */
/* 	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (pri)->action_area), */
/* 			    pri->button_preview, TRUE, TRUE, 0); */
/* 	gtk_widget_ref (pri->button_preview); */
/* 	gtk_object_set_data_full (GTK_OBJECT (pri->dialog),  */
/* 				  "button_preview",  */
/* 				  pri->button_preview, */
/* 				  (GtkDestroyNotify) gtk_widget_unref); */
/* 	gtk_widget_show (pri->button_preview); */
/* 	GTK_WIDGET_SET_FLAGS (pri->button_preview, GTK_CAN_DEFAULT); */
/* 	gtk_tooltips_set_tip(gpiv->tooltips, pri->button_preview, */
/* 			     _("Shows a preview of the output to be printed"), */
/* 			     NULL); */
	
/* 	gtk_object_set_data(GTK_OBJECT(pri->button_preview),  */
/* 			    "gpiv",  */
/* 			    gpiv); */
/* 	g_signal_connect (GTK_OBJECT (pri->button_preview), "clicked", */
/* 			    G_CALLBACK (on_button_preview), */
/* 			    NULL); */
/* 	gtk_widget_set_sensitive(pri->button_preview, FALSE); */
	
	
	
/* 	gnome_dialog_append_button (GNOME_DIALOG (pri->dialog),  */
/* 				    GTK_STOCK_CANCEL); */
/* 	pri->button_print_cancel =  */
/* 		GTK_WIDGET (g_list_last (GNOME_DIALOG (pri->dialog)->buttons)->data); */

/* 	pri->button_print_cancel =  */
/* 		(GtkWidget*) gtk_tool_button_new_from_stock ("gtk-cancel"); */
/* 	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (pri)->action_area), */
/* 			    pri->button_print_cancel, TRUE, TRUE, 0); */
/* 	gtk_widget_ref (pri->button_print_cancel); */
/* 	gtk_object_set_data_full (GTK_OBJECT (pri->dialog),  */
/* 				  "button_print_cancel",  */
/* 				  pri->button_print_cancel, */
/* 				  (GtkDestroyNotify) gtk_widget_unref); */
/* 	gtk_widget_show (pri->button_print_cancel); */
/* 	GTK_WIDGET_SET_FLAGS (pri->button_print_cancel,  */
/* 			      GTK_CAN_DEFAULT); */
/* 	gtk_tooltips_set_tip(gpiv->tooltips,  */
/* 			     pri->button_print_cancel, */
/* 			     _("Close pri->dialog window"), */
/* 			     NULL); */
	
/* 	gtk_object_set_data(GTK_OBJECT(pri->button_print_cancel),  */
/* 			    "pri",  */
/* 			    pri); */
/* 	g_signal_connect (GTK_OBJECT (pri->button_print_cancel),  */
/* 			    "clicked", */
/* 			    G_CALLBACK (on_button_print_cancel), */
/* 			    NULL); */




/*
 * Connecting to callback functions
 */


	g_signal_connect (GTK_OBJECT(pri->radiobutton_printer), 
			    "toggled",
			    G_CALLBACK(on_radiobutton_printer), 
			    NULL);
	
	g_signal_connect(GTK_OBJECT(pri->entry_printer),
			   "changed",
			   G_CALLBACK(on_entry_printer),
			   pri->entry_printer);
	
	g_signal_connect (GTK_OBJECT(pri->radiobutton_file), 
			    "toggled",
			    G_CALLBACK(on_radiobutton_file), 
			    NULL);
	
	g_signal_connect(GTK_OBJECT(pri->entry_file),
			   "changed",
			   G_CALLBACK(on_entry_file),
			   pri->entry_file);
	
	g_signal_connect(GTK_OBJECT(pri->button_browse),
			   "clicked",
			   G_CALLBACK(on_button_browse),
			   pri->button_browse);
	
	g_signal_connect (GTK_OBJECT(pri->radiobutton_all), 
			    "toggled",
			    G_CALLBACK(on_radiobutton_all), 
			    NULL);

	g_signal_connect (GTK_OBJECT(pri->radiobutton_range), 
			    "toggled",
			    G_CALLBACK(on_radiobutton_range), 
			    NULL);

	g_signal_connect(GTK_OBJECT(pri->spinbutton_range_start),
			   "changed",
			   G_CALLBACK(on_spinbutton_range_start),
			   pri->spinbutton_range_start);
	
	g_signal_connect(GTK_OBJECT(pri->spinbutton_range_end),
			   "changed",
			   G_CALLBACK(on_spinbutton_range_end),
			   pri->spinbutton_range_end);



/*
 * Initialize settings
 */
	if (pri->var.print_to_printer) {
		gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
					    (pri->radiobutton_printer),
					    TRUE);
	} else {
		gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
					    (pri->radiobutton_file),
					    TRUE);        
	}
	
	
	if (pri->var.select_range) {
		gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
					    (pri->radiobutton_range),
					    TRUE);
	} else {
		gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON
					    (pri->radiobutton_range),
					    FALSE);
/*         gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON */
/*                                     (pri->radiobutton_all), */
/*                                     TRUE); */
	}
	


	return pri->dialog;
}



static void 
file_ok_sel(GtkWidget * widget, 
            GtkFileSelection * fs
	    )
/* ----------------------------------------------------------------------------
 * Print file selection
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	pri->var.fname_print = 
		g_strdup(gtk_file_selection_get_filename(GTK_FILE_SELECTION(fs)));
	gtk_entry_set_text(GTK_ENTRY (pri->entry_file), pri->var.fname_print);
}



static void
print(PrintDialog *pri
      )
/*-----------------------------------------------------------------------------
 *  prints and closes print dialog
 */
{
	g_warning(_("Button \"Print\" pressed"));
	g_free(pri->var.print_cmd);
	g_free(pri->var.fname_print);
/* 	gnome_dialog_close(GNOME_DIALOG(gpiv_print_dialog)); */
/*
 * Gnome2:
 */
/*     gtk_widget_destroy(GTK_DIALOG(pri->dialog)); */
}



#ifdef HAVE_GNOME_PRINT
static int
print_on_context(/* gpaint_image * image, */
/*                  const gchar * name,  */
		 GnomePrintContext * pc
		 )
/* ----------------------------------------------------------------------------
 */
{
    double matrix[6] = { 1, 0, 0, 1, 0, 0 };
    gnome_print_beginpage(pc, display_act->file_uri_name);

    gnome_print_concat(pc, matrix);
    gnome_print_translate(pc, 0, 0);
    gnome_print_scale(pc, display_act->img->image->header->ncolumns,
		      display_act->img->image->header->nrows);

    gnome_print_rgbaimage(pc,
                          gdk_pixbuf_get_pixels(display_act->img->pixbuf1),
                          display_act->img->image->header->ncolumns,
                          display_act->img->image->header->nrows, 
			  display_act->img->rgb_img_width);

    gnome_print_showpage(pc);
    return 1;
}



static int
preview(void
	)
/* ----------------------------------------------------------------------------
 * preview print from gpaint: do_print_preview
 */
{
    GtkWidget *toplevel, *canvas, *sw;
    GnomePrintContext *pc = 0;
    GnomePrintConfig *cfg = NULL;
    GnomePrintJob *job = NULL;
    GnomePrintJobPreview *pmp = NULL;
    GnomePrintContext *ctx = NULL;
        
    cfg = gnome_print_config_default();
    job = gnome_print_job_new(cfg);
    

    /* transfer dialog data to output context */

    ctx = gnome_print_job_get_context(job);
    print_on_context(/* image, name,  */ctx);
    gnome_print_job_close(job);
    
    pmp = GNOME_PRINT_JOB_PREVIEW
	    (gnome_print_job_preview_new(job, 
					 "Print Preview"));
    g_signal_connect((gpointer)pmp, 
		     "destroy", 
		     G_CALLBACK(gtk_widget_destroy), 
		     (gpointer)pmp);
    gtk_window_set_modal(GTK_WINDOW(pmp), 
			 TRUE);
    gtk_widget_show(GTK_WIDGET(pmp));

    return 1;

}

#else

static int
preview(void
	)
/* ----------------------------------------------------------------------------
 * preview print from gpaint: do_print_preview
 */
{
    g_warning(_("Button \"Preview\" pressed"));
    return 1;

}

#endif

static void
cancel(PrintDialog *pri
/*        GtkDialog *dialog */
/*        GtkWidget *widget,  */
/*        gpointer data */
       )
/* ----------------------------------------------------------------------------
 * closes print dialog
 */
{	
	g_warning(_("Button \"Cancel\" pressed"));
	g_free(pri->var.print_cmd);
	g_free(pri->var.fname_print);
/* 	gnome_dialog_close(GNOME_DIALOG(gpiv_print_dialog)); */
/*
 * Gnome2:
 */
    gtk_widget_destroy(GTK_WIDGET(pri->dialog));
}


/*
 * Callback functions
 */

void
dialog_action(GtkDialog *dialog,
	      gint response,
	      gpointer data)
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(dialog), "pri");

	g_assert(response == GTK_RESPONSE_ACCEPT
		 || response == GTK_RESPONSE_APPLY
		 || response == GTK_RESPONSE_CANCEL);

	gint result = gtk_dialog_run (GTK_DIALOG (dialog));
	switch (result)
		{
		case GTK_RESPONSE_ACCEPT:
			print (pri);
			break;
		case GTK_RESPONSE_APPLY:
			preview ();
			break;
		case GTK_RESPONSE_CANCEL:
/* 			do_nothing_since_dialog_was_cancelled (); */
			cancel (pri);
			break;
		default:
			g_warning("dialog_action: should not arrive here");
			break;
		}
/* 	gtk_widget_destroy (dialog); */
}



void 
on_radiobutton_printer(GtkWidget *widget, 
                       gpointer data
		       )
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	if (GTK_TOGGLE_BUTTON(widget)->active) {
		pri->var.print_to_printer = TRUE;
		gtk_widget_set_sensitive(pri->entry_printer, TRUE);
	} else {
		pri->var.print_to_printer = FALSE;
		gtk_widget_set_sensitive(pri->entry_printer, FALSE);
	}
}



void
on_entry_printer(GtkSpinButton *widget, 
                 GtkWidget *entry
		 )
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");

/*     snprintf(printer_name, GPIV_MAX_CHARS,"%s",  */
/*              gtk_entry_get_text(GTK_ENTRY(entry))); */
	pri->var.print_cmd = 
		g_strdup(gtk_entry_get_text(GTK_ENTRY (pri->entry_file)));
	g_warning("print_cmd = %s", pri->var.print_cmd);
}



void 
on_radiobutton_file(GtkWidget *widget, 
		    gpointer data
		    )
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	if (GTK_TOGGLE_BUTTON(widget)->active) {
		gtk_widget_set_sensitive(pri->entry_file, TRUE);
		gtk_widget_set_sensitive(pri->button_browse, TRUE);
	} else {
		gtk_widget_set_sensitive(pri->entry_file, FALSE);
		gtk_widget_set_sensitive(pri->button_browse, FALSE);
		pri->var.print_to_printer = TRUE;
	}
}



void
on_entry_file(GtkSpinButton *widget, 
	      GtkWidget *entry
	      )
/*-----------------------------------------------------------------------------
 */
{
/*     snprintf(file_name, GPIV_MAX_CHARS,"%s",  */
/*              gtk_entry_get_text(GTK_ENTRY(entry))); */

}



void
on_button_browse(GtkWidget *widget, 
                 gpointer data
		 )
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	GtkWidget *filew = NULL;
	
	filew = gtk_file_selection_new("gpiv: print file");
	g_signal_connect(GTK_OBJECT(filew), "destroy",
			   G_CALLBACK( destroy), &filew);
	g_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(filew)->ok_button),
			   "clicked", G_CALLBACK( file_ok_sel),
			   GTK_OBJECT(filew));
	g_signal_connect_swapped(GTK_OBJECT(GTK_FILE_SELECTION(filew)->ok_button),
				  "clicked",
				  G_CALLBACK( gtk_widget_destroy),
				  GTK_OBJECT(filew));
	gtk_object_set_data(GTK_OBJECT(GTK_FILE_SELECTION(filew)->ok_button), 
			    "pri",
			    pri);
	g_signal_connect_swapped(GTK_OBJECT
				  (GTK_FILE_SELECTION(filew)->cancel_button),
				  "clicked",
				  G_CALLBACK( gtk_widget_destroy),
				  GTK_OBJECT(filew));
	gtk_file_selection_set_filename(GTK_FILE_SELECTION(filew),
					gpiv_var->fname_last_print);
	gtk_widget_show(filew);
	gtk_object_set_data(GTK_OBJECT(GTK_FILE_SELECTION(filew)), 
			    "pri",
			    pri);

}



void 
on_radiobutton_all(GtkWidget *widget, 
		   gpointer data
		   )
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	if (GTK_TOGGLE_BUTTON(widget)->active) {
		pri->var.select_range = FALSE;
	}
}



void 
on_radiobutton_range(GtkWidget *widget, 
		     gpointer data
		     )
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	if (GTK_TOGGLE_BUTTON(widget)->active) {
		pri->var.select_range = TRUE;
		gtk_widget_set_sensitive(pri->spinbutton_range_start, TRUE);
		gtk_widget_set_sensitive(pri->spinbutton_range_end, TRUE);
	} else {
		gtk_widget_set_sensitive(pri->spinbutton_range_start, FALSE);
		gtk_widget_set_sensitive(pri->spinbutton_range_end, FALSE);
		pri->var.select_range = FALSE;
	}
}



void
on_spinbutton_range_start(GtkSpinButton * widget, 
                          GtkWidget * entry
			  )
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	pri->var.print_start = gtk_spin_button_get_value_as_int(widget);
	GTK_ADJUSTMENT(pri->spinbutton_adj_range_end)->lower = 
		(gint) pri->var.print_start;
}



void
on_spinbutton_range_end(GtkSpinButton * widget, 
			GtkWidget * entry
			)
/*-----------------------------------------------------------------------------
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	pri->var.print_end = gtk_spin_button_get_value_as_int(widget);
	GTK_ADJUSTMENT(pri->spinbutton_adj_range_start)->upper = 
		(gint) pri->var.print_end;
}



void
on_print_response(GtkDialog *dialog,
                  gint response,
                  gpointer data
                  )
/*-----------------------------------------------------------------------------
 */
{
/*     GpivConsole *gpiv = gtk_object_get_data(GTK_OBJECT(dialog), "gpiv"); */
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(dialog), "print_dialog");
	g_assert( response == GTK_RESPONSE_OK
		  || response == GTK_RESPONSE_APPLY
		  || response == GTK_RESPONSE_CANCEL);
	
	switch (response) {
	case GTK_RESPONSE_OK:
		g_warning(_("Button \"Print\" pressed"));
		g_free(pri->var.print_cmd);
		g_free(pri->var.fname_print);
		break;
		
	case GTK_RESPONSE_APPLY:
		g_warning(_("Button \"Preview\" pressed"));
		break;
		
	case GTK_RESPONSE_CANCEL:
		g_warning(_("Button \"Cancel\" pressed"));
		g_free(pri->var.print_cmd);
		g_free(pri->var.fname_print);
		break;
		
	default:
		g_warning("on_preferences_response: should not arrive here");
		break;
	}
}


/*
 * BUGFIX: obsolete functions: on_button_...; cleanup
 */
void
on_button_print(GtkWidget *widget, 
		gpointer data
		)
/*-----------------------------------------------------------------------------
 *  prints and closes print dialog
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	g_warning(_("Button \"Print\" pressed"));
	g_free(pri->var.print_cmd);
	g_free(pri->var.fname_print);
/* 	gnome_dialog_close(GNOME_DIALOG(gpiv_print_dialog)); */
/*
 * Gnome2:
 */
    gtk_widget_destroy(GTK_WIDGET (pri->dialog));
}



void
on_button_preview(GtkWidget *widget, 
		  gpointer data
		  )
/* ----------------------------------------------------------------------------
 * preview print
 */
{
        g_warning(_("Button \"Preview\" pressed"));
}



void
on_button_print_cancel(GtkWidget *widget, 
		       gpointer data
		       )
/* ----------------------------------------------------------------------------
 * closes print dialog
 */
{
	PrintDialog *pri = gtk_object_get_data(GTK_OBJECT(widget), "pri");
	
	g_warning(_("Button \"Cancel\" pressed"));
	g_free(pri->var.print_cmd);
	g_free(pri->var.fname_print);
/* 	gnome_dialog_close(GNOME_DIALOG(gpiv_print_dialog)); */
/*
 * Gnome2:
 */
    gtk_widget_destroy(GTK_WIDGET(pri->dialog));
}
