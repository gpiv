/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: print.h,v $
 * Revision 1.4  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.3  2006/01/31 14:28:13  gerber
 * version 0.3.0
 *
 * Revision 1.2  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.1  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 * Revision 1.3  2003/08/22 15:24:53  gerber
 * interactive spatial scaling
 *
 * Revision 1.2  2003/07/25 15:40:24  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#ifndef PRINT_H
#define PRINT_H
/* #ifdef HAVE_CONFIG_H */
/* #  include <config.h> */
/* #endif */

#define DEFAULT_PRINT_CMD "lpr"
#define DEFAULT_FNAME_PRINT "gpiv_print.pdf"


GtkDialog *
create_print_dialog (GpivConsole *gpiv);


/*
 * printer variables
 */

typedef struct _PrintVar PrintVar;
struct _PrintVar {
	gboolean print_to_printer;
	char label_printer_state[GPIV_MAX_CHARS];
	gchar *print_cmd; 
	gchar *fname_print;
	
	gboolean select_range;
	gint  print_start;
	gint  print_end;
};


/*
 * print dialog interface widgets
 */

typedef struct _PrintDialog PrintDialog;
struct _PrintDialog {
	PrintVar var;
	GtkDialog *dialog;
/* 	GtkWidget *vbox_dialog; */
	
	GSList *printer_group;
	GtkWidget *frame_print;
	GtkWidget *table_print;
	GtkWidget *radiobutton_printer;
	GtkWidget *entry_printer;
	GtkWidget *radiobutton_file;
	GtkWidget *entry_file;
	GtkWidget *button_browse;
	GtkWidget *label_label_printerstate;
	GtkWidget *label_printerstate;
	
	GSList *range_group;
	GtkWidget *frame_range;
	GtkWidget *table_range;
	GtkWidget *radiobutton_all;
	GtkWidget *radiobutton_range;
	GtkWidget *label_range_from;
	GtkObject *spinbutton_adj_range_start;
	GtkWidget *spinbutton_range_start;
	GtkWidget *label_range_end;
	GtkObject *spinbutton_adj_range_end;
	GtkWidget *spinbutton_range_end;

/* 	GtkWidget *dialog_action_area; */
/* 	GtkWidget *button_print; */
/* 	GtkWidget *button_preview; */
/* 	GtkWidget *button_print_cancel; */
};


/*
 * Callback functions
 * printer select
 */

void 
on_radiobutton_printer(GtkWidget *widget, 
                       gpointer data);

void
on_entry_printer(GtkSpinButton *widget, 
                 GtkWidget *entry);

void 
on_radiobutton_file(GtkWidget *widget, 
		    gpointer data);

void
on_entry_file(GtkSpinButton *widget, 
	      GtkWidget *entry);

void
on_button_browse(GtkWidget *widget, 
                 gpointer data);

/*
 * print range
 */

void 
on_radiobutton_all(GtkWidget *widget, 
		   gpointer data);

void 
on_radiobutton_range(GtkWidget *widget, 
		     gpointer data);


void
on_spinbutton_range_start(GtkSpinButton * widget, 
                          GtkWidget * entry);

void
on_spinbutton_range_end(GtkSpinButton * widget, 
			GtkWidget * entry);


/*
 * Dialog buttons
 */

void
on_print_response(GtkDialog *dialog,
                  gint response,
                  gpointer data
                  );

/* void */
/* on_button_print(GtkWidget *widget,  */
/* 		gpointer data); */

/* void */
/* on_button_preview(GtkWidget *widget,  */
/* 		  gpointer data); */

/* void */
/* on_button_print_cancel(GtkWidget *widget,  */
/* 		       gpointer data); */


#endif /* PRINT_H */
