/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * utility functions for gpiv
 * $Log: utils.c,v $
 * Revision 1.22  2008-04-28 12:00:58  gerber
 * hdf-formatted files are now with .hdf extension (previously: .gpi)
 *
 * Revision 1.21  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.20  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.19  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.18  2007-03-22 16:00:32  gerber
 * Added image processing tabulator
 *
 * Revision 1.17  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.16  2006-09-18 07:27:06  gerber
 * *** empty log message ***
 *
 * Revision 1.15  2006/01/29 15:36:56  gerber
 * repaired a few annoying bugs
 *
 * Revision 1.13  2005/03/03 16:21:09  gerber
 * version number
 *
 * Revision 1.12  2005/02/26 09:43:31  gerber
 * parameter flags (parameter_logic) defined as gboolean
 *
 * Revision 1.11  2005/02/26 09:17:14  gerber
 * structured of interrogate function by using gpiv_piv_isiadapt
 *
 * Revision 1.10  2005/01/19 15:53:43  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.9  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.8  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.7  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 * Revision 1.6  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.5  2003/08/22 15:24:53  gerber
 * interactive spatial scaling
 *
 * Revision 1.4  2003/07/31 11:43:26  gerber
 * display images in gnome canvas (HOERAreset)
 *
 * Revision 1.3  2003/07/25 15:40:24  gerber
 * removed/disabled setting of correlation in Eval tab, Correlation type in Image info tab
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */

#include "gpiv_gui.h"
#include "utils.h"
#include "display.h"
#include "dialog_interface.h"
#include "console_interface.h"
#ifdef ENABLE_DAC
#include "dac_interface.h"
#endif

void
update_imgh_entries (GpivConsole *gpiv,
                     GpivImagePar *image_par
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Refreshes image header entries and labels (when a different buffer is 
 *      selected)
 *
 * PROTOTYPE LOCATATION:
 *     gpiv_img.h
 *
 * INPUTS:
 *     gpiv:           console structure
 *     image_par:      image parameters
 *
 * OUTPUTS:
 *---------------------------------------------------------------------------*/
{
    gchar label[GPIV_MAX_CHARS];
    gchar *tmp_name, *text_uri;
    GnomeVFSURI* uri = NULL;

    if (display_act != NULL) {
        g_snprintf (label, GPIV_MAX_CHARS,"%d", display_act->id);
        gtk_label_set_text (GTK_LABEL (gpiv->imgh->label_bufno), 
                            (char *) label);

        text_uri = gnome_vfs_make_uri_from_input 
            ((const gchar *) display_act->file_uri_name);
        if ((uri = gnome_vfs_uri_new (text_uri)) == NULL) return;
        g_free (text_uri);
        if (gnome_vfs_uri_is_local (uri)) {
            gtk_label_set_text (GTK_LABEL (gpiv->imgh->label_label_name),
                                "file: ");
        } else {
            gtk_label_set_text (GTK_LABEL (gpiv->imgh->label_label_name),
                                "uri: ");
        }
        gtk_label_set_text (GTK_LABEL (gpiv->imgh->label_name), 
                            display_act->file_uri_name);
        g_snprintf (label, GPIV_MAX_CHARS, "%d", display_act->img->image->header->ncolumns);
        gtk_label_set_text (GTK_LABEL (gpiv->imgh->label_ncols), 
                            (char *) label);
        g_snprintf (label, GPIV_MAX_CHARS, "%d", display_act->img->image->header->nrows);
        gtk_label_set_text (GTK_LABEL (gpiv->imgh->label_nrows), 
                            (char *) label);
    }


    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_imgtitle), 
                        image_par->title);


    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gpiv->imgh->spinbutton_colpos), 
                               image_par->z_off_x);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gpiv->imgh->spinbutton_rowpos), 
                               image_par->z_off_y);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gpiv->imgh->spinbutton_sscale), 
                               image_par->s_scale);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gpiv->imgh->spinbutton_tscale), 
                               image_par->t_scale);


    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_crdate), 
                        image_par->creation_date);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_location), 
                        image_par->location);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_author), 
                        image_par->author);
    gtk_entry_set_text (GTK_ENTRY(gpiv->imgh->entry_software), 
                        image_par->software);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_source), 
                        image_par->source);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_usertext), 
                        image_par->usertext);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_warning), 
                        image_par->warning);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_disclaimer), 
                        image_par->disclaimer);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_comment), 
                        image_par->comment);
    gtk_entry_set_text (GTK_ENTRY(gpiv->imgh->entry_copyright), 
                        image_par->copyright);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_email), 
                        image_par->email);
    gtk_entry_set_text (GTK_ENTRY (gpiv->imgh->entry_url), 
                        image_par->url);
}



void
update_eval_entries (GpivConsole *gpiv,
                     GpivImagePar *image_par
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Refreshes spinners of col/row start/ end/ shift for image size 
 *      (when a different buffer is selected)
 *
 * PROTOTYPE LOCATATION:
 *     gpiv_img.h
 *
 * INPUTS:
 *     gpiv:           console structure
 *     image_par:      image parameters
 *
 * OUTPUTS:
 *---------------------------------------------------------------------------*/
{
    if (display_act != NULL) {
        gtk_spin_button_set_range (GTK_SPIN_BUTTON 
                                   (gpiv->piveval->spinbutton_colstart),
                                   0.0,
                                   image_par->ncolumns - 1);
        gtk_spin_button_set_range (GTK_SPIN_BUTTON 
                                   (gpiv->piveval->spinbutton_colend),
                                   0.0,
                                   image_par->ncolumns - 1);
        gtk_spin_button_set_range (GTK_SPIN_BUTTON 
                                   (gpiv->piveval->spinbutton_preshiftcol),
                                   0.0 - (image_par->ncolumns - 1),
                                   image_par->ncolumns - 1);
        gtk_spin_button_set_range (GTK_SPIN_BUTTON 
                                   (gpiv->piveval->spinbutton_rowstart),
                                   0.0,
                                   image_par->nrows - 1);
        gtk_spin_button_set_range (GTK_SPIN_BUTTON 
                                   (gpiv->piveval->spinbutton_rowend),
                                   0.0,
                                   display_act->img->image->header->nrows - 1);
        gtk_spin_button_set_range (GTK_SPIN_BUTTON 
                                   (gpiv->piveval->spinbutton_preshiftrow),
                                   0.0 - (image_par->nrows - 1),
                                   image_par->nrows - 1);


        /*
         * Adjust spinbutton values if they are invalid
         * Columns
         */
        if (gl_piv_par->col_start > image_par->ncolumns - 1) {
            gl_piv_par->col_start = image_par->ncolumns - 1;
            gtk_spin_button_set_value (GTK_SPIN_BUTTON 
                                       (gpiv->piveval->spinbutton_colstart),
                                       gl_piv_par->col_start);
        }

        if (gl_piv_par->col_end > image_par->ncolumns - 1) {
            gl_piv_par->col_end = image_par->ncolumns - 1;
            gtk_spin_button_set_value (GTK_SPIN_BUTTON 
                                       (gpiv->piveval->spinbutton_colend),
                                       gl_piv_par->col_end);
        }


        if (gl_piv_par->pre_shift_col > image_par->ncolumns - 1) {
            gl_piv_par->pre_shift_col = image_par->ncolumns - 1;
            gtk_spin_button_set_value (GTK_SPIN_BUTTON 
                                       (gpiv->piveval->spinbutton_preshiftcol),
                                       gl_piv_par->pre_shift_col);
        }

        if (gl_piv_par->pre_shift_col < (0.0 - (image_par->ncolumns - 1))) {
            gl_piv_par->pre_shift_col = - (image_par->ncolumns - 1);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON 
                                       (gpiv->piveval->spinbutton_preshiftcol),
                                       gl_piv_par->pre_shift_col);
        }

        /*
         * Rows
         */
        if (gl_piv_par->row_start > image_par->nrows - 1) {
            gl_piv_par->row_start = image_par->nrows - 1;
            gtk_spin_button_set_value (GTK_SPIN_BUTTON 
                                       (gpiv->piveval->spinbutton_rowstart),
                                       gl_piv_par->row_start);
        }

        if (gl_piv_par->row_end > image_par->nrows - 1) {
            gl_piv_par->row_end = image_par->nrows - 1;
            gtk_spin_button_set_value (GTK_SPIN_BUTTON 
                                       (gpiv->piveval->spinbutton_rowend),
                                       gl_piv_par->row_end);
        }


        if (gl_piv_par->pre_shift_row > image_par->nrows - 1) {
            gl_piv_par->pre_shift_row = image_par->nrows - 1;
            gtk_spin_button_set_value (GTK_SPIN_BUTTON 
                                       (gpiv->piveval->spinbutton_preshiftrow),
                                       gl_piv_par->pre_shift_row);
        }

        if (gl_piv_par->pre_shift_row < (0.0 - (image_par->nrows - 1))) {
            gl_piv_par->pre_shift_row = - (image_par->nrows - 1);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON 
                                       (gpiv->piveval->spinbutton_preshiftrow),
                                       gl_piv_par->pre_shift_row);
        }

    }
}



gchar *
month_name (GDateMonth month)
/*--------------------------------------------------------------------
 * returns the month name
 */
{
    gchar *name = NULL;

    if (month == G_DATE_JANUARY) {
        name = g_strdup ( _("January"));
    } else if (month == G_DATE_FEBRUARY) {
        name = g_strdup ( _("February"));
    } else if (month == G_DATE_MARCH) {
        name = g_strdup ( _("March"));
    } else if (month == G_DATE_APRIL) {
        name = g_strdup ( _("April"));
    } else if (month == G_DATE_MAY) {
        name = g_strdup ( _("May"));
    } else if (month == G_DATE_JUNE) {
        name = g_strdup ( _("June"));
    } else if (month == G_DATE_JULY) {
        name = g_strdup ( _("July"));
    } else if (month == G_DATE_AUGUST) {
        name = g_strdup ( _("August"));
    } else if (month == G_DATE_SEPTEMBER) {
        name = g_strdup ( _("September"));
    } else if (month == G_DATE_OCTOBER) {
        name = g_strdup ( _("October"));
    } else if (month == G_DATE_NOVEMBER) {
        name = g_strdup ( _("November"));
    } else if (month == G_DATE_DECEMBER) {
        name = g_strdup ( _("December"));
    } else if (month == G_DATE_BAD_MONTH) {
        name = g_strdup ( _("Bad month"));
    } else {
        name = g_strdup ( _("month_name: Unvalid month"));
    }

    return name;
}



void 
free_all_mems (void)
/*--------------------------------------------------------------------
 * Frees all allocated memory data from the application
 */
{
    gint i = 0;

    for (i = 1; i < gpiv_var->number_fnames_last; i++) {
        g_free (gpiv_var->fn_last[i]);
    }
}



void
push_list_lastfnames (gchar *fname
                      )
/*--------------------------------------------------------------------
 * Pushes fname on gpiv_var fn_last[0]
 * Eventually removes fn_last[MAX_LIST]
 */
{
    gchar fname_nr[GPIV_MAX_CHARS];
    gint i;

    gnome_config_push_prefix ("/gpiv/RuntimeVariables/");
    if (gpiv_var->number_fnames_last < MAX_LIST) {
        gpiv_var->number_fnames_last++;
        gnome_config_set_int ("number_fnames_last", 
                              gpiv_var->number_fnames_last);
    }

    for (i = gpiv_var->number_fnames_last - 1; i > 0; i--) {
        gpiv_var->fn_last[i] = g_strdup (gpiv_var->fn_last[i - 1]);
    }

    gpiv_var->fn_last[0] = g_strdup (fname);
    for (i = 0; i < gpiv_var->number_fnames_last; i++) {
        g_snprintf (fname_nr, GPIV_MAX_CHARS,"fname_last_%d", i);
        gnome_config_set_string (fname_nr, gpiv_var->fn_last[i]);
    }
    if (gpiv_par->verbose)
        g_message ("push_list_lastfnames: fn_last[0]=%s", gpiv_var->fn_last[0]);

    gnome_config_pop_prefix ();
    gnome_config_sync ();
}



guint 
get_row_from_id (GpivConsole *gpiv,
                 Display *disp
                 )
/*-----------------------------------------------------------------------------
 * Searches the row of buffer clist that belongs to deplay identity.
 */
{
    guint i, row;
    Display *display_loc = NULL;

 
    for (i = 0; i < nbufs; i++) {
        display_loc = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), i); 
        if (disp->id == display_loc->id) row = i;
    }


    return row;
}



void
point_to_existbuffer (GpivConsole *gpiv
                     )
/*-----------------------------------------------------------------------------
 * Point display_act to first existing buffer
 */
{
    guint i;


    if (nbufs > 0) {
        display_act = gtk_clist_get_row_data (GTK_CLIST (gpiv->clist_buf), 0); 
        gtk_clist_select_row (GTK_CLIST (gpiv->clist_buf), 0, 0);
    }
}



void
close_buffer__check_saved (GpivConsole *gpiv,
                           Display *disp
              )
/*-----------------------------------------------------------------------------
 * closes buffer, checks if (modified) image and data have been saved.
 * If unsaved, close_buffer_dialog will direct to on_close_buffer_response, 
 * found in dialog.c.
 */
{
    char message[2 * GPIV_MAX_CHARS];
    GtkDialog *close_buffer_dialog;
    guint row = get_row_from_id (gpiv, disp);


    if (disp == NULL) return;

#ifdef DEBUG
    g_message ("close_buffer__check_saved:: id = %d saved_img = %d", 
               disp->id, display_act->img->saved_img); 
#endif

    if (!disp->img->saved_img
        || !disp->pida->saved_piv
        || !disp->pida->saved_histo
        || !disp->pida->saved_vor
        || !disp->pida->saved_nstrain
        || !disp->pida->saved_sstrain) {
/*
 * Data not stored, deleting buffer only after permission
 */
        g_snprintf (message, 2 * GPIV_MAX_CHARS,
                    _("There are unsaved data that will be lost.\n\
Are you sure you want to close buffer #%d ?"), 
                    disp->id);
        close_buffer_dialog = create_close_buffer_dialog (gpiv, disp, message);
        gtk_widget_show (GTK_WIDGET (close_buffer_dialog));

    } else {
/*
 * No modified / new data or already stored, deleting buffer unconditionally
 */
        close_buffer (gpiv, disp);

    }
}



void
close_buffer (GpivConsole *gpiv, 
              Display *disp 
              ) 
/*-----------------------------------------------------------------------------
 * closes all data of a display and cleans up clist of buffer
 */
{
    guint row = get_row_from_id (gpiv, disp);

    free_all_bufmems (disp);
    g_free (disp->file_uri_name);
    gtk_widget_destroy (GTK_WIDGET (disp->mwin));
    disp = NULL;

    /*
     * Cleaning up clist
     */
    gtk_clist_remove (GTK_CLIST (gpiv->clist_buf), 
                      row);
    nbufs--;

    /*
     * Point display to an existing buffer
     */
    if (nbufs == 0) {
        update_imgh_entries (gpiv, gl_image_par);

    } else {
        point_to_existbuffer (gpiv);
    }

}



gfloat
image_mean (guint16 **img, 
            gint ncols, 
            gint nrows)
/*-----------------------------------------------------------------------------
 */
{
    int i, j, ncount = ncols * nrows;
    float img_sum = 0.0, mean = 0.0;
    
    for (i = 0; i < nrows; i++) {
        for (j = 0; j < ncols; j++) {
            img_sum += (float) img[i][j];
        }
    }
    
    mean = img_sum / (float) ncount;
    return mean;
}



GnomeCanvasItem
**alloc_gci_matrix (long nr,
                    long nc
                    )
/*-----------------------------------------------------------------------------
 * Allocates 2-dimensional array for GnomeCanvasItem
 */
{
    long i;
    GnomeCanvasItem **item;


    /*
     * allocate pointers to rows
     */
    item = (GnomeCanvasItem **) g_malloc ((size_t) ((nr - 1) *
                                                    sizeof (GnomeCanvasItem*)));
    if (!item) gpiv_error ("%s: allocation failure 1 in g_malloc()", 
                           "alloc_gci_matrix");
    item += 1;
    
    /*
     * allocate rows and set pointers to them
     */
    item[0] = (GnomeCanvasItem *) g_malloc ((size_t) ((nr * nc - 1) *
                                                      sizeof (GnomeCanvasItem)));
    if (!item[0]) gpiv_error ("%s: allocation failure 2 in g_malloc()", 
                              "alloc_gci_matrix");
    item[0] += 1;
    
    for (i = 1; i <= nr; i++) item[i] = item[i-1] + 1;
    
    /*
     * return pointer to array of pointers to rows
     */
    return item;
}



void 
free_gci_matrix (GnomeCanvasItem **item,
                 long nr, 
                 long nc
                 )
/*-----------------------------------------------------------------------------
 * Frees 2-dimensional array for GnomeCanvasItem
 */
{
    assert (item[0] != NULL);

    free ((char*) (item[0] - 1));
    free ((char*) (item - 1));
}



void 
free_all_bufmems (Display * disp)
/*-----------------------------------------------------------------------------
 * Frees all dynamic memory of Buffer structure
 */
{
    if (disp != NULL) {
        free_img_bufmems (disp);
        free_eval_bufmems (disp);
        free_valid_bufmems (disp);
        free_post_bufmems (disp);
    }
}



void 
free_img_bufmems (Display * disp)
/*-----------------------------------------------------------------------------
 * Frees all dynamic memory of Buffer structure of img
 */
{
    /*     char *foo = '\0'; */
    /*     g_snprintf (disp->file_uri_name, GPIV_MAX_CHARS, "");  */

    if (disp != NULL) {
        if (disp->img->exist_img) {
            gdk_pixbuf_unref (disp->img->pixbuf1);
            if (disp->img->rgbbuf_img1 != NULL) {
                g_free (disp->img->rgbbuf_img1);
                disp->img->rgbbuf_img1 = NULL;
            }

            gdk_pixbuf_unref (disp->img->pixbuf2);
            if (disp->img->rgbbuf_img2 != NULL) {
                g_free (disp->img->rgbbuf_img2);
                disp->img->rgbbuf_img2 = NULL;
            }


            gpiv_free_img (disp->img->image);
            destroy_img (disp);
            disp->img->exist_img = FALSE;
        }

/*         destroy_background (disp); */
        destroy_background(display_act->gci_bg);
        if (disp->intreg->exist_int) {
            destroy_all_intregs (disp);
            disp->intreg->exist_int = FALSE;
        }
    }
}



void 
free_eval_bufmems (Display * disp)
/*-----------------------------------------------------------------------------
 * Frees all dynamic memory of Buffer structure of evaluation processing
 */
{
    if (disp != NULL) {
        if (disp->pida->exist_piv) {
            destroy_all_vectors (disp->pida);
            gpiv_free_pivdata (disp->pida->piv_data);
            disp->pida->exist_piv = FALSE;
        }

        if (disp->pida->scaled_piv) {
            gpiv_free_pivdata (disp->pida->piv_data_scaled);
            disp->pida->scaled_piv = FALSE;
        }

        disp->pida->averaged_piv = FALSE;
    }
}


void 
free_valid_bufmems (Display * disp)
/*-----------------------------------------------------------------------------
 * Frees all dynamic memory of Buffer structure of validation processing
 */
{
    if (disp != NULL) {
        if (disp->pida->exist_histo) {
            gpiv_free_bindata (disp->pida->klass);
            disp->pida->exist_valid = FALSE;
        }
    }
}


void 
free_post_bufmems (Display * disp
                   )
/*-----------------------------------------------------------------------------
 * Frees all dynamic memory of Buffer structure of post processing
 */
{
    if (disp != NULL) {
        if (disp->pida->exist_vor) {
            destroy_all_scalars (disp, GPIV_VORTICITY);
            gpiv_free_scdata (disp->pida->vor_data);
            disp->pida->exist_vor = FALSE;
            disp->pida->post_par->diff_type__set = FALSE;
            disp->pida->post_par->operator_vorstra__set = FALSE;
        }

        if (disp->pida->exist_vor_scaled) {
            gpiv_free_scdata (disp->pida->vor_data_scaled);
            disp->pida->exist_vor_scaled = FALSE;
        }

        if (disp->pida->exist_sstrain) {
            destroy_all_scalars (disp, GPIV_S_STRAIN);
            gpiv_free_scdata (disp->pida->sstrain_data);
            disp->pida->exist_sstrain = FALSE;
            disp->pida->post_par->diff_type__set = FALSE;
            disp->pida->post_par->operator_vorstra__set = FALSE;
        }
        
        if (disp->pida->exist_sstrain_scaled) {
            gpiv_free_scdata (disp->pida->sstrain_data_scaled);
            disp->pida->exist_sstrain_scaled = FALSE;
        }
        
        if (disp->pida->exist_nstrain) {
            destroy_all_scalars (disp, GPIV_N_STRAIN);
            gpiv_free_scdata (disp->pida->nstrain_data);
            disp->pida->exist_nstrain = FALSE;
            disp->pida->post_par->diff_type__set = FALSE;
            disp->pida->post_par->operator_vorstra__set = FALSE;
        }

        if (disp->pida->exist_nstrain_scaled) {
            gpiv_free_scdata (disp->pida->nstrain_data_scaled);
            disp->pida->exist_nstrain_scaled = FALSE;
        }
    }
}


/*
 * gtk routines
 */


void
on_widget_leave (GtkContainer * container,
                 GtkDirectionType direction, 
                 gpointer user_data
                 )
/*-----------------------------------------------------------------------------
 * Resets text in application bar to default after poiner leaves 
 * container object
 */
{
    GpivConsole *gpiv = gtk_object_get_data (GTK_OBJECT (container), "gpiv");
    gnome_appbar_push (GNOME_APPBAR (gpiv->appbar), msg_default);
}



void 
sensitive (GpivConsole *gpiv,
           enum WidgetSet wi_set, 
           gboolean sense)
/*-----------------------------------------------------------------------------
 * Changes the sensitivity of a set of witgets
 */
{

#ifdef ENABLE_CAM
    Dac *dac = gpiv->dac;
    Cam *cam = dac->cam;
    Trig *trig =  dac->trig;
#else
#ifdef ENABLE_TRIG
    Dac *dac = gpiv->dac;
    Trig *trig =  dac->trig;
#endif /* ENABLE_TRIG */
#endif /* ENABLE_CAM */
    /*
     * Setting sensitivity of Dac witgets
     */
#ifdef ENABLE_DAC
    if (wi_set == DAC) {
	gtk_widget_set_sensitive (gpiv->tablabel_dac, sense);
	gtk_widget_set_sensitive (gpiv->dac->label_title, sense);
	gtk_widget_set_sensitive (gpiv->dac->label_fname, sense);
	gtk_widget_set_sensitive (gpiv->dac->entry_fname, sense);
    }
#endif /* DAC */

#ifdef ENABLE_TRIG
    if (wi_set == DAC_TRIG) {
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
                                      (gpiv->button_toolbar_trig), 
                                      gpiv_par->process__trig);
	gtk_widget_set_sensitive (gpiv->button_toolbar_trig, sense);
	gtk_widget_set_sensitive (trig->frame_trigger, sense);
	gtk_widget_set_sensitive (trig->radiobutton_mouse_3, sense);
	gtk_widget_set_sensitive (trig->radiobutton_mouse_4, sense);
	gtk_widget_set_sensitive (trig->radiobutton_mouse_5, sense);
	gtk_widget_set_sensitive (trig->radiobutton_mouse_6, sense);
	gtk_widget_set_sensitive (trig->label_trigger_dt, sense);
	gtk_widget_set_sensitive (trig->spinbutton_trigger_dt, sense);
	gtk_widget_set_sensitive (trig->label_trigger_incrdt, sense);
	gtk_widget_set_sensitive (trig->spinbutton_trigger_incrdt, sense);
	gtk_widget_set_sensitive (trig->label_trigger_cap, sense);
	gtk_widget_set_sensitive (trig->spinbutton_trigger_cap, sense);
	gtk_widget_set_sensitive (trig->button_trigger_start, sense);
	gtk_widget_set_sensitive (trig->button_trigger_stop, sense);
    }
#endif /* ENABLE_TRIG */

#ifdef ENABLE_CAM
    if (wi_set == DAC_TIMING) {
	gtk_widget_set_sensitive (trig->frame_trigger, sense);
	gtk_widget_set_sensitive (trig->radiobutton_mouse_1, sense);
	gtk_widget_set_sensitive (trig->radiobutton_mouse_2, sense);
	gtk_widget_set_sensitive (trig->label_trigger_nf, sense);
	gtk_widget_set_sensitive (trig->spinbutton_trigger_nf, sense);
    }
#else
#ifdef ENABLE_TRIG
    if (wi_set == DAC_TIMING) {
	gtk_widget_set_sensitive (trig->frame_trigger, sense);
	gtk_widget_set_sensitive (trig->radiobutton_mouse_1, sense);
	gtk_widget_set_sensitive (trig->radiobutton_mouse_2, sense);
	gtk_widget_set_sensitive (trig->label_trigger_nf, sense);
	gtk_widget_set_sensitive (trig->spinbutton_trigger_nf, sense);
    }
#endif /* ENABLE_TRIG */
#endif /* ENABLE_CAM */


#ifdef ENABLE_CAM
    if (wi_set == DAC_CAM) {
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
                                      (gpiv->button_toolbar_cam), 
                                      gpiv_par->process__cam);
	gtk_widget_set_sensitive (gpiv->button_toolbar_cam, sense);
	gtk_widget_set_sensitive (cam->frame_cam, sense);
	gtk_widget_set_sensitive (cam->camera_select, sense);
	gtk_widget_set_sensitive (cam->camera_select_menu, sense);
	gtk_widget_set_sensitive (cam->menu_item, sense);
	gtk_widget_set_sensitive (cam->label_temp, sense);
	gtk_widget_set_sensitive (cam->label_label_temp, sense);
	gtk_widget_set_sensitive (cam->button_cam_start, sense);
	gtk_widget_set_sensitive (cam->button_cam_stop, sense);
    }
#endif /* ENABLE_CAM */

    /*
     * Setting sensitivity of Image witgets
     */
    if (wi_set == IMG) {

        /* 	gtk_widget_set_sensitive (gpiv->imgh->label_name, sense); */
        /* 	gtk_widget_set_sensitive (gpiv->imgh->entry_name, sense); */
        /* 	gtk_widget_set_sensitive (gpiv->imgh->spinbutton_ncols, sense); */

	gtk_widget_set_sensitive (gpiv->imgh->label_colpos, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_sscale, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_ncols, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_nrows, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_depth, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_rowpos, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_tscale, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_imgtitle, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_crdate, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_location, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_author, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_software, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_source, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_usertext, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_warning, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_disclaimer, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_comment, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_copyright, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_email, sense);
	gtk_widget_set_sensitive (gpiv->imgh->label_url, sense);

	gtk_widget_set_sensitive (gpiv->imgh->spinbutton_colpos, sense);
	gtk_widget_set_sensitive (gpiv->imgh->spinbutton_sscale, sense);
	gtk_widget_set_sensitive (gpiv->imgh->spinbutton_rowpos, sense);
	gtk_widget_set_sensitive (gpiv->imgh->spinbutton_tscale, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_imgtitle, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_crdate, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_location, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_author, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_software, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_source, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_usertext, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_warning, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_disclaimer, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_comment, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_copyright, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_email, sense);
	gtk_widget_set_sensitive (gpiv->imgh->entry_url, sense);
    }


    /*
     * Setting sensitivity of Image Processing witgets
     */
    if (wi_set == IMGPROC) {
    }


    /*
     * Setting sensitivity of PIV witgets
     */
    if (wi_set == EVAL) {
	gtk_widget_set_sensitive (gpiv->piveval->spinbutton_colstart, sense);
 	gtk_widget_set_sensitive (gpiv->piveval->spinbutton_colend, sense);
	gtk_widget_set_sensitive (gpiv->piveval->spinbutton_preshiftcol, sense);
	gtk_widget_set_sensitive (gpiv->piveval->spinbutton_rowstart, sense);
	gtk_widget_set_sensitive (gpiv->piveval->spinbutton_rowend, sense);
	gtk_widget_set_sensitive (gpiv->piveval->spinbutton_preshiftrow, sense);

        /* 	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_f_1, sense); */
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_f_2, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_f_3, sense);

	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_f_4, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_f_5, sense);

        /* 	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_i_1, sense); */
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_i_2, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_i_3, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_i_4, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intsize_i_5, sense);

	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intshift_1, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intshift_2, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intshift_3, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intshift_4, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_intshift_5, sense);

	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_fit_none, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_fit_gauss, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_fit_power, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_fit_gravity, sense);

	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_mouse_1, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_mouse_2, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_mouse_3, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_mouse_4, sense);

	gtk_widget_set_sensitive (gpiv->piveval->checkbutton_monitor, sense);

	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_peak_1, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_peak_2, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_peak_3, sense);

	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_weightkernel, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_zerooff, sense);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_centraldiff, sense);

	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_cross_1, FALSE);
	gtk_widget_set_sensitive (gpiv->piveval->radiobutton_cross_2, FALSE);

	gtk_widget_set_sensitive (gpiv->piveval->button, sense);
    }


    /*
     * Setting sensitivity of a slection of PIV witgets
     */
    if (wi_set == INTREGS) {
        /* 	if (piv_par->int_size_f >= 16) { */
        /* 	    gtk_widget_set_sensitive (GTK_WIDGET */
        /* 				     (gpiv->piveval->radiobutton_intsize_i_1), sense); */
        if (gl_piv_par->int_size_f >= 32) {
            gtk_widget_set_sensitive (GTK_WIDGET
                                      (gpiv->piveval->radiobutton_intsize_i_2),
                                      sense);
            if (gl_piv_par->int_size_f >= 64) {
                gtk_widget_set_sensitive (GTK_WIDGET
                                          (gpiv->piveval->radiobutton_intsize_i_3),
                                          sense);
                if (gl_piv_par->int_size_f >= 128) {
                    gtk_widget_set_sensitive (GTK_WIDGET
                                              (gpiv->piveval->radiobutton_intsize_i_4),
                                              sense);
                }
            }
        }
        /* 	} */
    }



    /*
     * Setting sensitivity of Validation witgets
     */
    if (wi_set == VALID) {
	gtk_widget_set_sensitive (gpiv->pivvalid->radiobutton_errvec_residu_snr, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->radiobutton_errvec_residu_median, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->radiobutton_errvec_residu_normmedian, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->radiobutton_disable_1, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->label_errvec_res, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->spinbutton_errvec_res, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->checkbutton_errvec_disres, sense);
        gtk_widget_set_sensitive (gpiv->pivvalid->button_errvec_resstats, sense);

	gtk_widget_set_sensitive (gpiv->pivvalid->radiobutton_errvec_subst_1, sense);
        /* 	gtk_widget_set_sensitive (gpiv->pivvalid->radiobutton_errvec_subst_2, sense); */
	gtk_widget_set_sensitive (gpiv->pivvalid->radiobutton_errvec_subst_3, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->button_errvec, sense);

	gtk_widget_set_sensitive (gpiv->pivvalid->spinbutton_histo_bins, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->button_peaklck, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->button_uhisto, sense);
	gtk_widget_set_sensitive (gpiv->pivvalid->button_vhisto, sense);
    }


    /*
     * Setting sensitivity of Post processing witgets
     */
    if (wi_set == POST) {
	gtk_widget_set_sensitive (gpiv->pivpost->radiobutton_vorstra_output_1, sense);
	gtk_widget_set_sensitive (gpiv->pivpost->radiobutton_vorstra_output_2, sense);
	gtk_widget_set_sensitive (gpiv->pivpost->radiobutton_vorstra_output_3, sense);

	gtk_widget_set_sensitive (gpiv->pivpost->radiobutton_vorstra_diffscheme_1,
                                  sense);
	gtk_widget_set_sensitive (gpiv->pivpost->radiobutton_vorstra_diffscheme_2,
                                  sense);
	gtk_widget_set_sensitive (gpiv->pivpost->radiobutton_vorstra_diffscheme_3,
                                  sense);
	gtk_widget_set_sensitive (gpiv->pivpost->radiobutton_vorstra_diffscheme_4,
                                  sense);
	gtk_widget_set_sensitive (gpiv->pivpost->button_vorstra, sense);
    }

}



gint
on_my_popup_handler (GtkWidget *widget, 
                     GdkEvent *event)
/*-----------------------------------------------------------------------------
 * Callback function for pop-up menus
 */
 {
   GtkMenu *menu;
   GdkEventButton *event_button;

   g_return_val_if_fail (widget != NULL, FALSE);
   g_return_val_if_fail (GTK_IS_MENU (widget), FALSE);
   g_return_val_if_fail (event != NULL, FALSE);

   /* The "widget" is the menu that was supplied when 
    * gtk_signal_connect_object was called.
    */
   menu = GTK_MENU (widget);

   if (event->type == GDK_BUTTON_PRESS)
     {
       event_button = (GdkEventButton *) event;
       if (event_button->button == 3)
         {
           gtk_menu_popup (menu, NULL, NULL, NULL, NULL, 
                           event_button->button, event_button->time);
           return TRUE;
         }
     }

   return FALSE;
 }



gchar *
replace_home_dir_with_tilde (const gchar *uri)
/*-----------------------------------------------------------------------------
 * I stole this from gedit
 * return value will have to be freed
 */
{
	gchar *tmp;
	gchar *home;

	g_return_val_if_fail (uri != NULL, NULL);

	/* Note that g_get_home_dir returns a const string */
	tmp = (gchar *)g_get_home_dir ();

	if (tmp == NULL)
		return g_strdup (uri);

	home = g_filename_to_utf8 (tmp, -1, NULL, NULL, NULL);
	if (home == NULL)
		return g_strdup (uri);

	if (strcmp (uri, home) == 0) {
		g_free (home);
		
		return g_strdup ("~");
	}

	tmp = home;
	home = g_strdup_printf ("%s/", tmp);
	g_free (tmp);

	if (g_str_has_prefix (uri, home))
	{
		gchar *res;

		res = g_strdup_printf ("~/%s", uri + strlen (home));

		g_free (home);
		
		return res;		
	}

	g_free (home);

	return g_strdup (uri);
}



gchar *
replace_tilde_with_home_dir (const gchar *uri)
/*-----------------------------------------------------------------------------
 * And wrote this by myself :) (What a genius that guy)
 */
{
    gchar *tmp;
    gchar *home;
    gchar* home_name;

    g_return_val_if_fail (uri != NULL, NULL);

    if ((tmp =(gchar *)g_get_home_dir ()) == NULL) {
        return g_strdup (uri);
    }

    if ((home  = g_filename_to_utf8 (tmp, -1, NULL, NULL, NULL))  == NULL) {
        g_free (tmp);
        return g_strdup (uri);
    }
    
    if (g_str_has_prefix (uri, "~/")) {
        home_name = g_strdup_printf ("%s/%s", home, uri + strlen ("~/"));
    } else {
        g_free (home);
        return g_strdup (uri);
    }

    g_free (home);
    return (home_name);
}


void 
destroy (GtkWidget * widget, 
         gpointer data)
/*-----------------------------------------------------------------------------
 * Destroys widget
 */
{
    gtk_widget_destroy (widget);
}



void message_gpiv (gchar * msg, ...)
/*-----------------------------------------------------------------------------
 * Message box with warning
 */
{
    /* GtkWidget */ GtkDialog *gtk_message_wi;
    va_list args;
    char local_msg[3 * GPIV_MAX_CHARS];

    va_start (args, msg);
    g_vsnprintf (local_msg, 3 * GPIV_MAX_CHARS, msg, args);
    gtk_message_wi = create_message_dialog (local_msg);
    gtk_widget_show (GTK_WIDGET (gtk_message_wi));
    /*     gtk_widget_show_all (gtk_message_wi); */
    va_end (args);
}



void warning_gpiv (gchar * msg, ...)
/*-----------------------------------------------------------------------------
 * Message box with warning
 */
{
    GtkDialog *gtk_warning_wi;
    va_list args;
    char lo_msg[3 * GPIV_MAX_CHARS];

    va_start (args, msg);
    g_vsnprintf (lo_msg, 3 * GPIV_MAX_CHARS, msg, args);
    gtk_warning_wi = create_message_dialog (lo_msg);
    gtk_widget_show (GTK_WIDGET (gtk_warning_wi));
    va_end (args);
}



void error_gpiv (gchar * msg, ...)
/*-----------------------------------------------------------------------------
 * Message box with error
 */
{
    GtkDialog *gtk_error_wi;
    va_list args;
    char lo_msg[2 * GPIV_MAX_CHARS];

    va_start (args, msg);
    g_vsnprintf (lo_msg, 2 * GPIV_MAX_CHARS, msg, args);
    gtk_error_wi = create_error_dialog (lo_msg);
    gtk_widget_show (GTK_WIDGET (gtk_error_wi));
    va_end (args);
}
