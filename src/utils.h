/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: utils.h,v $
 * Revision 1.13  2007-12-19 08:42:35  gerber
 * debugged
 *
 * Revision 1.12  2007-11-23 16:24:08  gerber
 * release 0.5.0: Kafka
 *
 * Revision 1.11  2007-06-06 17:00:48  gerber
 * Retreives images/data from URI using Gnome Virtual File System.
 *
 * Revision 1.10  2007-01-29 11:27:44  gerber
 * added image formats png, gif, tif png, bmp, improved buffer display
 *
 * Revision 1.9  2006/01/31 14:28:13  gerber
 * version 0.3.0
 *
 * Revision 1.8  2005/01/19 15:53:43  gerber
 * Initiation of Data Acquisition (DAC); trigerring of lasers and camera
 * by using RTAI and Realtime Linux, recording images from IEEE1394
 * (Firewire) IIDC compliant camera's
 *
 * Revision 1.7  2004/10/15 19:24:05  gerber
 * GPIV_ and Gpiv prefix to defines and structure names of libgpiv
 *
 * Revision 1.6  2004/06/14 21:19:23  gerber
 * Image depth up to 16 bits.
 * Improvement "single int" and "drag int" in Eval tab.
 * Viewer's pop-up menu.
 * Adaption for gpiv_matrix_* and gpiv_vector_*.
 * Resizing console.
 * See Changelog for further info.
 *
 * Revision 1.5  2003/09/04 13:31:55  gerber
 * init of printing (unfinished)
 *
 * Revision 1.4  2003/09/01 11:17:15  gerber
 * improved monitoring of interrogation process
 *
 * Revision 1.3  2003/08/22 15:24:53  gerber
 * interactive spatial scaling
 *
 * Revision 1.2  2003/06/27 13:47:26  gerber
 * display ruler, line/point evaluation
 *
 * Revision 1.1.1.1  2003/06/17 17:10:52  gerber
 * Imported gpiv
 *
 */


#ifndef UTILS_H
#define UTILS_H


void
update_imgh_entries (GpivConsole *gpiv,
                     GpivImagePar *image_par);
void
update_eval_entries (GpivConsole *gpiv,
                     GpivImagePar *image_par);

gchar *
month_name(GDateMonth month);
/*--------------------------------------------------------------------
 * returns the month name
 */

void
free_all_mems(void);

void
push_list_lastfnames(gchar *fname);

guint 
get_row_from_id (GpivConsole *gpiv, 
                 Display *disp);

void
point_to_existbuffer (GpivConsole *gpiv);

void
close_buffer__check_saved (GpivConsole *gpiv,
                           Display *disp);

void
close_buffer (GpivConsole *gpiv, 
              Display *disp);


gfloat
image_mean (guint16 **img, 
            gint ncols, 
            gint nrows);

/*
 * mem allocation functions
 */

/*--------------------------------------------------------------------
 Allocates 2-dimensional array for GnomeCanvasItem */
GnomeCanvasItem
**alloc_gci_matrix (long nr, 
                    long nc);

/*--------------------------------------------------------------------
 Frees 2-dimensional array for GnomeCanvasItem */
void 
free_gci_matrix (GnomeCanvasItem **item,
                 long nr, 
                 long nc);

void 
free_all_bufmems (Display *disp);

void 
free_img_bufmems (Display *disp);

void 
free_eval_bufmems (Display *disp);

void 
free_valid_bufmems (Display *disp);

void 
free_post_bufmems (Display *disp);

/*
 * general gtk functions
 */
void 
on_widget_leave (GtkContainer *container,
                GtkDirectionType direction,
                gpointer user_data);

void 
sensitive (GpivConsole *gpiv,
          enum WidgetSet wi_set, 
          gboolean sense);

gint
on_my_popup_handler (GtkWidget *widget, 
                     GdkEvent *event);

gchar *
replace_home_dir_with_tilde (const gchar *uri);

gchar *
replace_tilde_with_home_dir (const gchar *uri);

void 
destroy (GtkWidget *widget, 
        gpointer data);

void 
message_gpiv (gchar *msg, ...);

void 
warning_gpiv (gchar *msg, ...);

void 
error_gpiv (gchar *msg, ...);



#endif /* UTILS_H */
