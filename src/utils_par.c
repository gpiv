/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*----------------------------------------------------------------------
 
  gpiv - Graphic program for Particle Image Velocimetry, based on gtk/gnome
          libraries.

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of gpiv.

   Gpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * utility functions for gpiv
 * $Log: utils_par.c,v $
 * Revision 1.2  2008-10-09 14:43:37  gerber
 * paralellized with OMP and MPI
 *
 * Revision 1.1  2008-09-16 11:24:37  gerber
 * added utils_par
 *
 *
 */

#include "gpiv_gui.h"
#include "utils_par.h"


/*
 * Prottyping
 */
static void
set_par_bool_ifdiff (gboolean *val_src,
		     gboolean *val_dest,
                     gboolean *set_dest);

static void
set_par_int_ifdiff (gint *val_src,
		    gint *val_dest,
		    gboolean *set_dest);

static void
print_par_bool (gchar *parname, 
                gboolean val, 
                gboolean set);

static void
print_par_int (gchar *parname, 
               gint val, 
               gboolean set);

static void
par_ovwrt_parameters (Par *par_src, 
                      Par *par_dest, 
                      gboolean force);


/*
 * Program-wide public functions
 */
void
parameters_set (Par *par,
                gboolean bool
                )
/*-----------------------------------------------------------------------------
 * Defines each parameter member of par_dest->*__set to bool
 */
{

    par->img_fmt__set = bool;
    par->hdf__set = bool;
    par->print_par__set = bool;
    par->verbose__set = bool;
    par->x_corr__set = bool;
    par->console__show_tooltips__set = bool;
    par->console__view_tabulator__set = bool;
    par->console__view_gpivbuttons__set = bool;
    par->console__nbins__set = bool;
    
#ifdef ENABLE_CAM
    par->process__cam = bool; 
#endif /* ENABLE_CAM */
    
#ifdef ENABLE_TRIG
    par->process__trig = bool; 
#endif /* ENABLE_DAC */
    
#ifdef ENABLE_IMGPROC
    par->process__imgproc = bool; 
#endif /* ENABLE_IMGPROC */
    
    par->process__piv = bool; 
    par->process__gradient = bool; 
    par->process__resstats = bool; 
    par->process__errvec = bool; 
    par->process__peaklock = bool; 
    par->process__scale = bool;  
    par->process__average = bool;    
    par->process__subtract = bool;    
    par->process__vorstra = bool;    
    par->display__view_menubar = bool;   
    par->display__view_rulers = bool;     
    par->display__stretch_auto = bool;     
    par->display__vector_scale = bool;     
    par->display__vector_color = bool;     
    par->display__zoom_index = bool;     
    par->display__backgrnd__set = bool;     
    par->display__intregs = bool;    
    par->display__piv = bool;    
    par->display__scalar__set = bool; 

#ifdef ENABLE_MPI
    par->mpi_nodes__set = bool;
#endif /* ENABLE_MPI */

}



void
set_parameters_ifdiff (Par *par_src,
		       Par *par_dest
		       )
/*-----------------------------------------------------------------------------
 * Defines each parameter member of par_dest->*__set to TRUE if par_src differs
 */
{

  set_par_int_ifdiff ((int *) &par_src->img_fmt, 
		      (int *) &par_dest->img_fmt, 
		      &par_dest->img_fmt__set);

  set_par_bool_ifdiff (&par_src->hdf, 
		       &par_dest->hdf, 
		       &par_dest->hdf__set);

  set_par_bool_ifdiff (&par_src->print_par, 
		       &par_dest->print_par, 
		       &par_dest->print_par__set);

  set_par_bool_ifdiff (&par_src->verbose, 
		       &par_dest->verbose, 
		       &par_dest->verbose__set);

  set_par_bool_ifdiff (&par_src->x_corr, 
		       &par_dest->x_corr, 
		       &par_dest->x_corr__set);

  set_par_bool_ifdiff (&par_src->console__show_tooltips, 
		       &par_dest->console__show_tooltips, 
		       &par_dest->console__show_tooltips__set);

  set_par_bool_ifdiff (&par_src->console__view_tabulator, 
		       &par_dest->console__view_tabulator, 
		       &par_dest->console__view_tabulator__set);

  set_par_bool_ifdiff (&par_src->console__view_gpivbuttons, 
		       &par_dest->console__view_gpivbuttons, 
		       &par_dest->console__view_gpivbuttons__set);

  set_par_int_ifdiff (&par_src->console__nbins, 
		      &par_dest->console__nbins, 
		      &par_dest->console__nbins__set);
    
#ifdef ENABLE_CAM
  set_par_bool_ifdiff (&par_src->process__cam, 
		       &par_dest->process__cam, 
		       &par_dest->process__cam__set);
#endif /* ENABLE_CAM */
    
#ifdef ENABLE_TRIG
  set_par_bool_ifdiff (&par_src->process__trig, 
		       &par_dest->process__trig, 
		       &par_dest->process__trig__set);
#endif /* ENABLE_DAC */
    
#ifdef ENABLE_IMGPROC
  set_par_bool_ifdiff (&par_src->process__imgproc, 
		       &par_dest->process__imgproc, 
		       &par_dest->process__imgproc__set);
#endif /* ENABLE_IMGPROC */
    
  set_par_bool_ifdiff (&par_src->process__piv, 
		       &par_dest->process__piv, 
		       &par_dest->process__piv__set);

  set_par_bool_ifdiff (&par_src->process__gradient, 
		       &par_dest->process__gradient, 
		       &par_dest->process__gradient__set);

  set_par_bool_ifdiff (&par_src->process__resstats, 
		       &par_dest->process__resstats, 
		       &par_dest->process__resstats__set);

  set_par_bool_ifdiff (&par_src->process__errvec, 
		       &par_dest->process__errvec, 
		       &par_dest->process__errvec__set);

  set_par_bool_ifdiff (&par_src->process__peaklock, 
		       &par_dest->process__peaklock, 
		       &par_dest->process__peaklock__set);

    
  set_par_bool_ifdiff (&par_src->process__scale, 
		       &par_dest->process__scale, 
		       &par_dest->process__scale__set);

    
  set_par_bool_ifdiff (&par_src->process__average, 
		       &par_dest->process__average, 
		       &par_dest->process__average__set);

    
  set_par_bool_ifdiff (&par_src->process__subtract, 
		       &par_dest->process__subtract, 
		       &par_dest->process__subtract__set);

    
  set_par_bool_ifdiff (&par_src->process__vorstra, 
		       &par_dest->process__vorstra, 
		       &par_dest->process__vorstra__set);

    
  set_par_bool_ifdiff (&par_src->display__view_menubar, 
		       &par_dest->display__view_menubar, 
		       &par_dest->display__view_menubar__set);

    
  set_par_bool_ifdiff (&par_src->display__view_rulers, 
		       &par_dest->display__view_rulers, 
		       &par_dest->display__view_rulers__set);

    
  set_par_bool_ifdiff (&par_src->display__stretch_auto, 
		       &par_dest->display__stretch_auto, 
		       &par_dest->display__stretch_auto__set);

    
  set_par_int_ifdiff ((int *) &par_src->display__vector_scale, 
		      (int *) &par_dest->display__vector_scale, 
		      &par_dest->display__vector_scale__set);

    
  set_par_int_ifdiff ((int *) &par_src->display__vector_color, 
		      (int *) &par_dest->display__vector_color, 
		      &par_dest->display__vector_color__set);

    
  set_par_int_ifdiff ((int *) &par_src->display__zoom_index, 
		      (int *) &par_dest->display__zoom_index, 
		      &par_dest->display__zoom_index__set);

    
  set_par_int_ifdiff ((int *) &par_src->display__backgrnd, 
		      (int *) &par_dest->display__backgrnd, 
		      &par_dest->display__backgrnd__set);

    
  set_par_bool_ifdiff (&par_src->display__intregs, 
		       &par_dest->display__intregs, 
		       &par_dest->display__intregs__set);

    
  set_par_bool_ifdiff (&par_src->display__piv, 
		       &par_dest->display__piv, 
		       &par_dest->display__piv__set);

    
  set_par_int_ifdiff ((int *) &par_src->display__scalar, 
                      (int *) &par_dest->display__scalar, 
                      &par_dest->display__scalar__set);

#ifdef ENABLE_MPI
  set_par_int_ifdiff (&par_src->mpi_nodes, 
		      &par_dest->mpi_nodes, 
		      &par_dest->mpi_nodes__set);
#endif /* ENABLE_MPI */

}



void
print_parameters (Par *par
                  )
/*-----------------------------------------------------------------------------
 */
{

    print_par_int ("img_fmt", par->img_fmt, 
                   par->img_fmt__set);

    print_par_bool ("hdf", par->hdf, 
                    par->hdf__set);

    print_par_bool ("print_par", par->print_par, 
                    par->print_par__set);

    print_par_bool ("verbose", par->verbose, 
                    par->verbose__set);

    print_par_bool ("x_corr", par->x_corr, 
                    par->x_corr__set);

    print_par_bool ("console__show_tooltips", par->console__show_tooltips, 
                    par->console__show_tooltips__set);

    print_par_bool ("console__view_tabulator", par->console__view_tabulator, 
                    par->console__view_tabulator__set);

    print_par_bool ("console__view_gpivbuttons", par->console__view_gpivbuttons, 
                    par->console__view_gpivbuttons__set);

    print_par_int ("console__nbins", par->console__nbins, 
                   par->console__nbins__set);
    
#ifdef ENABLE_CAM
    print_par_bool ("process__cam", par->process__cam, 
                    par->process__cam__set);
#endif /* ENABLE_CAM */
    
#ifdef ENABLE_TRIG
    print_par_bool ("process__trig", par->process__trig, 
                    par->process__trig__set);
#endif /* ENABLE_DAC */
    
#ifdef ENABLE_IMGPROC
    print_par_bool ("process__imgproc", par->process__imgproc, 
                    par->process__imgproc__set);
#endif /* ENABLE_IMGPROC */
    
    print_par_bool ("process__piv", par->process__piv, 
                    par->process__piv__set);

    print_par_bool ("process__gradient", par->process__gradient, 
                    par->process__gradient__set);

    print_par_bool ("process__resstats", par->process__resstats, 
                    par->process__resstats__set);

    print_par_bool ("process__errvec", par->process__errvec, 
                    par->process__errvec__set);

    print_par_bool ("process__peaklock", par->process__peaklock, 
                    par->process__peaklock__set);

    
    print_par_bool ("process__scale", par->process__scale, 
                    par->process__scale__set);

    
    print_par_bool ("process__average", par->process__average, 
                    par->process__average__set);

    
    print_par_bool ("process__subtract", par->process__subtract, 
                    par->process__subtract__set);

    
    print_par_bool ("process__vorstra", par->process__vorstra, 
                    par->process__vorstra__set);

    
    print_par_bool ("display__view_menubar", par->display__view_menubar, 
                    par->display__view_menubar__set);

    
    print_par_bool ("display__view_rulers", par->display__view_rulers, 
                    par->display__view_rulers__set);

    
    print_par_bool ("display__stretch_auto", par->display__stretch_auto, 
                    par->display__stretch_auto__set);

    
    print_par_bool ("display__vector_scale", par->display__vector_scale, 
                    par->display__vector_scale__set);

    
    print_par_bool ("display__vector_color", par->display__vector_color, 
                    par->display__vector_color__set);

    
    print_par_bool ("display__zoom_index", par->display__zoom_index, 
                    par->display__zoom_index__set);

    
    print_par_bool ("display__background", par->display__backgrnd, 
                    par->display__backgrnd__set);

    
    print_par_bool ("display__intregs", par->display__intregs, 
                    par->display__intregs__set);

    
    print_par_bool ("display__piv", par->display__piv, 
                    par->display__piv__set);

    
    print_par_bool ("display__scalar", par->display__scalar, 
                    par->display__scalar__set);

#ifdef ENABLE_MPI
    print_par_int ("mpi_nodes", par->mpi_nodes, 
                   par->mpi_nodes__set);

#endif /* ENABLE_MPI */    
}



gchar *
cp_undef_parameters (Par *par_src,
                     Par *par_dest
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Copies only those gpiv parameters from src to dest if dest has not 
 *      been defined
 *
 * INPUTS:
 *      par_src:                source gpiv parameters
 *      force:                  flag to force the copying, even if destination
 *                              already exists
 *      print_par:              verbose output. Not implemented, yet.
 *
 * OUTPUTS:
 *      par_dest:               destination piv parameters
 *
 * RETURNS:
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gboolean force = FALSE;


    par_ovwrt_parameters (par_src, par_dest, force);


    return err_msg;
}



Par *
cp_parameters (Par *par_src
               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Copies all gpiv parameters
 *
 * INPUTS:
 *      par_src:                source gpiv parameters
 *      force:                  flag to force the copying, even if destination
 *                              already exists
 *      print_par:              verbose output. Not implemented, yet.
 *
 * OUTPUTS:
 *      par_dest:               destination piv parameters
 *
 * RETURNS:
 *
 *---------------------------------------------------------------------------*/
{
    Par *par_dest = g_new0 (Par, 1);
    gboolean force = TRUE;


    par_ovwrt_parameters (par_src, par_dest, force);


    return par_dest;
}


/*
 * Local functions
 */

static void
set_par_bool_ifdiff (gboolean *val_src,
		     gboolean *val_dest,
                     gboolean *set_dest)
/*-----------------------------------------------------------------------------
 */
{
  if (*val_src != *val_dest) {
    *val_dest = *val_src;
    *set_dest = TRUE;
  } else {
    *set_dest = FALSE;
  }

}


static void
set_par_int_ifdiff (gint *val_src,
		    gint *val_dest,
		    gboolean *set_dest)
/*-----------------------------------------------------------------------------
 */
{
  if (*val_src != *val_dest) {
    *val_dest = *val_src;
    *set_dest = TRUE;
  } else {
    *set_dest = FALSE;
  }

}


static void
print_par_bool (gchar *parname, 
                gboolean val, 
                gboolean set)
/*-----------------------------------------------------------------------------
 */
{
    if (set) {
        g_message ("%s__set is TRUE", parname);
    } else {
        g_message ("%s__set is FALSE", parname);
    }

    if (val) {
        g_message ("%s is set to: TRUE", parname);
    } else {
        g_message ("%s is set to: FALSE", parname);
    }
}


static void
print_par_int (gchar *parname, 
               gint val, 
               gboolean set)
/*-----------------------------------------------------------------------------
 */
{
    if (set) {
        g_message ("%s__set is TRUE", parname);
    } else {
        g_message ("%s__set is FALSE", parname);
    }

    g_message ("%s is set to: %d", parname, val);
}


static void
par_ovwrt_parameters (Par *par_src, 
                      Par *par_dest, 
                      gboolean force
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Copies gpiv parameters
 *
 * INPUTS:
 *      par_src:                source gpiv parameters
 *      par_dest:               destination gpiv parameters
 *      force:                  flag to force the copying, even if destination
 *                              already exists
 *
 * OUTPUTS:
 *      par_dest:               destination piv parameters
 *
 * RETURNS:
 *
 *---------------------------------------------------------------------------*/
{

    if (force 
        || ( par_src->img_fmt__set 
             && !par_dest->img_fmt__set)) {
        par_dest->img_fmt = par_src->img_fmt;
        par_dest->img_fmt__set = TRUE;
    }
    
    if (force 
        || ( par_src->hdf__set 
             && !par_dest->hdf__set)) {
        par_dest->hdf = par_src->hdf;
        par_dest->hdf__set = TRUE;
    }
    
    if (force 
        || ( par_src->print_par__set 
             && !par_dest->print_par__set)) {
        par_dest->print_par = par_src->print_par;
        par_dest->print_par__set = TRUE;
    }
    
    if (force 
        || ( par_src->verbose__set 
             && !par_dest->verbose__set)) {
        par_dest->verbose = par_src->verbose;
        par_dest->verbose__set = TRUE;
    }
    
    if (force 
        || ( par_src->x_corr__set 
             && !par_dest->x_corr__set)) {
        par_dest->x_corr = par_src->x_corr;
        par_dest->x_corr__set = TRUE;
    }

    if (force 
        || ( par_src->console__show_tooltips__set 
             && !par_dest->console__show_tooltips__set)) {
        par_dest->console__show_tooltips = par_src->console__show_tooltips;
        par_dest->console__show_tooltips__set = TRUE;
    }
    
    if (force 
        || ( par_src->console__view_tabulator__set 
             && !par_dest->console__view_tabulator__set)) {
        par_dest->console__view_tabulator = par_src->console__view_tabulator;
        par_dest->console__view_tabulator__set = TRUE;
    }
    
    if (force 
        || ( par_src->console__view_gpivbuttons__set 
             && !par_dest->console__view_gpivbuttons__set)) {
        par_dest->console__view_gpivbuttons = par_src->console__view_gpivbuttons;
        par_dest->console__view_gpivbuttons__set = TRUE;
    }
    
    if (force 
        || (  par_src->console__nbins__set 
              && !par_dest->console__nbins__set)) {
        par_dest->console__nbins = par_src->console__nbins;
        par_dest->console__nbins__set = TRUE;
    }

#ifdef ENABLE_CAM
    if (force 
        || ( par_src->process__cam__set 
             && !par_dest->process__cam__set)) {
        par_dest->process__cam = par_src->process__cam;
        par_dest->process__cam__set = TRUE;
    }
#endif /* ENABLE_CAM */
    
#ifdef ENABLE_TRIG
    if (force 
        || ( par_src->process__trig__set 
             && !par_dest->process__trig__set)) {
        par_dest->process__trig = par_src->process__trig;
        par_dest->process__trig__set = TRUE;
    }
#endif /* ENABLE_DAC */
    
#ifdef ENABLE_IMGPROC
    if (force 
        || ( par_src->process__imgproc__set 
             && !par_dest->process__imgproc__set)) {
        par_dest->process__piv = par_src->process__piv;
        par_dest->process__imgproc__set = TRUE;
    }
#endif /* ENABLE_IMGPROC */
    
    if (force 
        || ( par_src->process__piv__set 
             && !par_dest->process__piv__set)) {
        par_dest->process__piv = par_src->process__piv;
        par_dest->process__piv__set = TRUE;
    }
    
    if (force 
        || ( par_src->process__gradient__set 
             && !par_dest->process__gradient__set)) {
        par_dest->process__gradient = par_src->process__gradient;
        par_dest->process__gradient__set = TRUE;
    }
    
    if (force 
        || ( par_src->process__resstats__set 
             && !par_dest->process__resstats__set)) {
        par_src->process__resstats = par_dest->process__resstats;
        par_dest->process__resstats__set = TRUE;
    }
    
    if (force 
        || ( par_src->process__errvec__set 
             && !par_dest->process__errvec__set)) {
        par_dest->process__errvec = par_src->process__errvec;
        par_dest->process__errvec__set = TRUE;
    }
    
    if (force 
        || ( par_src->process__peaklock__set 
             && !par_dest->process__peaklock__set)) {
        par_dest->process__peaklock = par_src->process__peaklock;
        par_dest->process__peaklock__set = TRUE;
    }
    
    if (force 
        || ( par_src->process__scale__set 
             && !par_dest->process__scale__set)) {
        par_dest->process__scale = par_src->process__scale;
        par_dest->process__scale__set = TRUE;
    }
    
    if (force 
        || ( par_src->process__average__set 
             && !par_dest->process__average__set)) {
        par_dest->process__average = par_src->process__average;
        par_dest->process__average__set = TRUE;
    }
    
    if (force 
        || ( par_src->process__subtract__set 
             && !par_dest->process__subtract__set)) {
        par_dest->process__subtract = par_src->process__subtract;
        par_dest->process__subtract__set = TRUE;
    }
    
    if (force 
        || ( par_src->process__vorstra__set 
             && !par_dest->process__vorstra__set)) {
        par_dest->process__vorstra = par_src->process__vorstra;
        par_dest->process__vorstra__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__view_menubar__set 
             && !par_dest->display__view_menubar__set)) {
        par_dest->display__view_menubar = par_src->display__view_menubar;
        par_dest->display__view_menubar__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__view_rulers__set 
             && !par_dest->display__view_rulers__set)) {
        par_dest->display__view_rulers = par_src->display__view_rulers;
        par_dest->display__view_rulers__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__stretch_auto__set 
             && !par_dest->display__stretch_auto__set)) {
        par_dest->display__view_rulers = par_src->display__view_rulers;
        par_dest->display__stretch_auto__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__vector_scale__set 
             && !par_dest->display__vector_scale__set)) {
        par_dest->display__vector_scale = par_src->display__vector_scale;
        par_dest->display__vector_scale__set = TRUE;
    }
    
    if (force 
        || (  par_src->display__vector_color__set 
              && !par_dest->display__vector_color__set)) {
        par_dest->display__vector_color = par_src->display__vector_color;
        par_dest->display__vector_color__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__zoom_index__set 
             && !par_dest->display__zoom_index__set)) {
        par_dest->display__zoom_index = par_src->display__zoom_index;
        par_dest->display__zoom_index__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__backgrnd__set 
             && !par_dest->display__backgrnd__set)) {
        par_dest->display__backgrnd = par_src->display__backgrnd;
        par_dest->display__backgrnd__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__intregs__set 
             && !par_dest->display__intregs__set)) {
        par_dest->display__intregs = par_src->display__intregs;
        par_dest->display__intregs__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__piv__set 
             && !par_dest->display__piv__set)) {
        par_dest->display__piv = par_src->display__piv;
        par_dest->display__piv__set = TRUE;
    }
    
    if (force 
        || ( par_src->display__scalar__set 
             && !par_dest->display__scalar__set)) {
        par_dest->display__scalar = par_src->display__scalar;
        par_dest->display__scalar__set = TRUE;
    }

#ifdef ENABLE_MPI
    if (force 
        || ( par_src->mpi_nodes__set 
             && !par_dest->mpi_nodes__set)) {
        par_dest->mpi_nodes = par_src->mpi_nodes;
        par_dest->mpi_nodes__set = TRUE;
    }
#endif /* ENABLE_MPI */

}
