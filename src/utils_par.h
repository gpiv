/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

----------------------------------------------------------------------*/

/*
 * $Log: utils_par.h,v $
 * Revision 1.1  2008-09-16 11:24:37  gerber
 * added utils_par
 *
 */


#ifndef UTILS_PAR_H
#define UTILS_PAR_H

void
print_parameters (Par *par
                  );

void
parameters_set (Par *par,
                gboolean bool
                );

gchar *
cp_undef_parameters (Par *par_src,
                     Par *par_dest
                     );

Par *
cp_parameters (Par *par_src);


#endif /* UTILS_PAR_H */
